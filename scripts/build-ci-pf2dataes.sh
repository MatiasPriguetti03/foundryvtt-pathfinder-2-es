#!/bin/bash

set -e

echo "Updating pf2-data-es repository",
./update-pf2dataes.py

echo "Done"