# Estado de la traducción (pathfinder-bestiary-3-items)

 * **ninguna**: 343
 * **auto-trad**: 2895
 * **vacía**: 136
 * **modificada**: 30


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[01e3LF3IBDnqhKXi.htm](pathfinder-bestiary-3-items/01e3LF3IBDnqhKXi.htm)|Magic Aura (Constant) (Shaukeen and its Items Only)|
|[025oERI6JXgOoPWL.htm](pathfinder-bestiary-3-items/025oERI6JXgOoPWL.htm)|Pass Without Trace (Constant)|
|[0CCLws9uCZ7vqR3l.htm](pathfinder-bestiary-3-items/0CCLws9uCZ7vqR3l.htm)|Air Walk (At Will)|
|[0FzHP2R1UYaIKxeF.htm](pathfinder-bestiary-3-items/0FzHP2R1UYaIKxeF.htm)|Clairvoyance (At Will)|
|[0IiLvdj1EuziMc4L.htm](pathfinder-bestiary-3-items/0IiLvdj1EuziMc4L.htm)|Tongues (Constant)|
|[0lXyiooxNtOuTiyT.htm](pathfinder-bestiary-3-items/0lXyiooxNtOuTiyT.htm)|Dimension Door (At Will)|
|[0zFvRc93Gl6eANug.htm](pathfinder-bestiary-3-items/0zFvRc93Gl6eANug.htm)|Halberd|+1|
|[1b748L6U33YBgL1V.htm](pathfinder-bestiary-3-items/1b748L6U33YBgL1V.htm)|Undetectable Alignment (Constant)|
|[1DAzbaSEKgnFKzqV.htm](pathfinder-bestiary-3-items/1DAzbaSEKgnFKzqV.htm)|True Seeing (Constant)|
|[1elKyf9JIeMq5jrZ.htm](pathfinder-bestiary-3-items/1elKyf9JIeMq5jrZ.htm)|Fan Bolts|
|[1jFj68yQe0moSwH1.htm](pathfinder-bestiary-3-items/1jFj68yQe0moSwH1.htm)|Circle of Protection (Constant)|
|[1mdGEjHdiIWq524G.htm](pathfinder-bestiary-3-items/1mdGEjHdiIWq524G.htm)|Air Walk (Constant)|
|[1rv8rr7V1SPtaWM4.htm](pathfinder-bestiary-3-items/1rv8rr7V1SPtaWM4.htm)|True Seeing (Constant)|
|[1UZfCsTCiCQdrzdC.htm](pathfinder-bestiary-3-items/1UZfCsTCiCQdrzdC.htm)|Dream Message (At Will)|
|[1VZg8BVuvg7BFe9c.htm](pathfinder-bestiary-3-items/1VZg8BVuvg7BFe9c.htm)|Earthbind (At Will)|
|[1WtEtbp6ITuVjGUO.htm](pathfinder-bestiary-3-items/1WtEtbp6ITuVjGUO.htm)|Water Walk (Constant)|
|[1YwW47mP6ygWjXlh.htm](pathfinder-bestiary-3-items/1YwW47mP6ygWjXlh.htm)|+2 Resilient Glamered Full Plate|
|[2dwXilE1hqrwdBdk.htm](pathfinder-bestiary-3-items/2dwXilE1hqrwdBdk.htm)|Spear|+1,striking|
|[2EhHblW8AgWdPhB8.htm](pathfinder-bestiary-3-items/2EhHblW8AgWdPhB8.htm)|Detect Alignment (At Will) (Evil Only)|
|[2etpUEez5sWOqlMb.htm](pathfinder-bestiary-3-items/2etpUEez5sWOqlMb.htm)|True Seeing (Constant)|
|[2if951NcJCXU3XXv.htm](pathfinder-bestiary-3-items/2if951NcJCXU3XXv.htm)|Resurrect (Doesn't Require Secondary Casters)|
|[2m8xZRm2hISAn5U0.htm](pathfinder-bestiary-3-items/2m8xZRm2hISAn5U0.htm)|Charm (Undead Only)|
|[2nQjq9UXbBGNp21e.htm](pathfinder-bestiary-3-items/2nQjq9UXbBGNp21e.htm)|Detect Alignment (At Will)|
|[2tww80yq8tBSw5KT.htm](pathfinder-bestiary-3-items/2tww80yq8tBSw5KT.htm)|Blowgun Darts with Harvester Poison|
|[3Lzyr6PVBTQSPZzD.htm](pathfinder-bestiary-3-items/3Lzyr6PVBTQSPZzD.htm)|Flail|+1,striking|
|[3YK02GIFVfg4gqYs.htm](pathfinder-bestiary-3-items/3YK02GIFVfg4gqYs.htm)|Plane Shift (At Will)|
|[41XCv8P4PnillpYy.htm](pathfinder-bestiary-3-items/41XCv8P4PnillpYy.htm)|Longsword|+1,striking|
|[4rONi7rqC9gJLQZT.htm](pathfinder-bestiary-3-items/4rONi7rqC9gJLQZT.htm)|+2 Greater Resilient Full Plate|
|[4v1IFAFpIxXq9Uzv.htm](pathfinder-bestiary-3-items/4v1IFAFpIxXq9Uzv.htm)|Remove Curse (At Will)|
|[57if0qj4jc4HcQGo.htm](pathfinder-bestiary-3-items/57if0qj4jc4HcQGo.htm)|Nondetection (Constant) (Self Only)|
|[5KlhepR2kBxgHDeS.htm](pathfinder-bestiary-3-items/5KlhepR2kBxgHDeS.htm)|Clairaudience (At Will)|
|[6frIjAjjbonxZDls.htm](pathfinder-bestiary-3-items/6frIjAjjbonxZDls.htm)|Endure Elements (Constant)|
|[6gxrdqOXjumJ5L6w.htm](pathfinder-bestiary-3-items/6gxrdqOXjumJ5L6w.htm)|Sound Burst (At Will)|
|[6ruimm3IigTOGGP3.htm](pathfinder-bestiary-3-items/6ruimm3IigTOGGP3.htm)|Tongues (Constant)|
|[6S6IDhHyRogz5bB7.htm](pathfinder-bestiary-3-items/6S6IDhHyRogz5bB7.htm)|Illusory Object (At Will)|
|[6utF0bwgFoQkxHUD.htm](pathfinder-bestiary-3-items/6utF0bwgFoQkxHUD.htm)|True Seeing (Constant)|
|[6Xmm3pc3idD2x94u.htm](pathfinder-bestiary-3-items/6Xmm3pc3idD2x94u.htm)|Create Water (At Will)|
|[76GyDdxf5paIWLH7.htm](pathfinder-bestiary-3-items/76GyDdxf5paIWLH7.htm)|Air Walk (Constant)|
|[76utl0ImcsGHY3qX.htm](pathfinder-bestiary-3-items/76utl0ImcsGHY3qX.htm)|Freedom of Movement (At Will)|
|[7FMwxdPwaQDAu4Bt.htm](pathfinder-bestiary-3-items/7FMwxdPwaQDAu4Bt.htm)|Invisibility (At Will) (Self Only)|
|[7iPW2qJv28sNIMlG.htm](pathfinder-bestiary-3-items/7iPW2qJv28sNIMlG.htm)|Tongues (Constant)|
|[7je9N9tvy1YfaYoe.htm](pathfinder-bestiary-3-items/7je9N9tvy1YfaYoe.htm)|Create Water (At Will)|
|[7kWnGY8jWtyZFMYQ.htm](pathfinder-bestiary-3-items/7kWnGY8jWtyZFMYQ.htm)|Detect Alignment (At Will) (Evil Only)|
|[7N5JvxeRQzcnB5rG.htm](pathfinder-bestiary-3-items/7N5JvxeRQzcnB5rG.htm)|Create Water (At Will)|
|[7qC64GqGP34ClPu4.htm](pathfinder-bestiary-3-items/7qC64GqGP34ClPu4.htm)|True Seeing (Constant)|
|[7ueu1lLVg5YdZ0pi.htm](pathfinder-bestiary-3-items/7ueu1lLVg5YdZ0pi.htm)|Create Undead (Doesn't Require Secondary Casters)|
|[82eOb078T13bwHGH.htm](pathfinder-bestiary-3-items/82eOb078T13bwHGH.htm)|Shape Stone (See Idols of Stone)|
|[82PUYYf4hPvhXKja.htm](pathfinder-bestiary-3-items/82PUYYf4hPvhXKja.htm)|Fear (At Will)|
|[8d5s5kWfxsTt1E7x.htm](pathfinder-bestiary-3-items/8d5s5kWfxsTt1E7x.htm)|Scythe|+1,striking|
|[8fuYbarODhWt1FXZ.htm](pathfinder-bestiary-3-items/8fuYbarODhWt1FXZ.htm)|Invisibility (At Will) (Self Only)|
|[8geadWeP6EV4w18U.htm](pathfinder-bestiary-3-items/8geadWeP6EV4w18U.htm)|Charm (At Will)|
|[8jSa46Rumwn8HF3e.htm](pathfinder-bestiary-3-items/8jSa46Rumwn8HF3e.htm)|Dominate (At Will) (See Doninate)|
|[8pagn6n2lyrn1sVZ.htm](pathfinder-bestiary-3-items/8pagn6n2lyrn1sVZ.htm)|Scroll of Confusion (Level 4)|
|[8pbf8Jlvt2FtaVXg.htm](pathfinder-bestiary-3-items/8pbf8Jlvt2FtaVXg.htm)|Spear|+1,striking,returning|
|[8YB1Ghz9CBsNGNNe.htm](pathfinder-bestiary-3-items/8YB1Ghz9CBsNGNNe.htm)|Invisibility (At Will) (Self Only)|
|[9azjuUmcCUGNaceC.htm](pathfinder-bestiary-3-items/9azjuUmcCUGNaceC.htm)|Ventriloquism (At Will)|
|[9bZvu1KELcz1AAxo.htm](pathfinder-bestiary-3-items/9bZvu1KELcz1AAxo.htm)|Dimension Door (At Will)|
|[9f0C9k9fO0tU9t1k.htm](pathfinder-bestiary-3-items/9f0C9k9fO0tU9t1k.htm)|Detect Alignment (At Will) (Evil Only)|
|[9jiKLswJEKTZnpYW.htm](pathfinder-bestiary-3-items/9jiKLswJEKTZnpYW.htm)|Charm (Animals Only)|
|[9RHcmhuS5shME3T2.htm](pathfinder-bestiary-3-items/9RHcmhuS5shME3T2.htm)|Greatclub|+3,majorStriking|
|[9utEXpSfzcSAzWEq.htm](pathfinder-bestiary-3-items/9utEXpSfzcSAzWEq.htm)|Paranoia (At Will)|
|[9xFBrkHYg5Q7QAn6.htm](pathfinder-bestiary-3-items/9xFBrkHYg5Q7QAn6.htm)|Earthbind (At Will)|
|[A1bXxv71EPG8m8nv.htm](pathfinder-bestiary-3-items/A1bXxv71EPG8m8nv.htm)|Tongues (Constant)|
|[Ae2lLfIjacCnLEfD.htm](pathfinder-bestiary-3-items/Ae2lLfIjacCnLEfD.htm)|Floating Disk (see Disk Rider)|
|[AelQuSvbNDOMD6xd.htm](pathfinder-bestiary-3-items/AelQuSvbNDOMD6xd.htm)|Humanoid Form (At Will)|
|[aHZC7D82nkogCiRc.htm](pathfinder-bestiary-3-items/aHZC7D82nkogCiRc.htm)|Air Walk (Constant) (Self Only)|
|[AiejWhSgj1tzGlOW.htm](pathfinder-bestiary-3-items/AiejWhSgj1tzGlOW.htm)|True Seeing (Constant)|
|[aLEUgg9lhbABSHFk.htm](pathfinder-bestiary-3-items/aLEUgg9lhbABSHFk.htm)|Fear (Animals, Fungi, And Plants Only)|
|[amsXIUz7b4eRuZRR.htm](pathfinder-bestiary-3-items/amsXIUz7b4eRuZRR.htm)|Dagger|+1,striking,returning|
|[anJNKDz3R8ApuLb2.htm](pathfinder-bestiary-3-items/anJNKDz3R8ApuLb2.htm)|Floating Disk (At Will)|
|[aox1nvPhhyKKaTi9.htm](pathfinder-bestiary-3-items/aox1nvPhhyKKaTi9.htm)|Scroll of Fly (Level 4)|
|[APy2XnP43Ct9hDjp.htm](pathfinder-bestiary-3-items/APy2XnP43Ct9hDjp.htm)|True Seeing (Constant)|
|[AR1CzPoZmsKpCqkD.htm](pathfinder-bestiary-3-items/AR1CzPoZmsKpCqkD.htm)|Plane Shift (To Or From The Shadow Plane Only)|
|[AW7uAsCknGCb4PDf.htm](pathfinder-bestiary-3-items/AW7uAsCknGCb4PDf.htm)|Read Omens (At Will)|
|[B5UmGa9iTcz4fPFG.htm](pathfinder-bestiary-3-items/B5UmGa9iTcz4fPFG.htm)|Clairaudience (At Will)|
|[B9hUiPWhzaBpL0E4.htm](pathfinder-bestiary-3-items/B9hUiPWhzaBpL0E4.htm)|Water Walk (Constant)|
|[bBRh5P56Wfgd2pT1.htm](pathfinder-bestiary-3-items/bBRh5P56Wfgd2pT1.htm)|Scroll of Fly (Level 4)|
|[BhejT6zPZ9fehFxC.htm](pathfinder-bestiary-3-items/BhejT6zPZ9fehFxC.htm)|Tree Shape (See Forest Shape)|
|[bnDqqEqGa8k1Und0.htm](pathfinder-bestiary-3-items/bnDqqEqGa8k1Und0.htm)|Plane Shift (Self Only)|
|[bpNYXau9AAUwSo12.htm](pathfinder-bestiary-3-items/bpNYXau9AAUwSo12.htm)|Invisibility (At Will) (Self Only)|
|[BrYFJCgLri32rKt1.htm](pathfinder-bestiary-3-items/BrYFJCgLri32rKt1.htm)|Tongues (Constant)|
|[BWPZgNFWXwjeS6rr.htm](pathfinder-bestiary-3-items/BWPZgNFWXwjeS6rr.htm)|Clairaudience (At Will)|
|[C1CsUuSohUDsve6I.htm](pathfinder-bestiary-3-items/C1CsUuSohUDsve6I.htm)|Air Walk (Constant)|
|[c2bv8VJPrEoXworT.htm](pathfinder-bestiary-3-items/c2bv8VJPrEoXworT.htm)|Ventriloquism (At Will)|
|[C2fTIwqzj871lbht.htm](pathfinder-bestiary-3-items/C2fTIwqzj871lbht.htm)|Fear (At Will)|
|[C8Ncqnw55IXOEyme.htm](pathfinder-bestiary-3-items/C8Ncqnw55IXOEyme.htm)|Pest Form (Monkey Only)|
|[cfPdfe58QE4eOHM0.htm](pathfinder-bestiary-3-items/cfPdfe58QE4eOHM0.htm)|Fear (At Will)|
|[cgNs8rXo0upiL2I7.htm](pathfinder-bestiary-3-items/cgNs8rXo0upiL2I7.htm)|Invisibility (Self Only) (At Will)|
|[cRVYRqrBJjmjPGU5.htm](pathfinder-bestiary-3-items/cRVYRqrBJjmjPGU5.htm)|Augury (At Will)|
|[ctbxOCAPmqkVJNSg.htm](pathfinder-bestiary-3-items/ctbxOCAPmqkVJNSg.htm)|Detect Alignment (At Will) (Evil Only)|
|[CTcOXdYuenSJsDQC.htm](pathfinder-bestiary-3-items/CTcOXdYuenSJsDQC.htm)|Suggestion (At Will)|
|[ctFijrOvAkTERCu9.htm](pathfinder-bestiary-3-items/ctFijrOvAkTERCu9.htm)|Water Breathing (Constant)|
|[CU8RCoSIPpjOMZoO.htm](pathfinder-bestiary-3-items/CU8RCoSIPpjOMZoO.htm)|Greataxe|+1,striking|
|[d5EqozxY8XEY3NIw.htm](pathfinder-bestiary-3-items/d5EqozxY8XEY3NIw.htm)|Telekinetic Maneuver (At Will)|
|[dCFKo5jQVRwny9tZ.htm](pathfinder-bestiary-3-items/dCFKo5jQVRwny9tZ.htm)|Control Water (At Will)|
|[dKid8PGYfgX6asC5.htm](pathfinder-bestiary-3-items/dKid8PGYfgX6asC5.htm)|Detect Alignment (Constant)|
|[DTNOBV0vRtCJQIK8.htm](pathfinder-bestiary-3-items/DTNOBV0vRtCJQIK8.htm)|+1 Resilient Hide Armor|
|[DVNHPkNXh0og7150.htm](pathfinder-bestiary-3-items/DVNHPkNXh0og7150.htm)|Dimension Door (At Will)|
|[EKw1H7sHPZrfrDL9.htm](pathfinder-bestiary-3-items/EKw1H7sHPZrfrDL9.htm)|Composite Longbow|+1,striking|
|[eLmaLQ51OWOQK6bj.htm](pathfinder-bestiary-3-items/eLmaLQ51OWOQK6bj.htm)|Divine Decree (Evil Only)|
|[ELpuKrFNyvwzCapj.htm](pathfinder-bestiary-3-items/ELpuKrFNyvwzCapj.htm)|True Strike (At Will)|
|[eno8CnD1EnlY1yIZ.htm](pathfinder-bestiary-3-items/eno8CnD1EnlY1yIZ.htm)|Zone of Truth (At Will)|
|[ETR9TOwQKjt3PBxR.htm](pathfinder-bestiary-3-items/ETR9TOwQKjt3PBxR.htm)|Sudden Blight (At Will)|
|[Eyuxywd5xyJvHMPE.htm](pathfinder-bestiary-3-items/Eyuxywd5xyJvHMPE.htm)|Mind Reading (At Will)|
|[f27aQS5iwwcDr82I.htm](pathfinder-bestiary-3-items/f27aQS5iwwcDr82I.htm)|Full Plate (see Death Blaze)|
|[f44ALHTlp3xIkYpz.htm](pathfinder-bestiary-3-items/f44ALHTlp3xIkYpz.htm)|Tree Shape (See Forest Shape)|
|[fCrkAswBNGcLQ2S5.htm](pathfinder-bestiary-3-items/fCrkAswBNGcLQ2S5.htm)|Detect Alignment (At Will) (Evil Only)|
|[fkJeFr0LCaDT6DYo.htm](pathfinder-bestiary-3-items/fkJeFr0LCaDT6DYo.htm)|Air Walk (Constant)|
|[FoWz6fNCdey5pLcj.htm](pathfinder-bestiary-3-items/FoWz6fNCdey5pLcj.htm)|Dispel Magic (At Will)|
|[fpKOIfEsphtEaCbR.htm](pathfinder-bestiary-3-items/fpKOIfEsphtEaCbR.htm)|Longsword|+1,striking,disrupting|
|[FpyBx0T91Ob8bRXz.htm](pathfinder-bestiary-3-items/FpyBx0T91Ob8bRXz.htm)|Mindlink (At Will)|
|[FWYR69TIoTEaL4Sc.htm](pathfinder-bestiary-3-items/FWYR69TIoTEaL4Sc.htm)|Speak with Animals (Constant)|
|[GeCGs1bKTNRXa8oP.htm](pathfinder-bestiary-3-items/GeCGs1bKTNRXa8oP.htm)|Comprehend Language (Self Only)|
|[gEPUwy8WYR35LnP8.htm](pathfinder-bestiary-3-items/gEPUwy8WYR35LnP8.htm)|Maul|+1|
|[gfVREKT4YiAXf0Sh.htm](pathfinder-bestiary-3-items/gfVREKT4YiAXf0Sh.htm)|Mind Reading (At Will)|
|[gg1skd2GL6lN70Op.htm](pathfinder-bestiary-3-items/gg1skd2GL6lN70Op.htm)|Foresight (Constant) (Self Only)|
|[gGq6qmIUe3LgDalo.htm](pathfinder-bestiary-3-items/gGq6qmIUe3LgDalo.htm)|Illusory Creature (At Will)|
|[GIMEaPqcCrWjsAx0.htm](pathfinder-bestiary-3-items/GIMEaPqcCrWjsAx0.htm)|Suggestion (At Will)|
|[gll6P1NY192pZXCX.htm](pathfinder-bestiary-3-items/gll6P1NY192pZXCX.htm)|Zealous Conviction (Self Only)|
|[GPFQRaqbTvsHzgLv.htm](pathfinder-bestiary-3-items/GPFQRaqbTvsHzgLv.htm)|Speak with Animals (Constant) (Spiders Only)|
|[GXP09LUUMpZjzZNK.htm](pathfinder-bestiary-3-items/GXP09LUUMpZjzZNK.htm)|Meld into Stone (At Will)|
|[h3rEsPLD8eRRgdqm.htm](pathfinder-bestiary-3-items/h3rEsPLD8eRRgdqm.htm)|Freedom of Movement (Constant)|
|[hagL5rqsb5F2Dib3.htm](pathfinder-bestiary-3-items/hagL5rqsb5F2Dib3.htm)|Detect Alignment (At Will) (Good or Evil Only)|
|[HdHa8fMuZx5zztbP.htm](pathfinder-bestiary-3-items/HdHa8fMuZx5zztbP.htm)|Voidglass Kukri|
|[HHcUzm4oVPHPOsqI.htm](pathfinder-bestiary-3-items/HHcUzm4oVPHPOsqI.htm)|Suggestion (At Will)|
|[HHzuVZDMyhnuUwVS.htm](pathfinder-bestiary-3-items/HHzuVZDMyhnuUwVS.htm)|Speak with Animals (Constant)|
|[HMAJFIpbicpFe1je.htm](pathfinder-bestiary-3-items/HMAJFIpbicpFe1je.htm)|Charm (Undead Only)|
|[HpKFDo446U6j9P11.htm](pathfinder-bestiary-3-items/HpKFDo446U6j9P11.htm)|Composite Longbow|+1|
|[hR5BG9TXTZY8EKQh.htm](pathfinder-bestiary-3-items/hR5BG9TXTZY8EKQh.htm)|Glyph of Warding (At Will)|
|[Hr6tT6rCEgL0bZVy.htm](pathfinder-bestiary-3-items/Hr6tT6rCEgL0bZVy.htm)|Speak with Animals (At Will)|
|[HRwyZKqvUxP3jUSQ.htm](pathfinder-bestiary-3-items/HRwyZKqvUxP3jUSQ.htm)|Plane Shift (Self Only) (Astral or Material Plane Only)|
|[HulNPVFqPe3KCKvw.htm](pathfinder-bestiary-3-items/HulNPVFqPe3KCKvw.htm)|+1 Resilient Breastplate|
|[hZBLQC0oOFlrs0Tt.htm](pathfinder-bestiary-3-items/hZBLQC0oOFlrs0Tt.htm)|Fear (At Will)|
|[I5elsFKNOWOCVOwx.htm](pathfinder-bestiary-3-items/I5elsFKNOWOCVOwx.htm)|Falchion|+2,striking|
|[I6HjzvdkLUHJLowS.htm](pathfinder-bestiary-3-items/I6HjzvdkLUHJLowS.htm)|Detect Alignment (At Will)|
|[Idr71bCK1gM6nuVc.htm](pathfinder-bestiary-3-items/Idr71bCK1gM6nuVc.htm)|Blight (Doesn't Require Secondary Casters)|
|[IegAA8jyEDik5QGH.htm](pathfinder-bestiary-3-items/IegAA8jyEDik5QGH.htm)|Darkness (At Will)|
|[IELKs5btgBCSQgGs.htm](pathfinder-bestiary-3-items/IELKs5btgBCSQgGs.htm)|Misdirection (At Will) (Self Only)|
|[iFLHtzCCLkMeOw2v.htm](pathfinder-bestiary-3-items/iFLHtzCCLkMeOw2v.htm)|Longsword|+1,striking|
|[iGgnvBVMpX59E7rh.htm](pathfinder-bestiary-3-items/iGgnvBVMpX59E7rh.htm)|Invisibility (Self Only)|
|[iinBWMkulBkpeUxH.htm](pathfinder-bestiary-3-items/iinBWMkulBkpeUxH.htm)|Dispel Magic (At Will)|
|[IKkbf0MnPy4EbhpO.htm](pathfinder-bestiary-3-items/IKkbf0MnPy4EbhpO.htm)|Read Omens (At Will)|
|[IKYih1OQrUQy2d83.htm](pathfinder-bestiary-3-items/IKYih1OQrUQy2d83.htm)|Fear (At Will)|
|[iN9SAyDZsYVDzYfN.htm](pathfinder-bestiary-3-items/iN9SAyDZsYVDzYfN.htm)|Mind Reading (At Will)|
|[ISNwBWNpl1RrSuwh.htm](pathfinder-bestiary-3-items/ISNwBWNpl1RrSuwh.htm)|Invisibility (At Will) (Self Only)|
|[it8V6trXRaEs8bg1.htm](pathfinder-bestiary-3-items/it8V6trXRaEs8bg1.htm)|Charm (At Will)|
|[IYB1MIKRfITclT4i.htm](pathfinder-bestiary-3-items/IYB1MIKRfITclT4i.htm)|Suggestion (At Will)|
|[iZEu9jYTf7nhB7z5.htm](pathfinder-bestiary-3-items/iZEu9jYTf7nhB7z5.htm)|Composite Shortbow|+1,striking|
|[J5Is1PpCXNmzaN0y.htm](pathfinder-bestiary-3-items/J5Is1PpCXNmzaN0y.htm)|Maze (Once per Week)|
|[jbVk6BJ2TRpBl0PN.htm](pathfinder-bestiary-3-items/jbVk6BJ2TRpBl0PN.htm)|Composite Longbow|+1,striking|
|[jcu8xSbi5cUAAu26.htm](pathfinder-bestiary-3-items/jcu8xSbi5cUAAu26.htm)|+2 Greater Resilient Breastplate|
|[Jd2DYD67ChktNYVN.htm](pathfinder-bestiary-3-items/Jd2DYD67ChktNYVN.htm)|Tongues (Constant)|
|[jDheLY8hqIt7kVGR.htm](pathfinder-bestiary-3-items/jDheLY8hqIt7kVGR.htm)|Dispel Magic (At Will)|
|[JGruUFHuCKGitKYf.htm](pathfinder-bestiary-3-items/JGruUFHuCKGitKYf.htm)|Magic Aura (At Will)|
|[jhyOsq6KuZehFYax.htm](pathfinder-bestiary-3-items/jhyOsq6KuZehFYax.htm)|Pass Without Trace (Constant) (Forest Terrain Only)|
|[jidSgetwzkY6k5WH.htm](pathfinder-bestiary-3-items/jidSgetwzkY6k5WH.htm)|See Invisibility (Constant)|
|[JxGDiOzvqKBoFWu5.htm](pathfinder-bestiary-3-items/JxGDiOzvqKBoFWu5.htm)|Plane Shift (Self Only)|
|[JZJkUq4BEg1IAg4W.htm](pathfinder-bestiary-3-items/JZJkUq4BEg1IAg4W.htm)|Tongues (Constant)|
|[K3dfOHkozmujfcG3.htm](pathfinder-bestiary-3-items/K3dfOHkozmujfcG3.htm)|Endure Elements (Self Only)|
|[K4gqGlmGETi7zB7J.htm](pathfinder-bestiary-3-items/K4gqGlmGETi7zB7J.htm)|Charm (Undead Only)|
|[k5ozmTQNA7IyzbmH.htm](pathfinder-bestiary-3-items/k5ozmTQNA7IyzbmH.htm)|Mind Reading (At Will)|
|[kCoPTtj3Cu57Xebn.htm](pathfinder-bestiary-3-items/kCoPTtj3Cu57Xebn.htm)|Halberd|+3,greaterStriking|
|[KdJvX2wvTeXTeu5i.htm](pathfinder-bestiary-3-items/KdJvX2wvTeXTeu5i.htm)|Dimension Door (At Will)|
|[KF8c1lT0YVj5lMG4.htm](pathfinder-bestiary-3-items/KF8c1lT0YVj5lMG4.htm)|Mind Reading (At Will)|
|[KgGTXxW5g0BAy1nV.htm](pathfinder-bestiary-3-items/KgGTXxW5g0BAy1nV.htm)|Spiked Chain|+1,striking,ghostTouch|
|[KHXOl15PR5DH02Gu.htm](pathfinder-bestiary-3-items/KHXOl15PR5DH02Gu.htm)|Dispel Magic (At Will)|
|[kIfdLdjbTbTKS03H.htm](pathfinder-bestiary-3-items/kIfdLdjbTbTKS03H.htm)|Comprehend Language (At Will) (Self Only)|
|[kVLMWbSRPeQyl3Oe.htm](pathfinder-bestiary-3-items/kVLMWbSRPeQyl3Oe.htm)|Spiritual Epidemic (At Will)|
|[l5mWQVGm4hAXrFYp.htm](pathfinder-bestiary-3-items/l5mWQVGm4hAXrFYp.htm)|Spiked Chain|+3,greaterStriking,cold-iron|
|[L86Ap8gRiTU3o6rf.htm](pathfinder-bestiary-3-items/L86Ap8gRiTU3o6rf.htm)|Dimension Door (At Will)|
|[lbhWyawC0OJPtNR4.htm](pathfinder-bestiary-3-items/lbhWyawC0OJPtNR4.htm)|Geas (Doesn't Require Secondary Casters And Can Target a Willing Creature of Any Level)|
|[lGFDmPhDxFNEVo95.htm](pathfinder-bestiary-3-items/lGFDmPhDxFNEVo95.htm)|Invisibility (At Will) (Self Only)|
|[Lni9y4yXW9RhxwLM.htm](pathfinder-bestiary-3-items/Lni9y4yXW9RhxwLM.htm)|Ventriloquism (At Will)|
|[LNRtNrzHIseTefIq.htm](pathfinder-bestiary-3-items/LNRtNrzHIseTefIq.htm)|Clockwork Wand|
|[LoE3dWpTJPk8g8Ei.htm](pathfinder-bestiary-3-items/LoE3dWpTJPk8g8Ei.htm)|Create Water (At Will)|
|[lpvW88Zc5DItTR6J.htm](pathfinder-bestiary-3-items/lpvW88Zc5DItTR6J.htm)|Water Walk (Constant)|
|[lRiN69RyGchL1AiD.htm](pathfinder-bestiary-3-items/lRiN69RyGchL1AiD.htm)|Speak with Animals (Constant)|
|[lTytlP2GFTlmWEkk.htm](pathfinder-bestiary-3-items/lTytlP2GFTlmWEkk.htm)|Haste (At Will) (Self Only)|
|[m0gDkd2xvDLRZxHw.htm](pathfinder-bestiary-3-items/m0gDkd2xvDLRZxHw.htm)|Invisibility (Self Only)|
|[M6L8pe2IvyC5P2i4.htm](pathfinder-bestiary-3-items/M6L8pe2IvyC5P2i4.htm)|Longspear|+1,striking|
|[M9CMfdS3cQy6u8eS.htm](pathfinder-bestiary-3-items/M9CMfdS3cQy6u8eS.htm)|Create Undead (No Secondary Caster Required)|
|[maMkCYAHEvq6tAqg.htm](pathfinder-bestiary-3-items/maMkCYAHEvq6tAqg.htm)|Invisibility (At Will)|
|[MGFv64yTRt7JOqf8.htm](pathfinder-bestiary-3-items/MGFv64yTRt7JOqf8.htm)|+1 Striking Felt Shears (as Dagger)|+1,striking|
|[MHmU7Zg92WbTJ7CF.htm](pathfinder-bestiary-3-items/MHmU7Zg92WbTJ7CF.htm)|Calm Emotions (At Will)|
|[mKGOHDAfBouobqfT.htm](pathfinder-bestiary-3-items/mKGOHDAfBouobqfT.htm)|Darkness (At Will)|
|[mOQO1nFke9ZyiXTD.htm](pathfinder-bestiary-3-items/mOQO1nFke9ZyiXTD.htm)|Mending (At Will)|
|[MtmIGyh1mEwfrLk8.htm](pathfinder-bestiary-3-items/MtmIGyh1mEwfrLk8.htm)|Ventriloquism (At Will)|
|[MvqE0WoDiotm6xOj.htm](pathfinder-bestiary-3-items/MvqE0WoDiotm6xOj.htm)|Gust of Wind (At Will)|
|[N6eFdrrSCUkH7776.htm](pathfinder-bestiary-3-items/N6eFdrrSCUkH7776.htm)|Misdirection (At Will) (Self Only)|
|[Nc80ofsnO8nL7SBk.htm](pathfinder-bestiary-3-items/Nc80ofsnO8nL7SBk.htm)|Nondetection (Constant) (Self Only)|
|[NCGFbhlnJHZ1rJp9.htm](pathfinder-bestiary-3-items/NCGFbhlnJHZ1rJp9.htm)|Clairvoyance (At Will)|
|[ner0C5vBZGTckOHF.htm](pathfinder-bestiary-3-items/ner0C5vBZGTckOHF.htm)|Obscuring Mist (At Will)|
|[nIqiNS5etJdv3J1R.htm](pathfinder-bestiary-3-items/nIqiNS5etJdv3J1R.htm)|Amulet|
|[nivZKXrJ6jbmI61w.htm](pathfinder-bestiary-3-items/nivZKXrJ6jbmI61w.htm)|Plane Shift (Self Only) (To Or From The Shadow Plane Only)|
|[NqfD7fDgNcg0SGu9.htm](pathfinder-bestiary-3-items/NqfD7fDgNcg0SGu9.htm)|True Seeing (Constant)|
|[nr0PMLn9MREOWXYD.htm](pathfinder-bestiary-3-items/nr0PMLn9MREOWXYD.htm)|Tongues (Constant)|
|[nROFFDEy5y0nKYS0.htm](pathfinder-bestiary-3-items/nROFFDEy5y0nKYS0.htm)|Suggestion (At Will)|
|[nrXR81qrvYlS1etB.htm](pathfinder-bestiary-3-items/nrXR81qrvYlS1etB.htm)|Chill Touch (Undead Only)|
|[O4OD37pDdJ7DDfXC.htm](pathfinder-bestiary-3-items/O4OD37pDdJ7DDfXC.htm)|Staff|+2,striking|
|[O4uO0WyOyJlypiSX.htm](pathfinder-bestiary-3-items/O4uO0WyOyJlypiSX.htm)|Charm (Undead Only)|
|[o6IceHXhizmbsddI.htm](pathfinder-bestiary-3-items/o6IceHXhizmbsddI.htm)|Invisibility (Self Only)|
|[O7lGGxeUoi47EMD0.htm](pathfinder-bestiary-3-items/O7lGGxeUoi47EMD0.htm)|Fear (At Will)|
|[O9kHIQhPaga6ZnIu.htm](pathfinder-bestiary-3-items/O9kHIQhPaga6ZnIu.htm)|Speak with Plants (At Will)|
|[ObwEq85BpNcwAeik.htm](pathfinder-bestiary-3-items/ObwEq85BpNcwAeik.htm)|Tongues (Constant)|
|[OcXCtZyWYIL0Reqo.htm](pathfinder-bestiary-3-items/OcXCtZyWYIL0Reqo.htm)|True Seeing (Constant)|
|[oeWCnT69F8n5CEeg.htm](pathfinder-bestiary-3-items/oeWCnT69F8n5CEeg.htm)|Plane Shift (Self Only)|
|[OeXYKvZIjZ3ny4IS.htm](pathfinder-bestiary-3-items/OeXYKvZIjZ3ny4IS.htm)|Glyph of Warding (At Will)|
|[OflSSMFtOIlEpn58.htm](pathfinder-bestiary-3-items/OflSSMFtOIlEpn58.htm)|Elegant Cane (As Mace)|
|[OHawqyo3r0zsutMI.htm](pathfinder-bestiary-3-items/OHawqyo3r0zsutMI.htm)|Shortsword|+1,striking|
|[OhLCOozHQSHlWn0N.htm](pathfinder-bestiary-3-items/OhLCOozHQSHlWn0N.htm)|True Seeing (Constant)|
|[oil97jTz67aGZk18.htm](pathfinder-bestiary-3-items/oil97jTz67aGZk18.htm)|Tongues (Constant)|
|[oqveVOVwv267LEYp.htm](pathfinder-bestiary-3-items/oqveVOVwv267LEYp.htm)|Speak with Animals (Constant)|
|[OsLma7Z7aL2Uy9WP.htm](pathfinder-bestiary-3-items/OsLma7Z7aL2Uy9WP.htm)|Pass Without Trace (Constant) (Forest Terrain Only)|
|[OTCTmOT1dbEFaHMD.htm](pathfinder-bestiary-3-items/OTCTmOT1dbEFaHMD.htm)|Detect Alignment (At Will)|
|[ou4TzsHhJ2YJ9Ub3.htm](pathfinder-bestiary-3-items/ou4TzsHhJ2YJ9Ub3.htm)|Fireball (At Will) (See Unstable Magic)|
|[oyLpBeZ7RvXbKxJA.htm](pathfinder-bestiary-3-items/oyLpBeZ7RvXbKxJA.htm)|Outcast's Curse (At Will)|
|[p2TaK9MrvykMKB0o.htm](pathfinder-bestiary-3-items/p2TaK9MrvykMKB0o.htm)|Dimension Door (At Will)|
|[p5VLQl4f56EqDMBY.htm](pathfinder-bestiary-3-items/p5VLQl4f56EqDMBY.htm)|Magic Aura (Constant) (Self Only)|
|[P9EmNj8ZddzoKHMT.htm](pathfinder-bestiary-3-items/P9EmNj8ZddzoKHMT.htm)|Possession (See Mind Swap)|
|[pbe04bBz3BMbUs0Q.htm](pathfinder-bestiary-3-items/pbe04bBz3BMbUs0Q.htm)|Suggestion (At Will)|
|[PCyOFsdSGo2P3emN.htm](pathfinder-bestiary-3-items/PCyOFsdSGo2P3emN.htm)|Glyph of Warding (At Will)|
|[pCYui6n9e9HBtE9b.htm](pathfinder-bestiary-3-items/pCYui6n9e9HBtE9b.htm)|Tree Stride (At Will)|
|[PDk5irePkj7swYlp.htm](pathfinder-bestiary-3-items/PDk5irePkj7swYlp.htm)|Hatchet|+1|
|[pEUpV6dWX1Vbavcs.htm](pathfinder-bestiary-3-items/pEUpV6dWX1Vbavcs.htm)|Glyph of Warding (At Will)|
|[pfRf6x3WGakhgEUH.htm](pathfinder-bestiary-3-items/pfRf6x3WGakhgEUH.htm)|Tongues (Constant)|
|[pHk7V2hLZrgzY4OE.htm](pathfinder-bestiary-3-items/pHk7V2hLZrgzY4OE.htm)|Charm (At Will)|
|[phln4muQIICQhLF5.htm](pathfinder-bestiary-3-items/phln4muQIICQhLF5.htm)|Illusory Disguise (At Will)|
|[pLvuVluNzMrdi4RY.htm](pathfinder-bestiary-3-items/pLvuVluNzMrdi4RY.htm)|Illusory Scene (At Will)|
|[pOCKvTWHNSvPbzYY.htm](pathfinder-bestiary-3-items/pOCKvTWHNSvPbzYY.htm)|See Invisibility (Constant)|
|[pQzYMrj90yKkCKzz.htm](pathfinder-bestiary-3-items/pQzYMrj90yKkCKzz.htm)|Mind Blank (Constant)|
|[PVVUt6V4luw3UnL9.htm](pathfinder-bestiary-3-items/PVVUt6V4luw3UnL9.htm)|Dimension Door (At Will)|
|[pWSuV2gScsPeanBP.htm](pathfinder-bestiary-3-items/pWSuV2gScsPeanBP.htm)|Divine Lance (Chaos or Evil)|
|[pwWN3Mp741VnQ6Lx.htm](pathfinder-bestiary-3-items/pwWN3Mp741VnQ6Lx.htm)|Command (Animals Only)|
|[q44VpDPavuRcix13.htm](pathfinder-bestiary-3-items/q44VpDPavuRcix13.htm)|Composite Shortbow|+1,striking|
|[q72VNUpEzEqiaYgo.htm](pathfinder-bestiary-3-items/q72VNUpEzEqiaYgo.htm)|Tongues (Constant)|
|[Q8UNdxr6PscskBIx.htm](pathfinder-bestiary-3-items/Q8UNdxr6PscskBIx.htm)|Flaming Sphere (At Will) (See Unstable Magic)|
|[qaQZJI6uMjww56l2.htm](pathfinder-bestiary-3-items/qaQZJI6uMjww56l2.htm)|Tongues (Constant)|
|[QBCimVCmKxfPzDh2.htm](pathfinder-bestiary-3-items/QBCimVCmKxfPzDh2.htm)|+1 Breastplate|
|[QC1IrArhxNW6ef3t.htm](pathfinder-bestiary-3-items/QC1IrArhxNW6ef3t.htm)|Longbow|+1|
|[qcMqwJBE8PblmJXd.htm](pathfinder-bestiary-3-items/qcMqwJBE8PblmJXd.htm)|Tongues (Constant)|
|[QIy5U5WATHHnpiex.htm](pathfinder-bestiary-3-items/QIy5U5WATHHnpiex.htm)|Touch of Idiocy (At Will)|
|[qRZz1ras2ekZ3Dnn.htm](pathfinder-bestiary-3-items/qRZz1ras2ekZ3Dnn.htm)|Crushing Despair (At Will)|
|[r1Ei390nYbTkJ2Ac.htm](pathfinder-bestiary-3-items/r1Ei390nYbTkJ2Ac.htm)|Air Walk (Constant)|
|[R8ZpJ5Qjyevn9M2I.htm](pathfinder-bestiary-3-items/R8ZpJ5Qjyevn9M2I.htm)|Control Weather (Doesn't Require Secondary Casters)|
|[RgzVnccNPsj40XQj.htm](pathfinder-bestiary-3-items/RgzVnccNPsj40XQj.htm)|Speak with Animals (Constant)|
|[RLIvbPC5j2g106DY.htm](pathfinder-bestiary-3-items/RLIvbPC5j2g106DY.htm)|Katana|+1|
|[RLNzraMr15AQohaZ.htm](pathfinder-bestiary-3-items/RLNzraMr15AQohaZ.htm)|Endure Elements (Constant)|
|[RqeQ9keiv0CzpNUe.htm](pathfinder-bestiary-3-items/RqeQ9keiv0CzpNUe.htm)|Pass Without Trace (Constant) (Forest Terrain Only)|
|[rREO3eXjpKJUOkJt.htm](pathfinder-bestiary-3-items/rREO3eXjpKJUOkJt.htm)|Dispel Magic (At Will)|
|[rRrYw3Yi8aOQcMIK.htm](pathfinder-bestiary-3-items/rRrYw3Yi8aOQcMIK.htm)|Detect Alignment (At Will)|
|[S1xpDPa446jLmH3J.htm](pathfinder-bestiary-3-items/S1xpDPa446jLmH3J.htm)|Mask of Terror (Self Only)|
|[s49RXHWrH4qqNcEe.htm](pathfinder-bestiary-3-items/s49RXHWrH4qqNcEe.htm)|Tree Stride (At Will) (Cherry Trees Only)|
|[s5b07GowVsMRP7ro.htm](pathfinder-bestiary-3-items/s5b07GowVsMRP7ro.htm)|Mask of Terror (At Will)|
|[S7iB7CDLRoVYT6Pp.htm](pathfinder-bestiary-3-items/S7iB7CDLRoVYT6Pp.htm)|Mind Reading (At Will)|
|[sieqVjLe7KzZuR5l.htm](pathfinder-bestiary-3-items/sieqVjLe7KzZuR5l.htm)|Clairvoyance (At Will)|
|[sLLDvETLI8wX6s36.htm](pathfinder-bestiary-3-items/sLLDvETLI8wX6s36.htm)|Heal (At Will)|
|[sLqam8gjwb8YnuXS.htm](pathfinder-bestiary-3-items/sLqam8gjwb8YnuXS.htm)|Voidglass Kukri|
|[Snsjj1UYj5mIU9UL.htm](pathfinder-bestiary-3-items/Snsjj1UYj5mIU9UL.htm)|Falchion|+2,greaterStriking|
|[SoMnIapfSLR7sCTB.htm](pathfinder-bestiary-3-items/SoMnIapfSLR7sCTB.htm)|Control Weather (No Secondary Caster Required)|
|[sTFtTWlMXrbtb4sx.htm](pathfinder-bestiary-3-items/sTFtTWlMXrbtb4sx.htm)|Detect Alignment (At Will) (Evil Only)|
|[STWxL9WtBfQg569j.htm](pathfinder-bestiary-3-items/STWxL9WtBfQg569j.htm)|Dimension Door (At Will)|
|[szK7m2cRz0pWuEUb.htm](pathfinder-bestiary-3-items/szK7m2cRz0pWuEUb.htm)|Mind Reading (At Will)|
|[TF0po3aZY65Jw1V4.htm](pathfinder-bestiary-3-items/TF0po3aZY65Jw1V4.htm)|Suggestion (At Will)|
|[TjfSgZMdZR3uPTrF.htm](pathfinder-bestiary-3-items/TjfSgZMdZR3uPTrF.htm)|Dimension Door (At Will)|
|[tpcdOVPJoLXH4D7L.htm](pathfinder-bestiary-3-items/tpcdOVPJoLXH4D7L.htm)|Planar Binding (Doesn't Require Secondary Casters)|
|[tqRZXY6ERk4h1lPa.htm](pathfinder-bestiary-3-items/tqRZXY6ERk4h1lPa.htm)|Nondetection (At Will) (Self Only)|
|[Ts3F6FIbPPOUlZZJ.htm](pathfinder-bestiary-3-items/Ts3F6FIbPPOUlZZJ.htm)|Pass Without Trace (Constant)|
|[TS6AnRbKZab05oJP.htm](pathfinder-bestiary-3-items/TS6AnRbKZab05oJP.htm)|Fly (At Will)|
|[TUW5DOM2gHCfHxkX.htm](pathfinder-bestiary-3-items/TUW5DOM2gHCfHxkX.htm)|Air Walk (Constant)|
|[tx21vhxchyBGHxOD.htm](pathfinder-bestiary-3-items/tx21vhxchyBGHxOD.htm)|Touch of Idiocy (At Will)|
|[U2e7s0f0SwOH3IDu.htm](pathfinder-bestiary-3-items/U2e7s0f0SwOH3IDu.htm)|Air Walk (Constant)|
|[U8FXRYDBOA6S2wc1.htm](pathfinder-bestiary-3-items/U8FXRYDBOA6S2wc1.htm)|Darkness (At Will)|
|[U9ViaHzmhkqzKJ3Q.htm](pathfinder-bestiary-3-items/U9ViaHzmhkqzKJ3Q.htm)|See Invisibility (Constant)|
|[uaFUxzIXa5N9QFrp.htm](pathfinder-bestiary-3-items/uaFUxzIXa5N9QFrp.htm)|Fly (Constant)|
|[uDHQXwHJ5JXGBLA0.htm](pathfinder-bestiary-3-items/uDHQXwHJ5JXGBLA0.htm)|Fire Shield (Constant)|
|[uM4bCzmiegIXPcJf.htm](pathfinder-bestiary-3-items/uM4bCzmiegIXPcJf.htm)|Meld into Stone (At Will)|
|[uN7b49VuSYz5eKNd.htm](pathfinder-bestiary-3-items/uN7b49VuSYz5eKNd.htm)|Water Walk (Constant)|
|[unMEeRrwlVMzjW7n.htm](pathfinder-bestiary-3-items/unMEeRrwlVMzjW7n.htm)|Mind Reading (At Will)|
|[uQOW7jTQlM2mFtVU.htm](pathfinder-bestiary-3-items/uQOW7jTQlM2mFtVU.htm)|Endure Elements (Self Only)|
|[usTqKqK0mzh2dkzZ.htm](pathfinder-bestiary-3-items/usTqKqK0mzh2dkzZ.htm)|Scroll of Dimensional Anchor (Level 4)|
|[uWPyQC3rQMHc7uh1.htm](pathfinder-bestiary-3-items/uWPyQC3rQMHc7uh1.htm)|Hypercognition (At Will)|
|[Uytd5UdwOMgJqTS5.htm](pathfinder-bestiary-3-items/Uytd5UdwOMgJqTS5.htm)|Tongues (Constant)|
|[V1DW1BLRqkZoZJFG.htm](pathfinder-bestiary-3-items/V1DW1BLRqkZoZJFG.htm)|Shape Stone (At Will)|
|[v43kRV9YPyaSeo23.htm](pathfinder-bestiary-3-items/v43kRV9YPyaSeo23.htm)|Air Walk (Constant)|
|[V4TijnwUl9hbxsnI.htm](pathfinder-bestiary-3-items/V4TijnwUl9hbxsnI.htm)|Greatpick|+3,greaterStriking|
|[V5i5HJfqqedgNK8E.htm](pathfinder-bestiary-3-items/V5i5HJfqqedgNK8E.htm)|Bind Undead (At Will)|
|[vFvsiqwCZ9mkVwHL.htm](pathfinder-bestiary-3-items/vFvsiqwCZ9mkVwHL.htm)|True Seeing (Constant)|
|[vg0HZLmdVPcQrpjQ.htm](pathfinder-bestiary-3-items/vg0HZLmdVPcQrpjQ.htm)|Speak with Animals (Constant)|
|[vjPEjP5swzJDQYdF.htm](pathfinder-bestiary-3-items/vjPEjP5swzJDQYdF.htm)|Plane Shift (Self and Mount Only)|
|[vOU0EcH2WqzHGtgl.htm](pathfinder-bestiary-3-items/vOU0EcH2WqzHGtgl.htm)|Air Walk (Constant)|
|[Vp57wsIQVmtG1jPa.htm](pathfinder-bestiary-3-items/Vp57wsIQVmtG1jPa.htm)|Suggestion (At Will)|
|[vQvd4d7TqB4xYYzG.htm](pathfinder-bestiary-3-items/vQvd4d7TqB4xYYzG.htm)|Silence (At Will)|
|[vQY8xEtGMI71ZFRC.htm](pathfinder-bestiary-3-items/vQY8xEtGMI71ZFRC.htm)|Outcast's Curse (At Will)|
|[VTqkprtwM4tCpoqy.htm](pathfinder-bestiary-3-items/VTqkprtwM4tCpoqy.htm)|True Seeing (Constant)|
|[VWEAgF7iG7hLA3uD.htm](pathfinder-bestiary-3-items/VWEAgF7iG7hLA3uD.htm)|Gaseous Form (At Will)|
|[w5870YWidWVrdHst.htm](pathfinder-bestiary-3-items/w5870YWidWVrdHst.htm)|Nondetection (Constant) (Self Only)|
|[W7hhuWr7UL3cytW8.htm](pathfinder-bestiary-3-items/W7hhuWr7UL3cytW8.htm)|Defiled Religious Symbol of Pharasma (Wooden)|
|[wA8VVomLPk1zjrds.htm](pathfinder-bestiary-3-items/wA8VVomLPk1zjrds.htm)|Handwraps of Mighty Blows|+1|
|[WAFwt6ZXf9QztSeJ.htm](pathfinder-bestiary-3-items/WAFwt6ZXf9QztSeJ.htm)|Create Water (At Will)|
|[wBBkm38TLkhTV2GZ.htm](pathfinder-bestiary-3-items/wBBkm38TLkhTV2GZ.htm)|Control Weather (No Secondary Caster Required)|
|[WBhfbegIvXs0rmDc.htm](pathfinder-bestiary-3-items/WBhfbegIvXs0rmDc.htm)|Air Walk (Constant)|
|[Wg3QK5gzo1szg9zD.htm](pathfinder-bestiary-3-items/Wg3QK5gzo1szg9zD.htm)|Mindlink (At Will)|
|[WjhRi0h58DIX9ATj.htm](pathfinder-bestiary-3-items/WjhRi0h58DIX9ATj.htm)|Suggestion (At Will)|
|[WOmi5gJbb9PPIAM8.htm](pathfinder-bestiary-3-items/WOmi5gJbb9PPIAM8.htm)|Ventriloquism (At Will)|
|[Wv5iLNU2m5Tyg4mv.htm](pathfinder-bestiary-3-items/Wv5iLNU2m5Tyg4mv.htm)|Dimension Door (At Will)|
|[wX6Fc9wZK3CK5WNe.htm](pathfinder-bestiary-3-items/wX6Fc9wZK3CK5WNe.htm)|Shadow Walk (See Shadow's Swiftness)|
|[Wzi3fuQeULw68X2T.htm](pathfinder-bestiary-3-items/Wzi3fuQeULw68X2T.htm)|Purify Food and Drink (At Will)|
|[x29TRtgx6HdtboNu.htm](pathfinder-bestiary-3-items/x29TRtgx6HdtboNu.htm)|Dimension Door (At Will)|
|[XAg8ikRjbnklrzff.htm](pathfinder-bestiary-3-items/XAg8ikRjbnklrzff.htm)|True Seeing (Constant)|
|[XAyHusrI0x96dmIN.htm](pathfinder-bestiary-3-items/XAyHusrI0x96dmIN.htm)|True Seeing (Constant)|
|[Xco2zlLptJvbWtBI.htm](pathfinder-bestiary-3-items/Xco2zlLptJvbWtBI.htm)|Dimension Door (At Will)|
|[xcujrw8UlSaahhu6.htm](pathfinder-bestiary-3-items/xcujrw8UlSaahhu6.htm)|Earthbind (At Will)|
|[xFikHberuOZ3zfc4.htm](pathfinder-bestiary-3-items/xFikHberuOZ3zfc4.htm)|Speak with Plants (Constant)|
|[xfMHekuTgDpCE7UY.htm](pathfinder-bestiary-3-items/xfMHekuTgDpCE7UY.htm)|See Invisibility (Constant)|
|[xhfzAaoPZ4coGOku.htm](pathfinder-bestiary-3-items/xhfzAaoPZ4coGOku.htm)|Detect Alignment (At Will) (Good or Evil Only)|
|[Xiw2DZbMz5qQ6sUm.htm](pathfinder-bestiary-3-items/Xiw2DZbMz5qQ6sUm.htm)|Tongues (Constant)|
|[xkZQodLXco1BCblV.htm](pathfinder-bestiary-3-items/xkZQodLXco1BCblV.htm)|Suggestion (At Will)|
|[XRASk2fmJyT1LAqe.htm](pathfinder-bestiary-3-items/XRASk2fmJyT1LAqe.htm)|Mind Reading (At Will)|
|[XrFiVMNybl9KRDD9.htm](pathfinder-bestiary-3-items/XrFiVMNybl9KRDD9.htm)|Speak with Animals (Constant)|
|[XrGSSYX0IvMaaBC0.htm](pathfinder-bestiary-3-items/XrGSSYX0IvMaaBC0.htm)|Stone Tell (Constant)|
|[xTlKFL3sVgzIZhmL.htm](pathfinder-bestiary-3-items/xTlKFL3sVgzIZhmL.htm)|Pass Without Trace (Constant)|
|[xWxtACkZ1XENgLax.htm](pathfinder-bestiary-3-items/xWxtACkZ1XENgLax.htm)|Greatclub|+1,striking|
|[XX6N64aoSGL9jWS6.htm](pathfinder-bestiary-3-items/XX6N64aoSGL9jWS6.htm)|Magic Weapon (At Will)|
|[xZlNbzJzZCNKvumU.htm](pathfinder-bestiary-3-items/xZlNbzJzZCNKvumU.htm)|Plane Shift (Self Only) (to the Material or Shadow Plane only)|
|[Y8b1W9ZdIMaUhI2I.htm](pathfinder-bestiary-3-items/Y8b1W9ZdIMaUhI2I.htm)|Magic Aura (Constant) (Self Only)|
|[Y9Hwv9W7YTv5qXrI.htm](pathfinder-bestiary-3-items/Y9Hwv9W7YTv5qXrI.htm)|Endure Elements (Self Only)|
|[YC9q2aIlbYj8sEUp.htm](pathfinder-bestiary-3-items/YC9q2aIlbYj8sEUp.htm)|Greatclub|+1,striking|
|[yfzdBwOPqpyuRl0x.htm](pathfinder-bestiary-3-items/yfzdBwOPqpyuRl0x.htm)|Flute|
|[YLG2dq3uetOa36Vb.htm](pathfinder-bestiary-3-items/YLG2dq3uetOa36Vb.htm)|Bind Undead (At Will)|
|[YLigc5sHGyr9ZE6A.htm](pathfinder-bestiary-3-items/YLigc5sHGyr9ZE6A.htm)|Detect Alignment (At Will) (Evil Only)|
|[YlQhHUez8TDobfxk.htm](pathfinder-bestiary-3-items/YlQhHUez8TDobfxk.htm)|Illusory Object (At Will)|
|[yMZjDluDwdyDmxeR.htm](pathfinder-bestiary-3-items/yMZjDluDwdyDmxeR.htm)|Dominate (At Will) (See Dominate)|
|[YNbqq2itxEP5DMSE.htm](pathfinder-bestiary-3-items/YNbqq2itxEP5DMSE.htm)|Wall of Fire (At Will) (See Unstable Magic)|
|[zANtgVUDX9St398r.htm](pathfinder-bestiary-3-items/zANtgVUDX9St398r.htm)|Tree Shape (Cherry Tree Only)|
|[ZarnbOm1D79SECpY.htm](pathfinder-bestiary-3-items/ZarnbOm1D79SECpY.htm)|Summon Animal (Spiders Only)|
|[zBG0dgN8cMdRn2tF.htm](pathfinder-bestiary-3-items/zBG0dgN8cMdRn2tF.htm)|Phantasmal Killer (Image Resembles the Brainchild)|
|[zChDwHyu2FWl3wIc.htm](pathfinder-bestiary-3-items/zChDwHyu2FWl3wIc.htm)|+1 Breastplate|
|[Zh5jidqsamP7xlFa.htm](pathfinder-bestiary-3-items/Zh5jidqsamP7xlFa.htm)|Detect Alignment (At Will) (Good or Evil Only)|
|[zr91h9LPlTzhqjrE.htm](pathfinder-bestiary-3-items/zr91h9LPlTzhqjrE.htm)|True Seeing (Constant)|
|[ZVB4BE6xf6rF4Gl7.htm](pathfinder-bestiary-3-items/ZVB4BE6xf6rF4Gl7.htm)|Tongues (Constant)|
|[zVHM5ms0PMEfCJcT.htm](pathfinder-bestiary-3-items/zVHM5ms0PMEfCJcT.htm)|Darkness (At Will)|
|[zvQC0uB9gzHndGBg.htm](pathfinder-bestiary-3-items/zvQC0uB9gzHndGBg.htm)|Air Walk (Constant)|
|[zWsIEzGEL4qx0Z0P.htm](pathfinder-bestiary-3-items/zWsIEzGEL4qx0Z0P.htm)|Scimitar|+1,striking|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[04WUnuVaMwggkQxr.htm](pathfinder-bestiary-3-items/04WUnuVaMwggkQxr.htm)|Plaque Burst|auto-trad|
|[06gks6jfRaUwycln.htm](pathfinder-bestiary-3-items/06gks6jfRaUwycln.htm)|Gift of Knowledge|auto-trad|
|[06mT1ZKXqT7oAL0x.htm](pathfinder-bestiary-3-items/06mT1ZKXqT7oAL0x.htm)|Talon|auto-trad|
|[06rO4SWWCzcZfK0D.htm](pathfinder-bestiary-3-items/06rO4SWWCzcZfK0D.htm)|Spiny Eurypterid Venom|auto-trad|
|[08t9rJT8yGrqWVpw.htm](pathfinder-bestiary-3-items/08t9rJT8yGrqWVpw.htm)|Darkvision|auto-trad|
|[08TcEyoA4GxnmNBv.htm](pathfinder-bestiary-3-items/08TcEyoA4GxnmNBv.htm)|Raise Grain Cloud|auto-trad|
|[0a3piFjb7YtsWrsN.htm](pathfinder-bestiary-3-items/0a3piFjb7YtsWrsN.htm)|Uncanny Pounce|auto-trad|
|[0AymbzLegfs9ibC8.htm](pathfinder-bestiary-3-items/0AymbzLegfs9ibC8.htm)|Tied to the Land|auto-trad|
|[0BvqGo4aw3xH9SrP.htm](pathfinder-bestiary-3-items/0BvqGo4aw3xH9SrP.htm)|Fan the Flames|auto-trad|
|[0cGHRE1RTMbDwp5J.htm](pathfinder-bestiary-3-items/0cGHRE1RTMbDwp5J.htm)|Tremorsense (Imprecise) 15 feet|auto-trad|
|[0cUUUVCjDDYMeTyQ.htm](pathfinder-bestiary-3-items/0cUUUVCjDDYMeTyQ.htm)|Darkvision|auto-trad|
|[0eBQQnVqxlDicgcI.htm](pathfinder-bestiary-3-items/0eBQQnVqxlDicgcI.htm)|Greater Darkvision|auto-trad|
|[0eEbWDjEXskh1gvK.htm](pathfinder-bestiary-3-items/0eEbWDjEXskh1gvK.htm)|Cold Adaptation|auto-trad|
|[0eyOInV05vjoOU3X.htm](pathfinder-bestiary-3-items/0eyOInV05vjoOU3X.htm)|Blinding Sulfur|auto-trad|
|[0fDCOqKLdjm45yXu.htm](pathfinder-bestiary-3-items/0fDCOqKLdjm45yXu.htm)|Resonance|auto-trad|
|[0fEFXQi4HqLkc4EL.htm](pathfinder-bestiary-3-items/0fEFXQi4HqLkc4EL.htm)|Raise a Shield|auto-trad|
|[0fGH8UJJm4ccTubw.htm](pathfinder-bestiary-3-items/0fGH8UJJm4ccTubw.htm)|Accord Essence|auto-trad|
|[0fyQTAFgkuG0R7JH.htm](pathfinder-bestiary-3-items/0fyQTAFgkuG0R7JH.htm)|Stinger|auto-trad|
|[0GjO0CZOnFyUsbIr.htm](pathfinder-bestiary-3-items/0GjO0CZOnFyUsbIr.htm)|Fast Healing 15|auto-trad|
|[0hiycK0RHpWLyxZG.htm](pathfinder-bestiary-3-items/0hiycK0RHpWLyxZG.htm)|No Breath|auto-trad|
|[0Hotofas3viOSSly.htm](pathfinder-bestiary-3-items/0Hotofas3viOSSly.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[0hSL8u2ImY5BTyJo.htm](pathfinder-bestiary-3-items/0hSL8u2ImY5BTyJo.htm)|Drain Life|auto-trad|
|[0Iag5l05kpRuaYEj.htm](pathfinder-bestiary-3-items/0Iag5l05kpRuaYEj.htm)|Philanthropic Bile|auto-trad|
|[0IOuDjFm6iLT2q5x.htm](pathfinder-bestiary-3-items/0IOuDjFm6iLT2q5x.htm)|Vanish into the Wilds|auto-trad|
|[0jNl0jg5W1N5NrTS.htm](pathfinder-bestiary-3-items/0jNl0jg5W1N5NrTS.htm)|Divine Innate Spells|auto-trad|
|[0k3Jmkc79tf6HAi0.htm](pathfinder-bestiary-3-items/0k3Jmkc79tf6HAi0.htm)|All-Around Vision|auto-trad|
|[0kBV6c4sgKTQp33O.htm](pathfinder-bestiary-3-items/0kBV6c4sgKTQp33O.htm)|Talon|auto-trad|
|[0LkFUNpvQRTipNKI.htm](pathfinder-bestiary-3-items/0LkFUNpvQRTipNKI.htm)|Constrict|auto-trad|
|[0LnVcBoOwSUOZfMh.htm](pathfinder-bestiary-3-items/0LnVcBoOwSUOZfMh.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[0nLSNrzLY7to9xEI.htm](pathfinder-bestiary-3-items/0nLSNrzLY7to9xEI.htm)|Form Up|auto-trad|
|[0NvY0SLrHGDIxxdQ.htm](pathfinder-bestiary-3-items/0NvY0SLrHGDIxxdQ.htm)|Darkvision|auto-trad|
|[0oYv3Dg6IHZomTdm.htm](pathfinder-bestiary-3-items/0oYv3Dg6IHZomTdm.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[0PhWUNCIDMNJ1T2x.htm](pathfinder-bestiary-3-items/0PhWUNCIDMNJ1T2x.htm)|Telepathy (Touch)|auto-trad|
|[0PU6xyU0Ewf6vUVD.htm](pathfinder-bestiary-3-items/0PU6xyU0Ewf6vUVD.htm)|Huntblade Brutality|auto-trad|
|[0QWcjdtxCTYgd8Bm.htm](pathfinder-bestiary-3-items/0QWcjdtxCTYgd8Bm.htm)|Autonomous Spell|auto-trad|
|[0Rn8CE4JcrTxdKqN.htm](pathfinder-bestiary-3-items/0Rn8CE4JcrTxdKqN.htm)|Easy to Call|auto-trad|
|[0taZfsX99OZREL4H.htm](pathfinder-bestiary-3-items/0taZfsX99OZREL4H.htm)|At-Will Spells|auto-trad|
|[0wqFlm8L407f7apV.htm](pathfinder-bestiary-3-items/0wqFlm8L407f7apV.htm)|Grab|auto-trad|
|[0XhFSYYoNSo9Uab7.htm](pathfinder-bestiary-3-items/0XhFSYYoNSo9Uab7.htm)|Claw|auto-trad|
|[0yTcCw02hLLf5qMi.htm](pathfinder-bestiary-3-items/0yTcCw02hLLf5qMi.htm)|Change Shape|auto-trad|
|[0zMlEvkv5OFS1XI2.htm](pathfinder-bestiary-3-items/0zMlEvkv5OFS1XI2.htm)|Low-Light Vision|auto-trad|
|[10hI8m7tJ9cKdeoE.htm](pathfinder-bestiary-3-items/10hI8m7tJ9cKdeoE.htm)|Hoof|auto-trad|
|[10pooBpkF2WtrqUt.htm](pathfinder-bestiary-3-items/10pooBpkF2WtrqUt.htm)|Consume Organ|auto-trad|
|[135ZjsQICC0n6hMK.htm](pathfinder-bestiary-3-items/135ZjsQICC0n6hMK.htm)|Hoof|auto-trad|
|[13p4CNlMthrJMobp.htm](pathfinder-bestiary-3-items/13p4CNlMthrJMobp.htm)|Negative Healing|auto-trad|
|[19AKAkXuEUEtoqkr.htm](pathfinder-bestiary-3-items/19AKAkXuEUEtoqkr.htm)|Recall the Fallen|auto-trad|
|[1a2EGaGWMPDZQqkX.htm](pathfinder-bestiary-3-items/1a2EGaGWMPDZQqkX.htm)|Harvester Poison|auto-trad|
|[1AH3QAMA4BbIHNxh.htm](pathfinder-bestiary-3-items/1AH3QAMA4BbIHNxh.htm)|Stinger|auto-trad|
|[1bmJGFD6zAmHJap5.htm](pathfinder-bestiary-3-items/1bmJGFD6zAmHJap5.htm)|Leng Ghoul Fever|auto-trad|
|[1BzSQkOqOPqFvz2u.htm](pathfinder-bestiary-3-items/1BzSQkOqOPqFvz2u.htm)|Dominate|auto-trad|
|[1CAm301u2swnFr8b.htm](pathfinder-bestiary-3-items/1CAm301u2swnFr8b.htm)|Horn|auto-trad|
|[1cGNt3AXx2z6VszS.htm](pathfinder-bestiary-3-items/1cGNt3AXx2z6VszS.htm)|Constant Spells|auto-trad|
|[1EJsPFGk71lyNvRf.htm](pathfinder-bestiary-3-items/1EJsPFGk71lyNvRf.htm)|Mauler|auto-trad|
|[1emEy5ty5iwJ1bDG.htm](pathfinder-bestiary-3-items/1emEy5ty5iwJ1bDG.htm)|Constant Spells|auto-trad|
|[1EYcnayZrJgCFXca.htm](pathfinder-bestiary-3-items/1EYcnayZrJgCFXca.htm)|Primal Innate Spells|auto-trad|
|[1F1dP9JtIaewb9ib.htm](pathfinder-bestiary-3-items/1F1dP9JtIaewb9ib.htm)|Catch Rock|auto-trad|
|[1F2GCLISmzNTXa9u.htm](pathfinder-bestiary-3-items/1F2GCLISmzNTXa9u.htm)|Extend Vines|auto-trad|
|[1GpBs3jPWwtTzZjf.htm](pathfinder-bestiary-3-items/1GpBs3jPWwtTzZjf.htm)|Impossible Stature|auto-trad|
|[1hdOnHb2baGiZGTs.htm](pathfinder-bestiary-3-items/1hdOnHb2baGiZGTs.htm)|Darkvision|auto-trad|
|[1hmYrhxS6td9Cs3g.htm](pathfinder-bestiary-3-items/1hmYrhxS6td9Cs3g.htm)|Heartsong|auto-trad|
|[1i0yksL3nMzWjZLz.htm](pathfinder-bestiary-3-items/1i0yksL3nMzWjZLz.htm)|Tickle|auto-trad|
|[1IBSMp5wMRvadNtl.htm](pathfinder-bestiary-3-items/1IBSMp5wMRvadNtl.htm)|Hyponatremia|auto-trad|
|[1iD6khc2Ev2LOLO7.htm](pathfinder-bestiary-3-items/1iD6khc2Ev2LOLO7.htm)|Perfect Camouflage|auto-trad|
|[1Ld61yL8VIWBvakX.htm](pathfinder-bestiary-3-items/1Ld61yL8VIWBvakX.htm)|Swarm Shape|auto-trad|
|[1LUodDO3qpr7CYqM.htm](pathfinder-bestiary-3-items/1LUodDO3qpr7CYqM.htm)|Violent Retort|auto-trad|
|[1lYiwUfYhh9bsuiW.htm](pathfinder-bestiary-3-items/1lYiwUfYhh9bsuiW.htm)|Buck|auto-trad|
|[1Nzt9YANcjiUjxZu.htm](pathfinder-bestiary-3-items/1Nzt9YANcjiUjxZu.htm)|Liquefy|auto-trad|
|[1oCth6jpnhdrJCOk.htm](pathfinder-bestiary-3-items/1oCth6jpnhdrJCOk.htm)|Walk the Ethereal Line|auto-trad|
|[1pEzu9hLGS2XL7cf.htm](pathfinder-bestiary-3-items/1pEzu9hLGS2XL7cf.htm)|Verdant Burst|auto-trad|
|[1qHeEusR10OOW67e.htm](pathfinder-bestiary-3-items/1qHeEusR10OOW67e.htm)|Sneak Attack|auto-trad|
|[1qvnCAYxCtqLHNWB.htm](pathfinder-bestiary-3-items/1qvnCAYxCtqLHNWB.htm)|Extend Limbs|auto-trad|
|[1RfKpuWxU1cMc4p7.htm](pathfinder-bestiary-3-items/1RfKpuWxU1cMc4p7.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[1RYNKTCRimaI46EV.htm](pathfinder-bestiary-3-items/1RYNKTCRimaI46EV.htm)|Sand Stride|auto-trad|
|[1Skzxnout1K9TVNP.htm](pathfinder-bestiary-3-items/1Skzxnout1K9TVNP.htm)|Tied to the Land|auto-trad|
|[1Ssl4YAndiCjS1sF.htm](pathfinder-bestiary-3-items/1Ssl4YAndiCjS1sF.htm)|At-Will Spells|auto-trad|
|[1sZ0LFXxfqkedyyb.htm](pathfinder-bestiary-3-items/1sZ0LFXxfqkedyyb.htm)|Swallow Whole|auto-trad|
|[1TjWSXYs7BlJtfxX.htm](pathfinder-bestiary-3-items/1TjWSXYs7BlJtfxX.htm)|Opening Statement|auto-trad|
|[1UaZoefxmocVHBRY.htm](pathfinder-bestiary-3-items/1UaZoefxmocVHBRY.htm)|Darkvision|auto-trad|
|[1URQxVXYnS7mCVrp.htm](pathfinder-bestiary-3-items/1URQxVXYnS7mCVrp.htm)|Occult Spontaneous Spells|auto-trad|
|[1vK61e5tiUHGEF7i.htm](pathfinder-bestiary-3-items/1vK61e5tiUHGEF7i.htm)|Attack of Opportunity|auto-trad|
|[1xKmwhrUAlCPlAwz.htm](pathfinder-bestiary-3-items/1xKmwhrUAlCPlAwz.htm)|Darkvision|auto-trad|
|[1YPP0PyKjErYFPk7.htm](pathfinder-bestiary-3-items/1YPP0PyKjErYFPk7.htm)|Paralytic Fear|auto-trad|
|[1zABoyA2x54PjRbW.htm](pathfinder-bestiary-3-items/1zABoyA2x54PjRbW.htm)|Staff|auto-trad|
|[1zDo0dT2WRDJ2XqD.htm](pathfinder-bestiary-3-items/1zDo0dT2WRDJ2XqD.htm)|Mist Vision|auto-trad|
|[20T7ujLS0ToWyxkl.htm](pathfinder-bestiary-3-items/20T7ujLS0ToWyxkl.htm)|Inhabit Object|auto-trad|
|[276uGZ3JI9bINWVh.htm](pathfinder-bestiary-3-items/276uGZ3JI9bINWVh.htm)|Greater Darkvision|auto-trad|
|[2815rJTOaYW8FTZW.htm](pathfinder-bestiary-3-items/2815rJTOaYW8FTZW.htm)|At-Will Spells|auto-trad|
|[2aPAtYqGlSZ40pDT.htm](pathfinder-bestiary-3-items/2aPAtYqGlSZ40pDT.htm)|Reflect Spell|auto-trad|
|[2bcr0iC80HhF1QZo.htm](pathfinder-bestiary-3-items/2bcr0iC80HhF1QZo.htm)|Inhabit Vessel|auto-trad|
|[2BRmODyR3yH6ev3Q.htm](pathfinder-bestiary-3-items/2BRmODyR3yH6ev3Q.htm)|Swamp Stride|auto-trad|
|[2CGRxGWozrsdSvIi.htm](pathfinder-bestiary-3-items/2CGRxGWozrsdSvIi.htm)|Low-Light Vision|auto-trad|
|[2cVkSactyUiKGzdD.htm](pathfinder-bestiary-3-items/2cVkSactyUiKGzdD.htm)|Backdrop|auto-trad|
|[2DJRrjRKoxQStF9N.htm](pathfinder-bestiary-3-items/2DJRrjRKoxQStF9N.htm)|Kukri|auto-trad|
|[2epurBlrSyaqWetH.htm](pathfinder-bestiary-3-items/2epurBlrSyaqWetH.htm)|Darkvision|auto-trad|
|[2ERFu4hiXliGhhKj.htm](pathfinder-bestiary-3-items/2ERFu4hiXliGhhKj.htm)|All-Around Vision|auto-trad|
|[2GzvaeDl0Dl1kjjw.htm](pathfinder-bestiary-3-items/2GzvaeDl0Dl1kjjw.htm)|Drink Blood|auto-trad|
|[2iPW0FxKyAPkbkDp.htm](pathfinder-bestiary-3-items/2iPW0FxKyAPkbkDp.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[2iSSH3y2Kuv4MNqO.htm](pathfinder-bestiary-3-items/2iSSH3y2Kuv4MNqO.htm)|Emotional Bolt|auto-trad|
|[2IybDR9dBLK8bfWV.htm](pathfinder-bestiary-3-items/2IybDR9dBLK8bfWV.htm)|Agent of Fate|auto-trad|
|[2JP7ZbSsTYsLb6ux.htm](pathfinder-bestiary-3-items/2JP7ZbSsTYsLb6ux.htm)|Aquatic Drag|auto-trad|
|[2JQEQa6cUhcYoUM4.htm](pathfinder-bestiary-3-items/2JQEQa6cUhcYoUM4.htm)|Tentacle|auto-trad|
|[2kcFz29KySrfv5cW.htm](pathfinder-bestiary-3-items/2kcFz29KySrfv5cW.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[2kFQsJv3fMYJ1RND.htm](pathfinder-bestiary-3-items/2kFQsJv3fMYJ1RND.htm)|Beak|auto-trad|
|[2kSrspDPmUktGvBB.htm](pathfinder-bestiary-3-items/2kSrspDPmUktGvBB.htm)|Tooth Tug|auto-trad|
|[2KYuE80DPoFkgFGT.htm](pathfinder-bestiary-3-items/2KYuE80DPoFkgFGT.htm)|Spiked Chain|auto-trad|
|[2LhHLzXgOoyNmS7p.htm](pathfinder-bestiary-3-items/2LhHLzXgOoyNmS7p.htm)|Darkvision|auto-trad|
|[2lp8axHkLWnfErrb.htm](pathfinder-bestiary-3-items/2lp8axHkLWnfErrb.htm)|Lower Spears!|auto-trad|
|[2mSXXYPRnq3gIcpj.htm](pathfinder-bestiary-3-items/2mSXXYPRnq3gIcpj.htm)|Grab|auto-trad|
|[2nEKM6ZrGJImSDZh.htm](pathfinder-bestiary-3-items/2nEKM6ZrGJImSDZh.htm)|Grab|auto-trad|
|[2NUCE7ph3hld0mgZ.htm](pathfinder-bestiary-3-items/2NUCE7ph3hld0mgZ.htm)|Darkvision|auto-trad|
|[2oNUku2hW5pSquMS.htm](pathfinder-bestiary-3-items/2oNUku2hW5pSquMS.htm)|Sap Mind|auto-trad|
|[2Ou5MlYuSq3KB2Rn.htm](pathfinder-bestiary-3-items/2Ou5MlYuSq3KB2Rn.htm)|At-Will Spells|auto-trad|
|[2OyUOHJ2HoOkk9ki.htm](pathfinder-bestiary-3-items/2OyUOHJ2HoOkk9ki.htm)|Light Blindness|auto-trad|
|[2pZ54UBlAD1KlL3E.htm](pathfinder-bestiary-3-items/2pZ54UBlAD1KlL3E.htm)|Flame|auto-trad|
|[2Qt36u2pXWig3wKR.htm](pathfinder-bestiary-3-items/2Qt36u2pXWig3wKR.htm)|Shell Block|auto-trad|
|[2rRPRh9T9ed91v7l.htm](pathfinder-bestiary-3-items/2rRPRh9T9ed91v7l.htm)|Cacophony|auto-trad|
|[2rT3ehZEN8ZxCrCM.htm](pathfinder-bestiary-3-items/2rT3ehZEN8ZxCrCM.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[2Rzw5Pms3YBG0FNZ.htm](pathfinder-bestiary-3-items/2Rzw5Pms3YBG0FNZ.htm)|Viper Swarm Venom|auto-trad|
|[2tYrB09YxVQESLhH.htm](pathfinder-bestiary-3-items/2tYrB09YxVQESLhH.htm)|Darkvision|auto-trad|
|[2vb8hf7ikquLEd9z.htm](pathfinder-bestiary-3-items/2vb8hf7ikquLEd9z.htm)|Aquatic Echolocation (Precise) 120 feet|auto-trad|
|[2Vj9sjJhfJuSzBn1.htm](pathfinder-bestiary-3-items/2Vj9sjJhfJuSzBn1.htm)|Grab|auto-trad|
|[2xn05F1PZDFa3Es4.htm](pathfinder-bestiary-3-items/2xn05F1PZDFa3Es4.htm)|Jaws|auto-trad|
|[2yxDzeKlqEJcTu27.htm](pathfinder-bestiary-3-items/2yxDzeKlqEJcTu27.htm)|Uncanny Pounce|auto-trad|
|[302qi3pb2HGvsM6E.htm](pathfinder-bestiary-3-items/302qi3pb2HGvsM6E.htm)|Shield Spikes|auto-trad|
|[333AoGB4COJFtWLx.htm](pathfinder-bestiary-3-items/333AoGB4COJFtWLx.htm)|Divine Innate Spells|auto-trad|
|[34s5p7VALREkL8Sh.htm](pathfinder-bestiary-3-items/34s5p7VALREkL8Sh.htm)|Greater Darkvision|auto-trad|
|[34yAb411T3yPWYAp.htm](pathfinder-bestiary-3-items/34yAb411T3yPWYAp.htm)|Darting Legs|auto-trad|
|[37C5tkSFoAf7HJd2.htm](pathfinder-bestiary-3-items/37C5tkSFoAf7HJd2.htm)|Fist|auto-trad|
|[3b4YyISk8knkUFuM.htm](pathfinder-bestiary-3-items/3b4YyISk8knkUFuM.htm)|Falchion|auto-trad|
|[3BM49h9dJSP6UcHN.htm](pathfinder-bestiary-3-items/3BM49h9dJSP6UcHN.htm)|Enraged Home (Piercing)|auto-trad|
|[3BpK7a2mX5UCDLmB.htm](pathfinder-bestiary-3-items/3BpK7a2mX5UCDLmB.htm)|Stick a Fork in It|auto-trad|
|[3bqSjZ8OFBahcmGk.htm](pathfinder-bestiary-3-items/3bqSjZ8OFBahcmGk.htm)|Darkvision|auto-trad|
|[3Epd1MoTcY2zIoJQ.htm](pathfinder-bestiary-3-items/3Epd1MoTcY2zIoJQ.htm)|Darkvision|auto-trad|
|[3eyaSs1hwilHo39q.htm](pathfinder-bestiary-3-items/3eyaSs1hwilHo39q.htm)|Frightful Presence|auto-trad|
|[3F0FPP20eSpY3u1Q.htm](pathfinder-bestiary-3-items/3F0FPP20eSpY3u1Q.htm)|Darkvision|auto-trad|
|[3FHM8X9p0VGx9jpe.htm](pathfinder-bestiary-3-items/3FHM8X9p0VGx9jpe.htm)|Rock|auto-trad|
|[3FtOenHRAd3nreJ2.htm](pathfinder-bestiary-3-items/3FtOenHRAd3nreJ2.htm)|Jaws|auto-trad|
|[3gpkwgTnI0kfqxxS.htm](pathfinder-bestiary-3-items/3gpkwgTnI0kfqxxS.htm)|Missile Volley|auto-trad|
|[3gYN9UrAQK8utoKH.htm](pathfinder-bestiary-3-items/3gYN9UrAQK8utoKH.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[3ISG9gfCmesBzeYt.htm](pathfinder-bestiary-3-items/3ISG9gfCmesBzeYt.htm)|Nosferatu Vulnerabilities|auto-trad|
|[3IZdVb2IEp23wALM.htm](pathfinder-bestiary-3-items/3IZdVb2IEp23wALM.htm)|Lifesense (Imprecise) 60 feet|auto-trad|
|[3jrrGptpZSe7SNzt.htm](pathfinder-bestiary-3-items/3jrrGptpZSe7SNzt.htm)|Constant Spells|auto-trad|
|[3keJLwwlQMlCUtDM.htm](pathfinder-bestiary-3-items/3keJLwwlQMlCUtDM.htm)|Frightful Presence|auto-trad|
|[3ltFeU7t0v6ZbQqL.htm](pathfinder-bestiary-3-items/3ltFeU7t0v6ZbQqL.htm)|Sudden Destruction|auto-trad|
|[3MP8S4relPgvIRf2.htm](pathfinder-bestiary-3-items/3MP8S4relPgvIRf2.htm)|Hat Toss|auto-trad|
|[3nAIvklO9ro6F88p.htm](pathfinder-bestiary-3-items/3nAIvklO9ro6F88p.htm)|Big Claw|auto-trad|
|[3OQIsBgxhUgao5MD.htm](pathfinder-bestiary-3-items/3OQIsBgxhUgao5MD.htm)|Nourishing Feast|auto-trad|
|[3p5BUgo3SACdAkGj.htm](pathfinder-bestiary-3-items/3p5BUgo3SACdAkGj.htm)|Leaping Pounce|auto-trad|
|[3r5kNijs14DNKRqh.htm](pathfinder-bestiary-3-items/3r5kNijs14DNKRqh.htm)|Ice Climb|auto-trad|
|[3SCP38lxFNM5HyPh.htm](pathfinder-bestiary-3-items/3SCP38lxFNM5HyPh.htm)|At-Will Spells|auto-trad|
|[3sFxSnysZjKDkccp.htm](pathfinder-bestiary-3-items/3sFxSnysZjKDkccp.htm)|Jaws|auto-trad|
|[3SJ0K1VlKcITVMvi.htm](pathfinder-bestiary-3-items/3SJ0K1VlKcITVMvi.htm)|Twisting Thrash|auto-trad|
|[3UNbqS2zBuQO6NaW.htm](pathfinder-bestiary-3-items/3UNbqS2zBuQO6NaW.htm)|Constant Spells|auto-trad|
|[3vdP680iRFJVfvwV.htm](pathfinder-bestiary-3-items/3vdP680iRFJVfvwV.htm)|Sudden Charge|auto-trad|
|[3VhaqjXhDYgbAlz2.htm](pathfinder-bestiary-3-items/3VhaqjXhDYgbAlz2.htm)|Smoke Vision|auto-trad|
|[3VjRp3T0kmoqaYH2.htm](pathfinder-bestiary-3-items/3VjRp3T0kmoqaYH2.htm)|Break Swell|auto-trad|
|[3VJSS90TtdqUONTz.htm](pathfinder-bestiary-3-items/3VJSS90TtdqUONTz.htm)|Constant Spells|auto-trad|
|[3wLOdV2RXEY5pnF5.htm](pathfinder-bestiary-3-items/3wLOdV2RXEY5pnF5.htm)|Mutilating Bite|auto-trad|
|[3Wnn8u8Vi6jbroLI.htm](pathfinder-bestiary-3-items/3Wnn8u8Vi6jbroLI.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[3xQc4jPhxkh2EbIF.htm](pathfinder-bestiary-3-items/3xQc4jPhxkh2EbIF.htm)|Claw|auto-trad|
|[3z3CSczhmYzfNCxB.htm](pathfinder-bestiary-3-items/3z3CSczhmYzfNCxB.htm)|Ward|auto-trad|
|[3ZfdDyLTXwOFpx2V.htm](pathfinder-bestiary-3-items/3ZfdDyLTXwOFpx2V.htm)|Attack of Opportunity|auto-trad|
|[3ZZyaQ6uTOW9lyxY.htm](pathfinder-bestiary-3-items/3ZZyaQ6uTOW9lyxY.htm)|Tail|auto-trad|
|[40daZI7TkA0gZGz3.htm](pathfinder-bestiary-3-items/40daZI7TkA0gZGz3.htm)|Divine Innate Spells|auto-trad|
|[40G9Tq0fb0vTHD9L.htm](pathfinder-bestiary-3-items/40G9Tq0fb0vTHD9L.htm)|Negative Healing|auto-trad|
|[41sWFM6OF3ytKfyk.htm](pathfinder-bestiary-3-items/41sWFM6OF3ytKfyk.htm)|Drink Abundance|auto-trad|
|[425lZ6UBU1Pd31FP.htm](pathfinder-bestiary-3-items/425lZ6UBU1Pd31FP.htm)|Change Shape|auto-trad|
|[42qxpi5EYfZ17E0X.htm](pathfinder-bestiary-3-items/42qxpi5EYfZ17E0X.htm)|Darkvision|auto-trad|
|[43AUXc5WoGyh1pKT.htm](pathfinder-bestiary-3-items/43AUXc5WoGyh1pKT.htm)|Drench|auto-trad|
|[43VfyQa8GnQbb1JR.htm](pathfinder-bestiary-3-items/43VfyQa8GnQbb1JR.htm)|Team Attack|auto-trad|
|[45zsYQgswZpSa14E.htm](pathfinder-bestiary-3-items/45zsYQgswZpSa14E.htm)|Resonance|auto-trad|
|[46fsaAZvo5Pu9usU.htm](pathfinder-bestiary-3-items/46fsaAZvo5Pu9usU.htm)|Troop Movement|auto-trad|
|[46M4PewZUpmrWn09.htm](pathfinder-bestiary-3-items/46M4PewZUpmrWn09.htm)|Longsword|auto-trad|
|[48uJqrNPzQyywSb2.htm](pathfinder-bestiary-3-items/48uJqrNPzQyywSb2.htm)|Claw|auto-trad|
|[4923v7qhGbPOnzkA.htm](pathfinder-bestiary-3-items/4923v7qhGbPOnzkA.htm)|Xiuh Couatl Venom|auto-trad|
|[49AEhw1auYxOww4q.htm](pathfinder-bestiary-3-items/49AEhw1auYxOww4q.htm)|Feast|auto-trad|
|[4ajY6AeHzOgwoB3H.htm](pathfinder-bestiary-3-items/4ajY6AeHzOgwoB3H.htm)|Slip|auto-trad|
|[4bIGF4lDjFiCWkiL.htm](pathfinder-bestiary-3-items/4bIGF4lDjFiCWkiL.htm)|Strafing Chomp|auto-trad|
|[4csEUCYiZOJmhWRo.htm](pathfinder-bestiary-3-items/4csEUCYiZOJmhWRo.htm)|Improved Knockdown|auto-trad|
|[4cx1d9efogHuWe26.htm](pathfinder-bestiary-3-items/4cx1d9efogHuWe26.htm)|Wreck|auto-trad|
|[4DUpOE1nhjDUloIt.htm](pathfinder-bestiary-3-items/4DUpOE1nhjDUloIt.htm)|Attack of Opportunity|auto-trad|
|[4DxK55znXhBy05Je.htm](pathfinder-bestiary-3-items/4DxK55znXhBy05Je.htm)|Primal Innate Spells|auto-trad|
|[4FIPIf24n9DntnSj.htm](pathfinder-bestiary-3-items/4FIPIf24n9DntnSj.htm)|Rend|auto-trad|
|[4FtT2fJlh9Q0McYv.htm](pathfinder-bestiary-3-items/4FtT2fJlh9Q0McYv.htm)|Tackle|auto-trad|
|[4gjSVNhiTon9n0Xd.htm](pathfinder-bestiary-3-items/4gjSVNhiTon9n0Xd.htm)|Swarming Strikes|auto-trad|
|[4j8tGzWuHcBuRSsZ.htm](pathfinder-bestiary-3-items/4j8tGzWuHcBuRSsZ.htm)|Frightful Presence|auto-trad|
|[4JqTaWMRh2PWQLJX.htm](pathfinder-bestiary-3-items/4JqTaWMRh2PWQLJX.htm)|Foot|auto-trad|
|[4JSskrFw05x7ggm1.htm](pathfinder-bestiary-3-items/4JSskrFw05x7ggm1.htm)|Kick Back|auto-trad|
|[4k6Uvj17HBcW7ylM.htm](pathfinder-bestiary-3-items/4k6Uvj17HBcW7ylM.htm)|Energy Conversion|auto-trad|
|[4KGlzeHqmtsJnLv9.htm](pathfinder-bestiary-3-items/4KGlzeHqmtsJnLv9.htm)|Spectral Hand|auto-trad|
|[4nSekhH06OdsuVpK.htm](pathfinder-bestiary-3-items/4nSekhH06OdsuVpK.htm)|Dominate|auto-trad|
|[4q3DAdCBSdwFd8Ge.htm](pathfinder-bestiary-3-items/4q3DAdCBSdwFd8Ge.htm)|Claw|auto-trad|
|[4Q80XDMMBCj2Mihv.htm](pathfinder-bestiary-3-items/4Q80XDMMBCj2Mihv.htm)|Low-Light Vision|auto-trad|
|[4QajQWtHxBUlKsgo.htm](pathfinder-bestiary-3-items/4QajQWtHxBUlKsgo.htm)|Reconstitution|auto-trad|
|[4QFJBrjb8mR8VE6I.htm](pathfinder-bestiary-3-items/4QFJBrjb8mR8VE6I.htm)|Stormcalling|auto-trad|
|[4QS3vuiF6NBVHzYN.htm](pathfinder-bestiary-3-items/4QS3vuiF6NBVHzYN.htm)|Blood Wake|auto-trad|
|[4r3fZEvFKQpzQMJ0.htm](pathfinder-bestiary-3-items/4r3fZEvFKQpzQMJ0.htm)|Dagger|auto-trad|
|[4rjTham5fRzKLGA2.htm](pathfinder-bestiary-3-items/4rjTham5fRzKLGA2.htm)|Low-Light Vision|auto-trad|
|[4SUjs1Fnnil2m2Qt.htm](pathfinder-bestiary-3-items/4SUjs1Fnnil2m2Qt.htm)|Cursed Gaze|auto-trad|
|[4t55o9zeccODBur0.htm](pathfinder-bestiary-3-items/4t55o9zeccODBur0.htm)|Stitch Skin|auto-trad|
|[4t6lOiLhCYAlFpnf.htm](pathfinder-bestiary-3-items/4t6lOiLhCYAlFpnf.htm)|Greatsword|auto-trad|
|[4TqIsk44LcZm61Ws.htm](pathfinder-bestiary-3-items/4TqIsk44LcZm61Ws.htm)|Rend|auto-trad|
|[4vjdVbvQ2cPvkxA1.htm](pathfinder-bestiary-3-items/4vjdVbvQ2cPvkxA1.htm)|Seek Quarry|auto-trad|
|[4W60RWwqP4LPF4Ve.htm](pathfinder-bestiary-3-items/4W60RWwqP4LPF4Ve.htm)|Primal Innate Spells|auto-trad|
|[4w6cni5pDSonPMZW.htm](pathfinder-bestiary-3-items/4w6cni5pDSonPMZW.htm)|Shortsword|auto-trad|
|[4W98qLPXYtwytrBj.htm](pathfinder-bestiary-3-items/4W98qLPXYtwytrBj.htm)|Low-Light Vision|auto-trad|
|[4wEJM0uffDwyN4QS.htm](pathfinder-bestiary-3-items/4wEJM0uffDwyN4QS.htm)|Coiling Frenzy|auto-trad|
|[4x1dSgcNIbL7YtTa.htm](pathfinder-bestiary-3-items/4x1dSgcNIbL7YtTa.htm)|Frightening Howl|auto-trad|
|[4XdLVNceTaTeWXxs.htm](pathfinder-bestiary-3-items/4XdLVNceTaTeWXxs.htm)|Weak Acid|auto-trad|
|[4y0Im0eAbj4YFOII.htm](pathfinder-bestiary-3-items/4y0Im0eAbj4YFOII.htm)|Troop Movement|auto-trad|
|[4yxB753affbJoFUo.htm](pathfinder-bestiary-3-items/4yxB753affbJoFUo.htm)|Constant Spells|auto-trad|
|[4ZK6UkKCERwU9jVd.htm](pathfinder-bestiary-3-items/4ZK6UkKCERwU9jVd.htm)|Primal Innate Spells|auto-trad|
|[50TREly1W5yrRm7R.htm](pathfinder-bestiary-3-items/50TREly1W5yrRm7R.htm)|Thoughtsense (Imprecise) 60 feet|auto-trad|
|[51mhuepTTAtKvDL3.htm](pathfinder-bestiary-3-items/51mhuepTTAtKvDL3.htm)|Diseased Pustules|auto-trad|
|[53ZHZQrxzfyMUm3s.htm](pathfinder-bestiary-3-items/53ZHZQrxzfyMUm3s.htm)|Occult Innate Spells|auto-trad|
|[54ARy8tIUzIr76GH.htm](pathfinder-bestiary-3-items/54ARy8tIUzIr76GH.htm)|Fed by Metal|auto-trad|
|[55pDhEb6m82JCtfR.htm](pathfinder-bestiary-3-items/55pDhEb6m82JCtfR.htm)|Abrogation of Consequences|auto-trad|
|[56iLpxxqpM1gYATc.htm](pathfinder-bestiary-3-items/56iLpxxqpM1gYATc.htm)|Death Gaze|auto-trad|
|[58QNGdCWXAUWVAti.htm](pathfinder-bestiary-3-items/58QNGdCWXAUWVAti.htm)|Darkvision|auto-trad|
|[5AumaQTWt3WVkDej.htm](pathfinder-bestiary-3-items/5AumaQTWt3WVkDej.htm)|At-Will Spells|auto-trad|
|[5auWJrdx79CUpWa2.htm](pathfinder-bestiary-3-items/5auWJrdx79CUpWa2.htm)|Attack of Opportunity (Tail Only)|auto-trad|
|[5B3uRzeJSlXUyJPR.htm](pathfinder-bestiary-3-items/5B3uRzeJSlXUyJPR.htm)|Darkvision|auto-trad|
|[5d0jyVv6vjBbVxdc.htm](pathfinder-bestiary-3-items/5d0jyVv6vjBbVxdc.htm)|Sure Stride|auto-trad|
|[5e9nAAnEPwtzAoiR.htm](pathfinder-bestiary-3-items/5e9nAAnEPwtzAoiR.htm)|Darkvision|auto-trad|
|[5eqtzIqWpDscyCIh.htm](pathfinder-bestiary-3-items/5eqtzIqWpDscyCIh.htm)|Breath Weapon|auto-trad|
|[5FoNMqcRkDgmHGWx.htm](pathfinder-bestiary-3-items/5FoNMqcRkDgmHGWx.htm)|Darkvision|auto-trad|
|[5FqnH65aSIz1yYu4.htm](pathfinder-bestiary-3-items/5FqnH65aSIz1yYu4.htm)|Share Pain|auto-trad|
|[5gfJon4GXyqVHTz0.htm](pathfinder-bestiary-3-items/5gfJon4GXyqVHTz0.htm)|Negative Healing|auto-trad|
|[5gY8gk28J2srHl1t.htm](pathfinder-bestiary-3-items/5gY8gk28J2srHl1t.htm)|Skip Between|auto-trad|
|[5h2xMj0CQYOauzxy.htm](pathfinder-bestiary-3-items/5h2xMj0CQYOauzxy.htm)|Monk Ki Spells|auto-trad|
|[5hGIIhdbBibMGMJ3.htm](pathfinder-bestiary-3-items/5hGIIhdbBibMGMJ3.htm)|Cat's Grace|auto-trad|
|[5hNT46xUSUSqA2x1.htm](pathfinder-bestiary-3-items/5hNT46xUSUSqA2x1.htm)|Musk|auto-trad|
|[5iBIN1PlI1tshlnI.htm](pathfinder-bestiary-3-items/5iBIN1PlI1tshlnI.htm)|Knockdown|auto-trad|
|[5iH51KTA8CfAOS08.htm](pathfinder-bestiary-3-items/5iH51KTA8CfAOS08.htm)|Light Wisp|auto-trad|
|[5IHuVnedjxI2lxzF.htm](pathfinder-bestiary-3-items/5IHuVnedjxI2lxzF.htm)|Distracting Gaze|auto-trad|
|[5iw21DmwOYFPkVbC.htm](pathfinder-bestiary-3-items/5iw21DmwOYFPkVbC.htm)|Prickly Burst|auto-trad|
|[5j0da1f0NNKtJ9uB.htm](pathfinder-bestiary-3-items/5j0da1f0NNKtJ9uB.htm)|Pulse of Rage|auto-trad|
|[5JFwqj1O27XBnI7T.htm](pathfinder-bestiary-3-items/5JFwqj1O27XBnI7T.htm)|Primal Innate Spells|auto-trad|
|[5JGaGCjUurVrjoyX.htm](pathfinder-bestiary-3-items/5JGaGCjUurVrjoyX.htm)|Unfathomable Infinity|auto-trad|
|[5JjMeb6t3MgQy8HQ.htm](pathfinder-bestiary-3-items/5JjMeb6t3MgQy8HQ.htm)|Rock|auto-trad|
|[5mZBnyI8jN8hRDpC.htm](pathfinder-bestiary-3-items/5mZBnyI8jN8hRDpC.htm)|Barbed Net|auto-trad|
|[5NPHDSBtMnU44c52.htm](pathfinder-bestiary-3-items/5NPHDSBtMnU44c52.htm)|Rally|auto-trad|
|[5OeW9Mm1qaPRCaYt.htm](pathfinder-bestiary-3-items/5OeW9Mm1qaPRCaYt.htm)|+2 to All Saves vs. Poison|auto-trad|
|[5OmSIpE30Qir6OKb.htm](pathfinder-bestiary-3-items/5OmSIpE30Qir6OKb.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[5OPP2h29MKUEtig6.htm](pathfinder-bestiary-3-items/5OPP2h29MKUEtig6.htm)|Ectoplasmic Secretions|auto-trad|
|[5OTunhc4CEJ5XrAr.htm](pathfinder-bestiary-3-items/5OTunhc4CEJ5XrAr.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[5pRHBAOx7kPRJDBR.htm](pathfinder-bestiary-3-items/5pRHBAOx7kPRJDBR.htm)|Negative Healing|auto-trad|
|[5PRYxLiEoiOScKcc.htm](pathfinder-bestiary-3-items/5PRYxLiEoiOScKcc.htm)|Swift Steps|auto-trad|
|[5qFwYj9ry9CVIKY7.htm](pathfinder-bestiary-3-items/5qFwYj9ry9CVIKY7.htm)|Telepathy 60 feet|auto-trad|
|[5rmiy80ibEEvlvIL.htm](pathfinder-bestiary-3-items/5rmiy80ibEEvlvIL.htm)|Consecration Vulnerability|auto-trad|
|[5RtC2ASqDYa1fPfS.htm](pathfinder-bestiary-3-items/5RtC2ASqDYa1fPfS.htm)|Constant Spells|auto-trad|
|[5SsaserwdJ4my9cj.htm](pathfinder-bestiary-3-items/5SsaserwdJ4my9cj.htm)|Darkvision|auto-trad|
|[5SZG9rr8f11whg0x.htm](pathfinder-bestiary-3-items/5SZG9rr8f11whg0x.htm)|Speak with Bats|auto-trad|
|[5TY3CK7BZ3ZrKA1C.htm](pathfinder-bestiary-3-items/5TY3CK7BZ3ZrKA1C.htm)|Occult Prepared Spells|auto-trad|
|[5u8Fh5gvFK0DreBo.htm](pathfinder-bestiary-3-items/5u8Fh5gvFK0DreBo.htm)|Handspring Kick|auto-trad|
|[5VVtnGIh8V9iyhaS.htm](pathfinder-bestiary-3-items/5VVtnGIh8V9iyhaS.htm)|Sense Apostate 500 feet|auto-trad|
|[5WhjDHH8kb9e6f8l.htm](pathfinder-bestiary-3-items/5WhjDHH8kb9e6f8l.htm)|Shadowplay|auto-trad|
|[5wlVfCVGmDq3fRKw.htm](pathfinder-bestiary-3-items/5wlVfCVGmDq3fRKw.htm)|Malodorous Smoke|auto-trad|
|[5XfUKnzADDIB9IN0.htm](pathfinder-bestiary-3-items/5XfUKnzADDIB9IN0.htm)|Create Spawn|auto-trad|
|[5YGPeCSsTq71DxAb.htm](pathfinder-bestiary-3-items/5YGPeCSsTq71DxAb.htm)|Souleater|auto-trad|
|[5YK8hJa0XyWYTPW1.htm](pathfinder-bestiary-3-items/5YK8hJa0XyWYTPW1.htm)|Darkvision|auto-trad|
|[5zASRad3bigyGHMo.htm](pathfinder-bestiary-3-items/5zASRad3bigyGHMo.htm)|Coiling Frenzy|auto-trad|
|[5ZYqE0wvrRymU2Bg.htm](pathfinder-bestiary-3-items/5ZYqE0wvrRymU2Bg.htm)|Darkvision|auto-trad|
|[60CCB5Edf3n6TRxV.htm](pathfinder-bestiary-3-items/60CCB5Edf3n6TRxV.htm)|Tongue|auto-trad|
|[60deOo2mPqEopbvy.htm](pathfinder-bestiary-3-items/60deOo2mPqEopbvy.htm)|Arcane Innate Spells|auto-trad|
|[61RVU0BzmvnLofFj.htm](pathfinder-bestiary-3-items/61RVU0BzmvnLofFj.htm)|Absorb Evocation|auto-trad|
|[62PNJhmFdbtT0TrS.htm](pathfinder-bestiary-3-items/62PNJhmFdbtT0TrS.htm)|Voice Imitation|auto-trad|
|[62tSPn9Q3DkS4DA9.htm](pathfinder-bestiary-3-items/62tSPn9Q3DkS4DA9.htm)|Sandwalking|auto-trad|
|[64CXDHwnDpHKKZpp.htm](pathfinder-bestiary-3-items/64CXDHwnDpHKKZpp.htm)|At-Will Spells|auto-trad|
|[64fFy6rN4KqZRePB.htm](pathfinder-bestiary-3-items/64fFy6rN4KqZRePB.htm)|Feign Death|auto-trad|
|[66fDZvUWClHrgbKL.htm](pathfinder-bestiary-3-items/66fDZvUWClHrgbKL.htm)|Breath Weapon|auto-trad|
|[66GzinTfrKKU8xfg.htm](pathfinder-bestiary-3-items/66GzinTfrKKU8xfg.htm)|Darkvision|auto-trad|
|[66q0s4rjQzYyXMxq.htm](pathfinder-bestiary-3-items/66q0s4rjQzYyXMxq.htm)|Snake Search|auto-trad|
|[69BcgWueS6EljWeK.htm](pathfinder-bestiary-3-items/69BcgWueS6EljWeK.htm)|Benthic Wave|auto-trad|
|[6cqbtL8P73BAKLlb.htm](pathfinder-bestiary-3-items/6cqbtL8P73BAKLlb.htm)|Void Weapon|auto-trad|
|[6CxEvaX1CFDQ7rOR.htm](pathfinder-bestiary-3-items/6CxEvaX1CFDQ7rOR.htm)|Deep Breath|auto-trad|
|[6d6cIadAucEYZpne.htm](pathfinder-bestiary-3-items/6d6cIadAucEYZpne.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[6D8hv3P04IeQWepE.htm](pathfinder-bestiary-3-items/6D8hv3P04IeQWepE.htm)|Alien Vestment|auto-trad|
|[6DJ5UGod5sVQIgQ8.htm](pathfinder-bestiary-3-items/6DJ5UGod5sVQIgQ8.htm)|Sunset Ribbon|auto-trad|
|[6fc7mDScSYrAh4aK.htm](pathfinder-bestiary-3-items/6fc7mDScSYrAh4aK.htm)|Broom|auto-trad|
|[6FLKit13w08Xyhqy.htm](pathfinder-bestiary-3-items/6FLKit13w08Xyhqy.htm)|Bond with Ward|auto-trad|
|[6hgXjCxuZTwCxmFa.htm](pathfinder-bestiary-3-items/6hgXjCxuZTwCxmFa.htm)|Darkvision|auto-trad|
|[6HIVko7t9eQdURP8.htm](pathfinder-bestiary-3-items/6HIVko7t9eQdURP8.htm)|Tendril|auto-trad|
|[6jf1IhNb2PBN2aXV.htm](pathfinder-bestiary-3-items/6jf1IhNb2PBN2aXV.htm)|Attack of Opportunity (Special)|auto-trad|
|[6Jhrih75LDb7Tovl.htm](pathfinder-bestiary-3-items/6Jhrih75LDb7Tovl.htm)|Warping Ray|auto-trad|
|[6jmiViG6aADCKgpM.htm](pathfinder-bestiary-3-items/6jmiViG6aADCKgpM.htm)|Wing|auto-trad|
|[6jXWp7OM7AukkRT3.htm](pathfinder-bestiary-3-items/6jXWp7OM7AukkRT3.htm)|Bite|auto-trad|
|[6kZm8XnYELAQFppU.htm](pathfinder-bestiary-3-items/6kZm8XnYELAQFppU.htm)|Bat Empathy|auto-trad|
|[6LlX67R81S5J80Ws.htm](pathfinder-bestiary-3-items/6LlX67R81S5J80Ws.htm)|All-Around Vision|auto-trad|
|[6LN4eVSW1LF6YGqK.htm](pathfinder-bestiary-3-items/6LN4eVSW1LF6YGqK.htm)|Scent (Imprecise) 100 feet|auto-trad|
|[6ludWZkd4jUcg8tv.htm](pathfinder-bestiary-3-items/6ludWZkd4jUcg8tv.htm)|Telepathy 100 feet|auto-trad|
|[6LVQDNRocR70kvVD.htm](pathfinder-bestiary-3-items/6LVQDNRocR70kvVD.htm)|Darkvision|auto-trad|
|[6m81W89YrbQ1gHjG.htm](pathfinder-bestiary-3-items/6m81W89YrbQ1gHjG.htm)|Elongate Tongue|auto-trad|
|[6meS6jDvt73ZAOZV.htm](pathfinder-bestiary-3-items/6meS6jDvt73ZAOZV.htm)|Primal Innate Spells|auto-trad|
|[6mHmfXwMk0MX37HC.htm](pathfinder-bestiary-3-items/6mHmfXwMk0MX37HC.htm)|Greater Constrict|auto-trad|
|[6OrGVyunvCJATyUD.htm](pathfinder-bestiary-3-items/6OrGVyunvCJATyUD.htm)|Self-Absorbed|auto-trad|
|[6OT1Cj34QvB0zv3Q.htm](pathfinder-bestiary-3-items/6OT1Cj34QvB0zv3Q.htm)|Tail|auto-trad|
|[6PLzHEkZC8lh9I6a.htm](pathfinder-bestiary-3-items/6PLzHEkZC8lh9I6a.htm)|At-Will Spells|auto-trad|
|[6PZisICkQg9iEoQs.htm](pathfinder-bestiary-3-items/6PZisICkQg9iEoQs.htm)|Occult Spontaneous Spells|auto-trad|
|[6qxKmQ1NJ2m3caTx.htm](pathfinder-bestiary-3-items/6qxKmQ1NJ2m3caTx.htm)|No Breath|auto-trad|
|[6QzeJqibXMpIAXDj.htm](pathfinder-bestiary-3-items/6QzeJqibXMpIAXDj.htm)|Low-Light Vision|auto-trad|
|[6r3WJI32FjeNNZlp.htm](pathfinder-bestiary-3-items/6r3WJI32FjeNNZlp.htm)|Echolocation 20 feet|auto-trad|
|[6SDloGsttpr7ZAJq.htm](pathfinder-bestiary-3-items/6SDloGsttpr7ZAJq.htm)|Hatred of Beauty|auto-trad|
|[6ThLzzOnr19ZeVm4.htm](pathfinder-bestiary-3-items/6ThLzzOnr19ZeVm4.htm)|Darkvision|auto-trad|
|[6tZvLirOPTqzo6of.htm](pathfinder-bestiary-3-items/6tZvLirOPTqzo6of.htm)|Occult Innate Spells|auto-trad|
|[6u4WiGSgJcSBMIVQ.htm](pathfinder-bestiary-3-items/6u4WiGSgJcSBMIVQ.htm)|Universal Language|auto-trad|
|[6U6A8ryVDLXX3rmv.htm](pathfinder-bestiary-3-items/6U6A8ryVDLXX3rmv.htm)|+1 Status to All Saves vs. Positive|auto-trad|
|[6UlwNI4X3CPg9JEG.htm](pathfinder-bestiary-3-items/6UlwNI4X3CPg9JEG.htm)|Occult Innate Spells|auto-trad|
|[6wmw4xZcc4sGgnNR.htm](pathfinder-bestiary-3-items/6wmw4xZcc4sGgnNR.htm)|Carrion Fever|auto-trad|
|[6WZJcN2ygA1eIjQE.htm](pathfinder-bestiary-3-items/6WZJcN2ygA1eIjQE.htm)|Arcane Innate Spells|auto-trad|
|[6XahuYsNr6LHrkAs.htm](pathfinder-bestiary-3-items/6XahuYsNr6LHrkAs.htm)|Sudden Dive|auto-trad|
|[6xVdg4wmRT0c4mXW.htm](pathfinder-bestiary-3-items/6xVdg4wmRT0c4mXW.htm)|Sugar Rush|auto-trad|
|[6yq9O6WHEsvMhrT8.htm](pathfinder-bestiary-3-items/6yq9O6WHEsvMhrT8.htm)|Change Shape|auto-trad|
|[6ZdU1MvTTYu6fH0E.htm](pathfinder-bestiary-3-items/6ZdU1MvTTYu6fH0E.htm)|Occult Innate Spells|auto-trad|
|[6zHIzorHFZDIXyyR.htm](pathfinder-bestiary-3-items/6zHIzorHFZDIXyyR.htm)|Coiling Frenzy|auto-trad|
|[6ZOExAAHG9cE1G7V.htm](pathfinder-bestiary-3-items/6ZOExAAHG9cE1G7V.htm)|Jaws|auto-trad|
|[7101NEoU5qf7eU5R.htm](pathfinder-bestiary-3-items/7101NEoU5qf7eU5R.htm)|Greater Constrict|auto-trad|
|[71GJwFaE95Ae7SXJ.htm](pathfinder-bestiary-3-items/71GJwFaE95Ae7SXJ.htm)|Mage Bond|auto-trad|
|[71vyIq4WtorjpuSI.htm](pathfinder-bestiary-3-items/71vyIq4WtorjpuSI.htm)|Pseudopod|auto-trad|
|[71ZISy5sK6KLCTyg.htm](pathfinder-bestiary-3-items/71ZISy5sK6KLCTyg.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[72K68WtjLjkNXxWr.htm](pathfinder-bestiary-3-items/72K68WtjLjkNXxWr.htm)|Invigorating Feast|auto-trad|
|[73Q1gfTlvsbrJIOV.htm](pathfinder-bestiary-3-items/73Q1gfTlvsbrJIOV.htm)|Occult Innate Spells|auto-trad|
|[74cvbuceLeJciQVI.htm](pathfinder-bestiary-3-items/74cvbuceLeJciQVI.htm)|Flesh Grafting|auto-trad|
|[74QNWqV1T1TsPKzT.htm](pathfinder-bestiary-3-items/74QNWqV1T1TsPKzT.htm)|Rend|auto-trad|
|[75r7SZRmZYqQiKfH.htm](pathfinder-bestiary-3-items/75r7SZRmZYqQiKfH.htm)|Occult Spontaneous Spells|auto-trad|
|[77tPlIEwFc6nptCm.htm](pathfinder-bestiary-3-items/77tPlIEwFc6nptCm.htm)|Wildfire Storm|auto-trad|
|[78eiTtsU1ddUvLm1.htm](pathfinder-bestiary-3-items/78eiTtsU1ddUvLm1.htm)|Dromophobia|auto-trad|
|[7ASsgG1v1611ihJD.htm](pathfinder-bestiary-3-items/7ASsgG1v1611ihJD.htm)|Bile|auto-trad|
|[7bHEWVYytDW7lLet.htm](pathfinder-bestiary-3-items/7bHEWVYytDW7lLet.htm)|At-Will Spells|auto-trad|
|[7bvs9Qjgwf9G0s4w.htm](pathfinder-bestiary-3-items/7bvs9Qjgwf9G0s4w.htm)|Darkvision|auto-trad|
|[7c1InSV9VXV4cB8Z.htm](pathfinder-bestiary-3-items/7c1InSV9VXV4cB8Z.htm)|Grasping Tail|auto-trad|
|[7cb3300PlOTebHcM.htm](pathfinder-bestiary-3-items/7cb3300PlOTebHcM.htm)|Troop Spellcasting|auto-trad|
|[7cU2Ki9ZUte4XeOY.htm](pathfinder-bestiary-3-items/7cU2Ki9ZUte4XeOY.htm)|Claw|auto-trad|
|[7F01r7qfkds4bfEo.htm](pathfinder-bestiary-3-items/7F01r7qfkds4bfEo.htm)|Claw|auto-trad|
|[7fBfykPx1yG8CYGz.htm](pathfinder-bestiary-3-items/7fBfykPx1yG8CYGz.htm)|Atrophic Plague|auto-trad|
|[7FCmN4UL4psU0tDh.htm](pathfinder-bestiary-3-items/7FCmN4UL4psU0tDh.htm)|Occult Innate Spells|auto-trad|
|[7FPtqs0896vW7FwI.htm](pathfinder-bestiary-3-items/7FPtqs0896vW7FwI.htm)|Titanic Charge|auto-trad|
|[7GA62FWHgOMnOoir.htm](pathfinder-bestiary-3-items/7GA62FWHgOMnOoir.htm)|Pounce|auto-trad|
|[7H5EOw27GyoL0zDR.htm](pathfinder-bestiary-3-items/7H5EOw27GyoL0zDR.htm)|Nauseating Slap|auto-trad|
|[7hmsuLy1J8DJxM6F.htm](pathfinder-bestiary-3-items/7hmsuLy1J8DJxM6F.htm)|Fast Healing 15|auto-trad|
|[7i9LTQqlhq2Hhy7j.htm](pathfinder-bestiary-3-items/7i9LTQqlhq2Hhy7j.htm)|Constant Spells|auto-trad|
|[7Ja558AxHimR0DNe.htm](pathfinder-bestiary-3-items/7Ja558AxHimR0DNe.htm)|No Breath|auto-trad|
|[7JznoiaHsY4zQn0z.htm](pathfinder-bestiary-3-items/7JznoiaHsY4zQn0z.htm)|Telepathy 100 feet|auto-trad|
|[7KrbqcGEJ1GaWuQc.htm](pathfinder-bestiary-3-items/7KrbqcGEJ1GaWuQc.htm)|Attack of Opportunity (Special)|auto-trad|
|[7lG8jBDPOKE6KGt2.htm](pathfinder-bestiary-3-items/7lG8jBDPOKE6KGt2.htm)|Green Caress|auto-trad|
|[7M6TofwZ6Yvd0iw4.htm](pathfinder-bestiary-3-items/7M6TofwZ6Yvd0iw4.htm)|Change Shape|auto-trad|
|[7NvFdNeBt7AssuX2.htm](pathfinder-bestiary-3-items/7NvFdNeBt7AssuX2.htm)|Faithful Weapon|auto-trad|
|[7O5f2VrYGu8KYH37.htm](pathfinder-bestiary-3-items/7O5f2VrYGu8KYH37.htm)|Emit Spores|auto-trad|
|[7OHhyz8S9jaICp6G.htm](pathfinder-bestiary-3-items/7OHhyz8S9jaICp6G.htm)|Root In Place|auto-trad|
|[7oiohGkxeOfZceVR.htm](pathfinder-bestiary-3-items/7oiohGkxeOfZceVR.htm)|Low-Light Vision|auto-trad|
|[7q0sH9B8Zn1uCrNj.htm](pathfinder-bestiary-3-items/7q0sH9B8Zn1uCrNj.htm)|Cleric Domain Spells|auto-trad|
|[7qBjkooMOFnRW7Uy.htm](pathfinder-bestiary-3-items/7qBjkooMOFnRW7Uy.htm)|Jaws|auto-trad|
|[7SJO477OusJy7wpB.htm](pathfinder-bestiary-3-items/7SJO477OusJy7wpB.htm)|Jaws|auto-trad|
|[7SyelFSQ8Pix3kVJ.htm](pathfinder-bestiary-3-items/7SyelFSQ8Pix3kVJ.htm)|Primal Innate Spells|auto-trad|
|[7u9nInfiCammnrKi.htm](pathfinder-bestiary-3-items/7u9nInfiCammnrKi.htm)|Jaws|auto-trad|
|[7UK0F7yfKDuLMdqq.htm](pathfinder-bestiary-3-items/7UK0F7yfKDuLMdqq.htm)|Coiling Frenzy|auto-trad|
|[7ulY5fkbW89xf55O.htm](pathfinder-bestiary-3-items/7ulY5fkbW89xf55O.htm)|Pest Haven|auto-trad|
|[7V9CRphWnj5Wbphk.htm](pathfinder-bestiary-3-items/7V9CRphWnj5Wbphk.htm)|Constant Spells|auto-trad|
|[7VXwGJWTdkaP6Ch0.htm](pathfinder-bestiary-3-items/7VXwGJWTdkaP6Ch0.htm)|At-Will Spells|auto-trad|
|[7wr42RamnztEcqpc.htm](pathfinder-bestiary-3-items/7wr42RamnztEcqpc.htm)|Send Beyond|auto-trad|
|[7WVvb1W4IfVQZzSv.htm](pathfinder-bestiary-3-items/7WVvb1W4IfVQZzSv.htm)|Malevolent Possession|auto-trad|
|[7wxTRall7vLvrVdI.htm](pathfinder-bestiary-3-items/7wxTRall7vLvrVdI.htm)|Sharp Riposte|auto-trad|
|[7xqB2gVMxUHI1QMC.htm](pathfinder-bestiary-3-items/7xqB2gVMxUHI1QMC.htm)|Skeleton Crew|auto-trad|
|[847XuFlQNdsrsK3G.htm](pathfinder-bestiary-3-items/847XuFlQNdsrsK3G.htm)|Roll Up|auto-trad|
|[85a0vfvP7BEWaC7o.htm](pathfinder-bestiary-3-items/85a0vfvP7BEWaC7o.htm)|At-Will Spells|auto-trad|
|[85CNz1N6H7AEa4kU.htm](pathfinder-bestiary-3-items/85CNz1N6H7AEa4kU.htm)|Scimitar|auto-trad|
|[87If4txGT9U4ihPf.htm](pathfinder-bestiary-3-items/87If4txGT9U4ihPf.htm)|Scrabbling Swarm|auto-trad|
|[88a6l4wOhZKuqey0.htm](pathfinder-bestiary-3-items/88a6l4wOhZKuqey0.htm)|Flight Commander of Dis|auto-trad|
|[88wfIYYh2caypx0G.htm](pathfinder-bestiary-3-items/88wfIYYh2caypx0G.htm)|Wavesense (Imprecise) 100 feet|auto-trad|
|[89pRruq95QsvuZYX.htm](pathfinder-bestiary-3-items/89pRruq95QsvuZYX.htm)|Sensory Fever|auto-trad|
|[89ujLImiUz00Zomc.htm](pathfinder-bestiary-3-items/89ujLImiUz00Zomc.htm)|Lance Charge|auto-trad|
|[8AK5AzyX03GXrikY.htm](pathfinder-bestiary-3-items/8AK5AzyX03GXrikY.htm)|Fist|auto-trad|
|[8Amc9UddnS7QBYOL.htm](pathfinder-bestiary-3-items/8Amc9UddnS7QBYOL.htm)|Cryptomnesia|auto-trad|
|[8B31oTS6yoGOkPve.htm](pathfinder-bestiary-3-items/8B31oTS6yoGOkPve.htm)|Flurry of Blows|auto-trad|
|[8BmcD1iEjqhQRthD.htm](pathfinder-bestiary-3-items/8BmcD1iEjqhQRthD.htm)|Tail|auto-trad|
|[8cr5UQhOU7Fs4x6B.htm](pathfinder-bestiary-3-items/8cr5UQhOU7Fs4x6B.htm)|Scent (Imprecise) 100 feet|auto-trad|
|[8cXuUewtlaWaE5MT.htm](pathfinder-bestiary-3-items/8cXuUewtlaWaE5MT.htm)|Tendril|auto-trad|
|[8e4jbz96UsZDTcka.htm](pathfinder-bestiary-3-items/8e4jbz96UsZDTcka.htm)|Retaliate|auto-trad|
|[8EzoTBKzzz2kcPsk.htm](pathfinder-bestiary-3-items/8EzoTBKzzz2kcPsk.htm)|Darkvision|auto-trad|
|[8FgEIFUzEgLTHjHc.htm](pathfinder-bestiary-3-items/8FgEIFUzEgLTHjHc.htm)|Fearsense (Precise) 60 feet|auto-trad|
|[8GbsrAKrtoDroA9Y.htm](pathfinder-bestiary-3-items/8GbsrAKrtoDroA9Y.htm)|Snout|auto-trad|
|[8GmTiza6VxQxjlFA.htm](pathfinder-bestiary-3-items/8GmTiza6VxQxjlFA.htm)|Arcane Prepared Spells|auto-trad|
|[8GuVI0hREIK9sjQW.htm](pathfinder-bestiary-3-items/8GuVI0hREIK9sjQW.htm)|Coiling Frenzy|auto-trad|
|[8hoPxayirM6ubTyC.htm](pathfinder-bestiary-3-items/8hoPxayirM6ubTyC.htm)|Longspear|auto-trad|
|[8HtO6GuiGguIFsmV.htm](pathfinder-bestiary-3-items/8HtO6GuiGguIFsmV.htm)|Spine|auto-trad|
|[8IAg57ZRzV51urlh.htm](pathfinder-bestiary-3-items/8IAg57ZRzV51urlh.htm)|Warhammer|auto-trad|
|[8IQ2N3VKsykBfWvw.htm](pathfinder-bestiary-3-items/8IQ2N3VKsykBfWvw.htm)|Pincer|auto-trad|
|[8jAtUvdgKdyaNzHN.htm](pathfinder-bestiary-3-items/8jAtUvdgKdyaNzHN.htm)|Fiery Wake|auto-trad|
|[8JO7dr59w8qgOjKx.htm](pathfinder-bestiary-3-items/8JO7dr59w8qgOjKx.htm)|Sling|auto-trad|
|[8k94OQXOqEJ2FUbE.htm](pathfinder-bestiary-3-items/8k94OQXOqEJ2FUbE.htm)|Grab|auto-trad|
|[8KGUlBcPtdSEm805.htm](pathfinder-bestiary-3-items/8KGUlBcPtdSEm805.htm)|Fist|auto-trad|
|[8kIjzzH6mYzHjhor.htm](pathfinder-bestiary-3-items/8kIjzzH6mYzHjhor.htm)|Darkvision|auto-trad|
|[8kKowoHm6gXmxhb1.htm](pathfinder-bestiary-3-items/8kKowoHm6gXmxhb1.htm)|-1 to All Saves vs. Emotion Effects|auto-trad|
|[8LHwhivJZ4n6E2Gd.htm](pathfinder-bestiary-3-items/8LHwhivJZ4n6E2Gd.htm)|Mass Wriggle|auto-trad|
|[8lKDQyCeoSuhotl9.htm](pathfinder-bestiary-3-items/8lKDQyCeoSuhotl9.htm)|Regeneration 50 (Deactivated by Evil, Mental, or Orichalcum)|auto-trad|
|[8N2A9BvddYZu0qKO.htm](pathfinder-bestiary-3-items/8N2A9BvddYZu0qKO.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[8n2FSC3RM16gF3r7.htm](pathfinder-bestiary-3-items/8n2FSC3RM16gF3r7.htm)|Falchion|auto-trad|
|[8N8GpSFs6h26Jfs5.htm](pathfinder-bestiary-3-items/8N8GpSFs6h26Jfs5.htm)|Prehensile Tail|auto-trad|
|[8NQPS5MLiZHxcRCO.htm](pathfinder-bestiary-3-items/8NQPS5MLiZHxcRCO.htm)|Ward|auto-trad|
|[8NXS92A64sYRn2Rv.htm](pathfinder-bestiary-3-items/8NXS92A64sYRn2Rv.htm)|Bully the Departed|auto-trad|
|[8oZMFFQs6aQ2T29D.htm](pathfinder-bestiary-3-items/8oZMFFQs6aQ2T29D.htm)|Knockdown|auto-trad|
|[8rkyOZr5JbW7dVMr.htm](pathfinder-bestiary-3-items/8rkyOZr5JbW7dVMr.htm)|Nanite Surge|auto-trad|
|[8So4CPwp508RfAuQ.htm](pathfinder-bestiary-3-items/8So4CPwp508RfAuQ.htm)|Hoof|auto-trad|
|[8SPxajKwnuThr56n.htm](pathfinder-bestiary-3-items/8SPxajKwnuThr56n.htm)|Darkvision|auto-trad|
|[8tOUu5ISAj7PMRRU.htm](pathfinder-bestiary-3-items/8tOUu5ISAj7PMRRU.htm)|Absorb Blood|auto-trad|
|[8Ukd831yvoAA6YEz.htm](pathfinder-bestiary-3-items/8Ukd831yvoAA6YEz.htm)|Hull|auto-trad|
|[8UV5F66KioAedVVP.htm](pathfinder-bestiary-3-items/8UV5F66KioAedVVP.htm)|Challenge Foe|auto-trad|
|[8VqNl9kqJUEBBRW1.htm](pathfinder-bestiary-3-items/8VqNl9kqJUEBBRW1.htm)|Ferocious Will|auto-trad|
|[8wjQ8NlAQiJtzns1.htm](pathfinder-bestiary-3-items/8wjQ8NlAQiJtzns1.htm)|Mindbound|auto-trad|
|[8XnKrSfP40tWixA8.htm](pathfinder-bestiary-3-items/8XnKrSfP40tWixA8.htm)|Knockdown|auto-trad|
|[8YPBBSxz9NwSDdqE.htm](pathfinder-bestiary-3-items/8YPBBSxz9NwSDdqE.htm)|Expel Wave|auto-trad|
|[8znZn6HZQJY5Ryah.htm](pathfinder-bestiary-3-items/8znZn6HZQJY5Ryah.htm)|Coiling Frenzy|auto-trad|
|[8zVt6SNpE1sJ6ntx.htm](pathfinder-bestiary-3-items/8zVt6SNpE1sJ6ntx.htm)|Hydraulic Deflection|auto-trad|
|[90h9CZlrzmq29yPp.htm](pathfinder-bestiary-3-items/90h9CZlrzmq29yPp.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[926QjL85SoOXZHaD.htm](pathfinder-bestiary-3-items/926QjL85SoOXZHaD.htm)|Clever Disguises|auto-trad|
|[92aGlrWvxAQQzu5H.htm](pathfinder-bestiary-3-items/92aGlrWvxAQQzu5H.htm)|Cheaters Never Prosper|auto-trad|
|[983mBG3qwmzQKGS2.htm](pathfinder-bestiary-3-items/983mBG3qwmzQKGS2.htm)|Hatred of Mirrors|auto-trad|
|[98oLnVVl0tvsGdHl.htm](pathfinder-bestiary-3-items/98oLnVVl0tvsGdHl.htm)|Darkvision|auto-trad|
|[9aVqnZyByrnNoi0O.htm](pathfinder-bestiary-3-items/9aVqnZyByrnNoi0O.htm)|Plagued Coffin Restoration|auto-trad|
|[9bnT3kvF7tmt4OdX.htm](pathfinder-bestiary-3-items/9bnT3kvF7tmt4OdX.htm)|Draconic Frenzy|auto-trad|
|[9Cf5HlYqqu2sA7jZ.htm](pathfinder-bestiary-3-items/9Cf5HlYqqu2sA7jZ.htm)|Vortex|auto-trad|
|[9DknzWoTx4dGllLO.htm](pathfinder-bestiary-3-items/9DknzWoTx4dGllLO.htm)|Pressgang Soul|auto-trad|
|[9eI0RiQpFdTMAoty.htm](pathfinder-bestiary-3-items/9eI0RiQpFdTMAoty.htm)|Darkvision|auto-trad|
|[9FHqdE5S2AJGokrP.htm](pathfinder-bestiary-3-items/9FHqdE5S2AJGokrP.htm)|Am I Pretty?|auto-trad|
|[9FrKdx9Hv7Nqbxqh.htm](pathfinder-bestiary-3-items/9FrKdx9Hv7Nqbxqh.htm)|Snow Vision|auto-trad|
|[9FS7AxQLqZNLvTdp.htm](pathfinder-bestiary-3-items/9FS7AxQLqZNLvTdp.htm)|At-Will Spells|auto-trad|
|[9h6KJeGxzm8rEPaD.htm](pathfinder-bestiary-3-items/9h6KJeGxzm8rEPaD.htm)|Primal Prepared Spells|auto-trad|
|[9hTzL7fC49Gu7r0d.htm](pathfinder-bestiary-3-items/9hTzL7fC49Gu7r0d.htm)|Jaws|auto-trad|
|[9Io2XFFvCWC7aHPp.htm](pathfinder-bestiary-3-items/9Io2XFFvCWC7aHPp.htm)|Darkvision|auto-trad|
|[9iPQQuORbP2KlVOO.htm](pathfinder-bestiary-3-items/9iPQQuORbP2KlVOO.htm)|Ultrasonic Thrust|auto-trad|
|[9jBzC5INoea9yhwL.htm](pathfinder-bestiary-3-items/9jBzC5INoea9yhwL.htm)|Arcane Innate Spells|auto-trad|
|[9Jcz4Yr2wdXvPymK.htm](pathfinder-bestiary-3-items/9Jcz4Yr2wdXvPymK.htm)|Believe the Lie|auto-trad|
|[9kiCgRcrTLqrvzAj.htm](pathfinder-bestiary-3-items/9kiCgRcrTLqrvzAj.htm)|Negative Healing|auto-trad|
|[9ksWonUvUDnlpZJ9.htm](pathfinder-bestiary-3-items/9ksWonUvUDnlpZJ9.htm)|Darkvision|auto-trad|
|[9LhBHOk8wBt80kaJ.htm](pathfinder-bestiary-3-items/9LhBHOk8wBt80kaJ.htm)|Darkvision|auto-trad|
|[9MHMx6sPv3PI8rHT.htm](pathfinder-bestiary-3-items/9MHMx6sPv3PI8rHT.htm)|Rock|auto-trad|
|[9nNxDGgy23r1ETVB.htm](pathfinder-bestiary-3-items/9nNxDGgy23r1ETVB.htm)|Fed by Water|auto-trad|
|[9NuzaAkDhBBdhrxI.htm](pathfinder-bestiary-3-items/9NuzaAkDhBBdhrxI.htm)|Constrict|auto-trad|
|[9om3JGlrd1NB7cuF.htm](pathfinder-bestiary-3-items/9om3JGlrd1NB7cuF.htm)|Burning Cold Fusillade|auto-trad|
|[9Pg0o9SqvEh925xr.htm](pathfinder-bestiary-3-items/9Pg0o9SqvEh925xr.htm)|In Concert|auto-trad|
|[9RNFc8HCHuW9XJgm.htm](pathfinder-bestiary-3-items/9RNFc8HCHuW9XJgm.htm)|Lithe|auto-trad|
|[9RSHzC11L764mlf4.htm](pathfinder-bestiary-3-items/9RSHzC11L764mlf4.htm)|Liquefy|auto-trad|
|[9RW0VcibxF9cA8uA.htm](pathfinder-bestiary-3-items/9RW0VcibxF9cA8uA.htm)|All This Will Happen Again|auto-trad|
|[9rWukglRKNo6c7Jk.htm](pathfinder-bestiary-3-items/9rWukglRKNo6c7Jk.htm)|Claw|auto-trad|
|[9sOhZcHSPREufoxA.htm](pathfinder-bestiary-3-items/9sOhZcHSPREufoxA.htm)|Arcane Innate Spells|auto-trad|
|[9sVRsQeqp2NqPZKy.htm](pathfinder-bestiary-3-items/9sVRsQeqp2NqPZKy.htm)|Darkvision|auto-trad|
|[9uc04LSVh0aJFRbE.htm](pathfinder-bestiary-3-items/9uc04LSVh0aJFRbE.htm)|At-Will Spells|auto-trad|
|[9uczAsAAuGtsnZP2.htm](pathfinder-bestiary-3-items/9uczAsAAuGtsnZP2.htm)|Attack of Opportunity|auto-trad|
|[9uDPkhvDHQvs18xG.htm](pathfinder-bestiary-3-items/9uDPkhvDHQvs18xG.htm)|Viviparous Birth|auto-trad|
|[9uhb8pW1o8nRmwPD.htm](pathfinder-bestiary-3-items/9uhb8pW1o8nRmwPD.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[9uoAAHOpjHhrvzWl.htm](pathfinder-bestiary-3-items/9uoAAHOpjHhrvzWl.htm)|Darkvision|auto-trad|
|[9UrYZXifKee6nnBC.htm](pathfinder-bestiary-3-items/9UrYZXifKee6nnBC.htm)|Retributive Suplex|auto-trad|
|[9wegz19U5LREiL9x.htm](pathfinder-bestiary-3-items/9wegz19U5LREiL9x.htm)|Strix Vengeance|auto-trad|
|[9Wm3gZTBJ0G9sk1w.htm](pathfinder-bestiary-3-items/9Wm3gZTBJ0G9sk1w.htm)|Glimpse of Redemption|auto-trad|
|[9WqbsuRExPiFqTnh.htm](pathfinder-bestiary-3-items/9WqbsuRExPiFqTnh.htm)|Flameheart Weapon|auto-trad|
|[9WYmk5gm1kpKey4j.htm](pathfinder-bestiary-3-items/9WYmk5gm1kpKey4j.htm)|Attack of Opportunity|auto-trad|
|[9x6UoyWK1LPpP2M1.htm](pathfinder-bestiary-3-items/9x6UoyWK1LPpP2M1.htm)|Arcane Innate Spells|auto-trad|
|[9X9mEo92lODvZP6d.htm](pathfinder-bestiary-3-items/9X9mEo92lODvZP6d.htm)|Troop Defenses|auto-trad|
|[9xVKiyYkmLvWLsPN.htm](pathfinder-bestiary-3-items/9xVKiyYkmLvWLsPN.htm)|Countered by Earth|auto-trad|
|[a0XndmlcRchhe4Sl.htm](pathfinder-bestiary-3-items/a0XndmlcRchhe4Sl.htm)|Horn|auto-trad|
|[a2FJlFGOGuTMTLOI.htm](pathfinder-bestiary-3-items/a2FJlFGOGuTMTLOI.htm)|Fist|auto-trad|
|[A2z7JUAPZ4320zP5.htm](pathfinder-bestiary-3-items/A2z7JUAPZ4320zP5.htm)|Negative Healing|auto-trad|
|[A3POhpGYovQJDozd.htm](pathfinder-bestiary-3-items/A3POhpGYovQJDozd.htm)|Bonded Vessel|auto-trad|
|[a4OTeLXz850yN3NZ.htm](pathfinder-bestiary-3-items/a4OTeLXz850yN3NZ.htm)|Axe Vulnerability|auto-trad|
|[a6e5k1in4L7W0ieF.htm](pathfinder-bestiary-3-items/a6e5k1in4L7W0ieF.htm)|Divine Innate Spells|auto-trad|
|[a77can8I0pBhmKe0.htm](pathfinder-bestiary-3-items/a77can8I0pBhmKe0.htm)|+2 Circumstance to All Saves vs. Shove and Trip|auto-trad|
|[A7emqw5XPEaVatWu.htm](pathfinder-bestiary-3-items/A7emqw5XPEaVatWu.htm)|Constant Spells|auto-trad|
|[a7ntWl311o1OfxCR.htm](pathfinder-bestiary-3-items/a7ntWl311o1OfxCR.htm)|Vulnerable to Sunlight|auto-trad|
|[a83gShMg9yb9LGhv.htm](pathfinder-bestiary-3-items/a83gShMg9yb9LGhv.htm)|At-Will Spells|auto-trad|
|[a8Oke93EjcLb3CyD.htm](pathfinder-bestiary-3-items/a8Oke93EjcLb3CyD.htm)|Trident|auto-trad|
|[a9aLzghe9uCANIDN.htm](pathfinder-bestiary-3-items/a9aLzghe9uCANIDN.htm)|Jaws|auto-trad|
|[aA0Li6f8IO68qqXw.htm](pathfinder-bestiary-3-items/aA0Li6f8IO68qqXw.htm)|Draining Blight|auto-trad|
|[ab11N1FmMkMVAP9Q.htm](pathfinder-bestiary-3-items/ab11N1FmMkMVAP9Q.htm)|Greater Constrict|auto-trad|
|[aBGHzzN9TzDAz9kf.htm](pathfinder-bestiary-3-items/aBGHzzN9TzDAz9kf.htm)|Divine Lightning|auto-trad|
|[AbzlxSNMB7g4nTJc.htm](pathfinder-bestiary-3-items/AbzlxSNMB7g4nTJc.htm)|Passive Points|auto-trad|
|[aCOs4ab0FqX7Nup4.htm](pathfinder-bestiary-3-items/aCOs4ab0FqX7Nup4.htm)|Champion Focus Spells|auto-trad|
|[acq6pZafGJRNM5Um.htm](pathfinder-bestiary-3-items/acq6pZafGJRNM5Um.htm)|+2 Status Bonus on Saves vs. Linguistic Effects|auto-trad|
|[Ad7v2ldIjRYpmP6V.htm](pathfinder-bestiary-3-items/Ad7v2ldIjRYpmP6V.htm)|Swallow Whole|auto-trad|
|[ADj1D97ZuGgEgAMr.htm](pathfinder-bestiary-3-items/ADj1D97ZuGgEgAMr.htm)|At-Will Spells|auto-trad|
|[adMwYHLlDzh2bKaz.htm](pathfinder-bestiary-3-items/adMwYHLlDzh2bKaz.htm)|Divine Innate Spells|auto-trad|
|[adUdgeL10QH11zJu.htm](pathfinder-bestiary-3-items/adUdgeL10QH11zJu.htm)|Mortal Shield|auto-trad|
|[Ae1LTM5lLFv32i3l.htm](pathfinder-bestiary-3-items/Ae1LTM5lLFv32i3l.htm)|Change Shape|auto-trad|
|[AELQ9eZZYSwimA7P.htm](pathfinder-bestiary-3-items/AELQ9eZZYSwimA7P.htm)|Bide|auto-trad|
|[aEnTHY8PwlCJWqzG.htm](pathfinder-bestiary-3-items/aEnTHY8PwlCJWqzG.htm)|Foot|auto-trad|
|[AErcLCe3qBLb8HU0.htm](pathfinder-bestiary-3-items/AErcLCe3qBLb8HU0.htm)|No Breath|auto-trad|
|[AEz7KLpjP8KnKGjx.htm](pathfinder-bestiary-3-items/AEz7KLpjP8KnKGjx.htm)|Liquid Leap|auto-trad|
|[AF48h2X3hAjthWZX.htm](pathfinder-bestiary-3-items/AF48h2X3hAjthWZX.htm)|Jaws|auto-trad|
|[af7dgcRuc5gWm1ej.htm](pathfinder-bestiary-3-items/af7dgcRuc5gWm1ej.htm)|At-Will Spells|auto-trad|
|[AG5uVa2pIKuc9bfQ.htm](pathfinder-bestiary-3-items/AG5uVa2pIKuc9bfQ.htm)|Flailing Thrash|auto-trad|
|[AGsJjbwv43AZYfPx.htm](pathfinder-bestiary-3-items/AGsJjbwv43AZYfPx.htm)|Telepathy 100 feet|auto-trad|
|[AHL4n7RnueMnqqXS.htm](pathfinder-bestiary-3-items/AHL4n7RnueMnqqXS.htm)|Form a Phalanx|auto-trad|
|[aHOLCHiRhu5ipc7L.htm](pathfinder-bestiary-3-items/aHOLCHiRhu5ipc7L.htm)|Ectoplasmic Shield|auto-trad|
|[Ai9DSqNliIuPOhxP.htm](pathfinder-bestiary-3-items/Ai9DSqNliIuPOhxP.htm)|Interplanar Lifesense|auto-trad|
|[AijMBZl9gTqlCcb0.htm](pathfinder-bestiary-3-items/AijMBZl9gTqlCcb0.htm)|Tormented Snarl|auto-trad|
|[AirzEgJQdbzrmlyJ.htm](pathfinder-bestiary-3-items/AirzEgJQdbzrmlyJ.htm)|Light Wisp|auto-trad|
|[aJ9nob7nAtJIDXwG.htm](pathfinder-bestiary-3-items/aJ9nob7nAtJIDXwG.htm)|Arcane Innate Spells|auto-trad|
|[ajC29c9gmHsNUgs1.htm](pathfinder-bestiary-3-items/ajC29c9gmHsNUgs1.htm)|Nymph's Beauty|auto-trad|
|[AJLCWM50AXEWFDYT.htm](pathfinder-bestiary-3-items/AJLCWM50AXEWFDYT.htm)|Telepathy 100 feet|auto-trad|
|[aK7xyvWTJe4GyQ42.htm](pathfinder-bestiary-3-items/aK7xyvWTJe4GyQ42.htm)|Darkvision|auto-trad|
|[aK9UmOvlCoa0e4uF.htm](pathfinder-bestiary-3-items/aK9UmOvlCoa0e4uF.htm)|Arcane Innate Spells|auto-trad|
|[AKn4Oke8VMoU85yk.htm](pathfinder-bestiary-3-items/AKn4Oke8VMoU85yk.htm)|Indomitable Oration|auto-trad|
|[AKUv4ErgbncGAS63.htm](pathfinder-bestiary-3-items/AKUv4ErgbncGAS63.htm)|On All Fours|auto-trad|
|[AkvUF9WUqlp4ytHG.htm](pathfinder-bestiary-3-items/AkvUF9WUqlp4ytHG.htm)|Toxic Body|auto-trad|
|[aKXQLekTbiAqj1Id.htm](pathfinder-bestiary-3-items/aKXQLekTbiAqj1Id.htm)|Troop Movement|auto-trad|
|[alAwSazDkJZWiv6r.htm](pathfinder-bestiary-3-items/alAwSazDkJZWiv6r.htm)|Cooperative Scrying|auto-trad|
|[AlDSrVGyweTmAY0N.htm](pathfinder-bestiary-3-items/AlDSrVGyweTmAY0N.htm)|Hoof|auto-trad|
|[alp4ZDrGM2zotNDt.htm](pathfinder-bestiary-3-items/alp4ZDrGM2zotNDt.htm)|Slash the Suffering|auto-trad|
|[aLsS2G1pU06ABZHI.htm](pathfinder-bestiary-3-items/aLsS2G1pU06ABZHI.htm)|Powerful Scimitars|auto-trad|
|[aMgKVhdJKyEfpoK9.htm](pathfinder-bestiary-3-items/aMgKVhdJKyEfpoK9.htm)|Low-Light Vision|auto-trad|
|[AmUgwUNSujKkZSco.htm](pathfinder-bestiary-3-items/AmUgwUNSujKkZSco.htm)|Forehead|auto-trad|
|[aMxPxph18d6Elee9.htm](pathfinder-bestiary-3-items/aMxPxph18d6Elee9.htm)|Echoblade Flurry|auto-trad|
|[An7ZmOxkCPkGUuBN.htm](pathfinder-bestiary-3-items/An7ZmOxkCPkGUuBN.htm)|Claw|auto-trad|
|[aNoJsFxQdiX8ZDqx.htm](pathfinder-bestiary-3-items/aNoJsFxQdiX8ZDqx.htm)|Halberd|auto-trad|
|[anyFEiqXXoGS63Gs.htm](pathfinder-bestiary-3-items/anyFEiqXXoGS63Gs.htm)|Grasping Tail|auto-trad|
|[aO0RDgMo7pF6pa0h.htm](pathfinder-bestiary-3-items/aO0RDgMo7pF6pa0h.htm)|Snake Fangs|auto-trad|
|[aOaPtLmoTvz46pJw.htm](pathfinder-bestiary-3-items/aOaPtLmoTvz46pJw.htm)|Divine Prepared Spells|auto-trad|
|[aOeCz4c09QMbgoTG.htm](pathfinder-bestiary-3-items/aOeCz4c09QMbgoTG.htm)|Divine Innate Spells|auto-trad|
|[apuiQK82CCi4s2wU.htm](pathfinder-bestiary-3-items/apuiQK82CCi4s2wU.htm)|Echoblade|auto-trad|
|[AQ7Uf21qvFcnJL1X.htm](pathfinder-bestiary-3-items/AQ7Uf21qvFcnJL1X.htm)|Occult Innate Spells|auto-trad|
|[aqpkkRpk5KmwHfhG.htm](pathfinder-bestiary-3-items/aqpkkRpk5KmwHfhG.htm)|Knockdown|auto-trad|
|[AQxSKJnCPtcDDqGB.htm](pathfinder-bestiary-3-items/AQxSKJnCPtcDDqGB.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[arx5pjwY2S5WLBAA.htm](pathfinder-bestiary-3-items/arx5pjwY2S5WLBAA.htm)|Mounted Troop|auto-trad|
|[AsbIVQDcJZfJhU4N.htm](pathfinder-bestiary-3-items/AsbIVQDcJZfJhU4N.htm)|Troop Defenses|auto-trad|
|[ASdIdSgTDc6bN48E.htm](pathfinder-bestiary-3-items/ASdIdSgTDc6bN48E.htm)|Telepathy 100 feet|auto-trad|
|[AsRS8u2z4hfaeLCO.htm](pathfinder-bestiary-3-items/AsRS8u2z4hfaeLCO.htm)|Telepathy 50 feet|auto-trad|
|[At4UFNUZDqPnIhl7.htm](pathfinder-bestiary-3-items/At4UFNUZDqPnIhl7.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[aT8anQIUKNtDMO8Y.htm](pathfinder-bestiary-3-items/aT8anQIUKNtDMO8Y.htm)|Arcane Innate Spells|auto-trad|
|[aT8INKRo18qw8cTl.htm](pathfinder-bestiary-3-items/aT8INKRo18qw8cTl.htm)|Breath Weapon|auto-trad|
|[atf7XFGfAWXeI1TN.htm](pathfinder-bestiary-3-items/atf7XFGfAWXeI1TN.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[atgtDLkpO7KEpGOE.htm](pathfinder-bestiary-3-items/atgtDLkpO7KEpGOE.htm)|Darkvision|auto-trad|
|[atierys9eTCKzPyz.htm](pathfinder-bestiary-3-items/atierys9eTCKzPyz.htm)|Greater Constrict|auto-trad|
|[ATj9bVkYk7iDTGyM.htm](pathfinder-bestiary-3-items/ATj9bVkYk7iDTGyM.htm)|Dagger|auto-trad|
|[AtSxBEaHS4ljnAyv.htm](pathfinder-bestiary-3-items/AtSxBEaHS4ljnAyv.htm)|Coven|auto-trad|
|[AuA52N8pGsULYcjw.htm](pathfinder-bestiary-3-items/AuA52N8pGsULYcjw.htm)|Tendril|auto-trad|
|[AuPR3E2MyY05z9N2.htm](pathfinder-bestiary-3-items/AuPR3E2MyY05z9N2.htm)|Low-Light Vision|auto-trad|
|[AUSkGV3ZjHHnyIrW.htm](pathfinder-bestiary-3-items/AUSkGV3ZjHHnyIrW.htm)|Skitter Away|auto-trad|
|[aVCE3zDK0cQohDQM.htm](pathfinder-bestiary-3-items/aVCE3zDK0cQohDQM.htm)|Monk Ki Spells|auto-trad|
|[AvNNZdhRs6SKQiy3.htm](pathfinder-bestiary-3-items/AvNNZdhRs6SKQiy3.htm)|Divine Innate Spells|auto-trad|
|[aw8UyWcvmu5VncsT.htm](pathfinder-bestiary-3-items/aw8UyWcvmu5VncsT.htm)|Mist Vision|auto-trad|
|[AWOczN1NAKx6tjD8.htm](pathfinder-bestiary-3-items/AWOczN1NAKx6tjD8.htm)|Kukri|auto-trad|
|[AwRCPMRzo9dIymmM.htm](pathfinder-bestiary-3-items/AwRCPMRzo9dIymmM.htm)|Susceptible to Death|auto-trad|
|[AXuWafDpkZy5SX8V.htm](pathfinder-bestiary-3-items/AXuWafDpkZy5SX8V.htm)|Incite Violence|auto-trad|
|[aYd4v0mDwmgHckIH.htm](pathfinder-bestiary-3-items/aYd4v0mDwmgHckIH.htm)|Bonded Strike|auto-trad|
|[AyDallashGfDWvMX.htm](pathfinder-bestiary-3-items/AyDallashGfDWvMX.htm)|Telepathy 100 feet|auto-trad|
|[ayEWmTQnDQXk54zc.htm](pathfinder-bestiary-3-items/ayEWmTQnDQXk54zc.htm)|Immortal Flesh|auto-trad|
|[aYv6mdSXvzzYWgRA.htm](pathfinder-bestiary-3-items/aYv6mdSXvzzYWgRA.htm)|Capuchin's Curse|auto-trad|
|[aZTKSdmtywAvr7TJ.htm](pathfinder-bestiary-3-items/aZTKSdmtywAvr7TJ.htm)|Claw|auto-trad|
|[AzWuaOagwffOzhLP.htm](pathfinder-bestiary-3-items/AzWuaOagwffOzhLP.htm)|Sneak Attack|auto-trad|
|[aZYbkerDQjojnR8G.htm](pathfinder-bestiary-3-items/aZYbkerDQjojnR8G.htm)|Shield Block|auto-trad|
|[B0CIGI7Er3AYCaem.htm](pathfinder-bestiary-3-items/B0CIGI7Er3AYCaem.htm)|Darkvision|auto-trad|
|[B0U6YmeB8UGglyGa.htm](pathfinder-bestiary-3-items/B0U6YmeB8UGglyGa.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[B2rF45WAMrowNHio.htm](pathfinder-bestiary-3-items/B2rF45WAMrowNHio.htm)|Darkvision|auto-trad|
|[b2yf3FTVhb4cL3mI.htm](pathfinder-bestiary-3-items/b2yf3FTVhb4cL3mI.htm)|Pose a Riddle|auto-trad|
|[b3L5EEZVfcjwleqc.htm](pathfinder-bestiary-3-items/b3L5EEZVfcjwleqc.htm)|Low-Light Vision|auto-trad|
|[B3NBi21pYXb87QM2.htm](pathfinder-bestiary-3-items/B3NBi21pYXb87QM2.htm)|Nosferatu Vulnerabilities|auto-trad|
|[b4LBZAjcRm8KzaWR.htm](pathfinder-bestiary-3-items/b4LBZAjcRm8KzaWR.htm)|Darkvision|auto-trad|
|[B5cFXl5CIucCzoy0.htm](pathfinder-bestiary-3-items/B5cFXl5CIucCzoy0.htm)|Launched Blade|auto-trad|
|[b5tNOyXUMi9maaal.htm](pathfinder-bestiary-3-items/b5tNOyXUMi9maaal.htm)|Telepathy 100 feet|auto-trad|
|[B6M6fTBtJwAQDYCs.htm](pathfinder-bestiary-3-items/B6M6fTBtJwAQDYCs.htm)|Swarm Mind|auto-trad|
|[b70iU5hV08AtqcDw.htm](pathfinder-bestiary-3-items/b70iU5hV08AtqcDw.htm)|Coven Spells|auto-trad|
|[b7j8M7xzLStzldNC.htm](pathfinder-bestiary-3-items/b7j8M7xzLStzldNC.htm)|Fast Healing 5|auto-trad|
|[b8889HDEEpdiqYFf.htm](pathfinder-bestiary-3-items/b8889HDEEpdiqYFf.htm)|Constant Spells|auto-trad|
|[b8MhSEnrYG0hWLqw.htm](pathfinder-bestiary-3-items/b8MhSEnrYG0hWLqw.htm)|+2 Status to All Saves vs. Divine Magic|auto-trad|
|[B8Uf7QHdJ2D0Fs74.htm](pathfinder-bestiary-3-items/B8Uf7QHdJ2D0Fs74.htm)|Capture|auto-trad|
|[B9EouOshvYsQf0Td.htm](pathfinder-bestiary-3-items/B9EouOshvYsQf0Td.htm)|Low-Light Vision|auto-trad|
|[baOID7dh8Ikettqn.htm](pathfinder-bestiary-3-items/baOID7dh8Ikettqn.htm)|Magical Broth|auto-trad|
|[BaTacjtQvnaLoVc5.htm](pathfinder-bestiary-3-items/BaTacjtQvnaLoVc5.htm)|Wind-Up|auto-trad|
|[bAYPijLnxouZs1Ea.htm](pathfinder-bestiary-3-items/bAYPijLnxouZs1Ea.htm)|Grab|auto-trad|
|[bcflm5XlNEDnnGZ3.htm](pathfinder-bestiary-3-items/bcflm5XlNEDnnGZ3.htm)|Low-Light Vision|auto-trad|
|[BCIdFtWYKRXP0vLE.htm](pathfinder-bestiary-3-items/BCIdFtWYKRXP0vLE.htm)|Typhoon Dive|auto-trad|
|[BCsGqHil1Dziu3FX.htm](pathfinder-bestiary-3-items/BCsGqHil1Dziu3FX.htm)|Lower Halberds!|auto-trad|
|[BcWStlMljvf4ap7v.htm](pathfinder-bestiary-3-items/BcWStlMljvf4ap7v.htm)|Wide Cleave|auto-trad|
|[BD35N4Wa7tdNg5y4.htm](pathfinder-bestiary-3-items/BD35N4Wa7tdNg5y4.htm)|Jaws|auto-trad|
|[BD81Ab7NPFAacuhc.htm](pathfinder-bestiary-3-items/BD81Ab7NPFAacuhc.htm)|Attack of Opportunity (Special)|auto-trad|
|[BDbSH109hF8RleEa.htm](pathfinder-bestiary-3-items/BDbSH109hF8RleEa.htm)|Hatred of Red|auto-trad|
|[bDZ6kq6HqfFLK2PU.htm](pathfinder-bestiary-3-items/bDZ6kq6HqfFLK2PU.htm)|Embed|auto-trad|
|[BE8uoWMEnODt0Xw7.htm](pathfinder-bestiary-3-items/BE8uoWMEnODt0Xw7.htm)|Swallow Whole|auto-trad|
|[BEcAt0SdsvIa6qp9.htm](pathfinder-bestiary-3-items/BEcAt0SdsvIa6qp9.htm)|Befuddling Lash|auto-trad|
|[befWMYmJQKeMMpzD.htm](pathfinder-bestiary-3-items/befWMYmJQKeMMpzD.htm)|Club|auto-trad|
|[BFhitQjxtXx7cQKS.htm](pathfinder-bestiary-3-items/BFhitQjxtXx7cQKS.htm)|Sting Shot|auto-trad|
|[BFS5mQ3ITub7ZdJL.htm](pathfinder-bestiary-3-items/BFS5mQ3ITub7ZdJL.htm)|Claw|auto-trad|
|[BG9o62cI16MBV3Qy.htm](pathfinder-bestiary-3-items/BG9o62cI16MBV3Qy.htm)|Darkvision|auto-trad|
|[BgiZcqILhEbxEVG7.htm](pathfinder-bestiary-3-items/BgiZcqILhEbxEVG7.htm)|In Concert|auto-trad|
|[BgqCS9ExYivBMHU2.htm](pathfinder-bestiary-3-items/BgqCS9ExYivBMHU2.htm)|Occult Innate Spells|auto-trad|
|[bgr9Qyvp6pX1W3KQ.htm](pathfinder-bestiary-3-items/bgr9Qyvp6pX1W3KQ.htm)|Raktavarna Venom|auto-trad|
|[bgwyucC0EpyEmxl7.htm](pathfinder-bestiary-3-items/bgwyucC0EpyEmxl7.htm)|Change Shape|auto-trad|
|[Bh4tXIFDTCdh3Vzs.htm](pathfinder-bestiary-3-items/Bh4tXIFDTCdh3Vzs.htm)|Breath Weapon|auto-trad|
|[BHN2n3G77QRNLHhM.htm](pathfinder-bestiary-3-items/BHN2n3G77QRNLHhM.htm)|Stinger|auto-trad|
|[BHNm9PtnZ9Hrj6fQ.htm](pathfinder-bestiary-3-items/BHNm9PtnZ9Hrj6fQ.htm)|Unsettled Mind|auto-trad|
|[bhqneDVB7PGBIMMh.htm](pathfinder-bestiary-3-items/bhqneDVB7PGBIMMh.htm)|Divine Innate Spells|auto-trad|
|[BI9PPwdfEfP8sJv5.htm](pathfinder-bestiary-3-items/BI9PPwdfEfP8sJv5.htm)|Constant Spells|auto-trad|
|[BIKoqqYwjx9IZ1th.htm](pathfinder-bestiary-3-items/BIKoqqYwjx9IZ1th.htm)|Fox's Cunning|auto-trad|
|[bIOojNVEElRivlmd.htm](pathfinder-bestiary-3-items/bIOojNVEElRivlmd.htm)|Claw|auto-trad|
|[BISNEn6kmVke1CIR.htm](pathfinder-bestiary-3-items/BISNEn6kmVke1CIR.htm)|Forest Shape|auto-trad|
|[bjqB60qqpfDFqKNU.htm](pathfinder-bestiary-3-items/bjqB60qqpfDFqKNU.htm)|Primal Innate Spells|auto-trad|
|[bJUdfd279JTmWONI.htm](pathfinder-bestiary-3-items/bJUdfd279JTmWONI.htm)|Door|auto-trad|
|[bkajUVm6XDKbIO89.htm](pathfinder-bestiary-3-items/bkajUVm6XDKbIO89.htm)|Spike|auto-trad|
|[BKsoNNVMI5d9mEhL.htm](pathfinder-bestiary-3-items/BKsoNNVMI5d9mEhL.htm)|All-Around Vision|auto-trad|
|[BlH8W4GE6xpZoZM2.htm](pathfinder-bestiary-3-items/BlH8W4GE6xpZoZM2.htm)|Troop Defenses|auto-trad|
|[BLkAKMlADR3rQxEw.htm](pathfinder-bestiary-3-items/BLkAKMlADR3rQxEw.htm)|Troop Defenses|auto-trad|
|[bLo1G95EZjpM4Zf6.htm](pathfinder-bestiary-3-items/bLo1G95EZjpM4Zf6.htm)|Arcane Innate Spells|auto-trad|
|[blSV9HiaoSCWQT9w.htm](pathfinder-bestiary-3-items/blSV9HiaoSCWQT9w.htm)|Grab|auto-trad|
|[bLxMh4Pt4sU0x8Pp.htm](pathfinder-bestiary-3-items/bLxMh4Pt4sU0x8Pp.htm)|Telepathic Singer|auto-trad|
|[bMrJDUmLpocEes2w.htm](pathfinder-bestiary-3-items/bMrJDUmLpocEes2w.htm)|Draconic Momentum|auto-trad|
|[bMrmRT314fkcPeuk.htm](pathfinder-bestiary-3-items/bMrmRT314fkcPeuk.htm)|Jaws|auto-trad|
|[bnBa5XcA2wHzP5KM.htm](pathfinder-bestiary-3-items/bnBa5XcA2wHzP5KM.htm)|Champion Devotion Spells|auto-trad|
|[bnU4poD2v5S9mLDT.htm](pathfinder-bestiary-3-items/bnU4poD2v5S9mLDT.htm)|Eternal Fear|auto-trad|
|[Bo9liMM0Qn69gKdi.htm](pathfinder-bestiary-3-items/Bo9liMM0Qn69gKdi.htm)|Grab|auto-trad|
|[Bpok96twh3ZtuzEH.htm](pathfinder-bestiary-3-items/Bpok96twh3ZtuzEH.htm)|Wavesense (Imprecise) 60 feet|auto-trad|
|[bpvnWZi8sukcfy9H.htm](pathfinder-bestiary-3-items/bpvnWZi8sukcfy9H.htm)|Displace|auto-trad|
|[bqDyC6IKtXktariv.htm](pathfinder-bestiary-3-items/bqDyC6IKtXktariv.htm)|Needle|auto-trad|
|[BqST74R7dRaivdDQ.htm](pathfinder-bestiary-3-items/BqST74R7dRaivdDQ.htm)|Beak|auto-trad|
|[bQxZThiBIxPwNuDH.htm](pathfinder-bestiary-3-items/bQxZThiBIxPwNuDH.htm)|Sea Spray|auto-trad|
|[bQZIEB7cmxSGgulV.htm](pathfinder-bestiary-3-items/bQZIEB7cmxSGgulV.htm)|Jaws|auto-trad|
|[br8FCwfVCLAXisnU.htm](pathfinder-bestiary-3-items/br8FCwfVCLAXisnU.htm)|Dragon's Wisdom|auto-trad|
|[brAwbDIfmKeRHjjo.htm](pathfinder-bestiary-3-items/brAwbDIfmKeRHjjo.htm)|Verdant Burst|auto-trad|
|[bRWkzuZ2ByRvUEXx.htm](pathfinder-bestiary-3-items/bRWkzuZ2ByRvUEXx.htm)|Negative Healing|auto-trad|
|[BRY6lAS6ePaNJsIO.htm](pathfinder-bestiary-3-items/BRY6lAS6ePaNJsIO.htm)|Disk Rider|auto-trad|
|[bScgZ7J4SQWXIPfp.htm](pathfinder-bestiary-3-items/bScgZ7J4SQWXIPfp.htm)|Void Transmission|auto-trad|
|[bsKbGLCEij4kqAPY.htm](pathfinder-bestiary-3-items/bsKbGLCEij4kqAPY.htm)|Ice Staff|auto-trad|
|[BsVgKelZGZWfZaYs.htm](pathfinder-bestiary-3-items/BsVgKelZGZWfZaYs.htm)|Greatclub|auto-trad|
|[bTNygOkcj07sIda5.htm](pathfinder-bestiary-3-items/bTNygOkcj07sIda5.htm)|Improved Grab|auto-trad|
|[btVPiH1rJHK8iziH.htm](pathfinder-bestiary-3-items/btVPiH1rJHK8iziH.htm)|Elven Curve Blade|auto-trad|
|[BTxgwCFuROqASlPz.htm](pathfinder-bestiary-3-items/BTxgwCFuROqASlPz.htm)|Darkvision|auto-trad|
|[bUFMLfwRNZ1zBmqa.htm](pathfinder-bestiary-3-items/bUFMLfwRNZ1zBmqa.htm)|Swarm Mind|auto-trad|
|[BurQSnXk9enXhVZH.htm](pathfinder-bestiary-3-items/BurQSnXk9enXhVZH.htm)|Steal Breath|auto-trad|
|[BVccjoWLx8QgvIDh.htm](pathfinder-bestiary-3-items/BVccjoWLx8QgvIDh.htm)|Thoughtsense (Imprecise) 60 feet|auto-trad|
|[BVGtxqRvaiXmxNDG.htm](pathfinder-bestiary-3-items/BVGtxqRvaiXmxNDG.htm)|Rage of Spirits|auto-trad|
|[BVZS9T28bimG6NBU.htm](pathfinder-bestiary-3-items/BVZS9T28bimG6NBU.htm)|Primal Innate Spells|auto-trad|
|[BW9Qv2EOWjmgeEgL.htm](pathfinder-bestiary-3-items/BW9Qv2EOWjmgeEgL.htm)|Inspiration|auto-trad|
|[BwimiLeXJKeEigfN.htm](pathfinder-bestiary-3-items/BwimiLeXJKeEigfN.htm)|Tail|auto-trad|
|[bwScFju6YTBM7feQ.htm](pathfinder-bestiary-3-items/bwScFju6YTBM7feQ.htm)|Horn|auto-trad|
|[BwU8clOdeUsnJX4w.htm](pathfinder-bestiary-3-items/BwU8clOdeUsnJX4w.htm)|Push|auto-trad|
|[BX7sT7k5GNZuI9hc.htm](pathfinder-bestiary-3-items/BX7sT7k5GNZuI9hc.htm)|Silver Scissors|auto-trad|
|[bxnrpUx8pN5QerLc.htm](pathfinder-bestiary-3-items/bxnrpUx8pN5QerLc.htm)|Divine Prepared Spells|auto-trad|
|[bYiotnUBfPu81i3V.htm](pathfinder-bestiary-3-items/bYiotnUBfPu81i3V.htm)|Swarm Mind|auto-trad|
|[BySVNQG4P1lbNbyJ.htm](pathfinder-bestiary-3-items/BySVNQG4P1lbNbyJ.htm)|Mortic Ferocity|auto-trad|
|[BZPtJSauua8fLt2G.htm](pathfinder-bestiary-3-items/BZPtJSauua8fLt2G.htm)|At-Will Spells|auto-trad|
|[C0NEkV1zwoV5vogY.htm](pathfinder-bestiary-3-items/C0NEkV1zwoV5vogY.htm)|Reconstitution|auto-trad|
|[C10wjHeJK4PRbQiQ.htm](pathfinder-bestiary-3-items/C10wjHeJK4PRbQiQ.htm)|Jaws|auto-trad|
|[C22ZfqwOAI1G9r1O.htm](pathfinder-bestiary-3-items/C22ZfqwOAI1G9r1O.htm)|Maul|auto-trad|
|[c2jgSAE0FH6AmDJ2.htm](pathfinder-bestiary-3-items/c2jgSAE0FH6AmDJ2.htm)|Jaws|auto-trad|
|[c2zqPCnz9E8UlKRw.htm](pathfinder-bestiary-3-items/c2zqPCnz9E8UlKRw.htm)|Arcane Innate Spells|auto-trad|
|[c3sGdZtwuCDBGlig.htm](pathfinder-bestiary-3-items/c3sGdZtwuCDBGlig.htm)|Shortbow|auto-trad|
|[c85RY0nsV1xyDA3t.htm](pathfinder-bestiary-3-items/c85RY0nsV1xyDA3t.htm)|Improved Grab|auto-trad|
|[C8pZwyrtELK6lVYw.htm](pathfinder-bestiary-3-items/C8pZwyrtELK6lVYw.htm)|Arrow of Despair|auto-trad|
|[c95y554eFZ79003o.htm](pathfinder-bestiary-3-items/c95y554eFZ79003o.htm)|Grab|auto-trad|
|[caAfIPQy8oLw6CGa.htm](pathfinder-bestiary-3-items/caAfIPQy8oLw6CGa.htm)|At-Will Spells|auto-trad|
|[CAgNpEik2Ky3XXu4.htm](pathfinder-bestiary-3-items/CAgNpEik2Ky3XXu4.htm)|Retract|auto-trad|
|[caybfvEpmrHbdtN6.htm](pathfinder-bestiary-3-items/caybfvEpmrHbdtN6.htm)|Return Arrow|auto-trad|
|[cb5dMTXksovLZMVP.htm](pathfinder-bestiary-3-items/cb5dMTXksovLZMVP.htm)|Claw|auto-trad|
|[cBLNcI8XKhBuOFqE.htm](pathfinder-bestiary-3-items/cBLNcI8XKhBuOFqE.htm)|Claw|auto-trad|
|[CC7MztEx8rEoNgaL.htm](pathfinder-bestiary-3-items/CC7MztEx8rEoNgaL.htm)|Longsword|auto-trad|
|[CChXCrOyQxz6h0X9.htm](pathfinder-bestiary-3-items/CChXCrOyQxz6h0X9.htm)|Abeyance Rift|auto-trad|
|[CcOaWq1ZikuzfZ88.htm](pathfinder-bestiary-3-items/CcOaWq1ZikuzfZ88.htm)|Tenebral Form|auto-trad|
|[CCUY44whvoSSEz3I.htm](pathfinder-bestiary-3-items/CCUY44whvoSSEz3I.htm)|Primal Innate Spells|auto-trad|
|[Cdh7brQkfnpWbZDJ.htm](pathfinder-bestiary-3-items/Cdh7brQkfnpWbZDJ.htm)|Mandibles|auto-trad|
|[cdmwwMzpk0Bsc4Jg.htm](pathfinder-bestiary-3-items/cdmwwMzpk0Bsc4Jg.htm)|Jaws|auto-trad|
|[cdrifaen6W903klB.htm](pathfinder-bestiary-3-items/cdrifaen6W903klB.htm)|Telepathy 100 feet|auto-trad|
|[ce63eYi0AotvHaz2.htm](pathfinder-bestiary-3-items/ce63eYi0AotvHaz2.htm)|Constant Spells|auto-trad|
|[cE6juVvy0O4iJMgY.htm](pathfinder-bestiary-3-items/cE6juVvy0O4iJMgY.htm)|Divine Innate Spells|auto-trad|
|[CEHvST4zuzVjG0oO.htm](pathfinder-bestiary-3-items/CEHvST4zuzVjG0oO.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[ceUAQwTriMohyVjk.htm](pathfinder-bestiary-3-items/ceUAQwTriMohyVjk.htm)|Swooping Dive|auto-trad|
|[CFh42XjjfBF8zZRE.htm](pathfinder-bestiary-3-items/CFh42XjjfBF8zZRE.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[CFiD5D2XvZ2N67x1.htm](pathfinder-bestiary-3-items/CFiD5D2XvZ2N67x1.htm)|Swarming Gnaw|auto-trad|
|[cFSlH4qPRNdzcxY6.htm](pathfinder-bestiary-3-items/cFSlH4qPRNdzcxY6.htm)|Tide of Creation|auto-trad|
|[cg8fgSm6sGAJE6bt.htm](pathfinder-bestiary-3-items/cg8fgSm6sGAJE6bt.htm)|Fast Healing 10|auto-trad|
|[CGIXYmLYYlMz2iRx.htm](pathfinder-bestiary-3-items/CGIXYmLYYlMz2iRx.htm)|Primal Innate Spells|auto-trad|
|[CgQW6ZFOrGsrgkHZ.htm](pathfinder-bestiary-3-items/CgQW6ZFOrGsrgkHZ.htm)|Throw Rock|auto-trad|
|[CGyzuG0hwimkA0im.htm](pathfinder-bestiary-3-items/CGyzuG0hwimkA0im.htm)|Champion Devotion Spells|auto-trad|
|[CHKrjbjG2iuM6dGa.htm](pathfinder-bestiary-3-items/CHKrjbjG2iuM6dGa.htm)|Black Apoxia|auto-trad|
|[cIdzhTylzbVeNykJ.htm](pathfinder-bestiary-3-items/cIdzhTylzbVeNykJ.htm)|Arcane Innate Spells|auto-trad|
|[CjNF1hyzoK7u7txT.htm](pathfinder-bestiary-3-items/CjNF1hyzoK7u7txT.htm)|Wind Blast|auto-trad|
|[ck37LN0pcQmPLf0a.htm](pathfinder-bestiary-3-items/ck37LN0pcQmPLf0a.htm)|Troop Movement|auto-trad|
|[CK5NsrPLZx91r8av.htm](pathfinder-bestiary-3-items/CK5NsrPLZx91r8av.htm)|Constant Spells|auto-trad|
|[CKhA0wknMwjoGOGJ.htm](pathfinder-bestiary-3-items/CKhA0wknMwjoGOGJ.htm)|Low-Light Vision|auto-trad|
|[cLFteTTZ1b4O3AoI.htm](pathfinder-bestiary-3-items/cLFteTTZ1b4O3AoI.htm)|Fire Crossbows!|auto-trad|
|[cm8kh6khpDzrtqm7.htm](pathfinder-bestiary-3-items/cm8kh6khpDzrtqm7.htm)|Divine Innate Spells|auto-trad|
|[CMbpDKN5GasPKdEy.htm](pathfinder-bestiary-3-items/CMbpDKN5GasPKdEy.htm)|Primal Innate Spells|auto-trad|
|[cp5UE666Pg5Pz1gR.htm](pathfinder-bestiary-3-items/cp5UE666Pg5Pz1gR.htm)|Shadow Step|auto-trad|
|[cPRyXJDPZFJ7bHKq.htm](pathfinder-bestiary-3-items/cPRyXJDPZFJ7bHKq.htm)|Sweltering Heat|auto-trad|
|[cpVCCJ4aUToJ2wyU.htm](pathfinder-bestiary-3-items/cpVCCJ4aUToJ2wyU.htm)|Beak|auto-trad|
|[cpZes8po87QoRYMi.htm](pathfinder-bestiary-3-items/cpZes8po87QoRYMi.htm)|Occult Innate Spells|auto-trad|
|[cQCs1PH16u35vCzT.htm](pathfinder-bestiary-3-items/cQCs1PH16u35vCzT.htm)|Countered by Fire|auto-trad|
|[cQMEu0nbq6cPn2sD.htm](pathfinder-bestiary-3-items/cQMEu0nbq6cPn2sD.htm)|Hellstrider|auto-trad|
|[CqrOCl7qaEx1GVuB.htm](pathfinder-bestiary-3-items/CqrOCl7qaEx1GVuB.htm)|Powerful Charge|auto-trad|
|[CR2hQQQ9hlXe7miP.htm](pathfinder-bestiary-3-items/CR2hQQQ9hlXe7miP.htm)|Divine Innate Spells|auto-trad|
|[CrApsTeWiiQiyew0.htm](pathfinder-bestiary-3-items/CrApsTeWiiQiyew0.htm)|Living Machine|auto-trad|
|[CRN9VNs8XxxFho4z.htm](pathfinder-bestiary-3-items/CRN9VNs8XxxFho4z.htm)|Low-Light Vision|auto-trad|
|[CsRpTNQIfspNNH9f.htm](pathfinder-bestiary-3-items/CsRpTNQIfspNNH9f.htm)|Darkvision|auto-trad|
|[ct5v7Ozu8ADoqRMV.htm](pathfinder-bestiary-3-items/ct5v7Ozu8ADoqRMV.htm)|Primal Innate Spells|auto-trad|
|[CTfA2dy8dT55Ygfa.htm](pathfinder-bestiary-3-items/CTfA2dy8dT55Ygfa.htm)|Knockdown|auto-trad|
|[cTfE4TeDdxDyNmZt.htm](pathfinder-bestiary-3-items/cTfE4TeDdxDyNmZt.htm)|Darkvision|auto-trad|
|[cUAlKL5D2aom7SjG.htm](pathfinder-bestiary-3-items/cUAlKL5D2aom7SjG.htm)|Greater Darkvision|auto-trad|
|[cuBDxCCJtk4iMr0A.htm](pathfinder-bestiary-3-items/cuBDxCCJtk4iMr0A.htm)|Trailblazing Stride|auto-trad|
|[CucaKzBXWexDGgV3.htm](pathfinder-bestiary-3-items/CucaKzBXWexDGgV3.htm)|Swing Back|auto-trad|
|[cUQLbIojBehGDQn1.htm](pathfinder-bestiary-3-items/cUQLbIojBehGDQn1.htm)|Frightful Presence|auto-trad|
|[cur7nFf5cRCA5ccx.htm](pathfinder-bestiary-3-items/cur7nFf5cRCA5ccx.htm)|Occult Innate Spells|auto-trad|
|[CuvZkG5iAgBSIV6w.htm](pathfinder-bestiary-3-items/CuvZkG5iAgBSIV6w.htm)|Darkvision|auto-trad|
|[cUvzo0ln0c4WgCh2.htm](pathfinder-bestiary-3-items/cUvzo0ln0c4WgCh2.htm)|Mind Swap|auto-trad|
|[Cv7lau7bBNhJQwti.htm](pathfinder-bestiary-3-items/Cv7lau7bBNhJQwti.htm)|Fist|auto-trad|
|[CvF5ABLW3yZL4Pya.htm](pathfinder-bestiary-3-items/CvF5ABLW3yZL4Pya.htm)|Dagger|auto-trad|
|[CVVLZgcOZxdjSrnH.htm](pathfinder-bestiary-3-items/CVVLZgcOZxdjSrnH.htm)|Wasting Curse|auto-trad|
|[Cw4UJYMkN1OxnJo9.htm](pathfinder-bestiary-3-items/Cw4UJYMkN1OxnJo9.htm)|Grab|auto-trad|
|[cwfUBFIbuyqydNEj.htm](pathfinder-bestiary-3-items/cwfUBFIbuyqydNEj.htm)|Darkvision|auto-trad|
|[CWqtP7800C9YheCY.htm](pathfinder-bestiary-3-items/CWqtP7800C9YheCY.htm)|Constant Spells|auto-trad|
|[cwvyDjJgUkCmj3aI.htm](pathfinder-bestiary-3-items/cwvyDjJgUkCmj3aI.htm)|Adamantine Claws|auto-trad|
|[cwzm6hgGDNWyBDjU.htm](pathfinder-bestiary-3-items/cwzm6hgGDNWyBDjU.htm)|Draconic Momentum|auto-trad|
|[cXcE5HUwD2KD7fgp.htm](pathfinder-bestiary-3-items/cXcE5HUwD2KD7fgp.htm)|Regeneration 30 (Deactivated by Acid, Cold, or Fire)|auto-trad|
|[cXE4SYOLUHsl6waW.htm](pathfinder-bestiary-3-items/cXE4SYOLUHsl6waW.htm)|Darkvision|auto-trad|
|[cxN4u9cT3UrjnDry.htm](pathfinder-bestiary-3-items/cxN4u9cT3UrjnDry.htm)|Water Jet|auto-trad|
|[CybU0X1fwtJZX5W4.htm](pathfinder-bestiary-3-items/CybU0X1fwtJZX5W4.htm)|Telepathy 60 feet|auto-trad|
|[CYFB6ARFi1FTJRUe.htm](pathfinder-bestiary-3-items/CYFB6ARFi1FTJRUe.htm)|Low-Light Vision|auto-trad|
|[cyqkRqwp35f8CtAJ.htm](pathfinder-bestiary-3-items/cyqkRqwp35f8CtAJ.htm)|Whispers of Discord|auto-trad|
|[Cz9TDLxidHRU8bVI.htm](pathfinder-bestiary-3-items/Cz9TDLxidHRU8bVI.htm)|Psychic Shard|auto-trad|
|[CzKSMmeRg2rmeWlU.htm](pathfinder-bestiary-3-items/CzKSMmeRg2rmeWlU.htm)|Telepathy 100 feet|auto-trad|
|[CZmwCcrjPndjeWl3.htm](pathfinder-bestiary-3-items/CZmwCcrjPndjeWl3.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[CZrjwV0LG9oK1IpV.htm](pathfinder-bestiary-3-items/CZrjwV0LG9oK1IpV.htm)|Countered by Fire|auto-trad|
|[CZTZt5TtBNcJtjOp.htm](pathfinder-bestiary-3-items/CZTZt5TtBNcJtjOp.htm)|Uncanny Pounce|auto-trad|
|[czupLRtK5lXMuEaI.htm](pathfinder-bestiary-3-items/czupLRtK5lXMuEaI.htm)|Telepathy 80 feet|auto-trad|
|[D0ZfsdnVuEVXFyqB.htm](pathfinder-bestiary-3-items/D0ZfsdnVuEVXFyqB.htm)|Focused Slam|auto-trad|
|[D1C3re9l8xuzleoq.htm](pathfinder-bestiary-3-items/D1C3re9l8xuzleoq.htm)|Divine Innate Spells|auto-trad|
|[d1cyC0vptbTtXFE2.htm](pathfinder-bestiary-3-items/d1cyC0vptbTtXFE2.htm)|Dagger|auto-trad|
|[D1Nl8ZLF8I2wRwxv.htm](pathfinder-bestiary-3-items/D1Nl8ZLF8I2wRwxv.htm)|Hoof|auto-trad|
|[d26LgNmNMpMAAoX3.htm](pathfinder-bestiary-3-items/d26LgNmNMpMAAoX3.htm)|Calculated Reload|auto-trad|
|[d2g5vbtRBlZsAPu4.htm](pathfinder-bestiary-3-items/d2g5vbtRBlZsAPu4.htm)|Wrathful Misfortune|auto-trad|
|[d3CPZluPoEOBmUDr.htm](pathfinder-bestiary-3-items/d3CPZluPoEOBmUDr.htm)|Arcane Innate Spells|auto-trad|
|[D4ExwJPbq9vP2fZU.htm](pathfinder-bestiary-3-items/D4ExwJPbq9vP2fZU.htm)|Shy|auto-trad|
|[D4noCa8oJeJbAEEn.htm](pathfinder-bestiary-3-items/D4noCa8oJeJbAEEn.htm)|Change Shape|auto-trad|
|[D5k9m1BTcXvhogT6.htm](pathfinder-bestiary-3-items/D5k9m1BTcXvhogT6.htm)|Composite Shortbow|auto-trad|
|[d5pg9c64JL7EqHVV.htm](pathfinder-bestiary-3-items/d5pg9c64JL7EqHVV.htm)|Earth Glide|auto-trad|
|[D6GffrGRbXblVQ49.htm](pathfinder-bestiary-3-items/D6GffrGRbXblVQ49.htm)|Transparent|auto-trad|
|[D6XunANSlrJ1WOJS.htm](pathfinder-bestiary-3-items/D6XunANSlrJ1WOJS.htm)|Tilt Scales|auto-trad|
|[d7MLmQG9ZkBIstUb.htm](pathfinder-bestiary-3-items/d7MLmQG9ZkBIstUb.htm)|Compulsive Counting|auto-trad|
|[d85usCDnVlCRnJzP.htm](pathfinder-bestiary-3-items/d85usCDnVlCRnJzP.htm)|Darkvision|auto-trad|
|[d8kaihBUHhOfdpSO.htm](pathfinder-bestiary-3-items/d8kaihBUHhOfdpSO.htm)|Coiling Frenzy|auto-trad|
|[D8VyX86E4dNG3pvk.htm](pathfinder-bestiary-3-items/D8VyX86E4dNG3pvk.htm)|Katana|auto-trad|
|[d8xFQfk53fDdc7Ge.htm](pathfinder-bestiary-3-items/d8xFQfk53fDdc7Ge.htm)|Telepathy (Touch)|auto-trad|
|[d9VRUtPpmcA0zUfF.htm](pathfinder-bestiary-3-items/d9VRUtPpmcA0zUfF.htm)|Darkvision|auto-trad|
|[DA63Y6dqt1pn07ex.htm](pathfinder-bestiary-3-items/DA63Y6dqt1pn07ex.htm)|Sludge Tendril|auto-trad|
|[DADYHBK8ittlLsj9.htm](pathfinder-bestiary-3-items/DADYHBK8ittlLsj9.htm)|At-Will Spells|auto-trad|
|[dahd3jKunJZarMSO.htm](pathfinder-bestiary-3-items/dahd3jKunJZarMSO.htm)|Frightful Presence|auto-trad|
|[DAwhqp01jQeK2AM2.htm](pathfinder-bestiary-3-items/DAwhqp01jQeK2AM2.htm)|Wolfstorm|auto-trad|
|[dbA4U6OCx4Nn4dOe.htm](pathfinder-bestiary-3-items/dbA4U6OCx4Nn4dOe.htm)|Seaweed Strand|auto-trad|
|[dBTUlLnCE86ceUMS.htm](pathfinder-bestiary-3-items/dBTUlLnCE86ceUMS.htm)|Low-Light Vision|auto-trad|
|[DbyMn4FBjCER4ikN.htm](pathfinder-bestiary-3-items/DbyMn4FBjCER4ikN.htm)|Dance of Destruction|auto-trad|
|[DCeikz6Fl0ryJW3v.htm](pathfinder-bestiary-3-items/DCeikz6Fl0ryJW3v.htm)|No Escape|auto-trad|
|[DCeV72u09INkg49k.htm](pathfinder-bestiary-3-items/DCeV72u09INkg49k.htm)|Trident|auto-trad|
|[DCLIjWlO5fWrsiw7.htm](pathfinder-bestiary-3-items/DCLIjWlO5fWrsiw7.htm)|Shining Blaze|auto-trad|
|[dd90VYwacTBPrupI.htm](pathfinder-bestiary-3-items/dd90VYwacTBPrupI.htm)|Grioth Venom|auto-trad|
|[DDcodZwZoVe2IQw6.htm](pathfinder-bestiary-3-items/DDcodZwZoVe2IQw6.htm)|Constant Spells|auto-trad|
|[dDtA4Al0gFgC9jck.htm](pathfinder-bestiary-3-items/dDtA4Al0gFgC9jck.htm)|Darkvision|auto-trad|
|[dEG5crSMOZ8MheG9.htm](pathfinder-bestiary-3-items/dEG5crSMOZ8MheG9.htm)|Darkvision|auto-trad|
|[DElua88KAjGxlJiI.htm](pathfinder-bestiary-3-items/DElua88KAjGxlJiI.htm)|Cold Adaptation|auto-trad|
|[deXYiHJyBjDSNFF5.htm](pathfinder-bestiary-3-items/deXYiHJyBjDSNFF5.htm)|Grasping Bites|auto-trad|
|[df6x7z2JqZ325lY2.htm](pathfinder-bestiary-3-items/df6x7z2JqZ325lY2.htm)|Bite|auto-trad|
|[DF8EHlGsG4hDnHIf.htm](pathfinder-bestiary-3-items/DF8EHlGsG4hDnHIf.htm)|Countered by Metal|auto-trad|
|[dGghVG22K6EF1IOo.htm](pathfinder-bestiary-3-items/dGghVG22K6EF1IOo.htm)|Twin Bites|auto-trad|
|[DHbKIfHAeaazMHgH.htm](pathfinder-bestiary-3-items/DHbKIfHAeaazMHgH.htm)|Occult Spontaneous Spells|auto-trad|
|[dHCnMadBsh7HsABT.htm](pathfinder-bestiary-3-items/dHCnMadBsh7HsABT.htm)|Polymorphic Appendage|auto-trad|
|[dhMEgZcYzpqH15gY.htm](pathfinder-bestiary-3-items/dhMEgZcYzpqH15gY.htm)|Chattering Teeth|auto-trad|
|[DhZVlrSd475Ljs1k.htm](pathfinder-bestiary-3-items/DhZVlrSd475Ljs1k.htm)|Telepathy 100 feet|auto-trad|
|[DJ5RgiOgDCBnlgH3.htm](pathfinder-bestiary-3-items/DJ5RgiOgDCBnlgH3.htm)|Low-Light Vision|auto-trad|
|[DjcXxE3rldN09uxu.htm](pathfinder-bestiary-3-items/DjcXxE3rldN09uxu.htm)|Divine Innate Spells|auto-trad|
|[DjRAEGwWJTNcjr6z.htm](pathfinder-bestiary-3-items/DjRAEGwWJTNcjr6z.htm)|Draining Blight|auto-trad|
|[dJT018LZV3FEJvJO.htm](pathfinder-bestiary-3-items/dJT018LZV3FEJvJO.htm)|Knockdown|auto-trad|
|[DkdUj7zo7tI7BCpF.htm](pathfinder-bestiary-3-items/DkdUj7zo7tI7BCpF.htm)|Jaws|auto-trad|
|[DKeP0NHGZX49S9dE.htm](pathfinder-bestiary-3-items/DKeP0NHGZX49S9dE.htm)|Grab|auto-trad|
|[DkRuMYAHXbZdXuqZ.htm](pathfinder-bestiary-3-items/DkRuMYAHXbZdXuqZ.htm)|Threatening Visage|auto-trad|
|[Dl0UbAiU402HXgL9.htm](pathfinder-bestiary-3-items/Dl0UbAiU402HXgL9.htm)|Occult Innate Spells|auto-trad|
|[DL9RtsdSy9Gulbqd.htm](pathfinder-bestiary-3-items/DL9RtsdSy9Gulbqd.htm)|Greatsword|auto-trad|
|[DLKqB3QVep4jRNGS.htm](pathfinder-bestiary-3-items/DLKqB3QVep4jRNGS.htm)|Truescript|auto-trad|
|[dLMAFMqNUxlmTN4X.htm](pathfinder-bestiary-3-items/dLMAFMqNUxlmTN4X.htm)|Dagger|auto-trad|
|[DNA5DHqZmcle3dXg.htm](pathfinder-bestiary-3-items/DNA5DHqZmcle3dXg.htm)|Shriek|auto-trad|
|[DNDY9jZtwm6GdoBP.htm](pathfinder-bestiary-3-items/DNDY9jZtwm6GdoBP.htm)|Improved Grab|auto-trad|
|[DNkqMNI50CBgRZbS.htm](pathfinder-bestiary-3-items/DNkqMNI50CBgRZbS.htm)|Ice Stride|auto-trad|
|[DNMBuy0G69bflxTr.htm](pathfinder-bestiary-3-items/DNMBuy0G69bflxTr.htm)|Tendril|auto-trad|
|[doTDspe65aJOvJFL.htm](pathfinder-bestiary-3-items/doTDspe65aJOvJFL.htm)|Telepathy 100 feet|auto-trad|
|[DotZCkt036LONE1t.htm](pathfinder-bestiary-3-items/DotZCkt036LONE1t.htm)|Claw|auto-trad|
|[DP0QpHqfJo5budhm.htm](pathfinder-bestiary-3-items/DP0QpHqfJo5budhm.htm)|Swallow Whole|auto-trad|
|[dp14tTXhxb0SZSZq.htm](pathfinder-bestiary-3-items/dp14tTXhxb0SZSZq.htm)|Project Echoblade|auto-trad|
|[DPf0kYMfi2748Y1L.htm](pathfinder-bestiary-3-items/DPf0kYMfi2748Y1L.htm)|Improved Grab|auto-trad|
|[Dpji6q4R6gQMLALe.htm](pathfinder-bestiary-3-items/Dpji6q4R6gQMLALe.htm)|Earthen Fist|auto-trad|
|[dpMfwwbltlZ55vSZ.htm](pathfinder-bestiary-3-items/dpMfwwbltlZ55vSZ.htm)|Final Blasphemy|auto-trad|
|[DpQ8esg6GOdnxHkt.htm](pathfinder-bestiary-3-items/DpQ8esg6GOdnxHkt.htm)|Claw|auto-trad|
|[DqllMZa6Ja60FhmJ.htm](pathfinder-bestiary-3-items/DqllMZa6Ja60FhmJ.htm)|Arcane Innate Spells|auto-trad|
|[dQW2DvGO2PSM5XBY.htm](pathfinder-bestiary-3-items/dQW2DvGO2PSM5XBY.htm)|Attack of Opportunity|auto-trad|
|[Dr1ESmEvHchNQHYd.htm](pathfinder-bestiary-3-items/Dr1ESmEvHchNQHYd.htm)|Grab|auto-trad|
|[DRfoCntho3tTjU8x.htm](pathfinder-bestiary-3-items/DRfoCntho3tTjU8x.htm)|Constrict|auto-trad|
|[DrR0GXOBDUWyiyEd.htm](pathfinder-bestiary-3-items/DrR0GXOBDUWyiyEd.htm)|Frightful Presence|auto-trad|
|[dRWLZGZySawLWgLz.htm](pathfinder-bestiary-3-items/dRWLZGZySawLWgLz.htm)|Winning Smile|auto-trad|
|[dS2bG5NB6KkMXMXH.htm](pathfinder-bestiary-3-items/dS2bG5NB6KkMXMXH.htm)|Jaws|auto-trad|
|[ds6T4axImpnQYao4.htm](pathfinder-bestiary-3-items/ds6T4axImpnQYao4.htm)|Break Ground|auto-trad|
|[DSDbtNz99LPKAHnV.htm](pathfinder-bestiary-3-items/DSDbtNz99LPKAHnV.htm)|Darkvision|auto-trad|
|[DSdWvRkowqLA8hig.htm](pathfinder-bestiary-3-items/DSdWvRkowqLA8hig.htm)|Occult Innate Spells|auto-trad|
|[dSIKXmZhpwoDtoh3.htm](pathfinder-bestiary-3-items/dSIKXmZhpwoDtoh3.htm)|Beak|auto-trad|
|[dStg3v3luFQKIonn.htm](pathfinder-bestiary-3-items/dStg3v3luFQKIonn.htm)|Darkvision|auto-trad|
|[DToLcWr6RKiExkcA.htm](pathfinder-bestiary-3-items/DToLcWr6RKiExkcA.htm)|Arcane Prepared Spells|auto-trad|
|[dUtAAFoA9eQnE0J5.htm](pathfinder-bestiary-3-items/dUtAAFoA9eQnE0J5.htm)|Disrupting Staff|auto-trad|
|[dvHLzTpiciD6p1Yb.htm](pathfinder-bestiary-3-items/dvHLzTpiciD6p1Yb.htm)|Astral Recoil|auto-trad|
|[dW7bkWhXvNcwfUCh.htm](pathfinder-bestiary-3-items/dW7bkWhXvNcwfUCh.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[DW8Cqi1XpUWPpQyd.htm](pathfinder-bestiary-3-items/DW8Cqi1XpUWPpQyd.htm)|Stinger|auto-trad|
|[dWa7XGI4buL9uDdZ.htm](pathfinder-bestiary-3-items/dWa7XGI4buL9uDdZ.htm)|Feed on Fear|auto-trad|
|[DwbOG3M8gGZfp6CI.htm](pathfinder-bestiary-3-items/DwbOG3M8gGZfp6CI.htm)|Pry|auto-trad|
|[dwH6nJY0Yb3I4jsA.htm](pathfinder-bestiary-3-items/dwH6nJY0Yb3I4jsA.htm)|Reposition|auto-trad|
|[dwi92HM5kiYhFz8W.htm](pathfinder-bestiary-3-items/dwi92HM5kiYhFz8W.htm)|Fist|auto-trad|
|[dwMummZWtYk7b2LJ.htm](pathfinder-bestiary-3-items/dwMummZWtYk7b2LJ.htm)|Final Jape|auto-trad|
|[dWphYFgVjZP3oKOy.htm](pathfinder-bestiary-3-items/dWphYFgVjZP3oKOy.htm)|Negative Healing|auto-trad|
|[dWtpFgBDu68MZFgx.htm](pathfinder-bestiary-3-items/dWtpFgBDu68MZFgx.htm)|Fist|auto-trad|
|[DWxhZZHfxKIg0dak.htm](pathfinder-bestiary-3-items/DWxhZZHfxKIg0dak.htm)|Fist|auto-trad|
|[dX486NKkzEeXKpLt.htm](pathfinder-bestiary-3-items/dX486NKkzEeXKpLt.htm)|Composite Longbow|auto-trad|
|[dXeWudwXB4aN3eTx.htm](pathfinder-bestiary-3-items/dXeWudwXB4aN3eTx.htm)|Primal Innate Spells|auto-trad|
|[dXTDQTPpz6jKxLYH.htm](pathfinder-bestiary-3-items/dXTDQTPpz6jKxLYH.htm)|Improved Grab|auto-trad|
|[Dy4ItmxMvWZNtvIJ.htm](pathfinder-bestiary-3-items/Dy4ItmxMvWZNtvIJ.htm)|Surface-Bound|auto-trad|
|[DY5764x6bU3jfnwu.htm](pathfinder-bestiary-3-items/DY5764x6bU3jfnwu.htm)|Slow|auto-trad|
|[dyHPOgwE2Bso99pQ.htm](pathfinder-bestiary-3-items/dyHPOgwE2Bso99pQ.htm)|Antler|auto-trad|
|[DYPC5uZCfiJo895S.htm](pathfinder-bestiary-3-items/DYPC5uZCfiJo895S.htm)|Manifest|auto-trad|
|[dyqAZBHswn1148hK.htm](pathfinder-bestiary-3-items/dyqAZBHswn1148hK.htm)|Knockdown|auto-trad|
|[dyrhkvCX2H3pZBp9.htm](pathfinder-bestiary-3-items/dyrhkvCX2H3pZBp9.htm)|Greater Darkvision|auto-trad|
|[dYuJ14oybPHNyAOW.htm](pathfinder-bestiary-3-items/dYuJ14oybPHNyAOW.htm)|Divine Prepared Spells|auto-trad|
|[dyWftDFNWQBORQLX.htm](pathfinder-bestiary-3-items/dyWftDFNWQBORQLX.htm)|Grab Weapon|auto-trad|
|[dZb5yXk5MS02kU1G.htm](pathfinder-bestiary-3-items/dZb5yXk5MS02kU1G.htm)|Brutal Blows|auto-trad|
|[DzdpbPovIBcXuiRi.htm](pathfinder-bestiary-3-items/DzdpbPovIBcXuiRi.htm)|Claw|auto-trad|
|[dzHyKmRIrVao4ke0.htm](pathfinder-bestiary-3-items/dzHyKmRIrVao4ke0.htm)|Darkvision|auto-trad|
|[E06rANySjyXiIjDH.htm](pathfinder-bestiary-3-items/E06rANySjyXiIjDH.htm)|Boiling Blood|auto-trad|
|[E1tUKsZc3ldzya9y.htm](pathfinder-bestiary-3-items/E1tUKsZc3ldzya9y.htm)|At-Will Spells|auto-trad|
|[e2fOY59UxAgnqdHF.htm](pathfinder-bestiary-3-items/e2fOY59UxAgnqdHF.htm)|Command Thrall|auto-trad|
|[e3kt6jZa3bspNiSF.htm](pathfinder-bestiary-3-items/e3kt6jZa3bspNiSF.htm)|Anchored Soul|auto-trad|
|[E3RAvTSsOhNonHz1.htm](pathfinder-bestiary-3-items/E3RAvTSsOhNonHz1.htm)|Constant Spells|auto-trad|
|[E3tmyHUT7v0BSgAP.htm](pathfinder-bestiary-3-items/E3tmyHUT7v0BSgAP.htm)|Greater Constrict|auto-trad|
|[e4gcYFnxnq6FmPws.htm](pathfinder-bestiary-3-items/e4gcYFnxnq6FmPws.htm)|Deadly Spark|auto-trad|
|[E4Nclckgv1bSz78W.htm](pathfinder-bestiary-3-items/E4Nclckgv1bSz78W.htm)|Blood Scent|auto-trad|
|[e4q0ijGMv88voluX.htm](pathfinder-bestiary-3-items/e4q0ijGMv88voluX.htm)|Grab|auto-trad|
|[E5jGyO2W780NMvYV.htm](pathfinder-bestiary-3-items/E5jGyO2W780NMvYV.htm)|Antler|auto-trad|
|[E6dBhZRBf4SXzGl7.htm](pathfinder-bestiary-3-items/E6dBhZRBf4SXzGl7.htm)|Resize Plant|auto-trad|
|[EAC12Ob2QRDH7V3R.htm](pathfinder-bestiary-3-items/EAC12Ob2QRDH7V3R.htm)|Breath Weapon|auto-trad|
|[EaFbQFfNsYFJLrSK.htm](pathfinder-bestiary-3-items/EaFbQFfNsYFJLrSK.htm)|Fangs|auto-trad|
|[eaYQr6jbgAf1fd2l.htm](pathfinder-bestiary-3-items/eaYQr6jbgAf1fd2l.htm)|Jaws|auto-trad|
|[eb3vh43tEB1z2vtJ.htm](pathfinder-bestiary-3-items/eb3vh43tEB1z2vtJ.htm)|At-Will Spells|auto-trad|
|[ebO4IFQGFnQQfnSq.htm](pathfinder-bestiary-3-items/ebO4IFQGFnQQfnSq.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[EbsFN4gbz3zpDwft.htm](pathfinder-bestiary-3-items/EbsFN4gbz3zpDwft.htm)|Telepathy 100 feet|auto-trad|
|[ebuEFfG89GEGr5RZ.htm](pathfinder-bestiary-3-items/ebuEFfG89GEGr5RZ.htm)|Telepathy 100 feet|auto-trad|
|[eBzYlEBFycddJLAk.htm](pathfinder-bestiary-3-items/eBzYlEBFycddJLAk.htm)|Hunt Prey|auto-trad|
|[ECl6ybDr5qUy0KF6.htm](pathfinder-bestiary-3-items/ECl6ybDr5qUy0KF6.htm)|Fed by Wood|auto-trad|
|[ECVcMDSSzJXSESDi.htm](pathfinder-bestiary-3-items/ECVcMDSSzJXSESDi.htm)|Claw|auto-trad|
|[eD9aNvigXSOpv46o.htm](pathfinder-bestiary-3-items/eD9aNvigXSOpv46o.htm)|Ultrasonic Pulse|auto-trad|
|[EDn6PkdwmLETtSlD.htm](pathfinder-bestiary-3-items/EDn6PkdwmLETtSlD.htm)|Phase Lurch|auto-trad|
|[edVyMro5viX5rgD9.htm](pathfinder-bestiary-3-items/edVyMro5viX5rgD9.htm)|Wind-Up|auto-trad|
|[EdX3IAMq8CXGNxOu.htm](pathfinder-bestiary-3-items/EdX3IAMq8CXGNxOu.htm)|Arcane Prepared Spells|auto-trad|
|[ee90NjJ1Ze4Ghwsk.htm](pathfinder-bestiary-3-items/ee90NjJ1Ze4Ghwsk.htm)|At-Will Spells|auto-trad|
|[EFidHtj7QBgjYsOb.htm](pathfinder-bestiary-3-items/EFidHtj7QBgjYsOb.htm)|Primal Innate Spells|auto-trad|
|[EFkfk6hbU9iqt1ja.htm](pathfinder-bestiary-3-items/EFkfk6hbU9iqt1ja.htm)|Frightful Presence|auto-trad|
|[EGyTn2mFKrYKKJvX.htm](pathfinder-bestiary-3-items/EGyTn2mFKrYKKJvX.htm)|Nature's Chosen|auto-trad|
|[EhA1NamwzAZrJMj2.htm](pathfinder-bestiary-3-items/EhA1NamwzAZrJMj2.htm)|Bite|auto-trad|
|[EHAajCEP6flvt7fx.htm](pathfinder-bestiary-3-items/EHAajCEP6flvt7fx.htm)|Lignifying Bite|auto-trad|
|[EhcLe86zdmbslOZG.htm](pathfinder-bestiary-3-items/EhcLe86zdmbslOZG.htm)|Wave|auto-trad|
|[ehlp9AWnS6V5OAuB.htm](pathfinder-bestiary-3-items/ehlp9AWnS6V5OAuB.htm)|Telepathy 100 feet|auto-trad|
|[EHsKUjzbFonBGt9N.htm](pathfinder-bestiary-3-items/EHsKUjzbFonBGt9N.htm)|Envenom|auto-trad|
|[eI8RLbyITDSsCGtu.htm](pathfinder-bestiary-3-items/eI8RLbyITDSsCGtu.htm)|Bound Spirits|auto-trad|
|[eiBZvzNac6ZHZQDi.htm](pathfinder-bestiary-3-items/eiBZvzNac6ZHZQDi.htm)|Hatchet|auto-trad|
|[EIopSecYGMv2HxGV.htm](pathfinder-bestiary-3-items/EIopSecYGMv2HxGV.htm)|Embed Quill|auto-trad|
|[eiuG3n39AiXSGDav.htm](pathfinder-bestiary-3-items/eiuG3n39AiXSGDav.htm)|Dimensional Pit|auto-trad|
|[EJ2Xgd9VGb4Rby3n.htm](pathfinder-bestiary-3-items/EJ2Xgd9VGb4Rby3n.htm)|Isolating Lash|auto-trad|
|[eJPyB6k2nbixpAxn.htm](pathfinder-bestiary-3-items/eJPyB6k2nbixpAxn.htm)|At-Will Spells|auto-trad|
|[EknlSoHchVPvAGPA.htm](pathfinder-bestiary-3-items/EknlSoHchVPvAGPA.htm)|Grasping Bites|auto-trad|
|[eKQzWhQFHFwAwUYf.htm](pathfinder-bestiary-3-items/eKQzWhQFHFwAwUYf.htm)|Perfected Flight|auto-trad|
|[EKRq00ExnMsBTNP4.htm](pathfinder-bestiary-3-items/EKRq00ExnMsBTNP4.htm)|Hurtful Critique|auto-trad|
|[EkUXODIcewWOk6qH.htm](pathfinder-bestiary-3-items/EkUXODIcewWOk6qH.htm)|Freeze Shell|auto-trad|
|[elcu7DQCtC0dX7Q4.htm](pathfinder-bestiary-3-items/elcu7DQCtC0dX7Q4.htm)|Negative Healing|auto-trad|
|[eLKeINPUnuoZeD9D.htm](pathfinder-bestiary-3-items/eLKeINPUnuoZeD9D.htm)|Swarm Mind|auto-trad|
|[ELmguzDIW19zBvG2.htm](pathfinder-bestiary-3-items/ELmguzDIW19zBvG2.htm)|Constant Spells|auto-trad|
|[emdMVqKxpviFTtN7.htm](pathfinder-bestiary-3-items/emdMVqKxpviFTtN7.htm)|Regeneration 40 (Deactivated by Chaotic, Mental or Orichalcum)|auto-trad|
|[eMH8aLOsO2dW2SPL.htm](pathfinder-bestiary-3-items/eMH8aLOsO2dW2SPL.htm)|Low-Light Vision|auto-trad|
|[eNkQHj53Zq1tG2jD.htm](pathfinder-bestiary-3-items/eNkQHj53Zq1tG2jD.htm)|Eclipse|auto-trad|
|[ENpP2o2H3ia9c6NJ.htm](pathfinder-bestiary-3-items/ENpP2o2H3ia9c6NJ.htm)|Surreal Anatomy|auto-trad|
|[ENxpJknQm2f0aGri.htm](pathfinder-bestiary-3-items/ENxpJknQm2f0aGri.htm)|Speak with Arthropods|auto-trad|
|[eoGlb3oqWZjUaNDi.htm](pathfinder-bestiary-3-items/eoGlb3oqWZjUaNDi.htm)|Telepathy 100 feet|auto-trad|
|[eogxSxYx5HC0X9Tc.htm](pathfinder-bestiary-3-items/eogxSxYx5HC0X9Tc.htm)|Jaws|auto-trad|
|[EoRz5vyqTCn8pIIk.htm](pathfinder-bestiary-3-items/EoRz5vyqTCn8pIIk.htm)|Low-Light Vision|auto-trad|
|[EPLy8QbtiSRbEEme.htm](pathfinder-bestiary-3-items/EPLy8QbtiSRbEEme.htm)|Swarm Mind|auto-trad|
|[eQjA75mkF3WdkNS8.htm](pathfinder-bestiary-3-items/eQjA75mkF3WdkNS8.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[eqx6f68rVSfojXpJ.htm](pathfinder-bestiary-3-items/eqx6f68rVSfojXpJ.htm)|Hyponatremia|auto-trad|
|[erIKrXEzR8ADuFq8.htm](pathfinder-bestiary-3-items/erIKrXEzR8ADuFq8.htm)|Tail|auto-trad|
|[ERS9rhuIGUTjxi8H.htm](pathfinder-bestiary-3-items/ERS9rhuIGUTjxi8H.htm)|At-Will Spells|auto-trad|
|[ET0tToniC9uHpsbM.htm](pathfinder-bestiary-3-items/ET0tToniC9uHpsbM.htm)|Attack of Opportunity|auto-trad|
|[eT3PtWf4uiMjHI7j.htm](pathfinder-bestiary-3-items/eT3PtWf4uiMjHI7j.htm)|Perfected Flight|auto-trad|
|[EtHDwrRNzkfZ50vn.htm](pathfinder-bestiary-3-items/EtHDwrRNzkfZ50vn.htm)|Forsaken Patron|auto-trad|
|[eTWugVatqxe8rLdZ.htm](pathfinder-bestiary-3-items/eTWugVatqxe8rLdZ.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[ETZ0a2GwlpGrMxWM.htm](pathfinder-bestiary-3-items/ETZ0a2GwlpGrMxWM.htm)|Rock|auto-trad|
|[eU85Cgnz1ezRDCdF.htm](pathfinder-bestiary-3-items/eU85Cgnz1ezRDCdF.htm)|Hatchet|auto-trad|
|[EuQQ73V8hJuBn5EU.htm](pathfinder-bestiary-3-items/EuQQ73V8hJuBn5EU.htm)|Grab Item|auto-trad|
|[EViJHho1D7U93moQ.htm](pathfinder-bestiary-3-items/EViJHho1D7U93moQ.htm)|Smoke Vision|auto-trad|
|[eVqb0828602ykLqp.htm](pathfinder-bestiary-3-items/eVqb0828602ykLqp.htm)|Head Bowl|auto-trad|
|[Ew8oLYahqyGEluxo.htm](pathfinder-bestiary-3-items/Ew8oLYahqyGEluxo.htm)|Greater Constrict|auto-trad|
|[eWeqt47GwgkuwIQ8.htm](pathfinder-bestiary-3-items/eWeqt47GwgkuwIQ8.htm)|At-Will Spells|auto-trad|
|[eWV9eGMG3Ss6ZE78.htm](pathfinder-bestiary-3-items/eWV9eGMG3Ss6ZE78.htm)|+4 Status to All Saves vs. Mental or Divine|auto-trad|
|[EWX813DLZAePlSAY.htm](pathfinder-bestiary-3-items/EWX813DLZAePlSAY.htm)|Jaws|auto-trad|
|[EXdI3guudVr8XFFO.htm](pathfinder-bestiary-3-items/EXdI3guudVr8XFFO.htm)|Improved Knockdown|auto-trad|
|[eY479IPS7BQvkOTF.htm](pathfinder-bestiary-3-items/eY479IPS7BQvkOTF.htm)|Love's Impunity|auto-trad|
|[EybKkcXQpisZym7b.htm](pathfinder-bestiary-3-items/EybKkcXQpisZym7b.htm)|Claim Trophy|auto-trad|
|[eYkYSZX390lDUFFK.htm](pathfinder-bestiary-3-items/eYkYSZX390lDUFFK.htm)|Pounce|auto-trad|
|[EYLtQQcjxfjr6MNe.htm](pathfinder-bestiary-3-items/EYLtQQcjxfjr6MNe.htm)|Regeneration (50) (Deactivated by Good, Mental, Orichalcum)|auto-trad|
|[eZ5EIf8w0pc83e2W.htm](pathfinder-bestiary-3-items/eZ5EIf8w0pc83e2W.htm)|Shameful Loathing|auto-trad|
|[EZESCdF1dzv8z0rc.htm](pathfinder-bestiary-3-items/EZESCdF1dzv8z0rc.htm)|Darkvision|auto-trad|
|[EzfpvwnxObWcM5rn.htm](pathfinder-bestiary-3-items/EzfpvwnxObWcM5rn.htm)|Carver's Curse|auto-trad|
|[eZiRahkdCT1SRMx1.htm](pathfinder-bestiary-3-items/eZiRahkdCT1SRMx1.htm)|Rock|auto-trad|
|[EzVhFWQhs6axH99B.htm](pathfinder-bestiary-3-items/EzVhFWQhs6axH99B.htm)|Spear|auto-trad|
|[F0RaqdAbEfogkX2c.htm](pathfinder-bestiary-3-items/F0RaqdAbEfogkX2c.htm)|Fan Bolt|auto-trad|
|[F0su6gl7ieMAuCn8.htm](pathfinder-bestiary-3-items/F0su6gl7ieMAuCn8.htm)|Raise Guard|auto-trad|
|[f0YkO5SMtEfB3r39.htm](pathfinder-bestiary-3-items/f0YkO5SMtEfB3r39.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[F1BUx1q2oTY8sFpj.htm](pathfinder-bestiary-3-items/F1BUx1q2oTY8sFpj.htm)|Clutching Cold|auto-trad|
|[f4MFrfJdBTJTN23S.htm](pathfinder-bestiary-3-items/f4MFrfJdBTJTN23S.htm)|Command Thrall|auto-trad|
|[f4Mkjn4uAW6nabJR.htm](pathfinder-bestiary-3-items/f4Mkjn4uAW6nabJR.htm)|Rearrange Possessions|auto-trad|
|[F5nrBH7jkTIQsJqp.htm](pathfinder-bestiary-3-items/F5nrBH7jkTIQsJqp.htm)|Spirit Body|auto-trad|
|[f661MaVKOhxZZMUI.htm](pathfinder-bestiary-3-items/f661MaVKOhxZZMUI.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[f6S96FxE504I9tBq.htm](pathfinder-bestiary-3-items/f6S96FxE504I9tBq.htm)|Lifesense 120 feet|auto-trad|
|[f6v0xX9B6TCjk14f.htm](pathfinder-bestiary-3-items/f6v0xX9B6TCjk14f.htm)|Arcane Prepared Spells|auto-trad|
|[f714vqweH1u5q1ZG.htm](pathfinder-bestiary-3-items/f714vqweH1u5q1ZG.htm)|Low-Light Vision|auto-trad|
|[F7XOjtJspPrYwCzy.htm](pathfinder-bestiary-3-items/F7XOjtJspPrYwCzy.htm)|Darkvision|auto-trad|
|[f8ucclfLXdX89h8k.htm](pathfinder-bestiary-3-items/f8ucclfLXdX89h8k.htm)|Occult Innate Spells|auto-trad|
|[F98RBByZ9IcrIL7x.htm](pathfinder-bestiary-3-items/F98RBByZ9IcrIL7x.htm)|Occult Innate Spells|auto-trad|
|[f9gqhbFaPyj77i0e.htm](pathfinder-bestiary-3-items/f9gqhbFaPyj77i0e.htm)|Light Hammer|auto-trad|
|[F9hcxqTd9pv3rmpZ.htm](pathfinder-bestiary-3-items/F9hcxqTd9pv3rmpZ.htm)|Skip Between|auto-trad|
|[F9OvUrIH2XqsqnGm.htm](pathfinder-bestiary-3-items/F9OvUrIH2XqsqnGm.htm)|Root|auto-trad|
|[f9xJOejUBSfPwnLt.htm](pathfinder-bestiary-3-items/f9xJOejUBSfPwnLt.htm)|Draconic Momentum|auto-trad|
|[F9z5eqsEuY9xP7v7.htm](pathfinder-bestiary-3-items/F9z5eqsEuY9xP7v7.htm)|Frightful Presence|auto-trad|
|[FAGZiwfUhhTFHxxn.htm](pathfinder-bestiary-3-items/FAGZiwfUhhTFHxxn.htm)|Claw|auto-trad|
|[faqkx0QvzDNLMR1M.htm](pathfinder-bestiary-3-items/faqkx0QvzDNLMR1M.htm)|Constant Spells|auto-trad|
|[fAtIew8TlThwaKzS.htm](pathfinder-bestiary-3-items/fAtIew8TlThwaKzS.htm)|Dagger|auto-trad|
|[fbcWIR3VTHaRmvLw.htm](pathfinder-bestiary-3-items/fbcWIR3VTHaRmvLw.htm)|Swarming Slither|auto-trad|
|[fcF7yPdFg6xTRRlu.htm](pathfinder-bestiary-3-items/fcF7yPdFg6xTRRlu.htm)|Hunter's Triumph|auto-trad|
|[fCSVGUKsmrepwWjP.htm](pathfinder-bestiary-3-items/fCSVGUKsmrepwWjP.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[fcx7u3nEY1i0o8JF.htm](pathfinder-bestiary-3-items/fcx7u3nEY1i0o8JF.htm)|Pungent|auto-trad|
|[fDBEEgdrhbSzqYYM.htm](pathfinder-bestiary-3-items/fDBEEgdrhbSzqYYM.htm)|Delicious|auto-trad|
|[fDOGJqfBeWIT7IOl.htm](pathfinder-bestiary-3-items/fDOGJqfBeWIT7IOl.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[fE3RaY5ixs8crh6p.htm](pathfinder-bestiary-3-items/fE3RaY5ixs8crh6p.htm)|Divine Innate Spells|auto-trad|
|[FeIulhzHCcKLFyNH.htm](pathfinder-bestiary-3-items/FeIulhzHCcKLFyNH.htm)|Claw|auto-trad|
|[FEmYP0SV4WLWDH9k.htm](pathfinder-bestiary-3-items/FEmYP0SV4WLWDH9k.htm)|Divine Spontaneous Spells|auto-trad|
|[fFOSRHY0ACjbHmk0.htm](pathfinder-bestiary-3-items/fFOSRHY0ACjbHmk0.htm)|Strix Camaraderie|auto-trad|
|[fg1os9FHkiVGPkuh.htm](pathfinder-bestiary-3-items/fg1os9FHkiVGPkuh.htm)|Limited Immortality|auto-trad|
|[FGcmZG66TcZc2r8o.htm](pathfinder-bestiary-3-items/FGcmZG66TcZc2r8o.htm)|Constrict|auto-trad|
|[fgDDQdQN0mgFgxVm.htm](pathfinder-bestiary-3-items/fgDDQdQN0mgFgxVm.htm)|Raccoon's Whimsy|auto-trad|
|[Fgyso5whxxzTMpVE.htm](pathfinder-bestiary-3-items/Fgyso5whxxzTMpVE.htm)|Divine Innate Spells|auto-trad|
|[fH4zN6EufktPqbW4.htm](pathfinder-bestiary-3-items/fH4zN6EufktPqbW4.htm)|Clockwork Wand|auto-trad|
|[FH98KC2pUcRXdN35.htm](pathfinder-bestiary-3-items/FH98KC2pUcRXdN35.htm)|Divine Innate Spells|auto-trad|
|[FHCDHU9Avy8uYlDu.htm](pathfinder-bestiary-3-items/FHCDHU9Avy8uYlDu.htm)|Telepathy 60 feet|auto-trad|
|[fhIh70SWeQm18cfv.htm](pathfinder-bestiary-3-items/fhIh70SWeQm18cfv.htm)|Low-Light Vision|auto-trad|
|[fhK47KtCqw1hqre0.htm](pathfinder-bestiary-3-items/fhK47KtCqw1hqre0.htm)|At-Will Spells|auto-trad|
|[FHVxImDGF7MB4tzs.htm](pathfinder-bestiary-3-items/FHVxImDGF7MB4tzs.htm)|Occult Innate Spells|auto-trad|
|[FHWDKbm2r5ltPmmK.htm](pathfinder-bestiary-3-items/FHWDKbm2r5ltPmmK.htm)|Greater Darkvision|auto-trad|
|[Fil9NBsfO3iADreb.htm](pathfinder-bestiary-3-items/Fil9NBsfO3iADreb.htm)|Sandstorm|auto-trad|
|[FJEaQqHttC6Z3MP3.htm](pathfinder-bestiary-3-items/FJEaQqHttC6Z3MP3.htm)|Sneak Attack|auto-trad|
|[FJGG1xzdcXWDtz4x.htm](pathfinder-bestiary-3-items/FJGG1xzdcXWDtz4x.htm)|Bard Composition Spells|auto-trad|
|[fJlI13pn1Y8ntWFP.htm](pathfinder-bestiary-3-items/fJlI13pn1Y8ntWFP.htm)|Heartless Furnace|auto-trad|
|[Fk0okw5jh318HfC0.htm](pathfinder-bestiary-3-items/Fk0okw5jh318HfC0.htm)|Boundless Reach|auto-trad|
|[Fk6KpPG5r84Cxf3v.htm](pathfinder-bestiary-3-items/Fk6KpPG5r84Cxf3v.htm)|Constant Spells|auto-trad|
|[fKdYArB4XGbeZO0e.htm](pathfinder-bestiary-3-items/fKdYArB4XGbeZO0e.htm)|Staff|auto-trad|
|[FkGm83vCSFYej5yy.htm](pathfinder-bestiary-3-items/FkGm83vCSFYej5yy.htm)|Darkvision|auto-trad|
|[FkwYpWuwVTFS02Jn.htm](pathfinder-bestiary-3-items/FkwYpWuwVTFS02Jn.htm)|All-Around Vision|auto-trad|
|[fLCQ4ieE5VzQA9NP.htm](pathfinder-bestiary-3-items/fLCQ4ieE5VzQA9NP.htm)|Borrowed Skin|auto-trad|
|[flo5oC8zWtFBUsvt.htm](pathfinder-bestiary-3-items/flo5oC8zWtFBUsvt.htm)|Jaws|auto-trad|
|[fLXcaCWjl1dAjO33.htm](pathfinder-bestiary-3-items/fLXcaCWjl1dAjO33.htm)|Bite|auto-trad|
|[FLY1h37SM1s6HQ3K.htm](pathfinder-bestiary-3-items/FLY1h37SM1s6HQ3K.htm)|Web Trap|auto-trad|
|[FmabDHqtzgl9JIl4.htm](pathfinder-bestiary-3-items/FmabDHqtzgl9JIl4.htm)|Jaws|auto-trad|
|[fmxx7smV9KEkfRN8.htm](pathfinder-bestiary-3-items/fmxx7smV9KEkfRN8.htm)|Isolate Foes|auto-trad|
|[fN2P498vq870hq1W.htm](pathfinder-bestiary-3-items/fN2P498vq870hq1W.htm)|All-Around Vision|auto-trad|
|[FnIxsS3RE81lmBvi.htm](pathfinder-bestiary-3-items/FnIxsS3RE81lmBvi.htm)|Kukri|auto-trad|
|[fNOL0XY0wCqc5rUq.htm](pathfinder-bestiary-3-items/fNOL0XY0wCqc5rUq.htm)|+3 Status to All Saves vs. Divine Magic|auto-trad|
|[fOaemwLydAbI9Tdb.htm](pathfinder-bestiary-3-items/fOaemwLydAbI9Tdb.htm)|Grab|auto-trad|
|[FodKNeEvBm6FqwvE.htm](pathfinder-bestiary-3-items/FodKNeEvBm6FqwvE.htm)|Impaling Charge|auto-trad|
|[foFqU5DE5kDifheg.htm](pathfinder-bestiary-3-items/foFqU5DE5kDifheg.htm)|Smoke Vision|auto-trad|
|[foR7mgXEL0XMHJ6J.htm](pathfinder-bestiary-3-items/foR7mgXEL0XMHJ6J.htm)|Flurry of Pods|auto-trad|
|[fp6MDEvJKxcE7hE3.htm](pathfinder-bestiary-3-items/fp6MDEvJKxcE7hE3.htm)|Low-Light Vision|auto-trad|
|[FPfOOgSVW55hbKb0.htm](pathfinder-bestiary-3-items/FPfOOgSVW55hbKb0.htm)|Splatter|auto-trad|
|[fPZelv8NqLRyuQ1p.htm](pathfinder-bestiary-3-items/fPZelv8NqLRyuQ1p.htm)|Divine Innate Spells|auto-trad|
|[fQAp6FTjVozJr8XU.htm](pathfinder-bestiary-3-items/fQAp6FTjVozJr8XU.htm)|Attack of Opportunity (Special)|auto-trad|
|[fQuNqFErQHkBJLE8.htm](pathfinder-bestiary-3-items/fQuNqFErQHkBJLE8.htm)|Claw|auto-trad|
|[FqY5LFjb311dDEv1.htm](pathfinder-bestiary-3-items/FqY5LFjb311dDEv1.htm)|Darkvision|auto-trad|
|[FR0NE1tkGwNuwmOZ.htm](pathfinder-bestiary-3-items/FR0NE1tkGwNuwmOZ.htm)|Improved Grab|auto-trad|
|[fr7zLtKzGQVPqqKL.htm](pathfinder-bestiary-3-items/fr7zLtKzGQVPqqKL.htm)|Spit|auto-trad|
|[fRSODlNG63LciyB1.htm](pathfinder-bestiary-3-items/fRSODlNG63LciyB1.htm)|Constant Spells|auto-trad|
|[fRYKciZWxC0TiEcw.htm](pathfinder-bestiary-3-items/fRYKciZWxC0TiEcw.htm)|Darkvision|auto-trad|
|[fSDTMDmYkmPrgbRM.htm](pathfinder-bestiary-3-items/fSDTMDmYkmPrgbRM.htm)|Occult Innate Spells|auto-trad|
|[fSFM5tsKzCAUyEVt.htm](pathfinder-bestiary-3-items/fSFM5tsKzCAUyEVt.htm)|Sneak Attack|auto-trad|
|[FsndhUfvxvaK2vMV.htm](pathfinder-bestiary-3-items/FsndhUfvxvaK2vMV.htm)|Grab|auto-trad|
|[FTFBPXIVH7AJW2HL.htm](pathfinder-bestiary-3-items/FTFBPXIVH7AJW2HL.htm)|Rejuvenation|auto-trad|
|[FtnpYBZenPEqBKZT.htm](pathfinder-bestiary-3-items/FtnpYBZenPEqBKZT.htm)|Vishkanyan Venom|auto-trad|
|[FV9xFHPHkv8KNSub.htm](pathfinder-bestiary-3-items/FV9xFHPHkv8KNSub.htm)|Inspire Envoy|auto-trad|
|[fv9ZIBX07mdzmfwt.htm](pathfinder-bestiary-3-items/fv9ZIBX07mdzmfwt.htm)|Change Shape|auto-trad|
|[fvbnbBzVNf3FpASj.htm](pathfinder-bestiary-3-items/fvbnbBzVNf3FpASj.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[FVNiKshCldd6k244.htm](pathfinder-bestiary-3-items/FVNiKshCldd6k244.htm)|Hand of Despair|auto-trad|
|[fvVvVEF0pK4ATbmC.htm](pathfinder-bestiary-3-items/fvVvVEF0pK4ATbmC.htm)|Luck Osmosis|auto-trad|
|[FWr6Ex98OyC3DSds.htm](pathfinder-bestiary-3-items/FWr6Ex98OyC3DSds.htm)|At-Will Spells|auto-trad|
|[fXjJYcC6HHWIClZe.htm](pathfinder-bestiary-3-items/fXjJYcC6HHWIClZe.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[fXKjy8wUG1Jgkyga.htm](pathfinder-bestiary-3-items/fXKjy8wUG1Jgkyga.htm)|Inspiration|auto-trad|
|[FYOsSxnsDAEzO8SZ.htm](pathfinder-bestiary-3-items/FYOsSxnsDAEzO8SZ.htm)|Claw|auto-trad|
|[fYpLZdqHoH2EpmhI.htm](pathfinder-bestiary-3-items/fYpLZdqHoH2EpmhI.htm)|Swift Steps|auto-trad|
|[FYQBhz6UyHtF4h3Q.htm](pathfinder-bestiary-3-items/FYQBhz6UyHtF4h3Q.htm)|Claws|auto-trad|
|[FyrrgVjaMyZCtRfk.htm](pathfinder-bestiary-3-items/FyrrgVjaMyZCtRfk.htm)|Darkvision|auto-trad|
|[fysOeh3zCgDpX1IC.htm](pathfinder-bestiary-3-items/fysOeh3zCgDpX1IC.htm)|Antler|auto-trad|
|[Fz5eA42qzHRa526p.htm](pathfinder-bestiary-3-items/Fz5eA42qzHRa526p.htm)|Death Gasp|auto-trad|
|[fzjCg0wFBhRYlKEh.htm](pathfinder-bestiary-3-items/fzjCg0wFBhRYlKEh.htm)|Dragon's Salvation|auto-trad|
|[fzkFHUIw06YZhQU3.htm](pathfinder-bestiary-3-items/fzkFHUIw06YZhQU3.htm)|Greater Darkvision|auto-trad|
|[fzv99kNrktzJJd55.htm](pathfinder-bestiary-3-items/fzv99kNrktzJJd55.htm)|Overpowering Healing|auto-trad|
|[g078MCPSCGCFoMNJ.htm](pathfinder-bestiary-3-items/g078MCPSCGCFoMNJ.htm)|Occult Innate Spells|auto-trad|
|[g2ws0uSei8FpMGsN.htm](pathfinder-bestiary-3-items/g2ws0uSei8FpMGsN.htm)|Negative Healing|auto-trad|
|[g4EAOBnH3VEiBpbj.htm](pathfinder-bestiary-3-items/g4EAOBnH3VEiBpbj.htm)|Divine Focus Spells|auto-trad|
|[G4lup29lJteGQEx7.htm](pathfinder-bestiary-3-items/G4lup29lJteGQEx7.htm)|Divine Prepared Spells|auto-trad|
|[g4OWJGRw7uHmBmrk.htm](pathfinder-bestiary-3-items/g4OWJGRw7uHmBmrk.htm)|Axe Vulnerability|auto-trad|
|[g5jByzHTDqydOMis.htm](pathfinder-bestiary-3-items/g5jByzHTDqydOMis.htm)|Relentless|auto-trad|
|[G5wnFqnf2owuuqKY.htm](pathfinder-bestiary-3-items/G5wnFqnf2owuuqKY.htm)|Bonded Vessel|auto-trad|
|[g5zUhW3HyLCDaXR1.htm](pathfinder-bestiary-3-items/g5zUhW3HyLCDaXR1.htm)|Fist|auto-trad|
|[g6t9K95If99AfQfM.htm](pathfinder-bestiary-3-items/g6t9K95If99AfQfM.htm)|Change Shape|auto-trad|
|[g7u85rySCuCvovJ1.htm](pathfinder-bestiary-3-items/g7u85rySCuCvovJ1.htm)|Tail|auto-trad|
|[G9pbNjrUbv6UwBPm.htm](pathfinder-bestiary-3-items/G9pbNjrUbv6UwBPm.htm)|Feed on Quintessence|auto-trad|
|[ga5JXiw8tZ2VnkLB.htm](pathfinder-bestiary-3-items/ga5JXiw8tZ2VnkLB.htm)|Low-Light Vision|auto-trad|
|[gaAm3rT0oo2DoWFh.htm](pathfinder-bestiary-3-items/gaAm3rT0oo2DoWFh.htm)|Nymph's Beauty|auto-trad|
|[gacO7J9Bw3Sc7Hnf.htm](pathfinder-bestiary-3-items/gacO7J9Bw3Sc7Hnf.htm)|Hop On|auto-trad|
|[gbeM9uQuXvgenm6R.htm](pathfinder-bestiary-3-items/gbeM9uQuXvgenm6R.htm)|At-Will Spells|auto-trad|
|[gbGFrdjx00klWwUk.htm](pathfinder-bestiary-3-items/gbGFrdjx00klWwUk.htm)|Coven|auto-trad|
|[GBOycFh1m1Y5Jb9h.htm](pathfinder-bestiary-3-items/GBOycFh1m1Y5Jb9h.htm)|Change Shape|auto-trad|
|[gbX1GThYkUe0eSAF.htm](pathfinder-bestiary-3-items/gbX1GThYkUe0eSAF.htm)|Darkvision|auto-trad|
|[GC4dnaKqE0PPSLwc.htm](pathfinder-bestiary-3-items/GC4dnaKqE0PPSLwc.htm)|Blinding Spittle|auto-trad|
|[gcmEV9uoqo5w9B3u.htm](pathfinder-bestiary-3-items/gcmEV9uoqo5w9B3u.htm)|Throw Rock|auto-trad|
|[gdbnGMdSRW0Nzev8.htm](pathfinder-bestiary-3-items/gdbnGMdSRW0Nzev8.htm)|Spit|auto-trad|
|[GdnX1i87yKmvFsjB.htm](pathfinder-bestiary-3-items/GdnX1i87yKmvFsjB.htm)|Fume|auto-trad|
|[GDsko6VNw9V3q3Fn.htm](pathfinder-bestiary-3-items/GDsko6VNw9V3q3Fn.htm)|Nature's Chosen|auto-trad|
|[GeAdyeLnGrb8jf9c.htm](pathfinder-bestiary-3-items/GeAdyeLnGrb8jf9c.htm)|Coiling Frenzy|auto-trad|
|[geu1krmMeuI55Tx7.htm](pathfinder-bestiary-3-items/geu1krmMeuI55Tx7.htm)|At-Will Spells|auto-trad|
|[GFFvPm7EbrqTqDNh.htm](pathfinder-bestiary-3-items/GFFvPm7EbrqTqDNh.htm)|Borer|auto-trad|
|[GfHEx2NMpfuQjhkS.htm](pathfinder-bestiary-3-items/GfHEx2NMpfuQjhkS.htm)|Divine Innate Spells|auto-trad|
|[GFv0unGRcJq78oa1.htm](pathfinder-bestiary-3-items/GFv0unGRcJq78oa1.htm)|Negative Healing|auto-trad|
|[gGHq0sBBHPVuZhDJ.htm](pathfinder-bestiary-3-items/gGHq0sBBHPVuZhDJ.htm)|Constrict|auto-trad|
|[gGpB7QLHdHm3LW5f.htm](pathfinder-bestiary-3-items/gGpB7QLHdHm3LW5f.htm)|Pufferfish Venom|auto-trad|
|[ggqzAen8OXMFadmR.htm](pathfinder-bestiary-3-items/ggqzAen8OXMFadmR.htm)|Low-Light Vision|auto-trad|
|[Gh301Rw6jmBr5P8s.htm](pathfinder-bestiary-3-items/Gh301Rw6jmBr5P8s.htm)|Putrid Vomit|auto-trad|
|[ghavKYYcFvQxKQ3n.htm](pathfinder-bestiary-3-items/ghavKYYcFvQxKQ3n.htm)|Font of Death|auto-trad|
|[gHkt9ZPmla4aDS6c.htm](pathfinder-bestiary-3-items/gHkt9ZPmla4aDS6c.htm)|Telepathy|auto-trad|
|[GhM8SkeNK34K4KuM.htm](pathfinder-bestiary-3-items/GhM8SkeNK34K4KuM.htm)|Extend Neck|auto-trad|
|[gi7A2YM0065rtCEt.htm](pathfinder-bestiary-3-items/gi7A2YM0065rtCEt.htm)|Horn|auto-trad|
|[gIPLLT7gHYLZoTe5.htm](pathfinder-bestiary-3-items/gIPLLT7gHYLZoTe5.htm)|Moon Frenzy|auto-trad|
|[Gj7A4LTikw9f1cDC.htm](pathfinder-bestiary-3-items/Gj7A4LTikw9f1cDC.htm)|Jaws|auto-trad|
|[gJ7hdDvMBTlevMyx.htm](pathfinder-bestiary-3-items/gJ7hdDvMBTlevMyx.htm)|Maw|auto-trad|
|[gk6ccJsfW8SnQ1XT.htm](pathfinder-bestiary-3-items/gk6ccJsfW8SnQ1XT.htm)|Limited Flight|auto-trad|
|[gKE1dU3eNHT5VhmQ.htm](pathfinder-bestiary-3-items/gKE1dU3eNHT5VhmQ.htm)|Darkvision|auto-trad|
|[GkEh5ET2hVXw3FKO.htm](pathfinder-bestiary-3-items/GkEh5ET2hVXw3FKO.htm)|Darkvision|auto-trad|
|[gKhFLhWvvDGY1Xbf.htm](pathfinder-bestiary-3-items/gKhFLhWvvDGY1Xbf.htm)|Trident|auto-trad|
|[GKPDyYkNMI6xU6fY.htm](pathfinder-bestiary-3-items/GKPDyYkNMI6xU6fY.htm)|Trample|auto-trad|
|[gKuOYzPHRi2J8prN.htm](pathfinder-bestiary-3-items/gKuOYzPHRi2J8prN.htm)|Woodland Stride|auto-trad|
|[gKxdmvPJZYi5W7YT.htm](pathfinder-bestiary-3-items/gKxdmvPJZYi5W7YT.htm)|Vicious Strafe|auto-trad|
|[gLEu7XWn7pQSVc6w.htm](pathfinder-bestiary-3-items/gLEu7XWn7pQSVc6w.htm)|Occult Innate Spells|auto-trad|
|[gLK9FYslyozpAyWH.htm](pathfinder-bestiary-3-items/gLK9FYslyozpAyWH.htm)|Draconic Frenzy|auto-trad|
|[gMnuejIYSlD0wrxg.htm](pathfinder-bestiary-3-items/gMnuejIYSlD0wrxg.htm)|Darkvision|auto-trad|
|[gmOQkMoumZBsdfpn.htm](pathfinder-bestiary-3-items/gmOQkMoumZBsdfpn.htm)|Jaws|auto-trad|
|[GNmpOlkmA2KqPmhK.htm](pathfinder-bestiary-3-items/GNmpOlkmA2KqPmhK.htm)|Darkvision|auto-trad|
|[gNwlqfCtYRIjrH6e.htm](pathfinder-bestiary-3-items/gNwlqfCtYRIjrH6e.htm)|Fed by Metal|auto-trad|
|[go13nXjPXGhj1YCQ.htm](pathfinder-bestiary-3-items/go13nXjPXGhj1YCQ.htm)|Low-Light Vision|auto-trad|
|[GOF0Ud2jC782jqa5.htm](pathfinder-bestiary-3-items/GOF0Ud2jC782jqa5.htm)|Lore Master|auto-trad|
|[gOiaDjo5WK8ESJHN.htm](pathfinder-bestiary-3-items/gOiaDjo5WK8ESJHN.htm)|Divine Innate Spells|auto-trad|
|[gokPql4rMjZsUmFa.htm](pathfinder-bestiary-3-items/gokPql4rMjZsUmFa.htm)|Claw|auto-trad|
|[GOr0VDbwyouZE0as.htm](pathfinder-bestiary-3-items/GOr0VDbwyouZE0as.htm)|Guardian's Aegis|auto-trad|
|[GosHB6QvTUOTDIVs.htm](pathfinder-bestiary-3-items/GosHB6QvTUOTDIVs.htm)|Hurl Corpse|auto-trad|
|[gPaWa8Zkt9jBAg2K.htm](pathfinder-bestiary-3-items/gPaWa8Zkt9jBAg2K.htm)|Forge Jewelry|auto-trad|
|[GPeTASJke20Pg69M.htm](pathfinder-bestiary-3-items/GPeTASJke20Pg69M.htm)|Negative Healing|auto-trad|
|[GpjM5EevUR9J0dQv.htm](pathfinder-bestiary-3-items/GpjM5EevUR9J0dQv.htm)|Viscous Choke|auto-trad|
|[gpsZOiwpAEi2TKTL.htm](pathfinder-bestiary-3-items/gpsZOiwpAEi2TKTL.htm)|Despoiler|auto-trad|
|[GPxeevqqNhveOB8u.htm](pathfinder-bestiary-3-items/GPxeevqqNhveOB8u.htm)|Snowstep|auto-trad|
|[GpYSF4Qy9tOtd9Z4.htm](pathfinder-bestiary-3-items/GpYSF4Qy9tOtd9Z4.htm)|Slice and Dice|auto-trad|
|[GPZIIPDmUwYRcaYF.htm](pathfinder-bestiary-3-items/GPZIIPDmUwYRcaYF.htm)|Grab|auto-trad|
|[gQmDkG34wRgRZDty.htm](pathfinder-bestiary-3-items/gQmDkG34wRgRZDty.htm)|Taste Anger (Imprecise) 1 mile|auto-trad|
|[GqtkVnERy0SmBtKA.htm](pathfinder-bestiary-3-items/GqtkVnERy0SmBtKA.htm)|Dart|auto-trad|
|[gr15jMdsKQApNTr1.htm](pathfinder-bestiary-3-items/gr15jMdsKQApNTr1.htm)|Hatchet|auto-trad|
|[grdS9mKHqWcDMpuH.htm](pathfinder-bestiary-3-items/grdS9mKHqWcDMpuH.htm)|Divine Innate Spells|auto-trad|
|[GrP9ijoNzD5cbT4X.htm](pathfinder-bestiary-3-items/GrP9ijoNzD5cbT4X.htm)|Composite Shortbow|auto-trad|
|[GRQsdgoPIcgExLRe.htm](pathfinder-bestiary-3-items/GRQsdgoPIcgExLRe.htm)|Darkvision|auto-trad|
|[GS25DV8nC7hEzR8I.htm](pathfinder-bestiary-3-items/GS25DV8nC7hEzR8I.htm)|Easy to Call|auto-trad|
|[gsORJTMYFRzNjUAn.htm](pathfinder-bestiary-3-items/gsORJTMYFRzNjUAn.htm)|Low-Light Vision|auto-trad|
|[Gt37cWDUiwwAkjyI.htm](pathfinder-bestiary-3-items/Gt37cWDUiwwAkjyI.htm)|Troop Movement|auto-trad|
|[gTGE8w5NItEtmFuG.htm](pathfinder-bestiary-3-items/gTGE8w5NItEtmFuG.htm)|Constant Spells|auto-trad|
|[gtLlO4bF2NESErTl.htm](pathfinder-bestiary-3-items/gtLlO4bF2NESErTl.htm)|Boat Breaker|auto-trad|
|[gU4MP6UZdedQtspk.htm](pathfinder-bestiary-3-items/gU4MP6UZdedQtspk.htm)|Frightful Presence|auto-trad|
|[Gu762rdO8uTT9ixQ.htm](pathfinder-bestiary-3-items/Gu762rdO8uTT9ixQ.htm)|Attack of Opportunity (Stinger Only)|auto-trad|
|[GugfZAMFY5fRgjN3.htm](pathfinder-bestiary-3-items/GugfZAMFY5fRgjN3.htm)|Longbow|auto-trad|
|[guqcEGLb6OHHFXDW.htm](pathfinder-bestiary-3-items/guqcEGLb6OHHFXDW.htm)|Occult Innate Spells|auto-trad|
|[GUTBHqMY0oGnnUx0.htm](pathfinder-bestiary-3-items/GUTBHqMY0oGnnUx0.htm)|Plagued Coffin Restoration|auto-trad|
|[GVOl6lIUVxSW5gZK.htm](pathfinder-bestiary-3-items/GVOl6lIUVxSW5gZK.htm)|Rejuvenation|auto-trad|
|[GWdF6u5sHvS26nIZ.htm](pathfinder-bestiary-3-items/GWdF6u5sHvS26nIZ.htm)|Divine Prepared Spells|auto-trad|
|[gWdFg8nl9k3xaERt.htm](pathfinder-bestiary-3-items/gWdFg8nl9k3xaERt.htm)|Shove into Stone|auto-trad|
|[GWKCP70pS87RXrFP.htm](pathfinder-bestiary-3-items/GWKCP70pS87RXrFP.htm)|Aura of Disquietude|auto-trad|
|[Gwusjl0kDQRYPaID.htm](pathfinder-bestiary-3-items/Gwusjl0kDQRYPaID.htm)|Astral Shock|auto-trad|
|[GWx7ppzGBq5TVLbL.htm](pathfinder-bestiary-3-items/GWx7ppzGBq5TVLbL.htm)|Constant Spells|auto-trad|
|[GWXJlKCvo60X8rp8.htm](pathfinder-bestiary-3-items/GWXJlKCvo60X8rp8.htm)|Hundred-Handed Whirlwind|auto-trad|
|[gxBpGZrMdteLCa6c.htm](pathfinder-bestiary-3-items/gxBpGZrMdteLCa6c.htm)|Hand|auto-trad|
|[Gxd48LW0au3UyXRn.htm](pathfinder-bestiary-3-items/Gxd48LW0au3UyXRn.htm)|Greater Darkvision|auto-trad|
|[gxkIdHdPIrnUNavf.htm](pathfinder-bestiary-3-items/gxkIdHdPIrnUNavf.htm)|Transcribe|auto-trad|
|[gXT2WwNt19Jqn8Lo.htm](pathfinder-bestiary-3-items/gXT2WwNt19Jqn8Lo.htm)|Proficient Poisoner|auto-trad|
|[GyUxfyjtgA7EdiPY.htm](pathfinder-bestiary-3-items/GyUxfyjtgA7EdiPY.htm)|Darkvision|auto-trad|
|[gZ11aiTmjuFfS0D9.htm](pathfinder-bestiary-3-items/gZ11aiTmjuFfS0D9.htm)|Swarm Mind|auto-trad|
|[gzbNuojJ54XXTTe3.htm](pathfinder-bestiary-3-items/gzbNuojJ54XXTTe3.htm)|Arcane Innate Spells|auto-trad|
|[gzGzxCPvcdZu8f4O.htm](pathfinder-bestiary-3-items/gzGzxCPvcdZu8f4O.htm)|Constrict|auto-trad|
|[GZIzUrSeCXXcCvhZ.htm](pathfinder-bestiary-3-items/GZIzUrSeCXXcCvhZ.htm)|At-Will Spells|auto-trad|
|[gZVSvNNNodOSP8Js.htm](pathfinder-bestiary-3-items/gZVSvNNNodOSP8Js.htm)|At-Will Spells|auto-trad|
|[gzZaUElRwVXoDLly.htm](pathfinder-bestiary-3-items/gzZaUElRwVXoDLly.htm)|Sagebane|auto-trad|
|[H00CJvnkXb56atN1.htm](pathfinder-bestiary-3-items/H00CJvnkXb56atN1.htm)|Jaws|auto-trad|
|[h1ZAFdNFnrnrUzal.htm](pathfinder-bestiary-3-items/h1ZAFdNFnrnrUzal.htm)|Wavesense (Imprecise) 120 feet|auto-trad|
|[H2G6L0SJ45uHxDiE.htm](pathfinder-bestiary-3-items/H2G6L0SJ45uHxDiE.htm)|Jaws|auto-trad|
|[h2l4ytlaIH7Elzxp.htm](pathfinder-bestiary-3-items/h2l4ytlaIH7Elzxp.htm)|Fist|auto-trad|
|[h2v7VpyXTPjTzVX1.htm](pathfinder-bestiary-3-items/h2v7VpyXTPjTzVX1.htm)|Smoke Vision|auto-trad|
|[H3pa17KySBkdeqib.htm](pathfinder-bestiary-3-items/H3pa17KySBkdeqib.htm)|Moonlight's Kiss|auto-trad|
|[H4TL62mthkiNmZPt.htm](pathfinder-bestiary-3-items/H4TL62mthkiNmZPt.htm)|Jaws|auto-trad|
|[h4X51BYbgu9tzFnJ.htm](pathfinder-bestiary-3-items/h4X51BYbgu9tzFnJ.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[h4zO0kK45DmIdXa1.htm](pathfinder-bestiary-3-items/h4zO0kK45DmIdXa1.htm)|Grab|auto-trad|
|[H6bcCPSTnAt5zBu8.htm](pathfinder-bestiary-3-items/H6bcCPSTnAt5zBu8.htm)|Felling Assault|auto-trad|
|[H6E4xmp7lHmRZXO2.htm](pathfinder-bestiary-3-items/H6E4xmp7lHmRZXO2.htm)|Hadalic Presence|auto-trad|
|[h6z492dnOVTr6NoI.htm](pathfinder-bestiary-3-items/h6z492dnOVTr6NoI.htm)|Occult Innate Spells|auto-trad|
|[h7VjKWk9XbmBEL2M.htm](pathfinder-bestiary-3-items/h7VjKWk9XbmBEL2M.htm)|Low-Light Vision|auto-trad|
|[h8caQJMnKj4F22zZ.htm](pathfinder-bestiary-3-items/h8caQJMnKj4F22zZ.htm)|Frenzied Slashes|auto-trad|
|[h8iiN0oJfHoKkkw6.htm](pathfinder-bestiary-3-items/h8iiN0oJfHoKkkw6.htm)|Darkvision|auto-trad|
|[h8Phu8ErJOaWCuUU.htm](pathfinder-bestiary-3-items/h8Phu8ErJOaWCuUU.htm)|Pincer|auto-trad|
|[h8rEFtxwQFlx0MIz.htm](pathfinder-bestiary-3-items/h8rEFtxwQFlx0MIz.htm)|Venomous Fangs|auto-trad|
|[h8UYIVYJvZxviPCt.htm](pathfinder-bestiary-3-items/h8UYIVYJvZxviPCt.htm)|Web|auto-trad|
|[H8WZz3DKIs1SCcFr.htm](pathfinder-bestiary-3-items/H8WZz3DKIs1SCcFr.htm)|Darkvision|auto-trad|
|[H9YGbRcmWQV6mrYy.htm](pathfinder-bestiary-3-items/H9YGbRcmWQV6mrYy.htm)|Grab|auto-trad|
|[HAbopgX5BTCYokYc.htm](pathfinder-bestiary-3-items/HAbopgX5BTCYokYc.htm)|Regenerative Blood|auto-trad|
|[HayFvnvEzF4yeL8e.htm](pathfinder-bestiary-3-items/HayFvnvEzF4yeL8e.htm)|Cleanly Vulnerability|auto-trad|
|[hbJLdmKm20p84vtq.htm](pathfinder-bestiary-3-items/hbJLdmKm20p84vtq.htm)|Hurled Debris|auto-trad|
|[HbNu5OQ0rlq7Yc5T.htm](pathfinder-bestiary-3-items/HbNu5OQ0rlq7Yc5T.htm)|Surface Skimmer|auto-trad|
|[HBWRwsQLwNl5P9GZ.htm](pathfinder-bestiary-3-items/HBWRwsQLwNl5P9GZ.htm)|Painful Bite|auto-trad|
|[hCBIDqLoaomtVumJ.htm](pathfinder-bestiary-3-items/hCBIDqLoaomtVumJ.htm)|Tremorsense (Imprecise) within their entire bound yard|auto-trad|
|[hcf7RGBrhHKjGcG4.htm](pathfinder-bestiary-3-items/hcf7RGBrhHKjGcG4.htm)|Camel Spit|auto-trad|
|[hCqIQRbX94na2LJI.htm](pathfinder-bestiary-3-items/hCqIQRbX94na2LJI.htm)|Spit|auto-trad|
|[hcWQWEF9ku17m4ue.htm](pathfinder-bestiary-3-items/hcWQWEF9ku17m4ue.htm)|Claw|auto-trad|
|[HD1JVNGcHKCVtQRP.htm](pathfinder-bestiary-3-items/HD1JVNGcHKCVtQRP.htm)|Fed by Earth|auto-trad|
|[hD5zZmaH7szmKu7h.htm](pathfinder-bestiary-3-items/hD5zZmaH7szmKu7h.htm)|Claw|auto-trad|
|[hD8oXqV8Wz5DBv2c.htm](pathfinder-bestiary-3-items/hD8oXqV8Wz5DBv2c.htm)|Claw|auto-trad|
|[HDgHNFoKqVXHIcGb.htm](pathfinder-bestiary-3-items/HDgHNFoKqVXHIcGb.htm)|Darkvision|auto-trad|
|[hDrYmsPyJApRYUgu.htm](pathfinder-bestiary-3-items/hDrYmsPyJApRYUgu.htm)|Wrap in Coils|auto-trad|
|[He0zYHgzyYLryoo4.htm](pathfinder-bestiary-3-items/He0zYHgzyYLryoo4.htm)|Claw|auto-trad|
|[He1e2JVLL4LLFCXX.htm](pathfinder-bestiary-3-items/He1e2JVLL4LLFCXX.htm)|Protective Pinch|auto-trad|
|[HeSYt5jeXQwluP8x.htm](pathfinder-bestiary-3-items/HeSYt5jeXQwluP8x.htm)|Fangs|auto-trad|
|[hfaAY55AoAQW59YR.htm](pathfinder-bestiary-3-items/hfaAY55AoAQW59YR.htm)|Inflict Misery|auto-trad|
|[hfbHiFnMOX2vWG3b.htm](pathfinder-bestiary-3-items/hfbHiFnMOX2vWG3b.htm)|Engulf|auto-trad|
|[hfMabMzyolY9p2Gw.htm](pathfinder-bestiary-3-items/hfMabMzyolY9p2Gw.htm)|Euphoric Spark|auto-trad|
|[hFX9SvNVh26s4No7.htm](pathfinder-bestiary-3-items/hFX9SvNVh26s4No7.htm)|Fade Away|auto-trad|
|[HHDECazmKhMUWZZF.htm](pathfinder-bestiary-3-items/HHDECazmKhMUWZZF.htm)|Divine Innate Spells|auto-trad|
|[hHLSrHMm2augW6G7.htm](pathfinder-bestiary-3-items/hHLSrHMm2augW6G7.htm)|At-Will Spells|auto-trad|
|[HHS4CPEzSJeRK2TJ.htm](pathfinder-bestiary-3-items/HHS4CPEzSJeRK2TJ.htm)|Drain Life|auto-trad|
|[Hi0AFeSVBBw7vuPF.htm](pathfinder-bestiary-3-items/Hi0AFeSVBBw7vuPF.htm)|Suction|auto-trad|
|[HiELCWTyhJvQaGUl.htm](pathfinder-bestiary-3-items/HiELCWTyhJvQaGUl.htm)|Low-Light Vision|auto-trad|
|[HiTMvvga2hoAWC9Y.htm](pathfinder-bestiary-3-items/HiTMvvga2hoAWC9Y.htm)|Snatch Between|auto-trad|
|[HItnjZpl8JZa8gGc.htm](pathfinder-bestiary-3-items/HItnjZpl8JZa8gGc.htm)|Primal Innate Spells|auto-trad|
|[Hj7vX0JdYOvuiOk2.htm](pathfinder-bestiary-3-items/Hj7vX0JdYOvuiOk2.htm)|Occult Innate Spells|auto-trad|
|[hJMhsrU4b1Xsyslz.htm](pathfinder-bestiary-3-items/hJMhsrU4b1Xsyslz.htm)|Low-Light Vision|auto-trad|
|[HJT5NvgiIrqOYp97.htm](pathfinder-bestiary-3-items/HJT5NvgiIrqOYp97.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[hJYdbtet0tqbLviV.htm](pathfinder-bestiary-3-items/hJYdbtet0tqbLviV.htm)|Fed by Metal|auto-trad|
|[HKJDIUhv4yxNRcOz.htm](pathfinder-bestiary-3-items/HKJDIUhv4yxNRcOz.htm)|Grab|auto-trad|
|[hKMqAgh48onTAhpH.htm](pathfinder-bestiary-3-items/hKMqAgh48onTAhpH.htm)|Clinch Victory|auto-trad|
|[Hl6TVTFqmE0SOKoE.htm](pathfinder-bestiary-3-items/Hl6TVTFqmE0SOKoE.htm)|Storm of Battle|auto-trad|
|[hljpYgDUm3hDZfDm.htm](pathfinder-bestiary-3-items/hljpYgDUm3hDZfDm.htm)|Low-Light Vision|auto-trad|
|[hlLjyEJFBxuPfIsH.htm](pathfinder-bestiary-3-items/hlLjyEJFBxuPfIsH.htm)|Spectral Claw|auto-trad|
|[hNp02EzjCutL6cMN.htm](pathfinder-bestiary-3-items/hNp02EzjCutL6cMN.htm)|Jaws|auto-trad|
|[hoeCSNdGULGrdSYX.htm](pathfinder-bestiary-3-items/hoeCSNdGULGrdSYX.htm)|Shield Block|auto-trad|
|[hozqm7eiBZchD7T3.htm](pathfinder-bestiary-3-items/hozqm7eiBZchD7T3.htm)|Attack of Opportunity|auto-trad|
|[hP7NqLHuZTlhjPtO.htm](pathfinder-bestiary-3-items/hP7NqLHuZTlhjPtO.htm)|Divine Innate Spells|auto-trad|
|[hpbnFFNrcbDIDwkP.htm](pathfinder-bestiary-3-items/hpbnFFNrcbDIDwkP.htm)|Occult Innate Spells|auto-trad|
|[hPBPkTGmnJVgZ22X.htm](pathfinder-bestiary-3-items/hPBPkTGmnJVgZ22X.htm)|Primal Innate Spells|auto-trad|
|[HPHMTMivSSxm6A9g.htm](pathfinder-bestiary-3-items/HPHMTMivSSxm6A9g.htm)|Firebolt|auto-trad|
|[hT2TFquy1W3hmWXW.htm](pathfinder-bestiary-3-items/hT2TFquy1W3hmWXW.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[HTapFz8e3dL6LNJc.htm](pathfinder-bestiary-3-items/HTapFz8e3dL6LNJc.htm)|Smoke Vision|auto-trad|
|[hteKQpvnrGLuVGSS.htm](pathfinder-bestiary-3-items/hteKQpvnrGLuVGSS.htm)|Low-Light Vision|auto-trad|
|[HTL4UczGmmOt7HUk.htm](pathfinder-bestiary-3-items/HTL4UczGmmOt7HUk.htm)|Persuasive Rebuttal|auto-trad|
|[HTp6N4B1ohShQdxQ.htm](pathfinder-bestiary-3-items/HTp6N4B1ohShQdxQ.htm)|Spray Musk|auto-trad|
|[hUmX5vToV3dXSRRK.htm](pathfinder-bestiary-3-items/hUmX5vToV3dXSRRK.htm)|Change Shape|auto-trad|
|[Hv7Blt8YRg7FuNu6.htm](pathfinder-bestiary-3-items/Hv7Blt8YRg7FuNu6.htm)|Deflect Aggression|auto-trad|
|[HvbA2JrPJX04mXf4.htm](pathfinder-bestiary-3-items/HvbA2JrPJX04mXf4.htm)|Smoke Vision|auto-trad|
|[HVELxWKZ2of1Q0F1.htm](pathfinder-bestiary-3-items/HVELxWKZ2of1Q0F1.htm)|Suspend Soul|auto-trad|
|[HvrOYSndJdFfyuSN.htm](pathfinder-bestiary-3-items/HvrOYSndJdFfyuSN.htm)|Towering Stance|auto-trad|
|[hVTC4rrt8IWJq51F.htm](pathfinder-bestiary-3-items/hVTC4rrt8IWJq51F.htm)|Green Tongue|auto-trad|
|[HvYM5Z1ubUt120XB.htm](pathfinder-bestiary-3-items/HvYM5Z1ubUt120XB.htm)|Claw|auto-trad|
|[HwqKHt0yeIDL3Jjm.htm](pathfinder-bestiary-3-items/HwqKHt0yeIDL3Jjm.htm)|Attack of Opportunity|auto-trad|
|[hww0iSmzvgv1CuVE.htm](pathfinder-bestiary-3-items/hww0iSmzvgv1CuVE.htm)|Sunset Ray|auto-trad|
|[HX5VU8Xvb5yPZ2aH.htm](pathfinder-bestiary-3-items/HX5VU8Xvb5yPZ2aH.htm)|Discorporate|auto-trad|
|[HX6LFkk4karuzyse.htm](pathfinder-bestiary-3-items/HX6LFkk4karuzyse.htm)|Constrict|auto-trad|
|[hXhEkQgvn1PZadRa.htm](pathfinder-bestiary-3-items/hXhEkQgvn1PZadRa.htm)|Divine Innate Spells|auto-trad|
|[hyj6N5o5oSyNgbJd.htm](pathfinder-bestiary-3-items/hyj6N5o5oSyNgbJd.htm)|Vine|auto-trad|
|[hykuv0gwwDzF4GZW.htm](pathfinder-bestiary-3-items/hykuv0gwwDzF4GZW.htm)|Bonded Strike|auto-trad|
|[HYkzxImoz15eP5c0.htm](pathfinder-bestiary-3-items/HYkzxImoz15eP5c0.htm)|Primal Innate Spells|auto-trad|
|[HYlZSPGidgo056wQ.htm](pathfinder-bestiary-3-items/HYlZSPGidgo056wQ.htm)|Constant Spells|auto-trad|
|[hYXNxWZ2lgrY2oNX.htm](pathfinder-bestiary-3-items/hYXNxWZ2lgrY2oNX.htm)|Primal Prepared Spells|auto-trad|
|[hzAD0q71nN1lkiVO.htm](pathfinder-bestiary-3-items/hzAD0q71nN1lkiVO.htm)|Primal Innate Spells|auto-trad|
|[hZqDsZrM19xB27pn.htm](pathfinder-bestiary-3-items/hZqDsZrM19xB27pn.htm)|Blessed Strikes|auto-trad|
|[i0CgRjDNx28m8d7r.htm](pathfinder-bestiary-3-items/i0CgRjDNx28m8d7r.htm)|Hundred-Dimension Grasp|auto-trad|
|[i0GVDRjE4Mf3fPfW.htm](pathfinder-bestiary-3-items/i0GVDRjE4Mf3fPfW.htm)|Spear|auto-trad|
|[i1KyfP7F6bj0FZS5.htm](pathfinder-bestiary-3-items/i1KyfP7F6bj0FZS5.htm)|Constrict|auto-trad|
|[I1xWawDf3yw69pnw.htm](pathfinder-bestiary-3-items/I1xWawDf3yw69pnw.htm)|Desert Stride|auto-trad|
|[I239EJXsU4ME0R50.htm](pathfinder-bestiary-3-items/I239EJXsU4ME0R50.htm)|Shortsword|auto-trad|
|[i2o1AoEa6G6aYbNZ.htm](pathfinder-bestiary-3-items/i2o1AoEa6G6aYbNZ.htm)|Low-Light Vision|auto-trad|
|[i2xCkOeImkc5M5zz.htm](pathfinder-bestiary-3-items/i2xCkOeImkc5M5zz.htm)|Darkvision|auto-trad|
|[i2YbHX9ZBEKMZMOv.htm](pathfinder-bestiary-3-items/i2YbHX9ZBEKMZMOv.htm)|Darkvision|auto-trad|
|[I3LHE7CKMGHBy5aq.htm](pathfinder-bestiary-3-items/I3LHE7CKMGHBy5aq.htm)|Call to Arms|auto-trad|
|[i49FXrRnhIZz63Ee.htm](pathfinder-bestiary-3-items/i49FXrRnhIZz63Ee.htm)|Recognize Hero|auto-trad|
|[i4jbUrRlPXHHy15s.htm](pathfinder-bestiary-3-items/i4jbUrRlPXHHy15s.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[I5FrBDYXau3Iuild.htm](pathfinder-bestiary-3-items/I5FrBDYXau3Iuild.htm)|Telepathy 120 feet|auto-trad|
|[I6ATQo2zBIuQQinX.htm](pathfinder-bestiary-3-items/I6ATQo2zBIuQQinX.htm)|Draconic Momentum|auto-trad|
|[I7lMAhjCqMyxPu6y.htm](pathfinder-bestiary-3-items/I7lMAhjCqMyxPu6y.htm)|Inexorable|auto-trad|
|[i7YE4zH1GBMET2Uu.htm](pathfinder-bestiary-3-items/i7YE4zH1GBMET2Uu.htm)|Change Shape|auto-trad|
|[i8I92PVcpFQf4HBz.htm](pathfinder-bestiary-3-items/i8I92PVcpFQf4HBz.htm)|Occult Innate Spells|auto-trad|
|[I9tVwoP0Z0dV0r5j.htm](pathfinder-bestiary-3-items/I9tVwoP0Z0dV0r5j.htm)|Amulet Relic|auto-trad|
|[ia7nfgIs3ERJytZy.htm](pathfinder-bestiary-3-items/ia7nfgIs3ERJytZy.htm)|Persistence of Memory|auto-trad|
|[IaFXrGv80HlNVl19.htm](pathfinder-bestiary-3-items/IaFXrGv80HlNVl19.htm)|Telepathy 150 feet|auto-trad|
|[IAO39ERjCHDfcZlr.htm](pathfinder-bestiary-3-items/IAO39ERjCHDfcZlr.htm)|Hoof|auto-trad|
|[IBxaJ1PsBPxvnN70.htm](pathfinder-bestiary-3-items/IBxaJ1PsBPxvnN70.htm)|Droning Distraction|auto-trad|
|[iC8kTbasCU08DFOc.htm](pathfinder-bestiary-3-items/iC8kTbasCU08DFOc.htm)|Loathing Garotte|auto-trad|
|[ICHmpsSeRPd2QliC.htm](pathfinder-bestiary-3-items/ICHmpsSeRPd2QliC.htm)|Coiling Frenzy|auto-trad|
|[iCM4V1WXNcTslhi7.htm](pathfinder-bestiary-3-items/iCM4V1WXNcTslhi7.htm)|Coven|auto-trad|
|[ICqXmQ1I3WhSNBF7.htm](pathfinder-bestiary-3-items/ICqXmQ1I3WhSNBF7.htm)|Green Rituals|auto-trad|
|[icR13DUCw64iVgQ2.htm](pathfinder-bestiary-3-items/icR13DUCw64iVgQ2.htm)|Divine Innate Spells|auto-trad|
|[icUrUnsfgvWuac89.htm](pathfinder-bestiary-3-items/icUrUnsfgvWuac89.htm)|Tail|auto-trad|
|[IDk5Kln9p90yl0SF.htm](pathfinder-bestiary-3-items/IDk5Kln9p90yl0SF.htm)|Telepathy 100 feet|auto-trad|
|[Idu5EyPugITPjmRq.htm](pathfinder-bestiary-3-items/Idu5EyPugITPjmRq.htm)|Paralysis|auto-trad|
|[IE75LW4gCgc5TOPJ.htm](pathfinder-bestiary-3-items/IE75LW4gCgc5TOPJ.htm)|Designate Master|auto-trad|
|[iewmwrowaGzoIyTr.htm](pathfinder-bestiary-3-items/iewmwrowaGzoIyTr.htm)|Woodland Stride|auto-trad|
|[if2CB9IchjxMbiab.htm](pathfinder-bestiary-3-items/if2CB9IchjxMbiab.htm)|Coven Spells|auto-trad|
|[IFBwgiXBfRM7tF8Q.htm](pathfinder-bestiary-3-items/IFBwgiXBfRM7tF8Q.htm)|Eel Dart|auto-trad|
|[IfgRYpjy1kMxYZ7N.htm](pathfinder-bestiary-3-items/IfgRYpjy1kMxYZ7N.htm)|Occult Innate Spells|auto-trad|
|[iFhR8LsLMNJ2v6O0.htm](pathfinder-bestiary-3-items/iFhR8LsLMNJ2v6O0.htm)|Absorb Magic|auto-trad|
|[iFKv7PFNKtSVy9XA.htm](pathfinder-bestiary-3-items/iFKv7PFNKtSVy9XA.htm)|Slow|auto-trad|
|[IfQ2Ql0FDoKs4CXh.htm](pathfinder-bestiary-3-items/IfQ2Ql0FDoKs4CXh.htm)|Fist|auto-trad|
|[ifv7iRcnxapPCh5S.htm](pathfinder-bestiary-3-items/ifv7iRcnxapPCh5S.htm)|Grab|auto-trad|
|[Ig0zFPa6ddSOqkXY.htm](pathfinder-bestiary-3-items/Ig0zFPa6ddSOqkXY.htm)|Javelin|auto-trad|
|[iGBJDGQHVXz9hpgE.htm](pathfinder-bestiary-3-items/iGBJDGQHVXz9hpgE.htm)|Constant Spells|auto-trad|
|[igF8SYccIFVckGta.htm](pathfinder-bestiary-3-items/igF8SYccIFVckGta.htm)|Master of the Granary|auto-trad|
|[IgNXM2vcFy0D1uCF.htm](pathfinder-bestiary-3-items/IgNXM2vcFy0D1uCF.htm)|Cudgel|auto-trad|
|[Igt62T1pB0clDkfb.htm](pathfinder-bestiary-3-items/Igt62T1pB0clDkfb.htm)|All-Around Vision|auto-trad|
|[iGx7eQKe0LJXzUAo.htm](pathfinder-bestiary-3-items/iGx7eQKe0LJXzUAo.htm)|Breath Weapon|auto-trad|
|[igxHjmUJWlr6ULG7.htm](pathfinder-bestiary-3-items/igxHjmUJWlr6ULG7.htm)|Head Spin|auto-trad|
|[IH1oaRD65ApAyt5k.htm](pathfinder-bestiary-3-items/IH1oaRD65ApAyt5k.htm)|Longsword|auto-trad|
|[iHJPeHHphrdXidbV.htm](pathfinder-bestiary-3-items/iHJPeHHphrdXidbV.htm)|Enraged Home (Slashing)|auto-trad|
|[iHPmzxNLB3Yqy1nG.htm](pathfinder-bestiary-3-items/iHPmzxNLB3Yqy1nG.htm)|Frightful Presence|auto-trad|
|[IhW67AKfQIjSFjgx.htm](pathfinder-bestiary-3-items/IhW67AKfQIjSFjgx.htm)|Elegy of the Faithless|auto-trad|
|[iIIyNY3zJBmroqzM.htm](pathfinder-bestiary-3-items/iIIyNY3zJBmroqzM.htm)|Occult Innate Spells|auto-trad|
|[iIkQe60YaPrjs5Wa.htm](pathfinder-bestiary-3-items/iIkQe60YaPrjs5Wa.htm)|Claw|auto-trad|
|[iJaSMrCi07AzcK4L.htm](pathfinder-bestiary-3-items/iJaSMrCi07AzcK4L.htm)|Share Defenses|auto-trad|
|[Ijep1LR2jAGG5Xqz.htm](pathfinder-bestiary-3-items/Ijep1LR2jAGG5Xqz.htm)|Negative Healing|auto-trad|
|[IjHEwnv7xjIRPpYZ.htm](pathfinder-bestiary-3-items/IjHEwnv7xjIRPpYZ.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[IjkPvQ204dD2FWaQ.htm](pathfinder-bestiary-3-items/IjkPvQ204dD2FWaQ.htm)|Primal Innate Spells|auto-trad|
|[ijtDf4mbj5lb5bvM.htm](pathfinder-bestiary-3-items/ijtDf4mbj5lb5bvM.htm)|Sneak Attack|auto-trad|
|[iJuVRVeuCbiPmn0M.htm](pathfinder-bestiary-3-items/iJuVRVeuCbiPmn0M.htm)|Grab|auto-trad|
|[iK81tvrAddbR20xS.htm](pathfinder-bestiary-3-items/iK81tvrAddbR20xS.htm)|Pincer|auto-trad|
|[IKBRGKTG4o0biJz9.htm](pathfinder-bestiary-3-items/IKBRGKTG4o0biJz9.htm)|Low-Light Vision|auto-trad|
|[iKR8M3h9wCtVtDTe.htm](pathfinder-bestiary-3-items/iKR8M3h9wCtVtDTe.htm)|Swing from the Saddle|auto-trad|
|[ikvqvxMtrtqOwbWu.htm](pathfinder-bestiary-3-items/ikvqvxMtrtqOwbWu.htm)|Seize Prayer|auto-trad|
|[Il0NCYZxE3i4U1ud.htm](pathfinder-bestiary-3-items/Il0NCYZxE3i4U1ud.htm)|Echoblade|auto-trad|
|[iLC1r5LnzDRvTshg.htm](pathfinder-bestiary-3-items/iLC1r5LnzDRvTshg.htm)|Banished from the Ground|auto-trad|
|[ILDPyfeR9ijXko3B.htm](pathfinder-bestiary-3-items/ILDPyfeR9ijXko3B.htm)|Jaws|auto-trad|
|[IlJzVorvcYKLmyZM.htm](pathfinder-bestiary-3-items/IlJzVorvcYKLmyZM.htm)|Backdrop|auto-trad|
|[ILOl1dY50IaR6WPN.htm](pathfinder-bestiary-3-items/ILOl1dY50IaR6WPN.htm)|Liquefy|auto-trad|
|[IO9iVIVpEfBjrcmw.htm](pathfinder-bestiary-3-items/IO9iVIVpEfBjrcmw.htm)|Divine Dispelling|auto-trad|
|[IOBHBhN2gfM5QQHO.htm](pathfinder-bestiary-3-items/IOBHBhN2gfM5QQHO.htm)|Greater Constrict|auto-trad|
|[iommO8SU4yNamwC1.htm](pathfinder-bestiary-3-items/iommO8SU4yNamwC1.htm)|Focus Gaze|auto-trad|
|[iov5t4k8TcxeShEp.htm](pathfinder-bestiary-3-items/iov5t4k8TcxeShEp.htm)|Shortbow|auto-trad|
|[IP7OFv62ex3QvHzS.htm](pathfinder-bestiary-3-items/IP7OFv62ex3QvHzS.htm)|Knockdown|auto-trad|
|[IpaUvWrHaznmdOGb.htm](pathfinder-bestiary-3-items/IpaUvWrHaznmdOGb.htm)|Darkvision|auto-trad|
|[iPdoOjHPIU1Zhkvd.htm](pathfinder-bestiary-3-items/iPdoOjHPIU1Zhkvd.htm)|Stepping Decoy|auto-trad|
|[IPI8W2nuWEsuk2ti.htm](pathfinder-bestiary-3-items/IPI8W2nuWEsuk2ti.htm)|Despairing Weep|auto-trad|
|[iq5hF08qzTQTeTBD.htm](pathfinder-bestiary-3-items/iq5hF08qzTQTeTBD.htm)|Trample|auto-trad|
|[iQBwvNjlgULNrpOy.htm](pathfinder-bestiary-3-items/iQBwvNjlgULNrpOy.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[iQNybW2FjtzVHCOS.htm](pathfinder-bestiary-3-items/iQNybW2FjtzVHCOS.htm)|Self-Resurrection|auto-trad|
|[iQvaTMNbSTahgvnU.htm](pathfinder-bestiary-3-items/iQvaTMNbSTahgvnU.htm)|Foot|auto-trad|
|[iraYrEvANESYqSYz.htm](pathfinder-bestiary-3-items/iraYrEvANESYqSYz.htm)|Antler|auto-trad|
|[IRdpsVF3pePDKcEF.htm](pathfinder-bestiary-3-items/IRdpsVF3pePDKcEF.htm)|Enraged Home (Bludgeoning)|auto-trad|
|[iRJk9tacSkOQcb2p.htm](pathfinder-bestiary-3-items/iRJk9tacSkOQcb2p.htm)|Create Golden Apple|auto-trad|
|[IRV6fBqa828pUJvZ.htm](pathfinder-bestiary-3-items/IRV6fBqa828pUJvZ.htm)|Unnatural Leap|auto-trad|
|[IsKgw1GkBDUH68wl.htm](pathfinder-bestiary-3-items/IsKgw1GkBDUH68wl.htm)|Wavesense (Imprecise) 60 feet|auto-trad|
|[ISW9dU1e7h1uA2Vu.htm](pathfinder-bestiary-3-items/ISW9dU1e7h1uA2Vu.htm)|In Concert|auto-trad|
|[IT0DYOyhbNdwhEsc.htm](pathfinder-bestiary-3-items/IT0DYOyhbNdwhEsc.htm)|At-Will Spells|auto-trad|
|[It88LMNyaOfw3HMG.htm](pathfinder-bestiary-3-items/It88LMNyaOfw3HMG.htm)|Shadow's Swiftness|auto-trad|
|[iuA961D6giH9Ci1G.htm](pathfinder-bestiary-3-items/iuA961D6giH9Ci1G.htm)|Construct Armor (Hardness 10)|auto-trad|
|[iUcqbwAXsz9BBt5U.htm](pathfinder-bestiary-3-items/iUcqbwAXsz9BBt5U.htm)|Change Shape|auto-trad|
|[IUp0cmk3iouIb4mc.htm](pathfinder-bestiary-3-items/IUp0cmk3iouIb4mc.htm)|Mundane Appearance|auto-trad|
|[IvCrd9B34OQupCPq.htm](pathfinder-bestiary-3-items/IvCrd9B34OQupCPq.htm)|Slime Ball|auto-trad|
|[IVnGWQzHYIx6BwIf.htm](pathfinder-bestiary-3-items/IVnGWQzHYIx6BwIf.htm)|Ride Corpse|auto-trad|
|[ivTJbPtmjGlcioNL.htm](pathfinder-bestiary-3-items/ivTJbPtmjGlcioNL.htm)|Halberd|auto-trad|
|[IvUr6AnOtG8K2BSD.htm](pathfinder-bestiary-3-items/IvUr6AnOtG8K2BSD.htm)|Energize Clockwork Wand|auto-trad|
|[iVuxv6GFqvanqgOO.htm](pathfinder-bestiary-3-items/iVuxv6GFqvanqgOO.htm)|Rend|auto-trad|
|[iWhzjrN6V3SMvbjf.htm](pathfinder-bestiary-3-items/iWhzjrN6V3SMvbjf.htm)|Claw|auto-trad|
|[IwRzWoZPoZN3p2w4.htm](pathfinder-bestiary-3-items/IwRzWoZPoZN3p2w4.htm)|Divine Innate Spells|auto-trad|
|[iXj6VKYDT9ao0fDy.htm](pathfinder-bestiary-3-items/iXj6VKYDT9ao0fDy.htm)|Darkvision|auto-trad|
|[iXMM9LrLHQONVyO1.htm](pathfinder-bestiary-3-items/iXMM9LrLHQONVyO1.htm)|Throw Spirits|auto-trad|
|[IyoiZQ9XhzNia9nz.htm](pathfinder-bestiary-3-items/IyoiZQ9XhzNia9nz.htm)|Shortsword|auto-trad|
|[IyQeFgzzdZuwWSrh.htm](pathfinder-bestiary-3-items/IyQeFgzzdZuwWSrh.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[iYWcxAxjnGp9gag8.htm](pathfinder-bestiary-3-items/iYWcxAxjnGp9gag8.htm)|Hatchet|auto-trad|
|[iZ48ihJeJ9DbfW5I.htm](pathfinder-bestiary-3-items/iZ48ihJeJ9DbfW5I.htm)|Jaws|auto-trad|
|[IzAYt0TOGDn03Iuj.htm](pathfinder-bestiary-3-items/IzAYt0TOGDn03Iuj.htm)|Fiery Explosion|auto-trad|
|[iZqo211s6bnAgJto.htm](pathfinder-bestiary-3-items/iZqo211s6bnAgJto.htm)|Arcane Innate Spells|auto-trad|
|[j1Bwf8sEmKvjZNHK.htm](pathfinder-bestiary-3-items/j1Bwf8sEmKvjZNHK.htm)|Low-Light Vision|auto-trad|
|[j1yTlYWclYVbVuCO.htm](pathfinder-bestiary-3-items/j1yTlYWclYVbVuCO.htm)|Composite Longbow|auto-trad|
|[j2EwWhmaQSWT0Ktw.htm](pathfinder-bestiary-3-items/j2EwWhmaQSWT0Ktw.htm)|Wrath of Spurned Hospitality|auto-trad|
|[J2Wcbnb4u8polTU4.htm](pathfinder-bestiary-3-items/J2Wcbnb4u8polTU4.htm)|Grab|auto-trad|
|[J3Ldbu2TKyrPVZBY.htm](pathfinder-bestiary-3-items/J3Ldbu2TKyrPVZBY.htm)|Instrument of Faith|auto-trad|
|[j3teD40q6U6024pW.htm](pathfinder-bestiary-3-items/j3teD40q6U6024pW.htm)|Foot|auto-trad|
|[j3YK3rYte4HUEXXX.htm](pathfinder-bestiary-3-items/j3YK3rYte4HUEXXX.htm)|Troop Defenses|auto-trad|
|[J5VPK19qAf7MabPt.htm](pathfinder-bestiary-3-items/J5VPK19qAf7MabPt.htm)|Emerge From Undergrowth|auto-trad|
|[j67aVe855G3FeDEO.htm](pathfinder-bestiary-3-items/j67aVe855G3FeDEO.htm)|Improved Grab|auto-trad|
|[j68N9UkahC1d6lhq.htm](pathfinder-bestiary-3-items/j68N9UkahC1d6lhq.htm)|-1 Status to All Saves vs. Death Effects|auto-trad|
|[J6FVU37oPVKikGeY.htm](pathfinder-bestiary-3-items/J6FVU37oPVKikGeY.htm)|Darkvision|auto-trad|
|[j6jdXFHyW1BBy2iT.htm](pathfinder-bestiary-3-items/j6jdXFHyW1BBy2iT.htm)|Attack of Opportunity|auto-trad|
|[j6z5lXtJI1JXtP6N.htm](pathfinder-bestiary-3-items/j6z5lXtJI1JXtP6N.htm)|Constant Spells|auto-trad|
|[j71uBba5yrrEslXS.htm](pathfinder-bestiary-3-items/j71uBba5yrrEslXS.htm)|Flexible|auto-trad|
|[J7axLPnjNBn2CYP9.htm](pathfinder-bestiary-3-items/J7axLPnjNBn2CYP9.htm)|Negative Healing|auto-trad|
|[J8ElHZzPwIKugFCK.htm](pathfinder-bestiary-3-items/J8ElHZzPwIKugFCK.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[J9DbeNCFrbq08rQy.htm](pathfinder-bestiary-3-items/J9DbeNCFrbq08rQy.htm)|Felt Shears|auto-trad|
|[JA8W1EoSKmMH74zg.htm](pathfinder-bestiary-3-items/JA8W1EoSKmMH74zg.htm)|Illusory Weapon|auto-trad|
|[JAaRcZ0xgXIgQi8D.htm](pathfinder-bestiary-3-items/JAaRcZ0xgXIgQi8D.htm)|Divine Innate Spells|auto-trad|
|[jAcMhmLTK9Y34bhZ.htm](pathfinder-bestiary-3-items/jAcMhmLTK9Y34bhZ.htm)|Coven Spells|auto-trad|
|[jaJAvtkcuLs6oqUz.htm](pathfinder-bestiary-3-items/jaJAvtkcuLs6oqUz.htm)|Primal Innate Spells|auto-trad|
|[jB6DPG2ZqtuA1Z3r.htm](pathfinder-bestiary-3-items/jB6DPG2ZqtuA1Z3r.htm)|Regeneration 15 (Deactivated by Acid)|auto-trad|
|[jBnvJizQVNlbpByJ.htm](pathfinder-bestiary-3-items/jBnvJizQVNlbpByJ.htm)|Jaws|auto-trad|
|[jbOS76Gp3MzY4W9x.htm](pathfinder-bestiary-3-items/jbOS76Gp3MzY4W9x.htm)|Torturous Buzz|auto-trad|
|[jBu4ahdLBrGQ0cxJ.htm](pathfinder-bestiary-3-items/jBu4ahdLBrGQ0cxJ.htm)|Shuriken|auto-trad|
|[JCVCYsMSue62ekID.htm](pathfinder-bestiary-3-items/JCVCYsMSue62ekID.htm)|Project Terror|auto-trad|
|[JcXJdmKDoEq5nqTp.htm](pathfinder-bestiary-3-items/JcXJdmKDoEq5nqTp.htm)|Covetous of Secrets|auto-trad|
|[jd3HtosBbbWGDYMZ.htm](pathfinder-bestiary-3-items/jd3HtosBbbWGDYMZ.htm)|Spider Legs|auto-trad|
|[JD7dIGSb1yYwZOil.htm](pathfinder-bestiary-3-items/JD7dIGSb1yYwZOil.htm)|At-Will Spells|auto-trad|
|[jdhRSwRmm4AuXQt1.htm](pathfinder-bestiary-3-items/jdhRSwRmm4AuXQt1.htm)|Foot|auto-trad|
|[JDNUanP8QQ5riRTG.htm](pathfinder-bestiary-3-items/JDNUanP8QQ5riRTG.htm)|Attack of Opportunity|auto-trad|
|[jEfZwLub2SXZ9ApN.htm](pathfinder-bestiary-3-items/jEfZwLub2SXZ9ApN.htm)|Ominous Footsteps|auto-trad|
|[jETzhtuReRYnPbPE.htm](pathfinder-bestiary-3-items/jETzhtuReRYnPbPE.htm)|False Foe|auto-trad|
|[jeUZBWMJc0ynT78X.htm](pathfinder-bestiary-3-items/jeUZBWMJc0ynT78X.htm)|Darkvision|auto-trad|
|[JG2FJBq2rmJM7TqB.htm](pathfinder-bestiary-3-items/JG2FJBq2rmJM7TqB.htm)|+1 Status to All Saves vs. Darkness or Shadow|auto-trad|
|[jgB5xsEwGDphGPM7.htm](pathfinder-bestiary-3-items/jgB5xsEwGDphGPM7.htm)|Golem Antimagic|auto-trad|
|[JgIL2Chquwv95iZZ.htm](pathfinder-bestiary-3-items/JgIL2Chquwv95iZZ.htm)|Hyponatremia|auto-trad|
|[JGSUP9GpkQinyXNh.htm](pathfinder-bestiary-3-items/JGSUP9GpkQinyXNh.htm)|Darkvision|auto-trad|
|[jGVjxRiLDypUaQCi.htm](pathfinder-bestiary-3-items/jGVjxRiLDypUaQCi.htm)|Primal Innate Spells|auto-trad|
|[JHEhs3yuQkcfulWY.htm](pathfinder-bestiary-3-items/JHEhs3yuQkcfulWY.htm)|Anguished Cry|auto-trad|
|[jhi0sreme2yjolfI.htm](pathfinder-bestiary-3-items/jhi0sreme2yjolfI.htm)|Spear|auto-trad|
|[jHVQHOSLTThAA8th.htm](pathfinder-bestiary-3-items/jHVQHOSLTThAA8th.htm)|Antler|auto-trad|
|[ji4mZPdeTTXd5DD2.htm](pathfinder-bestiary-3-items/ji4mZPdeTTXd5DD2.htm)|Low-Light Vision|auto-trad|
|[JiieW7FHU7H9vlCH.htm](pathfinder-bestiary-3-items/JiieW7FHU7H9vlCH.htm)|Greater Constrict|auto-trad|
|[jJdhYFljJjyWjYuO.htm](pathfinder-bestiary-3-items/jJdhYFljJjyWjYuO.htm)|Light to Dark|auto-trad|
|[JjTVprpY8GeQPnTT.htm](pathfinder-bestiary-3-items/JjTVprpY8GeQPnTT.htm)|Thoughtsense (Imprecise) 60 feet|auto-trad|
|[jkOvDfJsGXDaYicW.htm](pathfinder-bestiary-3-items/jkOvDfJsGXDaYicW.htm)|Crocodile Empathy|auto-trad|
|[jKqS45yr5dqPtPAf.htm](pathfinder-bestiary-3-items/jKqS45yr5dqPtPAf.htm)|Claw|auto-trad|
|[jlBFW0dhslsLTtR1.htm](pathfinder-bestiary-3-items/jlBFW0dhslsLTtR1.htm)|Collective Sense|auto-trad|
|[jlphhH3RnH4L3Qup.htm](pathfinder-bestiary-3-items/jlphhH3RnH4L3Qup.htm)|Grab|auto-trad|
|[jLWpzWTqyo83F7Uy.htm](pathfinder-bestiary-3-items/jLWpzWTqyo83F7Uy.htm)|At-Will Spells|auto-trad|
|[JmeFujNsZiXeU7qA.htm](pathfinder-bestiary-3-items/JmeFujNsZiXeU7qA.htm)|Breath Weapon|auto-trad|
|[JmiVjM3xyF1WaZae.htm](pathfinder-bestiary-3-items/JmiVjM3xyF1WaZae.htm)|Uncanny Pounce|auto-trad|
|[jMJ3VKTn3P4G9pne.htm](pathfinder-bestiary-3-items/jMJ3VKTn3P4G9pne.htm)|Instant Suspension|auto-trad|
|[jMQ5yo71IDbzIoiV.htm](pathfinder-bestiary-3-items/jMQ5yo71IDbzIoiV.htm)|Jaws|auto-trad|
|[JNAbSt0GireCV1x1.htm](pathfinder-bestiary-3-items/JNAbSt0GireCV1x1.htm)|Darkvision|auto-trad|
|[JNflvBLtzzPBlMKZ.htm](pathfinder-bestiary-3-items/JNflvBLtzzPBlMKZ.htm)|Heat of the Forge|auto-trad|
|[JNHO9c4kCv8dEqkU.htm](pathfinder-bestiary-3-items/JNHO9c4kCv8dEqkU.htm)|Tongue|auto-trad|
|[jnPwDZEoWvaBHihS.htm](pathfinder-bestiary-3-items/jnPwDZEoWvaBHihS.htm)|Jorogumo Venom|auto-trad|
|[JOnNHpqNRRfoZym8.htm](pathfinder-bestiary-3-items/JOnNHpqNRRfoZym8.htm)|At-Will Spells|auto-trad|
|[JOPv6XlxMrnEuMiK.htm](pathfinder-bestiary-3-items/JOPv6XlxMrnEuMiK.htm)|Negative Healing|auto-trad|
|[jpLpYja2kQc3ywu3.htm](pathfinder-bestiary-3-items/jpLpYja2kQc3ywu3.htm)|Telepathy 30 feet (Munavris Only)|auto-trad|
|[JpsnonKEA4ScV5n5.htm](pathfinder-bestiary-3-items/JpsnonKEA4ScV5n5.htm)|Tail|auto-trad|
|[jQCQmFdnjvvlQOeA.htm](pathfinder-bestiary-3-items/jQCQmFdnjvvlQOeA.htm)|Darkvision|auto-trad|
|[jQDDocLXx8D5KEwx.htm](pathfinder-bestiary-3-items/jQDDocLXx8D5KEwx.htm)|Catch Rock|auto-trad|
|[jQpWelCVysebUl0R.htm](pathfinder-bestiary-3-items/jQpWelCVysebUl0R.htm)|Tail|auto-trad|
|[jqQAuZsUGFs0m1uI.htm](pathfinder-bestiary-3-items/jqQAuZsUGFs0m1uI.htm)|Lifesense 120 feet|auto-trad|
|[jrAbmB8HIKhZ8PFd.htm](pathfinder-bestiary-3-items/jrAbmB8HIKhZ8PFd.htm)|Jaws|auto-trad|
|[jRNANkCIgSnmDUWP.htm](pathfinder-bestiary-3-items/jRNANkCIgSnmDUWP.htm)|Low-Light Vision|auto-trad|
|[Js5HPgVs02JYjPwr.htm](pathfinder-bestiary-3-items/Js5HPgVs02JYjPwr.htm)|Spray Pus|auto-trad|
|[JScL1xh1oN0V3BSZ.htm](pathfinder-bestiary-3-items/JScL1xh1oN0V3BSZ.htm)|Darkvision|auto-trad|
|[JSGbWepzGabICqtd.htm](pathfinder-bestiary-3-items/JSGbWepzGabICqtd.htm)|Spray Blinding Musk|auto-trad|
|[jspQIwxqoVmeF37U.htm](pathfinder-bestiary-3-items/jspQIwxqoVmeF37U.htm)|Invoke Haunter of the Dark|auto-trad|
|[JSRXi7FpsI5stZOw.htm](pathfinder-bestiary-3-items/JSRXi7FpsI5stZOw.htm)|Fangs|auto-trad|
|[Jt5uw27Ef9hVZ8D5.htm](pathfinder-bestiary-3-items/Jt5uw27Ef9hVZ8D5.htm)|Thoughtsense (Precise) 60 feet|auto-trad|
|[jTT3N3sXixsPE4b1.htm](pathfinder-bestiary-3-items/jTT3N3sXixsPE4b1.htm)|Fist|auto-trad|
|[jTvAXL1UqsQrbQhO.htm](pathfinder-bestiary-3-items/jTvAXL1UqsQrbQhO.htm)|Telepathy (Touch)|auto-trad|
|[jUd95l5nba0bNy3I.htm](pathfinder-bestiary-3-items/jUd95l5nba0bNy3I.htm)|Gleaming Armor|auto-trad|
|[jveVL0lDMa0EIIx6.htm](pathfinder-bestiary-3-items/jveVL0lDMa0EIIx6.htm)|Constant Spells|auto-trad|
|[jvJQ52E2kygewU8J.htm](pathfinder-bestiary-3-items/jvJQ52E2kygewU8J.htm)|Spear|auto-trad|
|[jw9ImZmmbzTSmeX5.htm](pathfinder-bestiary-3-items/jw9ImZmmbzTSmeX5.htm)|Threatening Lunge|auto-trad|
|[JwmDTNMiGDZ6Kis3.htm](pathfinder-bestiary-3-items/JwmDTNMiGDZ6Kis3.htm)|Dagger|auto-trad|
|[jwmn7WQty7oJo2L7.htm](pathfinder-bestiary-3-items/jwmn7WQty7oJo2L7.htm)|Construct Armor (Hardness 15)|auto-trad|
|[JwRLhgIxYJX1wLQx.htm](pathfinder-bestiary-3-items/JwRLhgIxYJX1wLQx.htm)|Thundering Charge|auto-trad|
|[JxdXfCP6ayaEEZND.htm](pathfinder-bestiary-3-items/JxdXfCP6ayaEEZND.htm)|Jaws|auto-trad|
|[jXHs8IwojZHlZUSA.htm](pathfinder-bestiary-3-items/jXHs8IwojZHlZUSA.htm)|Wind Mastery|auto-trad|
|[JxnXxf6brDnOqBXG.htm](pathfinder-bestiary-3-items/JxnXxf6brDnOqBXG.htm)|Sweltering Heat|auto-trad|
|[JXy1hnyoxsc0xnCO.htm](pathfinder-bestiary-3-items/JXy1hnyoxsc0xnCO.htm)|Telepathy 100 feet|auto-trad|
|[JYnZKoZ4Immi2yaz.htm](pathfinder-bestiary-3-items/JYnZKoZ4Immi2yaz.htm)|Divine Innate Spells|auto-trad|
|[Jyzm7OFRyidSiJk0.htm](pathfinder-bestiary-3-items/Jyzm7OFRyidSiJk0.htm)|Spore Domination|auto-trad|
|[Jzdce7Z64aM5gu70.htm](pathfinder-bestiary-3-items/Jzdce7Z64aM5gu70.htm)|Phantom Sword|auto-trad|
|[JZeE5BqCANDTf6hq.htm](pathfinder-bestiary-3-items/JZeE5BqCANDTf6hq.htm)|Change Shape|auto-trad|
|[jZf3j2xK5YKD3uC0.htm](pathfinder-bestiary-3-items/jZf3j2xK5YKD3uC0.htm)|Activate Defenses|auto-trad|
|[jzI14zm2om9KJeOM.htm](pathfinder-bestiary-3-items/jzI14zm2om9KJeOM.htm)|Devourer of Swarms|auto-trad|
|[K0pcy2s3gTJZZK7a.htm](pathfinder-bestiary-3-items/K0pcy2s3gTJZZK7a.htm)|Stormsight|auto-trad|
|[k1fBI6krs8NLi3AR.htm](pathfinder-bestiary-3-items/k1fBI6krs8NLi3AR.htm)|Pitchfork|auto-trad|
|[k1FCB3wNkyv3rnly.htm](pathfinder-bestiary-3-items/k1FCB3wNkyv3rnly.htm)|Vortex|auto-trad|
|[k2B8t1pzRzSkO2qC.htm](pathfinder-bestiary-3-items/k2B8t1pzRzSkO2qC.htm)|Talon|auto-trad|
|[k2HBbsayvnQzyEIo.htm](pathfinder-bestiary-3-items/k2HBbsayvnQzyEIo.htm)|Death Umbra|auto-trad|
|[k2Num39uDHGiZwTm.htm](pathfinder-bestiary-3-items/k2Num39uDHGiZwTm.htm)|Breath Weapon|auto-trad|
|[k2oujPtkWYkCjV8t.htm](pathfinder-bestiary-3-items/k2oujPtkWYkCjV8t.htm)|Jaws|auto-trad|
|[K32Ag7GpssjobCOz.htm](pathfinder-bestiary-3-items/K32Ag7GpssjobCOz.htm)|Darkvision|auto-trad|
|[k3a4ohAlfjdpm7Ck.htm](pathfinder-bestiary-3-items/k3a4ohAlfjdpm7Ck.htm)|Grab|auto-trad|
|[k3dgXH9CegWMvwQi.htm](pathfinder-bestiary-3-items/k3dgXH9CegWMvwQi.htm)|Consume Souls|auto-trad|
|[k4kguAJD9K2mg4zY.htm](pathfinder-bestiary-3-items/k4kguAJD9K2mg4zY.htm)|Impossible Stature|auto-trad|
|[k5a2kb9JPuGSAe7J.htm](pathfinder-bestiary-3-items/k5a2kb9JPuGSAe7J.htm)|Darkvision|auto-trad|
|[K5ozGM2drmKhGIoI.htm](pathfinder-bestiary-3-items/K5ozGM2drmKhGIoI.htm)|Aquatic Ambush|auto-trad|
|[k6bhkrULfZctxZZa.htm](pathfinder-bestiary-3-items/k6bhkrULfZctxZZa.htm)|Landslide|auto-trad|
|[k6KXYRt0pYf0ds2B.htm](pathfinder-bestiary-3-items/k6KXYRt0pYf0ds2B.htm)|Attack of Opportunity|auto-trad|
|[K6p90DP5TOgu1uHl.htm](pathfinder-bestiary-3-items/K6p90DP5TOgu1uHl.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[K8HY6MgNzIvDLuDT.htm](pathfinder-bestiary-3-items/K8HY6MgNzIvDLuDT.htm)|Bone Chariot|auto-trad|
|[K9b86nLjHgbbzFnm.htm](pathfinder-bestiary-3-items/K9b86nLjHgbbzFnm.htm)|Fist|auto-trad|
|[ka8ggPLmvKPNC19r.htm](pathfinder-bestiary-3-items/ka8ggPLmvKPNC19r.htm)|Wind Blast|auto-trad|
|[kacehtkxmzv3j5Zn.htm](pathfinder-bestiary-3-items/kacehtkxmzv3j5Zn.htm)|Primal Prepared Spells|auto-trad|
|[kB4Y7lsPktJzaUtm.htm](pathfinder-bestiary-3-items/kB4Y7lsPktJzaUtm.htm)|Countered by Metal|auto-trad|
|[kBmOKMaUjcEjTeia.htm](pathfinder-bestiary-3-items/kBmOKMaUjcEjTeia.htm)|Occult Innate Spells|auto-trad|
|[KbRk42Ska9TyPynC.htm](pathfinder-bestiary-3-items/KbRk42Ska9TyPynC.htm)|Dab|auto-trad|
|[KD9WyZyMHgs07nLa.htm](pathfinder-bestiary-3-items/KD9WyZyMHgs07nLa.htm)|Claw|auto-trad|
|[KdePPltFxHzCPJrm.htm](pathfinder-bestiary-3-items/KdePPltFxHzCPJrm.htm)|At-Will Spells|auto-trad|
|[KDhzo4KVYsJvSpXo.htm](pathfinder-bestiary-3-items/KDhzo4KVYsJvSpXo.htm)|Keelhaul|auto-trad|
|[KdYM2NV3yWc57PNc.htm](pathfinder-bestiary-3-items/KdYM2NV3yWc57PNc.htm)|Darkvision|auto-trad|
|[Ke8jPPnQzzzeKXWw.htm](pathfinder-bestiary-3-items/Ke8jPPnQzzzeKXWw.htm)|Darkvision|auto-trad|
|[KEJNkeF17Hd5RgpW.htm](pathfinder-bestiary-3-items/KEJNkeF17Hd5RgpW.htm)|Feral Trackers|auto-trad|
|[KewwLz9N8mWqhTpA.htm](pathfinder-bestiary-3-items/KewwLz9N8mWqhTpA.htm)|Frightful Presence|auto-trad|
|[kfBNKa5ltxDIa0qG.htm](pathfinder-bestiary-3-items/kfBNKa5ltxDIa0qG.htm)|At-Will Spells|auto-trad|
|[KftIJAHBIGp8Mnad.htm](pathfinder-bestiary-3-items/KftIJAHBIGp8Mnad.htm)|Camouflage|auto-trad|
|[KGCeRo6qPcjltHnA.htm](pathfinder-bestiary-3-items/KGCeRo6qPcjltHnA.htm)|Wavesense (Imprecise) 10 feet|auto-trad|
|[KgidUZZqAF6dfZdR.htm](pathfinder-bestiary-3-items/KgidUZZqAF6dfZdR.htm)|Bo Staff|auto-trad|
|[KGyAqL8XCgBPKJwN.htm](pathfinder-bestiary-3-items/KGyAqL8XCgBPKJwN.htm)|Divine Lightning|auto-trad|
|[khSydk2fZucaWpwr.htm](pathfinder-bestiary-3-items/khSydk2fZucaWpwr.htm)|At-Will Spells|auto-trad|
|[KIEOH7Hzo0bgoAnC.htm](pathfinder-bestiary-3-items/KIEOH7Hzo0bgoAnC.htm)|Darkvision|auto-trad|
|[kimzS4sVtC1JuBK6.htm](pathfinder-bestiary-3-items/kimzS4sVtC1JuBK6.htm)|Tail|auto-trad|
|[KjameIcuKwYkyTzJ.htm](pathfinder-bestiary-3-items/KjameIcuKwYkyTzJ.htm)|Cooperative Hunting|auto-trad|
|[kjCwpEZhQpTXIyZj.htm](pathfinder-bestiary-3-items/kjCwpEZhQpTXIyZj.htm)|Defensive Disarm|auto-trad|
|[KjLaF8NJBaPjsRAz.htm](pathfinder-bestiary-3-items/KjLaF8NJBaPjsRAz.htm)|Gnash|auto-trad|
|[KkkIZP8XxpPJcECa.htm](pathfinder-bestiary-3-items/KkkIZP8XxpPJcECa.htm)|Flaming Greataxe|auto-trad|
|[KkSN8aLpddZGTcZq.htm](pathfinder-bestiary-3-items/KkSN8aLpddZGTcZq.htm)|Greater Constrict|auto-trad|
|[klPNjA3PDnepkkn6.htm](pathfinder-bestiary-3-items/klPNjA3PDnepkkn6.htm)|At-Will Spells|auto-trad|
|[KluTBobaNmcJwryy.htm](pathfinder-bestiary-3-items/KluTBobaNmcJwryy.htm)|Darkvision|auto-trad|
|[KMdZUSARMPSywMsn.htm](pathfinder-bestiary-3-items/KMdZUSARMPSywMsn.htm)|Divine Aegis|auto-trad|
|[kMfzeWG6rv2XM68m.htm](pathfinder-bestiary-3-items/kMfzeWG6rv2XM68m.htm)|Blade-Leg|auto-trad|
|[KmgwRHZExpyqDBij.htm](pathfinder-bestiary-3-items/KmgwRHZExpyqDBij.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[knIdAZZkuVAcH33t.htm](pathfinder-bestiary-3-items/knIdAZZkuVAcH33t.htm)|Claw|auto-trad|
|[kNl1E67lBZGpSC5U.htm](pathfinder-bestiary-3-items/kNl1E67lBZGpSC5U.htm)|Gory Tendril|auto-trad|
|[KnlMKaajUHNW24mj.htm](pathfinder-bestiary-3-items/KnlMKaajUHNW24mj.htm)|Crossbow|auto-trad|
|[KOan7i2UeawwyO2v.htm](pathfinder-bestiary-3-items/KOan7i2UeawwyO2v.htm)|Push|auto-trad|
|[koOLkiJRcE1XYMxS.htm](pathfinder-bestiary-3-items/koOLkiJRcE1XYMxS.htm)|Darkvision|auto-trad|
|[kpB9dRBV5LWoW0uG.htm](pathfinder-bestiary-3-items/kpB9dRBV5LWoW0uG.htm)|Darkvision|auto-trad|
|[kpvnHl1XmqHSX6rP.htm](pathfinder-bestiary-3-items/kpvnHl1XmqHSX6rP.htm)|At-Will Spells|auto-trad|
|[KPwsQn8bO8tcLOgw.htm](pathfinder-bestiary-3-items/KPwsQn8bO8tcLOgw.htm)|Frightful Presence|auto-trad|
|[KpWuD5FV2RQBjAeo.htm](pathfinder-bestiary-3-items/KpWuD5FV2RQBjAeo.htm)|Primal Prepared Spells|auto-trad|
|[kpx9ES2ZOnjt0XMh.htm](pathfinder-bestiary-3-items/kpx9ES2ZOnjt0XMh.htm)|Troop Defenses|auto-trad|
|[kpySQfuxcXr6ztFA.htm](pathfinder-bestiary-3-items/kpySQfuxcXr6ztFA.htm)|Halberd|auto-trad|
|[kQaDhJaLxNJw4l8l.htm](pathfinder-bestiary-3-items/kQaDhJaLxNJw4l8l.htm)|Low-Light Vision|auto-trad|
|[kqcQU0n1PhfM4fgW.htm](pathfinder-bestiary-3-items/kqcQU0n1PhfM4fgW.htm)|Swarm Mind|auto-trad|
|[KqQvNC6PQXFL9bre.htm](pathfinder-bestiary-3-items/KqQvNC6PQXFL9bre.htm)|Occult Prepared Spells|auto-trad|
|[KRjbKjp9iT8REk4M.htm](pathfinder-bestiary-3-items/KRjbKjp9iT8REk4M.htm)|Ecstatic Hunger|auto-trad|
|[krWu51yBcWRllWxP.htm](pathfinder-bestiary-3-items/krWu51yBcWRllWxP.htm)|Sand Spin|auto-trad|
|[ks2bGQlMCszTgN6c.htm](pathfinder-bestiary-3-items/ks2bGQlMCszTgN6c.htm)|Bardic Lore|auto-trad|
|[ks76ZS8gWSFgrdl9.htm](pathfinder-bestiary-3-items/ks76ZS8gWSFgrdl9.htm)|Catch Rock|auto-trad|
|[kSMYzNRzwjJcLeCe.htm](pathfinder-bestiary-3-items/kSMYzNRzwjJcLeCe.htm)|Tail|auto-trad|
|[kt9Kmearp1t75oDh.htm](pathfinder-bestiary-3-items/kt9Kmearp1t75oDh.htm)|Grab|auto-trad|
|[KUErEL0JPCYaIeu0.htm](pathfinder-bestiary-3-items/KUErEL0JPCYaIeu0.htm)|Electrical Field|auto-trad|
|[KvEbx6NMqRfeeaD2.htm](pathfinder-bestiary-3-items/KvEbx6NMqRfeeaD2.htm)|Darkvision|auto-trad|
|[kWARaiGoyPB0KHgj.htm](pathfinder-bestiary-3-items/kWARaiGoyPB0KHgj.htm)|Spirit Body|auto-trad|
|[kWiBTKt3h01RReit.htm](pathfinder-bestiary-3-items/kWiBTKt3h01RReit.htm)|Darkvision|auto-trad|
|[kX3JkP0HZPIQ7vWj.htm](pathfinder-bestiary-3-items/kX3JkP0HZPIQ7vWj.htm)|Light Blindness|auto-trad|
|[kXXLa1y6PCq4db69.htm](pathfinder-bestiary-3-items/kXXLa1y6PCq4db69.htm)|Occult Spontaneous Spells|auto-trad|
|[KXYQjUBjJFbx6t3Z.htm](pathfinder-bestiary-3-items/KXYQjUBjJFbx6t3Z.htm)|Claw|auto-trad|
|[kYmteRQa3LahCveN.htm](pathfinder-bestiary-3-items/kYmteRQa3LahCveN.htm)|Touch of Ages|auto-trad|
|[kZCjL41fVSpE8vkC.htm](pathfinder-bestiary-3-items/kZCjL41fVSpE8vkC.htm)|Skip Between|auto-trad|
|[KZhlld4EKQcCTBjw.htm](pathfinder-bestiary-3-items/KZhlld4EKQcCTBjw.htm)|Swarm Mind|auto-trad|
|[kziqHkR0lOsYBKJz.htm](pathfinder-bestiary-3-items/kziqHkR0lOsYBKJz.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[kzOsIUUeSNgTP9xK.htm](pathfinder-bestiary-3-items/kzOsIUUeSNgTP9xK.htm)|At-Will Spells|auto-trad|
|[kzwDYGXsWl0wyTwN.htm](pathfinder-bestiary-3-items/kzwDYGXsWl0wyTwN.htm)|Light Hammer|auto-trad|
|[L0AaJkB1CVD61yGv.htm](pathfinder-bestiary-3-items/L0AaJkB1CVD61yGv.htm)|Master's Eyes|auto-trad|
|[L2LnR1T2MiBlznWr.htm](pathfinder-bestiary-3-items/L2LnR1T2MiBlznWr.htm)|Draconic Momentum|auto-trad|
|[l2ymfoz6uzs9Q7vM.htm](pathfinder-bestiary-3-items/l2ymfoz6uzs9Q7vM.htm)|Constant Spells|auto-trad|
|[l2ZTQQTqrVwPvUY1.htm](pathfinder-bestiary-3-items/l2ZTQQTqrVwPvUY1.htm)|Primal Innate Spells|auto-trad|
|[L3k41cRxvKs5ZH4L.htm](pathfinder-bestiary-3-items/L3k41cRxvKs5ZH4L.htm)|Morpheme Glyph|auto-trad|
|[l3QKE8kkrmNe5aZO.htm](pathfinder-bestiary-3-items/l3QKE8kkrmNe5aZO.htm)|Darkvision|auto-trad|
|[L4eZWrxPCscqkfv1.htm](pathfinder-bestiary-3-items/L4eZWrxPCscqkfv1.htm)|Hurl Javelins!|auto-trad|
|[l6dezchxG4CL8dny.htm](pathfinder-bestiary-3-items/l6dezchxG4CL8dny.htm)|Burning Wings|auto-trad|
|[L87Xx2Cd9z06I697.htm](pathfinder-bestiary-3-items/L87Xx2Cd9z06I697.htm)|Divine Innate Spells|auto-trad|
|[l8Vwgjba06YNgLZg.htm](pathfinder-bestiary-3-items/l8Vwgjba06YNgLZg.htm)|Regeneration 20 (Deactivated by Evil)|auto-trad|
|[L9XsoNHRPeOdO6cL.htm](pathfinder-bestiary-3-items/L9XsoNHRPeOdO6cL.htm)|Striking Koa|auto-trad|
|[LaB8UoWpNQqZKM30.htm](pathfinder-bestiary-3-items/LaB8UoWpNQqZKM30.htm)|Rolling Thunder|auto-trad|
|[LAhnCuHlBH3zJBoP.htm](pathfinder-bestiary-3-items/LAhnCuHlBH3zJBoP.htm)|Golem Antimagic|auto-trad|
|[Lb8baU3vfeAtn1Nw.htm](pathfinder-bestiary-3-items/Lb8baU3vfeAtn1Nw.htm)|Cower|auto-trad|
|[lbG0r8knl6cp3RdW.htm](pathfinder-bestiary-3-items/lbG0r8knl6cp3RdW.htm)|Claw|auto-trad|
|[lBss0k9IK6iCcQEo.htm](pathfinder-bestiary-3-items/lBss0k9IK6iCcQEo.htm)|Excruciating Enzyme|auto-trad|
|[LcChlrMndc88kp30.htm](pathfinder-bestiary-3-items/LcChlrMndc88kp30.htm)|Sunlight Powerlessness|auto-trad|
|[LcdaZzKwze9fmC6D.htm](pathfinder-bestiary-3-items/LcdaZzKwze9fmC6D.htm)|Defensive Slam|auto-trad|
|[lcGPZQiY2GFPXOVQ.htm](pathfinder-bestiary-3-items/lcGPZQiY2GFPXOVQ.htm)|Stunning Flurry|auto-trad|
|[lciayGn2ma9dSbcM.htm](pathfinder-bestiary-3-items/lciayGn2ma9dSbcM.htm)|Curse of Darkness|auto-trad|
|[lcYKbUtg1W8w9cul.htm](pathfinder-bestiary-3-items/lcYKbUtg1W8w9cul.htm)|Wing|auto-trad|
|[lcYudXUlkKY7E3iT.htm](pathfinder-bestiary-3-items/lcYudXUlkKY7E3iT.htm)|Trample|auto-trad|
|[LCYVlQZcfJT8AY75.htm](pathfinder-bestiary-3-items/LCYVlQZcfJT8AY75.htm)|Primal Innate Spells|auto-trad|
|[lD1oJbNSEUiWgWzZ.htm](pathfinder-bestiary-3-items/lD1oJbNSEUiWgWzZ.htm)|Attack of Opportunity|auto-trad|
|[lD2k5BU1mLju5ewn.htm](pathfinder-bestiary-3-items/lD2k5BU1mLju5ewn.htm)|Blade|auto-trad|
|[LD67mqtsXztjCTmJ.htm](pathfinder-bestiary-3-items/LD67mqtsXztjCTmJ.htm)|Regeneration 20 (Deactivated by Fire)|auto-trad|
|[LdhQrUO09um9htY6.htm](pathfinder-bestiary-3-items/LdhQrUO09um9htY6.htm)|Drain Blood|auto-trad|
|[lDtCFaHXdvj9JdET.htm](pathfinder-bestiary-3-items/lDtCFaHXdvj9JdET.htm)|Shortbow|auto-trad|
|[LDTFrdNHyvizP1mX.htm](pathfinder-bestiary-3-items/LDTFrdNHyvizP1mX.htm)|At-Will Spells|auto-trad|
|[le8d6e5N7Wjb7Rgo.htm](pathfinder-bestiary-3-items/le8d6e5N7Wjb7Rgo.htm)|Divine Innate Spells|auto-trad|
|[LehQoRloBWVwpTHh.htm](pathfinder-bestiary-3-items/LehQoRloBWVwpTHh.htm)|Mangle|auto-trad|
|[leLKKGPbDbUPHstg.htm](pathfinder-bestiary-3-items/leLKKGPbDbUPHstg.htm)|Darkvision|auto-trad|
|[lFFJEvkkHiFKVLEu.htm](pathfinder-bestiary-3-items/lFFJEvkkHiFKVLEu.htm)|Pinch|auto-trad|
|[LFIyH1YUMUoGUmPX.htm](pathfinder-bestiary-3-items/LFIyH1YUMUoGUmPX.htm)|Countered by Metal|auto-trad|
|[LfpK0azkldAisbCY.htm](pathfinder-bestiary-3-items/LfpK0azkldAisbCY.htm)|Extend Limb|auto-trad|
|[lgdEGoF8XPhWshor.htm](pathfinder-bestiary-3-items/lgdEGoF8XPhWshor.htm)|At-Will Spells|auto-trad|
|[Lgf1CXruVEasnLms.htm](pathfinder-bestiary-3-items/Lgf1CXruVEasnLms.htm)|Tremorsense (Imprecise) within their entire bound home|auto-trad|
|[LH24pBPDHDAEH6TV.htm](pathfinder-bestiary-3-items/LH24pBPDHDAEH6TV.htm)|Create Golden Apple|auto-trad|
|[LhA2xtluaWsZrc1T.htm](pathfinder-bestiary-3-items/LhA2xtluaWsZrc1T.htm)|Greater Darkvision|auto-trad|
|[lHGpzeVpbkAh9PNL.htm](pathfinder-bestiary-3-items/lHGpzeVpbkAh9PNL.htm)|Bastard Sword|auto-trad|
|[liA5VGcqAKl36Vqh.htm](pathfinder-bestiary-3-items/liA5VGcqAKl36Vqh.htm)|Darkvision|auto-trad|
|[liadzf6LKysC2rK8.htm](pathfinder-bestiary-3-items/liadzf6LKysC2rK8.htm)|Low-Light Vision|auto-trad|
|[liMrTZS1ECyEEh6I.htm](pathfinder-bestiary-3-items/liMrTZS1ECyEEh6I.htm)|At-Will Spells|auto-trad|
|[lIN8nRgTYzPOm90G.htm](pathfinder-bestiary-3-items/lIN8nRgTYzPOm90G.htm)|Incalculable Fangs|auto-trad|
|[LIQcObuGu1wqBnEw.htm](pathfinder-bestiary-3-items/LIQcObuGu1wqBnEw.htm)|Grab|auto-trad|
|[lj6vRNNZwXQmeqG7.htm](pathfinder-bestiary-3-items/lj6vRNNZwXQmeqG7.htm)|Claw|auto-trad|
|[lJDxq1w6WvzVEghk.htm](pathfinder-bestiary-3-items/lJDxq1w6WvzVEghk.htm)|Sprint|auto-trad|
|[Ljg3kgAdR1sVGBeL.htm](pathfinder-bestiary-3-items/Ljg3kgAdR1sVGBeL.htm)|Echolocation 60 feet|auto-trad|
|[ljmD4eOFLneTqsb9.htm](pathfinder-bestiary-3-items/ljmD4eOFLneTqsb9.htm)|At-Will Spells|auto-trad|
|[lK75Yi29CDBjFNYu.htm](pathfinder-bestiary-3-items/lK75Yi29CDBjFNYu.htm)|Darkvision|auto-trad|
|[LKCha1KO0muUvUr0.htm](pathfinder-bestiary-3-items/LKCha1KO0muUvUr0.htm)|Hellwasp Venom|auto-trad|
|[lkfuADlIxVnyt6EH.htm](pathfinder-bestiary-3-items/lkfuADlIxVnyt6EH.htm)|Scimitar|auto-trad|
|[lL49wJa4ig4V0ag1.htm](pathfinder-bestiary-3-items/lL49wJa4ig4V0ag1.htm)|Record Audio|auto-trad|
|[Ll9EKS0upQ66GUIJ.htm](pathfinder-bestiary-3-items/Ll9EKS0upQ66GUIJ.htm)|Low-Light Vision|auto-trad|
|[LLArpiTAsI8pUto2.htm](pathfinder-bestiary-3-items/LLArpiTAsI8pUto2.htm)|Master of the Home|auto-trad|
|[llRkvAv40JIHPwXk.htm](pathfinder-bestiary-3-items/llRkvAv40JIHPwXk.htm)|Horn|auto-trad|
|[LMRK013cxClnSMlQ.htm](pathfinder-bestiary-3-items/LMRK013cxClnSMlQ.htm)|Tail|auto-trad|
|[lmUd66np5PVIvPFr.htm](pathfinder-bestiary-3-items/lmUd66np5PVIvPFr.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[Ln0qmde8wRjzuIos.htm](pathfinder-bestiary-3-items/Ln0qmde8wRjzuIos.htm)|Moisture Dependency|auto-trad|
|[lnftcCa5nWLimB4r.htm](pathfinder-bestiary-3-items/lnftcCa5nWLimB4r.htm)|Wing Thrash|auto-trad|
|[lNuMFP2gGs9IFNk7.htm](pathfinder-bestiary-3-items/lNuMFP2gGs9IFNk7.htm)|Telepathy 30 feet|auto-trad|
|[lO3tFaQKyvdDCf09.htm](pathfinder-bestiary-3-items/lO3tFaQKyvdDCf09.htm)|Low-Light Vision|auto-trad|
|[LoLS2Gw8HzHFPWld.htm](pathfinder-bestiary-3-items/LoLS2Gw8HzHFPWld.htm)|Languages|auto-trad|
|[LorLNjRUhElv6hVC.htm](pathfinder-bestiary-3-items/LorLNjRUhElv6hVC.htm)|Tail|auto-trad|
|[LP9djZsLKMnkBo3M.htm](pathfinder-bestiary-3-items/LP9djZsLKMnkBo3M.htm)|Draconic Momentum|auto-trad|
|[lp9zpRImtn8gAWuk.htm](pathfinder-bestiary-3-items/lp9zpRImtn8gAWuk.htm)|Bloodsense (Imprecise) 90 feet|auto-trad|
|[lpbIToo47rzzxQRz.htm](pathfinder-bestiary-3-items/lpbIToo47rzzxQRz.htm)|Darkvision|auto-trad|
|[lPHcKkQiBqbtQQ8z.htm](pathfinder-bestiary-3-items/lPHcKkQiBqbtQQ8z.htm)|Telepathy 100 feet|auto-trad|
|[lPUDxg0sCW4m6DZX.htm](pathfinder-bestiary-3-items/lPUDxg0sCW4m6DZX.htm)|Dagger|auto-trad|
|[lq5shiJkFR0pdE6O.htm](pathfinder-bestiary-3-items/lq5shiJkFR0pdE6O.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[lQ9VDly9F5WovfxG.htm](pathfinder-bestiary-3-items/lQ9VDly9F5WovfxG.htm)|Tail|auto-trad|
|[lqiFGbLGpuC1xc0l.htm](pathfinder-bestiary-3-items/lqiFGbLGpuC1xc0l.htm)|Grab|auto-trad|
|[lQKNHnyXpn3nHnzE.htm](pathfinder-bestiary-3-items/lQKNHnyXpn3nHnzE.htm)|Swarm Mind|auto-trad|
|[LRFB4rNU7O66JVlQ.htm](pathfinder-bestiary-3-items/LRFB4rNU7O66JVlQ.htm)|Grab|auto-trad|
|[LrGPFAVrLLeOkRJj.htm](pathfinder-bestiary-3-items/LrGPFAVrLLeOkRJj.htm)|Whisper Earworm|auto-trad|
|[LrtLY1p0wVwq0afd.htm](pathfinder-bestiary-3-items/LrtLY1p0wVwq0afd.htm)|Jaws|auto-trad|
|[Lscs4sD2vIZEzZIX.htm](pathfinder-bestiary-3-items/Lscs4sD2vIZEzZIX.htm)|Tail|auto-trad|
|[lsECqEvllEKtOv5g.htm](pathfinder-bestiary-3-items/lsECqEvllEKtOv5g.htm)|Chain Shot|auto-trad|
|[Lt0Vd3S5Hji79WEW.htm](pathfinder-bestiary-3-items/Lt0Vd3S5Hji79WEW.htm)|Draconic Frenzy|auto-trad|
|[LtMnRFGdEZ9u0QFh.htm](pathfinder-bestiary-3-items/LtMnRFGdEZ9u0QFh.htm)|Pluck Dream|auto-trad|
|[lTNuGoh4u8QMy0pk.htm](pathfinder-bestiary-3-items/lTNuGoh4u8QMy0pk.htm)|Tendril|auto-trad|
|[lufhjv4m2WdjSTZs.htm](pathfinder-bestiary-3-items/lufhjv4m2WdjSTZs.htm)|Countered by Water|auto-trad|
|[lUKDtZBh5MBovcUd.htm](pathfinder-bestiary-3-items/lUKDtZBh5MBovcUd.htm)|Negative Healing|auto-trad|
|[LupnDs04lfZkJ52r.htm](pathfinder-bestiary-3-items/LupnDs04lfZkJ52r.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[LUWAzCWdvMJPK5Am.htm](pathfinder-bestiary-3-items/LUWAzCWdvMJPK5Am.htm)|Hide and Seek|auto-trad|
|[lV8lCapoUOFDMPz8.htm](pathfinder-bestiary-3-items/lV8lCapoUOFDMPz8.htm)|Pukwudgie Poison|auto-trad|
|[LVNb1MU1A4uFDYSo.htm](pathfinder-bestiary-3-items/LVNb1MU1A4uFDYSo.htm)|Push|auto-trad|
|[LVzeZlyXzpgBSv5s.htm](pathfinder-bestiary-3-items/LVzeZlyXzpgBSv5s.htm)|Jaws|auto-trad|
|[lwLwA2EvXhOPYIuA.htm](pathfinder-bestiary-3-items/lwLwA2EvXhOPYIuA.htm)|At-Will Spells|auto-trad|
|[lwULACd9cg2TzXz5.htm](pathfinder-bestiary-3-items/lwULACd9cg2TzXz5.htm)|Deep Breath|auto-trad|
|[Lx5Q366mof1O0MPp.htm](pathfinder-bestiary-3-items/Lx5Q366mof1O0MPp.htm)|Form Up|auto-trad|
|[lxcQL19jOkUey7nF.htm](pathfinder-bestiary-3-items/lxcQL19jOkUey7nF.htm)|Blowgun|auto-trad|
|[lXlQ3g7mZu8LDrvB.htm](pathfinder-bestiary-3-items/lXlQ3g7mZu8LDrvB.htm)|Woodland Stride|auto-trad|
|[lxWOOSDzdS3gKtgq.htm](pathfinder-bestiary-3-items/lxWOOSDzdS3gKtgq.htm)|Swarm Mind|auto-trad|
|[LY9m5BMsAb8g3SIx.htm](pathfinder-bestiary-3-items/LY9m5BMsAb8g3SIx.htm)|Negative Healing|auto-trad|
|[lYRZM8I1flblujiU.htm](pathfinder-bestiary-3-items/lYRZM8I1flblujiU.htm)|Negative Healing|auto-trad|
|[LzvPd4UTa6iofTzD.htm](pathfinder-bestiary-3-items/LzvPd4UTa6iofTzD.htm)|Hoof|auto-trad|
|[lZYIDbiemHOhYhm5.htm](pathfinder-bestiary-3-items/lZYIDbiemHOhYhm5.htm)|Abandon Corpse|auto-trad|
|[m03bX0sD03E32bCM.htm](pathfinder-bestiary-3-items/m03bX0sD03E32bCM.htm)|Constant Spells|auto-trad|
|[m0pBEDByK2E42m1L.htm](pathfinder-bestiary-3-items/m0pBEDByK2E42m1L.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[m1kfe61fI2Y0NgZd.htm](pathfinder-bestiary-3-items/m1kfe61fI2Y0NgZd.htm)|Darkvision|auto-trad|
|[M1p2Iq57MOBT9II9.htm](pathfinder-bestiary-3-items/M1p2Iq57MOBT9II9.htm)|Darkvision|auto-trad|
|[M1rzEefuLXlWlYN8.htm](pathfinder-bestiary-3-items/M1rzEefuLXlWlYN8.htm)|Colossus's Grasp|auto-trad|
|[m2GqanJ6fVKX39SQ.htm](pathfinder-bestiary-3-items/m2GqanJ6fVKX39SQ.htm)|Grab|auto-trad|
|[m2xn36z0bizgQRSl.htm](pathfinder-bestiary-3-items/m2xn36z0bizgQRSl.htm)|Polyglot|auto-trad|
|[M3azf7Pp6YDlxgN3.htm](pathfinder-bestiary-3-items/M3azf7Pp6YDlxgN3.htm)|Quick Escape|auto-trad|
|[M3e5MWLAt9COPusW.htm](pathfinder-bestiary-3-items/M3e5MWLAt9COPusW.htm)|Tendril|auto-trad|
|[m3ZfC7sl35Uue2KZ.htm](pathfinder-bestiary-3-items/m3ZfC7sl35Uue2KZ.htm)|Low-Light Vision|auto-trad|
|[M4gK880IlR5fkws0.htm](pathfinder-bestiary-3-items/M4gK880IlR5fkws0.htm)|Darkvision|auto-trad|
|[M4yZRTxw1DkV1j2m.htm](pathfinder-bestiary-3-items/M4yZRTxw1DkV1j2m.htm)|Grab|auto-trad|
|[M68tcWaPYgJDf7pp.htm](pathfinder-bestiary-3-items/M68tcWaPYgJDf7pp.htm)|Launch|auto-trad|
|[M6bPLFIXKoFFgM2u.htm](pathfinder-bestiary-3-items/M6bPLFIXKoFFgM2u.htm)|Darkvision|auto-trad|
|[M6HiBc42J8BI5rZo.htm](pathfinder-bestiary-3-items/M6HiBc42J8BI5rZo.htm)|Unstable Magic|auto-trad|
|[M6VhLwSsBbyN4CsJ.htm](pathfinder-bestiary-3-items/M6VhLwSsBbyN4CsJ.htm)|Greatpick|auto-trad|
|[M7bSwayItCZu1H43.htm](pathfinder-bestiary-3-items/M7bSwayItCZu1H43.htm)|Crush Chitin|auto-trad|
|[m8Ycgo6ZGdfthZSw.htm](pathfinder-bestiary-3-items/m8Ycgo6ZGdfthZSw.htm)|Sanguine Spray|auto-trad|
|[M9Gq6cRX2w40Q3Yb.htm](pathfinder-bestiary-3-items/M9Gq6cRX2w40Q3Yb.htm)|Darkvision|auto-trad|
|[maPEoGc6nv9lMP5R.htm](pathfinder-bestiary-3-items/maPEoGc6nv9lMP5R.htm)|Fist|auto-trad|
|[MAu9spTENeddU6Zv.htm](pathfinder-bestiary-3-items/MAu9spTENeddU6Zv.htm)|Foot|auto-trad|
|[maVqItGv6VD6w8M3.htm](pathfinder-bestiary-3-items/maVqItGv6VD6w8M3.htm)|Roiling Rebuke|auto-trad|
|[MB3A4i2cdRbTv3vg.htm](pathfinder-bestiary-3-items/MB3A4i2cdRbTv3vg.htm)|Occult Innate Spells|auto-trad|
|[MbX8MuX8mUeGVwXv.htm](pathfinder-bestiary-3-items/MbX8MuX8mUeGVwXv.htm)|Stunning Electricity|auto-trad|
|[MCvCX567cQVCilQv.htm](pathfinder-bestiary-3-items/MCvCX567cQVCilQv.htm)|Claw|auto-trad|
|[MdAaSnMbM5KOAlWl.htm](pathfinder-bestiary-3-items/MdAaSnMbM5KOAlWl.htm)|Pummeling Assault|auto-trad|
|[MDEHgLAgesfgrGrt.htm](pathfinder-bestiary-3-items/MDEHgLAgesfgrGrt.htm)|Low-Light Vision|auto-trad|
|[MDqLlKyAsgRUfism.htm](pathfinder-bestiary-3-items/MDqLlKyAsgRUfism.htm)|Constant Spells|auto-trad|
|[mEcsR2Hq0xwLS5m1.htm](pathfinder-bestiary-3-items/mEcsR2Hq0xwLS5m1.htm)|Shadow Whip|auto-trad|
|[MEdTOOe1fbIYRy1N.htm](pathfinder-bestiary-3-items/MEdTOOe1fbIYRy1N.htm)|Jaws|auto-trad|
|[mep6FodvFU5OWGuk.htm](pathfinder-bestiary-3-items/mep6FodvFU5OWGuk.htm)|Consecration Vulnerability|auto-trad|
|[Mf6HjC4fVjp3RLPm.htm](pathfinder-bestiary-3-items/Mf6HjC4fVjp3RLPm.htm)|Greater Darkvision|auto-trad|
|[mfooCk9alzoismP0.htm](pathfinder-bestiary-3-items/mfooCk9alzoismP0.htm)|Grab|auto-trad|
|[mfQZq4ONY5lVM7IX.htm](pathfinder-bestiary-3-items/mfQZq4ONY5lVM7IX.htm)|Constant Spells|auto-trad|
|[mFrFiI59EQmtyVk8.htm](pathfinder-bestiary-3-items/mFrFiI59EQmtyVk8.htm)|Scorch Earth|auto-trad|
|[MfS2aGB9sp6OgznC.htm](pathfinder-bestiary-3-items/MfS2aGB9sp6OgznC.htm)|Claw|auto-trad|
|[Mfyiq7MrkDXVAE1n.htm](pathfinder-bestiary-3-items/Mfyiq7MrkDXVAE1n.htm)|Greater Darkvision|auto-trad|
|[MGarveOJoEZEZRDb.htm](pathfinder-bestiary-3-items/MGarveOJoEZEZRDb.htm)|Vine Forest|auto-trad|
|[mGFhVUVLfuBvJdR9.htm](pathfinder-bestiary-3-items/mGFhVUVLfuBvJdR9.htm)|Fist|auto-trad|
|[MgZCPERomWYyl1Nh.htm](pathfinder-bestiary-3-items/MgZCPERomWYyl1Nh.htm)|Inspire Envoy|auto-trad|
|[MhafIYjoiiUIlHV9.htm](pathfinder-bestiary-3-items/MhafIYjoiiUIlHV9.htm)|Darkvision|auto-trad|
|[MHEumHsZqQqW917y.htm](pathfinder-bestiary-3-items/MHEumHsZqQqW917y.htm)|Arcane Innate Spells|auto-trad|
|[MhmAMaJ3NhpSRreS.htm](pathfinder-bestiary-3-items/MhmAMaJ3NhpSRreS.htm)|Girtablilu Venom|auto-trad|
|[MHWhouO72rbCG3vP.htm](pathfinder-bestiary-3-items/MHWhouO72rbCG3vP.htm)|Whip Tail|auto-trad|
|[MIgppLmIY2AofKTC.htm](pathfinder-bestiary-3-items/MIgppLmIY2AofKTC.htm)|Swarming Chimes|auto-trad|
|[Mj3sDpLmMbVTOv59.htm](pathfinder-bestiary-3-items/Mj3sDpLmMbVTOv59.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[MjldvRpasuQZnNAa.htm](pathfinder-bestiary-3-items/MjldvRpasuQZnNAa.htm)|Absorbed Language|auto-trad|
|[mjNu7tTMIDpyD02w.htm](pathfinder-bestiary-3-items/mjNu7tTMIDpyD02w.htm)|Foot|auto-trad|
|[mjVHIWJqPBBMeBhy.htm](pathfinder-bestiary-3-items/mjVHIWJqPBBMeBhy.htm)|Talon|auto-trad|
|[mk57CVzb2MTvBoCX.htm](pathfinder-bestiary-3-items/mk57CVzb2MTvBoCX.htm)|At-Will Spells|auto-trad|
|[MKArQIqeRcZmRwMf.htm](pathfinder-bestiary-3-items/MKArQIqeRcZmRwMf.htm)|Faith Bound|auto-trad|
|[mkOmRandYut3CQzA.htm](pathfinder-bestiary-3-items/mkOmRandYut3CQzA.htm)|Spade|auto-trad|
|[mksu92xcObxdYZwg.htm](pathfinder-bestiary-3-items/mksu92xcObxdYZwg.htm)|Rend Faith|auto-trad|
|[MLb6AX5Sc9kHDdik.htm](pathfinder-bestiary-3-items/MLb6AX5Sc9kHDdik.htm)|Ranseur|auto-trad|
|[mLIM2js0SKePsRgt.htm](pathfinder-bestiary-3-items/mLIM2js0SKePsRgt.htm)|Shortsword|auto-trad|
|[mLT5MDiW1VnsaeKk.htm](pathfinder-bestiary-3-items/mLT5MDiW1VnsaeKk.htm)|Breath Weapon|auto-trad|
|[mM2VhVOnYEHZ9vNy.htm](pathfinder-bestiary-3-items/mM2VhVOnYEHZ9vNy.htm)|Ooze Globule|auto-trad|
|[MN3PR10lW2gEDJPP.htm](pathfinder-bestiary-3-items/MN3PR10lW2gEDJPP.htm)|Swallow Whole|auto-trad|
|[Mn6J9sGKxgEPsSFM.htm](pathfinder-bestiary-3-items/Mn6J9sGKxgEPsSFM.htm)|Breath Weapon|auto-trad|
|[MnCWApQWsNq3LZGf.htm](pathfinder-bestiary-3-items/MnCWApQWsNq3LZGf.htm)|Rapid Evolution|auto-trad|
|[mnF1BUlA5VyNQsJJ.htm](pathfinder-bestiary-3-items/mnF1BUlA5VyNQsJJ.htm)|At-Will Spells|auto-trad|
|[Mnk4wTPNqIwrijFP.htm](pathfinder-bestiary-3-items/Mnk4wTPNqIwrijFP.htm)|Curse of Darkness|auto-trad|
|[mnmEm4rMTXMCnOlp.htm](pathfinder-bestiary-3-items/mnmEm4rMTXMCnOlp.htm)|Shuriken|auto-trad|
|[mNMGvHwMrDdtnVoO.htm](pathfinder-bestiary-3-items/mNMGvHwMrDdtnVoO.htm)|Fast Swallow|auto-trad|
|[mODjcKhuMNY9CZD6.htm](pathfinder-bestiary-3-items/mODjcKhuMNY9CZD6.htm)|Trample|auto-trad|
|[MODzmDCzn3MDbT9X.htm](pathfinder-bestiary-3-items/MODzmDCzn3MDbT9X.htm)|Negative Healing|auto-trad|
|[moI2lATWRRsnrF0Z.htm](pathfinder-bestiary-3-items/moI2lATWRRsnrF0Z.htm)|Raise Shields|auto-trad|
|[MoM2zK3F63zlLyW3.htm](pathfinder-bestiary-3-items/MoM2zK3F63zlLyW3.htm)|Venom Spritz|auto-trad|
|[MOMA8yyykZNLQhay.htm](pathfinder-bestiary-3-items/MOMA8yyykZNLQhay.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[MoqxDoRDqZhJhZwN.htm](pathfinder-bestiary-3-items/MoqxDoRDqZhJhZwN.htm)|Troop Movement|auto-trad|
|[MOxpvxjnhMCu9QIM.htm](pathfinder-bestiary-3-items/MOxpvxjnhMCu9QIM.htm)|Darkvision|auto-trad|
|[mpDohzU1L0FfV4Tr.htm](pathfinder-bestiary-3-items/mpDohzU1L0FfV4Tr.htm)|Toxic Blood|auto-trad|
|[MpmCmnbV6vrc9yPG.htm](pathfinder-bestiary-3-items/MpmCmnbV6vrc9yPG.htm)|Divine Innate Spells|auto-trad|
|[MPnS8zod0eSdIWLP.htm](pathfinder-bestiary-3-items/MPnS8zod0eSdIWLP.htm)|Mist Vision|auto-trad|
|[Mq1buiTxEF2rhIx8.htm](pathfinder-bestiary-3-items/Mq1buiTxEF2rhIx8.htm)|Weep|auto-trad|
|[MqBEcHEyj2aKh7uO.htm](pathfinder-bestiary-3-items/MqBEcHEyj2aKh7uO.htm)|Darkvision|auto-trad|
|[mqJmVUehvZOB9E80.htm](pathfinder-bestiary-3-items/mqJmVUehvZOB9E80.htm)|Null Spirit|auto-trad|
|[mqyNB4TXgzDtKJ3Q.htm](pathfinder-bestiary-3-items/mqyNB4TXgzDtKJ3Q.htm)|Fed by Earth|auto-trad|
|[mqzpWjry4YKks5Hz.htm](pathfinder-bestiary-3-items/mqzpWjry4YKks5Hz.htm)|Darkvision|auto-trad|
|[mR1hECOdG4ihzUpu.htm](pathfinder-bestiary-3-items/mR1hECOdG4ihzUpu.htm)|Drink Blood|auto-trad|
|[mr3JOEATvs2okn6i.htm](pathfinder-bestiary-3-items/mr3JOEATvs2okn6i.htm)|Slippery Grease|auto-trad|
|[MRHUn0OVB4n1HA2L.htm](pathfinder-bestiary-3-items/MRHUn0OVB4n1HA2L.htm)|Sudden Charge|auto-trad|
|[MS8VRrrPU2Us5eB9.htm](pathfinder-bestiary-3-items/MS8VRrrPU2Us5eB9.htm)|Shield Block|auto-trad|
|[MtxKrGdtCxm0wczN.htm](pathfinder-bestiary-3-items/MtxKrGdtCxm0wczN.htm)|Phase Lurch|auto-trad|
|[MUIAw06V4IdebBMA.htm](pathfinder-bestiary-3-items/MUIAw06V4IdebBMA.htm)|Construct Armor (Hardness 8)|auto-trad|
|[MUxEI1u92ZQGjdB8.htm](pathfinder-bestiary-3-items/MUxEI1u92ZQGjdB8.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[MvzDVPMtmss6IZMj.htm](pathfinder-bestiary-3-items/MvzDVPMtmss6IZMj.htm)|Occult Innate Spells|auto-trad|
|[mw396KKLKWJKNSqQ.htm](pathfinder-bestiary-3-items/mw396KKLKWJKNSqQ.htm)|Jaws|auto-trad|
|[MWJz2HDAK5IGRX8W.htm](pathfinder-bestiary-3-items/MWJz2HDAK5IGRX8W.htm)|Darkvision|auto-trad|
|[MWlueD3tLQnzSbjz.htm](pathfinder-bestiary-3-items/MWlueD3tLQnzSbjz.htm)|Sneak Attack|auto-trad|
|[mx7M2JUGpqZUL4LJ.htm](pathfinder-bestiary-3-items/mx7M2JUGpqZUL4LJ.htm)|Projectile Vomit|auto-trad|
|[mX7yAH39yBUykxli.htm](pathfinder-bestiary-3-items/mX7yAH39yBUykxli.htm)|Darkvision|auto-trad|
|[MYdtzKzZtoPS3j70.htm](pathfinder-bestiary-3-items/MYdtzKzZtoPS3j70.htm)|Tail|auto-trad|
|[mYKiwGxlqb75Vo6G.htm](pathfinder-bestiary-3-items/mYKiwGxlqb75Vo6G.htm)|Corrupting Touch|auto-trad|
|[Mynf5Z4xbH7kVwpi.htm](pathfinder-bestiary-3-items/Mynf5Z4xbH7kVwpi.htm)|Spit|auto-trad|
|[MYOFzFbpGCWFCD85.htm](pathfinder-bestiary-3-items/MYOFzFbpGCWFCD85.htm)|Constrict|auto-trad|
|[MzHIjOVqhOXnluws.htm](pathfinder-bestiary-3-items/MzHIjOVqhOXnluws.htm)|Extinguishing Aversion|auto-trad|
|[MzIjuOCLnKBWgj5w.htm](pathfinder-bestiary-3-items/MzIjuOCLnKBWgj5w.htm)|Claw|auto-trad|
|[MZrswDwhii9LR4mL.htm](pathfinder-bestiary-3-items/MZrswDwhii9LR4mL.htm)|-2 to All Saves vs. Emotion Effects|auto-trad|
|[n0IdU8aHDRaDIoOA.htm](pathfinder-bestiary-3-items/n0IdU8aHDRaDIoOA.htm)|At-Will Spells|auto-trad|
|[N0j4xFqLJ9FtXRAP.htm](pathfinder-bestiary-3-items/N0j4xFqLJ9FtXRAP.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[N0L60ChJeeVdQogn.htm](pathfinder-bestiary-3-items/N0L60ChJeeVdQogn.htm)|All-Around Vision|auto-trad|
|[N0n5SsuqfJHWLi1E.htm](pathfinder-bestiary-3-items/N0n5SsuqfJHWLi1E.htm)|Claw|auto-trad|
|[N0pERoSKEmaLOJSB.htm](pathfinder-bestiary-3-items/N0pERoSKEmaLOJSB.htm)|Leech Essence|auto-trad|
|[n249UcrNORZw28Hg.htm](pathfinder-bestiary-3-items/n249UcrNORZw28Hg.htm)|Swarm Mind|auto-trad|
|[N2oBeSLsFrTuwOTn.htm](pathfinder-bestiary-3-items/N2oBeSLsFrTuwOTn.htm)|Frightening Flurry|auto-trad|
|[N2Qsn1VaKlDp82Ga.htm](pathfinder-bestiary-3-items/N2Qsn1VaKlDp82Ga.htm)|Scalding Oil|auto-trad|
|[N4NhBjgFg8WdpS84.htm](pathfinder-bestiary-3-items/N4NhBjgFg8WdpS84.htm)|Negative Healing|auto-trad|
|[N6z6E7Ly3t7eFBoU.htm](pathfinder-bestiary-3-items/N6z6E7Ly3t7eFBoU.htm)|Moon Frenzy|auto-trad|
|[N7bXlw2VkUQPBHUy.htm](pathfinder-bestiary-3-items/N7bXlw2VkUQPBHUy.htm)|Scorch|auto-trad|
|[n7LgURaW8hihxzYn.htm](pathfinder-bestiary-3-items/n7LgURaW8hihxzYn.htm)|Phalanx Charge|auto-trad|
|[n7ZxA7v0OJE04AGq.htm](pathfinder-bestiary-3-items/n7ZxA7v0OJE04AGq.htm)|Frightful Presence|auto-trad|
|[n8MOp28YVOpx7Eoq.htm](pathfinder-bestiary-3-items/n8MOp28YVOpx7Eoq.htm)|Flit Back|auto-trad|
|[N8Z7KBCRIUBXL2Gu.htm](pathfinder-bestiary-3-items/N8Z7KBCRIUBXL2Gu.htm)|Darkvision|auto-trad|
|[n96wAvXJdhT8a2uD.htm](pathfinder-bestiary-3-items/n96wAvXJdhT8a2uD.htm)|Claw|auto-trad|
|[N9fBVLGGH7iHDjIs.htm](pathfinder-bestiary-3-items/N9fBVLGGH7iHDjIs.htm)|Darkvision|auto-trad|
|[N9uynDS2jAyjfbwR.htm](pathfinder-bestiary-3-items/N9uynDS2jAyjfbwR.htm)|Breath Weapon|auto-trad|
|[Na4ZinpZPOodP1WY.htm](pathfinder-bestiary-3-items/Na4ZinpZPOodP1WY.htm)|Blighted Footfalls|auto-trad|
|[NAngdrs8j255ZSod.htm](pathfinder-bestiary-3-items/NAngdrs8j255ZSod.htm)|Steal Memories|auto-trad|
|[NaTbdbqoxFlKljoH.htm](pathfinder-bestiary-3-items/NaTbdbqoxFlKljoH.htm)|Tremorsense (Precise) 40 feet, (Imprecise) 80 feet|auto-trad|
|[NbGq64tR4svPBmpM.htm](pathfinder-bestiary-3-items/NbGq64tR4svPBmpM.htm)|Darkvision|auto-trad|
|[Nc31MydJOBHcqpyb.htm](pathfinder-bestiary-3-items/Nc31MydJOBHcqpyb.htm)|Divine Dispelling|auto-trad|
|[NCFt2ltSfGTesan4.htm](pathfinder-bestiary-3-items/NCFt2ltSfGTesan4.htm)|-2 to All Saves vs. Emotion Effects|auto-trad|
|[ncklibzh6Dfp2LTc.htm](pathfinder-bestiary-3-items/ncklibzh6Dfp2LTc.htm)|Shield Block|auto-trad|
|[nCkzzJiycKKevhSG.htm](pathfinder-bestiary-3-items/nCkzzJiycKKevhSG.htm)|Countered by Water|auto-trad|
|[ncL6q9VyNef5Kfov.htm](pathfinder-bestiary-3-items/ncL6q9VyNef5Kfov.htm)|Interpose|auto-trad|
|[NCoWkgKyY8nUdbrO.htm](pathfinder-bestiary-3-items/NCoWkgKyY8nUdbrO.htm)|Occult Innate Spells|auto-trad|
|[NcpXTQwFwbyulXQM.htm](pathfinder-bestiary-3-items/NcpXTQwFwbyulXQM.htm)|Claw|auto-trad|
|[NDkFem8B3XVJXQkP.htm](pathfinder-bestiary-3-items/NDkFem8B3XVJXQkP.htm)|Cecaelia Jet|auto-trad|
|[ndQeCjr03ZZsI5o1.htm](pathfinder-bestiary-3-items/ndQeCjr03ZZsI5o1.htm)|Memory Maelstrom|auto-trad|
|[nEE49kEL2IsSaPcv.htm](pathfinder-bestiary-3-items/nEE49kEL2IsSaPcv.htm)|Green Grab|auto-trad|
|[nEHCDw3LRtrCnVMz.htm](pathfinder-bestiary-3-items/nEHCDw3LRtrCnVMz.htm)|Divine Innate Spells|auto-trad|
|[neSR5XCiVlUBQSvT.htm](pathfinder-bestiary-3-items/neSR5XCiVlUBQSvT.htm)|At-Will Spells|auto-trad|
|[NewHXtJk8yORbKND.htm](pathfinder-bestiary-3-items/NewHXtJk8yORbKND.htm)|Divine Innate Spells|auto-trad|
|[NFIsLlfq5D02gNol.htm](pathfinder-bestiary-3-items/NFIsLlfq5D02gNol.htm)|Grab|auto-trad|
|[Ng4z9p0PvzMzN8Di.htm](pathfinder-bestiary-3-items/Ng4z9p0PvzMzN8Di.htm)|Kinsense|auto-trad|
|[NgeIHogL6m58BKDp.htm](pathfinder-bestiary-3-items/NgeIHogL6m58BKDp.htm)|Verdant Burst|auto-trad|
|[ngkPL0GL8aCtZQ2t.htm](pathfinder-bestiary-3-items/ngkPL0GL8aCtZQ2t.htm)|Flaming Sword|auto-trad|
|[nH4VBesV9iFOfCnq.htm](pathfinder-bestiary-3-items/nH4VBesV9iFOfCnq.htm)|Ink Cloud|auto-trad|
|[nHC6WGYqix3arWBf.htm](pathfinder-bestiary-3-items/nHC6WGYqix3arWBf.htm)|Feed on Sorrow|auto-trad|
|[nhlXnY0WEq9xwOO7.htm](pathfinder-bestiary-3-items/nhlXnY0WEq9xwOO7.htm)|Darkvision|auto-trad|
|[NHqBLwHkFRahYskc.htm](pathfinder-bestiary-3-items/NHqBLwHkFRahYskc.htm)|Telepathy 30 feet|auto-trad|
|[NHrIQILaWSM6emU0.htm](pathfinder-bestiary-3-items/NHrIQILaWSM6emU0.htm)|Low-Light Vision|auto-trad|
|[nIiLVQf3htOx2fUg.htm](pathfinder-bestiary-3-items/nIiLVQf3htOx2fUg.htm)|Grab|auto-trad|
|[niyS4ePBUKlm1nbJ.htm](pathfinder-bestiary-3-items/niyS4ePBUKlm1nbJ.htm)|Frightful Presence|auto-trad|
|[NizhFg0yQ725ETUd.htm](pathfinder-bestiary-3-items/NizhFg0yQ725ETUd.htm)|Titanic Grasp|auto-trad|
|[njHt74BnuaBXWlru.htm](pathfinder-bestiary-3-items/njHt74BnuaBXWlru.htm)|Claw|auto-trad|
|[nJj3OATPJmrNMM3y.htm](pathfinder-bestiary-3-items/nJj3OATPJmrNMM3y.htm)|Inflating Rush|auto-trad|
|[NJm2VdVQrwDn112D.htm](pathfinder-bestiary-3-items/NJm2VdVQrwDn112D.htm)|Shadow Shift|auto-trad|
|[NKqgBeuuqEtFMoGN.htm](pathfinder-bestiary-3-items/NKqgBeuuqEtFMoGN.htm)|Dual Mind|auto-trad|
|[NlBTftnU3oQdbjtw.htm](pathfinder-bestiary-3-items/NlBTftnU3oQdbjtw.htm)|Low-Light Vision|auto-trad|
|[nllsy0skrldw3leB.htm](pathfinder-bestiary-3-items/nllsy0skrldw3leB.htm)|Low-Light Vision|auto-trad|
|[NLwKm20SkugAa88n.htm](pathfinder-bestiary-3-items/NLwKm20SkugAa88n.htm)|Feign Death|auto-trad|
|[NmGeqt2BxwfGod5Z.htm](pathfinder-bestiary-3-items/NmGeqt2BxwfGod5Z.htm)|Form Up|auto-trad|
|[nmzcowdCvUQCrcqI.htm](pathfinder-bestiary-3-items/nmzcowdCvUQCrcqI.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[Nn5E53ud3JgH2R3T.htm](pathfinder-bestiary-3-items/Nn5E53ud3JgH2R3T.htm)|Champion Focus Spells|auto-trad|
|[nNhyC2WwCF4L4Lhd.htm](pathfinder-bestiary-3-items/nNhyC2WwCF4L4Lhd.htm)|Primal Innate Spells|auto-trad|
|[nNJH2B7s8hfn3Tci.htm](pathfinder-bestiary-3-items/nNJH2B7s8hfn3Tci.htm)|Negative Healing|auto-trad|
|[NO4hZxcJ6X16f4Og.htm](pathfinder-bestiary-3-items/NO4hZxcJ6X16f4Og.htm)|Change Shape|auto-trad|
|[NO59VkQbOQIqZLrI.htm](pathfinder-bestiary-3-items/NO59VkQbOQIqZLrI.htm)|Saturated|auto-trad|
|[np15xTTViDna2Ngj.htm](pathfinder-bestiary-3-items/np15xTTViDna2Ngj.htm)|Greater Darkvision|auto-trad|
|[Np1YXLEjDe9xa1Qh.htm](pathfinder-bestiary-3-items/Np1YXLEjDe9xa1Qh.htm)|Darkvision|auto-trad|
|[NqfZq31gduTX8PMN.htm](pathfinder-bestiary-3-items/NqfZq31gduTX8PMN.htm)|Thorn|auto-trad|
|[nr5RYRA0lNXTCCfR.htm](pathfinder-bestiary-3-items/nr5RYRA0lNXTCCfR.htm)|Ram Charge|auto-trad|
|[nRcZT1xJaPAlURaU.htm](pathfinder-bestiary-3-items/nRcZT1xJaPAlURaU.htm)|Even Now?|auto-trad|
|[NrIJRYd3U38RpP6w.htm](pathfinder-bestiary-3-items/NrIJRYd3U38RpP6w.htm)|Fool's Gold|auto-trad|
|[Nsjcg03W4x2ElnCc.htm](pathfinder-bestiary-3-items/Nsjcg03W4x2ElnCc.htm)|Jaws|auto-trad|
|[Nskz1Z4716xdGyZy.htm](pathfinder-bestiary-3-items/Nskz1Z4716xdGyZy.htm)|Rock|auto-trad|
|[NsmFyn1MUaCrkX52.htm](pathfinder-bestiary-3-items/NsmFyn1MUaCrkX52.htm)|Frozen Weapons|auto-trad|
|[NsMzxbtDP9JAooC6.htm](pathfinder-bestiary-3-items/NsMzxbtDP9JAooC6.htm)|Bond in Light|auto-trad|
|[NsrDJM8cD8NU8iJt.htm](pathfinder-bestiary-3-items/NsrDJM8cD8NU8iJt.htm)|Grioth Venom|auto-trad|
|[nTIiE8RjvvPcUYEK.htm](pathfinder-bestiary-3-items/nTIiE8RjvvPcUYEK.htm)|Rend|auto-trad|
|[NtoApZINNb0XOSCK.htm](pathfinder-bestiary-3-items/NtoApZINNb0XOSCK.htm)|Positive Energy Transfer|auto-trad|
|[nul98z8Tuin8RNul.htm](pathfinder-bestiary-3-items/nul98z8Tuin8RNul.htm)|Low-Light Vision|auto-trad|
|[NunhObJDovYnRhq5.htm](pathfinder-bestiary-3-items/NunhObJDovYnRhq5.htm)|Claw|auto-trad|
|[nVF38zJQgYheOlaQ.htm](pathfinder-bestiary-3-items/nVF38zJQgYheOlaQ.htm)|Snatch Skull|auto-trad|
|[Nvg6G4rwFK7be0Td.htm](pathfinder-bestiary-3-items/Nvg6G4rwFK7be0Td.htm)|Claw|auto-trad|
|[nVOb5B6Sn8JM7NDn.htm](pathfinder-bestiary-3-items/nVOb5B6Sn8JM7NDn.htm)|Constant Spells|auto-trad|
|[NvRpRxRlhF2Xz2bB.htm](pathfinder-bestiary-3-items/NvRpRxRlhF2Xz2bB.htm)|Touch|auto-trad|
|[Nwuz4zQLB3u2ujwO.htm](pathfinder-bestiary-3-items/Nwuz4zQLB3u2ujwO.htm)|Tail|auto-trad|
|[NWXRpTU3jhjVFz93.htm](pathfinder-bestiary-3-items/NWXRpTU3jhjVFz93.htm)|Death Gasp|auto-trad|
|[NX0pJgKM21qtQACy.htm](pathfinder-bestiary-3-items/NX0pJgKM21qtQACy.htm)|Stench|auto-trad|
|[Nxg7iUxwWhxMhySE.htm](pathfinder-bestiary-3-items/Nxg7iUxwWhxMhySE.htm)|Occult Innate Spells|auto-trad|
|[nXjrQb5q4kAXIS61.htm](pathfinder-bestiary-3-items/nXjrQb5q4kAXIS61.htm)|Divine Innate Spells|auto-trad|
|[nXUfkwitBNWhIe8d.htm](pathfinder-bestiary-3-items/nXUfkwitBNWhIe8d.htm)|Fossilization|auto-trad|
|[NxYSa8z9vgTVhuCT.htm](pathfinder-bestiary-3-items/NxYSa8z9vgTVhuCT.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[NY9oPlAKUHSpnJ52.htm](pathfinder-bestiary-3-items/NY9oPlAKUHSpnJ52.htm)|Darkvision|auto-trad|
|[NYACujLILqXuexgZ.htm](pathfinder-bestiary-3-items/NYACujLILqXuexgZ.htm)|Light Blindness|auto-trad|
|[NZUOo1WmtomF3Pyf.htm](pathfinder-bestiary-3-items/NZUOo1WmtomF3Pyf.htm)|Swarming Gnaw|auto-trad|
|[o11nVdIh4C9R8NHS.htm](pathfinder-bestiary-3-items/o11nVdIh4C9R8NHS.htm)|Primal Innate Spells|auto-trad|
|[O1nBRrEFZ3T6AQYn.htm](pathfinder-bestiary-3-items/O1nBRrEFZ3T6AQYn.htm)|Attack of Opportunity (Tail Only)|auto-trad|
|[o1xm0AkoMLLKUiqH.htm](pathfinder-bestiary-3-items/o1xm0AkoMLLKUiqH.htm)|Claw|auto-trad|
|[O36hrH8CJRLhvwrI.htm](pathfinder-bestiary-3-items/O36hrH8CJRLhvwrI.htm)|Temporal Sense|auto-trad|
|[o3fl8ed8HrHnV97F.htm](pathfinder-bestiary-3-items/o3fl8ed8HrHnV97F.htm)|Longbow|auto-trad|
|[O5TqNDUtiJmWEbpZ.htm](pathfinder-bestiary-3-items/O5TqNDUtiJmWEbpZ.htm)|Desert Stride|auto-trad|
|[O5Ub4Pr4nhdrG6Xy.htm](pathfinder-bestiary-3-items/O5Ub4Pr4nhdrG6Xy.htm)|Telepathy 100 feet|auto-trad|
|[o5UbC8XEchUb9DrO.htm](pathfinder-bestiary-3-items/o5UbC8XEchUb9DrO.htm)|Darkvision|auto-trad|
|[O5YGoCAO4nuXA6Pb.htm](pathfinder-bestiary-3-items/O5YGoCAO4nuXA6Pb.htm)|Ganzi Resistance|auto-trad|
|[o681r0ZsSIWthmiQ.htm](pathfinder-bestiary-3-items/o681r0ZsSIWthmiQ.htm)|Nimble Stride|auto-trad|
|[o6pEQo5KbjxyW7Bj.htm](pathfinder-bestiary-3-items/o6pEQo5KbjxyW7Bj.htm)|Trample|auto-trad|
|[o754wI61VUPIBAZy.htm](pathfinder-bestiary-3-items/o754wI61VUPIBAZy.htm)|Grab|auto-trad|
|[O7GCOmC1ewn8Hglr.htm](pathfinder-bestiary-3-items/O7GCOmC1ewn8Hglr.htm)|Claw|auto-trad|
|[o7YwycWP8YFceUCM.htm](pathfinder-bestiary-3-items/o7YwycWP8YFceUCM.htm)|Darkvision|auto-trad|
|[o9bdzGDtUTzmMtAv.htm](pathfinder-bestiary-3-items/o9bdzGDtUTzmMtAv.htm)|Tendril|auto-trad|
|[o9gWtjSqFLRnOVk2.htm](pathfinder-bestiary-3-items/o9gWtjSqFLRnOVk2.htm)|Claw|auto-trad|
|[O9KVtNjS6f57XAHS.htm](pathfinder-bestiary-3-items/O9KVtNjS6f57XAHS.htm)|Construct Armor (Hardness 14)|auto-trad|
|[o9sNXlscXUyhKlOK.htm](pathfinder-bestiary-3-items/o9sNXlscXUyhKlOK.htm)|Blatant Liar|auto-trad|
|[oaeEm5dS1vLarcaG.htm](pathfinder-bestiary-3-items/oaeEm5dS1vLarcaG.htm)|Whip Drain|auto-trad|
|[oAwLdSvl1lwzYmYb.htm](pathfinder-bestiary-3-items/oAwLdSvl1lwzYmYb.htm)|Greater Darkvision|auto-trad|
|[OazlOifRnQFSPc2g.htm](pathfinder-bestiary-3-items/OazlOifRnQFSPc2g.htm)|Fire Healing|auto-trad|
|[oB4SNmUD0LdtVMO3.htm](pathfinder-bestiary-3-items/oB4SNmUD0LdtVMO3.htm)|Occult Innate Spells|auto-trad|
|[oB6zbIt1d6nbCIXg.htm](pathfinder-bestiary-3-items/oB6zbIt1d6nbCIXg.htm)|Breath Weapon|auto-trad|
|[obpFkh9f0LWUmFG2.htm](pathfinder-bestiary-3-items/obpFkh9f0LWUmFG2.htm)|Frightful Presence|auto-trad|
|[oBzXzmE3ZsGSx8r0.htm](pathfinder-bestiary-3-items/oBzXzmE3ZsGSx8r0.htm)|Darkvision|auto-trad|
|[OC5iW5RWtphm9Mby.htm](pathfinder-bestiary-3-items/OC5iW5RWtphm9Mby.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[ocvnX6iZ7cCsqHIf.htm](pathfinder-bestiary-3-items/ocvnX6iZ7cCsqHIf.htm)|Low-Light Vision|auto-trad|
|[OCyAo1qV9dwYv7Ul.htm](pathfinder-bestiary-3-items/OCyAo1qV9dwYv7Ul.htm)|No Breath|auto-trad|
|[oD3DnNMV4dQa3Gss.htm](pathfinder-bestiary-3-items/oD3DnNMV4dQa3Gss.htm)|Constant Spells|auto-trad|
|[ODB8H9ordvDj9MK8.htm](pathfinder-bestiary-3-items/ODB8H9ordvDj9MK8.htm)|Telepathy 100 feet|auto-trad|
|[oeXGUL3qzPHQgzrW.htm](pathfinder-bestiary-3-items/oeXGUL3qzPHQgzrW.htm)|Slime Trap|auto-trad|
|[OEzKykNunY5znzf7.htm](pathfinder-bestiary-3-items/OEzKykNunY5znzf7.htm)|Claw|auto-trad|
|[ofXdILv2twbnZo5Z.htm](pathfinder-bestiary-3-items/ofXdILv2twbnZo5Z.htm)|Claw|auto-trad|
|[Og5zTgT13s8QIGgb.htm](pathfinder-bestiary-3-items/Og5zTgT13s8QIGgb.htm)|Ram Charge|auto-trad|
|[OgFZ82Cgk6qKkD9l.htm](pathfinder-bestiary-3-items/OgFZ82Cgk6qKkD9l.htm)|Aim as One|auto-trad|
|[OGIF0iSMoUfXZTlW.htm](pathfinder-bestiary-3-items/OGIF0iSMoUfXZTlW.htm)|Breath Weapon|auto-trad|
|[oGImpwgKAU80AN98.htm](pathfinder-bestiary-3-items/oGImpwgKAU80AN98.htm)|Faceless|auto-trad|
|[OH7aLLnIFKP4SaLG.htm](pathfinder-bestiary-3-items/OH7aLLnIFKP4SaLG.htm)|Sneak Attack|auto-trad|
|[ohHcoAMXMCHN6Scv.htm](pathfinder-bestiary-3-items/ohHcoAMXMCHN6Scv.htm)|Beak|auto-trad|
|[ohqrq2d7d3If0s76.htm](pathfinder-bestiary-3-items/ohqrq2d7d3If0s76.htm)|Club|auto-trad|
|[oHw8udhGCS0SJgQT.htm](pathfinder-bestiary-3-items/oHw8udhGCS0SJgQT.htm)|Hold Still|auto-trad|
|[OhwcTonDsZ7QOZTR.htm](pathfinder-bestiary-3-items/OhwcTonDsZ7QOZTR.htm)|Smoke Vision|auto-trad|
|[OIExxzLzZHeopXKg.htm](pathfinder-bestiary-3-items/OIExxzLzZHeopXKg.htm)|Darkvision|auto-trad|
|[OiznKRNhVSCejM5r.htm](pathfinder-bestiary-3-items/OiznKRNhVSCejM5r.htm)|Divine Innate Spells|auto-trad|
|[Oj448zcLgfmVIFy1.htm](pathfinder-bestiary-3-items/Oj448zcLgfmVIFy1.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[oj5hseYYTFuq4OYh.htm](pathfinder-bestiary-3-items/oj5hseYYTFuq4OYh.htm)|Darkvision|auto-trad|
|[ojGq55t3EY5AHaLi.htm](pathfinder-bestiary-3-items/ojGq55t3EY5AHaLi.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[OjOzwh1AVQ8baTZt.htm](pathfinder-bestiary-3-items/OjOzwh1AVQ8baTZt.htm)|Gleaming Armor|auto-trad|
|[ojRftryhvZxryakY.htm](pathfinder-bestiary-3-items/ojRftryhvZxryakY.htm)|Godslayer|auto-trad|
|[oJXCaHxdH67r6d1I.htm](pathfinder-bestiary-3-items/oJXCaHxdH67r6d1I.htm)|Inhabit Vessel|auto-trad|
|[OK5mcs3GjpZRifx9.htm](pathfinder-bestiary-3-items/OK5mcs3GjpZRifx9.htm)|Jaws|auto-trad|
|[okbEN7zWQ3xJAzkg.htm](pathfinder-bestiary-3-items/okbEN7zWQ3xJAzkg.htm)|Jaws|auto-trad|
|[Ol5nO6bgZS5cJIkA.htm](pathfinder-bestiary-3-items/Ol5nO6bgZS5cJIkA.htm)|Constant Spells|auto-trad|
|[OLXpGW8u6XmpJXAp.htm](pathfinder-bestiary-3-items/OLXpGW8u6XmpJXAp.htm)|Swarming Bites|auto-trad|
|[oLyWvtqHGoL3rQb1.htm](pathfinder-bestiary-3-items/oLyWvtqHGoL3rQb1.htm)|Constant Spells|auto-trad|
|[oM4VdHuJNfDqEoHR.htm](pathfinder-bestiary-3-items/oM4VdHuJNfDqEoHR.htm)|Anchor|auto-trad|
|[oMZGYxkqoirlCaQm.htm](pathfinder-bestiary-3-items/oMZGYxkqoirlCaQm.htm)|Shortsword|auto-trad|
|[ON9shjsp6vVBXsO0.htm](pathfinder-bestiary-3-items/ON9shjsp6vVBXsO0.htm)|Darkvision|auto-trad|
|[oNP92kbO07u3ivGK.htm](pathfinder-bestiary-3-items/oNP92kbO07u3ivGK.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[Oo1ff4ERhvPS7Bhw.htm](pathfinder-bestiary-3-items/Oo1ff4ERhvPS7Bhw.htm)|Frightful Presence|auto-trad|
|[ooPhbtvdtMFxysJe.htm](pathfinder-bestiary-3-items/ooPhbtvdtMFxysJe.htm)|Evasion|auto-trad|
|[OoyUDyw74uN0JNYI.htm](pathfinder-bestiary-3-items/OoyUDyw74uN0JNYI.htm)|Regeneration 40 (Deactivated by Positive, Mental, or Orichalcum)|auto-trad|
|[oPdEp3kAGptn9Bkh.htm](pathfinder-bestiary-3-items/oPdEp3kAGptn9Bkh.htm)|Occult Innate Spells|auto-trad|
|[oPF7KkI27o6FCE0l.htm](pathfinder-bestiary-3-items/oPF7KkI27o6FCE0l.htm)|Improved Grab|auto-trad|
|[Ophbfa4MbDPldjGG.htm](pathfinder-bestiary-3-items/Ophbfa4MbDPldjGG.htm)|Darkvision|auto-trad|
|[OPww9QAdYNjhEqoV.htm](pathfinder-bestiary-3-items/OPww9QAdYNjhEqoV.htm)|Primal Innate Spells|auto-trad|
|[oQ1wQPUo6g2iysB1.htm](pathfinder-bestiary-3-items/oQ1wQPUo6g2iysB1.htm)|Jaws|auto-trad|
|[Or20Bb0UzTLxRqoj.htm](pathfinder-bestiary-3-items/Or20Bb0UzTLxRqoj.htm)|Shortbow|auto-trad|
|[ORnkr2b5BKCvtKXe.htm](pathfinder-bestiary-3-items/ORnkr2b5BKCvtKXe.htm)|Grab|auto-trad|
|[oRzzljhsCixNqf5j.htm](pathfinder-bestiary-3-items/oRzzljhsCixNqf5j.htm)|Elegant Cane|auto-trad|
|[osh2zvGEnGifdNKA.htm](pathfinder-bestiary-3-items/osh2zvGEnGifdNKA.htm)|Darkvision|auto-trad|
|[OshjLmAWIWTiJZoR.htm](pathfinder-bestiary-3-items/OshjLmAWIWTiJZoR.htm)|Adamantine Claws|auto-trad|
|[OSib6ckkzfe3tvbE.htm](pathfinder-bestiary-3-items/OSib6ckkzfe3tvbE.htm)|Enormous|auto-trad|
|[OsJRsnBwCYSFdA4G.htm](pathfinder-bestiary-3-items/OsJRsnBwCYSFdA4G.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[OTkxsBzxKFuPWRCX.htm](pathfinder-bestiary-3-items/OTkxsBzxKFuPWRCX.htm)|Composite Longbow|auto-trad|
|[oTrOmw914xPkqVd5.htm](pathfinder-bestiary-3-items/oTrOmw914xPkqVd5.htm)|Fist|auto-trad|
|[OU34B5fiyPMmjnXO.htm](pathfinder-bestiary-3-items/OU34B5fiyPMmjnXO.htm)|Easy to Call|auto-trad|
|[oU5HOhztZ5crg339.htm](pathfinder-bestiary-3-items/oU5HOhztZ5crg339.htm)|Kukri|auto-trad|
|[ou7GQNYw2dnoJPZW.htm](pathfinder-bestiary-3-items/ou7GQNYw2dnoJPZW.htm)|Greatsword|auto-trad|
|[oUqggV8Z8uzpYdNA.htm](pathfinder-bestiary-3-items/oUqggV8Z8uzpYdNA.htm)|Instrument of Retribution|auto-trad|
|[oveFRPbhF3sXi9RJ.htm](pathfinder-bestiary-3-items/oveFRPbhF3sXi9RJ.htm)|Ephemeral Claw|auto-trad|
|[ovf1EYDzh0K1JNl2.htm](pathfinder-bestiary-3-items/ovf1EYDzh0K1JNl2.htm)|Ward|auto-trad|
|[oVjrC7X48FBIjCug.htm](pathfinder-bestiary-3-items/oVjrC7X48FBIjCug.htm)|Breath Weapon|auto-trad|
|[oVMhwSHesHUR8PLW.htm](pathfinder-bestiary-3-items/oVMhwSHesHUR8PLW.htm)|Unluck Aura|auto-trad|
|[OvoJR0fdN07ZLFYR.htm](pathfinder-bestiary-3-items/OvoJR0fdN07ZLFYR.htm)|Attack of Opportunity (Stinger Only)|auto-trad|
|[Ovtg8L0Jkr5t6b4n.htm](pathfinder-bestiary-3-items/Ovtg8L0Jkr5t6b4n.htm)|Blazing Admonition|auto-trad|
|[OVXjqkURq28dcMFO.htm](pathfinder-bestiary-3-items/OVXjqkURq28dcMFO.htm)|Negative Healing|auto-trad|
|[OW9oEttpg9WFvgOt.htm](pathfinder-bestiary-3-items/OW9oEttpg9WFvgOt.htm)|Constant Spells|auto-trad|
|[OWa41XZ0GByISijn.htm](pathfinder-bestiary-3-items/OWa41XZ0GByISijn.htm)|Spear|auto-trad|
|[oWdMjEIfQss7DdS1.htm](pathfinder-bestiary-3-items/oWdMjEIfQss7DdS1.htm)|Jaws|auto-trad|
|[oWFVODhnr16hmGjN.htm](pathfinder-bestiary-3-items/oWFVODhnr16hmGjN.htm)|Constant Spells|auto-trad|
|[OwP8GUdLEkFwmgjL.htm](pathfinder-bestiary-3-items/OwP8GUdLEkFwmgjL.htm)|Champion Focus Spells|auto-trad|
|[OwpDVJif5SRqFrSS.htm](pathfinder-bestiary-3-items/OwpDVJif5SRqFrSS.htm)|Tail|auto-trad|
|[OwZj80arbVgN8YC9.htm](pathfinder-bestiary-3-items/OwZj80arbVgN8YC9.htm)|-1 to All Saves vs. Emotion Effects|auto-trad|
|[oX5mS0nonoT9j23k.htm](pathfinder-bestiary-3-items/oX5mS0nonoT9j23k.htm)|Paint|auto-trad|
|[oX8m7VQu9DO6VFnp.htm](pathfinder-bestiary-3-items/oX8m7VQu9DO6VFnp.htm)|Boneshard Burst|auto-trad|
|[oxJLEKIfZkDETCv8.htm](pathfinder-bestiary-3-items/oxJLEKIfZkDETCv8.htm)|Darkvision|auto-trad|
|[oXM5aJ0mHfq2fGWh.htm](pathfinder-bestiary-3-items/oXM5aJ0mHfq2fGWh.htm)|Javelin|auto-trad|
|[OYZDy9EFgKjBC4ZW.htm](pathfinder-bestiary-3-items/OYZDy9EFgKjBC4ZW.htm)|Coiling Frenzy|auto-trad|
|[ozof8R0i6Gx0GTeA.htm](pathfinder-bestiary-3-items/ozof8R0i6Gx0GTeA.htm)|Fist|auto-trad|
|[oZq1LEjJ1S0dXNc2.htm](pathfinder-bestiary-3-items/oZq1LEjJ1S0dXNc2.htm)|Change Shape|auto-trad|
|[ozywltVfeeRvEgor.htm](pathfinder-bestiary-3-items/ozywltVfeeRvEgor.htm)|Eel Jaws|auto-trad|
|[p0P37OZT7ovYmUkB.htm](pathfinder-bestiary-3-items/p0P37OZT7ovYmUkB.htm)|Canine Vulnerability|auto-trad|
|[p3DOduEPOUUeLaxK.htm](pathfinder-bestiary-3-items/p3DOduEPOUUeLaxK.htm)|Grab|auto-trad|
|[P3e58ycpovGthuDe.htm](pathfinder-bestiary-3-items/P3e58ycpovGthuDe.htm)|Scent (Imprecise) 40 feet|auto-trad|
|[p3v8D49u0adS76qw.htm](pathfinder-bestiary-3-items/p3v8D49u0adS76qw.htm)|Divine Innate Spells|auto-trad|
|[p4F4jzZzQp6LGCex.htm](pathfinder-bestiary-3-items/p4F4jzZzQp6LGCex.htm)|Smother|auto-trad|
|[p4RYgQW9q1rXPgz5.htm](pathfinder-bestiary-3-items/p4RYgQW9q1rXPgz5.htm)|Darkvision|auto-trad|
|[p58AYJyyu8cSum0T.htm](pathfinder-bestiary-3-items/p58AYJyyu8cSum0T.htm)|Grab|auto-trad|
|[p5i4xSC4BGMzlqUF.htm](pathfinder-bestiary-3-items/p5i4xSC4BGMzlqUF.htm)|Telepathy 60 feet|auto-trad|
|[p5iUKuZYPVDb0i3z.htm](pathfinder-bestiary-3-items/p5iUKuZYPVDb0i3z.htm)|Sumbreiva Huntblade|auto-trad|
|[P6lMDdeaBGEoO8lA.htm](pathfinder-bestiary-3-items/P6lMDdeaBGEoO8lA.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[p6oPHL2T4fZfMivY.htm](pathfinder-bestiary-3-items/p6oPHL2T4fZfMivY.htm)|Jaws|auto-trad|
|[p6uToNU7wgFDgDDH.htm](pathfinder-bestiary-3-items/p6uToNU7wgFDgDDH.htm)|Sloth|auto-trad|
|[P6yyZcwSDswPX7xS.htm](pathfinder-bestiary-3-items/P6yyZcwSDswPX7xS.htm)|Immortal|auto-trad|
|[P7PEpIp5LPhybC2C.htm](pathfinder-bestiary-3-items/P7PEpIp5LPhybC2C.htm)|Engulf|auto-trad|
|[P7rE9vi1P70NJ5iD.htm](pathfinder-bestiary-3-items/P7rE9vi1P70NJ5iD.htm)|Plague of Ancients|auto-trad|
|[p7SnyvGd17jjb0aF.htm](pathfinder-bestiary-3-items/p7SnyvGd17jjb0aF.htm)|Tidal Wave|auto-trad|
|[p8hPnF6s5wU3KJk3.htm](pathfinder-bestiary-3-items/p8hPnF6s5wU3KJk3.htm)|Claw|auto-trad|
|[P8K5a9sUjb1J99U0.htm](pathfinder-bestiary-3-items/P8K5a9sUjb1J99U0.htm)|Talon|auto-trad|
|[p931SlqMJE64SPG4.htm](pathfinder-bestiary-3-items/p931SlqMJE64SPG4.htm)|Spirit Tendril|auto-trad|
|[PA233M8LHOomjH0v.htm](pathfinder-bestiary-3-items/PA233M8LHOomjH0v.htm)|At-Will Spells|auto-trad|
|[pA64qfMCuUfGqBOF.htm](pathfinder-bestiary-3-items/pA64qfMCuUfGqBOF.htm)|Amphisbaena Venom|auto-trad|
|[paacuYr4AiURwRWW.htm](pathfinder-bestiary-3-items/paacuYr4AiURwRWW.htm)|Tail|auto-trad|
|[PB6GFQeZfrHdNRmj.htm](pathfinder-bestiary-3-items/PB6GFQeZfrHdNRmj.htm)|Telepathy 100 feet|auto-trad|
|[pBgScXHzKgG3o8iv.htm](pathfinder-bestiary-3-items/pBgScXHzKgG3o8iv.htm)|Oceanic Armor|auto-trad|
|[pbSfPZuBX9faeDui.htm](pathfinder-bestiary-3-items/pbSfPZuBX9faeDui.htm)|Horn|auto-trad|
|[pbwIXTUIrcHbVCd3.htm](pathfinder-bestiary-3-items/pbwIXTUIrcHbVCd3.htm)|Darkvision|auto-trad|
|[pCERhTThClxu07sY.htm](pathfinder-bestiary-3-items/pCERhTThClxu07sY.htm)|Occult Innate Spells|auto-trad|
|[pcMLR5ngGmapeJYS.htm](pathfinder-bestiary-3-items/pcMLR5ngGmapeJYS.htm)|Tail|auto-trad|
|[pDKSuwaiL7A6j7D7.htm](pathfinder-bestiary-3-items/pDKSuwaiL7A6j7D7.htm)|Jaws|auto-trad|
|[pDpwUF5lNCdUWFQT.htm](pathfinder-bestiary-3-items/pDpwUF5lNCdUWFQT.htm)|Claw|auto-trad|
|[PDYDZKTsK9cr0dq7.htm](pathfinder-bestiary-3-items/PDYDZKTsK9cr0dq7.htm)|Deep Breath|auto-trad|
|[pedL6wHN0TYSu6Fd.htm](pathfinder-bestiary-3-items/pedL6wHN0TYSu6Fd.htm)|Spear|auto-trad|
|[pePRN91wJ0Dpe7pM.htm](pathfinder-bestiary-3-items/pePRN91wJ0Dpe7pM.htm)|Urban Chasers|auto-trad|
|[pFDqDZzKZDMzZZk1.htm](pathfinder-bestiary-3-items/pFDqDZzKZDMzZZk1.htm)|Claw|auto-trad|
|[PfPbwkrM06fK0tGR.htm](pathfinder-bestiary-3-items/PfPbwkrM06fK0tGR.htm)|Hand|auto-trad|
|[PFvXq6fIzpNGeRPv.htm](pathfinder-bestiary-3-items/PFvXq6fIzpNGeRPv.htm)|Darkvision|auto-trad|
|[PG2qs05bMZmS8OIt.htm](pathfinder-bestiary-3-items/PG2qs05bMZmS8OIt.htm)|Composite Shortbow|auto-trad|
|[pgjyNm2rRbnBKZmb.htm](pathfinder-bestiary-3-items/pgjyNm2rRbnBKZmb.htm)|Sudden Manifestation|auto-trad|
|[pGmtgv4CyBqJ9cJH.htm](pathfinder-bestiary-3-items/pGmtgv4CyBqJ9cJH.htm)|Stunning Screech|auto-trad|
|[pgNaQm9L35oslwOp.htm](pathfinder-bestiary-3-items/pgNaQm9L35oslwOp.htm)|All-Around Vision|auto-trad|
|[PgnWSQrfu9kmcYy1.htm](pathfinder-bestiary-3-items/PgnWSQrfu9kmcYy1.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[pGQDgZ3Sme2JsoNm.htm](pathfinder-bestiary-3-items/pGQDgZ3Sme2JsoNm.htm)|Clutches|auto-trad|
|[pgVVTGf6DwqRarDp.htm](pathfinder-bestiary-3-items/pgVVTGf6DwqRarDp.htm)|Wolfrime|auto-trad|
|[phH8RreUZhjjwiuo.htm](pathfinder-bestiary-3-items/phH8RreUZhjjwiuo.htm)|Body|auto-trad|
|[Phi18mvpdKJpQmql.htm](pathfinder-bestiary-3-items/Phi18mvpdKJpQmql.htm)|Phantom Horn|auto-trad|
|[pHOf2i3tNwpIeAHs.htm](pathfinder-bestiary-3-items/pHOf2i3tNwpIeAHs.htm)|Frightful Presence|auto-trad|
|[phtWMfKuIsMCi5cE.htm](pathfinder-bestiary-3-items/phtWMfKuIsMCi5cE.htm)|Tongue Reposition|auto-trad|
|[PhxCt9NDQTWqfCU5.htm](pathfinder-bestiary-3-items/PhxCt9NDQTWqfCU5.htm)|Eviscerate|auto-trad|
|[PHZ0zixmQt2ljkJm.htm](pathfinder-bestiary-3-items/PHZ0zixmQt2ljkJm.htm)|Entrails|auto-trad|
|[pIoRWbV6ahXe5lvR.htm](pathfinder-bestiary-3-items/pIoRWbV6ahXe5lvR.htm)|Change Shape|auto-trad|
|[pj0FI4XEhBPiuOmy.htm](pathfinder-bestiary-3-items/pj0FI4XEhBPiuOmy.htm)|Change Shape|auto-trad|
|[pJKtjGeoTW0LzTPG.htm](pathfinder-bestiary-3-items/pJKtjGeoTW0LzTPG.htm)|Draconic Momentum|auto-trad|
|[pjtcZhkm20HjFEmn.htm](pathfinder-bestiary-3-items/pjtcZhkm20HjFEmn.htm)|Darkvision|auto-trad|
|[pJUrlkv2i5xEz2WO.htm](pathfinder-bestiary-3-items/pJUrlkv2i5xEz2WO.htm)|Star Child|auto-trad|
|[pKaeBB4bJYXTzs69.htm](pathfinder-bestiary-3-items/pKaeBB4bJYXTzs69.htm)|Coiling Frenzy|auto-trad|
|[PKh7s5uQyxm0yPQg.htm](pathfinder-bestiary-3-items/PKh7s5uQyxm0yPQg.htm)|Pliers|auto-trad|
|[pKpXpiLPYdr4VX46.htm](pathfinder-bestiary-3-items/pKpXpiLPYdr4VX46.htm)|Skip Between|auto-trad|
|[PLTi8diRg1rhDOR9.htm](pathfinder-bestiary-3-items/PLTi8diRg1rhDOR9.htm)|Trackless Step|auto-trad|
|[PlTKFXVlBOQ1pLtX.htm](pathfinder-bestiary-3-items/PlTKFXVlBOQ1pLtX.htm)|Telepathy 100 feet|auto-trad|
|[pmhHWb3PBr7rQpE5.htm](pathfinder-bestiary-3-items/pmhHWb3PBr7rQpE5.htm)|Spitfire|auto-trad|
|[PmLhjqOQdj9Bkm2j.htm](pathfinder-bestiary-3-items/PmLhjqOQdj9Bkm2j.htm)|Drink Oil|auto-trad|
|[pnaFRppT92BaCHjj.htm](pathfinder-bestiary-3-items/pnaFRppT92BaCHjj.htm)|Claw|auto-trad|
|[pnC70Lg68qGCzyl2.htm](pathfinder-bestiary-3-items/pnC70Lg68qGCzyl2.htm)|Darkvision|auto-trad|
|[pnip6OCCdZn9i0pD.htm](pathfinder-bestiary-3-items/pnip6OCCdZn9i0pD.htm)|Countered by Fire|auto-trad|
|[PO5OyKjxeT0rYaYl.htm](pathfinder-bestiary-3-items/PO5OyKjxeT0rYaYl.htm)|Jaws|auto-trad|
|[POpEyXnpPzh143yS.htm](pathfinder-bestiary-3-items/POpEyXnpPzh143yS.htm)|Improved Grab|auto-trad|
|[POZqpY2nqdOUZBf7.htm](pathfinder-bestiary-3-items/POZqpY2nqdOUZBf7.htm)|At-Will Spells|auto-trad|
|[pp693fsJOt23L3X9.htm](pathfinder-bestiary-3-items/pp693fsJOt23L3X9.htm)|Shock Mind|auto-trad|
|[pPPA3LPdsuQ55Gp0.htm](pathfinder-bestiary-3-items/pPPA3LPdsuQ55Gp0.htm)|Counterattack|auto-trad|
|[PqKDvCgTiNBUGAhF.htm](pathfinder-bestiary-3-items/PqKDvCgTiNBUGAhF.htm)|Coven Spells|auto-trad|
|[PrETYXvcaucelz4m.htm](pathfinder-bestiary-3-items/PrETYXvcaucelz4m.htm)|Powerful Leaper|auto-trad|
|[pRfIkGoEX8AyxeSw.htm](pathfinder-bestiary-3-items/pRfIkGoEX8AyxeSw.htm)|Darkvision|auto-trad|
|[prTBGYHOcT9eQskj.htm](pathfinder-bestiary-3-items/prTBGYHOcT9eQskj.htm)|Eye Probe|auto-trad|
|[pRvVMMMJPM8IsEkp.htm](pathfinder-bestiary-3-items/pRvVMMMJPM8IsEkp.htm)|Darkvision|auto-trad|
|[PrWWui76kCsgmSBk.htm](pathfinder-bestiary-3-items/PrWWui76kCsgmSBk.htm)|Flying Wheel|auto-trad|
|[pRXjlEM0YDK7HrCS.htm](pathfinder-bestiary-3-items/pRXjlEM0YDK7HrCS.htm)|Squirming Embrace|auto-trad|
|[PS82ZXV5QXRxiBXf.htm](pathfinder-bestiary-3-items/PS82ZXV5QXRxiBXf.htm)|Primal Innate Spells|auto-trad|
|[ptpsl6OafcvwkgOx.htm](pathfinder-bestiary-3-items/ptpsl6OafcvwkgOx.htm)|Accord Essence|auto-trad|
|[PtvDEVABQH9iJs6d.htm](pathfinder-bestiary-3-items/PtvDEVABQH9iJs6d.htm)|Siphon Magic|auto-trad|
|[PuvbyIbJk14Uho2a.htm](pathfinder-bestiary-3-items/PuvbyIbJk14Uho2a.htm)|Tremorsense 60 feet|auto-trad|
|[Pv0epZ0IMHJFIRG2.htm](pathfinder-bestiary-3-items/Pv0epZ0IMHJFIRG2.htm)|Darkvision|auto-trad|
|[pV1OZsdl0kyBME6Q.htm](pathfinder-bestiary-3-items/pV1OZsdl0kyBME6Q.htm)|Primal Innate Spells|auto-trad|
|[pVxNWlSICm5ij03c.htm](pathfinder-bestiary-3-items/pVxNWlSICm5ij03c.htm)|Grab|auto-trad|
|[pW9sevYZuAwpWAr3.htm](pathfinder-bestiary-3-items/pW9sevYZuAwpWAr3.htm)|Darkvision|auto-trad|
|[PWE9sinIQ5L6H4v2.htm](pathfinder-bestiary-3-items/PWE9sinIQ5L6H4v2.htm)|Consecration Vulnerability|auto-trad|
|[pwmnQMrCnI7oQIoE.htm](pathfinder-bestiary-3-items/pwmnQMrCnI7oQIoE.htm)|Jaws|auto-trad|
|[pWvysguPWMKlVIzr.htm](pathfinder-bestiary-3-items/pWvysguPWMKlVIzr.htm)|Raptor Dive|auto-trad|
|[Px9k1rac0p4O9b4J.htm](pathfinder-bestiary-3-items/Px9k1rac0p4O9b4J.htm)|Fist|auto-trad|
|[PxEpWsl4UxCVOJxy.htm](pathfinder-bestiary-3-items/PxEpWsl4UxCVOJxy.htm)|Occult Innate Spells|auto-trad|
|[PxHTXBiqvpnvByT4.htm](pathfinder-bestiary-3-items/PxHTXBiqvpnvByT4.htm)|Calculated Blow|auto-trad|
|[pXlz78EGb6lw6Aij.htm](pathfinder-bestiary-3-items/pXlz78EGb6lw6Aij.htm)|Fist|auto-trad|
|[pxvM1CdSpuWlWrQ3.htm](pathfinder-bestiary-3-items/pxvM1CdSpuWlWrQ3.htm)|-1 Status to All Saves vs. Death Effects|auto-trad|
|[pYnHQN69rHC0bPm0.htm](pathfinder-bestiary-3-items/pYnHQN69rHC0bPm0.htm)|Longspear|auto-trad|
|[PyxiGz5dlEpJ0SE7.htm](pathfinder-bestiary-3-items/PyxiGz5dlEpJ0SE7.htm)|Deluge|auto-trad|
|[pZYPf41244FTorQE.htm](pathfinder-bestiary-3-items/pZYPf41244FTorQE.htm)|Divine Innate Spells|auto-trad|
|[Q0IYQhi4qttZkUKn.htm](pathfinder-bestiary-3-items/Q0IYQhi4qttZkUKn.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[Q2J6B9C1q7HPAVHY.htm](pathfinder-bestiary-3-items/Q2J6B9C1q7HPAVHY.htm)|Telepathy 100 feet|auto-trad|
|[Q345ui1zxYcyOoLX.htm](pathfinder-bestiary-3-items/Q345ui1zxYcyOoLX.htm)|Constant Spells|auto-trad|
|[q3uWzNeCqQuLwiIA.htm](pathfinder-bestiary-3-items/q3uWzNeCqQuLwiIA.htm)|Aura of Courage|auto-trad|
|[Q49aLd8cKOkjpk9J.htm](pathfinder-bestiary-3-items/Q49aLd8cKOkjpk9J.htm)|Darkvision|auto-trad|
|[q5JXKKVYw0NywPhT.htm](pathfinder-bestiary-3-items/q5JXKKVYw0NywPhT.htm)|Pummeling Charge|auto-trad|
|[Q6AA6pHAaFSGJA1w.htm](pathfinder-bestiary-3-items/Q6AA6pHAaFSGJA1w.htm)|Little Oasis|auto-trad|
|[q6bluZPiKPFzD79n.htm](pathfinder-bestiary-3-items/q6bluZPiKPFzD79n.htm)|Frightful Presence|auto-trad|
|[Q6ieY38ZPpXhQz2w.htm](pathfinder-bestiary-3-items/Q6ieY38ZPpXhQz2w.htm)|Telepathy 500 feet|auto-trad|
|[Q77O3dFWbNc8OyJh.htm](pathfinder-bestiary-3-items/Q77O3dFWbNc8OyJh.htm)|Grab|auto-trad|
|[Q7Qfg4mcXnQcUmCd.htm](pathfinder-bestiary-3-items/Q7Qfg4mcXnQcUmCd.htm)|Fist|auto-trad|
|[Q9b50evzwuGvfkAB.htm](pathfinder-bestiary-3-items/Q9b50evzwuGvfkAB.htm)|Self-Destruct|auto-trad|
|[q9cxVnhnmsCNsS4G.htm](pathfinder-bestiary-3-items/q9cxVnhnmsCNsS4G.htm)|Earthen Fist|auto-trad|
|[Q9YkvCkVgM00oIy8.htm](pathfinder-bestiary-3-items/Q9YkvCkVgM00oIy8.htm)|Thorn|auto-trad|
|[qAXVCcqDcG1w6bek.htm](pathfinder-bestiary-3-items/qAXVCcqDcG1w6bek.htm)|Claw|auto-trad|
|[QB0FNEPET64VPGnO.htm](pathfinder-bestiary-3-items/QB0FNEPET64VPGnO.htm)|Greatclub|auto-trad|
|[QBHI3WhGKrjBNZMq.htm](pathfinder-bestiary-3-items/QBHI3WhGKrjBNZMq.htm)|Hydration|auto-trad|
|[QbUWrst99xyuP7X9.htm](pathfinder-bestiary-3-items/QbUWrst99xyuP7X9.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[QCpgvjWcAi1GGE3l.htm](pathfinder-bestiary-3-items/QCpgvjWcAi1GGE3l.htm)|Foot|auto-trad|
|[QCRkv3Pld8YuyUlE.htm](pathfinder-bestiary-3-items/QCRkv3Pld8YuyUlE.htm)|Light Blindness|auto-trad|
|[QE9gzkXJFMTGYg19.htm](pathfinder-bestiary-3-items/QE9gzkXJFMTGYg19.htm)|Out You Go|auto-trad|
|[QeDdL4PRsKD6EnrC.htm](pathfinder-bestiary-3-items/QeDdL4PRsKD6EnrC.htm)|Rock|auto-trad|
|[qedIdKEDmmqyp8F5.htm](pathfinder-bestiary-3-items/qedIdKEDmmqyp8F5.htm)|Low-Light Vision|auto-trad|
|[QeGks9jOjqzdzWci.htm](pathfinder-bestiary-3-items/QeGks9jOjqzdzWci.htm)|Bloodsense (Imprecise) 60 feet|auto-trad|
|[qEjTHqZLDRx2o0dJ.htm](pathfinder-bestiary-3-items/qEjTHqZLDRx2o0dJ.htm)|Claw|auto-trad|
|[qen9rYoBFDQD84n7.htm](pathfinder-bestiary-3-items/qen9rYoBFDQD84n7.htm)|Cannonade|auto-trad|
|[QEObXX17C19vzj8L.htm](pathfinder-bestiary-3-items/QEObXX17C19vzj8L.htm)|Tail|auto-trad|
|[Qew18sYA4NJJ2n7y.htm](pathfinder-bestiary-3-items/Qew18sYA4NJJ2n7y.htm)|Fed by Water|auto-trad|
|[QfgdLYrvrXT358Kh.htm](pathfinder-bestiary-3-items/QfgdLYrvrXT358Kh.htm)|Body|auto-trad|
|[QfsNS0isn7JsRZIX.htm](pathfinder-bestiary-3-items/QfsNS0isn7JsRZIX.htm)|Telepathy 100 feet|auto-trad|
|[qgbvTpc5c7E9GDGd.htm](pathfinder-bestiary-3-items/qgbvTpc5c7E9GDGd.htm)|Jaws|auto-trad|
|[Qgr1ThnIaqkHRArD.htm](pathfinder-bestiary-3-items/Qgr1ThnIaqkHRArD.htm)|At-Will Spells|auto-trad|
|[qH5DHTYxShVT5a5y.htm](pathfinder-bestiary-3-items/qH5DHTYxShVT5a5y.htm)|Curse of the Werecrocodile|auto-trad|
|[qhMGpfia6l2CmKf0.htm](pathfinder-bestiary-3-items/qhMGpfia6l2CmKf0.htm)|Leech Thought|auto-trad|
|[qhoxtRXXWKvmotj5.htm](pathfinder-bestiary-3-items/qhoxtRXXWKvmotj5.htm)|Darkvision|auto-trad|
|[qHZIM3SkMqqfQWWG.htm](pathfinder-bestiary-3-items/qHZIM3SkMqqfQWWG.htm)|Beak|auto-trad|
|[qi2lvWIXN1GWDfm1.htm](pathfinder-bestiary-3-items/qi2lvWIXN1GWDfm1.htm)|Entangling Train|auto-trad|
|[qI7na98jgBlQpNui.htm](pathfinder-bestiary-3-items/qI7na98jgBlQpNui.htm)|Blade Ally|auto-trad|
|[qiEzVS822d89t62v.htm](pathfinder-bestiary-3-items/qiEzVS822d89t62v.htm)|Occult Innate Spells|auto-trad|
|[qJi07N1qiZ8im9Ed.htm](pathfinder-bestiary-3-items/qJi07N1qiZ8im9Ed.htm)|Healing Arrow|auto-trad|
|[qjiQvNcg8Xp6U5g6.htm](pathfinder-bestiary-3-items/qjiQvNcg8Xp6U5g6.htm)|Jaws|auto-trad|
|[QJZgoLZp0BfuvxvJ.htm](pathfinder-bestiary-3-items/QJZgoLZp0BfuvxvJ.htm)|Echolocation (Precise) 120 feet|auto-trad|
|[qkbQpoQc1isiqlSL.htm](pathfinder-bestiary-3-items/qkbQpoQc1isiqlSL.htm)|Spiritual Rope|auto-trad|
|[qki0wO5XwHHhxpMl.htm](pathfinder-bestiary-3-items/qki0wO5XwHHhxpMl.htm)|Primal Innate Spells|auto-trad|
|[qKR5KyDCt5y2Z7A4.htm](pathfinder-bestiary-3-items/qKR5KyDCt5y2Z7A4.htm)|Liquefy|auto-trad|
|[qkU2EydXIrYI5Hxx.htm](pathfinder-bestiary-3-items/qkU2EydXIrYI5Hxx.htm)|Tusk|auto-trad|
|[qLKTHssJqXd4ynva.htm](pathfinder-bestiary-3-items/qLKTHssJqXd4ynva.htm)|Darkvision|auto-trad|
|[qm3E8aZeyjHSz3i8.htm](pathfinder-bestiary-3-items/qm3E8aZeyjHSz3i8.htm)|Rend|auto-trad|
|[QNcsHGzGsxzU10k0.htm](pathfinder-bestiary-3-items/QNcsHGzGsxzU10k0.htm)|Consume Spell|auto-trad|
|[qNUdm1wGl9coLrPP.htm](pathfinder-bestiary-3-items/qNUdm1wGl9coLrPP.htm)|Dagger|auto-trad|
|[QOpnCjoiZN5QnY8X.htm](pathfinder-bestiary-3-items/QOpnCjoiZN5QnY8X.htm)|Jaws|auto-trad|
|[qP4bmFVnqKAeVCn1.htm](pathfinder-bestiary-3-items/qP4bmFVnqKAeVCn1.htm)|At-Will Spells|auto-trad|
|[QP9beVsoQsi5P8A5.htm](pathfinder-bestiary-3-items/QP9beVsoQsi5P8A5.htm)|Improved Grab|auto-trad|
|[qpCzKmCc7RVqEcVK.htm](pathfinder-bestiary-3-items/qpCzKmCc7RVqEcVK.htm)|At-Will Spells|auto-trad|
|[qppBAh5FNr8f4f2X.htm](pathfinder-bestiary-3-items/qppBAh5FNr8f4f2X.htm)|Divine Destruction|auto-trad|
|[qQZbVChzEGjI5sIk.htm](pathfinder-bestiary-3-items/qQZbVChzEGjI5sIk.htm)|Claw|auto-trad|
|[qRW2k1VmeIkTIOB9.htm](pathfinder-bestiary-3-items/qRW2k1VmeIkTIOB9.htm)|Throw Rock|auto-trad|
|[qSCF9pmEsCQejxxw.htm](pathfinder-bestiary-3-items/qSCF9pmEsCQejxxw.htm)|Lifesense 30 feet|auto-trad|
|[qSEQHwgppWcWoGDK.htm](pathfinder-bestiary-3-items/qSEQHwgppWcWoGDK.htm)|Tail|auto-trad|
|[QSp7Nee2yP67M9zp.htm](pathfinder-bestiary-3-items/QSp7Nee2yP67M9zp.htm)|Divine Innate Spells|auto-trad|
|[qT7gzi3MA0fVfY8a.htm](pathfinder-bestiary-3-items/qT7gzi3MA0fVfY8a.htm)|Grab|auto-trad|
|[QTLnr9MYtcbjoq6C.htm](pathfinder-bestiary-3-items/QTLnr9MYtcbjoq6C.htm)|Negative Healing|auto-trad|
|[QtPVup2bHHIHGyCe.htm](pathfinder-bestiary-3-items/QtPVup2bHHIHGyCe.htm)|Attack of Opportunity|auto-trad|
|[qTVf39ECIPyC5jMa.htm](pathfinder-bestiary-3-items/qTVf39ECIPyC5jMa.htm)|Negative Healing|auto-trad|
|[Qu61Gf9rt5ANopIq.htm](pathfinder-bestiary-3-items/Qu61Gf9rt5ANopIq.htm)|Primal Innate Spells|auto-trad|
|[quetxJ36kYJJorBD.htm](pathfinder-bestiary-3-items/quetxJ36kYJJorBD.htm)|Feathered Charge|auto-trad|
|[qUO24kmJdTM3pw3H.htm](pathfinder-bestiary-3-items/qUO24kmJdTM3pw3H.htm)|Jaws|auto-trad|
|[QUp7RYwQLvBoTGp7.htm](pathfinder-bestiary-3-items/QUp7RYwQLvBoTGp7.htm)|Grab|auto-trad|
|[QuSzkFT66YGAGqLE.htm](pathfinder-bestiary-3-items/QuSzkFT66YGAGqLE.htm)|Occult Innate Spells|auto-trad|
|[qVbkGmZA4J4JEc5c.htm](pathfinder-bestiary-3-items/qVbkGmZA4J4JEc5c.htm)|Temporal Sense|auto-trad|
|[qvJ5SX7zo7TKIP2h.htm](pathfinder-bestiary-3-items/qvJ5SX7zo7TKIP2h.htm)|Toenail Cutter|auto-trad|
|[qVqG3qPrNw7cF8D4.htm](pathfinder-bestiary-3-items/qVqG3qPrNw7cF8D4.htm)|Breath Weapon|auto-trad|
|[QvzzSlemey10PLLP.htm](pathfinder-bestiary-3-items/QvzzSlemey10PLLP.htm)|Claw|auto-trad|
|[QwKsTc2ps0cjzmnW.htm](pathfinder-bestiary-3-items/QwKsTc2ps0cjzmnW.htm)|Jaws|auto-trad|
|[QxQT0BLNv0boSPAf.htm](pathfinder-bestiary-3-items/QxQT0BLNv0boSPAf.htm)|Abandon Puppet|auto-trad|
|[QXV1JtzUVWMyWGFW.htm](pathfinder-bestiary-3-items/QXV1JtzUVWMyWGFW.htm)|Thoughtsense (Imprecise) 120 feet|auto-trad|
|[QyBhco1VMbstqt4y.htm](pathfinder-bestiary-3-items/QyBhco1VMbstqt4y.htm)|Branch|auto-trad|
|[qYJ6jUOSJMEwOOuC.htm](pathfinder-bestiary-3-items/qYJ6jUOSJMEwOOuC.htm)|Nibble|auto-trad|
|[Qz5ELLWHhm9W0RD3.htm](pathfinder-bestiary-3-items/Qz5ELLWHhm9W0RD3.htm)|Gnathobase|auto-trad|
|[qzIjJHwF4eyLDmOh.htm](pathfinder-bestiary-3-items/qzIjJHwF4eyLDmOh.htm)|Fountain Pen|auto-trad|
|[QZPUv0k24Dx8d0JZ.htm](pathfinder-bestiary-3-items/QZPUv0k24Dx8d0JZ.htm)|At-Will Spells|auto-trad|
|[QZx7vPULEK8wzVbx.htm](pathfinder-bestiary-3-items/QZx7vPULEK8wzVbx.htm)|Void Weapon|auto-trad|
|[R0bN0QbVsJDFnHId.htm](pathfinder-bestiary-3-items/R0bN0QbVsJDFnHId.htm)|Dagger|auto-trad|
|[r0CnvIkvKsI5FMgI.htm](pathfinder-bestiary-3-items/r0CnvIkvKsI5FMgI.htm)|Thoughtsense (Precise) 120 feet|auto-trad|
|[R0HLiBnfzTgXGH4w.htm](pathfinder-bestiary-3-items/R0HLiBnfzTgXGH4w.htm)|Staff|auto-trad|
|[R0vde3bCSBW2a8br.htm](pathfinder-bestiary-3-items/R0vde3bCSBW2a8br.htm)|Clinging Bites|auto-trad|
|[R1JY8Uw7yAyXK7JQ.htm](pathfinder-bestiary-3-items/R1JY8Uw7yAyXK7JQ.htm)|Sunset Ribbon|auto-trad|
|[R2Ck3XzpQHa6qLKl.htm](pathfinder-bestiary-3-items/R2Ck3XzpQHa6qLKl.htm)|Darkvision|auto-trad|
|[r3ldx2ZybEAqaKTp.htm](pathfinder-bestiary-3-items/r3ldx2ZybEAqaKTp.htm)|+2 Perception to Sense Motive|auto-trad|
|[r3zwcLhwIxuPvofI.htm](pathfinder-bestiary-3-items/r3zwcLhwIxuPvofI.htm)|Fist|auto-trad|
|[r4jLytkNtr1uMewT.htm](pathfinder-bestiary-3-items/r4jLytkNtr1uMewT.htm)|Gleaming Armor|auto-trad|
|[R4srCUwoLVvI4h6T.htm](pathfinder-bestiary-3-items/R4srCUwoLVvI4h6T.htm)|Negative Healing|auto-trad|
|[R4UxTvMjd8XGo0H1.htm](pathfinder-bestiary-3-items/R4UxTvMjd8XGo0H1.htm)|Darkvision|auto-trad|
|[R4Vzee8KxsqTF0Wf.htm](pathfinder-bestiary-3-items/R4Vzee8KxsqTF0Wf.htm)|Darkvision|auto-trad|
|[r5rP9a4eNdlmkjiO.htm](pathfinder-bestiary-3-items/r5rP9a4eNdlmkjiO.htm)|Claw|auto-trad|
|[r5va25MjWQsL5Nut.htm](pathfinder-bestiary-3-items/r5va25MjWQsL5Nut.htm)|At-Will Spells|auto-trad|
|[R6LPamKnpozwfOnm.htm](pathfinder-bestiary-3-items/R6LPamKnpozwfOnm.htm)|Telepathic Ballad|auto-trad|
|[R6XHv4H8ZshCgHND.htm](pathfinder-bestiary-3-items/R6XHv4H8ZshCgHND.htm)|Grab|auto-trad|
|[r9r9sl3qGETAhlH9.htm](pathfinder-bestiary-3-items/r9r9sl3qGETAhlH9.htm)|Divine Innate Spells|auto-trad|
|[r9s9fT1PAiICOzs0.htm](pathfinder-bestiary-3-items/r9s9fT1PAiICOzs0.htm)|Telepathy 60 feet|auto-trad|
|[raEWo6pY0qJvkkyn.htm](pathfinder-bestiary-3-items/raEWo6pY0qJvkkyn.htm)|+2 Status to All Saves vs. Poison|auto-trad|
|[Ram97nW6kaJ9IBc0.htm](pathfinder-bestiary-3-items/Ram97nW6kaJ9IBc0.htm)|Constant Spells|auto-trad|
|[rb3UkMRburjTZJpj.htm](pathfinder-bestiary-3-items/rb3UkMRburjTZJpj.htm)|At-Will Spells|auto-trad|
|[RbIcnu5Fx9eAX5Uq.htm](pathfinder-bestiary-3-items/RbIcnu5Fx9eAX5Uq.htm)|At-Will Spells|auto-trad|
|[RbKBM2RsozHFLA9U.htm](pathfinder-bestiary-3-items/RbKBM2RsozHFLA9U.htm)|Bond with Mortal|auto-trad|
|[rbM0VDuWa5kOfsbQ.htm](pathfinder-bestiary-3-items/rbM0VDuWa5kOfsbQ.htm)|Tentacle|auto-trad|
|[RBrP1ThmfTW2EUCG.htm](pathfinder-bestiary-3-items/RBrP1ThmfTW2EUCG.htm)|Whistling Bones|auto-trad|
|[RcFq3iNSRnikQSug.htm](pathfinder-bestiary-3-items/RcFq3iNSRnikQSug.htm)|Tusk|auto-trad|
|[RcHSgAM2xaq0Lve5.htm](pathfinder-bestiary-3-items/RcHSgAM2xaq0Lve5.htm)|Easily Fascinated|auto-trad|
|[RcQhtjlnYqR8A8J9.htm](pathfinder-bestiary-3-items/RcQhtjlnYqR8A8J9.htm)|Spray Perfume|auto-trad|
|[RdbeYdVwFLgopJjk.htm](pathfinder-bestiary-3-items/RdbeYdVwFLgopJjk.htm)|Talon|auto-trad|
|[RDF08DgCVW9LDvLd.htm](pathfinder-bestiary-3-items/RDF08DgCVW9LDvLd.htm)|Change Shape|auto-trad|
|[RdGjfkq57iIV43T3.htm](pathfinder-bestiary-3-items/RdGjfkq57iIV43T3.htm)|Telepathy 30 feet|auto-trad|
|[RdiP9Lesck92HsdO.htm](pathfinder-bestiary-3-items/RdiP9Lesck92HsdO.htm)|Forest Shape|auto-trad|
|[Rdy2GbIHuRoKy7bU.htm](pathfinder-bestiary-3-items/Rdy2GbIHuRoKy7bU.htm)|Fed by Wood|auto-trad|
|[Re0uYYA9c4nnaRne.htm](pathfinder-bestiary-3-items/Re0uYYA9c4nnaRne.htm)|Primal Innate Spells|auto-trad|
|[Re3laGuS2iEiMxt0.htm](pathfinder-bestiary-3-items/Re3laGuS2iEiMxt0.htm)|Burning Touch|auto-trad|
|[Re8xzF2naY7y8ZBf.htm](pathfinder-bestiary-3-items/Re8xzF2naY7y8ZBf.htm)|Mauling Rush|auto-trad|
|[RE9BxxRzi4ENZrXg.htm](pathfinder-bestiary-3-items/RE9BxxRzi4ENZrXg.htm)|Proboscis Tongue|auto-trad|
|[REbcJzGp6sT7jOuZ.htm](pathfinder-bestiary-3-items/REbcJzGp6sT7jOuZ.htm)|Limb|auto-trad|
|[rFakD0KrlUr9DJiQ.htm](pathfinder-bestiary-3-items/rFakD0KrlUr9DJiQ.htm)|Claw|auto-trad|
|[RFaRYNVP3GPdcMkY.htm](pathfinder-bestiary-3-items/RFaRYNVP3GPdcMkY.htm)|Darkvision|auto-trad|
|[RfjRZPiLUrrypXhA.htm](pathfinder-bestiary-3-items/RfjRZPiLUrrypXhA.htm)|At-Will Spells|auto-trad|
|[RfXLcZpuY88IxZTD.htm](pathfinder-bestiary-3-items/RfXLcZpuY88IxZTD.htm)|Catch Rock|auto-trad|
|[rgq47BGgm0jbIXb6.htm](pathfinder-bestiary-3-items/rgq47BGgm0jbIXb6.htm)|Darkvision|auto-trad|
|[rGx3BT8mbRKRUpA2.htm](pathfinder-bestiary-3-items/rGx3BT8mbRKRUpA2.htm)|Destabilizing Field|auto-trad|
|[RIF4K2wDuZCHWToa.htm](pathfinder-bestiary-3-items/RIF4K2wDuZCHWToa.htm)|At-Will Spells|auto-trad|
|[RIfmoCoHRDlhRlU1.htm](pathfinder-bestiary-3-items/RIfmoCoHRDlhRlU1.htm)|Claw|auto-trad|
|[riFooTptrWPMV3tk.htm](pathfinder-bestiary-3-items/riFooTptrWPMV3tk.htm)|+1 Status to All Saves vs. Evil|auto-trad|
|[riqBNngN6YkDWYqv.htm](pathfinder-bestiary-3-items/riqBNngN6YkDWYqv.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[rIrMS5z48NzYuh9b.htm](pathfinder-bestiary-3-items/rIrMS5z48NzYuh9b.htm)|Impending Dread|auto-trad|
|[rjh44a42EWaBBBP1.htm](pathfinder-bestiary-3-items/rjh44a42EWaBBBP1.htm)|Fed by Earth|auto-trad|
|[rjVHVaa9BN3ZAiKL.htm](pathfinder-bestiary-3-items/rjVHVaa9BN3ZAiKL.htm)|Spike Storm|auto-trad|
|[RKz5iqiz10TiodA6.htm](pathfinder-bestiary-3-items/RKz5iqiz10TiodA6.htm)|Breath Weapon|auto-trad|
|[RKZuKmTaOGPFwnzJ.htm](pathfinder-bestiary-3-items/RKZuKmTaOGPFwnzJ.htm)|Impossible Stature|auto-trad|
|[rL2cjpuNRY9BBahF.htm](pathfinder-bestiary-3-items/rL2cjpuNRY9BBahF.htm)|Attack of Opportunity|auto-trad|
|[rlXD928xXNnyihPK.htm](pathfinder-bestiary-3-items/rlXD928xXNnyihPK.htm)|At-Will Spells|auto-trad|
|[RM8HImoxZfJtj8F9.htm](pathfinder-bestiary-3-items/RM8HImoxZfJtj8F9.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[rmezKNjwT2IakFHV.htm](pathfinder-bestiary-3-items/rmezKNjwT2IakFHV.htm)|Spine|auto-trad|
|[rmrUx8mVzDx7W96E.htm](pathfinder-bestiary-3-items/rmrUx8mVzDx7W96E.htm)|Flames of Fury|auto-trad|
|[Rmtq9X0tzbZPgELk.htm](pathfinder-bestiary-3-items/Rmtq9X0tzbZPgELk.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[Rmxe7dutW3AoDW4m.htm](pathfinder-bestiary-3-items/Rmxe7dutW3AoDW4m.htm)|Echolocation (Precise) 40 feet|auto-trad|
|[rNBF8QplzOETa39b.htm](pathfinder-bestiary-3-items/rNBF8QplzOETa39b.htm)|Branch|auto-trad|
|[RP2SeHrbTJkiKCCL.htm](pathfinder-bestiary-3-items/RP2SeHrbTJkiKCCL.htm)|Constrict|auto-trad|
|[rpBeKBUmHNZG8uQA.htm](pathfinder-bestiary-3-items/rpBeKBUmHNZG8uQA.htm)|Hoof|auto-trad|
|[rPhxztMzLxuNALLo.htm](pathfinder-bestiary-3-items/rPhxztMzLxuNALLo.htm)|Primal Innate Spells|auto-trad|
|[RpO4Le4eD3npO4n3.htm](pathfinder-bestiary-3-items/RpO4Le4eD3npO4n3.htm)|Low-Light Vision|auto-trad|
|[rPTqCxoOB0B5ON1m.htm](pathfinder-bestiary-3-items/rPTqCxoOB0B5ON1m.htm)|Weapon Arm|auto-trad|
|[rQhc6SkjKUBdbb2C.htm](pathfinder-bestiary-3-items/rQhc6SkjKUBdbb2C.htm)|Darkvision|auto-trad|
|[rtGARtAYnK1Pseab.htm](pathfinder-bestiary-3-items/rtGARtAYnK1Pseab.htm)|Darkvision|auto-trad|
|[RTMzhRViOSVhuoXI.htm](pathfinder-bestiary-3-items/RTMzhRViOSVhuoXI.htm)|Electric Projectiles|auto-trad|
|[rU17t3NM7TJ3u03o.htm](pathfinder-bestiary-3-items/rU17t3NM7TJ3u03o.htm)|Regeneration 10 (Deactivated by Piercing)|auto-trad|
|[rUKEfvwyoUMhaFzX.htm](pathfinder-bestiary-3-items/rUKEfvwyoUMhaFzX.htm)|Vicelike Jaws|auto-trad|
|[RUqAGMQwHrGpfJPs.htm](pathfinder-bestiary-3-items/RUqAGMQwHrGpfJPs.htm)|Arcane Tendril|auto-trad|
|[rVdhm2xzR2AUjY4g.htm](pathfinder-bestiary-3-items/rVdhm2xzR2AUjY4g.htm)|Beak|auto-trad|
|[RvhHvUE7nZmwkyMD.htm](pathfinder-bestiary-3-items/RvhHvUE7nZmwkyMD.htm)|Accord Essence|auto-trad|
|[RVk9mcBqt1YRswGY.htm](pathfinder-bestiary-3-items/RVk9mcBqt1YRswGY.htm)|Desert-Adapted|auto-trad|
|[rvv40f4QmFMFlF5L.htm](pathfinder-bestiary-3-items/rvv40f4QmFMFlF5L.htm)|Jaws|auto-trad|
|[RwrYgxqil6oSI14J.htm](pathfinder-bestiary-3-items/RwrYgxqil6oSI14J.htm)|Hurl Net|auto-trad|
|[rXKR8tn1tmh9DdJ3.htm](pathfinder-bestiary-3-items/rXKR8tn1tmh9DdJ3.htm)|Jaws|auto-trad|
|[rXrJgVzgOwMP7Cbr.htm](pathfinder-bestiary-3-items/rXrJgVzgOwMP7Cbr.htm)|Stable Stance|auto-trad|
|[rxsLa6trqZBzZIOL.htm](pathfinder-bestiary-3-items/rxsLa6trqZBzZIOL.htm)|Impossible Stature|auto-trad|
|[rxu5E3okZFA5hwkO.htm](pathfinder-bestiary-3-items/rxu5E3okZFA5hwkO.htm)|Plaque Burst|auto-trad|
|[RxVHmIYJh9eOX9c3.htm](pathfinder-bestiary-3-items/RxVHmIYJh9eOX9c3.htm)|Greater Darkvision|auto-trad|
|[rYBfhejHUk9mGJyA.htm](pathfinder-bestiary-3-items/rYBfhejHUk9mGJyA.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[RyfXnY4RnP8Gsh8n.htm](pathfinder-bestiary-3-items/RyfXnY4RnP8Gsh8n.htm)|Vulnerable to Calm Emotions|auto-trad|
|[rYN4yg46PUJy1sS9.htm](pathfinder-bestiary-3-items/rYN4yg46PUJy1sS9.htm)|Tail|auto-trad|
|[rYubtl6wVH1V3yQM.htm](pathfinder-bestiary-3-items/rYubtl6wVH1V3yQM.htm)|Surface-Bound|auto-trad|
|[rZeB3sV7jSAvG69a.htm](pathfinder-bestiary-3-items/rZeB3sV7jSAvG69a.htm)|Darkvision|auto-trad|
|[S0fNGUpgdHgR1UVg.htm](pathfinder-bestiary-3-items/S0fNGUpgdHgR1UVg.htm)|Occult Innate Spells|auto-trad|
|[s2oghprwMzFfjM11.htm](pathfinder-bestiary-3-items/s2oghprwMzFfjM11.htm)|Shy|auto-trad|
|[S2oLvvECz7beXP1O.htm](pathfinder-bestiary-3-items/S2oLvvECz7beXP1O.htm)|Jaws|auto-trad|
|[S3r2AvwbHmmhBwB3.htm](pathfinder-bestiary-3-items/S3r2AvwbHmmhBwB3.htm)|Regression|auto-trad|
|[S3S752aZGm6sxlqG.htm](pathfinder-bestiary-3-items/S3S752aZGm6sxlqG.htm)|Knockdown|auto-trad|
|[S5fuvAOuYoMnXjOB.htm](pathfinder-bestiary-3-items/S5fuvAOuYoMnXjOB.htm)|Darkvision|auto-trad|
|[s65OrlZwX6JQgM40.htm](pathfinder-bestiary-3-items/s65OrlZwX6JQgM40.htm)|Radiant Mantle|auto-trad|
|[s6BLghmvkOpXc6TW.htm](pathfinder-bestiary-3-items/s6BLghmvkOpXc6TW.htm)|Darkvision|auto-trad|
|[s6k7oyMyiZKIqAar.htm](pathfinder-bestiary-3-items/s6k7oyMyiZKIqAar.htm)|Claw|auto-trad|
|[s6UD93sTVBhNgF8P.htm](pathfinder-bestiary-3-items/s6UD93sTVBhNgF8P.htm)|Assume Fiery Form|auto-trad|
|[s7ODA9MMbyTJSlyF.htm](pathfinder-bestiary-3-items/s7ODA9MMbyTJSlyF.htm)|Lifesense 30 feet|auto-trad|
|[S7uNAygkE7sTVfcL.htm](pathfinder-bestiary-3-items/S7uNAygkE7sTVfcL.htm)|Vindictive Crush|auto-trad|
|[s87z24wrwStP1Ouk.htm](pathfinder-bestiary-3-items/s87z24wrwStP1Ouk.htm)|Positive Energy Transfer|auto-trad|
|[S8A8KdLa7fpa3GXF.htm](pathfinder-bestiary-3-items/S8A8KdLa7fpa3GXF.htm)|Ectoplasmic Form|auto-trad|
|[S8UsqHDRsdN6XtV5.htm](pathfinder-bestiary-3-items/S8UsqHDRsdN6XtV5.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[s9EYmFKDJ3ygchjQ.htm](pathfinder-bestiary-3-items/s9EYmFKDJ3ygchjQ.htm)|Darkvision|auto-trad|
|[S9RBBqUiQCE9JKB1.htm](pathfinder-bestiary-3-items/S9RBBqUiQCE9JKB1.htm)|Emotionally Unaware|auto-trad|
|[s9Y9sorFMjlmE7OS.htm](pathfinder-bestiary-3-items/s9Y9sorFMjlmE7OS.htm)|Anathematic Aversion|auto-trad|
|[Sa5PkHx5wTuKIBts.htm](pathfinder-bestiary-3-items/Sa5PkHx5wTuKIBts.htm)|Constrict|auto-trad|
|[SA7P07cUNUdSC4Sp.htm](pathfinder-bestiary-3-items/SA7P07cUNUdSC4Sp.htm)|Betrayal Toxin|auto-trad|
|[sAINNUJA1VNOBd31.htm](pathfinder-bestiary-3-items/sAINNUJA1VNOBd31.htm)|Lifesense 60 feet|auto-trad|
|[SamwGEbNZfw3hWWZ.htm](pathfinder-bestiary-3-items/SamwGEbNZfw3hWWZ.htm)|Rapier|auto-trad|
|[SaZttWSFJOlcV8gF.htm](pathfinder-bestiary-3-items/SaZttWSFJOlcV8gF.htm)|Violent Retort|auto-trad|
|[SBSCHoUK3Je0TZWJ.htm](pathfinder-bestiary-3-items/SBSCHoUK3Je0TZWJ.htm)|Axe Vulnerability|auto-trad|
|[sBY73yijWI9qogpX.htm](pathfinder-bestiary-3-items/sBY73yijWI9qogpX.htm)|All-Around Vision|auto-trad|
|[SbZX27efEktYhABv.htm](pathfinder-bestiary-3-items/SbZX27efEktYhABv.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[Scblqwycs4aZk1u1.htm](pathfinder-bestiary-3-items/Scblqwycs4aZk1u1.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[sCnH4uFy8fBv6bnP.htm](pathfinder-bestiary-3-items/sCnH4uFy8fBv6bnP.htm)|No Breath|auto-trad|
|[SCRF3318aESeiNRz.htm](pathfinder-bestiary-3-items/SCRF3318aESeiNRz.htm)|Idols of Stone|auto-trad|
|[ScsosKeNG0BlbPN5.htm](pathfinder-bestiary-3-items/ScsosKeNG0BlbPN5.htm)|Hallucinatory Haunting|auto-trad|
|[sCv6nwuflODCdMaO.htm](pathfinder-bestiary-3-items/sCv6nwuflODCdMaO.htm)|Portentous Gaze|auto-trad|
|[SCWOEv6X8RtegOjU.htm](pathfinder-bestiary-3-items/SCWOEv6X8RtegOjU.htm)|Scythe|auto-trad|
|[SE2Oc5VGiAxYtDb3.htm](pathfinder-bestiary-3-items/SE2Oc5VGiAxYtDb3.htm)|Crossbow|auto-trad|
|[se2uQ9cHOTmmlxON.htm](pathfinder-bestiary-3-items/se2uQ9cHOTmmlxON.htm)|Spectral Claw|auto-trad|
|[sEbF3y3ttLqHmwtS.htm](pathfinder-bestiary-3-items/sEbF3y3ttLqHmwtS.htm)|Flame Ray|auto-trad|
|[SEw7wjeoKGHYcloR.htm](pathfinder-bestiary-3-items/SEw7wjeoKGHYcloR.htm)|Accord Essence|auto-trad|
|[SF8eX3XCiOBA6wh4.htm](pathfinder-bestiary-3-items/SF8eX3XCiOBA6wh4.htm)|Intuit Object|auto-trad|
|[SfFcwGTaNbZ6zFNW.htm](pathfinder-bestiary-3-items/SfFcwGTaNbZ6zFNW.htm)|Spear|auto-trad|
|[sfp7Tgb12PISMHBT.htm](pathfinder-bestiary-3-items/sfp7Tgb12PISMHBT.htm)|Fist|auto-trad|
|[SfQfbmrgZ4vr3nFt.htm](pathfinder-bestiary-3-items/SfQfbmrgZ4vr3nFt.htm)|Spear|auto-trad|
|[sfynZ9FF1MvgyIsO.htm](pathfinder-bestiary-3-items/sfynZ9FF1MvgyIsO.htm)|At-Will Spells|auto-trad|
|[sGASuiT2DzsMvSIa.htm](pathfinder-bestiary-3-items/sGASuiT2DzsMvSIa.htm)|Divine Innate Spells|auto-trad|
|[sGEoB9eohFcV4gEe.htm](pathfinder-bestiary-3-items/sGEoB9eohFcV4gEe.htm)|Regeneration 50 (Deactivated by Fire)|auto-trad|
|[SGw3tiHnJxU3HeUz.htm](pathfinder-bestiary-3-items/SGw3tiHnJxU3HeUz.htm)|Darkvision|auto-trad|
|[SgXWjRi0fOvCndJM.htm](pathfinder-bestiary-3-items/SgXWjRi0fOvCndJM.htm)|Tongue|auto-trad|
|[Sh368mCxkzNFInV2.htm](pathfinder-bestiary-3-items/Sh368mCxkzNFInV2.htm)|Greataxe|auto-trad|
|[shT4gXjrfuduYtZr.htm](pathfinder-bestiary-3-items/shT4gXjrfuduYtZr.htm)|Hellwasp Stings|auto-trad|
|[sIbPX7HRtLZsZC7R.htm](pathfinder-bestiary-3-items/sIbPX7HRtLZsZC7R.htm)|Sneak Attack|auto-trad|
|[SIJ9zzLLshuohFwx.htm](pathfinder-bestiary-3-items/SIJ9zzLLshuohFwx.htm)|Darkvision|auto-trad|
|[sIJOxufLnRAxLd8H.htm](pathfinder-bestiary-3-items/sIJOxufLnRAxLd8H.htm)|Darkvision|auto-trad|
|[SIJXP2XGtiBASmHE.htm](pathfinder-bestiary-3-items/SIJXP2XGtiBASmHE.htm)|Constant Spells|auto-trad|
|[sIlgLiMP9ruto6w9.htm](pathfinder-bestiary-3-items/sIlgLiMP9ruto6w9.htm)|Scent (Imprecise) 40 feet|auto-trad|
|[sIWC4aZPyNtODaFY.htm](pathfinder-bestiary-3-items/sIWC4aZPyNtODaFY.htm)|Negative Healing|auto-trad|
|[sIZrd0MxYGEnR6YP.htm](pathfinder-bestiary-3-items/sIZrd0MxYGEnR6YP.htm)|Dual Opportunity|auto-trad|
|[sj9VbYqqHsDlLaRb.htm](pathfinder-bestiary-3-items/sj9VbYqqHsDlLaRb.htm)|Fire Jackal Saliva|auto-trad|
|[sjeNzkkJM6xfdPo8.htm](pathfinder-bestiary-3-items/sjeNzkkJM6xfdPo8.htm)|Eye Beam|auto-trad|
|[SJtGnpq0cBxkmxWc.htm](pathfinder-bestiary-3-items/SJtGnpq0cBxkmxWc.htm)|Punish Flight|auto-trad|
|[sK09hTiJx42HpDxj.htm](pathfinder-bestiary-3-items/sK09hTiJx42HpDxj.htm)|Nature's Rebirth|auto-trad|
|[sk5aFaaf3tRIgb0p.htm](pathfinder-bestiary-3-items/sk5aFaaf3tRIgb0p.htm)|Spear|auto-trad|
|[SKfXJh58tHRMQxNa.htm](pathfinder-bestiary-3-items/SKfXJh58tHRMQxNa.htm)|Low-Light Vision|auto-trad|
|[SKICe54HSoZHNbYB.htm](pathfinder-bestiary-3-items/SKICe54HSoZHNbYB.htm)|Darkvision|auto-trad|
|[sKizGnItFf8dn6kf.htm](pathfinder-bestiary-3-items/sKizGnItFf8dn6kf.htm)|Dooming Touch|auto-trad|
|[sl07Not81gCCYcg7.htm](pathfinder-bestiary-3-items/sl07Not81gCCYcg7.htm)|Cavern Empathy|auto-trad|
|[sLdsVr8WCWegLUjP.htm](pathfinder-bestiary-3-items/sLdsVr8WCWegLUjP.htm)|Darkvision|auto-trad|
|[SlKd6WmjF0C4lXGp.htm](pathfinder-bestiary-3-items/SlKd6WmjF0C4lXGp.htm)|Form Up|auto-trad|
|[sM1MA3hKVxt7ZvXp.htm](pathfinder-bestiary-3-items/sM1MA3hKVxt7ZvXp.htm)|+2 Circumstance to All Saves vs. Disease|auto-trad|
|[sm8dNJYFVglgLjGA.htm](pathfinder-bestiary-3-items/sm8dNJYFVglgLjGA.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[sMOW7L2I7HwP3CtJ.htm](pathfinder-bestiary-3-items/sMOW7L2I7HwP3CtJ.htm)|Reassemble|auto-trad|
|[Sn9QsKQ5fSuHrD9F.htm](pathfinder-bestiary-3-items/Sn9QsKQ5fSuHrD9F.htm)|At-Will Spells|auto-trad|
|[sNchZQFp9J0mXyVA.htm](pathfinder-bestiary-3-items/sNchZQFp9J0mXyVA.htm)|Throw Rock|auto-trad|
|[SnFwuUdykz9voojZ.htm](pathfinder-bestiary-3-items/SnFwuUdykz9voojZ.htm)|Darkvision|auto-trad|
|[sNGs8FpXUh0x309i.htm](pathfinder-bestiary-3-items/sNGs8FpXUh0x309i.htm)|Telepathy 30 feet|auto-trad|
|[sNth9LOE6nRfhE3n.htm](pathfinder-bestiary-3-items/sNth9LOE6nRfhE3n.htm)|Telepathy (Touch)|auto-trad|
|[sOt6KDS7Y34upG7G.htm](pathfinder-bestiary-3-items/sOt6KDS7Y34upG7G.htm)|Breath Weapon|auto-trad|
|[SP4I3NKUoM1EaTaK.htm](pathfinder-bestiary-3-items/SP4I3NKUoM1EaTaK.htm)|Occult Innate Spells|auto-trad|
|[SPB7FRg5vvaLT9yY.htm](pathfinder-bestiary-3-items/SPB7FRg5vvaLT9yY.htm)|Pummeling Assault|auto-trad|
|[sq2f8Tcx4gzFa7t5.htm](pathfinder-bestiary-3-items/sq2f8Tcx4gzFa7t5.htm)|Tusk|auto-trad|
|[SQ3H5jQFY4vWcYDC.htm](pathfinder-bestiary-3-items/SQ3H5jQFY4vWcYDC.htm)|Arcane Prepared Spells|auto-trad|
|[SQvpxbj1u4t8orbJ.htm](pathfinder-bestiary-3-items/SQvpxbj1u4t8orbJ.htm)|Ferocious Roar|auto-trad|
|[SrTNivRRl7pbrWZN.htm](pathfinder-bestiary-3-items/SrTNivRRl7pbrWZN.htm)|Ghostly Longbow|auto-trad|
|[sRU8pYLkFEMCRECp.htm](pathfinder-bestiary-3-items/sRU8pYLkFEMCRECp.htm)|Death Blaze|auto-trad|
|[SRv1ATMYjrhtxE5O.htm](pathfinder-bestiary-3-items/SRv1ATMYjrhtxE5O.htm)|Gloom Aura|auto-trad|
|[Sryn4awFAiEFPWJi.htm](pathfinder-bestiary-3-items/Sryn4awFAiEFPWJi.htm)|Telepathy 100 feet|auto-trad|
|[sT02s8CpPBlLaICN.htm](pathfinder-bestiary-3-items/sT02s8CpPBlLaICN.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[st3MDTSpYydB4U32.htm](pathfinder-bestiary-3-items/st3MDTSpYydB4U32.htm)|Material Leap|auto-trad|
|[stALstSmHYXTOJRU.htm](pathfinder-bestiary-3-items/stALstSmHYXTOJRU.htm)|Low-Light Vision|auto-trad|
|[STN5ZaMm14HJIg38.htm](pathfinder-bestiary-3-items/STN5ZaMm14HJIg38.htm)|Jaws|auto-trad|
|[stXqE9CezJeM4Whd.htm](pathfinder-bestiary-3-items/stXqE9CezJeM4Whd.htm)|Greatclub|auto-trad|
|[SU3ySHGYIBWN3dgx.htm](pathfinder-bestiary-3-items/SU3ySHGYIBWN3dgx.htm)|Adamantine Claw|auto-trad|
|[SUBRW3AOWawVs2rU.htm](pathfinder-bestiary-3-items/SUBRW3AOWawVs2rU.htm)|Trample|auto-trad|
|[SuGCwf8YVqJ0KZNG.htm](pathfinder-bestiary-3-items/SuGCwf8YVqJ0KZNG.htm)|Energy Ray|auto-trad|
|[SUKtpi6VGVyh7Jip.htm](pathfinder-bestiary-3-items/SUKtpi6VGVyh7Jip.htm)|Retaliatory Scratch|auto-trad|
|[SUQKMz3YCGazvNeH.htm](pathfinder-bestiary-3-items/SUQKMz3YCGazvNeH.htm)|At-Will Spells|auto-trad|
|[Svc6cpZQ69ghEA6T.htm](pathfinder-bestiary-3-items/Svc6cpZQ69ghEA6T.htm)|At-Will Spells|auto-trad|
|[SVF9VI2ywkitioon.htm](pathfinder-bestiary-3-items/SVF9VI2ywkitioon.htm)|Divine Innate Spells|auto-trad|
|[svqKqAYsrMhgU25p.htm](pathfinder-bestiary-3-items/svqKqAYsrMhgU25p.htm)|Occult Innate Spells|auto-trad|
|[sVS5x7UzSVU5jjX2.htm](pathfinder-bestiary-3-items/sVS5x7UzSVU5jjX2.htm)|Darkvision|auto-trad|
|[SVucgTqH9TEpdPX4.htm](pathfinder-bestiary-3-items/SVucgTqH9TEpdPX4.htm)|Divine Innate Spells|auto-trad|
|[sW0kQIjx6HrXLDiV.htm](pathfinder-bestiary-3-items/sW0kQIjx6HrXLDiV.htm)|Divine Innate Spells|auto-trad|
|[sX0MwC23Mvp1YeD2.htm](pathfinder-bestiary-3-items/sX0MwC23Mvp1YeD2.htm)|Merciless Thrust|auto-trad|
|[SYiq4FiF50sGlZtS.htm](pathfinder-bestiary-3-items/SYiq4FiF50sGlZtS.htm)|In Concert|auto-trad|
|[SZ0KpuFOSI1WY5Fo.htm](pathfinder-bestiary-3-items/SZ0KpuFOSI1WY5Fo.htm)|Low-Light Vision|auto-trad|
|[sZ10byXqZzWm4yp5.htm](pathfinder-bestiary-3-items/sZ10byXqZzWm4yp5.htm)|Throw Rock|auto-trad|
|[sZ2qJ9Ced7EAt6pU.htm](pathfinder-bestiary-3-items/sZ2qJ9Ced7EAt6pU.htm)|Fist|auto-trad|
|[SZBQldnYP5zx1PyP.htm](pathfinder-bestiary-3-items/SZBQldnYP5zx1PyP.htm)|Coiling Frenzy|auto-trad|
|[SzbZwTdWXUHNi17Y.htm](pathfinder-bestiary-3-items/SzbZwTdWXUHNi17Y.htm)|Claw|auto-trad|
|[SZjcOjfoi55nSlVu.htm](pathfinder-bestiary-3-items/SZjcOjfoi55nSlVu.htm)|Breath Weapon|auto-trad|
|[SzriRFiHHVYO1GYI.htm](pathfinder-bestiary-3-items/SzriRFiHHVYO1GYI.htm)|Gremlin Lice|auto-trad|
|[t0I4vgS1D9by3wX6.htm](pathfinder-bestiary-3-items/t0I4vgS1D9by3wX6.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[t0jxVnsS6HbKkoTf.htm](pathfinder-bestiary-3-items/t0jxVnsS6HbKkoTf.htm)|Divine Innate Spells|auto-trad|
|[T1AhLjoX6p5mr4gQ.htm](pathfinder-bestiary-3-items/T1AhLjoX6p5mr4gQ.htm)|Grab|auto-trad|
|[T27ladpNIH9pULrU.htm](pathfinder-bestiary-3-items/T27ladpNIH9pULrU.htm)|Divine Innate Spells|auto-trad|
|[t2HIuBRLJiGYJZXE.htm](pathfinder-bestiary-3-items/t2HIuBRLJiGYJZXE.htm)|At-Will Spells|auto-trad|
|[t4NelvHMno5EsQBb.htm](pathfinder-bestiary-3-items/t4NelvHMno5EsQBb.htm)|Penanggalan Bile|auto-trad|
|[T4S3miQ63r06a6Bu.htm](pathfinder-bestiary-3-items/T4S3miQ63r06a6Bu.htm)|Low-Light Vision|auto-trad|
|[T52SfDBkv0NA6Q8o.htm](pathfinder-bestiary-3-items/T52SfDBkv0NA6Q8o.htm)|Pincer|auto-trad|
|[t6e0LBuCBj0cSlzG.htm](pathfinder-bestiary-3-items/t6e0LBuCBj0cSlzG.htm)|Ward|auto-trad|
|[T6ZAqc1mMzmpUCtm.htm](pathfinder-bestiary-3-items/T6ZAqc1mMzmpUCtm.htm)|Barbed Net|auto-trad|
|[t73fUopYHfJiDPvy.htm](pathfinder-bestiary-3-items/t73fUopYHfJiDPvy.htm)|Grab|auto-trad|
|[T7cl6r9Q9cdH7q2Q.htm](pathfinder-bestiary-3-items/T7cl6r9Q9cdH7q2Q.htm)|Anchored Soul|auto-trad|
|[t9cow1t47Ns4t0H0.htm](pathfinder-bestiary-3-items/t9cow1t47Ns4t0H0.htm)|Ravenspeaker|auto-trad|
|[TAMn8gMkkXRH5ctl.htm](pathfinder-bestiary-3-items/TAMn8gMkkXRH5ctl.htm)|Darkvision|auto-trad|
|[tAmUn1fmHBzAZrMX.htm](pathfinder-bestiary-3-items/tAmUn1fmHBzAZrMX.htm)|Arrow Volley|auto-trad|
|[tAtaZY5U93gi2tUw.htm](pathfinder-bestiary-3-items/tAtaZY5U93gi2tUw.htm)|Leech Moisture|auto-trad|
|[tauTat1vId2N5H4u.htm](pathfinder-bestiary-3-items/tauTat1vId2N5H4u.htm)|Bite|auto-trad|
|[Taw4K2MqiOVhIddu.htm](pathfinder-bestiary-3-items/Taw4K2MqiOVhIddu.htm)|Pain Touch|auto-trad|
|[Tb6i1oczFczW6Kyf.htm](pathfinder-bestiary-3-items/Tb6i1oczFczW6Kyf.htm)|Jaws|auto-trad|
|[TBMl5Ts5TOwuhfd4.htm](pathfinder-bestiary-3-items/TBMl5Ts5TOwuhfd4.htm)|+2 Circumstance to All Saves vs. Disease|auto-trad|
|[tBtFLroAVGw32fIG.htm](pathfinder-bestiary-3-items/tBtFLroAVGw32fIG.htm)|Negative Healing|auto-trad|
|[tC5SlnihlaxwiNPE.htm](pathfinder-bestiary-3-items/tC5SlnihlaxwiNPE.htm)|Horn|auto-trad|
|[TcyFcfEZxlVqc3lO.htm](pathfinder-bestiary-3-items/TcyFcfEZxlVqc3lO.htm)|At-Will Spells|auto-trad|
|[td3gay0LzawYp5Ff.htm](pathfinder-bestiary-3-items/td3gay0LzawYp5Ff.htm)|Death Throes|auto-trad|
|[td7f48qGt23BZNFA.htm](pathfinder-bestiary-3-items/td7f48qGt23BZNFA.htm)|Telepathy 100 feet|auto-trad|
|[Tdbu943P6LKUO8bz.htm](pathfinder-bestiary-3-items/Tdbu943P6LKUO8bz.htm)|Arcane Prepared Spells|auto-trad|
|[Te1JpsOS99z9IDyD.htm](pathfinder-bestiary-3-items/Te1JpsOS99z9IDyD.htm)|Cone of Chalk|auto-trad|
|[tetpWKxbpwLYXdRP.htm](pathfinder-bestiary-3-items/tetpWKxbpwLYXdRP.htm)|Improved Grab|auto-trad|
|[TFCVPbBolzO2jAdu.htm](pathfinder-bestiary-3-items/TFCVPbBolzO2jAdu.htm)|Knockdown|auto-trad|
|[TFFEZk6wOV2NQ6BA.htm](pathfinder-bestiary-3-items/TFFEZk6wOV2NQ6BA.htm)|Hoof|auto-trad|
|[TFIM3DwopeaNPL4N.htm](pathfinder-bestiary-3-items/TFIM3DwopeaNPL4N.htm)|Darkvision|auto-trad|
|[TgCafFeowTgKBPzr.htm](pathfinder-bestiary-3-items/TgCafFeowTgKBPzr.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[TgpF2GlBi4HH9xoC.htm](pathfinder-bestiary-3-items/TgpF2GlBi4HH9xoC.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[tHyoisfllt6q3L0n.htm](pathfinder-bestiary-3-items/tHyoisfllt6q3L0n.htm)|Fed by Water|auto-trad|
|[Ti3e0CkEDJSOuOJy.htm](pathfinder-bestiary-3-items/Ti3e0CkEDJSOuOJy.htm)|Ferocity|auto-trad|
|[ti9wuWwyYalyQsgW.htm](pathfinder-bestiary-3-items/ti9wuWwyYalyQsgW.htm)|Tongue|auto-trad|
|[Tiw7WqKClIOO7liK.htm](pathfinder-bestiary-3-items/Tiw7WqKClIOO7liK.htm)|Rearward Rush|auto-trad|
|[TjBokB0HyB1Xj2dM.htm](pathfinder-bestiary-3-items/TjBokB0HyB1Xj2dM.htm)|Puppetmaster|auto-trad|
|[tJCrBl4wNM7ey8KT.htm](pathfinder-bestiary-3-items/tJCrBl4wNM7ey8KT.htm)|Paralysis|auto-trad|
|[TjHnriTSE9y88Oaj.htm](pathfinder-bestiary-3-items/TjHnriTSE9y88Oaj.htm)|Purple Pox|auto-trad|
|[TJq1OPO5CFe7Ww6A.htm](pathfinder-bestiary-3-items/TJq1OPO5CFe7Ww6A.htm)|Fist|auto-trad|
|[tLkLt02YQsb0Ahfo.htm](pathfinder-bestiary-3-items/tLkLt02YQsb0Ahfo.htm)|Low-Light Vision|auto-trad|
|[TlwNO1rmOqaNw0W7.htm](pathfinder-bestiary-3-items/TlwNO1rmOqaNw0W7.htm)|Bond with Mortal|auto-trad|
|[TM9l7vqFbUf1UCLT.htm](pathfinder-bestiary-3-items/TM9l7vqFbUf1UCLT.htm)|Negative Healing|auto-trad|
|[TmaCh6JDsKX35pc2.htm](pathfinder-bestiary-3-items/TmaCh6JDsKX35pc2.htm)|Upper Jaw|auto-trad|
|[TnfM5nNHrq8AMX9L.htm](pathfinder-bestiary-3-items/TnfM5nNHrq8AMX9L.htm)|Tremorsense (Imprecise) within their entire bound granary or storeroom|auto-trad|
|[tNH3T8aLwLgqsb4C.htm](pathfinder-bestiary-3-items/tNH3T8aLwLgqsb4C.htm)|Scintillating Claw|auto-trad|
|[TnmeD4Xchfk7n9oy.htm](pathfinder-bestiary-3-items/TnmeD4Xchfk7n9oy.htm)|Veil of Lies|auto-trad|
|[tnQ4cIcOMWuGbaPY.htm](pathfinder-bestiary-3-items/tnQ4cIcOMWuGbaPY.htm)|Swift Staff Strike|auto-trad|
|[TnYQrIo30Kk8l4BV.htm](pathfinder-bestiary-3-items/TnYQrIo30Kk8l4BV.htm)|Temporal Sense|auto-trad|
|[To0rnGkFKHIi4i3T.htm](pathfinder-bestiary-3-items/To0rnGkFKHIi4i3T.htm)|Echolocation (Precise) 20 feet|auto-trad|
|[tODukcahVjECiTcK.htm](pathfinder-bestiary-3-items/tODukcahVjECiTcK.htm)|Ghostly Light Mace|auto-trad|
|[Tp53J4QE9RBvdlCd.htm](pathfinder-bestiary-3-items/Tp53J4QE9RBvdlCd.htm)|Primal Innate Spells|auto-trad|
|[Tp8OXXguuWM7iKK6.htm](pathfinder-bestiary-3-items/Tp8OXXguuWM7iKK6.htm)|Electrical Blast|auto-trad|
|[tpcpAmqxc53jhCQ7.htm](pathfinder-bestiary-3-items/tpcpAmqxc53jhCQ7.htm)|Talon|auto-trad|
|[tqdfXRytHABtgkJ4.htm](pathfinder-bestiary-3-items/tqdfXRytHABtgkJ4.htm)|Devour Tail|auto-trad|
|[TQhgfFteEWb6zzlT.htm](pathfinder-bestiary-3-items/TQhgfFteEWb6zzlT.htm)|Curl Up|auto-trad|
|[tqxAfXEcZbDZLdxn.htm](pathfinder-bestiary-3-items/tqxAfXEcZbDZLdxn.htm)|Darkvision|auto-trad|
|[tR481pq7xrt35Xrw.htm](pathfinder-bestiary-3-items/tR481pq7xrt35Xrw.htm)|Frightful Presence|auto-trad|
|[TRofD56vepZwhI9C.htm](pathfinder-bestiary-3-items/TRofD56vepZwhI9C.htm)|Draconic Momentum|auto-trad|
|[TROzn3iLJ2A6fcpj.htm](pathfinder-bestiary-3-items/TROzn3iLJ2A6fcpj.htm)|At-Will Spells|auto-trad|
|[TRPmSholsdyNcMiC.htm](pathfinder-bestiary-3-items/TRPmSholsdyNcMiC.htm)|Guardian Monolith|auto-trad|
|[tsZ8GnCQamaRzMs3.htm](pathfinder-bestiary-3-items/tsZ8GnCQamaRzMs3.htm)|Darkvision|auto-trad|
|[tT4vLmSN3IsZ2yur.htm](pathfinder-bestiary-3-items/tT4vLmSN3IsZ2yur.htm)|Four-Fanged Assault|auto-trad|
|[ttBTEeYxisLmK0oL.htm](pathfinder-bestiary-3-items/ttBTEeYxisLmK0oL.htm)|Claw|auto-trad|
|[TTmqj0RzBKjYpDRC.htm](pathfinder-bestiary-3-items/TTmqj0RzBKjYpDRC.htm)|Telepathy 300 feet|auto-trad|
|[TTotdT50Y6eFIsHY.htm](pathfinder-bestiary-3-items/TTotdT50Y6eFIsHY.htm)|Jaws|auto-trad|
|[tTRbrpeMS0tQXfb1.htm](pathfinder-bestiary-3-items/tTRbrpeMS0tQXfb1.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[tuqKOrUDPUmRlE6Q.htm](pathfinder-bestiary-3-items/tuqKOrUDPUmRlE6Q.htm)|Primal Purpose|auto-trad|
|[tvG7g3H6RsfVKvsw.htm](pathfinder-bestiary-3-items/tvG7g3H6RsfVKvsw.htm)|Draconic Momentum|auto-trad|
|[TwivoTQWKF6KVynb.htm](pathfinder-bestiary-3-items/TwivoTQWKF6KVynb.htm)|Ransack|auto-trad|
|[tWOZ4o9V5cviyUJD.htm](pathfinder-bestiary-3-items/tWOZ4o9V5cviyUJD.htm)|Occult Innate Spells|auto-trad|
|[Twp10eJAJOK8lgbO.htm](pathfinder-bestiary-3-items/Twp10eJAJOK8lgbO.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[tX7g78Vle2NcjulW.htm](pathfinder-bestiary-3-items/tX7g78Vle2NcjulW.htm)|Flying Strafe|auto-trad|
|[TxmC3aaWzECZldPm.htm](pathfinder-bestiary-3-items/TxmC3aaWzECZldPm.htm)|Bone Spike|auto-trad|
|[tXq7aF0UVeeMJx53.htm](pathfinder-bestiary-3-items/tXq7aF0UVeeMJx53.htm)|Grab|auto-trad|
|[txykeoHQFVm7Kix0.htm](pathfinder-bestiary-3-items/txykeoHQFVm7Kix0.htm)|At-Will Spells|auto-trad|
|[ty02a1CedQJfgYkL.htm](pathfinder-bestiary-3-items/ty02a1CedQJfgYkL.htm)|Wild Swing|auto-trad|
|[tZtHn0wl4msKd5S2.htm](pathfinder-bestiary-3-items/tZtHn0wl4msKd5S2.htm)|Telepathy 60 feet|auto-trad|
|[u0EOvM37SL9GUjJu.htm](pathfinder-bestiary-3-items/u0EOvM37SL9GUjJu.htm)|Telepathy 100 feet|auto-trad|
|[u191bxAWSFhNXMic.htm](pathfinder-bestiary-3-items/u191bxAWSFhNXMic.htm)|Attack of Opportunity|auto-trad|
|[U1W2qEPWeEon0DtN.htm](pathfinder-bestiary-3-items/U1W2qEPWeEon0DtN.htm)|Recall Reflection|auto-trad|
|[u3ndMdQRfiAH7Z7J.htm](pathfinder-bestiary-3-items/u3ndMdQRfiAH7Z7J.htm)|Phalanx Fighter|auto-trad|
|[u4b35ICyQPSv3OKF.htm](pathfinder-bestiary-3-items/u4b35ICyQPSv3OKF.htm)|Scimitar|auto-trad|
|[u4eux5FgWu5TDOdj.htm](pathfinder-bestiary-3-items/u4eux5FgWu5TDOdj.htm)|Positive Nature|auto-trad|
|[U4IAJfb86qQA800m.htm](pathfinder-bestiary-3-items/U4IAJfb86qQA800m.htm)|Pincer|auto-trad|
|[u4uyw8rTejQnjDhT.htm](pathfinder-bestiary-3-items/u4uyw8rTejQnjDhT.htm)|Tail|auto-trad|
|[u5cLd0qEPkNofLzr.htm](pathfinder-bestiary-3-items/u5cLd0qEPkNofLzr.htm)|Clutching Cobbles|auto-trad|
|[u5VmraPdTduQVeUa.htm](pathfinder-bestiary-3-items/u5VmraPdTduQVeUa.htm)|Breach Vulnerability|auto-trad|
|[U615W2wUWCOrTcC6.htm](pathfinder-bestiary-3-items/U615W2wUWCOrTcC6.htm)|Splinter Sycophant|auto-trad|
|[U74B3Cx6dSGCsSHP.htm](pathfinder-bestiary-3-items/U74B3Cx6dSGCsSHP.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[u8fKUkjNFuyL7a5x.htm](pathfinder-bestiary-3-items/u8fKUkjNFuyL7a5x.htm)|Darkvision|auto-trad|
|[uara7mD6sRfBQr3k.htm](pathfinder-bestiary-3-items/uara7mD6sRfBQr3k.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[UBAt1dB8VrLmKvtW.htm](pathfinder-bestiary-3-items/UBAt1dB8VrLmKvtW.htm)|Holy Weaponry|auto-trad|
|[uBnRvlWD0zCwJWoX.htm](pathfinder-bestiary-3-items/uBnRvlWD0zCwJWoX.htm)|Grab|auto-trad|
|[uC8vf4vEv2zFOWgo.htm](pathfinder-bestiary-3-items/uC8vf4vEv2zFOWgo.htm)|Jaws|auto-trad|
|[uCxQUNy75OANDWsm.htm](pathfinder-bestiary-3-items/uCxQUNy75OANDWsm.htm)|Adamantine Jaws|auto-trad|
|[uD92m7U1xCZSE1U8.htm](pathfinder-bestiary-3-items/uD92m7U1xCZSE1U8.htm)|At-Will Spells|auto-trad|
|[udfFYOsTo90aVSzS.htm](pathfinder-bestiary-3-items/udfFYOsTo90aVSzS.htm)|Throw Rock|auto-trad|
|[UDgM4FtV95bM8Jo2.htm](pathfinder-bestiary-3-items/UDgM4FtV95bM8Jo2.htm)|Sprint|auto-trad|
|[uDO2KFHi9xxyZa8l.htm](pathfinder-bestiary-3-items/uDO2KFHi9xxyZa8l.htm)|Trample|auto-trad|
|[uDYAOTcNICSCyK3S.htm](pathfinder-bestiary-3-items/uDYAOTcNICSCyK3S.htm)|Shadow Invisibility|auto-trad|
|[uEckCwzDaV3V8orK.htm](pathfinder-bestiary-3-items/uEckCwzDaV3V8orK.htm)|Light Blindness|auto-trad|
|[UeMKUFMnWNbhRbEj.htm](pathfinder-bestiary-3-items/UeMKUFMnWNbhRbEj.htm)|Primal Innate Spells|auto-trad|
|[UEtsgHyvVOtngUcs.htm](pathfinder-bestiary-3-items/UEtsgHyvVOtngUcs.htm)|Low-Light Vision|auto-trad|
|[uf75VmCg2lXJXIS6.htm](pathfinder-bestiary-3-items/uf75VmCg2lXJXIS6.htm)|Spore Pop|auto-trad|
|[UGDbrE4flLByNgPe.htm](pathfinder-bestiary-3-items/UGDbrE4flLByNgPe.htm)|Tusk|auto-trad|
|[UGRYo2y98N0QxtVh.htm](pathfinder-bestiary-3-items/UGRYo2y98N0QxtVh.htm)|Shadowbind|auto-trad|
|[UH1P5Ajkj0bfwuUa.htm](pathfinder-bestiary-3-items/UH1P5Ajkj0bfwuUa.htm)|+2 Status to All Saves vs. Curses|auto-trad|
|[UHVntHDP7laXevSA.htm](pathfinder-bestiary-3-items/UHVntHDP7laXevSA.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[UjrUewvwpknHeqGz.htm](pathfinder-bestiary-3-items/UjrUewvwpknHeqGz.htm)|Flaming Composite Longbow|auto-trad|
|[UJX7CIiJGskPMXPv.htm](pathfinder-bestiary-3-items/UJX7CIiJGskPMXPv.htm)|Consecration Vulnerability|auto-trad|
|[UK9gsjbHYgmKZze9.htm](pathfinder-bestiary-3-items/UK9gsjbHYgmKZze9.htm)|Self-Destruct|auto-trad|
|[UKAw8CcCGNk7VFkB.htm](pathfinder-bestiary-3-items/UKAw8CcCGNk7VFkB.htm)|At-Will Spells|auto-trad|
|[UkgUfv2gU8Cnz7Dk.htm](pathfinder-bestiary-3-items/UkgUfv2gU8Cnz7Dk.htm)|Central Weapon|auto-trad|
|[uLElA6gc5JAsMKBq.htm](pathfinder-bestiary-3-items/uLElA6gc5JAsMKBq.htm)|Swarm Mind|auto-trad|
|[ulnI58GokIE09FJW.htm](pathfinder-bestiary-3-items/ulnI58GokIE09FJW.htm)|Fist|auto-trad|
|[ULp1f7aziCabSbdr.htm](pathfinder-bestiary-3-items/ULp1f7aziCabSbdr.htm)|Claw|auto-trad|
|[uLwnIHpENAqfjCwz.htm](pathfinder-bestiary-3-items/uLwnIHpENAqfjCwz.htm)|Jaws|auto-trad|
|[uLZvd57CejMavFox.htm](pathfinder-bestiary-3-items/uLZvd57CejMavFox.htm)|Eurypterid Venom|auto-trad|
|[uM7SvgL7DwtCruSv.htm](pathfinder-bestiary-3-items/uM7SvgL7DwtCruSv.htm)|Leg|auto-trad|
|[Umi4PGsADEgpFaTb.htm](pathfinder-bestiary-3-items/Umi4PGsADEgpFaTb.htm)|Skip Between|auto-trad|
|[UmmKeGJIw44WAK7i.htm](pathfinder-bestiary-3-items/UmmKeGJIw44WAK7i.htm)|Fell Shriek|auto-trad|
|[uMNXrK8Is6PAzNEB.htm](pathfinder-bestiary-3-items/uMNXrK8Is6PAzNEB.htm)|Scuttle Away|auto-trad|
|[UmrTl2VeCsF3GsBm.htm](pathfinder-bestiary-3-items/UmrTl2VeCsF3GsBm.htm)|Plantsense 60 feet|auto-trad|
|[uN385UW65mQl9tQb.htm](pathfinder-bestiary-3-items/uN385UW65mQl9tQb.htm)|Occult Prepared Spells|auto-trad|
|[unEjKFEEILdk8x7m.htm](pathfinder-bestiary-3-items/unEjKFEEILdk8x7m.htm)|Grab|auto-trad|
|[Unq1mYTZ7Jk2AHCF.htm](pathfinder-bestiary-3-items/Unq1mYTZ7Jk2AHCF.htm)|Primal Prepared Spells|auto-trad|
|[UnTBAVC6J8CCjIdl.htm](pathfinder-bestiary-3-items/UnTBAVC6J8CCjIdl.htm)|Rotting Flesh|auto-trad|
|[UOOf6xLzb7FqQtsU.htm](pathfinder-bestiary-3-items/UOOf6xLzb7FqQtsU.htm)|Offal|auto-trad|
|[upknbVcHKdFGHUwR.htm](pathfinder-bestiary-3-items/upknbVcHKdFGHUwR.htm)|Glorious Fist|auto-trad|
|[UpN12Ls7Ii9P965x.htm](pathfinder-bestiary-3-items/UpN12Ls7Ii9P965x.htm)|Troop Defenses|auto-trad|
|[UPSTethShZCl8vHY.htm](pathfinder-bestiary-3-items/UPSTethShZCl8vHY.htm)|Fist|auto-trad|
|[Upzw2dn4HSqTYIma.htm](pathfinder-bestiary-3-items/Upzw2dn4HSqTYIma.htm)|Home Guardian|auto-trad|
|[UQ5bqrNhcoxqd2TA.htm](pathfinder-bestiary-3-items/UQ5bqrNhcoxqd2TA.htm)|Change Shape|auto-trad|
|[uQaUx0jJSihzX8ob.htm](pathfinder-bestiary-3-items/uQaUx0jJSihzX8ob.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[UQgqIDpkTWprpPzL.htm](pathfinder-bestiary-3-items/UQgqIDpkTWprpPzL.htm)|Low-Light Vision|auto-trad|
|[UQmOfNcYdQJoHq8o.htm](pathfinder-bestiary-3-items/UQmOfNcYdQJoHq8o.htm)|Avenging Bite|auto-trad|
|[uQmt7JbKVVMAsd5R.htm](pathfinder-bestiary-3-items/uQmt7JbKVVMAsd5R.htm)|Grab|auto-trad|
|[URIeoHiQX0CLnpUA.htm](pathfinder-bestiary-3-items/URIeoHiQX0CLnpUA.htm)|Jaws|auto-trad|
|[UrsYHXoFzbr0aW0A.htm](pathfinder-bestiary-3-items/UrsYHXoFzbr0aW0A.htm)|Greater Darkvision|auto-trad|
|[Urwp7vLcfy6avc98.htm](pathfinder-bestiary-3-items/Urwp7vLcfy6avc98.htm)|Grab|auto-trad|
|[uS98HfKqTgAefSsN.htm](pathfinder-bestiary-3-items/uS98HfKqTgAefSsN.htm)|Devour Life|auto-trad|
|[USVxY3qpSEozrQU8.htm](pathfinder-bestiary-3-items/USVxY3qpSEozrQU8.htm)|Fist|auto-trad|
|[uuZFA4waTTmjGuf7.htm](pathfinder-bestiary-3-items/uuZFA4waTTmjGuf7.htm)|Claw|auto-trad|
|[UuZpaLnXCCuS5ijA.htm](pathfinder-bestiary-3-items/UuZpaLnXCCuS5ijA.htm)|Concussive Blow|auto-trad|
|[uVHi6RIyqbXq9V9N.htm](pathfinder-bestiary-3-items/uVHi6RIyqbXq9V9N.htm)|Regurgitated Wrath|auto-trad|
|[Uw2yhPghf93MQk8d.htm](pathfinder-bestiary-3-items/Uw2yhPghf93MQk8d.htm)|Constrict|auto-trad|
|[UwDGN0t984W2aQuV.htm](pathfinder-bestiary-3-items/UwDGN0t984W2aQuV.htm)|Paired Strike|auto-trad|
|[UWTdQ7IdEAOO2D4Q.htm](pathfinder-bestiary-3-items/UWTdQ7IdEAOO2D4Q.htm)|Champion Focus Spells|auto-trad|
|[UXcwYHarnmB2msA1.htm](pathfinder-bestiary-3-items/UXcwYHarnmB2msA1.htm)|Lifesense 60 feet|auto-trad|
|[UXEJVTlMCdSK3nis.htm](pathfinder-bestiary-3-items/UXEJVTlMCdSK3nis.htm)|Occult Innate Spells|auto-trad|
|[UxfNIf8feaMNNjuy.htm](pathfinder-bestiary-3-items/UxfNIf8feaMNNjuy.htm)|Jaws|auto-trad|
|[uxGxWm5nbZ7Oxq7U.htm](pathfinder-bestiary-3-items/uxGxWm5nbZ7Oxq7U.htm)|Telepathy 200 feet|auto-trad|
|[UxlcIKPaetxhxrB9.htm](pathfinder-bestiary-3-items/UxlcIKPaetxhxrB9.htm)|Stench|auto-trad|
|[uybrNwUFDqMCKZ3R.htm](pathfinder-bestiary-3-items/uybrNwUFDqMCKZ3R.htm)|Tail|auto-trad|
|[UYKJzdzZCg9KRzcD.htm](pathfinder-bestiary-3-items/UYKJzdzZCg9KRzcD.htm)|Easy to Call|auto-trad|
|[uyLFQBi1NYtMlqCF.htm](pathfinder-bestiary-3-items/uyLFQBi1NYtMlqCF.htm)|Tremorsense 60 feet|auto-trad|
|[uYmFPiCSJytUnLkG.htm](pathfinder-bestiary-3-items/uYmFPiCSJytUnLkG.htm)|Fatal Fantasia|auto-trad|
|[UYwd2bv5bCYGaPAI.htm](pathfinder-bestiary-3-items/UYwd2bv5bCYGaPAI.htm)|Silver Strike|auto-trad|
|[uZG6teo4GWKlxaiD.htm](pathfinder-bestiary-3-items/uZG6teo4GWKlxaiD.htm)|Punishing Strike|auto-trad|
|[UZivbag22tDFAKdd.htm](pathfinder-bestiary-3-items/UZivbag22tDFAKdd.htm)|Telepathy 100 feet|auto-trad|
|[UzKSlqvopdc1rGlh.htm](pathfinder-bestiary-3-items/UzKSlqvopdc1rGlh.htm)|Spit|auto-trad|
|[uzLf2ezYP1JPXfrQ.htm](pathfinder-bestiary-3-items/uzLf2ezYP1JPXfrQ.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[v0UApfvf8TYOWcOQ.htm](pathfinder-bestiary-3-items/v0UApfvf8TYOWcOQ.htm)|Constant Spells|auto-trad|
|[V1IL4PehVXrysvbs.htm](pathfinder-bestiary-3-items/V1IL4PehVXrysvbs.htm)|Darkvision|auto-trad|
|[V2ghyimSN4jxOywM.htm](pathfinder-bestiary-3-items/V2ghyimSN4jxOywM.htm)|Talon|auto-trad|
|[V2NsuzlWxUrYA0mE.htm](pathfinder-bestiary-3-items/V2NsuzlWxUrYA0mE.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[v2RqXgBeNgkZ0DtA.htm](pathfinder-bestiary-3-items/v2RqXgBeNgkZ0DtA.htm)|Wind-Up|auto-trad|
|[V6QvQyNmHdNppYYJ.htm](pathfinder-bestiary-3-items/V6QvQyNmHdNppYYJ.htm)|Divine Innate Spells|auto-trad|
|[v79Z6h5Gc46hkIBk.htm](pathfinder-bestiary-3-items/v79Z6h5Gc46hkIBk.htm)|Form Up|auto-trad|
|[v88O0NAy45VWmtTO.htm](pathfinder-bestiary-3-items/v88O0NAy45VWmtTO.htm)|Curse of the Werebat|auto-trad|
|[v9Q0Mz13HzloiDTp.htm](pathfinder-bestiary-3-items/v9Q0Mz13HzloiDTp.htm)|Dagger|auto-trad|
|[VAggE1QOFwbQrV0m.htm](pathfinder-bestiary-3-items/VAggE1QOFwbQrV0m.htm)|Grab|auto-trad|
|[vagKbfsnprcn97gn.htm](pathfinder-bestiary-3-items/vagKbfsnprcn97gn.htm)|Flurry of Kicks|auto-trad|
|[VAj42GSNyP2at5VR.htm](pathfinder-bestiary-3-items/VAj42GSNyP2at5VR.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[vAYa9X9DeqTHfehJ.htm](pathfinder-bestiary-3-items/vAYa9X9DeqTHfehJ.htm)|Shrieking Scream|auto-trad|
|[VB7Ak1txQ1RPwHwJ.htm](pathfinder-bestiary-3-items/VB7Ak1txQ1RPwHwJ.htm)|Construct Armor (Hardness 3)|auto-trad|
|[VBA2WIH3wy9R2XsO.htm](pathfinder-bestiary-3-items/VBA2WIH3wy9R2XsO.htm)|Kukri|auto-trad|
|[vBiVfLo0mr2jIdhC.htm](pathfinder-bestiary-3-items/vBiVfLo0mr2jIdhC.htm)|Greater Darkvision|auto-trad|
|[vBOtCKqI5yNghuqQ.htm](pathfinder-bestiary-3-items/vBOtCKqI5yNghuqQ.htm)|Low-Light Vision|auto-trad|
|[vbSi698j0JxF2fvv.htm](pathfinder-bestiary-3-items/vbSi698j0JxF2fvv.htm)|Negative Healing|auto-trad|
|[VBxySuyNQGXdtmjL.htm](pathfinder-bestiary-3-items/VBxySuyNQGXdtmjL.htm)|Claw|auto-trad|
|[VbzlMz5d1BV2siTI.htm](pathfinder-bestiary-3-items/VbzlMz5d1BV2siTI.htm)|Knockdown|auto-trad|
|[vcbazPs6B9bMeBpm.htm](pathfinder-bestiary-3-items/vcbazPs6B9bMeBpm.htm)|Flurry of Blows|auto-trad|
|[vct7Th4oUyktL8WO.htm](pathfinder-bestiary-3-items/vct7Th4oUyktL8WO.htm)|Constant Spells|auto-trad|
|[vdcv4s3CzGpAj5KB.htm](pathfinder-bestiary-3-items/vdcv4s3CzGpAj5KB.htm)|Occult Innate Spells|auto-trad|
|[VdgqOmcdTkgggnVY.htm](pathfinder-bestiary-3-items/VdgqOmcdTkgggnVY.htm)|Dagger|auto-trad|
|[VdKAGQI4ZZFDhZeL.htm](pathfinder-bestiary-3-items/VdKAGQI4ZZFDhZeL.htm)|Claw|auto-trad|
|[VdozlfKy0DHfmOKJ.htm](pathfinder-bestiary-3-items/VdozlfKy0DHfmOKJ.htm)|Flail|auto-trad|
|[vdpzi8I0mzpB3lHw.htm](pathfinder-bestiary-3-items/vdpzi8I0mzpB3lHw.htm)|Constant Spells|auto-trad|
|[vDs8pDlAo0An3Z5L.htm](pathfinder-bestiary-3-items/vDs8pDlAo0An3Z5L.htm)|Negative Healing|auto-trad|
|[vDVTFesUl1YqnWqr.htm](pathfinder-bestiary-3-items/vDVTFesUl1YqnWqr.htm)|Claw|auto-trad|
|[VE0UqyozDof75tsl.htm](pathfinder-bestiary-3-items/VE0UqyozDof75tsl.htm)|Form Up|auto-trad|
|[Ve2k20bE8qSEdyvY.htm](pathfinder-bestiary-3-items/Ve2k20bE8qSEdyvY.htm)|Drain Life|auto-trad|
|[VE76RnTQLL1Y1ttf.htm](pathfinder-bestiary-3-items/VE76RnTQLL1Y1ttf.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[VECsjpvEAaMDK0IJ.htm](pathfinder-bestiary-3-items/VECsjpvEAaMDK0IJ.htm)|Flying Strafe|auto-trad|
|[veXylj85GdkPPrim.htm](pathfinder-bestiary-3-items/veXylj85GdkPPrim.htm)|Swallow Whole|auto-trad|
|[veYnaiiLoAVleTjO.htm](pathfinder-bestiary-3-items/veYnaiiLoAVleTjO.htm)|Darkvision|auto-trad|
|[vFdekjhomHHOwhBO.htm](pathfinder-bestiary-3-items/vFdekjhomHHOwhBO.htm)|Darkvision|auto-trad|
|[VFkAq790PgfMtp1j.htm](pathfinder-bestiary-3-items/VFkAq790PgfMtp1j.htm)|Low-Light Vision|auto-trad|
|[vFTbscR1hFTWc1f3.htm](pathfinder-bestiary-3-items/vFTbscR1hFTWc1f3.htm)|Primal Innate Spells|auto-trad|
|[vfThatLsAEQPDKBL.htm](pathfinder-bestiary-3-items/vfThatLsAEQPDKBL.htm)|Claw|auto-trad|
|[VgbKhZurZd89uLHJ.htm](pathfinder-bestiary-3-items/VgbKhZurZd89uLHJ.htm)|Frightening Rant|auto-trad|
|[Vge4HZcUTyLH8jKo.htm](pathfinder-bestiary-3-items/Vge4HZcUTyLH8jKo.htm)|Darkvision|auto-trad|
|[vGPorxL6swyKt73y.htm](pathfinder-bestiary-3-items/vGPorxL6swyKt73y.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[VgsFe1dtKkzBHRVz.htm](pathfinder-bestiary-3-items/VgsFe1dtKkzBHRVz.htm)|Darkvision|auto-trad|
|[vGTPPE8IbwvNQkbd.htm](pathfinder-bestiary-3-items/vGTPPE8IbwvNQkbd.htm)|Shock Mind|auto-trad|
|[VgzHbzcl0K2wE4qk.htm](pathfinder-bestiary-3-items/VgzHbzcl0K2wE4qk.htm)|Telepathy 60 feet|auto-trad|
|[Vh1hkSBb3FKa0QjG.htm](pathfinder-bestiary-3-items/Vh1hkSBb3FKa0QjG.htm)|Sumbreiva Huntblade|auto-trad|
|[VHBHFrYc6mpE6sPl.htm](pathfinder-bestiary-3-items/VHBHFrYc6mpE6sPl.htm)|Trawl for Bones|auto-trad|
|[vhL7f1KV4YaXn5FV.htm](pathfinder-bestiary-3-items/vhL7f1KV4YaXn5FV.htm)|Darkvision|auto-trad|
|[vhPEduQMUJTBlxzC.htm](pathfinder-bestiary-3-items/vhPEduQMUJTBlxzC.htm)|Shape Void|auto-trad|
|[VHt437pso612ShZb.htm](pathfinder-bestiary-3-items/VHt437pso612ShZb.htm)|Darkvision|auto-trad|
|[VHwtj3724SEQAAlz.htm](pathfinder-bestiary-3-items/VHwtj3724SEQAAlz.htm)|Constant Spells|auto-trad|
|[VHx2gNusdgS21WWh.htm](pathfinder-bestiary-3-items/VHx2gNusdgS21WWh.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[VhXGyXRgvJQTjUxr.htm](pathfinder-bestiary-3-items/VhXGyXRgvJQTjUxr.htm)|Claw|auto-trad|
|[vHyMiWUvIDKSzTO4.htm](pathfinder-bestiary-3-items/vHyMiWUvIDKSzTO4.htm)|Darkvision|auto-trad|
|[vHZ8RpuMQkQsDTHW.htm](pathfinder-bestiary-3-items/vHZ8RpuMQkQsDTHW.htm)|Light Blindness|auto-trad|
|[VI8C7X1hf2SOT0jS.htm](pathfinder-bestiary-3-items/VI8C7X1hf2SOT0jS.htm)|Violent Retort|auto-trad|
|[vICaR1ONcB3ugGfH.htm](pathfinder-bestiary-3-items/vICaR1ONcB3ugGfH.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[VIDJVH2nBIkzXg2L.htm](pathfinder-bestiary-3-items/VIDJVH2nBIkzXg2L.htm)|Innate Divine Spells|auto-trad|
|[vihivNwS1MIyMHBB.htm](pathfinder-bestiary-3-items/vihivNwS1MIyMHBB.htm)|Rootbound|auto-trad|
|[vIMdth3wX5RnQu2i.htm](pathfinder-bestiary-3-items/vIMdth3wX5RnQu2i.htm)|Sneak Attack|auto-trad|
|[viOxzbPu5sPdBqSc.htm](pathfinder-bestiary-3-items/viOxzbPu5sPdBqSc.htm)|Constrict|auto-trad|
|[vJ4WPwSeQegH8KMs.htm](pathfinder-bestiary-3-items/vJ4WPwSeQegH8KMs.htm)|Occult Innate Spells|auto-trad|
|[VjdE8difbmDAhk1V.htm](pathfinder-bestiary-3-items/VjdE8difbmDAhk1V.htm)|Tail|auto-trad|
|[VjJS4dQI0QyHntez.htm](pathfinder-bestiary-3-items/VjJS4dQI0QyHntez.htm)|Soul Feast|auto-trad|
|[vjjTR5hrPf5pDmGk.htm](pathfinder-bestiary-3-items/vjjTR5hrPf5pDmGk.htm)|Change Shape|auto-trad|
|[VjpfyKmnhYR8Bus6.htm](pathfinder-bestiary-3-items/VjpfyKmnhYR8Bus6.htm)|Regeneration 5 (Deactivated by Acid or Fire)|auto-trad|
|[Vk1vUceA5furX6Ez.htm](pathfinder-bestiary-3-items/Vk1vUceA5furX6Ez.htm)|Wide Cleave|auto-trad|
|[vkAopqi1PCG6CvnD.htm](pathfinder-bestiary-3-items/vkAopqi1PCG6CvnD.htm)|Flame Jump|auto-trad|
|[vkQCMGp1H43fc8Jo.htm](pathfinder-bestiary-3-items/vkQCMGp1H43fc8Jo.htm)|Manipulate Luck|auto-trad|
|[vKSl7FDTI6tQZCio.htm](pathfinder-bestiary-3-items/vKSl7FDTI6tQZCio.htm)|Negative Healing|auto-trad|
|[vlGXeDzuvihHbasj.htm](pathfinder-bestiary-3-items/vlGXeDzuvihHbasj.htm)|Claw|auto-trad|
|[vlieQSRegKDFCdLu.htm](pathfinder-bestiary-3-items/vlieQSRegKDFCdLu.htm)|Occult Spontaneous Spells|auto-trad|
|[vlJE8WHtB9SH2Ca0.htm](pathfinder-bestiary-3-items/vlJE8WHtB9SH2Ca0.htm)|Telepathy 60 feet|auto-trad|
|[vLM5mXAWGq7oMjMv.htm](pathfinder-bestiary-3-items/vLM5mXAWGq7oMjMv.htm)|Grab|auto-trad|
|[VMapQLoJPfFmzdaS.htm](pathfinder-bestiary-3-items/VMapQLoJPfFmzdaS.htm)|Susceptible to Death|auto-trad|
|[VMiBWybV4muz0sCm.htm](pathfinder-bestiary-3-items/VMiBWybV4muz0sCm.htm)|Contingent Glyph|auto-trad|
|[VmpcHMhX036wRdpf.htm](pathfinder-bestiary-3-items/VmpcHMhX036wRdpf.htm)|Thorns|auto-trad|
|[vMRN1ON8B5Jk9K9h.htm](pathfinder-bestiary-3-items/vMRN1ON8B5Jk9K9h.htm)|Easy to Call|auto-trad|
|[vMTJr0Wv9CyKUmqi.htm](pathfinder-bestiary-3-items/vMTJr0Wv9CyKUmqi.htm)|Composite Shortbow|auto-trad|
|[vNKRTp7YasyHRjqH.htm](pathfinder-bestiary-3-items/vNKRTp7YasyHRjqH.htm)|Claw|auto-trad|
|[VnUdpwyEF14owCWc.htm](pathfinder-bestiary-3-items/VnUdpwyEF14owCWc.htm)|Grab|auto-trad|
|[vOKfAcHQJQuAlDmX.htm](pathfinder-bestiary-3-items/vOKfAcHQJQuAlDmX.htm)|Low-Light Vision|auto-trad|
|[vPFPkp810JifvYk3.htm](pathfinder-bestiary-3-items/vPFPkp810JifvYk3.htm)|Pack Attack|auto-trad|
|[vPiD83jHe3pfnCGb.htm](pathfinder-bestiary-3-items/vPiD83jHe3pfnCGb.htm)|Vine|auto-trad|
|[vpKKf7MXtmLs5zCF.htm](pathfinder-bestiary-3-items/vpKKf7MXtmLs5zCF.htm)|Coven|auto-trad|
|[vpPweRPkFLYzku6Q.htm](pathfinder-bestiary-3-items/vpPweRPkFLYzku6Q.htm)|Resonance|auto-trad|
|[VPTJLGDVtuMcWdkQ.htm](pathfinder-bestiary-3-items/VPTJLGDVtuMcWdkQ.htm)|Constrict|auto-trad|
|[VPwJArBSwEFYQLmI.htm](pathfinder-bestiary-3-items/VPwJArBSwEFYQLmI.htm)|Viscous Trap|auto-trad|
|[VR2zhinm3bRfVONP.htm](pathfinder-bestiary-3-items/VR2zhinm3bRfVONP.htm)|Breath Weapon|auto-trad|
|[vRBgSJWn6sIDkLoJ.htm](pathfinder-bestiary-3-items/vRBgSJWn6sIDkLoJ.htm)|Boneshard Burst|auto-trad|
|[VRhuQIGcrHKCOyL4.htm](pathfinder-bestiary-3-items/VRhuQIGcrHKCOyL4.htm)|Flaming Weapon|auto-trad|
|[vRX7fHLnwfVnMQMN.htm](pathfinder-bestiary-3-items/vRX7fHLnwfVnMQMN.htm)|Trackless Step|auto-trad|
|[vrYCafAUKL8c1AqG.htm](pathfinder-bestiary-3-items/vrYCafAUKL8c1AqG.htm)|Greater Darkvision|auto-trad|
|[VS3BUS7yb5FWxaZH.htm](pathfinder-bestiary-3-items/VS3BUS7yb5FWxaZH.htm)|Appetizing Aroma|auto-trad|
|[VSblAk10Who8I3wv.htm](pathfinder-bestiary-3-items/VSblAk10Who8I3wv.htm)|Darkvision|auto-trad|
|[VSLTIQMCHCfLxq1H.htm](pathfinder-bestiary-3-items/VSLTIQMCHCfLxq1H.htm)|Divine Innate Spells|auto-trad|
|[Vsv36iP3BjY1rHBr.htm](pathfinder-bestiary-3-items/Vsv36iP3BjY1rHBr.htm)|Claw|auto-trad|
|[VT3MCbqQoHOeEO6f.htm](pathfinder-bestiary-3-items/VT3MCbqQoHOeEO6f.htm)|Shadowed Blade|auto-trad|
|[VtDOTpa01IbIaeYP.htm](pathfinder-bestiary-3-items/VtDOTpa01IbIaeYP.htm)|Pack Attack|auto-trad|
|[vTV8lwwNuVs4eL7F.htm](pathfinder-bestiary-3-items/vTV8lwwNuVs4eL7F.htm)|Focus Vines|auto-trad|
|[Vu7yHDGMbHRCK2KR.htm](pathfinder-bestiary-3-items/Vu7yHDGMbHRCK2KR.htm)|All-Around Vision|auto-trad|
|[vumGr1e1Y2sLOMlM.htm](pathfinder-bestiary-3-items/vumGr1e1Y2sLOMlM.htm)|Negative Healing|auto-trad|
|[Vvhg9eA8gPTjQD32.htm](pathfinder-bestiary-3-items/Vvhg9eA8gPTjQD32.htm)|Grab|auto-trad|
|[VvNRwoYk3JBUgyor.htm](pathfinder-bestiary-3-items/VvNRwoYk3JBUgyor.htm)|Infest Corpse|auto-trad|
|[VvTUZ7JyzVpWPNRj.htm](pathfinder-bestiary-3-items/VvTUZ7JyzVpWPNRj.htm)|Sunset Ray|auto-trad|
|[VW8DMx7itpBNELJa.htm](pathfinder-bestiary-3-items/VW8DMx7itpBNELJa.htm)|Tail|auto-trad|
|[Vw9udtwUZC4fiY9x.htm](pathfinder-bestiary-3-items/Vw9udtwUZC4fiY9x.htm)|Trample|auto-trad|
|[vx5wkGlnDaE4xpPz.htm](pathfinder-bestiary-3-items/vx5wkGlnDaE4xpPz.htm)|Tail|auto-trad|
|[Vx9iqVf4ismUN743.htm](pathfinder-bestiary-3-items/Vx9iqVf4ismUN743.htm)|Strike as One|auto-trad|
|[vxPqbqu0444ekUR7.htm](pathfinder-bestiary-3-items/vxPqbqu0444ekUR7.htm)|Ghostly Longsword|auto-trad|
|[VXQuJ8bq1mVA48Xh.htm](pathfinder-bestiary-3-items/VXQuJ8bq1mVA48Xh.htm)|Heretic's Smite|auto-trad|
|[VxrL8PMJq1Vfet9z.htm](pathfinder-bestiary-3-items/VxrL8PMJq1Vfet9z.htm)|Countered by Earth|auto-trad|
|[vxu3Y6OKaA1HJSb6.htm](pathfinder-bestiary-3-items/vxu3Y6OKaA1HJSb6.htm)|At-Will Spells|auto-trad|
|[VY2kHKsJk6ZDMTkn.htm](pathfinder-bestiary-3-items/VY2kHKsJk6ZDMTkn.htm)|Demolish Veil|auto-trad|
|[vYEI2Ae6r4vFEWMb.htm](pathfinder-bestiary-3-items/vYEI2Ae6r4vFEWMb.htm)|Negative Healing|auto-trad|
|[vyrmCD1p0HDQDVzt.htm](pathfinder-bestiary-3-items/vyrmCD1p0HDQDVzt.htm)|Wailing Dive|auto-trad|
|[vYyLO2LUYvXaIzFj.htm](pathfinder-bestiary-3-items/vYyLO2LUYvXaIzFj.htm)|Warding Glyph|auto-trad|
|[VzlO3rF9ml7NQJRF.htm](pathfinder-bestiary-3-items/VzlO3rF9ml7NQJRF.htm)|Fleeting Blossoms|auto-trad|
|[VZlTraWqYIrTlPGW.htm](pathfinder-bestiary-3-items/VZlTraWqYIrTlPGW.htm)|Tormenting Dreams|auto-trad|
|[VZVdci43sd9eSYaO.htm](pathfinder-bestiary-3-items/VZVdci43sd9eSYaO.htm)|Absorb Spell|auto-trad|
|[VzzWLmieSojZVoLh.htm](pathfinder-bestiary-3-items/VzzWLmieSojZVoLh.htm)|Wrap in Coils|auto-trad|
|[W3i2bV0xohyukCqg.htm](pathfinder-bestiary-3-items/W3i2bV0xohyukCqg.htm)|Foxfire|auto-trad|
|[w43ksSNtCpLwtJqW.htm](pathfinder-bestiary-3-items/w43ksSNtCpLwtJqW.htm)|Ice Shard|auto-trad|
|[w4lnLML460ytZr1D.htm](pathfinder-bestiary-3-items/w4lnLML460ytZr1D.htm)|Form Up|auto-trad|
|[w6hxoqW1tHgrDy7z.htm](pathfinder-bestiary-3-items/w6hxoqW1tHgrDy7z.htm)|Constant Spells|auto-trad|
|[w7zrM0Z9BtHG4iCl.htm](pathfinder-bestiary-3-items/w7zrM0Z9BtHG4iCl.htm)|Darkvision|auto-trad|
|[W87SHAgheuR4yFTV.htm](pathfinder-bestiary-3-items/W87SHAgheuR4yFTV.htm)|Darkvision|auto-trad|
|[WaigNlDjE6reTIhN.htm](pathfinder-bestiary-3-items/WaigNlDjE6reTIhN.htm)|Mist Vision|auto-trad|
|[WC0xg40Qwic6NwdR.htm](pathfinder-bestiary-3-items/WC0xg40Qwic6NwdR.htm)|Radiant Ray|auto-trad|
|[Wc4yL4hHzoo1254s.htm](pathfinder-bestiary-3-items/Wc4yL4hHzoo1254s.htm)|Pained Muttering|auto-trad|
|[wCK0LYZXOOURpX1D.htm](pathfinder-bestiary-3-items/wCK0LYZXOOURpX1D.htm)|Constant Spells|auto-trad|
|[WCzpBUZV9MrBfZQq.htm](pathfinder-bestiary-3-items/WCzpBUZV9MrBfZQq.htm)|+4 Status to All Saves vs. Mental or Divine|auto-trad|
|[WD4ykPiVOX7xRtgI.htm](pathfinder-bestiary-3-items/WD4ykPiVOX7xRtgI.htm)|Perfected Flight|auto-trad|
|[wDaKwffZUuh51WSU.htm](pathfinder-bestiary-3-items/wDaKwffZUuh51WSU.htm)|Emit Musk|auto-trad|
|[WDDv1gdGqwkBHqVr.htm](pathfinder-bestiary-3-items/WDDv1gdGqwkBHqVr.htm)|Wavesense (Imprecise) 60 feet|auto-trad|
|[wdoiMwQp71XmTDBR.htm](pathfinder-bestiary-3-items/wdoiMwQp71XmTDBR.htm)|Spikes|auto-trad|
|[wdp8tW3xpAHzFNVP.htm](pathfinder-bestiary-3-items/wdp8tW3xpAHzFNVP.htm)|Primal Innate Spells|auto-trad|
|[wDXudHrGRboTsL0B.htm](pathfinder-bestiary-3-items/wDXudHrGRboTsL0B.htm)|Grab and Go|auto-trad|
|[WdzKzT9cZ51TPqV6.htm](pathfinder-bestiary-3-items/WdzKzT9cZ51TPqV6.htm)|Jotun Slayer|auto-trad|
|[WE0EIWm5dsgBMGU3.htm](pathfinder-bestiary-3-items/WE0EIWm5dsgBMGU3.htm)|Throw Rock|auto-trad|
|[wEJxEzOsrNPsy1G2.htm](pathfinder-bestiary-3-items/wEJxEzOsrNPsy1G2.htm)|Absorb Memories|auto-trad|
|[wETifc6MCAQ7g53r.htm](pathfinder-bestiary-3-items/wETifc6MCAQ7g53r.htm)|Low-Light Vision|auto-trad|
|[WEZI1PxyY7ic04Cy.htm](pathfinder-bestiary-3-items/WEZI1PxyY7ic04Cy.htm)|Grab|auto-trad|
|[Wf76V3o8gstoUE6k.htm](pathfinder-bestiary-3-items/Wf76V3o8gstoUE6k.htm)|Divine Innate Spells|auto-trad|
|[wFnxNcpjZQq97vyn.htm](pathfinder-bestiary-3-items/wFnxNcpjZQq97vyn.htm)|Mountain Stride|auto-trad|
|[WFRxpdnf4FFb3z7i.htm](pathfinder-bestiary-3-items/WFRxpdnf4FFb3z7i.htm)|Telepathy 30 feet|auto-trad|
|[WFS8UUOKxm78jwiH.htm](pathfinder-bestiary-3-items/WFS8UUOKxm78jwiH.htm)|Grab|auto-trad|
|[WgefMvFrdC4a4wjm.htm](pathfinder-bestiary-3-items/WgefMvFrdC4a4wjm.htm)|At-Will Spells|auto-trad|
|[WgimcieBkqdGPzKv.htm](pathfinder-bestiary-3-items/WgimcieBkqdGPzKv.htm)|Change Shape|auto-trad|
|[wGOw52Kk1d6zz1Wr.htm](pathfinder-bestiary-3-items/wGOw52Kk1d6zz1Wr.htm)|Avenging Claws|auto-trad|
|[whcEPoZw0dVdy1U1.htm](pathfinder-bestiary-3-items/whcEPoZw0dVdy1U1.htm)|Telepathy 100 feet (Myceloids and Those Afflicted by Purple Pox Only)|auto-trad|
|[wHFnTcSpBzMvrkSD.htm](pathfinder-bestiary-3-items/wHFnTcSpBzMvrkSD.htm)|Darkvision|auto-trad|
|[WIkVLqYDYSPO2pUz.htm](pathfinder-bestiary-3-items/WIkVLqYDYSPO2pUz.htm)|Telepathy 100 feet|auto-trad|
|[WIxfvddFY1utvHlp.htm](pathfinder-bestiary-3-items/WIxfvddFY1utvHlp.htm)|Air of Sickness|auto-trad|
|[WIXzhLmn2W2ARVKv.htm](pathfinder-bestiary-3-items/WIXzhLmn2W2ARVKv.htm)|Divine Spontaneous Spells|auto-trad|
|[wJ1vHRd41BTDplWu.htm](pathfinder-bestiary-3-items/wJ1vHRd41BTDplWu.htm)|Longspear|auto-trad|
|[wjAhC8gFPZh72a5L.htm](pathfinder-bestiary-3-items/wjAhC8gFPZh72a5L.htm)|Darkvision|auto-trad|
|[wJFd0snSm32sHDWx.htm](pathfinder-bestiary-3-items/wJFd0snSm32sHDWx.htm)|Punish the Naughty|auto-trad|
|[WJSuFXX25ngL6SXe.htm](pathfinder-bestiary-3-items/WJSuFXX25ngL6SXe.htm)|Speak With Snakes|auto-trad|
|[WjVRDLqgP2pwz4W9.htm](pathfinder-bestiary-3-items/WjVRDLqgP2pwz4W9.htm)|Wavesense (Imprecise) 30 feet|auto-trad|
|[wjyglR2qfZUfzQhE.htm](pathfinder-bestiary-3-items/wjyglR2qfZUfzQhE.htm)|Glide|auto-trad|
|[wK3dnqQbpz2HUZw5.htm](pathfinder-bestiary-3-items/wK3dnqQbpz2HUZw5.htm)|Telepathy 100 feet|auto-trad|
|[wKM9T9u0owr9Pynk.htm](pathfinder-bestiary-3-items/wKM9T9u0owr9Pynk.htm)|Divine Spontaneous Spells|auto-trad|
|[WKOA5jV7935qIaIo.htm](pathfinder-bestiary-3-items/WKOA5jV7935qIaIo.htm)|Wavesense (Imprecise) 30 feet|auto-trad|
|[wkqPL5ekfW5yOlp9.htm](pathfinder-bestiary-3-items/wkqPL5ekfW5yOlp9.htm)|Divine Innate Spells|auto-trad|
|[WKzCKcnmUlEawWY8.htm](pathfinder-bestiary-3-items/WKzCKcnmUlEawWY8.htm)|Fangs|auto-trad|
|[WLELPQnngNQdOSNv.htm](pathfinder-bestiary-3-items/WLELPQnngNQdOSNv.htm)|Contorted Clutch|auto-trad|
|[WLzqAFoShoUcDjON.htm](pathfinder-bestiary-3-items/WLzqAFoShoUcDjON.htm)|Death Gasp|auto-trad|
|[wMD2GLX6aqo2KE1s.htm](pathfinder-bestiary-3-items/wMD2GLX6aqo2KE1s.htm)|+1 Circumstance to All Saves vs. Disease, Poison, and Radiation|auto-trad|
|[WMgMO9G3wUtuSBs8.htm](pathfinder-bestiary-3-items/WMgMO9G3wUtuSBs8.htm)|Fangs|auto-trad|
|[WMiSz943Mp34MPxS.htm](pathfinder-bestiary-3-items/WMiSz943Mp34MPxS.htm)|Countered by Water|auto-trad|
|[wncZzBRw5mBr1I3b.htm](pathfinder-bestiary-3-items/wncZzBRw5mBr1I3b.htm)|Claw|auto-trad|
|[wndNBL1gS8EX4LkQ.htm](pathfinder-bestiary-3-items/wndNBL1gS8EX4LkQ.htm)|Coiling Frenzy|auto-trad|
|[WnLAb54M532yXeAK.htm](pathfinder-bestiary-3-items/WnLAb54M532yXeAK.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Wo73VHAjF7M37ttK.htm](pathfinder-bestiary-3-items/Wo73VHAjF7M37ttK.htm)|Dagger|auto-trad|
|[WOLOhrv8TOFesFur.htm](pathfinder-bestiary-3-items/WOLOhrv8TOFesFur.htm)|Greater Darkvision|auto-trad|
|[wP25CSpHH1XfhQZV.htm](pathfinder-bestiary-3-items/wP25CSpHH1XfhQZV.htm)|Wing|auto-trad|
|[wPEh1COqJAlq37ce.htm](pathfinder-bestiary-3-items/wPEh1COqJAlq37ce.htm)|Shock Composite Longbow|auto-trad|
|[WpHT325mhd62bmHx.htm](pathfinder-bestiary-3-items/WpHT325mhd62bmHx.htm)|Low-Light Vision|auto-trad|
|[wPsE3OnHZcFptWrh.htm](pathfinder-bestiary-3-items/wPsE3OnHZcFptWrh.htm)|Claimer of the Slain|auto-trad|
|[wq97ytCKPYfmWhuM.htm](pathfinder-bestiary-3-items/wq97ytCKPYfmWhuM.htm)|Hand Crossbow|auto-trad|
|[wqfNw7k5yrmfOl9I.htm](pathfinder-bestiary-3-items/wqfNw7k5yrmfOl9I.htm)|Cat's Curiosity|auto-trad|
|[wQOPxpduTlBDFOg3.htm](pathfinder-bestiary-3-items/wQOPxpduTlBDFOg3.htm)|Divine Innate Spells|auto-trad|
|[WrlWLg3CTbIvRroV.htm](pathfinder-bestiary-3-items/WrlWLg3CTbIvRroV.htm)|Mace|auto-trad|
|[wRO8DImltQqpTp5v.htm](pathfinder-bestiary-3-items/wRO8DImltQqpTp5v.htm)|Felt Shears|auto-trad|
|[wRvyhBs5zmAJtpe8.htm](pathfinder-bestiary-3-items/wRvyhBs5zmAJtpe8.htm)|Temporal Sense|auto-trad|
|[WsuMP3feuXNP1eeE.htm](pathfinder-bestiary-3-items/WsuMP3feuXNP1eeE.htm)|Sling|auto-trad|
|[WtYldqbo5WHxuBHU.htm](pathfinder-bestiary-3-items/WtYldqbo5WHxuBHU.htm)|Swarm Mind|auto-trad|
|[wuJYkji7dBLYrLAK.htm](pathfinder-bestiary-3-items/wuJYkji7dBLYrLAK.htm)|Constant Spells|auto-trad|
|[WUkTk0F7LErjOXbO.htm](pathfinder-bestiary-3-items/WUkTk0F7LErjOXbO.htm)|Site Bound|auto-trad|
|[WUlRpT25Akwrbx9n.htm](pathfinder-bestiary-3-items/WUlRpT25Akwrbx9n.htm)|Hatchet|auto-trad|
|[WUrrk4eny3GFngud.htm](pathfinder-bestiary-3-items/WUrrk4eny3GFngud.htm)|Burning Cold|auto-trad|
|[wuWal4gyvorVZotg.htm](pathfinder-bestiary-3-items/wuWal4gyvorVZotg.htm)|Deflecting Lie|auto-trad|
|[WvLlxDJajjcl40yg.htm](pathfinder-bestiary-3-items/WvLlxDJajjcl40yg.htm)|Pull Arm|auto-trad|
|[wVvVI3Jfnk8L5F3L.htm](pathfinder-bestiary-3-items/wVvVI3Jfnk8L5F3L.htm)|Pustulant Flail|auto-trad|
|[wVY7ijG6fY0kdgBv.htm](pathfinder-bestiary-3-items/wVY7ijG6fY0kdgBv.htm)|Spear|auto-trad|
|[wWvqRgRb0u1JwCr6.htm](pathfinder-bestiary-3-items/wWvqRgRb0u1JwCr6.htm)|Troop Movement|auto-trad|
|[wY1dx0xYE5qgM9aA.htm](pathfinder-bestiary-3-items/wY1dx0xYE5qgM9aA.htm)|Sonic Pulse|auto-trad|
|[WY6HZHUNFdCpdD8m.htm](pathfinder-bestiary-3-items/WY6HZHUNFdCpdD8m.htm)|Divine Innate Spells|auto-trad|
|[wy6isKCHF1syJmZf.htm](pathfinder-bestiary-3-items/wy6isKCHF1syJmZf.htm)|Fist|auto-trad|
|[wycfQOBOlmzkFJ7X.htm](pathfinder-bestiary-3-items/wycfQOBOlmzkFJ7X.htm)|Slough Toxins|auto-trad|
|[WypFVNf6qa1B0aVW.htm](pathfinder-bestiary-3-items/WypFVNf6qa1B0aVW.htm)|Voice of the Storm|auto-trad|
|[WYWDDEYazjVp60qA.htm](pathfinder-bestiary-3-items/WYWDDEYazjVp60qA.htm)|Rain of Debris|auto-trad|
|[WZ8Y5R6xt0YzzTUf.htm](pathfinder-bestiary-3-items/WZ8Y5R6xt0YzzTUf.htm)|Darkvision|auto-trad|
|[wzhDzF6I1V67YTEq.htm](pathfinder-bestiary-3-items/wzhDzF6I1V67YTEq.htm)|Low-Light Vision|auto-trad|
|[X0s0MfJlPDdh23yP.htm](pathfinder-bestiary-3-items/X0s0MfJlPDdh23yP.htm)|Peaceful Aura|auto-trad|
|[X0u8yRaoumH1Qm27.htm](pathfinder-bestiary-3-items/X0u8yRaoumH1Qm27.htm)|Lithe|auto-trad|
|[X2Wh8jC2XB0vLQWK.htm](pathfinder-bestiary-3-items/X2Wh8jC2XB0vLQWK.htm)|At-Will Spells|auto-trad|
|[x3F1PlXQKZuCBCGg.htm](pathfinder-bestiary-3-items/x3F1PlXQKZuCBCGg.htm)|Greater Darkvision|auto-trad|
|[x40IMqy9XVt1sVgV.htm](pathfinder-bestiary-3-items/x40IMqy9XVt1sVgV.htm)|Darkvision|auto-trad|
|[X47wX3wcEYVktTy9.htm](pathfinder-bestiary-3-items/X47wX3wcEYVktTy9.htm)|Bound|auto-trad|
|[X4dbGwvffyOz8AAl.htm](pathfinder-bestiary-3-items/X4dbGwvffyOz8AAl.htm)|Fist|auto-trad|
|[X4oCGHtZLhDPu84J.htm](pathfinder-bestiary-3-items/X4oCGHtZLhDPu84J.htm)|Shortbow|auto-trad|
|[X4QNdoSZgj2ri5we.htm](pathfinder-bestiary-3-items/X4QNdoSZgj2ri5we.htm)|Wavesense (Imprecise) 30 feet|auto-trad|
|[X5bjxwBhWpEDkFUA.htm](pathfinder-bestiary-3-items/X5bjxwBhWpEDkFUA.htm)|Forge Weapon|auto-trad|
|[x5dNPm9EV0pS1WDJ.htm](pathfinder-bestiary-3-items/x5dNPm9EV0pS1WDJ.htm)|Spiked Chain|auto-trad|
|[X5iRzpgvcVsGqbsr.htm](pathfinder-bestiary-3-items/X5iRzpgvcVsGqbsr.htm)|Thorn|auto-trad|
|[x5ke7WXv7p23V2Mc.htm](pathfinder-bestiary-3-items/x5ke7WXv7p23V2Mc.htm)|Low-Light Vision|auto-trad|
|[x68dPIsgXM3s4jCZ.htm](pathfinder-bestiary-3-items/x68dPIsgXM3s4jCZ.htm)|Claw|auto-trad|
|[X6SY2vedf1w4NFyM.htm](pathfinder-bestiary-3-items/X6SY2vedf1w4NFyM.htm)|Fangs|auto-trad|
|[x7CJTFxW4u5WZvs5.htm](pathfinder-bestiary-3-items/x7CJTFxW4u5WZvs5.htm)|Fade Away|auto-trad|
|[X7keRhNv8rJSPJUM.htm](pathfinder-bestiary-3-items/X7keRhNv8rJSPJUM.htm)|Composite Longbow|auto-trad|
|[X7My4JpekzJFoAlA.htm](pathfinder-bestiary-3-items/X7My4JpekzJFoAlA.htm)|Jaws|auto-trad|
|[X7tUpkvJ2IFENOEb.htm](pathfinder-bestiary-3-items/X7tUpkvJ2IFENOEb.htm)|Low-Light Vision|auto-trad|
|[X8q0VOcCAdQbazmw.htm](pathfinder-bestiary-3-items/X8q0VOcCAdQbazmw.htm)|Urban Legend|auto-trad|
|[x8x4bMdmpqcgY6vi.htm](pathfinder-bestiary-3-items/x8x4bMdmpqcgY6vi.htm)|Darkvision|auto-trad|
|[XAD9XYroOQ2xoyQF.htm](pathfinder-bestiary-3-items/XAD9XYroOQ2xoyQF.htm)|+4 Status to All Saves vs. Mental|auto-trad|
|[xb36ZcB5nu2McDrU.htm](pathfinder-bestiary-3-items/xb36ZcB5nu2McDrU.htm)|Grave Tide|auto-trad|
|[XbI8HI1jqqylY3ZB.htm](pathfinder-bestiary-3-items/XbI8HI1jqqylY3ZB.htm)|Fly Free|auto-trad|
|[xBKXa275uqX5FmD9.htm](pathfinder-bestiary-3-items/xBKXa275uqX5FmD9.htm)|Beak|auto-trad|
|[XBz2toYQ5LlpBAkp.htm](pathfinder-bestiary-3-items/XBz2toYQ5LlpBAkp.htm)|Transpose|auto-trad|
|[xc23c2YKrqqX2Dqy.htm](pathfinder-bestiary-3-items/xc23c2YKrqqX2Dqy.htm)|At-Will Spells|auto-trad|
|[XCtNEYFEjjOTsfCx.htm](pathfinder-bestiary-3-items/XCtNEYFEjjOTsfCx.htm)|Divine Prepared Spells|auto-trad|
|[XdiuvRsoT9Lh40Vm.htm](pathfinder-bestiary-3-items/XdiuvRsoT9Lh40Vm.htm)|Spike|auto-trad|
|[xDnUeGZfUafSNkYD.htm](pathfinder-bestiary-3-items/xDnUeGZfUafSNkYD.htm)|Darkvision|auto-trad|
|[XdPq6vZ9PifML9Fy.htm](pathfinder-bestiary-3-items/XdPq6vZ9PifML9Fy.htm)|Primal Innate Spells|auto-trad|
|[xDxTLZhpgmfspHHB.htm](pathfinder-bestiary-3-items/xDxTLZhpgmfspHHB.htm)|Primal Innate Spells|auto-trad|
|[xEaJ3PQ5662GbdCV.htm](pathfinder-bestiary-3-items/xEaJ3PQ5662GbdCV.htm)|Jaws|auto-trad|
|[xeAn1C6HdB8ksSEc.htm](pathfinder-bestiary-3-items/xeAn1C6HdB8ksSEc.htm)|Instant Repair|auto-trad|
|[xEEbQADYYULp07pu.htm](pathfinder-bestiary-3-items/xEEbQADYYULp07pu.htm)|Stinger|auto-trad|
|[XFaAhmVWAH18gZJm.htm](pathfinder-bestiary-3-items/XFaAhmVWAH18gZJm.htm)|Low-Light Vision|auto-trad|
|[XFheFXfH7AWzxt0S.htm](pathfinder-bestiary-3-items/XFheFXfH7AWzxt0S.htm)|Bone Cannon|auto-trad|
|[xFhOtowzFmFYfYz2.htm](pathfinder-bestiary-3-items/xFhOtowzFmFYfYz2.htm)|Constant Spells|auto-trad|
|[XfqrwEKpnBBJXrSD.htm](pathfinder-bestiary-3-items/XfqrwEKpnBBJXrSD.htm)|Attack of Opportunity (Special)|auto-trad|
|[XGigEFgiBGNmtOkF.htm](pathfinder-bestiary-3-items/XGigEFgiBGNmtOkF.htm)|Change Shape|auto-trad|
|[xglgBFL5H1uvbArD.htm](pathfinder-bestiary-3-items/xglgBFL5H1uvbArD.htm)|Adamantine Claws|auto-trad|
|[xglqaHZLA1NfPGQN.htm](pathfinder-bestiary-3-items/xglqaHZLA1NfPGQN.htm)|Divine Prepared Spells|auto-trad|
|[XGpAwWvwSpDI36HG.htm](pathfinder-bestiary-3-items/XGpAwWvwSpDI36HG.htm)|Skeleton Crew|auto-trad|
|[xGpHtG4FKoXUAvFz.htm](pathfinder-bestiary-3-items/xGpHtG4FKoXUAvFz.htm)|Trident|auto-trad|
|[XH6YaBHVaUMGRWkN.htm](pathfinder-bestiary-3-items/XH6YaBHVaUMGRWkN.htm)|Darkvision|auto-trad|
|[XhqGsa6pKMextMJ1.htm](pathfinder-bestiary-3-items/XhqGsa6pKMextMJ1.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[xIt4uW4tEMRY5Y7p.htm](pathfinder-bestiary-3-items/xIt4uW4tEMRY5Y7p.htm)|Fist|auto-trad|
|[XjarabsV3GAFA01K.htm](pathfinder-bestiary-3-items/XjarabsV3GAFA01K.htm)|Claw|auto-trad|
|[xJGiCaAnnERzWXes.htm](pathfinder-bestiary-3-items/xJGiCaAnnERzWXes.htm)|Change Shape|auto-trad|
|[xji68Q4mnqfdWVnq.htm](pathfinder-bestiary-3-items/xji68Q4mnqfdWVnq.htm)|Shield Block|auto-trad|
|[XkAvPPqqBigAbyb2.htm](pathfinder-bestiary-3-items/XkAvPPqqBigAbyb2.htm)|Telepathy 100 feet|auto-trad|
|[XlCJ1ZHQ8EGtKrFn.htm](pathfinder-bestiary-3-items/XlCJ1ZHQ8EGtKrFn.htm)|Bite|auto-trad|
|[Xlnd5wqyTUYU18hD.htm](pathfinder-bestiary-3-items/Xlnd5wqyTUYU18hD.htm)|Fangs|auto-trad|
|[xly8aKaWKIrEtqCD.htm](pathfinder-bestiary-3-items/xly8aKaWKIrEtqCD.htm)|Occult Innate Spells|auto-trad|
|[xMaFAzpC6rkfoyYs.htm](pathfinder-bestiary-3-items/xMaFAzpC6rkfoyYs.htm)|Draconic Momentum|auto-trad|
|[xMNtSzQGqtHwsi9I.htm](pathfinder-bestiary-3-items/xMNtSzQGqtHwsi9I.htm)|Rock|auto-trad|
|[xmvNKENAwK9gdUuB.htm](pathfinder-bestiary-3-items/xmvNKENAwK9gdUuB.htm)|Grab|auto-trad|
|[Xn5fxlgPbRTrvMi3.htm](pathfinder-bestiary-3-items/Xn5fxlgPbRTrvMi3.htm)|Crew's Call|auto-trad|
|[Xn6fpT76Eq3gNDgg.htm](pathfinder-bestiary-3-items/Xn6fpT76Eq3gNDgg.htm)|+4 to Will Saves vs. Fear|auto-trad|
|[XN9PqGr1Gft89U0v.htm](pathfinder-bestiary-3-items/XN9PqGr1Gft89U0v.htm)|Pervert Miracle|auto-trad|
|[xNumFo6zFMddKGAf.htm](pathfinder-bestiary-3-items/xNumFo6zFMddKGAf.htm)|Master of the Yard|auto-trad|
|[xOPKChTgrsXHKQAv.htm](pathfinder-bestiary-3-items/xOPKChTgrsXHKQAv.htm)|Light Blindness|auto-trad|
|[XPaMcuntcVkLXwLT.htm](pathfinder-bestiary-3-items/XPaMcuntcVkLXwLT.htm)|Jaws|auto-trad|
|[xPc5xxHX62bKWgkk.htm](pathfinder-bestiary-3-items/xPc5xxHX62bKWgkk.htm)|Tongue Grab|auto-trad|
|[xpiNy47A0TciwBNQ.htm](pathfinder-bestiary-3-items/xpiNy47A0TciwBNQ.htm)|Jaws|auto-trad|
|[XpkRG2gKRWLJkJEI.htm](pathfinder-bestiary-3-items/XpkRG2gKRWLJkJEI.htm)|Constant Spells|auto-trad|
|[XPllxNneASW7mX3o.htm](pathfinder-bestiary-3-items/XPllxNneASW7mX3o.htm)|Blizzard Sight|auto-trad|
|[XpzY4UQrwOlgO4km.htm](pathfinder-bestiary-3-items/XpzY4UQrwOlgO4km.htm)|Jaws|auto-trad|
|[Xq6IkurlmqAmuchc.htm](pathfinder-bestiary-3-items/Xq6IkurlmqAmuchc.htm)|Draconic Momentum|auto-trad|
|[XqjbxnxA89sOEKAK.htm](pathfinder-bestiary-3-items/XqjbxnxA89sOEKAK.htm)|Claw|auto-trad|
|[xQqEt4PLNOCTutWG.htm](pathfinder-bestiary-3-items/xQqEt4PLNOCTutWG.htm)|Draconic Momentum|auto-trad|
|[xRGfPORY2OUX19Yt.htm](pathfinder-bestiary-3-items/xRGfPORY2OUX19Yt.htm)|Spirit Dart|auto-trad|
|[xsnc3cQ3J9m1QSHk.htm](pathfinder-bestiary-3-items/xsnc3cQ3J9m1QSHk.htm)|Vie for Victory|auto-trad|
|[xSSsTbYujfY4mN5H.htm](pathfinder-bestiary-3-items/xSSsTbYujfY4mN5H.htm)|Spade|auto-trad|
|[xt4bQy0cnsmUHdm8.htm](pathfinder-bestiary-3-items/xt4bQy0cnsmUHdm8.htm)|Spherical Body|auto-trad|
|[XTe4xCgJVw7ZYYiN.htm](pathfinder-bestiary-3-items/XTe4xCgJVw7ZYYiN.htm)|Upside Down|auto-trad|
|[XTgwZSQK0BbVTSBC.htm](pathfinder-bestiary-3-items/XTgwZSQK0BbVTSBC.htm)|+1 Status to All Saves vs. Evil|auto-trad|
|[XTh9EmC4LPMteKEg.htm](pathfinder-bestiary-3-items/XTh9EmC4LPMteKEg.htm)|Darkvision|auto-trad|
|[XuhLACe64duPSQuG.htm](pathfinder-bestiary-3-items/XuhLACe64duPSQuG.htm)|Phantom Bow|auto-trad|
|[xuNy9YUXbjhDbBPJ.htm](pathfinder-bestiary-3-items/xuNy9YUXbjhDbBPJ.htm)|Leaping Pounce|auto-trad|
|[xUovRSHL0ImgkXwW.htm](pathfinder-bestiary-3-items/xUovRSHL0ImgkXwW.htm)|Claw|auto-trad|
|[xvKXt3hrhkReIqhL.htm](pathfinder-bestiary-3-items/xvKXt3hrhkReIqhL.htm)|Corrupt Speech|auto-trad|
|[xVneolKzqFvV56qV.htm](pathfinder-bestiary-3-items/xVneolKzqFvV56qV.htm)|Jaws|auto-trad|
|[xWKUQcMCVRZCnWMH.htm](pathfinder-bestiary-3-items/xWKUQcMCVRZCnWMH.htm)|Divine Innate Spells|auto-trad|
|[xWTrZfFzg3jvqkuu.htm](pathfinder-bestiary-3-items/xWTrZfFzg3jvqkuu.htm)|Greater Constrict|auto-trad|
|[xXgKRQDkgjLOs0Pw.htm](pathfinder-bestiary-3-items/xXgKRQDkgjLOs0Pw.htm)|At-Will Spells|auto-trad|
|[xxSdDW74Jjn0m4TC.htm](pathfinder-bestiary-3-items/xxSdDW74Jjn0m4TC.htm)|At-Will Spells|auto-trad|
|[xyVLktOlqlJ6Ce4h.htm](pathfinder-bestiary-3-items/xyVLktOlqlJ6Ce4h.htm)|Resonance|auto-trad|
|[Xyy6EkOtHfPObm42.htm](pathfinder-bestiary-3-items/Xyy6EkOtHfPObm42.htm)|Constant Spells|auto-trad|
|[xz2zppQgQtgwnNtp.htm](pathfinder-bestiary-3-items/xz2zppQgQtgwnNtp.htm)|Shriek|auto-trad|
|[xzjn66Avz2JyCDZb.htm](pathfinder-bestiary-3-items/xzjn66Avz2JyCDZb.htm)|Chitinous Spines|auto-trad|
|[Xzku75HXAlTNH0q3.htm](pathfinder-bestiary-3-items/Xzku75HXAlTNH0q3.htm)|Constrict|auto-trad|
|[y0GOJlbF7uqf9iXm.htm](pathfinder-bestiary-3-items/y0GOJlbF7uqf9iXm.htm)|Jaws|auto-trad|
|[Y1qTkVB6Ss3pauQB.htm](pathfinder-bestiary-3-items/Y1qTkVB6Ss3pauQB.htm)|Disorienting Faces|auto-trad|
|[Y2DLiZGrL1Rc9uJZ.htm](pathfinder-bestiary-3-items/Y2DLiZGrL1Rc9uJZ.htm)|Vulnerable to Slow|auto-trad|
|[Y2rKb2rKiZlOJBow.htm](pathfinder-bestiary-3-items/Y2rKb2rKiZlOJBow.htm)|Darkvision|auto-trad|
|[Y2Zs23iG5XQJYKVX.htm](pathfinder-bestiary-3-items/Y2Zs23iG5XQJYKVX.htm)|Darkvision|auto-trad|
|[Y4jOGsg9U0NdL52x.htm](pathfinder-bestiary-3-items/Y4jOGsg9U0NdL52x.htm)|Darkvision|auto-trad|
|[y4KzwwQKc4qz6rj7.htm](pathfinder-bestiary-3-items/y4KzwwQKc4qz6rj7.htm)|Colossal Echo|auto-trad|
|[Y4pImWAwa2NK5CDI.htm](pathfinder-bestiary-3-items/Y4pImWAwa2NK5CDI.htm)|Darkvision|auto-trad|
|[Y7BT5iq85aFgz9ND.htm](pathfinder-bestiary-3-items/Y7BT5iq85aFgz9ND.htm)|Rearing Thrust|auto-trad|
|[y7oztbBrqRdXk2kF.htm](pathfinder-bestiary-3-items/y7oztbBrqRdXk2kF.htm)|Knockdown|auto-trad|
|[y7quO1sHtNujvw9P.htm](pathfinder-bestiary-3-items/y7quO1sHtNujvw9P.htm)|Occult Innate Spells|auto-trad|
|[y8arNw5bZrQloRjm.htm](pathfinder-bestiary-3-items/y8arNw5bZrQloRjm.htm)|Wavesense (Imprecise) 60 feet|auto-trad|
|[Y9n8hgpI4feL9erM.htm](pathfinder-bestiary-3-items/Y9n8hgpI4feL9erM.htm)|Draconic Momentum|auto-trad|
|[y9v6b1HnMTeyR6Aa.htm](pathfinder-bestiary-3-items/y9v6b1HnMTeyR6Aa.htm)|Jaws|auto-trad|
|[YAt3ohXujxgfi1dY.htm](pathfinder-bestiary-3-items/YAt3ohXujxgfi1dY.htm)|Darkvision|auto-trad|
|[YAtOaRD1Fb2RuzBs.htm](pathfinder-bestiary-3-items/YAtOaRD1Fb2RuzBs.htm)|Divine Innate Spells|auto-trad|
|[YbkGuRsw8NVVsIfB.htm](pathfinder-bestiary-3-items/YbkGuRsw8NVVsIfB.htm)|Spectral Jaws|auto-trad|
|[yBpXyahw41IRnTwr.htm](pathfinder-bestiary-3-items/yBpXyahw41IRnTwr.htm)|Dreadful Prediction|auto-trad|
|[ybXtrOFeg06hi75j.htm](pathfinder-bestiary-3-items/ybXtrOFeg06hi75j.htm)|Divine Innate Spells|auto-trad|
|[yC55qBRcZsAS9EaR.htm](pathfinder-bestiary-3-items/yC55qBRcZsAS9EaR.htm)|Low-Light Vision|auto-trad|
|[ycIkiccSXhjaJ6am.htm](pathfinder-bestiary-3-items/ycIkiccSXhjaJ6am.htm)|Deflecting Gale|auto-trad|
|[yCLa7vNTrFdsMYUz.htm](pathfinder-bestiary-3-items/yCLa7vNTrFdsMYUz.htm)|Low-Light Vision|auto-trad|
|[Ycopi6QWkxh3DeYu.htm](pathfinder-bestiary-3-items/Ycopi6QWkxh3DeYu.htm)|Easy to Call|auto-trad|
|[yd3ZruXaEyefPpGs.htm](pathfinder-bestiary-3-items/yd3ZruXaEyefPpGs.htm)|Talon|auto-trad|
|[yDBgrswetRTvDh9j.htm](pathfinder-bestiary-3-items/yDBgrswetRTvDh9j.htm)|Revived Retaliation|auto-trad|
|[YDmIyZCs3c67oBWR.htm](pathfinder-bestiary-3-items/YDmIyZCs3c67oBWR.htm)|Jaws|auto-trad|
|[yds3BWASeIlBPuQ3.htm](pathfinder-bestiary-3-items/yds3BWASeIlBPuQ3.htm)|Telepathy 500 feet|auto-trad|
|[YE7BBnb0szfbgyZp.htm](pathfinder-bestiary-3-items/YE7BBnb0szfbgyZp.htm)|Change Shape|auto-trad|
|[YEEOqFUadOvlmJnt.htm](pathfinder-bestiary-3-items/YEEOqFUadOvlmJnt.htm)|Bounding Sprint|auto-trad|
|[YF0cM4fWT0fvGHqC.htm](pathfinder-bestiary-3-items/YF0cM4fWT0fvGHqC.htm)|Revived Retaliation|auto-trad|
|[yF5x6s4ZYEZtto6J.htm](pathfinder-bestiary-3-items/yF5x6s4ZYEZtto6J.htm)|Claw|auto-trad|
|[YFR9EAebk1pSnIl3.htm](pathfinder-bestiary-3-items/YFR9EAebk1pSnIl3.htm)|Tremorsense (Precise) 30 feet|auto-trad|
|[Yft4ncVPX04wtjTC.htm](pathfinder-bestiary-3-items/Yft4ncVPX04wtjTC.htm)|Echolocation (Precise) 20 feet|auto-trad|
|[YfW3hmqsouC0M5an.htm](pathfinder-bestiary-3-items/YfW3hmqsouC0M5an.htm)|Attack of Opportunity|auto-trad|
|[YGMkhOIOrpQ8AZbt.htm](pathfinder-bestiary-3-items/YGMkhOIOrpQ8AZbt.htm)|Erudite|auto-trad|
|[yHPiBI51EEqH1jct.htm](pathfinder-bestiary-3-items/yHPiBI51EEqH1jct.htm)|Rituals|auto-trad|
|[yipkwbXsFDd1t15w.htm](pathfinder-bestiary-3-items/yipkwbXsFDd1t15w.htm)|Consume Thoughts|auto-trad|
|[yiRwJtMI4udvjxuF.htm](pathfinder-bestiary-3-items/yiRwJtMI4udvjxuF.htm)|Frightful Presence|auto-trad|
|[YJ4VNOPXDLADhc0v.htm](pathfinder-bestiary-3-items/YJ4VNOPXDLADhc0v.htm)|Jaws|auto-trad|
|[yJ8dDwM4sX2nS4UR.htm](pathfinder-bestiary-3-items/yJ8dDwM4sX2nS4UR.htm)|Aquatic Drag|auto-trad|
|[yJoFSnPHI2YuGrUT.htm](pathfinder-bestiary-3-items/yJoFSnPHI2YuGrUT.htm)|Darkvision|auto-trad|
|[ykB9VqfwXdIegLS2.htm](pathfinder-bestiary-3-items/ykB9VqfwXdIegLS2.htm)|Sneak Attack|auto-trad|
|[YKcp0LZjzLqu4KfR.htm](pathfinder-bestiary-3-items/YKcp0LZjzLqu4KfR.htm)|Attack of Opportunity (Special)|auto-trad|
|[YKo3Am15Wz2kJyKI.htm](pathfinder-bestiary-3-items/YKo3Am15Wz2kJyKI.htm)|Illusory Weapon|auto-trad|
|[Ykw6ewrTUkGjjiKl.htm](pathfinder-bestiary-3-items/Ykw6ewrTUkGjjiKl.htm)|Defensive Shove|auto-trad|
|[ylEVi6mr1P1f1Nwz.htm](pathfinder-bestiary-3-items/ylEVi6mr1P1f1Nwz.htm)|Darkvision|auto-trad|
|[YLX02foABJxLh21p.htm](pathfinder-bestiary-3-items/YLX02foABJxLh21p.htm)|Occult Innate Spells|auto-trad|
|[Ym03EsvdVPymdBSB.htm](pathfinder-bestiary-3-items/Ym03EsvdVPymdBSB.htm)|Deep Breath|auto-trad|
|[ymApeJr8ZiUS6OhX.htm](pathfinder-bestiary-3-items/ymApeJr8ZiUS6OhX.htm)|Spewing Bile|auto-trad|
|[YmLWpVwgT7G41DLf.htm](pathfinder-bestiary-3-items/YmLWpVwgT7G41DLf.htm)|Tattered Soul|auto-trad|
|[yMNZmEZoX7NmpxxP.htm](pathfinder-bestiary-3-items/yMNZmEZoX7NmpxxP.htm)|Occult Innate Spells|auto-trad|
|[yn9p5OZFEVAJgqpJ.htm](pathfinder-bestiary-3-items/yn9p5OZFEVAJgqpJ.htm)|Occult Innate Spells|auto-trad|
|[yNJghW8czppyVYi1.htm](pathfinder-bestiary-3-items/yNJghW8czppyVYi1.htm)|Draconic Momentum|auto-trad|
|[YNpN5FwvZ27Zfviz.htm](pathfinder-bestiary-3-items/YNpN5FwvZ27Zfviz.htm)|Shambling Onslaught|auto-trad|
|[YnX58rhrT4hsl8zl.htm](pathfinder-bestiary-3-items/YnX58rhrT4hsl8zl.htm)|Arcane Bolt|auto-trad|
|[YnXG494wllYVpiYZ.htm](pathfinder-bestiary-3-items/YnXG494wllYVpiYZ.htm)|Negative Healing|auto-trad|
|[yo565WZx3G5mcub3.htm](pathfinder-bestiary-3-items/yo565WZx3G5mcub3.htm)|Coiling Frenzy|auto-trad|
|[yO6YNvoUDLZPlNwE.htm](pathfinder-bestiary-3-items/yO6YNvoUDLZPlNwE.htm)|Focus Beauty|auto-trad|
|[yodQpRTpnwuSLWgE.htm](pathfinder-bestiary-3-items/yodQpRTpnwuSLWgE.htm)|At-Will Spells|auto-trad|
|[yoFbIDWoysXLt9p4.htm](pathfinder-bestiary-3-items/yoFbIDWoysXLt9p4.htm)|Foot|auto-trad|
|[yox41dJM9YFdHSRU.htm](pathfinder-bestiary-3-items/yox41dJM9YFdHSRU.htm)|Constant Spells|auto-trad|
|[YPcOC7ZuzY8aASqU.htm](pathfinder-bestiary-3-items/YPcOC7ZuzY8aASqU.htm)|Ghost Hunter|auto-trad|
|[YPqH3eXv1Ux2rBDb.htm](pathfinder-bestiary-3-items/YPqH3eXv1Ux2rBDb.htm)|Lifesense (Imprecise) 60 feet|auto-trad|
|[ypyEw36BPA1kfq0T.htm](pathfinder-bestiary-3-items/ypyEw36BPA1kfq0T.htm)|Claw|auto-trad|
|[yQ6h3MLUgYAJcomj.htm](pathfinder-bestiary-3-items/yQ6h3MLUgYAJcomj.htm)|Furious Possession|auto-trad|
|[YQs3tTvZeC9rNSZB.htm](pathfinder-bestiary-3-items/YQs3tTvZeC9rNSZB.htm)|Song of the Swamp|auto-trad|
|[yQzN4wExqO58Ssdr.htm](pathfinder-bestiary-3-items/yQzN4wExqO58Ssdr.htm)|Slow|auto-trad|
|[YRKU8F7zH6qq03Om.htm](pathfinder-bestiary-3-items/YRKU8F7zH6qq03Om.htm)|Constant Spells|auto-trad|
|[YrKVoYsRIkzzPSEH.htm](pathfinder-bestiary-3-items/YrKVoYsRIkzzPSEH.htm)|Darkvision|auto-trad|
|[yRRl0UTbRwQx83WC.htm](pathfinder-bestiary-3-items/yRRl0UTbRwQx83WC.htm)|Smear|auto-trad|
|[yrUTciOjsrqkiFEh.htm](pathfinder-bestiary-3-items/yrUTciOjsrqkiFEh.htm)|Jaws|auto-trad|
|[YS29qvFcBB3p68fh.htm](pathfinder-bestiary-3-items/YS29qvFcBB3p68fh.htm)|Darkvision|auto-trad|
|[YsbObHTSL33vRGBA.htm](pathfinder-bestiary-3-items/YsbObHTSL33vRGBA.htm)|Darkvision|auto-trad|
|[ySPa5Sa7wY7Oyugj.htm](pathfinder-bestiary-3-items/ySPa5Sa7wY7Oyugj.htm)|Claw|auto-trad|
|[ySTvjgpwqUPCVoUE.htm](pathfinder-bestiary-3-items/ySTvjgpwqUPCVoUE.htm)|Darkvision|auto-trad|
|[YSVbN2hohx9XI8OH.htm](pathfinder-bestiary-3-items/YSVbN2hohx9XI8OH.htm)|Tail|auto-trad|
|[ytbxTl9miHNeB5iO.htm](pathfinder-bestiary-3-items/ytbxTl9miHNeB5iO.htm)|Spring Up|auto-trad|
|[yTqrSB5RzCRPTbYd.htm](pathfinder-bestiary-3-items/yTqrSB5RzCRPTbYd.htm)|Countered by Earth|auto-trad|
|[YvbJG9mY33OhkVRp.htm](pathfinder-bestiary-3-items/YvbJG9mY33OhkVRp.htm)|Negative Healing|auto-trad|
|[yVpUeHZRYHGkXayx.htm](pathfinder-bestiary-3-items/yVpUeHZRYHGkXayx.htm)|Suspended Animation|auto-trad|
|[YwO8ViIiTT0pBxLd.htm](pathfinder-bestiary-3-items/YwO8ViIiTT0pBxLd.htm)|Splatter|auto-trad|
|[YWWsxw1woFyoUa9a.htm](pathfinder-bestiary-3-items/YWWsxw1woFyoUa9a.htm)|Negative Healing|auto-trad|
|[Yx7C6ebcU2WQckqn.htm](pathfinder-bestiary-3-items/Yx7C6ebcU2WQckqn.htm)|Rift Sense|auto-trad|
|[YXFicpAJz9WZdrnS.htm](pathfinder-bestiary-3-items/YXFicpAJz9WZdrnS.htm)|Boneshard Burst|auto-trad|
|[yxOzb3edpHIxCIh9.htm](pathfinder-bestiary-3-items/yxOzb3edpHIxCIh9.htm)|Divine Innate Spells|auto-trad|
|[YxUwb1otEos5UMSS.htm](pathfinder-bestiary-3-items/YxUwb1otEos5UMSS.htm)|All This Has Happened Before|auto-trad|
|[YyJ8ioOaohHGksR2.htm](pathfinder-bestiary-3-items/YyJ8ioOaohHGksR2.htm)|Negative Healing|auto-trad|
|[yySGyBmXQKMzLIZ0.htm](pathfinder-bestiary-3-items/yySGyBmXQKMzLIZ0.htm)|+1 Status to all Saves vs. Divine and Positive|auto-trad|
|[yyvo4Lyc2M4RQhHy.htm](pathfinder-bestiary-3-items/yyvo4Lyc2M4RQhHy.htm)|Grab|auto-trad|
|[YZALPaj0pw2QQhWF.htm](pathfinder-bestiary-3-items/YZALPaj0pw2QQhWF.htm)|Grab|auto-trad|
|[YZgastPDC3pUUwDr.htm](pathfinder-bestiary-3-items/YZgastPDC3pUUwDr.htm)|Ink Blade|auto-trad|
|[yzSpI7KmbcXVQQEF.htm](pathfinder-bestiary-3-items/yzSpI7KmbcXVQQEF.htm)|Mentalist Counterspell|auto-trad|
|[YzVNI5zauuNyLldf.htm](pathfinder-bestiary-3-items/YzVNI5zauuNyLldf.htm)|Sunset Dependent|auto-trad|
|[z19VI7TL2KhjANdt.htm](pathfinder-bestiary-3-items/z19VI7TL2KhjANdt.htm)|Constrict|auto-trad|
|[z23Nlek8h4x4L0lq.htm](pathfinder-bestiary-3-items/z23Nlek8h4x4L0lq.htm)|Occult Innate Spells|auto-trad|
|[Z2AdAroZg5MIB8bp.htm](pathfinder-bestiary-3-items/Z2AdAroZg5MIB8bp.htm)|Wide Cleave|auto-trad|
|[z2iLBR1l32mSdxZg.htm](pathfinder-bestiary-3-items/z2iLBR1l32mSdxZg.htm)|Cynic's Curse|auto-trad|
|[Z2P5qc4Zo39S1t6w.htm](pathfinder-bestiary-3-items/Z2P5qc4Zo39S1t6w.htm)|Fist|auto-trad|
|[Z3lZR1EPDU26XJlG.htm](pathfinder-bestiary-3-items/Z3lZR1EPDU26XJlG.htm)|Jaws|auto-trad|
|[Z4KMWDa1g8Ai7MCE.htm](pathfinder-bestiary-3-items/Z4KMWDa1g8Ai7MCE.htm)|At-Will Spells|auto-trad|
|[Z4wolFb7CMno5VO4.htm](pathfinder-bestiary-3-items/Z4wolFb7CMno5VO4.htm)|Forest Stride|auto-trad|
|[Z6FPbe7j4A4d8s6s.htm](pathfinder-bestiary-3-items/Z6FPbe7j4A4d8s6s.htm)|Swarming Bites|auto-trad|
|[Z79hB9KYIozan9AY.htm](pathfinder-bestiary-3-items/Z79hB9KYIozan9AY.htm)|Telepathy 100 feet|auto-trad|
|[z8GylSGuIeDFrE0Z.htm](pathfinder-bestiary-3-items/z8GylSGuIeDFrE0Z.htm)|Shy|auto-trad|
|[z8QOqIIzB4F2TaBY.htm](pathfinder-bestiary-3-items/z8QOqIIzB4F2TaBY.htm)|Claw|auto-trad|
|[zaD6IUn59EF4mqzi.htm](pathfinder-bestiary-3-items/zaD6IUn59EF4mqzi.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[zAfIfdV5UBJtB564.htm](pathfinder-bestiary-3-items/zAfIfdV5UBJtB564.htm)|Smooth Swimmer|auto-trad|
|[ZaqhlgxYla5yl1jh.htm](pathfinder-bestiary-3-items/ZaqhlgxYla5yl1jh.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[zBdDYujCaw7HK02d.htm](pathfinder-bestiary-3-items/zBdDYujCaw7HK02d.htm)|Anticoagulant|auto-trad|
|[zbmQRPZHDEeRga68.htm](pathfinder-bestiary-3-items/zbmQRPZHDEeRga68.htm)|At-Will Spells|auto-trad|
|[zbt0txkLR1uhzPjL.htm](pathfinder-bestiary-3-items/zbt0txkLR1uhzPjL.htm)|+1 Status to All Saves vs. Controlled Condition|auto-trad|
|[zc0tBM6QCWY3pRQR.htm](pathfinder-bestiary-3-items/zc0tBM6QCWY3pRQR.htm)|Arm|auto-trad|
|[zCqM5htGK65bK9Uh.htm](pathfinder-bestiary-3-items/zCqM5htGK65bK9Uh.htm)|Telepathy 100 feet|auto-trad|
|[zdb8RR0jcIIol6on.htm](pathfinder-bestiary-3-items/zdb8RR0jcIIol6on.htm)|Wind-Up|auto-trad|
|[zDFtneAmSUjx6qnK.htm](pathfinder-bestiary-3-items/zDFtneAmSUjx6qnK.htm)|Pointed Charge|auto-trad|
|[ZEFMlTiTYApQQKuS.htm](pathfinder-bestiary-3-items/ZEFMlTiTYApQQKuS.htm)|Grab|auto-trad|
|[ZEGxGxVAaZSmhNp7.htm](pathfinder-bestiary-3-items/ZEGxGxVAaZSmhNp7.htm)|Foot|auto-trad|
|[zEouFbaKNoTFZeHy.htm](pathfinder-bestiary-3-items/zEouFbaKNoTFZeHy.htm)|Mix Couatl Venom|auto-trad|
|[zeWAnIFSjhteCiiY.htm](pathfinder-bestiary-3-items/zeWAnIFSjhteCiiY.htm)|At-Will Spells|auto-trad|
|[zfcj9MZNHpEvkerg.htm](pathfinder-bestiary-3-items/zfcj9MZNHpEvkerg.htm)|Telepathy 100 feet|auto-trad|
|[ZffdskKO51EjTFvx.htm](pathfinder-bestiary-3-items/ZffdskKO51EjTFvx.htm)|Constrict|auto-trad|
|[ZFhHkksQnaNOI9Rk.htm](pathfinder-bestiary-3-items/ZFhHkksQnaNOI9Rk.htm)|Fed by Wood|auto-trad|
|[ZFMGP6RS8c651Pki.htm](pathfinder-bestiary-3-items/ZFMGP6RS8c651Pki.htm)|Claw|auto-trad|
|[zFOxLexJnjqhpWy0.htm](pathfinder-bestiary-3-items/zFOxLexJnjqhpWy0.htm)|Needle|auto-trad|
|[zFuVmNNi7HexpunZ.htm](pathfinder-bestiary-3-items/zFuVmNNi7HexpunZ.htm)|Sixfold Flurry|auto-trad|
|[zg034Oefe1w3sHwh.htm](pathfinder-bestiary-3-items/zg034Oefe1w3sHwh.htm)|+2 Status to All Saves vs. Primal Magic|auto-trad|
|[zGreN5MDFtAqJh8U.htm](pathfinder-bestiary-3-items/zGreN5MDFtAqJh8U.htm)|Darkvision|auto-trad|
|[zGXRWPh3UMA5q46D.htm](pathfinder-bestiary-3-items/zGXRWPh3UMA5q46D.htm)|Darkvision|auto-trad|
|[ZgZ9FcS7hIAO9FxC.htm](pathfinder-bestiary-3-items/ZgZ9FcS7hIAO9FxC.htm)|Girtablilu Venom|auto-trad|
|[zHIoJ0oELrIHabJu.htm](pathfinder-bestiary-3-items/zHIoJ0oELrIHabJu.htm)|Slippery|auto-trad|
|[ZHJXY7uHZTuD4bL6.htm](pathfinder-bestiary-3-items/ZHJXY7uHZTuD4bL6.htm)|Scatterbrain Palm|auto-trad|
|[zhUapm5Z8hOglOwU.htm](pathfinder-bestiary-3-items/zhUapm5Z8hOglOwU.htm)|Low-Light Vision|auto-trad|
|[zILlYq3L0c0BJfWG.htm](pathfinder-bestiary-3-items/zILlYq3L0c0BJfWG.htm)|Runic Resistance|auto-trad|
|[ZiNy2010T2g4saPw.htm](pathfinder-bestiary-3-items/ZiNy2010T2g4saPw.htm)|Spiny Body|auto-trad|
|[zj5lykgkozQXgCYv.htm](pathfinder-bestiary-3-items/zj5lykgkozQXgCYv.htm)|Skip Between|auto-trad|
|[zJKO70Z0e4PdkPUS.htm](pathfinder-bestiary-3-items/zJKO70Z0e4PdkPUS.htm)|Quills|auto-trad|
|[zJprqN3Lkl03vYP7.htm](pathfinder-bestiary-3-items/zJprqN3Lkl03vYP7.htm)|Arcane Prepared Spells|auto-trad|
|[zjRf8vLHR2qRhuCx.htm](pathfinder-bestiary-3-items/zjRf8vLHR2qRhuCx.htm)|Kukri|auto-trad|
|[zJSR7LD72PzGqnDl.htm](pathfinder-bestiary-3-items/zJSR7LD72PzGqnDl.htm)|Low-Light Vision|auto-trad|
|[zJVmAmV1kg1wV6T3.htm](pathfinder-bestiary-3-items/zJVmAmV1kg1wV6T3.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[ZkPq0d1hLdzoA3Mf.htm](pathfinder-bestiary-3-items/ZkPq0d1hLdzoA3Mf.htm)|Low-Light Vision|auto-trad|
|[zLh5jRcxUaafQvod.htm](pathfinder-bestiary-3-items/zLh5jRcxUaafQvod.htm)|Greater Constrict|auto-trad|
|[ZlMkrt960OMod6zh.htm](pathfinder-bestiary-3-items/ZlMkrt960OMod6zh.htm)|Jaws|auto-trad|
|[ZLvVuoT7wsCVMtYC.htm](pathfinder-bestiary-3-items/ZLvVuoT7wsCVMtYC.htm)|Frightful Presence|auto-trad|
|[ZmPA9nWFtMXPCArA.htm](pathfinder-bestiary-3-items/ZmPA9nWFtMXPCArA.htm)|Ganzi Spells|auto-trad|
|[zMQJh0NjokM3IneE.htm](pathfinder-bestiary-3-items/zMQJh0NjokM3IneE.htm)|Draconic Momentum|auto-trad|
|[ZmVqNTMjqte6Xf1k.htm](pathfinder-bestiary-3-items/ZmVqNTMjqte6Xf1k.htm)|Fist|auto-trad|
|[zMy5ikhAFWW94nIF.htm](pathfinder-bestiary-3-items/zMy5ikhAFWW94nIF.htm)|Darkvision|auto-trad|
|[zmZ1L8pspuQQGh1m.htm](pathfinder-bestiary-3-items/zmZ1L8pspuQQGh1m.htm)|Drain Blood|auto-trad|
|[znaX5hWmLXH23ywJ.htm](pathfinder-bestiary-3-items/znaX5hWmLXH23ywJ.htm)|Subsonic Pulse|auto-trad|
|[ZneHwzrBFdJkdzsG.htm](pathfinder-bestiary-3-items/ZneHwzrBFdJkdzsG.htm)|Change Shape|auto-trad|
|[znuFqFCIX1YQTcWx.htm](pathfinder-bestiary-3-items/znuFqFCIX1YQTcWx.htm)|Resonant Chimes|auto-trad|
|[znWQpCRgJXaYEk5Q.htm](pathfinder-bestiary-3-items/znWQpCRgJXaYEk5Q.htm)|Grab|auto-trad|
|[zNYXcSmgZ8w8pTTE.htm](pathfinder-bestiary-3-items/zNYXcSmgZ8w8pTTE.htm)|Tremorsense (Imprecise) 15 feet|auto-trad|
|[Zo7w17AAhYEJgyRJ.htm](pathfinder-bestiary-3-items/Zo7w17AAhYEJgyRJ.htm)|Catch Rock|auto-trad|
|[zobdYiwWZOnM8PAI.htm](pathfinder-bestiary-3-items/zobdYiwWZOnM8PAI.htm)|Lifewick Candle|auto-trad|
|[zoT7uILQYCXy5Dtz.htm](pathfinder-bestiary-3-items/zoT7uILQYCXy5Dtz.htm)|Grab|auto-trad|
|[zOtMfpFqx7YaAJxc.htm](pathfinder-bestiary-3-items/zOtMfpFqx7YaAJxc.htm)|Crystalline Dust|auto-trad|
|[Zp3zBQ50NNxsfjVx.htm](pathfinder-bestiary-3-items/Zp3zBQ50NNxsfjVx.htm)|Low-Light Vision|auto-trad|
|[ZPn8LuEY8HTRPp38.htm](pathfinder-bestiary-3-items/ZPn8LuEY8HTRPp38.htm)|Withering Aura|auto-trad|
|[zPSlJozXBmZIdCsT.htm](pathfinder-bestiary-3-items/zPSlJozXBmZIdCsT.htm)|Nervous Consumption|auto-trad|
|[zPU1mG2FJpTt19Z0.htm](pathfinder-bestiary-3-items/zPU1mG2FJpTt19Z0.htm)|Change Shape|auto-trad|
|[zpYoiV9dZOr8GH5F.htm](pathfinder-bestiary-3-items/zpYoiV9dZOr8GH5F.htm)|Motion Sense 60 feet|auto-trad|
|[zQ1jyG0TlCxqXyuU.htm](pathfinder-bestiary-3-items/zQ1jyG0TlCxqXyuU.htm)|Jaws|auto-trad|
|[zqnwkdM7RpErrayk.htm](pathfinder-bestiary-3-items/zqnwkdM7RpErrayk.htm)|Death Gasp|auto-trad|
|[zR7WoyREJtjNrtuv.htm](pathfinder-bestiary-3-items/zR7WoyREJtjNrtuv.htm)|Rock|auto-trad|
|[zsHciQgmfpHT7Rdu.htm](pathfinder-bestiary-3-items/zsHciQgmfpHT7Rdu.htm)|Jaws|auto-trad|
|[ZSiz1ntZMGGH8ef9.htm](pathfinder-bestiary-3-items/ZSiz1ntZMGGH8ef9.htm)|Low-Light Vision|auto-trad|
|[zSnETnJBgsrn1A04.htm](pathfinder-bestiary-3-items/zSnETnJBgsrn1A04.htm)|Earthmound Dweller|auto-trad|
|[ZsO3Se3p63hjEFm6.htm](pathfinder-bestiary-3-items/ZsO3Se3p63hjEFm6.htm)|Darkvision|auto-trad|
|[ZSyd5tDHWnYAj8zD.htm](pathfinder-bestiary-3-items/ZSyd5tDHWnYAj8zD.htm)|Spectral Jaws|auto-trad|
|[Zta2Kf1JwyI8OprE.htm](pathfinder-bestiary-3-items/Zta2Kf1JwyI8OprE.htm)|Burning Cold|auto-trad|
|[ztbAEwewoiM3nyRj.htm](pathfinder-bestiary-3-items/ztbAEwewoiM3nyRj.htm)|Dagger|auto-trad|
|[zTdgUNL0sTJoxpwC.htm](pathfinder-bestiary-3-items/zTdgUNL0sTJoxpwC.htm)|Clutching Stones|auto-trad|
|[zTFuXpu6BuPdbg6N.htm](pathfinder-bestiary-3-items/zTFuXpu6BuPdbg6N.htm)|Brawling Critical|auto-trad|
|[Zu7OCWAY7oZL41Am.htm](pathfinder-bestiary-3-items/Zu7OCWAY7oZL41Am.htm)|Walk the Ethereal Line|auto-trad|
|[ZuL0FdvovBnvOlaq.htm](pathfinder-bestiary-3-items/ZuL0FdvovBnvOlaq.htm)|Negative Healing|auto-trad|
|[zuM9eNga9GMtun7x.htm](pathfinder-bestiary-3-items/zuM9eNga9GMtun7x.htm)|Low-Light Vision|auto-trad|
|[ZUqkHDdNfKyhgkOs.htm](pathfinder-bestiary-3-items/ZUqkHDdNfKyhgkOs.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[zvCWR1v2of2jkxSl.htm](pathfinder-bestiary-3-items/zvCWR1v2of2jkxSl.htm)|Plague of Ancients|auto-trad|
|[zVDg72KpqmTWTtgS.htm](pathfinder-bestiary-3-items/zVDg72KpqmTWTtgS.htm)|Doru Venom|auto-trad|
|[ZVncjXiJwtqSbdbS.htm](pathfinder-bestiary-3-items/ZVncjXiJwtqSbdbS.htm)|+4 Status to All Saves vs. Mental or Divine|auto-trad|
|[ZVnCqIGwZwPukDhh.htm](pathfinder-bestiary-3-items/ZVnCqIGwZwPukDhh.htm)|Spear|auto-trad|
|[zwfNSb5Ft4v1QTW3.htm](pathfinder-bestiary-3-items/zwfNSb5Ft4v1QTW3.htm)|Rock|auto-trad|
|[zwNUc8bsuHFTyEqi.htm](pathfinder-bestiary-3-items/zwNUc8bsuHFTyEqi.htm)|Coiling Frenzy|auto-trad|
|[zwPydFnSps4ZXlka.htm](pathfinder-bestiary-3-items/zwPydFnSps4ZXlka.htm)|Swarmwalker|auto-trad|
|[zXCRAQ4vboei7ilQ.htm](pathfinder-bestiary-3-items/zXCRAQ4vboei7ilQ.htm)|Carrion Scent (Imprecise) 30 feet|auto-trad|
|[ZXDEnUnH0Uz4H2J7.htm](pathfinder-bestiary-3-items/ZXDEnUnH0Uz4H2J7.htm)|Tremorsense (Precise) 120 feet, (Imprecise) 240 feet|auto-trad|
|[ZxPGNj1JDdTfH2vH.htm](pathfinder-bestiary-3-items/ZxPGNj1JDdTfH2vH.htm)|Sunlight Powerlessness|auto-trad|
|[zxRmkFpH5qKW2WJe.htm](pathfinder-bestiary-3-items/zxRmkFpH5qKW2WJe.htm)|Darkvision|auto-trad|
|[ZY0LmvdvxDzWYzTp.htm](pathfinder-bestiary-3-items/ZY0LmvdvxDzWYzTp.htm)|At-Will Spells|auto-trad|
|[zY3L0jOu3AcqWwDa.htm](pathfinder-bestiary-3-items/zY3L0jOu3AcqWwDa.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[zY6HRNQ28oqHNnmV.htm](pathfinder-bestiary-3-items/zY6HRNQ28oqHNnmV.htm)|Unsettling Mind|auto-trad|
|[ZYC0qWDiNlFEQBZ3.htm](pathfinder-bestiary-3-items/ZYC0qWDiNlFEQBZ3.htm)|Occult Innate Spells|auto-trad|
|[Zylzzu3xfzA6axxc.htm](pathfinder-bestiary-3-items/Zylzzu3xfzA6axxc.htm)|Craft Ice Staff|auto-trad|
|[zZ9i2cbCfyyfJXBh.htm](pathfinder-bestiary-3-items/zZ9i2cbCfyyfJXBh.htm)|Breath Weapon|auto-trad|
|[zZADJ8AZtILSidCx.htm](pathfinder-bestiary-3-items/zZADJ8AZtILSidCx.htm)|Horn|auto-trad|
|[zZdlKoZukVTRu2lC.htm](pathfinder-bestiary-3-items/zZdlKoZukVTRu2lC.htm)|Cavern Dependent|auto-trad|
|[zZdUA3JVNbgUwFrw.htm](pathfinder-bestiary-3-items/zZdUA3JVNbgUwFrw.htm)|Jump Scare|auto-trad|
|[ZZpXzxmtbX8o1MxN.htm](pathfinder-bestiary-3-items/ZZpXzxmtbX8o1MxN.htm)|Vulnerable to Stone to Flesh|auto-trad|
|[zzRvWHp5hTbs93tu.htm](pathfinder-bestiary-3-items/zzRvWHp5hTbs93tu.htm)|Breath Weapon|auto-trad|
|[ZZwg2ODhydDeHkEy.htm](pathfinder-bestiary-3-items/ZZwg2ODhydDeHkEy.htm)|Scimitar Blitz|auto-trad|
|[zZzMJYEYbjykXd3S.htm](pathfinder-bestiary-3-items/zZzMJYEYbjykXd3S.htm)|Flamewing Buffet|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0Himurdx3RL3D4gk.htm](pathfinder-bestiary-3-items/0Himurdx3RL3D4gk.htm)|Broken Thorns|Broken Thorns|modificada|
|[1BVkuiVh37hRA4y8.htm](pathfinder-bestiary-3-items/1BVkuiVh37hRA4y8.htm)|Vent Energy|Expulsar gases|modificada|
|[2PoHFE6FWRaWarJD.htm](pathfinder-bestiary-3-items/2PoHFE6FWRaWarJD.htm)|Breath Weapon|Breath Weapon|modificada|
|[3zY8sXqA8CTtAm4D.htm](pathfinder-bestiary-3-items/3zY8sXqA8CTtAm4D.htm)|Lingering Enmity|Enemistad persistente|modificada|
|[56KGSD8qAWdFbvdP.htm](pathfinder-bestiary-3-items/56KGSD8qAWdFbvdP.htm)|Divine Innate Spells (See Ganzi Spells)|Hechizos innatos divinos (Ver Hechizos de Ganz).|modificada|
|[5I1UyAH3ppy0Pi6l.htm](pathfinder-bestiary-3-items/5I1UyAH3ppy0Pi6l.htm)|Rib Skewer|Ensartar Costilla|modificada|
|[7osAd2va2Xd7Jqmw.htm](pathfinder-bestiary-3-items/7osAd2va2Xd7Jqmw.htm)|Ectoplasmic Form|Forma Ectoplasmica|modificada|
|[8isKGe0HwKWmQDEg.htm](pathfinder-bestiary-3-items/8isKGe0HwKWmQDEg.htm)|Focus Beauty|Focus Belleza|modificada|
|[AJK2FzoU6wIUxE43.htm](pathfinder-bestiary-3-items/AJK2FzoU6wIUxE43.htm)|Flaming Shroud|Mortaja Flamígera|modificada|
|[BskkanKxSINfDBr9.htm](pathfinder-bestiary-3-items/BskkanKxSINfDBr9.htm)|Bittersweet Dreams|Sueños agridulces|modificada|
|[e3ozogxqOi4jcWiU.htm](pathfinder-bestiary-3-items/e3ozogxqOi4jcWiU.htm)|Pungent Aura|Aura Picante|modificada|
|[e81mjeZGp9mKBDKv.htm](pathfinder-bestiary-3-items/e81mjeZGp9mKBDKv.htm)|Change Shape|Change Shape|modificada|
|[EcU0YXprTtXbRRmY.htm](pathfinder-bestiary-3-items/EcU0YXprTtXbRRmY.htm)|Bloodbird|Bloodbird|modificada|
|[fZUt506WBOAg0Mrm.htm](pathfinder-bestiary-3-items/fZUt506WBOAg0Mrm.htm)|Spearing Tail|Spearing Tail|modificada|
|[h3GtcNC8KADgxKzm.htm](pathfinder-bestiary-3-items/h3GtcNC8KADgxKzm.htm)|Tearing Clutch|Tearing Clutch|modificada|
|[HOEKikyuy4nwWSmn.htm](pathfinder-bestiary-3-items/HOEKikyuy4nwWSmn.htm)|Inkstain|Inkstain|modificada|
|[JQcN49zNIu4wTuSg.htm](pathfinder-bestiary-3-items/JQcN49zNIu4wTuSg.htm)|Defensive Quills|Defensive Quills|modificada|
|[kjoPJjVHl7tsoAet.htm](pathfinder-bestiary-3-items/kjoPJjVHl7tsoAet.htm)|Breath Weapon|Breath Weapon|modificada|
|[kQNlQmU6whlWK7M9.htm](pathfinder-bestiary-3-items/kQNlQmU6whlWK7M9.htm)|Telepathic Wail|Lamento Telepático|modificada|
|[kY9XyX74vUAFhali.htm](pathfinder-bestiary-3-items/kY9XyX74vUAFhali.htm)|Flay|Flay|modificada|
|[mAl92vhU2YdKG3bV.htm](pathfinder-bestiary-3-items/mAl92vhU2YdKG3bV.htm)|Breath Weapon|Breath Weapon|modificada|
|[OnmLcXNYU1Jxp5ji.htm](pathfinder-bestiary-3-items/OnmLcXNYU1Jxp5ji.htm)|Bubonic Plague|Bubonic Plague|modificada|
|[RGJbS9RTIb1LL5uF.htm](pathfinder-bestiary-3-items/RGJbS9RTIb1LL5uF.htm)|Swarming Snips|Swarming Snips|modificada|
|[snPpaYVXqbrRM8Ne.htm](pathfinder-bestiary-3-items/snPpaYVXqbrRM8Ne.htm)|Breath Weapon|Breath Weapon|modificada|
|[sT9rrvUVQPaZDRMI.htm](pathfinder-bestiary-3-items/sT9rrvUVQPaZDRMI.htm)|Drain Life|Drenar Vida|modificada|
|[UFxr3UoWoyUD8Oh4.htm](pathfinder-bestiary-3-items/UFxr3UoWoyUD8Oh4.htm)|Feral Gnaw|Feral Gnaw|modificada|
|[uubvbSioie58su3V.htm](pathfinder-bestiary-3-items/uubvbSioie58su3V.htm)|Feeding Frenzy|Frenesí alimenticio|modificada|
|[xA0xMv9xbqMW0QIQ.htm](pathfinder-bestiary-3-items/xA0xMv9xbqMW0QIQ.htm)|Tearing Clutch|Tearing Clutch|modificada|
|[xdQJFj57QglkQXeG.htm](pathfinder-bestiary-3-items/xdQJFj57QglkQXeG.htm)|Vicious Criticals|Críticos despiadados|modificada|
|[YBLgLADsWKQWYwXJ.htm](pathfinder-bestiary-3-items/YBLgLADsWKQWYwXJ.htm)|Breath Weapon|Breath Weapon|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0F9P2pX66pZEF0GL.htm](pathfinder-bestiary-3-items/0F9P2pX66pZEF0GL.htm)|Ruins Lore (Applies Only to Their Home Ruins)|vacía|
|[0R9aK9TliKd2z99X.htm](pathfinder-bestiary-3-items/0R9aK9TliKd2z99X.htm)|Bardic Lore|vacía|
|[18UAUscmHJhznFH9.htm](pathfinder-bestiary-3-items/18UAUscmHJhznFH9.htm)|Society|vacía|
|[1CvWXq8Ii6oimfQW.htm](pathfinder-bestiary-3-items/1CvWXq8Ii6oimfQW.htm)|Stealth|vacía|
|[2Cym04NuN3V52Lkm.htm](pathfinder-bestiary-3-items/2Cym04NuN3V52Lkm.htm)|Performance|vacía|
|[2LqNyl4yrY4R0XPU.htm](pathfinder-bestiary-3-items/2LqNyl4yrY4R0XPU.htm)|Forest Lore (applies to the arboreal archive's territory)|vacía|
|[49TebdoZrVR7bJei.htm](pathfinder-bestiary-3-items/49TebdoZrVR7bJei.htm)|Art Lore|vacía|
|[4o2YdJckFmrO6XVf.htm](pathfinder-bestiary-3-items/4o2YdJckFmrO6XVf.htm)|Plane of Fire Lore|vacía|
|[4uqNBCMxlug85vdT.htm](pathfinder-bestiary-3-items/4uqNBCMxlug85vdT.htm)|Stealth|vacía|
|[5h9QlGG2MnMtfSRm.htm](pathfinder-bestiary-3-items/5h9QlGG2MnMtfSRm.htm)|Boneyard Lore|vacía|
|[5xKUaQZJHSM8chyv.htm](pathfinder-bestiary-3-items/5xKUaQZJHSM8chyv.htm)|Intimidation|vacía|
|[6kF88w6XCeKwvBaF.htm](pathfinder-bestiary-3-items/6kF88w6XCeKwvBaF.htm)|Stealth|vacía|
|[6twRlVuFcy8KOm6s.htm](pathfinder-bestiary-3-items/6twRlVuFcy8KOm6s.htm)|Farming Lore|vacía|
|[7Ik8WbV7r4dXszxh.htm](pathfinder-bestiary-3-items/7Ik8WbV7r4dXszxh.htm)|Acrobatics|vacía|
|[81AZ7vz3WmQESZ0c.htm](pathfinder-bestiary-3-items/81AZ7vz3WmQESZ0c.htm)|Rock|vacía|
|[831DXsppKlNomlSq.htm](pathfinder-bestiary-3-items/831DXsppKlNomlSq.htm)|Thievery|vacía|
|[8ZmjmY8SXUuq9HVi.htm](pathfinder-bestiary-3-items/8ZmjmY8SXUuq9HVi.htm)|Dreamlands Lore|vacía|
|[9RLSccx0GSuMZYk2.htm](pathfinder-bestiary-3-items/9RLSccx0GSuMZYk2.htm)|Plane of Air Lore|vacía|
|[9u2lwbCkserddtx7.htm](pathfinder-bestiary-3-items/9u2lwbCkserddtx7.htm)|Lore (any three)|vacía|
|[9xiLnsl12cVesxyN.htm](pathfinder-bestiary-3-items/9xiLnsl12cVesxyN.htm)|Crafting|vacía|
|[a2IB9CvFYzHYpXPK.htm](pathfinder-bestiary-3-items/a2IB9CvFYzHYpXPK.htm)|Athletics|vacía|
|[a9pJTuCtvZCUpnCc.htm](pathfinder-bestiary-3-items/a9pJTuCtvZCUpnCc.htm)|Deception|vacía|
|[Ahyotq9GfFcfraAt.htm](pathfinder-bestiary-3-items/Ahyotq9GfFcfraAt.htm)|Signet Ring|vacía|
|[bJtDTWk5J19kyrIU.htm](pathfinder-bestiary-3-items/bJtDTWk5J19kyrIU.htm)|Dimension of Time Lore|vacía|
|[BlSwWRr9dRFacb9z.htm](pathfinder-bestiary-3-items/BlSwWRr9dRFacb9z.htm)|Warfare Lore|vacía|
|[BvmTCIQKaeHE9MSd.htm](pathfinder-bestiary-3-items/BvmTCIQKaeHE9MSd.htm)|Shadow Plane Lore|vacía|
|[bXIGmBzDzq3zSjQh.htm](pathfinder-bestiary-3-items/bXIGmBzDzq3zSjQh.htm)|Sailing Lore|vacía|
|[BXslGLJsOIB3mptW.htm](pathfinder-bestiary-3-items/BXslGLJsOIB3mptW.htm)|Diplomacy|vacía|
|[C6Fl3KvNV5ml8FIL.htm](pathfinder-bestiary-3-items/C6Fl3KvNV5ml8FIL.htm)|Stealth|vacía|
|[CG0ROboEmdpQKFaJ.htm](pathfinder-bestiary-3-items/CG0ROboEmdpQKFaJ.htm)|Stealth|vacía|
|[cNgD6mqVpe7o2GOL.htm](pathfinder-bestiary-3-items/cNgD6mqVpe7o2GOL.htm)|Dungeon Lore|vacía|
|[Cs1rBxjb2dOVjN5V.htm](pathfinder-bestiary-3-items/Cs1rBxjb2dOVjN5V.htm)|Rock|vacía|
|[cVJFpIMkobfBe9gI.htm](pathfinder-bestiary-3-items/cVJFpIMkobfBe9gI.htm)|Sailing Lore|vacía|
|[daFMLxLznj0FiDbZ.htm](pathfinder-bestiary-3-items/daFMLxLznj0FiDbZ.htm)|Nirvana Lore|vacía|
|[dAiQDFnLDhubSBkJ.htm](pathfinder-bestiary-3-items/dAiQDFnLDhubSBkJ.htm)|Stealth|vacía|
|[dcxy9KeoPzEN2Fpd.htm](pathfinder-bestiary-3-items/dcxy9KeoPzEN2Fpd.htm)|Lore (any one)|vacía|
|[dEqNcyLplxjymRI4.htm](pathfinder-bestiary-3-items/dEqNcyLplxjymRI4.htm)|Athletics|vacía|
|[dJBsPpWCcZqoQuz1.htm](pathfinder-bestiary-3-items/dJBsPpWCcZqoQuz1.htm)|Bardic Lore|vacía|
|[dYaztoeE08mMvpAK.htm](pathfinder-bestiary-3-items/dYaztoeE08mMvpAK.htm)|Dimension of Time Lore|vacía|
|[DyOzh90VV1d6GTKS.htm](pathfinder-bestiary-3-items/DyOzh90VV1d6GTKS.htm)|Dimension of Time Lore|vacía|
|[e6Ag02OjJBtEzB5f.htm](pathfinder-bestiary-3-items/e6Ag02OjJBtEzB5f.htm)|Acrobatics|vacía|
|[e6ZkEbkFGX8LfATR.htm](pathfinder-bestiary-3-items/e6ZkEbkFGX8LfATR.htm)|Rock|vacía|
|[eIrU8U3KMyv0flIO.htm](pathfinder-bestiary-3-items/eIrU8U3KMyv0flIO.htm)|Deception|vacía|
|[eMLzHgxK6gQ67wx6.htm](pathfinder-bestiary-3-items/eMLzHgxK6gQ67wx6.htm)|Household Lore|vacía|
|[erx9pGmKEEP0jCHW.htm](pathfinder-bestiary-3-items/erx9pGmKEEP0jCHW.htm)|Festival Lore|vacía|
|[fCe9kl0ZVqmf44Qi.htm](pathfinder-bestiary-3-items/fCe9kl0ZVqmf44Qi.htm)|Midwifery Lore|vacía|
|[fj2rLzo717u5F5Ya.htm](pathfinder-bestiary-3-items/fj2rLzo717u5F5Ya.htm)|Positive Energy Plane Lore|vacía|
|[fS0jOjwX16xkNJ2N.htm](pathfinder-bestiary-3-items/fS0jOjwX16xkNJ2N.htm)|Pitchfork|vacía|
|[Fx2DEwhdjtpLUXBj.htm](pathfinder-bestiary-3-items/Fx2DEwhdjtpLUXBj.htm)|Sack for Holding Rocks|vacía|
|[gemxvf3zXMNV7oqP.htm](pathfinder-bestiary-3-items/gemxvf3zXMNV7oqP.htm)|Sack for Holding Rocks|vacía|
|[GeuKn3hCssJl4FAm.htm](pathfinder-bestiary-3-items/GeuKn3hCssJl4FAm.htm)|Plane of Earth Lore|vacía|
|[gjjjiArVeBThq31t.htm](pathfinder-bestiary-3-items/gjjjiArVeBThq31t.htm)|Library Lore|vacía|
|[gWmjAFdZNF6c51z1.htm](pathfinder-bestiary-3-items/gWmjAFdZNF6c51z1.htm)|Plane of Fire Lore|vacía|
|[hGcK7acpYirlNAXQ.htm](pathfinder-bestiary-3-items/hGcK7acpYirlNAXQ.htm)|Jewelry Lore|vacía|
|[hjpDAszGRHhxvxyq.htm](pathfinder-bestiary-3-items/hjpDAszGRHhxvxyq.htm)|Legal Lore|vacía|
|[HofMxPk6h7wuflyA.htm](pathfinder-bestiary-3-items/HofMxPk6h7wuflyA.htm)|Stealth|vacía|
|[hR97QpYRCioTid90.htm](pathfinder-bestiary-3-items/hR97QpYRCioTid90.htm)|Lore (Any one Celestial Plane)|vacía|
|[Hu6MV5KT0NEuooVU.htm](pathfinder-bestiary-3-items/Hu6MV5KT0NEuooVU.htm)|Acrobatics|vacía|
|[HZoHUgBTn23rMgPJ.htm](pathfinder-bestiary-3-items/HZoHUgBTn23rMgPJ.htm)|Meteorology Lore|vacía|
|[IbrrhH58fU066gLI.htm](pathfinder-bestiary-3-items/IbrrhH58fU066gLI.htm)|Nirvana Lore|vacía|
|[inTqnHZGNmB3Nv4k.htm](pathfinder-bestiary-3-items/inTqnHZGNmB3Nv4k.htm)|Crafting|vacía|
|[iqOncPekvnZawgEq.htm](pathfinder-bestiary-3-items/iqOncPekvnZawgEq.htm)|Meteorology Lore|vacía|
|[iwCc7o3EXz0xMRTD.htm](pathfinder-bestiary-3-items/iwCc7o3EXz0xMRTD.htm)|Legal Ledgers|vacía|
|[IXlGwryzhB2V9aPV.htm](pathfinder-bestiary-3-items/IXlGwryzhB2V9aPV.htm)|Stealth|vacía|
|[JK8lH0FZWQnphVLZ.htm](pathfinder-bestiary-3-items/JK8lH0FZWQnphVLZ.htm)|Farming Lore|vacía|
|[JVzWcWD2qbxHGbUb.htm](pathfinder-bestiary-3-items/JVzWcWD2qbxHGbUb.htm)|Athletics|vacía|
|[K1MeCVjxSahZAhHQ.htm](pathfinder-bestiary-3-items/K1MeCVjxSahZAhHQ.htm)|Sack for Holding Rocks|vacía|
|[kALPdWJYfV0Cg7Iu.htm](pathfinder-bestiary-3-items/kALPdWJYfV0Cg7Iu.htm)|Toenail Cutter|vacía|
|[Kf0yffdxraBrMmiJ.htm](pathfinder-bestiary-3-items/Kf0yffdxraBrMmiJ.htm)|Acrobatics|vacía|
|[KJRA38LAe9tJK0hJ.htm](pathfinder-bestiary-3-items/KJRA38LAe9tJK0hJ.htm)|Heraldry Lore|vacía|
|[KJxwCEgKTrckplr8.htm](pathfinder-bestiary-3-items/KJxwCEgKTrckplr8.htm)|Hell Lore|vacía|
|[kry7QWW5PONsNRlw.htm](pathfinder-bestiary-3-items/kry7QWW5PONsNRlw.htm)|Stealth|vacía|
|[KZzCxzwU4zROZ7As.htm](pathfinder-bestiary-3-items/KZzCxzwU4zROZ7As.htm)|Engineering Lore|vacía|
|[l7a80DGdIvJp8Pp7.htm](pathfinder-bestiary-3-items/l7a80DGdIvJp8Pp7.htm)|Dwelling Lore|vacía|
|[LK5MP99wRhzbl0Py.htm](pathfinder-bestiary-3-items/LK5MP99wRhzbl0Py.htm)|Settlement Lore|vacía|
|[lo1xAOTNNBx9CRHH.htm](pathfinder-bestiary-3-items/lo1xAOTNNBx9CRHH.htm)|Harp|vacía|
|[LOMkNEBVrbQNC9tu.htm](pathfinder-bestiary-3-items/LOMkNEBVrbQNC9tu.htm)|Acrobatics|vacía|
|[lxK1cONnIVJsnFVA.htm](pathfinder-bestiary-3-items/lxK1cONnIVJsnFVA.htm)|Sandals|vacía|
|[mo4QicMXhbgaEdej.htm](pathfinder-bestiary-3-items/mo4QicMXhbgaEdej.htm)|Stealth|vacía|
|[MPHAiW7bYkK3AAtF.htm](pathfinder-bestiary-3-items/MPHAiW7bYkK3AAtF.htm)|Millinery Lore|vacía|
|[MTzVT8iMpCahyDa9.htm](pathfinder-bestiary-3-items/MTzVT8iMpCahyDa9.htm)|Stealth|vacía|
|[nkZ7Ek2VIoobJ59S.htm](pathfinder-bestiary-3-items/nkZ7Ek2VIoobJ59S.htm)|Cooking Lore|vacía|
|[nLrqfymDMpj0e5mW.htm](pathfinder-bestiary-3-items/nLrqfymDMpj0e5mW.htm)|Stealth|vacía|
|[nOiQxEVLn3tVCv9I.htm](pathfinder-bestiary-3-items/nOiQxEVLn3tVCv9I.htm)|Monastic Lore|vacía|
|[NweZyZVjfaAj87xq.htm](pathfinder-bestiary-3-items/NweZyZVjfaAj87xq.htm)|Nirvana Lore|vacía|
|[OrjyA3TToykWjeyF.htm](pathfinder-bestiary-3-items/OrjyA3TToykWjeyF.htm)|Boneyard Lore|vacía|
|[OW53VBMI2Iw52vm3.htm](pathfinder-bestiary-3-items/OW53VBMI2Iw52vm3.htm)|Nature|vacía|
|[PbrxNFl95AGC5nKu.htm](pathfinder-bestiary-3-items/PbrxNFl95AGC5nKu.htm)|Diplomacy|vacía|
|[pFF77x8PA51YY8TF.htm](pathfinder-bestiary-3-items/pFF77x8PA51YY8TF.htm)|Household Lore|vacía|
|[poFV0K8IzaMCquPb.htm](pathfinder-bestiary-3-items/poFV0K8IzaMCquPb.htm)|Nirvana Lore|vacía|
|[PSRTnwfMOQYpIg4X.htm](pathfinder-bestiary-3-items/PSRTnwfMOQYpIg4X.htm)|Scribing Lore|vacía|
|[qbjGaacQh4FUm7lS.htm](pathfinder-bestiary-3-items/qbjGaacQh4FUm7lS.htm)|Athletics|vacía|
|[qmwnlcLuNKPhcNEX.htm](pathfinder-bestiary-3-items/qmwnlcLuNKPhcNEX.htm)|Undead Lore|vacía|
|[Qq0M2j9jLs4ePYWY.htm](pathfinder-bestiary-3-items/Qq0M2j9jLs4ePYWY.htm)|Athletics|vacía|
|[qqLORgTIh6GxitQS.htm](pathfinder-bestiary-3-items/qqLORgTIh6GxitQS.htm)|Lore (Any One)|vacía|
|[qsBmWCL2GK9QfAiP.htm](pathfinder-bestiary-3-items/qsBmWCL2GK9QfAiP.htm)|Crafting|vacía|
|[Qv1U20ksT1elULUW.htm](pathfinder-bestiary-3-items/Qv1U20ksT1elULUW.htm)|Rock|vacía|
|[Ran96W84fw5Bh3Y3.htm](pathfinder-bestiary-3-items/Ran96W84fw5Bh3Y3.htm)|Manticore Lore|vacía|
|[RscN8Wq0CouXZD3j.htm](pathfinder-bestiary-3-items/RscN8Wq0CouXZD3j.htm)|Plane of Water Lore|vacía|
|[RzulMB9xO8oiQIcf.htm](pathfinder-bestiary-3-items/RzulMB9xO8oiQIcf.htm)|Forest Lore|vacía|
|[sqYKSxNkOZ37Fmz6.htm](pathfinder-bestiary-3-items/sqYKSxNkOZ37Fmz6.htm)|Warfare Lore|vacía|
|[tasMLbX195fSmOL7.htm](pathfinder-bestiary-3-items/tasMLbX195fSmOL7.htm)|Engineering Lore|vacía|
|[tF1tkro4vKNfKEDE.htm](pathfinder-bestiary-3-items/tF1tkro4vKNfKEDE.htm)|Spiritual Rope|vacía|
|[TGEeTa0OpK8ohmMq.htm](pathfinder-bestiary-3-items/TGEeTa0OpK8ohmMq.htm)|Athletics|vacía|
|[tgfeTgJO1gvajbtU.htm](pathfinder-bestiary-3-items/tgfeTgJO1gvajbtU.htm)|Dimension of Time Lore|vacía|
|[tkG1nJtu7gnW9eby.htm](pathfinder-bestiary-3-items/tkG1nJtu7gnW9eby.htm)|Outrageous Hat|vacía|
|[tYt3A9hKDpzLb05E.htm](pathfinder-bestiary-3-items/tYt3A9hKDpzLb05E.htm)|Deception|vacía|
|[u1oQzKxAI1vhe2bM.htm](pathfinder-bestiary-3-items/u1oQzKxAI1vhe2bM.htm)|Confectionery Lore|vacía|
|[uCO7vGNGI7hr4h24.htm](pathfinder-bestiary-3-items/uCO7vGNGI7hr4h24.htm)|Stealth|vacía|
|[UNgk0PBDduIdcbGj.htm](pathfinder-bestiary-3-items/UNgk0PBDduIdcbGj.htm)|Ocean Lore|vacía|
|[UR0KFHIUkZ47JTL0.htm](pathfinder-bestiary-3-items/UR0KFHIUkZ47JTL0.htm)|Athletics|vacía|
|[uUe3OiXyQgpCeszE.htm](pathfinder-bestiary-3-items/uUe3OiXyQgpCeszE.htm)|Lore (Its Home Settlement or Country)|vacía|
|[uxgEQgxtbzCI4zVo.htm](pathfinder-bestiary-3-items/uxgEQgxtbzCI4zVo.htm)|Athletics|vacía|
|[Uy3YAPSoQX8UsHK3.htm](pathfinder-bestiary-3-items/Uy3YAPSoQX8UsHK3.htm)|Athletics|vacía|
|[V3rzyrOOf8B6y3I2.htm](pathfinder-bestiary-3-items/V3rzyrOOf8B6y3I2.htm)|Silver Scissors|vacía|
|[vewX6T3r92AKkxif.htm](pathfinder-bestiary-3-items/vewX6T3r92AKkxif.htm)|Dark Tapestry Lore|vacía|
|[vgGl8uaNNQbavIkC.htm](pathfinder-bestiary-3-items/vgGl8uaNNQbavIkC.htm)|Stealth|vacía|
|[VZit0bc3TQKyAAbe.htm](pathfinder-bestiary-3-items/VZit0bc3TQKyAAbe.htm)|Shadow Plane Lore|vacía|
|[WavB32uYsyFzI2uA.htm](pathfinder-bestiary-3-items/WavB32uYsyFzI2uA.htm)|Performance|vacía|
|[WDHTK76R7cwm5ZSc.htm](pathfinder-bestiary-3-items/WDHTK76R7cwm5ZSc.htm)|Intimidation|vacía|
|[WdP7r88rTRk6Y9Lu.htm](pathfinder-bestiary-3-items/WdP7r88rTRk6Y9Lu.htm)|Positive Energy Plane Lore|vacía|
|[wwKhJGtC2Ah8Opom.htm](pathfinder-bestiary-3-items/wwKhJGtC2Ah8Opom.htm)|Satchel for Holding Rocks|vacía|
|[xdDVdaosFR2v4kpi.htm](pathfinder-bestiary-3-items/xdDVdaosFR2v4kpi.htm)|Black Onyx Gems|vacía|
|[xFzHJGL72aPpRLgB.htm](pathfinder-bestiary-3-items/xFzHJGL72aPpRLgB.htm)|Lore (Any One Subcategory)|vacía|
|[xGCY0XLJysaFyIyL.htm](pathfinder-bestiary-3-items/xGCY0XLJysaFyIyL.htm)|Labor Lore|vacía|
|[xKAa1bclabyOkPCJ.htm](pathfinder-bestiary-3-items/xKAa1bclabyOkPCJ.htm)|Pliers|vacía|
|[xLpHFUCeWtRMaSOY.htm](pathfinder-bestiary-3-items/xLpHFUCeWtRMaSOY.htm)|Crafting|vacía|
|[XzaWmxiliLGXDQy7.htm](pathfinder-bestiary-3-items/XzaWmxiliLGXDQy7.htm)|Lore (Associated with the Guardian's Need)|vacía|
|[y5xPl9xFyELDGekP.htm](pathfinder-bestiary-3-items/y5xPl9xFyELDGekP.htm)|Desert Lore|vacía|
|[ycvxnNDxb7l6WNHB.htm](pathfinder-bestiary-3-items/ycvxnNDxb7l6WNHB.htm)|Athletics|vacía|
|[YHH8WnAJViHJAQnW.htm](pathfinder-bestiary-3-items/YHH8WnAJViHJAQnW.htm)|Household Lore|vacía|
|[yUtAKhKRZtc1R8LE.htm](pathfinder-bestiary-3-items/yUtAKhKRZtc1R8LE.htm)|Athletics|vacía|
|[YwaUnI2GQySpg6fO.htm](pathfinder-bestiary-3-items/YwaUnI2GQySpg6fO.htm)|Stealth|vacía|
|[Z67Fh0402PQRc5fp.htm](pathfinder-bestiary-3-items/Z67Fh0402PQRc5fp.htm)|Weather Lore|vacía|
|[Zh8VyOJVStjNfjec.htm](pathfinder-bestiary-3-items/Zh8VyOJVStjNfjec.htm)|Acrobatics|vacía|
|[zTJl5YEwlSQQwXu1.htm](pathfinder-bestiary-3-items/zTJl5YEwlSQQwXu1.htm)|Stealth|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
