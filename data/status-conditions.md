# Estado de la traducción (conditions)

 * **oficial**: 41
 * **modificada**: 1


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[VRSef5y1LmL2Hkjf.htm](conditions/VRSef5y1LmL2Hkjf.htm)|Undetected|No detectado|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[1wQY3JYyhMYeeV2G.htm](conditions/1wQY3JYyhMYeeV2G.htm)|Observed|Observado|oficial|
|[3uh1r86TzbQvosxv.htm](conditions/3uh1r86TzbQvosxv.htm)|Doomed|Condenado|oficial|
|[4D2KBtexWXa6oUMR.htm](conditions/4D2KBtexWXa6oUMR.htm)|Drained|Drenado|oficial|
|[6dNUvdb1dhToNDj3.htm](conditions/6dNUvdb1dhToNDj3.htm)|Broken|Roto|oficial|
|[6uEgoh53GbXuHpTF.htm](conditions/6uEgoh53GbXuHpTF.htm)|Paralyzed|Paralizado|oficial|
|[9evPzg9E6muFcoSk.htm](conditions/9evPzg9E6muFcoSk.htm)|Unnoticed|Inadvertido|oficial|
|[9PR9y0bi4JPKnHPR.htm](conditions/9PR9y0bi4JPKnHPR.htm)|Deafened|Ensordecido|oficial|
|[9qGBRpbX9NEwtAAr.htm](conditions/9qGBRpbX9NEwtAAr.htm)|Controlled|Controlado|oficial|
|[AdPVz7rbaVSRxHFg.htm](conditions/AdPVz7rbaVSRxHFg.htm)|Fascinated|Fascinado|oficial|
|[AJh5ex99aV6VTggg.htm](conditions/AJh5ex99aV6VTggg.htm)|Flat-Footed|Desprevenido|oficial|
|[D5mg6Tc7Jzrj6ro7.htm](conditions/D5mg6Tc7Jzrj6ro7.htm)|Encumbered|Impedido|oficial|
|[dfCMdR4wnpbYNTix.htm](conditions/dfCMdR4wnpbYNTix.htm)|Stunned|Aturdido|oficial|
|[DmAIPqOBomZ7H95W.htm](conditions/DmAIPqOBomZ7H95W.htm)|Concealed|Oculto|oficial|
|[dTwPJuKgBQCMxixg.htm](conditions/dTwPJuKgBQCMxixg.htm)|Petrified|Petrificado|oficial|
|[e1XGnhKNSQIm5IXg.htm](conditions/e1XGnhKNSQIm5IXg.htm)|Stupefied|Anonadado|oficial|
|[eIcWbB5o3pP6OIMe.htm](conditions/eIcWbB5o3pP6OIMe.htm)|Immobilized|Inmovilizado|oficial|
|[fBnFDH2MTzgFijKf.htm](conditions/fBnFDH2MTzgFijKf.htm)|Unconscious|Inconsciente|oficial|
|[fesd1n5eVhpCSS18.htm](conditions/fesd1n5eVhpCSS18.htm)|Sickened|Indispuesto|oficial|
|[fuG8dgthlDWfWjIA.htm](conditions/fuG8dgthlDWfWjIA.htm)|Indifferent|Indiferente|oficial|
|[HL2l2VRSaQHu9lUw.htm](conditions/HL2l2VRSaQHu9lUw.htm)|Fatigued|Fatigado|oficial|
|[I1ffBVISxLr2gC4u.htm](conditions/I1ffBVISxLr2gC4u.htm)|Unfriendly|Hostil|oficial|
|[i3OJZU2nk64Df3xm.htm](conditions/i3OJZU2nk64Df3xm.htm)|Clumsy|Torpe|oficial|
|[iU0fEDdBp3rXpTMC.htm](conditions/iU0fEDdBp3rXpTMC.htm)|Hidden|Escondido|oficial|
|[j91X7x0XSomq8d60.htm](conditions/j91X7x0XSomq8d60.htm)|Prone|Tumbado|oficial|
|[kWc1fhmv9LBiTuei.htm](conditions/kWc1fhmv9LBiTuei.htm)|Grabbed|Agarrado|oficial|
|[lDVqvLKA6eF3Df60.htm](conditions/lDVqvLKA6eF3Df60.htm)|Persistent Damage|Daño Persistente|oficial|
|[MIRkyAjyBeXivMa7.htm](conditions/MIRkyAjyBeXivMa7.htm)|Enfeebled|Debilitado|oficial|
|[nlCjDvLMf2EkV2dl.htm](conditions/nlCjDvLMf2EkV2dl.htm)|Quickened|Acelerado|oficial|
|[sDPxOjQ9kx2RZE8D.htm](conditions/sDPxOjQ9kx2RZE8D.htm)|Fleeing|Huyendo|oficial|
|[TBSHQspnbcqxsmjL.htm](conditions/TBSHQspnbcqxsmjL.htm)|Frightened|Asustado|oficial|
|[TkIyaNPgTZFBCCuh.htm](conditions/TkIyaNPgTZFBCCuh.htm)|Dazzled|Deslumbrado|oficial|
|[ud7gTLwPeklzYSXG.htm](conditions/ud7gTLwPeklzYSXG.htm)|Hostile|Hostil|oficial|
|[v44P3WUcU1j0115l.htm](conditions/v44P3WUcU1j0115l.htm)|Helpful|Solícito|oficial|
|[v66R7FdOf11l94im.htm](conditions/v66R7FdOf11l94im.htm)|Friendly|Amistoso|oficial|
|[VcDeM8A5oI6VqhbM.htm](conditions/VcDeM8A5oI6VqhbM.htm)|Restrained|Neutralizado|oficial|
|[XgEqL1kFApUbl5Z2.htm](conditions/XgEqL1kFApUbl5Z2.htm)|Blinded|Cegado|oficial|
|[xYTAsEpcJE1Ccni3.htm](conditions/xYTAsEpcJE1Ccni3.htm)|Slowed|Lentificado|oficial|
|[yblD8fOR1J8rDwEQ.htm](conditions/yblD8fOR1J8rDwEQ.htm)|Confused|Confuso|oficial|
|[Yl48xTdMh3aeQYL2.htm](conditions/Yl48xTdMh3aeQYL2.htm)|Wounded|Herido|oficial|
|[yZRUzMqrMmfLu0V1.htm](conditions/yZRUzMqrMmfLu0V1.htm)|Dying|Moribundo|oficial|
|[zJxUflt9np0q4yML.htm](conditions/zJxUflt9np0q4yML.htm)|Invisible|Invisible|oficial|
