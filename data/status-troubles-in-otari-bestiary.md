# Estado de la traducción (troubles-in-otari-bestiary)

 * **auto-trad**: 18


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0Fvulq5Zv4BQPvYV.htm](troubles-in-otari-bestiary/0Fvulq5Zv4BQPvYV.htm)|Collapsing Porch|auto-trad|
|[3Pa5JoY9PYdV5x1e.htm](troubles-in-otari-bestiary/3Pa5JoY9PYdV5x1e.htm)|Brimstone Rat|auto-trad|
|[4GZsTkvuYADpcdmV.htm](troubles-in-otari-bestiary/4GZsTkvuYADpcdmV.htm)|Kurnugian Jackal|auto-trad|
|[5iOgKbhW9zVnV1cr.htm](troubles-in-otari-bestiary/5iOgKbhW9zVnV1cr.htm)|Bugbear Marauder|auto-trad|
|[6Ut7YjjLZADMU7NY.htm](troubles-in-otari-bestiary/6Ut7YjjLZADMU7NY.htm)|Hargrit Leadbuster|auto-trad|
|[aqDFxmLMx6cOnGlx.htm](troubles-in-otari-bestiary/aqDFxmLMx6cOnGlx.htm)|Tongues of Flame|auto-trad|
|[b8Wgun7T7sh7NVrd.htm](troubles-in-otari-bestiary/b8Wgun7T7sh7NVrd.htm)|Web Lurker Noose|auto-trad|
|[fhItdltCmcAZkvcI.htm](troubles-in-otari-bestiary/fhItdltCmcAZkvcI.htm)|Kobold Trapmaster|auto-trad|
|[hv50ovLKV6lu49dF.htm](troubles-in-otari-bestiary/hv50ovLKV6lu49dF.htm)|Nightmare Terrain|auto-trad|
|[l7wng1EXY7np8Hcu.htm](troubles-in-otari-bestiary/l7wng1EXY7np8Hcu.htm)|Scalliwing|auto-trad|
|[lTVgdXuSv2B0yVVW.htm](troubles-in-otari-bestiary/lTVgdXuSv2B0yVVW.htm)|Morgrym Leadbuster|auto-trad|
|[m4jitI3J1QkjJq3H.htm](troubles-in-otari-bestiary/m4jitI3J1QkjJq3H.htm)|Omblin Leadbuster|auto-trad|
|[MTI298bhLGYktyZX.htm](troubles-in-otari-bestiary/MTI298bhLGYktyZX.htm)|Orc Scrapper|auto-trad|
|[NeFSJTGROSgGIKNd.htm](troubles-in-otari-bestiary/NeFSJTGROSgGIKNd.htm)|Viper Urn|auto-trad|
|[PhLxqnAIhCovfcXy.htm](troubles-in-otari-bestiary/PhLxqnAIhCovfcXy.htm)|Kotgar Leadbuster|auto-trad|
|[rRSIqZ5g3QRfaAQ0.htm](troubles-in-otari-bestiary/rRSIqZ5g3QRfaAQ0.htm)|Orc Commander|auto-trad|
|[TIX9yDasefwJ4PxI.htm](troubles-in-otari-bestiary/TIX9yDasefwJ4PxI.htm)|Stinkweed Shambler|auto-trad|
|[vg6fEjWSj4jilXRn.htm](troubles-in-otari-bestiary/vg6fEjWSj4jilXRn.htm)|Magic Starknives Trap|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
