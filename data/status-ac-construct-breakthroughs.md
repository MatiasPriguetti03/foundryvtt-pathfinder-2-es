# Estado de la traducción (ac-construct-breakthroughs)

 * **auto-trad**: 18


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[1mE83uSucJZS4FB9.htm](ac-construct-breakthroughs/1mE83uSucJZS4FB9.htm)|Runic Keystone|auto-trad|
|[3Q7nASFamnMm7cBz.htm](ac-construct-breakthroughs/3Q7nASFamnMm7cBz.htm)|Turret Configuration|auto-trad|
|[A3UROfr7qu1iWNKy.htm](ac-construct-breakthroughs/A3UROfr7qu1iWNKy.htm)|Manual Dexterity|auto-trad|
|[BR7Bu48ijB4Jzgci.htm](ac-construct-breakthroughs/BR7Bu48ijB4Jzgci.htm)|Marvelous Gears|auto-trad|
|[CugdA3rsjW1hWt9O.htm](ac-construct-breakthroughs/CugdA3rsjW1hWt9O.htm)|Flight Chassis|auto-trad|
|[CxT9zcqn7LV2eMHZ.htm](ac-construct-breakthroughs/CxT9zcqn7LV2eMHZ.htm)|Accelerated Mobility|auto-trad|
|[cYu2Rf6SiyAhtsr2.htm](ac-construct-breakthroughs/cYu2Rf6SiyAhtsr2.htm)|Antimagic Construction|auto-trad|
|[djv1KiWWTOajTsCk.htm](ac-construct-breakthroughs/djv1KiWWTOajTsCk.htm)|Advanced Weaponry|auto-trad|
|[gL9ywotxNnoP1rOp.htm](ac-construct-breakthroughs/gL9ywotxNnoP1rOp.htm)|Durable Construction|auto-trad|
|[Jl0HsbQh05ajHZAh.htm](ac-construct-breakthroughs/Jl0HsbQh05ajHZAh.htm)|Climbing Limbs|auto-trad|
|[lg4K3gO0VtIawWq0.htm](ac-construct-breakthroughs/lg4K3gO0VtIawWq0.htm)|Increased Size|auto-trad|
|[O6kII65D6nqbbHtO.htm](ac-construct-breakthroughs/O6kII65D6nqbbHtO.htm)|Amphibious Construct|auto-trad|
|[owiurEqGXM726Iwd.htm](ac-construct-breakthroughs/owiurEqGXM726Iwd.htm)|Sensory Array|auto-trad|
|[phxQm79wotHHIGUg.htm](ac-construct-breakthroughs/phxQm79wotHHIGUg.htm)|Resistant Coating|auto-trad|
|[qRnfaGuJ1k0Agm35.htm](ac-construct-breakthroughs/qRnfaGuJ1k0Agm35.htm)|Miracle Gears|auto-trad|
|[TS2DXi5f4y7wtED5.htm](ac-construct-breakthroughs/TS2DXi5f4y7wtED5.htm)|Wall Configuration|auto-trad|
|[Z2GWP5gsbS1SsP1W.htm](ac-construct-breakthroughs/Z2GWP5gsbS1SsP1W.htm)|Wonder Gears|auto-trad|
|[zo6B3JGQIt99K1HF.htm](ac-construct-breakthroughs/zo6B3JGQIt99K1HF.htm)|Projectile Launcher|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
