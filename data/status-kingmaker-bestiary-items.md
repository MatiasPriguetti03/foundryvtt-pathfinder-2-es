# Estado de la traducción (kingmaker-bestiary-items)

 * **auto-trad**: 1339
 * **ninguna**: 224
 * **vacía**: 111
 * **modificada**: 4


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[0bBCzMxj06H1x2EI.htm](kingmaker-bestiary-items/0bBCzMxj06H1x2EI.htm)|Composite Longbow|+1,striking|
|[0bbJhjr4XkyXm2cv.htm](kingmaker-bestiary-items/0bbJhjr4XkyXm2cv.htm)|Mistform Elixir (Greater) (Infused)|
|[0bQTICfElBoz3XPf.htm](kingmaker-bestiary-items/0bQTICfElBoz3XPf.htm)|+1 Breastplate|
|[0NDexqBHwunzi360.htm](kingmaker-bestiary-items/0NDexqBHwunzi360.htm)|Dagger|+2,greaterStriking|
|[0qWcKzKBUEwvLvdH.htm](kingmaker-bestiary-items/0qWcKzKBUEwvLvdH.htm)|Pipes|
|[0rmDD9ymGXaLDR7A.htm](kingmaker-bestiary-items/0rmDD9ymGXaLDR7A.htm)|Longsword|+1|
|[0ShB5n7PzRHdtWVF.htm](kingmaker-bestiary-items/0ShB5n7PzRHdtWVF.htm)|Primal Call (Creatures with the Wild Hunt Trait Only)|
|[1bZpLXmtzfGcJX8S.htm](kingmaker-bestiary-items/1bZpLXmtzfGcJX8S.htm)|+2 Resilient Studded Leather Armor|
|[22UiypLQhIBbjWDe.htm](kingmaker-bestiary-items/22UiypLQhIBbjWDe.htm)|Religious Text of Saranrae|
|[2cPfluVOMi5nDGgx.htm](kingmaker-bestiary-items/2cPfluVOMi5nDGgx.htm)|Trident|+1,striking|
|[2jUfYxc6d6U4w3Z7.htm](kingmaker-bestiary-items/2jUfYxc6d6U4w3Z7.htm)|Dimension Door (At Will) (To any location in Armag's Tomb)|
|[2xC1UooYAPVhcemO.htm](kingmaker-bestiary-items/2xC1UooYAPVhcemO.htm)|Religious Symbol of Gogunta (Silver)|
|[3Oyvp6SK64TaOXVq.htm](kingmaker-bestiary-items/3Oyvp6SK64TaOXVq.htm)|Bastard Sword|+2,striking,wounding|
|[3WcqrLbeRK1eZWeJ.htm](kingmaker-bestiary-items/3WcqrLbeRK1eZWeJ.htm)|+1 Leather Armor|
|[432AQ0jbevEtWKdG.htm](kingmaker-bestiary-items/432AQ0jbevEtWKdG.htm)|Formula Book|
|[4zR81aivcsDVQxAp.htm](kingmaker-bestiary-items/4zR81aivcsDVQxAp.htm)|Whip|+1,striking|
|[57nrcKoJBLBDALst.htm](kingmaker-bestiary-items/57nrcKoJBLBDALst.htm)|Living Bow|
|[5juLpMpruoLCJ9JZ.htm](kingmaker-bestiary-items/5juLpMpruoLCJ9JZ.htm)|Hatchet|+2,striking|
|[5mJxaR7sQGjBWkRc.htm](kingmaker-bestiary-items/5mJxaR7sQGjBWkRc.htm)|+2 Resilient Leather Armor|
|[5Tlw4BDzoIJ4STyR.htm](kingmaker-bestiary-items/5Tlw4BDzoIJ4STyR.htm)|Battered Breastplate|
|[67Qch1f76P2Ze8KM.htm](kingmaker-bestiary-items/67Qch1f76P2Ze8KM.htm)|Religious Symbol of Gyronna (Silver)|
|[6jXk3sASWaQvOH5n.htm](kingmaker-bestiary-items/6jXk3sASWaQvOH5n.htm)|Dagger|+1|
|[6TCoYo76CHcPURpz.htm](kingmaker-bestiary-items/6TCoYo76CHcPURpz.htm)|Resilient Splint Mail|
|[76TiztNcrVVUhZQl.htm](kingmaker-bestiary-items/76TiztNcrVVUhZQl.htm)|Speak with Animals (Constant)|
|[7bd1h5tOi35bvK7Q.htm](kingmaker-bestiary-items/7bd1h5tOi35bvK7Q.htm)|Wand of Shocking Grasp (Level 2)|
|[7N8Klhs5wpgJ6RvM.htm](kingmaker-bestiary-items/7N8Klhs5wpgJ6RvM.htm)|Hunting Horn|
|[7PNhfO9yF8jlxc97.htm](kingmaker-bestiary-items/7PNhfO9yF8jlxc97.htm)|Rapier|+1|
|[88uOslPj6NpTUQNF.htm](kingmaker-bestiary-items/88uOslPj6NpTUQNF.htm)|Shortsword|+1,striking|
|[97i0qo04YJEOlBSn.htm](kingmaker-bestiary-items/97i0qo04YJEOlBSn.htm)|Rare Candles and Incense|
|[9RNGJOLNS21hY10V.htm](kingmaker-bestiary-items/9RNGJOLNS21hY10V.htm)|Falchion|+1,striking|
|[9TeqIJRQyjU0d2eg.htm](kingmaker-bestiary-items/9TeqIJRQyjU0d2eg.htm)|+2 Greater Fire Resistant Greater Resilient Leather Armor|
|[9xLtvFQAoJh8tSOd.htm](kingmaker-bestiary-items/9xLtvFQAoJh8tSOd.htm)|Staff of Power|+2,greaterStriking|
|[9XpL2lVavNQF91Zx.htm](kingmaker-bestiary-items/9XpL2lVavNQF91Zx.htm)|True Seeing (Constant)|
|[abr9bhWIMzRT7FHN.htm](kingmaker-bestiary-items/abr9bhWIMzRT7FHN.htm)|Grisly Scythe|+1,striking,wounding|
|[Aeu3hLK3x9HeXzhD.htm](kingmaker-bestiary-items/Aeu3hLK3x9HeXzhD.htm)|Darkness (At Will)|
|[AFXLJNAW0dhc0PkH.htm](kingmaker-bestiary-items/AFXLJNAW0dhc0PkH.htm)|Wineskin|
|[AfZgCzOcPYkL5eV2.htm](kingmaker-bestiary-items/AfZgCzOcPYkL5eV2.htm)|Kukri|+1|
|[aGPW7GAY6qDJQHh8.htm](kingmaker-bestiary-items/aGPW7GAY6qDJQHh8.htm)|Ring of Energy Resistance (Major) (Fire)|
|[ap4RPRdJx6CtkxmL.htm](kingmaker-bestiary-items/ap4RPRdJx6CtkxmL.htm)|Ovinrbaane|+2,striking,wounding|
|[Aq3vYXMGDf6pFo4D.htm](kingmaker-bestiary-items/Aq3vYXMGDf6pFo4D.htm)|Plane Shift (to or from the First World only)|
|[b2b4en2ylHZagF2u.htm](kingmaker-bestiary-items/b2b4en2ylHZagF2u.htm)|Wand of Burning Hands (Level 1)|
|[B2sQYrMGR7VibDvV.htm](kingmaker-bestiary-items/B2sQYrMGR7VibDvV.htm)|Dagger|+1,striking,silver|
|[b5j9aIeq7ERjg1cn.htm](kingmaker-bestiary-items/b5j9aIeq7ERjg1cn.htm)|Religious Symbol of Gozreh (Wooden and Bloodstained)|
|[BDyM0mqNVmkzGKe2.htm](kingmaker-bestiary-items/BDyM0mqNVmkzGKe2.htm)|Carpenter's Tools|
|[BvsSj4ZTgepDO6JN.htm](kingmaker-bestiary-items/BvsSj4ZTgepDO6JN.htm)|Battle Axe|+1,striking|
|[BXDlaMPWYXlB5Few.htm](kingmaker-bestiary-items/BXDlaMPWYXlB5Few.htm)|+1 Full Plate|
|[C3EpruyM244f0JVf.htm](kingmaker-bestiary-items/C3EpruyM244f0JVf.htm)|Greatsword|+2,striking|
|[c3NPgUPsH0WkQZXG.htm](kingmaker-bestiary-items/c3NPgUPsH0WkQZXG.htm)|Composite Shortbow|+1,striking|
|[CC2IGMZw3wpLt6Av.htm](kingmaker-bestiary-items/CC2IGMZw3wpLt6Av.htm)|Composite Longbow|+1|
|[cG10UmeLEy9eANOc.htm](kingmaker-bestiary-items/cG10UmeLEy9eANOc.htm)|Acid Flask (Greater) (Infused)|
|[cIFXkQ4Qhht73I7F.htm](kingmaker-bestiary-items/cIFXkQ4Qhht73I7F.htm)|Longsword|+1,striking|
|[CLhR4qg5HhAxRnYA.htm](kingmaker-bestiary-items/CLhR4qg5HhAxRnYA.htm)|Suggestion (At Will)|
|[cOFKoOU0dk3bhJeX.htm](kingmaker-bestiary-items/cOFKoOU0dk3bhJeX.htm)|Religious Symbol of Calistra (Silver)|
|[crdKpfEf0xqRNZES.htm](kingmaker-bestiary-items/crdKpfEf0xqRNZES.htm)|Greataxe|+2,striking|
|[CtzLyJ8UMGgt86Xw.htm](kingmaker-bestiary-items/CtzLyJ8UMGgt86Xw.htm)|+2 Greater Resilient Mithral Chain Mail|
|[CX6zUhlF06g9iSjB.htm](kingmaker-bestiary-items/CX6zUhlF06g9iSjB.htm)|Wand of Invisibility (Level 2)|
|[D2qG6bBAwV0Xu4iO.htm](kingmaker-bestiary-items/D2qG6bBAwV0Xu4iO.htm)|+1 Full Plate|
|[D8DPsCfECCJBxsBs.htm](kingmaker-bestiary-items/D8DPsCfECCJBxsBs.htm)|Speak with Plants (Constant)|
|[d8jlUApm6erhQTpM.htm](kingmaker-bestiary-items/d8jlUApm6erhQTpM.htm)|Whip|+1|
|[dBd9trOqG8wBEYZ6.htm](kingmaker-bestiary-items/dBd9trOqG8wBEYZ6.htm)|+1 Resilient Hide Armor|
|[dEj5HxpnzeRCKekU.htm](kingmaker-bestiary-items/dEj5HxpnzeRCKekU.htm)|Wand of Glitterdust (Level 2)|
|[DN5RQpUoYCp3qWou.htm](kingmaker-bestiary-items/DN5RQpUoYCp3qWou.htm)|Dagger|+1|
|[EH6c2qLG9oWj3bcW.htm](kingmaker-bestiary-items/EH6c2qLG9oWj3bcW.htm)|Greataxe|+2,striking|
|[f0ReT77CQpNnH0wr.htm](kingmaker-bestiary-items/f0ReT77CQpNnH0wr.htm)|+2 Resilient Full Plate|
|[f1KaRpZo7GoKPttu.htm](kingmaker-bestiary-items/f1KaRpZo7GoKPttu.htm)|Wand of Magic Missile (Level 1)|
|[F6NTNYwrbXvVsHwE.htm](kingmaker-bestiary-items/F6NTNYwrbXvVsHwE.htm)|Robes|
|[FEvGGdluzakeGdBF.htm](kingmaker-bestiary-items/FEvGGdluzakeGdBF.htm)|+2 Greater Resilient Clothing (Explorer's)|
|[FjxVn3oLSCrfIIIz.htm](kingmaker-bestiary-items/FjxVn3oLSCrfIIIz.htm)|Religious Symbol of Yog-Sothoth|
|[FoPN2th3v5NxtKds.htm](kingmaker-bestiary-items/FoPN2th3v5NxtKds.htm)|Maul|+1|
|[FS1iX2ShhtCPk1Wr.htm](kingmaker-bestiary-items/FS1iX2ShhtCPk1Wr.htm)|Shortsword|+1|
|[FZZwIPdH86woV8uz.htm](kingmaker-bestiary-items/FZZwIPdH86woV8uz.htm)|+1 Chain Mail|
|[g9hAlndNzXTVzNDu.htm](kingmaker-bestiary-items/g9hAlndNzXTVzNDu.htm)|Basic Woodworker's Book|
|[gEF9138FUjhpAL9X.htm](kingmaker-bestiary-items/gEF9138FUjhpAL9X.htm)|True Seeing (Constant)|
|[Gk2AECC4rYPqsJbg.htm](kingmaker-bestiary-items/Gk2AECC4rYPqsJbg.htm)|Restore Senses (At Will)|
|[GLcjMCpmzoaId5jx.htm](kingmaker-bestiary-items/GLcjMCpmzoaId5jx.htm)|Imprisonment (Cannot Currently Cast)|
|[gsd3hcxt7GSe5WzZ.htm](kingmaker-bestiary-items/gsd3hcxt7GSe5WzZ.htm)|Aldori Dueling Sword|+1|
|[gVhcOspe8h7FkmF1.htm](kingmaker-bestiary-items/gVhcOspe8h7FkmF1.htm)|Robes|
|[gvKFoNQurhUZnlWh.htm](kingmaker-bestiary-items/gvKFoNQurhUZnlWh.htm)|Ring of Energy Resistance (Fire)|
|[gymUtf0OxE6gzjqN.htm](kingmaker-bestiary-items/gymUtf0OxE6gzjqN.htm)|+1 Glamered Chain Shirt|
|[hCCNqP3969tyxk96.htm](kingmaker-bestiary-items/hCCNqP3969tyxk96.htm)|+2 Greater Striking Spiked Greatclub|+2,greaterStriking|
|[hdEvwgLsMiLlJbMm.htm](kingmaker-bestiary-items/hdEvwgLsMiLlJbMm.htm)|+2 Greater Resilient Hide Armor|
|[hjfvtpOl2IljcOOH.htm](kingmaker-bestiary-items/hjfvtpOl2IljcOOH.htm)|Outcast's Curse (At Will)|
|[HplaolUyjv0ldmah.htm](kingmaker-bestiary-items/HplaolUyjv0ldmah.htm)|Eagle Eye Elixir (Major) (Infused)|
|[HsvS5P4EDcjC5Y5A.htm](kingmaker-bestiary-items/HsvS5P4EDcjC5Y5A.htm)|Outcast's Curse (At Will)|
|[hvCc4reISEOqsFIa.htm](kingmaker-bestiary-items/hvCc4reISEOqsFIa.htm)|Darkness (At Will)|
|[HvSxWkSSm2CCuJHu.htm](kingmaker-bestiary-items/HvSxWkSSm2CCuJHu.htm)|+1 Cold-Resistant Leather Armor|
|[hXe1xaYFIHJ8f7tw.htm](kingmaker-bestiary-items/hXe1xaYFIHJ8f7tw.htm)|Trident|+1,striking|
|[Hyozh14mnCpe6YVy.htm](kingmaker-bestiary-items/Hyozh14mnCpe6YVy.htm)|Shortsword|+1,striking|
|[I9tYnS5FyCSmLBYY.htm](kingmaker-bestiary-items/I9tYnS5FyCSmLBYY.htm)|Tree Shape (At Will)|
|[iHz93nPS055VlpTQ.htm](kingmaker-bestiary-items/iHz93nPS055VlpTQ.htm)|+1 Crossbow|+1,striking|
|[IJyUjXFsO9M8LpUs.htm](kingmaker-bestiary-items/IJyUjXFsO9M8LpUs.htm)|Elegant Robes|
|[Ik3rJsTnSTRAi671.htm](kingmaker-bestiary-items/Ik3rJsTnSTRAi671.htm)|Invisibility (Self Only)|
|[IOZpqdcbetYaemZ3.htm](kingmaker-bestiary-items/IOZpqdcbetYaemZ3.htm)|Monstrous Bloom|
|[j8PYlRTNy6uxScHw.htm](kingmaker-bestiary-items/j8PYlRTNy6uxScHw.htm)|Wand of Dimension Door (Level 4)|
|[JHBWAs0dwbsbDu7J.htm](kingmaker-bestiary-items/JHBWAs0dwbsbDu7J.htm)|+2 Greater Resilient Full Plate|
|[jkJ5X9sX7YU7UA2F.htm](kingmaker-bestiary-items/jkJ5X9sX7YU7UA2F.htm)|+1 Resilient Breastplate|
|[JlGJE2TMv5okfuks.htm](kingmaker-bestiary-items/JlGJE2TMv5okfuks.htm)|Dagger|+1,greaterStriking,greaterFrost|
|[JloIn4LDx9M1fiyw.htm](kingmaker-bestiary-items/JloIn4LDx9M1fiyw.htm)|Eclipsed Incantations|
|[JRCxllIuf9pbk0QY.htm](kingmaker-bestiary-items/JRCxllIuf9pbk0QY.htm)|Kukri|+2,greaterStriking,keen|
|[k8GjK7psOKSAVUhr.htm](kingmaker-bestiary-items/k8GjK7psOKSAVUhr.htm)|Prismatic Spray (At Will)|
|[kEk8UpJ0Ow9EnnTr.htm](kingmaker-bestiary-items/kEk8UpJ0Ow9EnnTr.htm)|Warhammer|+2,greaterFrost|
|[kgHoUAo23IIBJKPK.htm](kingmaker-bestiary-items/kgHoUAo23IIBJKPK.htm)|+1 Striking Staff of Fire (Major)|+1,striking|
|[kTPMsO2Nauv9vM6Y.htm](kingmaker-bestiary-items/kTPMsO2Nauv9vM6Y.htm)|Speak with Plants (Constant)|
|[kVgdUKwhGxbwACnL.htm](kingmaker-bestiary-items/kVgdUKwhGxbwACnL.htm)|Hunting Spider Venom (On Darts)|
|[kXmoXkc5myLgiFoy.htm](kingmaker-bestiary-items/kXmoXkc5myLgiFoy.htm)|Mind Probe (At Will)|
|[l47LUxjBXwp06JZ8.htm](kingmaker-bestiary-items/l47LUxjBXwp06JZ8.htm)|Glaive|+1|
|[L9sxOR3SnpDYfZHq.htm](kingmaker-bestiary-items/L9sxOR3SnpDYfZHq.htm)|Tree Stride (Self plus Willing Rider)|
|[lH0kmQP6fJWkOqZ5.htm](kingmaker-bestiary-items/lH0kmQP6fJWkOqZ5.htm)|+2 Resilient White Dragonhide Breastplate|
|[lo9zjzoOZSRs5svZ.htm](kingmaker-bestiary-items/lo9zjzoOZSRs5svZ.htm)|Darkness (At Will)|
|[lta8LCkZJo5xDrQ5.htm](kingmaker-bestiary-items/lta8LCkZJo5xDrQ5.htm)|+1 Resilient Full Plate|
|[mcvnk6mFrYyK92Mb.htm](kingmaker-bestiary-items/mcvnk6mFrYyK92Mb.htm)|Morningstar|+1,darkwood,thundering|
|[MeD83DvPWfOBUylm.htm](kingmaker-bestiary-items/MeD83DvPWfOBUylm.htm)|Dagger|+2,greaterStriking,spellStoring|
|[mGvIyNKk0KBZu3BK.htm](kingmaker-bestiary-items/mGvIyNKk0KBZu3BK.htm)|Aldori Dueling Sword|+2,striking|
|[MHLJGTn6eQAmWIUX.htm](kingmaker-bestiary-items/MHLJGTn6eQAmWIUX.htm)|True Seeing (Constant)|
|[MHRiahLowofy8ek9.htm](kingmaker-bestiary-items/MHRiahLowofy8ek9.htm)|Invisibility (At Will)|
|[mo5giist2lOlgVh5.htm](kingmaker-bestiary-items/mo5giist2lOlgVh5.htm)|Longsword|+1,striking,grievous|
|[MxGd3lTVgUVyx8lw.htm](kingmaker-bestiary-items/MxGd3lTVgUVyx8lw.htm)|Handwraps of Mighty Blows|+2,striking|
|[mxTIF80CDuT8kywe.htm](kingmaker-bestiary-items/mxTIF80CDuT8kywe.htm)|Ring of Energy Resistance (Fire)|
|[myUDdwt1FjfMMl9p.htm](kingmaker-bestiary-items/myUDdwt1FjfMMl9p.htm)|Staff|+1|
|[N4FRJPLjGNaWpvxp.htm](kingmaker-bestiary-items/N4FRJPLjGNaWpvxp.htm)|Religious Symbol of Yog-Sothoth|
|[NcDY8Koc4BA35pJ3.htm](kingmaker-bestiary-items/NcDY8Koc4BA35pJ3.htm)|The Inward Flowing Source (Spellbook)|
|[nGVNV8Ln1yOs1ff4.htm](kingmaker-bestiary-items/nGVNV8Ln1yOs1ff4.htm)|Scimitar|+1,striking,disrupting|
|[nOFWQRAuYiD4lmUi.htm](kingmaker-bestiary-items/nOFWQRAuYiD4lmUi.htm)|+1 Resilient Clothing (Explorer's)|
|[NsbWotgycMOTGuGT.htm](kingmaker-bestiary-items/NsbWotgycMOTGuGT.htm)|Suggestion (At Will)|
|[NTO1vgINd90By1LE.htm](kingmaker-bestiary-items/NTO1vgINd90By1LE.htm)|Journal|
|[NzVLRMB9AI3b1pBg.htm](kingmaker-bestiary-items/NzVLRMB9AI3b1pBg.htm)|Tattered Robes|
|[O3DozXqnlvka4VIK.htm](kingmaker-bestiary-items/O3DozXqnlvka4VIK.htm)|Choker of Elocution (Sylvan)|
|[o5RWcZEKhSBVRSCd.htm](kingmaker-bestiary-items/o5RWcZEKhSBVRSCd.htm)|See Invisibility (Constant)|
|[oBUF4qu6Shoi0A37.htm](kingmaker-bestiary-items/oBUF4qu6Shoi0A37.htm)|Musical Instrument (Lute)|
|[OFUhWZezAxCDljSA.htm](kingmaker-bestiary-items/OFUhWZezAxCDljSA.htm)|Dogslicer|+1,striking|
|[OGDCzsyhNsf5h3j3.htm](kingmaker-bestiary-items/OGDCzsyhNsf5h3j3.htm)|Artisan's Tools (Woodworker's)|
|[oK1SNnCrHiQzSXJH.htm](kingmaker-bestiary-items/oK1SNnCrHiQzSXJH.htm)|True Seeing (Constant)|
|[OLgE1eKYfChrP7ip.htm](kingmaker-bestiary-items/OLgE1eKYfChrP7ip.htm)|Ring of Energy Resistance (Fire)|
|[oM6MOLJCVEE8WTTc.htm](kingmaker-bestiary-items/oM6MOLJCVEE8WTTc.htm)|Speak with Animals (Constant)|
|[oMIhKijss8NvtV5O.htm](kingmaker-bestiary-items/oMIhKijss8NvtV5O.htm)|Sickle|cold-iron|
|[OQrd4dnZRVS4hQEO.htm](kingmaker-bestiary-items/OQrd4dnZRVS4hQEO.htm)|Maestro's Instrument (Lesser) (Lute)|
|[OyvZeoS5KOjNU1P7.htm](kingmaker-bestiary-items/OyvZeoS5KOjNU1P7.htm)|Heavy Wooden Shield|
|[p4u7kb7SoMyLXv5z.htm](kingmaker-bestiary-items/p4u7kb7SoMyLXv5z.htm)|Wand of Paralyze (Level 3)|
|[P65Jvc5wODFPZ00e.htm](kingmaker-bestiary-items/P65Jvc5wODFPZ00e.htm)|Dagger|+1,striking,wounding|
|[pahKe1LJWdfkKj6M.htm](kingmaker-bestiary-items/pahKe1LJWdfkKj6M.htm)|Mind Reading (At Will)|
|[pF8Ngkhp4Zxkwqhd.htm](kingmaker-bestiary-items/pF8Ngkhp4Zxkwqhd.htm)|+2 Glamered Fortification Greater Resilient Leather Armor|
|[pIQHLI7rmcBAcJql.htm](kingmaker-bestiary-items/pIQHLI7rmcBAcJql.htm)|Wand of Heal (Level 1)|
|[PmwxtRI969adfqM7.htm](kingmaker-bestiary-items/PmwxtRI969adfqM7.htm)|Key Ring|
|[pNlVk6Eq16L9OBYt.htm](kingmaker-bestiary-items/pNlVk6Eq16L9OBYt.htm)|Tongues (Constant)|
|[pO1coGfLkTGz4KtP.htm](kingmaker-bestiary-items/pO1coGfLkTGz4KtP.htm)|Speak with Plants (Constant)|
|[q0qbUmAXaCjmrWfl.htm](kingmaker-bestiary-items/q0qbUmAXaCjmrWfl.htm)|Water Walk (Constant) (Self Only)|
|[q2Yeh8EgyWJTHR2E.htm](kingmaker-bestiary-items/q2Yeh8EgyWJTHR2E.htm)|Greataxe|+1|
|[Q3rzVNhMmKJUv8Hd.htm](kingmaker-bestiary-items/Q3rzVNhMmKJUv8Hd.htm)|Rusted Iron Holy Symbol of Erastil|
|[Q6aECCBC8XJs3Eke.htm](kingmaker-bestiary-items/Q6aECCBC8XJs3Eke.htm)|Water Breathing (Constant)|
|[QLpCft5YSm98kbx4.htm](kingmaker-bestiary-items/QLpCft5YSm98kbx4.htm)|Speak with Animals (At Will) (Arthropods Only)|
|[qnW0tYvY2eKqxqcI.htm](kingmaker-bestiary-items/qnW0tYvY2eKqxqcI.htm)|+1 Glamered Resilient reastplate|
|[r4ZnY6A2rOJTCSSI.htm](kingmaker-bestiary-items/r4ZnY6A2rOJTCSSI.htm)|Battle Axe|+1,striking|
|[R7WS5l7jScWRr53y.htm](kingmaker-bestiary-items/R7WS5l7jScWRr53y.htm)|+1 Resilient Hide Armor|
|[r9vIYF8LhOmQFT2b.htm](kingmaker-bestiary-items/r9vIYF8LhOmQFT2b.htm)|Dagger|+1|
|[rA1w7Wi3HDq5IpD3.htm](kingmaker-bestiary-items/rA1w7Wi3HDq5IpD3.htm)|Longsword|+1,striking,thundering|
|[rwoTbwYfMVqkiolf.htm](kingmaker-bestiary-items/rwoTbwYfMVqkiolf.htm)|Tongues (Tongues)|
|[RyEkbNlf2v3XdUOC.htm](kingmaker-bestiary-items/RyEkbNlf2v3XdUOC.htm)|Filthy Rags|
|[S01l0m7wdPHytTG5.htm](kingmaker-bestiary-items/S01l0m7wdPHytTG5.htm)|+1 Resilient Breastplate|
|[S0kF01v0eYuEyyQj.htm](kingmaker-bestiary-items/S0kF01v0eYuEyyQj.htm)|Charm (At Will)|
|[S3gLjkwIbPv8hrwC.htm](kingmaker-bestiary-items/S3gLjkwIbPv8hrwC.htm)|True Seeing (Constant)|
|[sAPqpw1AmafiS0qn.htm](kingmaker-bestiary-items/sAPqpw1AmafiS0qn.htm)|Palace Key|
|[SIvuOyYTHG1nqVDM.htm](kingmaker-bestiary-items/SIvuOyYTHG1nqVDM.htm)|Telekinetic Haul (At Will)|
|[SMqPkvP3sngKVR0q.htm](kingmaker-bestiary-items/SMqPkvP3sngKVR0q.htm)|Giant Wasp Venom (On Darts)|
|[SnhDyvqRiwCT2xFp.htm](kingmaker-bestiary-items/SnhDyvqRiwCT2xFp.htm)|Greatsword|+1,striking|
|[Sq7PsPkZ8LUfigSz.htm](kingmaker-bestiary-items/Sq7PsPkZ8LUfigSz.htm)|+1 Resilient Studded Leather Armor|
|[sUGKCp2vAoGiIFn9.htm](kingmaker-bestiary-items/sUGKCp2vAoGiIFn9.htm)|Oathbow|+2,striking|
|[SviievXlIEecohMT.htm](kingmaker-bestiary-items/SviievXlIEecohMT.htm)|Glaive|+2,greaterStriking|
|[sWBDDbGvVypQmjhH.htm](kingmaker-bestiary-items/sWBDDbGvVypQmjhH.htm)|Robes|
|[SzjP0fTFs7v3U74Q.htm](kingmaker-bestiary-items/SzjP0fTFs7v3U74Q.htm)|Composite Shortbow|+1,striking|
|[T2CxMAShv0ikuq5z.htm](kingmaker-bestiary-items/T2CxMAShv0ikuq5z.htm)|Ranseur|+1|
|[TgxTGQ9ZPvKuOKzP.htm](kingmaker-bestiary-items/TgxTGQ9ZPvKuOKzP.htm)|Elixir of Life (Major) (Infused)|
|[tKaxQW2SwxusRLDJ.htm](kingmaker-bestiary-items/tKaxQW2SwxusRLDJ.htm)|Mace|+1,striking,frost|
|[tt77yIY913w4s6Qb.htm](kingmaker-bestiary-items/tt77yIY913w4s6Qb.htm)|Rod of Razors|+2,greaterStriking,adamantine|
|[TXyxtLOPRcUESvfj.htm](kingmaker-bestiary-items/TXyxtLOPRcUESvfj.htm)|Tongues (Constant)|
|[TZKyD7ZHyYEDKtws.htm](kingmaker-bestiary-items/TZKyD7ZHyYEDKtws.htm)|Tongues (Constant)|
|[UCNMMptie1HfmWdu.htm](kingmaker-bestiary-items/UCNMMptie1HfmWdu.htm)|Rapier|+3,majorStriking,greaterShock,wounding|
|[UeSFgZM3o03iSxuq.htm](kingmaker-bestiary-items/UeSFgZM3o03iSxuq.htm)|+2 Resilient Full Plate|
|[UKYROFSj3c464waM.htm](kingmaker-bestiary-items/UKYROFSj3c464waM.htm)|Battle Axe|+2,striking|
|[Um8WRHtbayEPwLGn.htm](kingmaker-bestiary-items/Um8WRHtbayEPwLGn.htm)|Shortsword|cold-iron|
|[uq6NvrLNEXMkTsQf.htm](kingmaker-bestiary-items/uq6NvrLNEXMkTsQf.htm)|Pass Without Trace (Constant)|
|[UqyHj2l5UsVe9uNp.htm](kingmaker-bestiary-items/UqyHj2l5UsVe9uNp.htm)|+1 Leather Armor|
|[Us5NJn2RFIpZuqkH.htm](kingmaker-bestiary-items/Us5NJn2RFIpZuqkH.htm)|Crushing Despair (At Will)|
|[usScZt5t8JLT5Ipy.htm](kingmaker-bestiary-items/usScZt5t8JLT5Ipy.htm)|Illusory Disguise (At Will)|
|[UsxGNp0GLCHolNw9.htm](kingmaker-bestiary-items/UsxGNp0GLCHolNw9.htm)|Warhammer|+1|
|[UV0GwxK9jPNBGPkx.htm](kingmaker-bestiary-items/UV0GwxK9jPNBGPkx.htm)|Composite Longbow|+1|
|[uwaEfaQp2MZCrkyx.htm](kingmaker-bestiary-items/uwaEfaQp2MZCrkyx.htm)|Greataxe|cold-iron|
|[UZkmL2WnxA4MOAUK.htm](kingmaker-bestiary-items/UZkmL2WnxA4MOAUK.htm)|Composite Shortbow|+2,greaterStriking,darkwood,grievous,thundering|
|[vb6tOxigCXGBNO8n.htm](kingmaker-bestiary-items/vb6tOxigCXGBNO8n.htm)|+2 Resilient Leather Armor|
|[vbjtlEObLLpIhfeu.htm](kingmaker-bestiary-items/vbjtlEObLLpIhfeu.htm)|Rapier|+1|
|[vFhm5NJpp6V7lfc8.htm](kingmaker-bestiary-items/vFhm5NJpp6V7lfc8.htm)|Composite Shortbow|+1,striking|
|[VgQyT5O8X59xTXWs.htm](kingmaker-bestiary-items/VgQyT5O8X59xTXWs.htm)|Summon Elemental (Water Elementals only)|
|[vO3Jnytxei8sWTZC.htm](kingmaker-bestiary-items/vO3Jnytxei8sWTZC.htm)|Kukri|+1|
|[vVOwyQHVQvmtGjkx.htm](kingmaker-bestiary-items/vVOwyQHVQvmtGjkx.htm)|+1 Greater Invisibility Resilient Leather Armor|
|[VwMYfCPLMmuJQ2GH.htm](kingmaker-bestiary-items/VwMYfCPLMmuJQ2GH.htm)|+1 Resilient Breastplate|
|[vwnAqdmQtoc2jUsW.htm](kingmaker-bestiary-items/vwnAqdmQtoc2jUsW.htm)|Broken +1 Cold Iron Flaming Bastard Sword|+1,cold-iron,flaming|
|[vXW5poBVgyqzdM9r.htm](kingmaker-bestiary-items/vXW5poBVgyqzdM9r.htm)|Pass Without Trace (Constant)|
|[vyta0B6y63mLUnmx.htm](kingmaker-bestiary-items/vyta0B6y63mLUnmx.htm)|Maestro's Lute (Lesser)|
|[VzPueq3VgRjoXiJE.htm](kingmaker-bestiary-items/VzPueq3VgRjoXiJE.htm)|+1 Resilient Hide Armor|
|[W8NvzJ9eGCBEL7cH.htm](kingmaker-bestiary-items/W8NvzJ9eGCBEL7cH.htm)|Robes|
|[wd4WShZlAcAc3dLe.htm](kingmaker-bestiary-items/wd4WShZlAcAc3dLe.htm)|Religious Symbol of Yog-Sothoth|
|[wEUwtWWTglvCsbrr.htm](kingmaker-bestiary-items/wEUwtWWTglvCsbrr.htm)|Ring of Energy Resistance (Fire)|
|[Wj4PwJFZfcVXdLml.htm](kingmaker-bestiary-items/Wj4PwJFZfcVXdLml.htm)|Mind Reading (At Will)|
|[WlQZsDquZL824cDy.htm](kingmaker-bestiary-items/WlQZsDquZL824cDy.htm)|+1 Hide Armor|
|[wrFzv3AXMax8ZXTt.htm](kingmaker-bestiary-items/wrFzv3AXMax8ZXTt.htm)|Illusory Disguise (At Will)|
|[X3z6nTBllD8cR58y.htm](kingmaker-bestiary-items/X3z6nTBllD8cR58y.htm)|Ring of Energy Resistance (Cold)|
|[x9smkTQ1YyyY2Zcx.htm](kingmaker-bestiary-items/x9smkTQ1YyyY2Zcx.htm)|Invisibility (At Will)|
|[XgD0k33XDvhefMEQ.htm](kingmaker-bestiary-items/XgD0k33XDvhefMEQ.htm)|Tongues (Constant)|
|[XI80UWdBffkVI3qW.htm](kingmaker-bestiary-items/XI80UWdBffkVI3qW.htm)|Maul|+2,greaterShock|
|[XJFqRYGx02Um1W4Y.htm](kingmaker-bestiary-items/XJFqRYGx02Um1W4Y.htm)|Spear|+2,striking,anarchic|
|[xrKfhlv0heNZjJQl.htm](kingmaker-bestiary-items/xrKfhlv0heNZjJQl.htm)|+1 Morningstar|+1|
|[xT9pOWbS31HQus3Y.htm](kingmaker-bestiary-items/xT9pOWbS31HQus3Y.htm)|Hatchet|+1,striking|
|[xy0RcI3dflBZepLE.htm](kingmaker-bestiary-items/xy0RcI3dflBZepLE.htm)|Shortsword|+1|
|[Y9hSg33JjPCL2a3D.htm](kingmaker-bestiary-items/Y9hSg33JjPCL2a3D.htm)|+2 Resilient Leather Armor|
|[yGxF0q0Wjk3mF2Mo.htm](kingmaker-bestiary-items/yGxF0q0Wjk3mF2Mo.htm)|Air Walk (Constant)|
|[YJR2VSzinZXYbwkd.htm](kingmaker-bestiary-items/YJR2VSzinZXYbwkd.htm)|Greatclub|+1,striking|
|[YjsZVkEOo90ZoPjJ.htm](kingmaker-bestiary-items/YjsZVkEOo90ZoPjJ.htm)|Wand of Heal (Level 3)|
|[YmkfgSgetEPoWRKA.htm](kingmaker-bestiary-items/YmkfgSgetEPoWRKA.htm)|Religious Symbol of Gyronna|
|[ynDcPwSchoDdTX0R.htm](kingmaker-bestiary-items/ynDcPwSchoDdTX0R.htm)|Dimension Door (At Will)|
|[YtRTdI3UAqH6vZVL.htm](kingmaker-bestiary-items/YtRTdI3UAqH6vZVL.htm)|Elemental Form (Water only)|
|[z97KUUoWcBLn9aga.htm](kingmaker-bestiary-items/z97KUUoWcBLn9aga.htm)|+1 Leather Armor|
|[Zb5uVpmuHCmlnVkv.htm](kingmaker-bestiary-items/Zb5uVpmuHCmlnVkv.htm)|+2 Resilient Mithral Breastplate|
|[zGifR2PUMCuF3jab.htm](kingmaker-bestiary-items/zGifR2PUMCuF3jab.htm)|Religious Symbol of Yog-Sothoth|
|[ZixFLnOGrgPgMXgK.htm](kingmaker-bestiary-items/ZixFLnOGrgPgMXgK.htm)|Shortbow|+1,striking|
|[ZozpoIHVu6M6hsy6.htm](kingmaker-bestiary-items/ZozpoIHVu6M6hsy6.htm)|+2 Resilient Hide Armor|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[00zSyT0a2tdDQnmZ.htm](kingmaker-bestiary-items/00zSyT0a2tdDQnmZ.htm)|Low-Light Vision|auto-trad|
|[093ZnioHRqaohRiq.htm](kingmaker-bestiary-items/093ZnioHRqaohRiq.htm)|Throw Rock|auto-trad|
|[0BMCA9P7mLMAMs10.htm](kingmaker-bestiary-items/0BMCA9P7mLMAMs10.htm)|Sunder Objects|auto-trad|
|[0F1cIENDKxVfgMvZ.htm](kingmaker-bestiary-items/0F1cIENDKxVfgMvZ.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[0gCnA8mF12gNsipi.htm](kingmaker-bestiary-items/0gCnA8mF12gNsipi.htm)|Attack of Opportunity|auto-trad|
|[0ivwHldip6F8T0gA.htm](kingmaker-bestiary-items/0ivwHldip6F8T0gA.htm)|Attack of Opportunity (Fangs Only)|auto-trad|
|[0KLll6op6LwSp19s.htm](kingmaker-bestiary-items/0KLll6op6LwSp19s.htm)|Hydra Regeneration|auto-trad|
|[0L6qwj5JpEiwbnph.htm](kingmaker-bestiary-items/0L6qwj5JpEiwbnph.htm)|Storm of Jaws|auto-trad|
|[0mCmhGYAI47n4kdy.htm](kingmaker-bestiary-items/0mCmhGYAI47n4kdy.htm)|Divine Prepared Spells|auto-trad|
|[0Pe1uBGVehRLhBim.htm](kingmaker-bestiary-items/0Pe1uBGVehRLhBim.htm)|Attack of Opportunity|auto-trad|
|[0pHw83Qkw39rMcIx.htm](kingmaker-bestiary-items/0pHw83Qkw39rMcIx.htm)|Contingency|auto-trad|
|[0rYILxhM4TiZQaeA.htm](kingmaker-bestiary-items/0rYILxhM4TiZQaeA.htm)|Sudden Charge|auto-trad|
|[0TyxIsxCmJlWFTk6.htm](kingmaker-bestiary-items/0TyxIsxCmJlWFTk6.htm)|Evasion|auto-trad|
|[0uxUMo5bNxKA5eIg.htm](kingmaker-bestiary-items/0uxUMo5bNxKA5eIg.htm)|Change Shape|auto-trad|
|[0VYJH6vuFSZm9NIX.htm](kingmaker-bestiary-items/0VYJH6vuFSZm9NIX.htm)|Primal Innate Spells|auto-trad|
|[0WYsG1PdrVoJN2Ue.htm](kingmaker-bestiary-items/0WYsG1PdrVoJN2Ue.htm)|Divine Innate Spells|auto-trad|
|[137OFGe9srUCXPpN.htm](kingmaker-bestiary-items/137OFGe9srUCXPpN.htm)|Closing Volley|auto-trad|
|[1AbBtZhPY7lYpSm0.htm](kingmaker-bestiary-items/1AbBtZhPY7lYpSm0.htm)|Goblin Pox|auto-trad|
|[1AemwaFBoSAJ1UHj.htm](kingmaker-bestiary-items/1AemwaFBoSAJ1UHj.htm)|Change Shape|auto-trad|
|[1BD5jRohvhbGdDwq.htm](kingmaker-bestiary-items/1BD5jRohvhbGdDwq.htm)|Fangs|auto-trad|
|[1bKTp47s4lhPF0Ix.htm](kingmaker-bestiary-items/1bKTp47s4lhPF0Ix.htm)|Darkvision|auto-trad|
|[1ChXT4DL7N5JhHEq.htm](kingmaker-bestiary-items/1ChXT4DL7N5JhHEq.htm)|Tail|auto-trad|
|[1CVzuimA1fYyvCBq.htm](kingmaker-bestiary-items/1CVzuimA1fYyvCBq.htm)|Cruel Anatomist|auto-trad|
|[1fxLVAyfm2PZ7Zxg.htm](kingmaker-bestiary-items/1fxLVAyfm2PZ7Zxg.htm)|Attack of Opportunity|auto-trad|
|[1Ht8zfzmHL4tULd5.htm](kingmaker-bestiary-items/1Ht8zfzmHL4tULd5.htm)|Shield Rip and Chew|auto-trad|
|[1IvCoMHs0qGTlw8J.htm](kingmaker-bestiary-items/1IvCoMHs0qGTlw8J.htm)|Ripping Gaze|auto-trad|
|[1jsdM4qpD0smQsMV.htm](kingmaker-bestiary-items/1jsdM4qpD0smQsMV.htm)|Low-Light Vision|auto-trad|
|[1kX9bpy0yIk83pGw.htm](kingmaker-bestiary-items/1kX9bpy0yIk83pGw.htm)|Rejuvenation|auto-trad|
|[1lrxLnSpTkBwcSli.htm](kingmaker-bestiary-items/1lrxLnSpTkBwcSli.htm)|Ghostly Battle Axe|auto-trad|
|[1Lvngo9gteYE4ep9.htm](kingmaker-bestiary-items/1Lvngo9gteYE4ep9.htm)|Dagger|auto-trad|
|[1PdKOeeXtOPcFYCb.htm](kingmaker-bestiary-items/1PdKOeeXtOPcFYCb.htm)|Composite Longbow|auto-trad|
|[1RIzUNU0v2cfgsjD.htm](kingmaker-bestiary-items/1RIzUNU0v2cfgsjD.htm)|Darkvision|auto-trad|
|[1UmNfowZ4HWxS9rx.htm](kingmaker-bestiary-items/1UmNfowZ4HWxS9rx.htm)|Devastating Blast|auto-trad|
|[1V9pafG73kkZDmNM.htm](kingmaker-bestiary-items/1V9pafG73kkZDmNM.htm)|Negative Healing|auto-trad|
|[1V9uaQOnCMGgMxl2.htm](kingmaker-bestiary-items/1V9uaQOnCMGgMxl2.htm)|Darkvision|auto-trad|
|[1VH2ArAHvIhMi57j.htm](kingmaker-bestiary-items/1VH2ArAHvIhMi57j.htm)|Fist|auto-trad|
|[1VqUGL35cW3NbTOz.htm](kingmaker-bestiary-items/1VqUGL35cW3NbTOz.htm)|Attack of Opportunity|auto-trad|
|[1Y7PAPhO3aVpgKoq.htm](kingmaker-bestiary-items/1Y7PAPhO3aVpgKoq.htm)|Ghostly Rapier|auto-trad|
|[27wudVSiScZejHwt.htm](kingmaker-bestiary-items/27wudVSiScZejHwt.htm)|Negative Healing|auto-trad|
|[28VxmNDvpa40sH4P.htm](kingmaker-bestiary-items/28VxmNDvpa40sH4P.htm)|Divine Prepared Spells|auto-trad|
|[2A5WqL14A7eKaElj.htm](kingmaker-bestiary-items/2A5WqL14A7eKaElj.htm)|Claw|auto-trad|
|[2bthXOLfHVbIarRo.htm](kingmaker-bestiary-items/2bthXOLfHVbIarRo.htm)|At-Will Spells|auto-trad|
|[2c38HSvjdnaJgeI2.htm](kingmaker-bestiary-items/2c38HSvjdnaJgeI2.htm)|Awesome Blow|auto-trad|
|[2dHPDZu3BYN1GauB.htm](kingmaker-bestiary-items/2dHPDZu3BYN1GauB.htm)|No Breath|auto-trad|
|[2ECIwvcXVQ6w8vi5.htm](kingmaker-bestiary-items/2ECIwvcXVQ6w8vi5.htm)|Punishing Momentum|auto-trad|
|[2GNdGYBXQOD8VlC7.htm](kingmaker-bestiary-items/2GNdGYBXQOD8VlC7.htm)|Darkvision|auto-trad|
|[2hG90mAiBnKHWoxc.htm](kingmaker-bestiary-items/2hG90mAiBnKHWoxc.htm)|Fast Healing 20|auto-trad|
|[2hjq0oo2EsTcXPJe.htm](kingmaker-bestiary-items/2hjq0oo2EsTcXPJe.htm)|Tail|auto-trad|
|[2I9ZCEqUKoC0AAqQ.htm](kingmaker-bestiary-items/2I9ZCEqUKoC0AAqQ.htm)|At-Will Spells|auto-trad|
|[2IqWrJj9edOVS3Gp.htm](kingmaker-bestiary-items/2IqWrJj9edOVS3Gp.htm)|Triple Opportunity|auto-trad|
|[2lGYb3ZO8pwxgMhD.htm](kingmaker-bestiary-items/2lGYb3ZO8pwxgMhD.htm)|Focus Gaze|auto-trad|
|[2m9M9QruB9jYdEjd.htm](kingmaker-bestiary-items/2m9M9QruB9jYdEjd.htm)|Darkvision|auto-trad|
|[2n6nRtgDNRWONgvp.htm](kingmaker-bestiary-items/2n6nRtgDNRWONgvp.htm)|Drain Luck|auto-trad|
|[2nd3Ete1Wir3K3bj.htm](kingmaker-bestiary-items/2nd3Ete1Wir3K3bj.htm)|Wave|auto-trad|
|[2pEzXozrOXVdVg0A.htm](kingmaker-bestiary-items/2pEzXozrOXVdVg0A.htm)|Life Bloom|auto-trad|
|[2prGxYGRkpCNqmdI.htm](kingmaker-bestiary-items/2prGxYGRkpCNqmdI.htm)|Shelyn's Thanks|auto-trad|
|[2rAKighB6Ywhk5s3.htm](kingmaker-bestiary-items/2rAKighB6Ywhk5s3.htm)|Shock|auto-trad|
|[2YaktCYgfrnvuUbE.htm](kingmaker-bestiary-items/2YaktCYgfrnvuUbE.htm)|Slash Throat|auto-trad|
|[312Fug37MtFpvG0P.htm](kingmaker-bestiary-items/312Fug37MtFpvG0P.htm)|Tongue|auto-trad|
|[3AidCm7XJT968Swi.htm](kingmaker-bestiary-items/3AidCm7XJT968Swi.htm)|Choke|auto-trad|
|[3ANSkECTu3y2Lt3C.htm](kingmaker-bestiary-items/3ANSkECTu3y2Lt3C.htm)|Dogslicer|auto-trad|
|[3BSxL95mz6QjoWi1.htm](kingmaker-bestiary-items/3BSxL95mz6QjoWi1.htm)|Sneak Attack|auto-trad|
|[3Eksrmh390T4SrP7.htm](kingmaker-bestiary-items/3Eksrmh390T4SrP7.htm)|Magic Immunity|auto-trad|
|[3gmJPQIKZq7dcelo.htm](kingmaker-bestiary-items/3gmJPQIKZq7dcelo.htm)|Composite Shortbow|auto-trad|
|[3GqX6rg7gxLsmuvd.htm](kingmaker-bestiary-items/3GqX6rg7gxLsmuvd.htm)|Rock|auto-trad|
|[3hPEjn6DWOXtuQ2I.htm](kingmaker-bestiary-items/3hPEjn6DWOXtuQ2I.htm)|Occult Innate Spells|auto-trad|
|[3jVmH3McNsX8B0Od.htm](kingmaker-bestiary-items/3jVmH3McNsX8B0Od.htm)|Jaws|auto-trad|
|[3pzII2xLdduTjJrG.htm](kingmaker-bestiary-items/3pzII2xLdduTjJrG.htm)|Quickened Casting|auto-trad|
|[3t3ulXs6r1EL76F0.htm](kingmaker-bestiary-items/3t3ulXs6r1EL76F0.htm)|Breath Weapon|auto-trad|
|[3TfIMe12rJpDf9LZ.htm](kingmaker-bestiary-items/3TfIMe12rJpDf9LZ.htm)|Self-Loathing|auto-trad|
|[3tyBaMgyINlZCj76.htm](kingmaker-bestiary-items/3tyBaMgyINlZCj76.htm)|Fist|auto-trad|
|[3VDIY4vCKlW0qvFl.htm](kingmaker-bestiary-items/3VDIY4vCKlW0qvFl.htm)|Fist|auto-trad|
|[3VX47OPvg5PAjzsD.htm](kingmaker-bestiary-items/3VX47OPvg5PAjzsD.htm)|Primal Spontaneous Spells|auto-trad|
|[3wMfK5zZSqFhhTgs.htm](kingmaker-bestiary-items/3wMfK5zZSqFhhTgs.htm)|Primordial Roar|auto-trad|
|[3Ygn2F1yeN3UTO2S.htm](kingmaker-bestiary-items/3Ygn2F1yeN3UTO2S.htm)|Pain|auto-trad|
|[3ZL7oyw6PKNMzEhf.htm](kingmaker-bestiary-items/3ZL7oyw6PKNMzEhf.htm)|Grant Desire|auto-trad|
|[45aANCYgNwQS8Ega.htm](kingmaker-bestiary-items/45aANCYgNwQS8Ega.htm)|Hooktongue Venom|auto-trad|
|[48zaDpaZFzUKLJHL.htm](kingmaker-bestiary-items/48zaDpaZFzUKLJHL.htm)|Sudden Charge|auto-trad|
|[49xv61tdfV6J6P29.htm](kingmaker-bestiary-items/49xv61tdfV6J6P29.htm)|Discorporate|auto-trad|
|[4bbZ6NaZSeW5sE1f.htm](kingmaker-bestiary-items/4bbZ6NaZSeW5sE1f.htm)|Trident|auto-trad|
|[4BNXIgVORiBDrL95.htm](kingmaker-bestiary-items/4BNXIgVORiBDrL95.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[4C0eLAnhYCsQz9KO.htm](kingmaker-bestiary-items/4C0eLAnhYCsQz9KO.htm)|+4 Status to All Saves vs. Mental|auto-trad|
|[4EsbaXbLbmfGNLmn.htm](kingmaker-bestiary-items/4EsbaXbLbmfGNLmn.htm)|Constant Spells|auto-trad|
|[4gO7xPscU8GkItt4.htm](kingmaker-bestiary-items/4gO7xPscU8GkItt4.htm)|Spear|auto-trad|
|[4gP2dSNnL1rudfAt.htm](kingmaker-bestiary-items/4gP2dSNnL1rudfAt.htm)|Tongue|auto-trad|
|[4hjXOaTYgTu9yoad.htm](kingmaker-bestiary-items/4hjXOaTYgTu9yoad.htm)|Claws|auto-trad|
|[4mK32Hq1EHPgHPW4.htm](kingmaker-bestiary-items/4mK32Hq1EHPgHPW4.htm)|Dread Striker|auto-trad|
|[4n19omVeZZAAGXBk.htm](kingmaker-bestiary-items/4n19omVeZZAAGXBk.htm)|Coven|auto-trad|
|[4N1vw9fFoiAgxSn1.htm](kingmaker-bestiary-items/4N1vw9fFoiAgxSn1.htm)|Flying Strafe|auto-trad|
|[4OphdUKxBpxPyBKN.htm](kingmaker-bestiary-items/4OphdUKxBpxPyBKN.htm)|Tail Drag|auto-trad|
|[4QNftOG1O0v3IrVA.htm](kingmaker-bestiary-items/4QNftOG1O0v3IrVA.htm)|Trip Up|auto-trad|
|[4S2A9APP31jlMK4Q.htm](kingmaker-bestiary-items/4S2A9APP31jlMK4Q.htm)|Arcane Spontaneous Spells|auto-trad|
|[4SgwfAqj7PXlo4NI.htm](kingmaker-bestiary-items/4SgwfAqj7PXlo4NI.htm)|Kob's Ruinous Strike|auto-trad|
|[4WaSsGcstf79Y44R.htm](kingmaker-bestiary-items/4WaSsGcstf79Y44R.htm)|Jaws|auto-trad|
|[4WbCPkQrjR5xvIUs.htm](kingmaker-bestiary-items/4WbCPkQrjR5xvIUs.htm)|Temporal Strikes|auto-trad|
|[4z1JD9bhf1R5wpmX.htm](kingmaker-bestiary-items/4z1JD9bhf1R5wpmX.htm)|Trident|auto-trad|
|[54mSt8tnFXfSUjhU.htm](kingmaker-bestiary-items/54mSt8tnFXfSUjhU.htm)|Cling|auto-trad|
|[55Hvc61MUgVoahwb.htm](kingmaker-bestiary-items/55Hvc61MUgVoahwb.htm)|Terrain Advantage|auto-trad|
|[5AVIS7Cfz12DClSG.htm](kingmaker-bestiary-items/5AVIS7Cfz12DClSG.htm)|Darkvision|auto-trad|
|[5dKTN8eobwXG2iGV.htm](kingmaker-bestiary-items/5dKTN8eobwXG2iGV.htm)|Arcane Spontaneous Spells|auto-trad|
|[5dvPmX8OUqLml3m6.htm](kingmaker-bestiary-items/5dvPmX8OUqLml3m6.htm)|Claw|auto-trad|
|[5EExyaDy8xArOFh0.htm](kingmaker-bestiary-items/5EExyaDy8xArOFh0.htm)|Fast Healing 10|auto-trad|
|[5EvbvNghD1THDwUR.htm](kingmaker-bestiary-items/5EvbvNghD1THDwUR.htm)|Negative Healing|auto-trad|
|[5f6mJgvaVADXCaAz.htm](kingmaker-bestiary-items/5f6mJgvaVADXCaAz.htm)|Scent|auto-trad|
|[5G6Es9pOfgRiXtpy.htm](kingmaker-bestiary-items/5G6Es9pOfgRiXtpy.htm)|Power Attack|auto-trad|
|[5hqdjh4Jn5wtzqGs.htm](kingmaker-bestiary-items/5hqdjh4Jn5wtzqGs.htm)|Tendril|auto-trad|
|[5LclMp6q589Sl0Mn.htm](kingmaker-bestiary-items/5LclMp6q589Sl0Mn.htm)|Claw|auto-trad|
|[5LgL3UMxFZRmIgXy.htm](kingmaker-bestiary-items/5LgL3UMxFZRmIgXy.htm)|Vulture Beak|auto-trad|
|[5m1MoZTkyC5yL2jL.htm](kingmaker-bestiary-items/5m1MoZTkyC5yL2jL.htm)|Double Strike|auto-trad|
|[5nbfijONNe4T5HFM.htm](kingmaker-bestiary-items/5nbfijONNe4T5HFM.htm)|Draining Touch|auto-trad|
|[5O8YD9thn6myrfWy.htm](kingmaker-bestiary-items/5O8YD9thn6myrfWy.htm)|Pollen Burst|auto-trad|
|[5qgx9qhXAceS6uLh.htm](kingmaker-bestiary-items/5qgx9qhXAceS6uLh.htm)|Enraged Cunning|auto-trad|
|[5rxGka9M6pYlIoVb.htm](kingmaker-bestiary-items/5rxGka9M6pYlIoVb.htm)|Occult Innate Spells|auto-trad|
|[5uF0UI8Ihj8qhfy5.htm](kingmaker-bestiary-items/5uF0UI8Ihj8qhfy5.htm)|Support Benefit|auto-trad|
|[5uI2L7wMgW34OuXg.htm](kingmaker-bestiary-items/5uI2L7wMgW34OuXg.htm)|Vengeful Rage|auto-trad|
|[5ulvx0zqprJryzvq.htm](kingmaker-bestiary-items/5ulvx0zqprJryzvq.htm)|Attack of Opportunity|auto-trad|
|[5UTWzAXIsjMqThHw.htm](kingmaker-bestiary-items/5UTWzAXIsjMqThHw.htm)|Unfair Aim|auto-trad|
|[5w3fouC3WVuTBtWK.htm](kingmaker-bestiary-items/5w3fouC3WVuTBtWK.htm)|Trample|auto-trad|
|[5xktm2PrBdKEX5oh.htm](kingmaker-bestiary-items/5xktm2PrBdKEX5oh.htm)|Dagger|auto-trad|
|[5z29EkkGtyaSoB1a.htm](kingmaker-bestiary-items/5z29EkkGtyaSoB1a.htm)|Attack of Opportunity|auto-trad|
|[61GyOh9BdFBPZCUD.htm](kingmaker-bestiary-items/61GyOh9BdFBPZCUD.htm)|Low-Light Vision|auto-trad|
|[61pWFfMaO4glQNlI.htm](kingmaker-bestiary-items/61pWFfMaO4glQNlI.htm)|Fetch Weapon|auto-trad|
|[66WdJU8fGY3RHFZN.htm](kingmaker-bestiary-items/66WdJU8fGY3RHFZN.htm)|Blinded|auto-trad|
|[6aSDaFMEcz58NAqP.htm](kingmaker-bestiary-items/6aSDaFMEcz58NAqP.htm)|Despairing Breath|auto-trad|
|[6cprEKOmbMfF47Lu.htm](kingmaker-bestiary-items/6cprEKOmbMfF47Lu.htm)|Shame|auto-trad|
|[6e66VeO6FBdw8Fjn.htm](kingmaker-bestiary-items/6e66VeO6FBdw8Fjn.htm)|Hatchet|auto-trad|
|[6ESE4WVjACGF6VGi.htm](kingmaker-bestiary-items/6ESE4WVjACGF6VGi.htm)|Coven Spells|auto-trad|
|[6gDHZ6VPfZDrKCnp.htm](kingmaker-bestiary-items/6gDHZ6VPfZDrKCnp.htm)|Sneak Attack|auto-trad|
|[6gwNFiImkw2H0QWA.htm](kingmaker-bestiary-items/6gwNFiImkw2H0QWA.htm)|Grab|auto-trad|
|[6ILB5ny6782Ujs7o.htm](kingmaker-bestiary-items/6ILB5ny6782Ujs7o.htm)|Change Shape|auto-trad|
|[6k6LtJmrZAfTImSh.htm](kingmaker-bestiary-items/6k6LtJmrZAfTImSh.htm)|Staff|auto-trad|
|[6k7jBeAiFp27Wcv1.htm](kingmaker-bestiary-items/6k7jBeAiFp27Wcv1.htm)|Bard Composition Spells|auto-trad|
|[6kvbcAoDPilYf0fI.htm](kingmaker-bestiary-items/6kvbcAoDPilYf0fI.htm)|At-Will Spells|auto-trad|
|[6mO0FIwfW82Cszej.htm](kingmaker-bestiary-items/6mO0FIwfW82Cszej.htm)|Curse of the Weretiger|auto-trad|
|[6MwtMijY3R7DbxaJ.htm](kingmaker-bestiary-items/6MwtMijY3R7DbxaJ.htm)|Ghostly Attack|auto-trad|
|[6ORqPbdQEDKtPs55.htm](kingmaker-bestiary-items/6ORqPbdQEDKtPs55.htm)|Grab|auto-trad|
|[6qAaC77CfOknbaMv.htm](kingmaker-bestiary-items/6qAaC77CfOknbaMv.htm)|Quick Recovery|auto-trad|
|[6qaFJjq9JjlhZbaw.htm](kingmaker-bestiary-items/6qaFJjq9JjlhZbaw.htm)|Ogre Spider Venom|auto-trad|
|[6qhXb73HUuWsXVlw.htm](kingmaker-bestiary-items/6qhXb73HUuWsXVlw.htm)|Aldori Riposte|auto-trad|
|[6QtfXfEtO9Jk9Ceg.htm](kingmaker-bestiary-items/6QtfXfEtO9Jk9Ceg.htm)|Greensight|auto-trad|
|[6SHvRrPDTa74t0cD.htm](kingmaker-bestiary-items/6SHvRrPDTa74t0cD.htm)|Rock|auto-trad|
|[6SNxPBIEy6G6pJ5S.htm](kingmaker-bestiary-items/6SNxPBIEy6G6pJ5S.htm)|Fist|auto-trad|
|[6UhOo5jcpQz76GcZ.htm](kingmaker-bestiary-items/6UhOo5jcpQz76GcZ.htm)|Darkvision|auto-trad|
|[6VrL0WH1rLsYXFpD.htm](kingmaker-bestiary-items/6VrL0WH1rLsYXFpD.htm)|Claw|auto-trad|
|[6XfWpghskVUmJSVG.htm](kingmaker-bestiary-items/6XfWpghskVUmJSVG.htm)|Aldori Dueling Sword|auto-trad|
|[6xM67gan9MlmS1dN.htm](kingmaker-bestiary-items/6xM67gan9MlmS1dN.htm)|Tilt and Roll|auto-trad|
|[6YFC1LK4zBkiKg1N.htm](kingmaker-bestiary-items/6YFC1LK4zBkiKg1N.htm)|Beyond the Barrier|auto-trad|
|[6znbpXWxevbdvTl2.htm](kingmaker-bestiary-items/6znbpXWxevbdvTl2.htm)|Rip and Rend|auto-trad|
|[70wPP2Jajb7NN04d.htm](kingmaker-bestiary-items/70wPP2Jajb7NN04d.htm)|Change Shape|auto-trad|
|[73DVUYU49Oqo474k.htm](kingmaker-bestiary-items/73DVUYU49Oqo474k.htm)|Nymph's Tragedy|auto-trad|
|[73zfyyrBBuzM9Dh9.htm](kingmaker-bestiary-items/73zfyyrBBuzM9Dh9.htm)|Terrifying Croak|auto-trad|
|[77XHZv9nEQwJTaiO.htm](kingmaker-bestiary-items/77XHZv9nEQwJTaiO.htm)|Drain Oculus|auto-trad|
|[79pJPNMXOyazfHy6.htm](kingmaker-bestiary-items/79pJPNMXOyazfHy6.htm)|Accelerated Existence|auto-trad|
|[7aIGZtbifYYVXpiV.htm](kingmaker-bestiary-items/7aIGZtbifYYVXpiV.htm)|Grab|auto-trad|
|[7CQGpLGBAg2Jbtjv.htm](kingmaker-bestiary-items/7CQGpLGBAg2Jbtjv.htm)|Three Headed|auto-trad|
|[7ERyflHHinGfODO2.htm](kingmaker-bestiary-items/7ERyflHHinGfODO2.htm)|Primal Prepared Spells|auto-trad|
|[7ff6eVjIgZUezVKm.htm](kingmaker-bestiary-items/7ff6eVjIgZUezVKm.htm)|Jabberwock Bloodline|auto-trad|
|[7fPsSiHuHlM11fhF.htm](kingmaker-bestiary-items/7fPsSiHuHlM11fhF.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[7fVgYQK2ZSqvG5KV.htm](kingmaker-bestiary-items/7fVgYQK2ZSqvG5KV.htm)|Twin Feint|auto-trad|
|[7fwg0KKHJSx50lBW.htm](kingmaker-bestiary-items/7fwg0KKHJSx50lBW.htm)|Battlefield Command|auto-trad|
|[7iMYJiq8cs6BSSdN.htm](kingmaker-bestiary-items/7iMYJiq8cs6BSSdN.htm)|Nature's Edge|auto-trad|
|[7j0otQgc8z5zzALv.htm](kingmaker-bestiary-items/7j0otQgc8z5zzALv.htm)|Site Bound|auto-trad|
|[7kGu1ORgE4Nay79B.htm](kingmaker-bestiary-items/7kGu1ORgE4Nay79B.htm)|Sneak Attack|auto-trad|
|[7Lzy2YrsBlDo3O23.htm](kingmaker-bestiary-items/7Lzy2YrsBlDo3O23.htm)|Regeneration 25 (Deactivated by Acid or Fire)|auto-trad|
|[7MElwF26Lym91Wch.htm](kingmaker-bestiary-items/7MElwF26Lym91Wch.htm)|Longsword|auto-trad|
|[7N9zMdYcPW2J4Hzt.htm](kingmaker-bestiary-items/7N9zMdYcPW2J4Hzt.htm)|Nyrissa's Favor|auto-trad|
|[7O2rhLBXoMyayaTV.htm](kingmaker-bestiary-items/7O2rhLBXoMyayaTV.htm)|Distracting Yipping|auto-trad|
|[7omOmpvwPwKMmBfB.htm](kingmaker-bestiary-items/7omOmpvwPwKMmBfB.htm)|Intimidating Strike|auto-trad|
|[7pIVt1wT5IWY1dOP.htm](kingmaker-bestiary-items/7pIVt1wT5IWY1dOP.htm)|Flurry of Blows|auto-trad|
|[7pRxaQFmTGlbnKEr.htm](kingmaker-bestiary-items/7pRxaQFmTGlbnKEr.htm)|Darkvision|auto-trad|
|[7PTn0lPspj6Mo8E7.htm](kingmaker-bestiary-items/7PTn0lPspj6Mo8E7.htm)|Darkvision|auto-trad|
|[7QpzxU7FsF8lN6FP.htm](kingmaker-bestiary-items/7QpzxU7FsF8lN6FP.htm)|Shameful Memories|auto-trad|
|[7s2ezouHGaqFCuhW.htm](kingmaker-bestiary-items/7s2ezouHGaqFCuhW.htm)|Corrupting Gaze|auto-trad|
|[7SDmYIdMAER2dLGK.htm](kingmaker-bestiary-items/7SDmYIdMAER2dLGK.htm)|Invoke Stisshak|auto-trad|
|[7SmLN0kqsTk5kyLK.htm](kingmaker-bestiary-items/7SmLN0kqsTk5kyLK.htm)|Crystal Scimitar|auto-trad|
|[7TT0BqubK5ErNyN3.htm](kingmaker-bestiary-items/7TT0BqubK5ErNyN3.htm)|Ranseur|auto-trad|
|[7tW8esj9IVdwLqwS.htm](kingmaker-bestiary-items/7tW8esj9IVdwLqwS.htm)|Primal Weapon|auto-trad|
|[7Y8YE3i8NiZAOPkt.htm](kingmaker-bestiary-items/7Y8YE3i8NiZAOPkt.htm)|Collapse|auto-trad|
|[7ZhJ9zRnsiGY2LMq.htm](kingmaker-bestiary-items/7ZhJ9zRnsiGY2LMq.htm)|Change Shape|auto-trad|
|[83BVCoUzow3GIr5W.htm](kingmaker-bestiary-items/83BVCoUzow3GIr5W.htm)|Claw|auto-trad|
|[873UBJDHNOwZqAn0.htm](kingmaker-bestiary-items/873UBJDHNOwZqAn0.htm)|Longsword|auto-trad|
|[88hwkEhv8cx338VM.htm](kingmaker-bestiary-items/88hwkEhv8cx338VM.htm)|Attack of Opportunity|auto-trad|
|[89ANJyvglhWZiKZv.htm](kingmaker-bestiary-items/89ANJyvglhWZiKZv.htm)|Deploy Flytraps|auto-trad|
|[8F42ZUECGwbl9nmU.htm](kingmaker-bestiary-items/8F42ZUECGwbl9nmU.htm)|Darkvision|auto-trad|
|[8FamBadz3fY1Ktf1.htm](kingmaker-bestiary-items/8FamBadz3fY1Ktf1.htm)|Javelin|auto-trad|
|[8fXr0ygvqmKMJQ7y.htm](kingmaker-bestiary-items/8fXr0ygvqmKMJQ7y.htm)|Ambush Strike|auto-trad|
|[8GzTttsR8tPUpy3a.htm](kingmaker-bestiary-items/8GzTttsR8tPUpy3a.htm)|Manifest Fetch Weapon|auto-trad|
|[8Ib2hxgIftGdnnC4.htm](kingmaker-bestiary-items/8Ib2hxgIftGdnnC4.htm)|Occult Focus Spells|auto-trad|
|[8k6w6YpDwJuyU7Lj.htm](kingmaker-bestiary-items/8k6w6YpDwJuyU7Lj.htm)|Twin Feint|auto-trad|
|[8lbGHlEEQstmS0rd.htm](kingmaker-bestiary-items/8lbGHlEEQstmS0rd.htm)|Club|auto-trad|
|[8PkUYYXKF3YdJiBM.htm](kingmaker-bestiary-items/8PkUYYXKF3YdJiBM.htm)|Draconic Bite|auto-trad|
|[8R3jmOcyIfgNNdKr.htm](kingmaker-bestiary-items/8R3jmOcyIfgNNdKr.htm)|Negative Healing|auto-trad|
|[8RIFanFu6JDiIRH1.htm](kingmaker-bestiary-items/8RIFanFu6JDiIRH1.htm)|Thrash|auto-trad|
|[8s4nFCFF2c0eNDnN.htm](kingmaker-bestiary-items/8s4nFCFF2c0eNDnN.htm)|Divine Prepared Spells|auto-trad|
|[8SCH000Zv6X9BFPf.htm](kingmaker-bestiary-items/8SCH000Zv6X9BFPf.htm)|Fist|auto-trad|
|[8T1NpFqpuMXKzB51.htm](kingmaker-bestiary-items/8T1NpFqpuMXKzB51.htm)|Jaws|auto-trad|
|[8W67Bxaonilv5KO0.htm](kingmaker-bestiary-items/8W67Bxaonilv5KO0.htm)|Exhale Miasma|auto-trad|
|[8wBdJ7S9sRjGCmmT.htm](kingmaker-bestiary-items/8wBdJ7S9sRjGCmmT.htm)|Arcane Innate Spells|auto-trad|
|[8wo9GPXQVySZtBb5.htm](kingmaker-bestiary-items/8wo9GPXQVySZtBb5.htm)|Arcane Prepared Spells|auto-trad|
|[8x5ve1XWJQ7yIA7F.htm](kingmaker-bestiary-items/8x5ve1XWJQ7yIA7F.htm)|Attack of Opportunity|auto-trad|
|[8xYRzzHeY6uZztPe.htm](kingmaker-bestiary-items/8xYRzzHeY6uZztPe.htm)|Unstoppable Charge|auto-trad|
|[8yXu3fI5NfUS8lDH.htm](kingmaker-bestiary-items/8yXu3fI5NfUS8lDH.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[8zIAec8NlrwPEbab.htm](kingmaker-bestiary-items/8zIAec8NlrwPEbab.htm)|Trident|auto-trad|
|[907AYCZJGYIzNC2a.htm](kingmaker-bestiary-items/907AYCZJGYIzNC2a.htm)|Quickened Casting|auto-trad|
|[95EZclhH2zhKgoxm.htm](kingmaker-bestiary-items/95EZclhH2zhKgoxm.htm)|Dagger|auto-trad|
|[9a3n2YfibJlQxctv.htm](kingmaker-bestiary-items/9a3n2YfibJlQxctv.htm)|Unseen Sight|auto-trad|
|[9AVnb90miimzc7rS.htm](kingmaker-bestiary-items/9AVnb90miimzc7rS.htm)|Sneak Attack|auto-trad|
|[9bomxaBEHSLvRrkq.htm](kingmaker-bestiary-items/9bomxaBEHSLvRrkq.htm)|Statue|auto-trad|
|[9cHX93V0XpVb7b7b.htm](kingmaker-bestiary-items/9cHX93V0XpVb7b7b.htm)|Lashing Tongues|auto-trad|
|[9F78hYb3XMI7z4ai.htm](kingmaker-bestiary-items/9F78hYb3XMI7z4ai.htm)|Rapier|auto-trad|
|[9il80y0VHh8ZAWdY.htm](kingmaker-bestiary-items/9il80y0VHh8ZAWdY.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[9jMNfcA0u8pECJhS.htm](kingmaker-bestiary-items/9jMNfcA0u8pECJhS.htm)|Dagger|auto-trad|
|[9JQwWcFOpgrzmEIy.htm](kingmaker-bestiary-items/9JQwWcFOpgrzmEIy.htm)|Dagger|auto-trad|
|[9k7o784yI4Bka8ys.htm](kingmaker-bestiary-items/9k7o784yI4Bka8ys.htm)|Crossbow|auto-trad|
|[9khUkSjPZRFIilUZ.htm](kingmaker-bestiary-items/9khUkSjPZRFIilUZ.htm)|Rend|auto-trad|
|[9kVpK6J5EVHZcp0K.htm](kingmaker-bestiary-items/9kVpK6J5EVHZcp0K.htm)|Major Staff of Transmutation|auto-trad|
|[9ODQkquyZvS7PFNK.htm](kingmaker-bestiary-items/9ODQkquyZvS7PFNK.htm)|Knockdown|auto-trad|
|[9OiXFTpXGJPjEwUL.htm](kingmaker-bestiary-items/9OiXFTpXGJPjEwUL.htm)|Grab|auto-trad|
|[9oO34owbXFTjCGng.htm](kingmaker-bestiary-items/9oO34owbXFTjCGng.htm)|Darkvision|auto-trad|
|[9PUgsYyJWDJ9J0sE.htm](kingmaker-bestiary-items/9PUgsYyJWDJ9J0sE.htm)|+1 Status vs. Posion|auto-trad|
|[9qzHAK95CCa74dtM.htm](kingmaker-bestiary-items/9qzHAK95CCa74dtM.htm)|Absorb the Bloom|auto-trad|
|[9rDjTECEOqQb11SQ.htm](kingmaker-bestiary-items/9rDjTECEOqQb11SQ.htm)|Dagger|auto-trad|
|[9RDkmFyfywWSOj2V.htm](kingmaker-bestiary-items/9RDkmFyfywWSOj2V.htm)|Breath Weapon|auto-trad|
|[9S7ODMh2ZM6NXVpF.htm](kingmaker-bestiary-items/9S7ODMh2ZM6NXVpF.htm)|Enfeebling Humors|auto-trad|
|[9ST6BDxKH8vXvpEq.htm](kingmaker-bestiary-items/9ST6BDxKH8vXvpEq.htm)|Attack of Opportunity (Worm Jaws Only)|auto-trad|
|[9uNCXDfw6yzI9yL5.htm](kingmaker-bestiary-items/9uNCXDfw6yzI9yL5.htm)|Release Spell|auto-trad|
|[9VA9Qjcx5YJxs4P1.htm](kingmaker-bestiary-items/9VA9Qjcx5YJxs4P1.htm)|Jangle the Chain|auto-trad|
|[9VgJpOQ6xJ9gTgCU.htm](kingmaker-bestiary-items/9VgJpOQ6xJ9gTgCU.htm)|Bard Spontaneous Spells|auto-trad|
|[9yPuatISy0aUTsVE.htm](kingmaker-bestiary-items/9yPuatISy0aUTsVE.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[A33mGw1DCgRJRDSe.htm](kingmaker-bestiary-items/A33mGw1DCgRJRDSe.htm)|Tongue Grab|auto-trad|
|[a7WtcnRg6JlsVIDg.htm](kingmaker-bestiary-items/a7WtcnRg6JlsVIDg.htm)|Maul|auto-trad|
|[A8rxcOUACAmsVmTH.htm](kingmaker-bestiary-items/A8rxcOUACAmsVmTH.htm)|Command the Undead|auto-trad|
|[A8YjaMzEWbFC9JWF.htm](kingmaker-bestiary-items/A8YjaMzEWbFC9JWF.htm)|Grab|auto-trad|
|[A9Hfkb1IhZNWZW7U.htm](kingmaker-bestiary-items/A9Hfkb1IhZNWZW7U.htm)|Divine Prepared Spells|auto-trad|
|[AaMjBE4xQc5ZqroJ.htm](kingmaker-bestiary-items/AaMjBE4xQc5ZqroJ.htm)|Sneak Attack|auto-trad|
|[aan6tOAkYHhETUdL.htm](kingmaker-bestiary-items/aan6tOAkYHhETUdL.htm)|Deadly Aim|auto-trad|
|[AB7mMjajOUtvqlxD.htm](kingmaker-bestiary-items/AB7mMjajOUtvqlxD.htm)|Evasion|auto-trad|
|[AcBYEH0GxgM4iV5v.htm](kingmaker-bestiary-items/AcBYEH0GxgM4iV5v.htm)|Ice Tunneler|auto-trad|
|[aDF1WE7DJM17CR29.htm](kingmaker-bestiary-items/aDF1WE7DJM17CR29.htm)|Darkvision|auto-trad|
|[Aern77CIRMOy8Se3.htm](kingmaker-bestiary-items/Aern77CIRMOy8Se3.htm)|Claw|auto-trad|
|[AFv5FWlpT2zyryiG.htm](kingmaker-bestiary-items/AFv5FWlpT2zyryiG.htm)|Swarm Mind|auto-trad|
|[AfvMfBPmJB8eecwr.htm](kingmaker-bestiary-items/AfvMfBPmJB8eecwr.htm)|Sneak Attack|auto-trad|
|[afWpHJ6qqzsRORUX.htm](kingmaker-bestiary-items/afWpHJ6qqzsRORUX.htm)|Steady Spellcasting|auto-trad|
|[AFz5oCbdGYZQVrDD.htm](kingmaker-bestiary-items/AFz5oCbdGYZQVrDD.htm)|Agonized Wail|auto-trad|
|[Ag07kBthiVumPkcV.htm](kingmaker-bestiary-items/Ag07kBthiVumPkcV.htm)|Focus Hatred|auto-trad|
|[aHamTcS3vkrZLHGN.htm](kingmaker-bestiary-items/aHamTcS3vkrZLHGN.htm)|Instant Suggestion|auto-trad|
|[aHuQg1Wi1gb8KAbH.htm](kingmaker-bestiary-items/aHuQg1Wi1gb8KAbH.htm)|Time Siphon|auto-trad|
|[ai3N8T91JXo3Hx5e.htm](kingmaker-bestiary-items/ai3N8T91JXo3Hx5e.htm)|Negative Healing|auto-trad|
|[AIde6VUDBdxDlQQS.htm](kingmaker-bestiary-items/AIde6VUDBdxDlQQS.htm)|Dagger|auto-trad|
|[AiKIcXFJx9lEEfnt.htm](kingmaker-bestiary-items/AiKIcXFJx9lEEfnt.htm)|Surge|auto-trad|
|[AjjeSlBUiJKE8SOB.htm](kingmaker-bestiary-items/AjjeSlBUiJKE8SOB.htm)|Divine Prepared Spells|auto-trad|
|[AkE2QdLe1wysN32X.htm](kingmaker-bestiary-items/AkE2QdLe1wysN32X.htm)|Blindness|auto-trad|
|[AksHJ5xCuwQbdOmq.htm](kingmaker-bestiary-items/AksHJ5xCuwQbdOmq.htm)|Claw|auto-trad|
|[ALbbJNuSFbicqOfo.htm](kingmaker-bestiary-items/ALbbJNuSFbicqOfo.htm)|Ripping Disarm|auto-trad|
|[aLcLFpXMpgdiDrzO.htm](kingmaker-bestiary-items/aLcLFpXMpgdiDrzO.htm)|Symbiotic Swarm|auto-trad|
|[aM4rXRFUMH2Ufswh.htm](kingmaker-bestiary-items/aM4rXRFUMH2Ufswh.htm)|Improved Grab|auto-trad|
|[AMuN6VT4RTloaWQk.htm](kingmaker-bestiary-items/AMuN6VT4RTloaWQk.htm)|Bomber|auto-trad|
|[ao5De1dHxQjNJ8za.htm](kingmaker-bestiary-items/ao5De1dHxQjNJ8za.htm)|Souleater|auto-trad|
|[AoTJ2NwBcO5rItKq.htm](kingmaker-bestiary-items/AoTJ2NwBcO5rItKq.htm)|Darkvision|auto-trad|
|[ApHre7Dw1aJJlSiv.htm](kingmaker-bestiary-items/ApHre7Dw1aJJlSiv.htm)|Hatchet|auto-trad|
|[apQdVdQWSEbQzCw2.htm](kingmaker-bestiary-items/apQdVdQWSEbQzCw2.htm)|Focus Gaze|auto-trad|
|[aQsTI9u9o9zMbaHa.htm](kingmaker-bestiary-items/aQsTI9u9o9zMbaHa.htm)|Resolve|auto-trad|
|[Ar58HUSjCtqUCBWd.htm](kingmaker-bestiary-items/Ar58HUSjCtqUCBWd.htm)|Jaws|auto-trad|
|[Asj3JNQgw7B1ZXXC.htm](kingmaker-bestiary-items/Asj3JNQgw7B1ZXXC.htm)|Primal Innate Spells|auto-trad|
|[atdIIgSxVkVrP1Ti.htm](kingmaker-bestiary-items/atdIIgSxVkVrP1Ti.htm)|Druid Order Spells|auto-trad|
|[AtiJbhL8pY43Rvv3.htm](kingmaker-bestiary-items/AtiJbhL8pY43Rvv3.htm)|Smash Kneecaps|auto-trad|
|[aTtxivkD4mgfosfH.htm](kingmaker-bestiary-items/aTtxivkD4mgfosfH.htm)|Change Shape|auto-trad|
|[auHvPa2qajS0vG6w.htm](kingmaker-bestiary-items/auHvPa2qajS0vG6w.htm)|Primal Spontaneous Spells|auto-trad|
|[auvnM9kZNqXdqAIR.htm](kingmaker-bestiary-items/auvnM9kZNqXdqAIR.htm)|Outside of Time|auto-trad|
|[AvThDzMVhWejEVsL.htm](kingmaker-bestiary-items/AvThDzMVhWejEVsL.htm)|Low-Light Vision|auto-trad|
|[aW63u7ufl1uEOBQh.htm](kingmaker-bestiary-items/aW63u7ufl1uEOBQh.htm)|Furious Finish|auto-trad|
|[AwAeIIlDkmCoVoca.htm](kingmaker-bestiary-items/AwAeIIlDkmCoVoca.htm)|Regeneration 20 (Deactivated by Acid or Fire)|auto-trad|
|[AwKlC3yGdtDzjPWM.htm](kingmaker-bestiary-items/AwKlC3yGdtDzjPWM.htm)|Masterful Hunter|auto-trad|
|[AWNveQn0EQ1aYORU.htm](kingmaker-bestiary-items/AWNveQn0EQ1aYORU.htm)|Rise Up|auto-trad|
|[aWo3aVa2XXkGsrLY.htm](kingmaker-bestiary-items/aWo3aVa2XXkGsrLY.htm)|Final Spite|auto-trad|
|[AX9lSAPNk2PJCat1.htm](kingmaker-bestiary-items/AX9lSAPNk2PJCat1.htm)|Outside of Time|auto-trad|
|[axy7Bj9PMjBKlFdd.htm](kingmaker-bestiary-items/axy7Bj9PMjBKlFdd.htm)|Club|auto-trad|
|[ay85G9ub5DX6nf12.htm](kingmaker-bestiary-items/ay85G9ub5DX6nf12.htm)|No Escape|auto-trad|
|[AYVzOquWiUdDuNN3.htm](kingmaker-bestiary-items/AYVzOquWiUdDuNN3.htm)|Improved Grab|auto-trad|
|[Az48BJ0aNRNzQotH.htm](kingmaker-bestiary-items/Az48BJ0aNRNzQotH.htm)|Wounded Arm|auto-trad|
|[azD5OELMU31IrNwz.htm](kingmaker-bestiary-items/azD5OELMU31IrNwz.htm)|Dagger|auto-trad|
|[aZke0WE5mklrrBK7.htm](kingmaker-bestiary-items/aZke0WE5mklrrBK7.htm)|Draining Inhalation|auto-trad|
|[b07B1akTo84FKZUe.htm](kingmaker-bestiary-items/b07B1akTo84FKZUe.htm)|Attack of Opportunity|auto-trad|
|[BA3fqrqPyWZ59YSs.htm](kingmaker-bestiary-items/BA3fqrqPyWZ59YSs.htm)|Deadeye's Shame|auto-trad|
|[bC1xvgHpQUnix9Bt.htm](kingmaker-bestiary-items/bC1xvgHpQUnix9Bt.htm)|Deep Breath|auto-trad|
|[bekh1R8LAluGLNaA.htm](kingmaker-bestiary-items/bekh1R8LAluGLNaA.htm)|Low-Light Vision|auto-trad|
|[bEsmMOS1PqhLHHbh.htm](kingmaker-bestiary-items/bEsmMOS1PqhLHHbh.htm)|Chewing Bites|auto-trad|
|[BFpgenVI0pBLyV0S.htm](kingmaker-bestiary-items/BFpgenVI0pBLyV0S.htm)|Infused Items|auto-trad|
|[BGZlipUdtmQIe6By.htm](kingmaker-bestiary-items/BGZlipUdtmQIe6By.htm)|Divine Prepared Spells|auto-trad|
|[BJiQlSKAx6WhMT1p.htm](kingmaker-bestiary-items/BJiQlSKAx6WhMT1p.htm)|Low-Light Vision|auto-trad|
|[bJUTz62klSlUEuTo.htm](kingmaker-bestiary-items/bJUTz62klSlUEuTo.htm)|Drink Blood|auto-trad|
|[bKPOLbNfdVcrEpjV.htm](kingmaker-bestiary-items/bKPOLbNfdVcrEpjV.htm)|Bonds of Iron|auto-trad|
|[BKsKbanuSDh8osp9.htm](kingmaker-bestiary-items/BKsKbanuSDh8osp9.htm)|Hope or Despair|auto-trad|
|[bl1xW54BlfiFnU8h.htm](kingmaker-bestiary-items/bl1xW54BlfiFnU8h.htm)|Catch Rock|auto-trad|
|[BleHEEpt3dwEMeDo.htm](kingmaker-bestiary-items/BleHEEpt3dwEMeDo.htm)|Thorny Aura|auto-trad|
|[BLleGAYzhc0cIyIz.htm](kingmaker-bestiary-items/BLleGAYzhc0cIyIz.htm)|Darkvision|auto-trad|
|[bLrblJlMOfhn39My.htm](kingmaker-bestiary-items/bLrblJlMOfhn39My.htm)|Swarm Mind|auto-trad|
|[BltNCa3ZEsbxzhf7.htm](kingmaker-bestiary-items/BltNCa3ZEsbxzhf7.htm)|Buck|auto-trad|
|[bMbUyfSqVR9lWDB7.htm](kingmaker-bestiary-items/bMbUyfSqVR9lWDB7.htm)|Rejuvenation|auto-trad|
|[BMGyGT2KQinKwWfe.htm](kingmaker-bestiary-items/BMGyGT2KQinKwWfe.htm)|Shortsword|auto-trad|
|[bmmCoxENGAgHtWP1.htm](kingmaker-bestiary-items/bmmCoxENGAgHtWP1.htm)|Web Sense|auto-trad|
|[BO3hN1F0bkrNu60q.htm](kingmaker-bestiary-items/BO3hN1F0bkrNu60q.htm)|Fast Healing 15|auto-trad|
|[bOpAJRLpemL4HwNd.htm](kingmaker-bestiary-items/bOpAJRLpemL4HwNd.htm)|Swamp Stride|auto-trad|
|[BOya1lhwnf2RNvGy.htm](kingmaker-bestiary-items/BOya1lhwnf2RNvGy.htm)|Juggernaut|auto-trad|
|[Bpm9xvzcp3dBpCvp.htm](kingmaker-bestiary-items/Bpm9xvzcp3dBpCvp.htm)|Sneak Attack|auto-trad|
|[BqhAFrC5aDQ1BKoT.htm](kingmaker-bestiary-items/BqhAFrC5aDQ1BKoT.htm)|Blowgun|auto-trad|
|[BtJvNGAGxO2VOuXB.htm](kingmaker-bestiary-items/BtJvNGAGxO2VOuXB.htm)|Mind-Numbing Breath|auto-trad|
|[BuBkGYAPCPnJ03go.htm](kingmaker-bestiary-items/BuBkGYAPCPnJ03go.htm)|Claw|auto-trad|
|[bvBZPt5ocFCNJflm.htm](kingmaker-bestiary-items/bvBZPt5ocFCNJflm.htm)|Ready and Load|auto-trad|
|[BxuCFIo6yswNsArJ.htm](kingmaker-bestiary-items/BxuCFIo6yswNsArJ.htm)|Hunting Cry|auto-trad|
|[by7p3pdmrMKbVYk8.htm](kingmaker-bestiary-items/by7p3pdmrMKbVYk8.htm)|Tail|auto-trad|
|[bZEeE1MlmmqIJiiK.htm](kingmaker-bestiary-items/bZEeE1MlmmqIJiiK.htm)|Cleric Domain Spells|auto-trad|
|[c1sQmMq46Aeg6en6.htm](kingmaker-bestiary-items/c1sQmMq46Aeg6en6.htm)|Axe Specialization|auto-trad|
|[c2r3T8G5zXq6UOBS.htm](kingmaker-bestiary-items/c2r3T8G5zXq6UOBS.htm)|Mobility|auto-trad|
|[C31XU8XsrO1R112R.htm](kingmaker-bestiary-items/C31XU8XsrO1R112R.htm)|Focused Target|auto-trad|
|[c3vIbIBnVpy9fjsE.htm](kingmaker-bestiary-items/c3vIbIBnVpy9fjsE.htm)|Rage|auto-trad|
|[C4fZA5gB6JeZf08P.htm](kingmaker-bestiary-items/C4fZA5gB6JeZf08P.htm)|Club|auto-trad|
|[C91ggah5Ye65LrMH.htm](kingmaker-bestiary-items/C91ggah5Ye65LrMH.htm)|Leng Ruby|auto-trad|
|[C9Ds23BokMiIUMi5.htm](kingmaker-bestiary-items/C9Ds23BokMiIUMi5.htm)|Release Jewel|auto-trad|
|[C9ESEAYQaJUciIba.htm](kingmaker-bestiary-items/C9ESEAYQaJUciIba.htm)|Sneak Attack|auto-trad|
|[cA2cBXkewzB0Fwtj.htm](kingmaker-bestiary-items/cA2cBXkewzB0Fwtj.htm)|Change Shape|auto-trad|
|[CBuSOJ26ZXTgXAze.htm](kingmaker-bestiary-items/CBuSOJ26ZXTgXAze.htm)|Constant Spells|auto-trad|
|[cc2QQL18PozIgxVb.htm](kingmaker-bestiary-items/cc2QQL18PozIgxVb.htm)|Wizard School Spells|auto-trad|
|[cC853WQiGJtDTkRc.htm](kingmaker-bestiary-items/cC853WQiGJtDTkRc.htm)|Frightful Presence|auto-trad|
|[CcrqR1iP71tzTA5V.htm](kingmaker-bestiary-items/CcrqR1iP71tzTA5V.htm)|Collapse|auto-trad|
|[cD3KZDeLep4rM0aB.htm](kingmaker-bestiary-items/cD3KZDeLep4rM0aB.htm)|Darkvision|auto-trad|
|[cDS4azJaDaRZITrQ.htm](kingmaker-bestiary-items/cDS4azJaDaRZITrQ.htm)|Cold Vulnerability|auto-trad|
|[CDUrtwX9v7aZEVjj.htm](kingmaker-bestiary-items/CDUrtwX9v7aZEVjj.htm)|Change Shape|auto-trad|
|[CeBgXo66gTiufqL5.htm](kingmaker-bestiary-items/CeBgXo66gTiufqL5.htm)|Nature's Edge|auto-trad|
|[cFKmkUCecFtQweau.htm](kingmaker-bestiary-items/cFKmkUCecFtQweau.htm)|Occult Spontaneous Spells|auto-trad|
|[Ch2vwQBo7w7hd8ou.htm](kingmaker-bestiary-items/Ch2vwQBo7w7hd8ou.htm)|Harrying Swordfighter|auto-trad|
|[CiirPvlIj0ZtuHce.htm](kingmaker-bestiary-items/CiirPvlIj0ZtuHce.htm)|Vengeful Anger|auto-trad|
|[cJIYg8gvnBbvoiuI.htm](kingmaker-bestiary-items/cJIYg8gvnBbvoiuI.htm)|Mobility|auto-trad|
|[CJravyxL4AFAla1i.htm](kingmaker-bestiary-items/CJravyxL4AFAla1i.htm)|Curse of the Wererat|auto-trad|
|[ckg1JdT2pmJlw8zC.htm](kingmaker-bestiary-items/ckg1JdT2pmJlw8zC.htm)|Dagger|auto-trad|
|[ckkFk6fhxAXrQHzv.htm](kingmaker-bestiary-items/ckkFk6fhxAXrQHzv.htm)|Composite Shortbow|auto-trad|
|[ComzTzx9Gu4QDQxN.htm](kingmaker-bestiary-items/ComzTzx9Gu4QDQxN.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[cpFEKCq7CWkJ3qrw.htm](kingmaker-bestiary-items/cpFEKCq7CWkJ3qrw.htm)|Trident|auto-trad|
|[cprgu1LyMWRqzI0w.htm](kingmaker-bestiary-items/cprgu1LyMWRqzI0w.htm)|Claw|auto-trad|
|[cQluYUjYjTeHDhTp.htm](kingmaker-bestiary-items/cQluYUjYjTeHDhTp.htm)|Spinning Sweep|auto-trad|
|[CquGBdxcC31YxWhu.htm](kingmaker-bestiary-items/CquGBdxcC31YxWhu.htm)|Rotting Stench|auto-trad|
|[cr8chy1goN1ZSrnT.htm](kingmaker-bestiary-items/cr8chy1goN1ZSrnT.htm)|Mound|auto-trad|
|[cRTNLnVuzisUG7kl.htm](kingmaker-bestiary-items/cRTNLnVuzisUG7kl.htm)|Improved Knockdown|auto-trad|
|[CvfLXQsndA3wb5S1.htm](kingmaker-bestiary-items/CvfLXQsndA3wb5S1.htm)|Foot|auto-trad|
|[czVzxuwWmCRLgnuc.htm](kingmaker-bestiary-items/czVzxuwWmCRLgnuc.htm)|Forced Regeneration|auto-trad|
|[d2pAqCh02beVW16z.htm](kingmaker-bestiary-items/d2pAqCh02beVW16z.htm)|Jaws|auto-trad|
|[D4C1yHkCqpQckxgr.htm](kingmaker-bestiary-items/D4C1yHkCqpQckxgr.htm)|Tail Lash|auto-trad|
|[D4f8OzwwMvBLI86y.htm](kingmaker-bestiary-items/D4f8OzwwMvBLI86y.htm)|Cold Iron Shortsword|auto-trad|
|[D6qVDUiJjJhA5CIX.htm](kingmaker-bestiary-items/D6qVDUiJjJhA5CIX.htm)|Wild Reincarnation|auto-trad|
|[d7lY0T9fiGJzH2O1.htm](kingmaker-bestiary-items/d7lY0T9fiGJzH2O1.htm)|Arcane Innate Spells|auto-trad|
|[d7qSq3HVLR2AAayU.htm](kingmaker-bestiary-items/d7qSq3HVLR2AAayU.htm)|Low-Light Vision|auto-trad|
|[d8wBT2ExQFfDG2Mt.htm](kingmaker-bestiary-items/d8wBT2ExQFfDG2Mt.htm)|Wild Hunt Link|auto-trad|
|[d9FqMXrNIAm6rnHs.htm](kingmaker-bestiary-items/d9FqMXrNIAm6rnHs.htm)|Divine Innate Spells|auto-trad|
|[dahtG8lHhjQBQemo.htm](kingmaker-bestiary-items/dahtG8lHhjQBQemo.htm)|Hurled Thorn|auto-trad|
|[DbU5apk8FlJos4xz.htm](kingmaker-bestiary-items/DbU5apk8FlJos4xz.htm)|Critical Debilitating Strike|auto-trad|
|[DdEls9nTJvA7pEi0.htm](kingmaker-bestiary-items/DdEls9nTJvA7pEi0.htm)|Blowgun|auto-trad|
|[DEac5zIyNqdQYdnc.htm](kingmaker-bestiary-items/DEac5zIyNqdQYdnc.htm)|Grab|auto-trad|
|[DeAD5IPwOAtQ1Zjr.htm](kingmaker-bestiary-items/DeAD5IPwOAtQ1Zjr.htm)|Kukri|auto-trad|
|[deytOmtiZjHFzGRU.htm](kingmaker-bestiary-items/deytOmtiZjHFzGRU.htm)|Athach Venom|auto-trad|
|[DEz7gcf0AumMmDO8.htm](kingmaker-bestiary-items/DEz7gcf0AumMmDO8.htm)|Primal Innate Spells|auto-trad|
|[DFgNddcsAbZVlWSH.htm](kingmaker-bestiary-items/DFgNddcsAbZVlWSH.htm)|Pounce|auto-trad|
|[DfyikbxEMRuzW77m.htm](kingmaker-bestiary-items/DfyikbxEMRuzW77m.htm)|Swarm Shape|auto-trad|
|[DgApO6iOQpwx3KfR.htm](kingmaker-bestiary-items/DgApO6iOQpwx3KfR.htm)|Resolve|auto-trad|
|[dGglOjtXAyHqoW5X.htm](kingmaker-bestiary-items/dGglOjtXAyHqoW5X.htm)|Steady Spellcasting|auto-trad|
|[DGNIDiy16GZYbh8b.htm](kingmaker-bestiary-items/DGNIDiy16GZYbh8b.htm)|Trapdoor Lunge|auto-trad|
|[dH7eKw8pDJQDkR3P.htm](kingmaker-bestiary-items/dH7eKw8pDJQDkR3P.htm)|Constrict|auto-trad|
|[dhTLYiCmWDkONUN5.htm](kingmaker-bestiary-items/dhTLYiCmWDkONUN5.htm)|Jaws|auto-trad|
|[DI6Nw0XKEZZJLY8D.htm](kingmaker-bestiary-items/DI6Nw0XKEZZJLY8D.htm)|Motion Sense 60 feet|auto-trad|
|[DJVCW8WVRR0Prvrs.htm](kingmaker-bestiary-items/DJVCW8WVRR0Prvrs.htm)|Prismatic Burst|auto-trad|
|[dJzydVxO1TtPnaho.htm](kingmaker-bestiary-items/dJzydVxO1TtPnaho.htm)|Deny Advantage|auto-trad|
|[DksWsSv1BvpLeNhJ.htm](kingmaker-bestiary-items/DksWsSv1BvpLeNhJ.htm)|Jaws|auto-trad|
|[DLI2axhKyDzeg5Ub.htm](kingmaker-bestiary-items/DLI2axhKyDzeg5Ub.htm)|Primal Innate Spells|auto-trad|
|[DNaScDDIg4t6u0Hw.htm](kingmaker-bestiary-items/DNaScDDIg4t6u0Hw.htm)|Ballista Bolt|auto-trad|
|[dnLiUmNpLT4ZnUQ6.htm](kingmaker-bestiary-items/dnLiUmNpLT4ZnUQ6.htm)|Darkvision|auto-trad|
|[dNOthX618UT1hZN6.htm](kingmaker-bestiary-items/dNOthX618UT1hZN6.htm)|Distracting Shot|auto-trad|
|[DoCmnJxIOLLTb9Xm.htm](kingmaker-bestiary-items/DoCmnJxIOLLTb9Xm.htm)|Greater Acid Flask|auto-trad|
|[DOviMcCQeyu6rmjx.htm](kingmaker-bestiary-items/DOviMcCQeyu6rmjx.htm)|Attack of Opportunity|auto-trad|
|[DOWxg5PQXXUG8Ddy.htm](kingmaker-bestiary-items/DOWxg5PQXXUG8Ddy.htm)|Beak|auto-trad|
|[DPcRdZIL38qcDj2I.htm](kingmaker-bestiary-items/DPcRdZIL38qcDj2I.htm)|Snatch|auto-trad|
|[DrW6hlIWaGCF1pU4.htm](kingmaker-bestiary-items/DrW6hlIWaGCF1pU4.htm)|Juggernaut|auto-trad|
|[DsvyLX53TeuZHMQ3.htm](kingmaker-bestiary-items/DsvyLX53TeuZHMQ3.htm)|Clawed Feet|auto-trad|
|[dtTve1TvHU0al6Es.htm](kingmaker-bestiary-items/dtTve1TvHU0al6Es.htm)|Darkvision|auto-trad|
|[DTXq4teaiLU9haNE.htm](kingmaker-bestiary-items/DTXq4teaiLU9haNE.htm)|Wide Swing|auto-trad|
|[du4VcQ2P0kOpRVrS.htm](kingmaker-bestiary-items/du4VcQ2P0kOpRVrS.htm)|Buck|auto-trad|
|[DuDuSE2EKfZt9r6m.htm](kingmaker-bestiary-items/DuDuSE2EKfZt9r6m.htm)|Quickened Casting|auto-trad|
|[DUWbE68XkGU2jM9J.htm](kingmaker-bestiary-items/DUWbE68XkGU2jM9J.htm)|Fist|auto-trad|
|[DVsGmUvb4tqHJrCw.htm](kingmaker-bestiary-items/DVsGmUvb4tqHJrCw.htm)|Rejuvenation|auto-trad|
|[dW28Q4hMeEWpDtay.htm](kingmaker-bestiary-items/dW28Q4hMeEWpDtay.htm)|At-Will Spells|auto-trad|
|[dWpKdYaRxbAJiHSi.htm](kingmaker-bestiary-items/dWpKdYaRxbAJiHSi.htm)|Bloodbird|auto-trad|
|[dX22wsxoPsGLzOQ0.htm](kingmaker-bestiary-items/dX22wsxoPsGLzOQ0.htm)|Dagger|auto-trad|
|[DxTspLZpQI4zVPoa.htm](kingmaker-bestiary-items/DxTspLZpQI4zVPoa.htm)|Sorcerer Bloodline Spells|auto-trad|
|[dynvtBdr79KGnqBo.htm](kingmaker-bestiary-items/dynvtBdr79KGnqBo.htm)|Can't Catch Me|auto-trad|
|[e0fcFHdf91dsur91.htm](kingmaker-bestiary-items/e0fcFHdf91dsur91.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[e1txRyfONnCuYPny.htm](kingmaker-bestiary-items/e1txRyfONnCuYPny.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[E2BfR5UhdImAFitO.htm](kingmaker-bestiary-items/E2BfR5UhdImAFitO.htm)|Shyka's Judgement|auto-trad|
|[e2cSvAso2wePkLAK.htm](kingmaker-bestiary-items/e2cSvAso2wePkLAK.htm)|Korog's Command|auto-trad|
|[E3C47mJQhyySioGh.htm](kingmaker-bestiary-items/E3C47mJQhyySioGh.htm)|Hatchet|auto-trad|
|[e3mEtYHIO4f0NZZc.htm](kingmaker-bestiary-items/e3mEtYHIO4f0NZZc.htm)|Arcane Prepared Spells|auto-trad|
|[E3r5U0LkfWOXhgLt.htm](kingmaker-bestiary-items/E3r5U0LkfWOXhgLt.htm)|Shortbow|auto-trad|
|[E4bINR3oniilC0ba.htm](kingmaker-bestiary-items/E4bINR3oniilC0ba.htm)|Shell Defense|auto-trad|
|[E5KXLULpkukpqygI.htm](kingmaker-bestiary-items/E5KXLULpkukpqygI.htm)|Claw|auto-trad|
|[E8ICm8MwDDWyjgKR.htm](kingmaker-bestiary-items/E8ICm8MwDDWyjgKR.htm)|Rock|auto-trad|
|[EaWA2WkRjbpm2GiI.htm](kingmaker-bestiary-items/EaWA2WkRjbpm2GiI.htm)|Duplicate Victim|auto-trad|
|[ECXG4lnlaSBXuqcP.htm](kingmaker-bestiary-items/ECXG4lnlaSBXuqcP.htm)|Defensive Slice|auto-trad|
|[EdlfzUPi8lEnewu1.htm](kingmaker-bestiary-items/EdlfzUPi8lEnewu1.htm)|Dual Tusks|auto-trad|
|[EdoAyjfImWlnNF3Z.htm](kingmaker-bestiary-items/EdoAyjfImWlnNF3Z.htm)|End the Charade|auto-trad|
|[Eeclk1UrcRUe4od5.htm](kingmaker-bestiary-items/Eeclk1UrcRUe4od5.htm)|Lock and Shriek|auto-trad|
|[eG5iE2SpiXdvquWh.htm](kingmaker-bestiary-items/eG5iE2SpiXdvquWh.htm)|Funereal Dirge|auto-trad|
|[egV0y60X9Okkmuoc.htm](kingmaker-bestiary-items/egV0y60X9Okkmuoc.htm)|Giant Trapdoor Spider Venom|auto-trad|
|[EGY18Yebn9ZVwEbP.htm](kingmaker-bestiary-items/EGY18Yebn9ZVwEbP.htm)|Rat Empathy|auto-trad|
|[EgymgxzMTQGUvdBD.htm](kingmaker-bestiary-items/EgymgxzMTQGUvdBD.htm)|Telekinetic Assault|auto-trad|
|[eHtfWWTVUSw5Zu4F.htm](kingmaker-bestiary-items/eHtfWWTVUSw5Zu4F.htm)|Forceful Focus|auto-trad|
|[EihtJU42yTV6LfVg.htm](kingmaker-bestiary-items/EihtJU42yTV6LfVg.htm)|Fetch Weapon|auto-trad|
|[ek9L2OPkQLBaP0Ml.htm](kingmaker-bestiary-items/ek9L2OPkQLBaP0Ml.htm)|Bound to the Mortal Night|auto-trad|
|[ELQWppSD9MT72Ci1.htm](kingmaker-bestiary-items/ELQWppSD9MT72Ci1.htm)|Staff of Fire (Major)|auto-trad|
|[emb5f7dGdRTiUTnN.htm](kingmaker-bestiary-items/emb5f7dGdRTiUTnN.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[eMFelq83Tb6uIwqb.htm](kingmaker-bestiary-items/eMFelq83Tb6uIwqb.htm)|Moon Frenzy|auto-trad|
|[eN9AmHu7Wb5ZVY5F.htm](kingmaker-bestiary-items/eN9AmHu7Wb5ZVY5F.htm)|Whiffling|auto-trad|
|[Enk2lR0Vry7rabGe.htm](kingmaker-bestiary-items/Enk2lR0Vry7rabGe.htm)|Site Bound|auto-trad|
|[Epu5XvonDsZQClpu.htm](kingmaker-bestiary-items/Epu5XvonDsZQClpu.htm)|Flechette|auto-trad|
|[eqCGFR9UmHOuUpMI.htm](kingmaker-bestiary-items/eqCGFR9UmHOuUpMI.htm)|Hatchet|auto-trad|
|[eQQryZfmKwV4meJL.htm](kingmaker-bestiary-items/eQQryZfmKwV4meJL.htm)|Quickened Casting|auto-trad|
|[esNHCG2RP8SOfTuA.htm](kingmaker-bestiary-items/esNHCG2RP8SOfTuA.htm)|Axe Vulnerability|auto-trad|
|[ESPLHQJWpOyak6Wa.htm](kingmaker-bestiary-items/ESPLHQJWpOyak6Wa.htm)|Battle Axe|auto-trad|
|[eSXITfBnqJ3s71nu.htm](kingmaker-bestiary-items/eSXITfBnqJ3s71nu.htm)|Divine Focus Spells|auto-trad|
|[EtUgChhYGZ37rp9z.htm](kingmaker-bestiary-items/EtUgChhYGZ37rp9z.htm)|Sure Possession|auto-trad|
|[eUDWR9NZHLWYc7Dj.htm](kingmaker-bestiary-items/eUDWR9NZHLWYc7Dj.htm)|Darkvision|auto-trad|
|[EUf1gqfe5un9Aprz.htm](kingmaker-bestiary-items/EUf1gqfe5un9Aprz.htm)|Vine|auto-trad|
|[EVaLrmDugpw1gfO1.htm](kingmaker-bestiary-items/EVaLrmDugpw1gfO1.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[EVtI5ReFhXPbGsa8.htm](kingmaker-bestiary-items/EVtI5ReFhXPbGsa8.htm)|Primal Innate Spells|auto-trad|
|[EwwHk83vJUlZ0cbV.htm](kingmaker-bestiary-items/EwwHk83vJUlZ0cbV.htm)|Improved Grab|auto-trad|
|[ex0dTLedYlwxNB3T.htm](kingmaker-bestiary-items/ex0dTLedYlwxNB3T.htm)|Swallow Whole|auto-trad|
|[exrZ9BjFfwPvWYPf.htm](kingmaker-bestiary-items/exrZ9BjFfwPvWYPf.htm)|Ripping Grab|auto-trad|
|[ExsxFy6Bmcgsy0GX.htm](kingmaker-bestiary-items/ExsxFy6Bmcgsy0GX.htm)|Low-Light Vision|auto-trad|
|[Ez68XkpqwOnuPfr3.htm](kingmaker-bestiary-items/Ez68XkpqwOnuPfr3.htm)|Shield Block|auto-trad|
|[eznh9ydPPecUq1K1.htm](kingmaker-bestiary-items/eznh9ydPPecUq1K1.htm)|Steady Spellcasting|auto-trad|
|[F0yeghQAPNgRWKCf.htm](kingmaker-bestiary-items/F0yeghQAPNgRWKCf.htm)|Deafening Cry|auto-trad|
|[f42wFnhs1khcDf8G.htm](kingmaker-bestiary-items/f42wFnhs1khcDf8G.htm)|Fast Healing 20|auto-trad|
|[F4qi1llZbd9QZy4d.htm](kingmaker-bestiary-items/F4qi1llZbd9QZy4d.htm)|Raise a Shield|auto-trad|
|[F4z6xO5BYtoag9hK.htm](kingmaker-bestiary-items/F4z6xO5BYtoag9hK.htm)|Jaws|auto-trad|
|[f7AUQc1tVLR8FHEA.htm](kingmaker-bestiary-items/f7AUQc1tVLR8FHEA.htm)|Fog Vision|auto-trad|
|[F8xpT7h5a5ZviXU5.htm](kingmaker-bestiary-items/F8xpT7h5a5ZviXU5.htm)|Feed on Fear|auto-trad|
|[FaGxK1ootpG6uFmP.htm](kingmaker-bestiary-items/FaGxK1ootpG6uFmP.htm)|Low-Light Vision|auto-trad|
|[fakAfk3puTMpw1vT.htm](kingmaker-bestiary-items/fakAfk3puTMpw1vT.htm)|Slow Metabolism|auto-trad|
|[FapwAUPhHeXc1NKj.htm](kingmaker-bestiary-items/FapwAUPhHeXc1NKj.htm)|Warlord's Training|auto-trad|
|[fbNFZNeEcQgHVKW4.htm](kingmaker-bestiary-items/fbNFZNeEcQgHVKW4.htm)|Darkvision|auto-trad|
|[fckcc8VGCaiue1sX.htm](kingmaker-bestiary-items/fckcc8VGCaiue1sX.htm)|Divine Prepared Spells|auto-trad|
|[FCU5JyYgiHDot5rm.htm](kingmaker-bestiary-items/FCU5JyYgiHDot5rm.htm)|Swarming Bites|auto-trad|
|[FdJtaUOMkZdu6F7C.htm](kingmaker-bestiary-items/FdJtaUOMkZdu6F7C.htm)|Fangs|auto-trad|
|[FdQL1f8wOuuy69Tz.htm](kingmaker-bestiary-items/FdQL1f8wOuuy69Tz.htm)|Sneak Attack|auto-trad|
|[FEC49j9OjVBivyUX.htm](kingmaker-bestiary-items/FEC49j9OjVBivyUX.htm)|Planar Acclimation|auto-trad|
|[FGm1xbUPgE0BRiSf.htm](kingmaker-bestiary-items/FGm1xbUPgE0BRiSf.htm)|Focused Assault|auto-trad|
|[FH5MdxzW5lzQE9Hf.htm](kingmaker-bestiary-items/FH5MdxzW5lzQE9Hf.htm)|Regeneration 15 (deactivated by Acid or Fire)|auto-trad|
|[FKNzpxoNtzzC5ytc.htm](kingmaker-bestiary-items/FKNzpxoNtzzC5ytc.htm)|Dagger|auto-trad|
|[FktubLHfxuouP7QK.htm](kingmaker-bestiary-items/FktubLHfxuouP7QK.htm)|Stubborn Conviction|auto-trad|
|[Fl8QSyuys5Y8YCrK.htm](kingmaker-bestiary-items/Fl8QSyuys5Y8YCrK.htm)|Ghostly Hand|auto-trad|
|[fLIOuEGoDxmXYqNf.htm](kingmaker-bestiary-items/fLIOuEGoDxmXYqNf.htm)|Snack|auto-trad|
|[fm5cOYL11qhzHV3G.htm](kingmaker-bestiary-items/fm5cOYL11qhzHV3G.htm)|Low-Light Vision|auto-trad|
|[FMwhWB2KeMuU3kYQ.htm](kingmaker-bestiary-items/FMwhWB2KeMuU3kYQ.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[FMyBd24spHonNXKf.htm](kingmaker-bestiary-items/FMyBd24spHonNXKf.htm)|Occult Spontaneous Spells|auto-trad|
|[FpcxdJ4oqnyX8sBs.htm](kingmaker-bestiary-items/FpcxdJ4oqnyX8sBs.htm)|Long Neck|auto-trad|
|[fqcdNor1vTHZe5Zl.htm](kingmaker-bestiary-items/fqcdNor1vTHZe5Zl.htm)|Occult Spontaneous Spells|auto-trad|
|[fqO0yIYBu1FrLvB9.htm](kingmaker-bestiary-items/fqO0yIYBu1FrLvB9.htm)|Scythe|auto-trad|
|[FQyPQNxKCzuNCQHu.htm](kingmaker-bestiary-items/FQyPQNxKCzuNCQHu.htm)|Wolf Empathy|auto-trad|
|[fRaI1Z66oWzSAz2Q.htm](kingmaker-bestiary-items/fRaI1Z66oWzSAz2Q.htm)|Hit 'Em Hard|auto-trad|
|[FrDnLG8ehjXFC6Ey.htm](kingmaker-bestiary-items/FrDnLG8ehjXFC6Ey.htm)|Attack of Opportunity|auto-trad|
|[frIks1GEvtVQAPPw.htm](kingmaker-bestiary-items/frIks1GEvtVQAPPw.htm)|Vine|auto-trad|
|[FrlYBDNV4RyfqLaY.htm](kingmaker-bestiary-items/FrlYBDNV4RyfqLaY.htm)|Jaws|auto-trad|
|[FrmCI6DzaoSpGyxI.htm](kingmaker-bestiary-items/FrmCI6DzaoSpGyxI.htm)|Twin Parry|auto-trad|
|[FRSknzZLlvXTktV6.htm](kingmaker-bestiary-items/FRSknzZLlvXTktV6.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[fryglhBup2tRHWFS.htm](kingmaker-bestiary-items/fryglhBup2tRHWFS.htm)|Telepathy 300 feet|auto-trad|
|[fS0rXdbVRA1IMM8E.htm](kingmaker-bestiary-items/fS0rXdbVRA1IMM8E.htm)|Attack of Opportunity|auto-trad|
|[FsgtuZo9KF7fxlp5.htm](kingmaker-bestiary-items/FsgtuZo9KF7fxlp5.htm)|Jaws|auto-trad|
|[FTMgXssm5HK4Io0O.htm](kingmaker-bestiary-items/FTMgXssm5HK4Io0O.htm)|Vortex|auto-trad|
|[ftnV0syBwJfqGk6Z.htm](kingmaker-bestiary-items/ftnV0syBwJfqGk6Z.htm)|Thorn|auto-trad|
|[fTVvztwtM7PZgob6.htm](kingmaker-bestiary-items/fTVvztwtM7PZgob6.htm)|Grab|auto-trad|
|[FuDdgAP7wwq6LCFO.htm](kingmaker-bestiary-items/FuDdgAP7wwq6LCFO.htm)|Phomandala's Venom|auto-trad|
|[FuvkjEpm8ZZnjZ46.htm](kingmaker-bestiary-items/FuvkjEpm8ZZnjZ46.htm)|Sneak Attack|auto-trad|
|[Fv9HoAW6MyuvLWBs.htm](kingmaker-bestiary-items/Fv9HoAW6MyuvLWBs.htm)|At-Will Spells|auto-trad|
|[Fw6GAT5RmaN6VMII.htm](kingmaker-bestiary-items/Fw6GAT5RmaN6VMII.htm)|Giant's Lunge|auto-trad|
|[FwhFzYJLEkQ3J68c.htm](kingmaker-bestiary-items/FwhFzYJLEkQ3J68c.htm)|Low-Light Vision|auto-trad|
|[FwN998JxnTCCkJw3.htm](kingmaker-bestiary-items/FwN998JxnTCCkJw3.htm)|Reveal the Enemy|auto-trad|
|[fwojd1PspBIG8CEY.htm](kingmaker-bestiary-items/fwojd1PspBIG8CEY.htm)|Shortsword|auto-trad|
|[fxgXQNNgTLwJ2JoU.htm](kingmaker-bestiary-items/fxgXQNNgTLwJ2JoU.htm)|Arcane Prepared Spells|auto-trad|
|[fXlB9FRj3C3aY3Vk.htm](kingmaker-bestiary-items/fXlB9FRj3C3aY3Vk.htm)|Form Control|auto-trad|
|[fxuqMIJcFuZQworq.htm](kingmaker-bestiary-items/fxuqMIJcFuZQworq.htm)|Fangs|auto-trad|
|[fxZztiCJybeYYeOI.htm](kingmaker-bestiary-items/fxZztiCJybeYYeOI.htm)|Captivating Dance|auto-trad|
|[fYhoTq6AnWKAwKBM.htm](kingmaker-bestiary-items/fYhoTq6AnWKAwKBM.htm)|Release the Inmost Worm|auto-trad|
|[FZAkeuRb3tyRnlmH.htm](kingmaker-bestiary-items/FZAkeuRb3tyRnlmH.htm)|Hurried Retreat|auto-trad|
|[FzjT88a1Bn4XsEYM.htm](kingmaker-bestiary-items/FzjT88a1Bn4XsEYM.htm)|Battle Axe|auto-trad|
|[G0x7fr9ZPMuqw2RG.htm](kingmaker-bestiary-items/G0x7fr9ZPMuqw2RG.htm)|Weeping Aura|auto-trad|
|[g1KH4siW7BJpYTfM.htm](kingmaker-bestiary-items/g1KH4siW7BJpYTfM.htm)|Claw|auto-trad|
|[g2qQpMZqRaylNASw.htm](kingmaker-bestiary-items/g2qQpMZqRaylNASw.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[g4bJ3hTpL9J2uQKp.htm](kingmaker-bestiary-items/g4bJ3hTpL9J2uQKp.htm)|Attack of Opportunity|auto-trad|
|[GA9tSWJmy7KFgUjB.htm](kingmaker-bestiary-items/GA9tSWJmy7KFgUjB.htm)|Primal Innate Spells|auto-trad|
|[GAwPcYxItK5M24Ve.htm](kingmaker-bestiary-items/GAwPcYxItK5M24Ve.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[gbg7dpE5qccG0uh1.htm](kingmaker-bestiary-items/gbg7dpE5qccG0uh1.htm)|Greater Shock Maul|auto-trad|
|[GcSM5ZkG8OONvY0G.htm](kingmaker-bestiary-items/GcSM5ZkG8OONvY0G.htm)|Fey Conduit|auto-trad|
|[gDENaXLY2aYA7jYG.htm](kingmaker-bestiary-items/gDENaXLY2aYA7jYG.htm)|Greataxe|auto-trad|
|[GeAJNEdtRdXSvZvk.htm](kingmaker-bestiary-items/GeAJNEdtRdXSvZvk.htm)|Breath Weapon|auto-trad|
|[gfH6Km7ijznOKHzF.htm](kingmaker-bestiary-items/gfH6Km7ijznOKHzF.htm)|Negative Healing|auto-trad|
|[ghqAZgEVPVGBLtZW.htm](kingmaker-bestiary-items/ghqAZgEVPVGBLtZW.htm)|Tongue Grab|auto-trad|
|[gixaFzYqsyV9bsLi.htm](kingmaker-bestiary-items/gixaFzYqsyV9bsLi.htm)|Cairn Wight Spawn|auto-trad|
|[gj9hDQvaXekxyv1Q.htm](kingmaker-bestiary-items/gj9hDQvaXekxyv1Q.htm)|Longsword|auto-trad|
|[GJxtjkowvzk3cvaE.htm](kingmaker-bestiary-items/GJxtjkowvzk3cvaE.htm)|Constant Spells|auto-trad|
|[GMKTkSGoth8sggbH.htm](kingmaker-bestiary-items/GMKTkSGoth8sggbH.htm)|Call Glaive|auto-trad|
|[gNhak3Nf3ve1ouhK.htm](kingmaker-bestiary-items/gNhak3Nf3ve1ouhK.htm)|Claw|auto-trad|
|[gpZkw015Ns4Dbplk.htm](kingmaker-bestiary-items/gpZkw015Ns4Dbplk.htm)|Drowning Touch|auto-trad|
|[gRn39hK7VbDU7fip.htm](kingmaker-bestiary-items/gRn39hK7VbDU7fip.htm)|Low-Light Vision|auto-trad|
|[gRnmV8LgZS6qZ09j.htm](kingmaker-bestiary-items/gRnmV8LgZS6qZ09j.htm)|Low-Light Vision|auto-trad|
|[gsoYzQxRygC654Xf.htm](kingmaker-bestiary-items/gsoYzQxRygC654Xf.htm)|Pounce|auto-trad|
|[GxlQBNruDU25fJkQ.htm](kingmaker-bestiary-items/GxlQBNruDU25fJkQ.htm)|Open Doors|auto-trad|
|[GxSpJFY9UJiwry54.htm](kingmaker-bestiary-items/GxSpJFY9UJiwry54.htm)|Jaws|auto-trad|
|[GZ9qL58QMmBtlHul.htm](kingmaker-bestiary-items/GZ9qL58QMmBtlHul.htm)|Electric Healing|auto-trad|
|[gZfWtX8zkOoe49o7.htm](kingmaker-bestiary-items/gZfWtX8zkOoe49o7.htm)|Regeneration 25 (deactivated by Vorpal Weapons)|auto-trad|
|[gzziZdYQCjJ1pCM1.htm](kingmaker-bestiary-items/gzziZdYQCjJ1pCM1.htm)|Reminder of Doom|auto-trad|
|[h0MxDLIAI1CLU61G.htm](kingmaker-bestiary-items/h0MxDLIAI1CLU61G.htm)|Thorny Lash|auto-trad|
|[h2wSXBnK6CA96ypS.htm](kingmaker-bestiary-items/h2wSXBnK6CA96ypS.htm)|Reach Spell|auto-trad|
|[h4qPfDnAjrfUEFX6.htm](kingmaker-bestiary-items/h4qPfDnAjrfUEFX6.htm)|Sylvan Wine|auto-trad|
|[h4yqrCg7sP5DCU4F.htm](kingmaker-bestiary-items/h4yqrCg7sP5DCU4F.htm)|Darkvision|auto-trad|
|[H5KqV1tEsBEfhfvU.htm](kingmaker-bestiary-items/H5KqV1tEsBEfhfvU.htm)|Hunt Prey|auto-trad|
|[h8HPqnXI3dI7KFxV.htm](kingmaker-bestiary-items/h8HPqnXI3dI7KFxV.htm)|Hoof|auto-trad|
|[H8UomLORlnQErtR4.htm](kingmaker-bestiary-items/H8UomLORlnQErtR4.htm)|Gnawing Bite|auto-trad|
|[H9cKHc9OAHiXKDzf.htm](kingmaker-bestiary-items/H9cKHc9OAHiXKDzf.htm)|Swarming Bites|auto-trad|
|[h9Ugyirm1B1g6pt6.htm](kingmaker-bestiary-items/h9Ugyirm1B1g6pt6.htm)|Jaws|auto-trad|
|[hbBhekpAkC55ljY1.htm](kingmaker-bestiary-items/hbBhekpAkC55ljY1.htm)|Fleet Performer|auto-trad|
|[hbOLSP9RZmA3hyU8.htm](kingmaker-bestiary-items/hbOLSP9RZmA3hyU8.htm)|+1 Status to All Saves vs. Poison|auto-trad|
|[Hczu635vDDik3NIJ.htm](kingmaker-bestiary-items/Hczu635vDDik3NIJ.htm)|Grab|auto-trad|
|[hE1MVc0Pxfi3nERv.htm](kingmaker-bestiary-items/hE1MVc0Pxfi3nERv.htm)|Greatsword|auto-trad|
|[hFDR91zB1cjh1UHv.htm](kingmaker-bestiary-items/hFDR91zB1cjh1UHv.htm)|Dead Tree|auto-trad|
|[HfIRzOBnqZx60iSO.htm](kingmaker-bestiary-items/HfIRzOBnqZx60iSO.htm)|Club|auto-trad|
|[hhOHcOSPkhLdvric.htm](kingmaker-bestiary-items/hhOHcOSPkhLdvric.htm)|Confusing Gaze|auto-trad|
|[hID13CF39qYYQ2rX.htm](kingmaker-bestiary-items/hID13CF39qYYQ2rX.htm)|Collapse|auto-trad|
|[HiL2TCMI2Rzmy2SW.htm](kingmaker-bestiary-items/HiL2TCMI2Rzmy2SW.htm)|Juggernaut|auto-trad|
|[HiZTEnboQEoek6kL.htm](kingmaker-bestiary-items/HiZTEnboQEoek6kL.htm)|Three-Headed Strike|auto-trad|
|[hJ1mAKCkNvCy1GY0.htm](kingmaker-bestiary-items/hJ1mAKCkNvCy1GY0.htm)|Fangs|auto-trad|
|[HjqV5crA9cRbpUVj.htm](kingmaker-bestiary-items/HjqV5crA9cRbpUVj.htm)|Electric Surge|auto-trad|
|[HjxDZjwRHwjQx54V.htm](kingmaker-bestiary-items/HjxDZjwRHwjQx54V.htm)|Spear|auto-trad|
|[HKEKbaIpMla2LGS8.htm](kingmaker-bestiary-items/HKEKbaIpMla2LGS8.htm)|Sneak Attack|auto-trad|
|[hkgYTbnzCzYBfP7k.htm](kingmaker-bestiary-items/hkgYTbnzCzYBfP7k.htm)|Voice Imitation|auto-trad|
|[HLcMADTgT1nixxce.htm](kingmaker-bestiary-items/HLcMADTgT1nixxce.htm)|Breath of the Bog|auto-trad|
|[HNfXE0yusiYA2juN.htm](kingmaker-bestiary-items/HNfXE0yusiYA2juN.htm)|Darkvision|auto-trad|
|[hO1dPgyMTWEGJRxH.htm](kingmaker-bestiary-items/hO1dPgyMTWEGJRxH.htm)|Rejuvenation|auto-trad|
|[hook1XGZNlxXtjvJ.htm](kingmaker-bestiary-items/hook1XGZNlxXtjvJ.htm)|Fangs|auto-trad|
|[HQAGq6saSYHzJC0I.htm](kingmaker-bestiary-items/HQAGq6saSYHzJC0I.htm)|Three Headed|auto-trad|
|[hqm0z6fZAXCCTDOs.htm](kingmaker-bestiary-items/hqm0z6fZAXCCTDOs.htm)|Wild Stride|auto-trad|
|[hr8UQcvJzWfTNGoH.htm](kingmaker-bestiary-items/hr8UQcvJzWfTNGoH.htm)|Planar Fast Healing 15|auto-trad|
|[HrP24kDK1jVss45v.htm](kingmaker-bestiary-items/HrP24kDK1jVss45v.htm)|Fast Healing 10|auto-trad|
|[hSGaU1UT8mzmPFWl.htm](kingmaker-bestiary-items/hSGaU1UT8mzmPFWl.htm)|Petrifying Gaze|auto-trad|
|[hsn4E9lEPjjDCMAN.htm](kingmaker-bestiary-items/hsn4E9lEPjjDCMAN.htm)|Occult Focus Spells|auto-trad|
|[htpcG9CC3rJNay4j.htm](kingmaker-bestiary-items/htpcG9CC3rJNay4j.htm)|Fist|auto-trad|
|[HtScT1S9mxNqMylg.htm](kingmaker-bestiary-items/HtScT1S9mxNqMylg.htm)|Fast Healing 15|auto-trad|
|[HWNQMuf5GV8rRSMu.htm](kingmaker-bestiary-items/HWNQMuf5GV8rRSMu.htm)|Frightful Presence|auto-trad|
|[hwXxwT85s7KyKoWq.htm](kingmaker-bestiary-items/hwXxwT85s7KyKoWq.htm)|Lifesense 30 feet|auto-trad|
|[hxeZesdJLgHPkEJ5.htm](kingmaker-bestiary-items/hxeZesdJLgHPkEJ5.htm)|Spectral Hand|auto-trad|
|[HYAfgOkdktmrrKGf.htm](kingmaker-bestiary-items/HYAfgOkdktmrrKGf.htm)|Stygian Fire|auto-trad|
|[HYnUw0h1bJQMkzBc.htm](kingmaker-bestiary-items/HYnUw0h1bJQMkzBc.htm)|Planar Acclimation|auto-trad|
|[hZbArhODPV9EmZ0m.htm](kingmaker-bestiary-items/hZbArhODPV9EmZ0m.htm)|Sound Imitation|auto-trad|
|[hznpxgtC8SziycgN.htm](kingmaker-bestiary-items/hznpxgtC8SziycgN.htm)|Hatchet Onslaught|auto-trad|
|[HZvbzYmbccVoGV1B.htm](kingmaker-bestiary-items/HZvbzYmbccVoGV1B.htm)|Maul Mastery|auto-trad|
|[HZwLWbZdPHnymsUr.htm](kingmaker-bestiary-items/HZwLWbZdPHnymsUr.htm)|Darkvision|auto-trad|
|[i42oMxLUkfr8rkMc.htm](kingmaker-bestiary-items/i42oMxLUkfr8rkMc.htm)|Open Doors|auto-trad|
|[i5DVxzz2vN9zcxsC.htm](kingmaker-bestiary-items/i5DVxzz2vN9zcxsC.htm)|Attack of Opportunity|auto-trad|
|[i8ykQ7pFedbfWxeV.htm](kingmaker-bestiary-items/i8ykQ7pFedbfWxeV.htm)|Staff|auto-trad|
|[i98RWuQkObalnsWO.htm](kingmaker-bestiary-items/i98RWuQkObalnsWO.htm)|Shortbow|auto-trad|
|[IDIM2wkSDDgMI06V.htm](kingmaker-bestiary-items/IDIM2wkSDDgMI06V.htm)|Grasping Roots|auto-trad|
|[iE3cMVqsLzS8aqvH.htm](kingmaker-bestiary-items/iE3cMVqsLzS8aqvH.htm)|Rejuvenation|auto-trad|
|[ifhXNlwgfGtUNABs.htm](kingmaker-bestiary-items/ifhXNlwgfGtUNABs.htm)|Reactive Charge|auto-trad|
|[IFp4VK6X80nXiTJu.htm](kingmaker-bestiary-items/IFp4VK6X80nXiTJu.htm)|Darkvision|auto-trad|
|[ihGm6kH3jaNxYb3p.htm](kingmaker-bestiary-items/ihGm6kH3jaNxYb3p.htm)|Tremorsense (Precise) 30 feet|auto-trad|
|[ihI9TF3YdNJFYytf.htm](kingmaker-bestiary-items/ihI9TF3YdNJFYytf.htm)|Trample|auto-trad|
|[ik0FmQbY7s9wzZh2.htm](kingmaker-bestiary-items/ik0FmQbY7s9wzZh2.htm)|Wild Shape|auto-trad|
|[IkPoWCmWZcyW5osI.htm](kingmaker-bestiary-items/IkPoWCmWZcyW5osI.htm)|Staff|auto-trad|
|[IKXHj9lMhQ6Qy07y.htm](kingmaker-bestiary-items/IKXHj9lMhQ6Qy07y.htm)|Rend|auto-trad|
|[ILS10PAagocQkyqT.htm](kingmaker-bestiary-items/ILS10PAagocQkyqT.htm)|Hunt Prey|auto-trad|
|[ImYiua3KKxG5XU61.htm](kingmaker-bestiary-items/ImYiua3KKxG5XU61.htm)|Play the Pipes|auto-trad|
|[ImzpAPq1SgMcf7t3.htm](kingmaker-bestiary-items/ImzpAPq1SgMcf7t3.htm)|Hunt Prey|auto-trad|
|[In6OZtLo0Avr0mV1.htm](kingmaker-bestiary-items/In6OZtLo0Avr0mV1.htm)|Trident|auto-trad|
|[inAbFfjyYxWyFk2e.htm](kingmaker-bestiary-items/inAbFfjyYxWyFk2e.htm)|Prostrate|auto-trad|
|[Io2JD6hMiVboalmM.htm](kingmaker-bestiary-items/Io2JD6hMiVboalmM.htm)|Cunning|auto-trad|
|[iOA6wNMnIzBStwhB.htm](kingmaker-bestiary-items/iOA6wNMnIzBStwhB.htm)|Return to Pavetta|auto-trad|
|[IoKSTbFElqXPiGiX.htm](kingmaker-bestiary-items/IoKSTbFElqXPiGiX.htm)|Primal Innate Spells|auto-trad|
|[ipARSA7VFfyApjl1.htm](kingmaker-bestiary-items/ipARSA7VFfyApjl1.htm)|Throw Rock|auto-trad|
|[iqKVeVAvsVEgW29e.htm](kingmaker-bestiary-items/iqKVeVAvsVEgW29e.htm)|Rejuvenation|auto-trad|
|[iT766eWfAydCiJYK.htm](kingmaker-bestiary-items/iT766eWfAydCiJYK.htm)|Mandibles|auto-trad|
|[iTAZj4rMOPRWWJDP.htm](kingmaker-bestiary-items/iTAZj4rMOPRWWJDP.htm)|Corpse Throwing|auto-trad|
|[ITgYWyiUVLD2SpLl.htm](kingmaker-bestiary-items/ITgYWyiUVLD2SpLl.htm)|Composite Shortbow|auto-trad|
|[iu1YHlbvh64HBaor.htm](kingmaker-bestiary-items/iu1YHlbvh64HBaor.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[IUA7ZWVTepsameDv.htm](kingmaker-bestiary-items/IUA7ZWVTepsameDv.htm)|Fangs|auto-trad|
|[IuK4DrLEyP6BmKi2.htm](kingmaker-bestiary-items/IuK4DrLEyP6BmKi2.htm)|Draconic Frenzy|auto-trad|
|[iVaCyzg2MMKJ1SIu.htm](kingmaker-bestiary-items/iVaCyzg2MMKJ1SIu.htm)|Darkvision|auto-trad|
|[IvFL4oT7wjrxQZqw.htm](kingmaker-bestiary-items/IvFL4oT7wjrxQZqw.htm)|Claw|auto-trad|
|[IW0bpDD32dNAHnum.htm](kingmaker-bestiary-items/IW0bpDD32dNAHnum.htm)|Frumious Charge|auto-trad|
|[IYbU9IiWVPm4CGyc.htm](kingmaker-bestiary-items/IYbU9IiWVPm4CGyc.htm)|Fist|auto-trad|
|[Iyw7GZBX5oMU1kR0.htm](kingmaker-bestiary-items/Iyw7GZBX5oMU1kR0.htm)|Darkvision|auto-trad|
|[IzbfeuZKm6hz1cWS.htm](kingmaker-bestiary-items/IzbfeuZKm6hz1cWS.htm)|Darkvision|auto-trad|
|[IzVf6cehi9NajoXy.htm](kingmaker-bestiary-items/IzVf6cehi9NajoXy.htm)|Trident Twist|auto-trad|
|[j1GC3KLLJa3BPe0o.htm](kingmaker-bestiary-items/j1GC3KLLJa3BPe0o.htm)|Battle Axe|auto-trad|
|[J2LUtQlsnbpMEQ1r.htm](kingmaker-bestiary-items/J2LUtQlsnbpMEQ1r.htm)|Tongue|auto-trad|
|[j2XC7qI2EMqGvGSs.htm](kingmaker-bestiary-items/j2XC7qI2EMqGvGSs.htm)|Low-Light Vision|auto-trad|
|[J3lzyo3f2n6RNBG4.htm](kingmaker-bestiary-items/J3lzyo3f2n6RNBG4.htm)|Rushing Water|auto-trad|
|[j4kcdayowXPW4Avm.htm](kingmaker-bestiary-items/j4kcdayowXPW4Avm.htm)|Horns|auto-trad|
|[J4SHx8p5wCH9xlMo.htm](kingmaker-bestiary-items/J4SHx8p5wCH9xlMo.htm)|Clench Jaws|auto-trad|
|[J73AKz9wgVvUQWFo.htm](kingmaker-bestiary-items/J73AKz9wgVvUQWFo.htm)|Low-Light Vision|auto-trad|
|[j8nRwfwxCyxXRet5.htm](kingmaker-bestiary-items/j8nRwfwxCyxXRet5.htm)|Dagger|auto-trad|
|[JAh7SWOTsIXvrckz.htm](kingmaker-bestiary-items/JAh7SWOTsIXvrckz.htm)|Water Travel|auto-trad|
|[jAX00kRCiI1ahLJA.htm](kingmaker-bestiary-items/jAX00kRCiI1ahLJA.htm)|Bow Specialist|auto-trad|
|[jBAYmOeepk3w4EgS.htm](kingmaker-bestiary-items/jBAYmOeepk3w4EgS.htm)|Rend|auto-trad|
|[jCQO2RfqPLKqDeuF.htm](kingmaker-bestiary-items/jCQO2RfqPLKqDeuF.htm)|Slow Susceptibility|auto-trad|
|[JdsDXPVL1v4JEQt5.htm](kingmaker-bestiary-items/JdsDXPVL1v4JEQt5.htm)|Wing|auto-trad|
|[jEkasU1qDlN76A9i.htm](kingmaker-bestiary-items/jEkasU1qDlN76A9i.htm)|Occult Innate Spells|auto-trad|
|[JfR3IkhT8g6aU9ry.htm](kingmaker-bestiary-items/JfR3IkhT8g6aU9ry.htm)|Scythe Branch|auto-trad|
|[jga4jzrMzJRkNouq.htm](kingmaker-bestiary-items/jga4jzrMzJRkNouq.htm)|Formation Fighters|auto-trad|
|[JjphR7198vaXiAJo.htm](kingmaker-bestiary-items/JjphR7198vaXiAJo.htm)|Defensive Roll|auto-trad|
|[JJTobEFnvJPV524S.htm](kingmaker-bestiary-items/JJTobEFnvJPV524S.htm)|Vorpal Fear|auto-trad|
|[jlJLCgnS5JMrSDgN.htm](kingmaker-bestiary-items/jlJLCgnS5JMrSDgN.htm)|Tear Free|auto-trad|
|[jmgh011n8MXGN953.htm](kingmaker-bestiary-items/jmgh011n8MXGN953.htm)|Dagger|auto-trad|
|[jmL1vjBAG3jha7pe.htm](kingmaker-bestiary-items/jmL1vjBAG3jha7pe.htm)|Stabbing Fit|auto-trad|
|[Jn2HBbRFuiRxKRbB.htm](kingmaker-bestiary-items/Jn2HBbRFuiRxKRbB.htm)|Goblin Scuttle|auto-trad|
|[JoOzphwUQC4ZFm7J.htm](kingmaker-bestiary-items/JoOzphwUQC4ZFm7J.htm)|Saving Slash|auto-trad|
|[jp5lLzCnJEJ3qCUl.htm](kingmaker-bestiary-items/jp5lLzCnJEJ3qCUl.htm)|Arcane Spells Prepared|auto-trad|
|[JP9GiA6aaQlXRKAE.htm](kingmaker-bestiary-items/JP9GiA6aaQlXRKAE.htm)|Primal Prepared Spells|auto-trad|
|[JPmOqF1x4Au8drWv.htm](kingmaker-bestiary-items/JPmOqF1x4Au8drWv.htm)|Spectral Uprising|auto-trad|
|[jppcxZjbxQ0e8W30.htm](kingmaker-bestiary-items/jppcxZjbxQ0e8W30.htm)|At-Will Spells|auto-trad|
|[Jspfa7SNI6gNhPK4.htm](kingmaker-bestiary-items/Jspfa7SNI6gNhPK4.htm)|Coven|auto-trad|
|[jTkhIBakBrDpEBuu.htm](kingmaker-bestiary-items/jTkhIBakBrDpEBuu.htm)|At-Will Spells|auto-trad|
|[jU73I507kdECl13G.htm](kingmaker-bestiary-items/jU73I507kdECl13G.htm)|Contingency|auto-trad|
|[jUFQA2dWOvLpv6zg.htm](kingmaker-bestiary-items/jUFQA2dWOvLpv6zg.htm)|Fist|auto-trad|
|[jvDyUbBwbo3R1H4x.htm](kingmaker-bestiary-items/jvDyUbBwbo3R1H4x.htm)|Divine Prepared Spells|auto-trad|
|[jVnAGrnHMY0ZZTAt.htm](kingmaker-bestiary-items/jVnAGrnHMY0ZZTAt.htm)|Negative Healing|auto-trad|
|[JWMAFJOdy4Odx7RY.htm](kingmaker-bestiary-items/JWMAFJOdy4Odx7RY.htm)|Playful Switch|auto-trad|
|[JXtD0GzHXfGW7Aan.htm](kingmaker-bestiary-items/JXtD0GzHXfGW7Aan.htm)|Deny Advantage|auto-trad|
|[jXwGpQmNRAqEcUQN.htm](kingmaker-bestiary-items/jXwGpQmNRAqEcUQN.htm)|Quickened Casting|auto-trad|
|[jyTZDCNVazHG1tSk.htm](kingmaker-bestiary-items/jyTZDCNVazHG1tSk.htm)|Burst|auto-trad|
|[jZ8U8eTiEuKU6toZ.htm](kingmaker-bestiary-items/jZ8U8eTiEuKU6toZ.htm)|Heat|auto-trad|
|[JzdZZiLkLG0WmOid.htm](kingmaker-bestiary-items/JzdZZiLkLG0WmOid.htm)|Low-Light Vision|auto-trad|
|[jZfqY5quDvHq9tmh.htm](kingmaker-bestiary-items/jZfqY5quDvHq9tmh.htm)|Wild Empathy|auto-trad|
|[jzVssayBq4mzMU9u.htm](kingmaker-bestiary-items/jzVssayBq4mzMU9u.htm)|Darkvision|auto-trad|
|[k1LT9AcDMUPeGPOy.htm](kingmaker-bestiary-items/k1LT9AcDMUPeGPOy.htm)|Righteous Certainty|auto-trad|
|[k2HAX2N3qaYcbpb8.htm](kingmaker-bestiary-items/k2HAX2N3qaYcbpb8.htm)|Greater Darkvision|auto-trad|
|[k2R72XvX3USofjXB.htm](kingmaker-bestiary-items/k2R72XvX3USofjXB.htm)|Lash Out|auto-trad|
|[K3AfPmsD7hzdbhv0.htm](kingmaker-bestiary-items/K3AfPmsD7hzdbhv0.htm)|Sneak Attack|auto-trad|
|[K470oCZYTiRoTfqf.htm](kingmaker-bestiary-items/K470oCZYTiRoTfqf.htm)|Jaws|auto-trad|
|[K5qMhCPST2MiSV7J.htm](kingmaker-bestiary-items/K5qMhCPST2MiSV7J.htm)|Inveigled|auto-trad|
|[k6dnEhWODfyRtg4b.htm](kingmaker-bestiary-items/k6dnEhWODfyRtg4b.htm)|Composite Shortbow|auto-trad|
|[K7J1kJsdpeoiTUqZ.htm](kingmaker-bestiary-items/K7J1kJsdpeoiTUqZ.htm)|Attack of Opportunity|auto-trad|
|[k9kbOoeLWV3Bx8l5.htm](kingmaker-bestiary-items/k9kbOoeLWV3Bx8l5.htm)|Moment of Solitude|auto-trad|
|[Ka4LviiNUhy4FNzo.htm](kingmaker-bestiary-items/Ka4LviiNUhy4FNzo.htm)|Talon|auto-trad|
|[kaPLdey2Dv3F1VUN.htm](kingmaker-bestiary-items/kaPLdey2Dv3F1VUN.htm)|Oathbow|auto-trad|
|[kBr3AKRl4vlCwVK2.htm](kingmaker-bestiary-items/kBr3AKRl4vlCwVK2.htm)|Spiked Greatclub|auto-trad|
|[KbYGZddUNQGhrhEf.htm](kingmaker-bestiary-items/KbYGZddUNQGhrhEf.htm)|Composite Longbow|auto-trad|
|[KCOXqXVayJMKPZVz.htm](kingmaker-bestiary-items/KCOXqXVayJMKPZVz.htm)|Composite Longbow|auto-trad|
|[kdl1FTleQZfZf6eP.htm](kingmaker-bestiary-items/kdl1FTleQZfZf6eP.htm)|Primal Spontaneous Spells|auto-trad|
|[KEm1y8hEl4sjQ6Cv.htm](kingmaker-bestiary-items/KEm1y8hEl4sjQ6Cv.htm)|Talon|auto-trad|
|[KF8TJluc2GTgVJhl.htm](kingmaker-bestiary-items/KF8TJluc2GTgVJhl.htm)|Paddle|auto-trad|
|[kfdzBPs02IvVMabf.htm](kingmaker-bestiary-items/kfdzBPs02IvVMabf.htm)|Focus Gaze|auto-trad|
|[kh9EPJwZWvajnZBV.htm](kingmaker-bestiary-items/kh9EPJwZWvajnZBV.htm)|At-Will Spells|auto-trad|
|[KhgTIBtP3uB7Ls0w.htm](kingmaker-bestiary-items/KhgTIBtP3uB7Ls0w.htm)|Fireball Breath|auto-trad|
|[khvFVWc6itmsBv7T.htm](kingmaker-bestiary-items/khvFVWc6itmsBv7T.htm)|Darkvision|auto-trad|
|[khwh43CDMROkIfgf.htm](kingmaker-bestiary-items/khwh43CDMROkIfgf.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[KIhN4LqmxLYnSLJj.htm](kingmaker-bestiary-items/KIhN4LqmxLYnSLJj.htm)|Change Shape|auto-trad|
|[kJ9e1DXfbX04Qfoe.htm](kingmaker-bestiary-items/kJ9e1DXfbX04Qfoe.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[klmOYsNgGYaAMROh.htm](kingmaker-bestiary-items/klmOYsNgGYaAMROh.htm)|Knockback|auto-trad|
|[kMVtB4pZd0LYoYOm.htm](kingmaker-bestiary-items/kMVtB4pZd0LYoYOm.htm)|Slam Doors|auto-trad|
|[KngnsUrW0RbmKrce.htm](kingmaker-bestiary-items/KngnsUrW0RbmKrce.htm)|Focus Beauty|auto-trad|
|[KNywleLC7uqltyig.htm](kingmaker-bestiary-items/KNywleLC7uqltyig.htm)|Focus Gaze|auto-trad|
|[Kok7cEEC0PeallmB.htm](kingmaker-bestiary-items/Kok7cEEC0PeallmB.htm)|Horns|auto-trad|
|[kooXn2rvr04D9fM6.htm](kingmaker-bestiary-items/kooXn2rvr04D9fM6.htm)|Attack of Opportunity|auto-trad|
|[koqEAUHXo9t7wqVS.htm](kingmaker-bestiary-items/koqEAUHXo9t7wqVS.htm)|Mace|auto-trad|
|[kPaYMeS20gIcKi3E.htm](kingmaker-bestiary-items/kPaYMeS20gIcKi3E.htm)|Primal Innate Spells|auto-trad|
|[KpFmMp8bJz2ZkMQN.htm](kingmaker-bestiary-items/KpFmMp8bJz2ZkMQN.htm)|Darkvision|auto-trad|
|[kq0boAEZsA01NsEH.htm](kingmaker-bestiary-items/kq0boAEZsA01NsEH.htm)|Powerful Dive|auto-trad|
|[krTcoq8OKd5gFAL0.htm](kingmaker-bestiary-items/krTcoq8OKd5gFAL0.htm)|Sneak Attack|auto-trad|
|[KtA9MV7RA5nWRmko.htm](kingmaker-bestiary-items/KtA9MV7RA5nWRmko.htm)|Blinding Sickness|auto-trad|
|[kTN86guVl5qzlzIK.htm](kingmaker-bestiary-items/kTN86guVl5qzlzIK.htm)|Primal Innate Spells|auto-trad|
|[KTxjm0shb9zyrh0z.htm](kingmaker-bestiary-items/KTxjm0shb9zyrh0z.htm)|Drain Life|auto-trad|
|[kU6LiydL3jldv5KQ.htm](kingmaker-bestiary-items/kU6LiydL3jldv5KQ.htm)|Primal Innate Spells|auto-trad|
|[KUcCUd82J1JqXWE5.htm](kingmaker-bestiary-items/KUcCUd82J1JqXWE5.htm)|Breath Weapon|auto-trad|
|[KuURUnbzeAf0EaR1.htm](kingmaker-bestiary-items/KuURUnbzeAf0EaR1.htm)|Catch Rock|auto-trad|
|[kuV1kCq4ZEiH9DzH.htm](kingmaker-bestiary-items/kuV1kCq4ZEiH9DzH.htm)|Vulnerable to Curved Space|auto-trad|
|[kv7A4qqpl2mG1nHt.htm](kingmaker-bestiary-items/kv7A4qqpl2mG1nHt.htm)|Quickened Casting|auto-trad|
|[kX089KrOjeO3aDf3.htm](kingmaker-bestiary-items/kX089KrOjeO3aDf3.htm)|Composite Longbow|auto-trad|
|[KXuuj04cishnGNpk.htm](kingmaker-bestiary-items/KXuuj04cishnGNpk.htm)|Trapdoor Lunge|auto-trad|
|[L1NeMv3ZDcttKLw2.htm](kingmaker-bestiary-items/L1NeMv3ZDcttKLw2.htm)|Snatch|auto-trad|
|[L4dKqPtdmyspmZtd.htm](kingmaker-bestiary-items/L4dKqPtdmyspmZtd.htm)|Jaws|auto-trad|
|[l4eGICy0QK1EEERs.htm](kingmaker-bestiary-items/l4eGICy0QK1EEERs.htm)|Jaws|auto-trad|
|[l6mRpAGFCZ7gTIb1.htm](kingmaker-bestiary-items/l6mRpAGFCZ7gTIb1.htm)|Rasping Tongue|auto-trad|
|[LA4f99Yw7Dk1e16d.htm](kingmaker-bestiary-items/LA4f99Yw7Dk1e16d.htm)|Fast Healing 10 (in dust or sand)|auto-trad|
|[laXpsxFAPFs8EXkA.htm](kingmaker-bestiary-items/laXpsxFAPFs8EXkA.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[LbdwyQgOrSBqDgv9.htm](kingmaker-bestiary-items/LbdwyQgOrSBqDgv9.htm)|Dagger|auto-trad|
|[LC8WDqxmulYb1L4q.htm](kingmaker-bestiary-items/LC8WDqxmulYb1L4q.htm)|Dagger|auto-trad|
|[LcG0o3PUyemEv5oK.htm](kingmaker-bestiary-items/LcG0o3PUyemEv5oK.htm)|Darkvision|auto-trad|
|[lCn44iSRdDgu2GdC.htm](kingmaker-bestiary-items/lCn44iSRdDgu2GdC.htm)|Attack of Opportunity|auto-trad|
|[LCVw81WewWAm0wOB.htm](kingmaker-bestiary-items/LCVw81WewWAm0wOB.htm)|Primal Innate Spells|auto-trad|
|[LDLwyqZVioLuytc9.htm](kingmaker-bestiary-items/LDLwyqZVioLuytc9.htm)|Low-Light Vision|auto-trad|
|[LEDnvueR2NzH8zkD.htm](kingmaker-bestiary-items/LEDnvueR2NzH8zkD.htm)|Dagger|auto-trad|
|[leYjDFOKDNCIZOBf.htm](kingmaker-bestiary-items/leYjDFOKDNCIZOBf.htm)|Sneak Attack|auto-trad|
|[lffL96H4Twel1YRH.htm](kingmaker-bestiary-items/lffL96H4Twel1YRH.htm)|Darkvision|auto-trad|
|[LFvM7oAwEJD1soNx.htm](kingmaker-bestiary-items/LFvM7oAwEJD1soNx.htm)|Low-Light Vision|auto-trad|
|[lGRlzXnOSNGW7djU.htm](kingmaker-bestiary-items/lGRlzXnOSNGW7djU.htm)|Shame|auto-trad|
|[LHmME2otANk4a79s.htm](kingmaker-bestiary-items/LHmME2otANk4a79s.htm)|Worm Jaws|auto-trad|
|[LHPXbhgsSYCjB4uG.htm](kingmaker-bestiary-items/LHPXbhgsSYCjB4uG.htm)|Club|auto-trad|
|[LIdmD7VqztundfTh.htm](kingmaker-bestiary-items/LIdmD7VqztundfTh.htm)|Talon|auto-trad|
|[lim8vhAnHsss3MkL.htm](kingmaker-bestiary-items/lim8vhAnHsss3MkL.htm)|Light Hammer|auto-trad|
|[LK0wAtZtoKTLOgnz.htm](kingmaker-bestiary-items/LK0wAtZtoKTLOgnz.htm)|Claw|auto-trad|
|[lLQgAigQauz0Z3sx.htm](kingmaker-bestiary-items/lLQgAigQauz0Z3sx.htm)|Designate Blood Foe|auto-trad|
|[lnlNJpjHNjw06Wzi.htm](kingmaker-bestiary-items/lnlNJpjHNjw06Wzi.htm)|Tusk|auto-trad|
|[lOVrcn3AJGm4KWma.htm](kingmaker-bestiary-items/lOVrcn3AJGm4KWma.htm)|Eyes Of Flame|auto-trad|
|[lP5ul9ZfSW3FEFpa.htm](kingmaker-bestiary-items/lP5ul9ZfSW3FEFpa.htm)|Swarm Mind|auto-trad|
|[Lq9tywTCezzL2UYy.htm](kingmaker-bestiary-items/Lq9tywTCezzL2UYy.htm)|Lamashtu's Bloom|auto-trad|
|[lqfoMvtMondVCDSV.htm](kingmaker-bestiary-items/lqfoMvtMondVCDSV.htm)|Steady Spellcasting|auto-trad|
|[Lri10NA5TEU7fZeX.htm](kingmaker-bestiary-items/Lri10NA5TEU7fZeX.htm)|Call to the Hunt|auto-trad|
|[LRT2YJWR1S18u4vp.htm](kingmaker-bestiary-items/LRT2YJWR1S18u4vp.htm)|Shortsword|auto-trad|
|[LsUtyTnBZPF6Hvnp.htm](kingmaker-bestiary-items/LsUtyTnBZPF6Hvnp.htm)|Frightful Presence|auto-trad|
|[LTaIE8s2797qElxg.htm](kingmaker-bestiary-items/LTaIE8s2797qElxg.htm)|Dagger|auto-trad|
|[LTLa6hS15bgHkOBi.htm](kingmaker-bestiary-items/LTLa6hS15bgHkOBi.htm)|Protection from Decapitation|auto-trad|
|[ltvMN7K0itu2l3WZ.htm](kingmaker-bestiary-items/ltvMN7K0itu2l3WZ.htm)|Snake Fangs|auto-trad|
|[luiDDR5D3oiCAEZh.htm](kingmaker-bestiary-items/luiDDR5D3oiCAEZh.htm)|Hatchet|auto-trad|
|[lUMpyYokJ5dTTdu9.htm](kingmaker-bestiary-items/lUMpyYokJ5dTTdu9.htm)|Blood Drain|auto-trad|
|[luyr4NvU4euUL7Dh.htm](kingmaker-bestiary-items/luyr4NvU4euUL7Dh.htm)|Darkvision|auto-trad|
|[LxhUmOCBKZi7FNfa.htm](kingmaker-bestiary-items/LxhUmOCBKZi7FNfa.htm)|Betraying Touch|auto-trad|
|[lxsWZN09WFvanWVx.htm](kingmaker-bestiary-items/lxsWZN09WFvanWVx.htm)|Primal Innate Spells|auto-trad|
|[LXvqvVhorWXVewtI.htm](kingmaker-bestiary-items/LXvqvVhorWXVewtI.htm)|Twist Time|auto-trad|
|[lYCCv7NRnl6ZnNjT.htm](kingmaker-bestiary-items/lYCCv7NRnl6ZnNjT.htm)|Constant Spells|auto-trad|
|[lYx7vXEHaPIDLbWp.htm](kingmaker-bestiary-items/lYx7vXEHaPIDLbWp.htm)|Sickle|auto-trad|
|[Lz7LqoEOMbBrTosy.htm](kingmaker-bestiary-items/Lz7LqoEOMbBrTosy.htm)|Repulsing Blow|auto-trad|
|[LZhDKd6gpJJ5o2RO.htm](kingmaker-bestiary-items/LZhDKd6gpJJ5o2RO.htm)|Aura of Disquietude|auto-trad|
|[m0MCsCQ2HU7X7Rr2.htm](kingmaker-bestiary-items/m0MCsCQ2HU7X7Rr2.htm)|Claw|auto-trad|
|[m2aEAnMnepSKN57G.htm](kingmaker-bestiary-items/m2aEAnMnepSKN57G.htm)|Head Regrowth|auto-trad|
|[M4f06NOUIcSyQNr2.htm](kingmaker-bestiary-items/M4f06NOUIcSyQNr2.htm)|Regeneration 45 (Deactivated by Acid or Fire)|auto-trad|
|[m4LUwAovVkqwRsiG.htm](kingmaker-bestiary-items/m4LUwAovVkqwRsiG.htm)|Vengeful Rage|auto-trad|
|[m5He7mR6hyUEterZ.htm](kingmaker-bestiary-items/m5He7mR6hyUEterZ.htm)|Regeneration 30 (Deactivated by Fire or Good)|auto-trad|
|[M7sZToYvFNrig8TH.htm](kingmaker-bestiary-items/M7sZToYvFNrig8TH.htm)|Constant Spells|auto-trad|
|[M9wyjXrmWfCD7nBk.htm](kingmaker-bestiary-items/M9wyjXrmWfCD7nBk.htm)|Blinding Beauty|auto-trad|
|[MBYJQAMX2qMwwp1o.htm](kingmaker-bestiary-items/MBYJQAMX2qMwwp1o.htm)|Divine Prepared Spells|auto-trad|
|[mc4qvML9Y9Vngpwo.htm](kingmaker-bestiary-items/mc4qvML9Y9Vngpwo.htm)|Thorny Vine|auto-trad|
|[mcMNYM92nivzmq61.htm](kingmaker-bestiary-items/mcMNYM92nivzmq61.htm)|+2 Status to All Saves vs. Mental|auto-trad|
|[mCooSIipdGY8hP2o.htm](kingmaker-bestiary-items/mCooSIipdGY8hP2o.htm)|Living Bow|auto-trad|
|[mDmFiG2gsRPiENrP.htm](kingmaker-bestiary-items/mDmFiG2gsRPiENrP.htm)|Warp Teleportation|auto-trad|
|[MfVHl3j1UyN4VFks.htm](kingmaker-bestiary-items/MfVHl3j1UyN4VFks.htm)|Attack of Opportunity|auto-trad|
|[MgV9p8g40O0uwDjs.htm](kingmaker-bestiary-items/MgV9p8g40O0uwDjs.htm)|Wild Gaze|auto-trad|
|[MgVuD5shNkwpUCdM.htm](kingmaker-bestiary-items/MgVuD5shNkwpUCdM.htm)|Frightful Moan|auto-trad|
|[MGX1U8Kv59BVYAOI.htm](kingmaker-bestiary-items/MGX1U8Kv59BVYAOI.htm)|Push or Pull 10 feet|auto-trad|
|[mGzWQjNPCtlz97Cn.htm](kingmaker-bestiary-items/mGzWQjNPCtlz97Cn.htm)|Sorcerer Bloodline Spells|auto-trad|
|[mJ4kL0oQk4xWhXtX.htm](kingmaker-bestiary-items/mJ4kL0oQk4xWhXtX.htm)|Darkvision|auto-trad|
|[Mj7eIolH2KmKRos4.htm](kingmaker-bestiary-items/Mj7eIolH2KmKRos4.htm)|Wyvern Venom|auto-trad|
|[MJktJ33e2ejK5I1o.htm](kingmaker-bestiary-items/MJktJ33e2ejK5I1o.htm)|Debilitating Bite|auto-trad|
|[mkO8uUk9bttHYIRn.htm](kingmaker-bestiary-items/mkO8uUk9bttHYIRn.htm)|Snake Fangs|auto-trad|
|[Mkzdftre3Tzz3aVM.htm](kingmaker-bestiary-items/Mkzdftre3Tzz3aVM.htm)|Drop From Ceiling|auto-trad|
|[MMamQCPSgaxFWQd4.htm](kingmaker-bestiary-items/MMamQCPSgaxFWQd4.htm)|Whip|auto-trad|
|[mMCpgVRLUjGayWTB.htm](kingmaker-bestiary-items/mMCpgVRLUjGayWTB.htm)|Darkvision|auto-trad|
|[mmgcrA8q8AjJiPoF.htm](kingmaker-bestiary-items/mmgcrA8q8AjJiPoF.htm)|Shuriken|auto-trad|
|[mMPWudSAzEWfl1eN.htm](kingmaker-bestiary-items/mMPWudSAzEWfl1eN.htm)|Composite Longbow|auto-trad|
|[mNfAyKfUwSuMResA.htm](kingmaker-bestiary-items/mNfAyKfUwSuMResA.htm)|Low-Light Vision|auto-trad|
|[moA510VYoGxOfach.htm](kingmaker-bestiary-items/moA510VYoGxOfach.htm)|Recall Lintwerth|auto-trad|
|[MP8qtlyLZ6pRMVZl.htm](kingmaker-bestiary-items/MP8qtlyLZ6pRMVZl.htm)|Dagger|auto-trad|
|[MQbQ4WqtcjmvDsJS.htm](kingmaker-bestiary-items/MQbQ4WqtcjmvDsJS.htm)|Seeking Shots|auto-trad|
|[mqqrT74LZMyFyuic.htm](kingmaker-bestiary-items/mqqrT74LZMyFyuic.htm)|Jaws|auto-trad|
|[Mqryx0fWVqgV0RiU.htm](kingmaker-bestiary-items/Mqryx0fWVqgV0RiU.htm)|Mauler|auto-trad|
|[MqUTUiuHEWGtwSkQ.htm](kingmaker-bestiary-items/MqUTUiuHEWGtwSkQ.htm)|Instinctive Cooperation|auto-trad|
|[MrbeKdSngiqGczPC.htm](kingmaker-bestiary-items/MrbeKdSngiqGczPC.htm)|Glaive|auto-trad|
|[mSjLHeaPtyB5Sjcm.htm](kingmaker-bestiary-items/mSjLHeaPtyB5Sjcm.htm)|Fist|auto-trad|
|[mTecTRC1ab5f2qOh.htm](kingmaker-bestiary-items/mTecTRC1ab5f2qOh.htm)|Negative Healing|auto-trad|
|[MUZm9jerelh5o9lK.htm](kingmaker-bestiary-items/MUZm9jerelh5o9lK.htm)|Royal Command|auto-trad|
|[MuzXu4bDzodvaZ7w.htm](kingmaker-bestiary-items/MuzXu4bDzodvaZ7w.htm)|Watery Transparency|auto-trad|
|[mVgSuJseTKH5wKIO.htm](kingmaker-bestiary-items/mVgSuJseTKH5wKIO.htm)|Twin Takedown|auto-trad|
|[MWff0Ro0CLotg930.htm](kingmaker-bestiary-items/MWff0Ro0CLotg930.htm)|Constrict|auto-trad|
|[mwIVxTyDPXVZaEcf.htm](kingmaker-bestiary-items/mwIVxTyDPXVZaEcf.htm)|Fearsome Gaze|auto-trad|
|[MwxI4D0saKJpksvK.htm](kingmaker-bestiary-items/MwxI4D0saKJpksvK.htm)|Shoulder Slam|auto-trad|
|[MxkYw0L8AzlUayt9.htm](kingmaker-bestiary-items/MxkYw0L8AzlUayt9.htm)|Rush|auto-trad|
|[MxQXSU7ZH7FLyYf5.htm](kingmaker-bestiary-items/MxQXSU7ZH7FLyYf5.htm)|Scythe|auto-trad|
|[MZGshvRJ0ZlhdQ5k.htm](kingmaker-bestiary-items/MZGshvRJ0ZlhdQ5k.htm)|Rhetorical Spell|auto-trad|
|[MzmPJ00qwdaxKPhE.htm](kingmaker-bestiary-items/MzmPJ00qwdaxKPhE.htm)|Grab|auto-trad|
|[n1fjlb5osV2yl7as.htm](kingmaker-bestiary-items/n1fjlb5osV2yl7as.htm)|+4 Status to All Saves vs. Mental|auto-trad|
|[n1p0bJc7I91avM4S.htm](kingmaker-bestiary-items/n1p0bJc7I91avM4S.htm)|Wild Empathy|auto-trad|
|[n1wsbA3ExF7FB1bG.htm](kingmaker-bestiary-items/n1wsbA3ExF7FB1bG.htm)|Occult Innate Spells|auto-trad|
|[n2BHwPEdFYp07VvG.htm](kingmaker-bestiary-items/n2BHwPEdFYp07VvG.htm)|Pseudopod|auto-trad|
|[n4Cc3VF0F86FO0sG.htm](kingmaker-bestiary-items/n4Cc3VF0F86FO0sG.htm)|Cold Adaptation|auto-trad|
|[N4dy1EmZsnQD8XR3.htm](kingmaker-bestiary-items/N4dy1EmZsnQD8XR3.htm)|Chill Breath|auto-trad|
|[N4EVKfsc2JGoy6hL.htm](kingmaker-bestiary-items/N4EVKfsc2JGoy6hL.htm)|Rider Synergy|auto-trad|
|[n4uBILCtoQcDwVBY.htm](kingmaker-bestiary-items/n4uBILCtoQcDwVBY.htm)|Occult Spontaneous Spells|auto-trad|
|[n5KuZKOcFvPmlkze.htm](kingmaker-bestiary-items/n5KuZKOcFvPmlkze.htm)|Cleric Domain Spells|auto-trad|
|[n5PTMIe2knU6k8WD.htm](kingmaker-bestiary-items/n5PTMIe2knU6k8WD.htm)|Spiked Pitfall|auto-trad|
|[N7BomXiXSkV8IDmE.htm](kingmaker-bestiary-items/N7BomXiXSkV8IDmE.htm)|Perpetual Hangover|auto-trad|
|[N8EKVbUAmiDxtOlF.htm](kingmaker-bestiary-items/N8EKVbUAmiDxtOlF.htm)|Sneak Attack|auto-trad|
|[NaDfoZlbd0uAIh3t.htm](kingmaker-bestiary-items/NaDfoZlbd0uAIh3t.htm)|Trunk|auto-trad|
|[nbVDArIMsO5ljcOd.htm](kingmaker-bestiary-items/nbVDArIMsO5ljcOd.htm)|Claw|auto-trad|
|[nC28cnIAmXUrJhdf.htm](kingmaker-bestiary-items/nC28cnIAmXUrJhdf.htm)|Rage|auto-trad|
|[nCPtTG7EkbC5KQpL.htm](kingmaker-bestiary-items/nCPtTG7EkbC5KQpL.htm)|Furious Power Attack|auto-trad|
|[NcZMn3pf8nbMeY8x.htm](kingmaker-bestiary-items/NcZMn3pf8nbMeY8x.htm)|Darkvision|auto-trad|
|[nd0M4055DPNvjNvf.htm](kingmaker-bestiary-items/nd0M4055DPNvjNvf.htm)|Manifest Shawl|auto-trad|
|[NDm80a7dNC5LKMIS.htm](kingmaker-bestiary-items/NDm80a7dNC5LKMIS.htm)|Low-Light Vision|auto-trad|
|[NHDhjNbpULppyjkP.htm](kingmaker-bestiary-items/NHDhjNbpULppyjkP.htm)|Darkvision|auto-trad|
|[nHs2Wp0Ifqpenmxk.htm](kingmaker-bestiary-items/nHs2Wp0Ifqpenmxk.htm)|Warhammer|auto-trad|
|[nHt7MnXhByHOX78E.htm](kingmaker-bestiary-items/nHt7MnXhByHOX78E.htm)|Tandem Chop|auto-trad|
|[NIgj8uAlgosxMdVf.htm](kingmaker-bestiary-items/NIgj8uAlgosxMdVf.htm)|Chew Spider Venom|auto-trad|
|[nkAaDbVaSoGjlqCp.htm](kingmaker-bestiary-items/nkAaDbVaSoGjlqCp.htm)|Poison Spray|auto-trad|
|[NKFCPYANKZVEHAVK.htm](kingmaker-bestiary-items/NKFCPYANKZVEHAVK.htm)|No Escape|auto-trad|
|[nkFUtIxpiWKJ11ko.htm](kingmaker-bestiary-items/nkFUtIxpiWKJ11ko.htm)|Hand|auto-trad|
|[Nl15bkRTLq869IPi.htm](kingmaker-bestiary-items/Nl15bkRTLq869IPi.htm)|Petrifying Glance|auto-trad|
|[nNdV2vrrzVUlwrhp.htm](kingmaker-bestiary-items/nNdV2vrrzVUlwrhp.htm)|Fast Healing 15|auto-trad|
|[NO9VvuwakH04tpcx.htm](kingmaker-bestiary-items/NO9VvuwakH04tpcx.htm)|Hatchet|auto-trad|
|[nP9Bi95YRl4E9QMt.htm](kingmaker-bestiary-items/nP9Bi95YRl4E9QMt.htm)|Greataxe|auto-trad|
|[nrmfu7IJT3ecM1M8.htm](kingmaker-bestiary-items/nrmfu7IJT3ecM1M8.htm)|Beak|auto-trad|
|[nsDXE0HKZhrqgV54.htm](kingmaker-bestiary-items/nsDXE0HKZhrqgV54.htm)|Moon Frenzy|auto-trad|
|[ntcF2Dw0m1DqvDnw.htm](kingmaker-bestiary-items/ntcF2Dw0m1DqvDnw.htm)|Dream of Rulership|auto-trad|
|[NTFiXa55jt87djrs.htm](kingmaker-bestiary-items/NTFiXa55jt87djrs.htm)|Attack of Opportunity|auto-trad|
|[NTRKdsufuPkE5ERS.htm](kingmaker-bestiary-items/NTRKdsufuPkE5ERS.htm)|Aldori Parry|auto-trad|
|[Nu5hdhS5ABZ0CZjq.htm](kingmaker-bestiary-items/Nu5hdhS5ABZ0CZjq.htm)|Twin Butchery|auto-trad|
|[nUhHPT76LVNXNYRg.htm](kingmaker-bestiary-items/nUhHPT76LVNXNYRg.htm)|Occult Spontaneous Spells|auto-trad|
|[nvkbBLwKlX05vPo2.htm](kingmaker-bestiary-items/nvkbBLwKlX05vPo2.htm)|Breath Weapon|auto-trad|
|[NWflOL2JDt8KPMH6.htm](kingmaker-bestiary-items/NWflOL2JDt8KPMH6.htm)|Rejuvenation|auto-trad|
|[Nzx1vI7LkdpuIwph.htm](kingmaker-bestiary-items/Nzx1vI7LkdpuIwph.htm)|Unseen Sight|auto-trad|
|[o1Dh44Cl7px3KGea.htm](kingmaker-bestiary-items/o1Dh44Cl7px3KGea.htm)|Grab|auto-trad|
|[O2EsiBYHC57XSjTz.htm](kingmaker-bestiary-items/O2EsiBYHC57XSjTz.htm)|Jaws|auto-trad|
|[o2schqbpr94UUldL.htm](kingmaker-bestiary-items/o2schqbpr94UUldL.htm)|Hatchet|auto-trad|
|[O2U2on80bNw6GMEe.htm](kingmaker-bestiary-items/O2U2on80bNw6GMEe.htm)|Trample|auto-trad|
|[o38MzuC03KOCw8wS.htm](kingmaker-bestiary-items/o38MzuC03KOCw8wS.htm)|Jaws That Bite|auto-trad|
|[o3sdERN8Ehqn7jo0.htm](kingmaker-bestiary-items/o3sdERN8Ehqn7jo0.htm)|Low-Light Vision|auto-trad|
|[O6kjx7rDPPICVkym.htm](kingmaker-bestiary-items/O6kjx7rDPPICVkym.htm)|Rapier|auto-trad|
|[oAdazPfRl3wntiS1.htm](kingmaker-bestiary-items/oAdazPfRl3wntiS1.htm)|Attack of Opportunity (Fangs Only)|auto-trad|
|[ObX0PxwHHLMvaTJn.htm](kingmaker-bestiary-items/ObX0PxwHHLMvaTJn.htm)|At-Will Spells|auto-trad|
|[ocYHO8rNnbCQGplD.htm](kingmaker-bestiary-items/ocYHO8rNnbCQGplD.htm)|Bewildering Hoofbeats|auto-trad|
|[oE4IMiBnWcfVyg7X.htm](kingmaker-bestiary-items/oE4IMiBnWcfVyg7X.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[Oe7KNrqiGTrNfkV9.htm](kingmaker-bestiary-items/Oe7KNrqiGTrNfkV9.htm)|Jaws|auto-trad|
|[OenA92WNHdH1I94w.htm](kingmaker-bestiary-items/OenA92WNHdH1I94w.htm)|Conjure Daemon|auto-trad|
|[Oevi96zftiaBjAOH.htm](kingmaker-bestiary-items/Oevi96zftiaBjAOH.htm)|Occult Innate Spells|auto-trad|
|[OFtZ11CEYiceib4W.htm](kingmaker-bestiary-items/OFtZ11CEYiceib4W.htm)|Rend|auto-trad|
|[ogXJyvypuEP8U1hd.htm](kingmaker-bestiary-items/ogXJyvypuEP8U1hd.htm)|Bloom Venom|auto-trad|
|[ohbFrIxS5WF1abVb.htm](kingmaker-bestiary-items/ohbFrIxS5WF1abVb.htm)|Dagger|auto-trad|
|[oHxfnh9KIO2dQcTN.htm](kingmaker-bestiary-items/oHxfnh9KIO2dQcTN.htm)|Darkvision|auto-trad|
|[OiCu7wtLBEPD8y4D.htm](kingmaker-bestiary-items/OiCu7wtLBEPD8y4D.htm)|Spear|auto-trad|
|[oka4i9kQ0yTjUsVF.htm](kingmaker-bestiary-items/oka4i9kQ0yTjUsVF.htm)|Trident|auto-trad|
|[OKH1R1FoBSEm3oKP.htm](kingmaker-bestiary-items/OKH1R1FoBSEm3oKP.htm)|Enraged Growth|auto-trad|
|[oKuYToU9Nv9ALIIv.htm](kingmaker-bestiary-items/oKuYToU9Nv9ALIIv.htm)|Rejuvenation|auto-trad|
|[okVVkEEDYOrrlhfS.htm](kingmaker-bestiary-items/okVVkEEDYOrrlhfS.htm)|Cleric Domain Spells|auto-trad|
|[OlnWMyIX3qedGUTu.htm](kingmaker-bestiary-items/OlnWMyIX3qedGUTu.htm)|Darkvision|auto-trad|
|[on7BQSeRUgbsFSSE.htm](kingmaker-bestiary-items/on7BQSeRUgbsFSSE.htm)|Knockdown|auto-trad|
|[ONo2JspAa1GnoAlR.htm](kingmaker-bestiary-items/ONo2JspAa1GnoAlR.htm)|Pierce Armor|auto-trad|
|[onRMNTdGzo2Qq4rE.htm](kingmaker-bestiary-items/onRMNTdGzo2Qq4rE.htm)|Sneak Attack|auto-trad|
|[oORyGKlMVkIFWGHJ.htm](kingmaker-bestiary-items/oORyGKlMVkIFWGHJ.htm)|Greatsword|auto-trad|
|[OpUEqqVcx7trG4mu.htm](kingmaker-bestiary-items/OpUEqqVcx7trG4mu.htm)|Kukri|auto-trad|
|[OqVIaOEX3mbFTW9O.htm](kingmaker-bestiary-items/OqVIaOEX3mbFTW9O.htm)|Arcane Spontaneous Spells|auto-trad|
|[OSCP7JLtSYLDVYrO.htm](kingmaker-bestiary-items/OSCP7JLtSYLDVYrO.htm)|Jaws|auto-trad|
|[oSq2qYAln4B4sFY0.htm](kingmaker-bestiary-items/oSq2qYAln4B4sFY0.htm)|Branch|auto-trad|
|[OtqpSI4VXEPSxQox.htm](kingmaker-bestiary-items/OtqpSI4VXEPSxQox.htm)|Intimidating Prowess|auto-trad|
|[otVXldOceOZtEV2x.htm](kingmaker-bestiary-items/otVXldOceOZtEV2x.htm)|Frightening Rattle|auto-trad|
|[oVDdlUuHM54d6gAP.htm](kingmaker-bestiary-items/oVDdlUuHM54d6gAP.htm)|At-Will Spells|auto-trad|
|[OvgHtZCLvGtWHyg9.htm](kingmaker-bestiary-items/OvgHtZCLvGtWHyg9.htm)|Raging Resistance|auto-trad|
|[OvqcWkhFk95fFEEX.htm](kingmaker-bestiary-items/OvqcWkhFk95fFEEX.htm)|Shoulder Spikes|auto-trad|
|[owZSVQs8IkCQjNLE.htm](kingmaker-bestiary-items/owZSVQs8IkCQjNLE.htm)|Composite Longbow|auto-trad|
|[oXZ9gBxLTC4PuQec.htm](kingmaker-bestiary-items/oXZ9gBxLTC4PuQec.htm)|Horns|auto-trad|
|[OYTKATaPDdautdNt.htm](kingmaker-bestiary-items/OYTKATaPDdautdNt.htm)|Wing|auto-trad|
|[OZRGnwXPUQq6AsKe.htm](kingmaker-bestiary-items/OZRGnwXPUQq6AsKe.htm)|Force Bolt|auto-trad|
|[P0EyFCnT4og9udgU.htm](kingmaker-bestiary-items/P0EyFCnT4og9udgU.htm)|Deep Breath|auto-trad|
|[P143e4Kl2GktAt4b.htm](kingmaker-bestiary-items/P143e4Kl2GktAt4b.htm)|Greatsword Critical Specialization|auto-trad|
|[p3LP4p7cOQkgP2pl.htm](kingmaker-bestiary-items/p3LP4p7cOQkgP2pl.htm)|Befuddle|auto-trad|
|[p4gHmixTKN3Gb87J.htm](kingmaker-bestiary-items/p4gHmixTKN3Gb87J.htm)|Dragon Jaws|auto-trad|
|[P5jo7Pf9aXvKHvel.htm](kingmaker-bestiary-items/P5jo7Pf9aXvKHvel.htm)|Unseen Sight|auto-trad|
|[P8P9UmFHbSmMnTnd.htm](kingmaker-bestiary-items/P8P9UmFHbSmMnTnd.htm)|Low-Light Vision|auto-trad|
|[p9eUNDT8mPVTSS1D.htm](kingmaker-bestiary-items/p9eUNDT8mPVTSS1D.htm)|Dimensional Wormhole|auto-trad|
|[PAN3R11XOGsaiIbK.htm](kingmaker-bestiary-items/PAN3R11XOGsaiIbK.htm)|Claw|auto-trad|
|[paPaJBwymUtXLyYF.htm](kingmaker-bestiary-items/paPaJBwymUtXLyYF.htm)|Whip|auto-trad|
|[PBGOOwAe8HedCuGy.htm](kingmaker-bestiary-items/PBGOOwAe8HedCuGy.htm)|Longsword|auto-trad|
|[pc3Nk8nioNXX1wuM.htm](kingmaker-bestiary-items/pc3Nk8nioNXX1wuM.htm)|Flash of Insight|auto-trad|
|[peHpZeJ7VwkuSurw.htm](kingmaker-bestiary-items/peHpZeJ7VwkuSurw.htm)|Darkvision|auto-trad|
|[pEpgPRttHusG3elF.htm](kingmaker-bestiary-items/pEpgPRttHusG3elF.htm)|Three-Headed Strike|auto-trad|
|[PF6tfwJNh5sVOavs.htm](kingmaker-bestiary-items/PF6tfwJNh5sVOavs.htm)|Thrashing Retreat|auto-trad|
|[PfK4YgueBXw7cjqo.htm](kingmaker-bestiary-items/PfK4YgueBXw7cjqo.htm)|Vine Lash|auto-trad|
|[Pfn9Ff6lgtXDae3e.htm](kingmaker-bestiary-items/Pfn9Ff6lgtXDae3e.htm)|Occult Spontaneous Spells|auto-trad|
|[pFruogrBBB4pvImp.htm](kingmaker-bestiary-items/pFruogrBBB4pvImp.htm)|Primal Prepared Spells|auto-trad|
|[pg54945zKnAqECAM.htm](kingmaker-bestiary-items/pg54945zKnAqECAM.htm)|Darkvision|auto-trad|
|[PG62mZgFRnBReYcw.htm](kingmaker-bestiary-items/PG62mZgFRnBReYcw.htm)|Word of Recall|auto-trad|
|[PhsmxJZ4aqEowjOn.htm](kingmaker-bestiary-items/PhsmxJZ4aqEowjOn.htm)|Claws|auto-trad|
|[PjBdBCA31o7FcQvI.htm](kingmaker-bestiary-items/PjBdBCA31o7FcQvI.htm)|Telepathy|auto-trad|
|[pke6bvuCCSuidMVb.htm](kingmaker-bestiary-items/pke6bvuCCSuidMVb.htm)|Poisonous Touch|auto-trad|
|[pllCGrR7E0eAP35u.htm](kingmaker-bestiary-items/pllCGrR7E0eAP35u.htm)|Dagger|auto-trad|
|[PmcZB53eH1gUzaBq.htm](kingmaker-bestiary-items/PmcZB53eH1gUzaBq.htm)|Water-Bound|auto-trad|
|[pMKjMXZobeZ6zFPm.htm](kingmaker-bestiary-items/pMKjMXZobeZ6zFPm.htm)|Primal Prepared Spells|auto-trad|
|[po8aw9EkLZ78IlVy.htm](kingmaker-bestiary-items/po8aw9EkLZ78IlVy.htm)|Hatchet|auto-trad|
|[pPppryZClUfw47fK.htm](kingmaker-bestiary-items/pPppryZClUfw47fK.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[PQMwXaG8Zqcd02Oe.htm](kingmaker-bestiary-items/PQMwXaG8Zqcd02Oe.htm)|Triple Opportunity|auto-trad|
|[pQowCb4EVsOhUgK2.htm](kingmaker-bestiary-items/pQowCb4EVsOhUgK2.htm)|Breath Weapon|auto-trad|
|[PqqO1TraA4ak7FIy.htm](kingmaker-bestiary-items/PqqO1TraA4ak7FIy.htm)|Divine Focus Spells|auto-trad|
|[pQr1ecKdOpSlfSnw.htm](kingmaker-bestiary-items/pQr1ecKdOpSlfSnw.htm)|Spiteful Command|auto-trad|
|[pQrymwjXMI3vqcOk.htm](kingmaker-bestiary-items/pQrymwjXMI3vqcOk.htm)|Negative Healing|auto-trad|
|[pR9Yyd3sYM5iPspf.htm](kingmaker-bestiary-items/pR9Yyd3sYM5iPspf.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[pRRtkR99srCWUO1j.htm](kingmaker-bestiary-items/pRRtkR99srCWUO1j.htm)|Corrosive Mass|auto-trad|
|[pSKjTPdO8e9KO9LI.htm](kingmaker-bestiary-items/pSKjTPdO8e9KO9LI.htm)|Multiple Opportunities|auto-trad|
|[psRycYQDP439XLGm.htm](kingmaker-bestiary-items/psRycYQDP439XLGm.htm)|Wavesense (Imprecise) 30 feet|auto-trad|
|[Ptn3Zb1Syk7YdL4y.htm](kingmaker-bestiary-items/Ptn3Zb1Syk7YdL4y.htm)|Claw|auto-trad|
|[PtwaILORaUXLPA4E.htm](kingmaker-bestiary-items/PtwaILORaUXLPA4E.htm)|+2 Status to All Saves vs. Cold|auto-trad|
|[pu2uW299sl7fVAeT.htm](kingmaker-bestiary-items/pu2uW299sl7fVAeT.htm)|Hoof|auto-trad|
|[pu63PF0pspvou1kE.htm](kingmaker-bestiary-items/pu63PF0pspvou1kE.htm)|Whiplashing Tail|auto-trad|
|[PueAKB1MJBXmbhHV.htm](kingmaker-bestiary-items/PueAKB1MJBXmbhHV.htm)|Formation Fighter|auto-trad|
|[pugi4AZBmL8aH1oF.htm](kingmaker-bestiary-items/pugi4AZBmL8aH1oF.htm)|Vengeful Sting|auto-trad|
|[pWFsPT5b6xUAwvpD.htm](kingmaker-bestiary-items/pWFsPT5b6xUAwvpD.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[PxCKKRzmpOzQRHlN.htm](kingmaker-bestiary-items/PxCKKRzmpOzQRHlN.htm)|Tiger Empathy|auto-trad|
|[pyyskEw6HoHX0DBp.htm](kingmaker-bestiary-items/pyyskEw6HoHX0DBp.htm)|Site Bound|auto-trad|
|[PzkoaPhLG2JLClMM.htm](kingmaker-bestiary-items/PzkoaPhLG2JLClMM.htm)|Longsword|auto-trad|
|[pzRCNxN7IHDbV6Wv.htm](kingmaker-bestiary-items/pzRCNxN7IHDbV6Wv.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[Q0VRtsqm6etoZxCa.htm](kingmaker-bestiary-items/Q0VRtsqm6etoZxCa.htm)|Shadow Doubles|auto-trad|
|[Q0WHpEJkqbeT0KZr.htm](kingmaker-bestiary-items/Q0WHpEJkqbeT0KZr.htm)|Waves of Fear|auto-trad|
|[Q0zvzLaylr0AYBGR.htm](kingmaker-bestiary-items/Q0zvzLaylr0AYBGR.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[q1z16jklLBVTY0A8.htm](kingmaker-bestiary-items/q1z16jklLBVTY0A8.htm)|Glaive|auto-trad|
|[q5MeF25mWxrHIqg3.htm](kingmaker-bestiary-items/q5MeF25mWxrHIqg3.htm)|Primal Innate Spells|auto-trad|
|[q6QpDgIvY1kfKSWc.htm](kingmaker-bestiary-items/q6QpDgIvY1kfKSWc.htm)|Dagger|auto-trad|
|[q6ZsHR2exUj0eCp7.htm](kingmaker-bestiary-items/q6ZsHR2exUj0eCp7.htm)|Aldori Dueling Sword|auto-trad|
|[Q78tMeRUfO7vehcO.htm](kingmaker-bestiary-items/Q78tMeRUfO7vehcO.htm)|Regeneration 45 (Deactivated by Fire or Negative)|auto-trad|
|[Q7g8pfqUSNczyqNo.htm](kingmaker-bestiary-items/Q7g8pfqUSNczyqNo.htm)|Attack of Opportunity|auto-trad|
|[q7jtffxL9IJqb4Pq.htm](kingmaker-bestiary-items/q7jtffxL9IJqb4Pq.htm)|Change Shape|auto-trad|
|[Q8CDVoVPdXpwkoEA.htm](kingmaker-bestiary-items/Q8CDVoVPdXpwkoEA.htm)|Constant Spells|auto-trad|
|[qAjzrkRNotMQvgjw.htm](kingmaker-bestiary-items/qAjzrkRNotMQvgjw.htm)|Infuse Arrow|auto-trad|
|[QaqXp54jr3aKQjWN.htm](kingmaker-bestiary-items/QaqXp54jr3aKQjWN.htm)|Negative Healing|auto-trad|
|[QB62e4FfQvymENul.htm](kingmaker-bestiary-items/QB62e4FfQvymENul.htm)|Tail|auto-trad|
|[qCmKPruRObzVL1Dr.htm](kingmaker-bestiary-items/qCmKPruRObzVL1Dr.htm)|Screeching Advance|auto-trad|
|[QDw6f81ZeMuIueQP.htm](kingmaker-bestiary-items/QDw6f81ZeMuIueQP.htm)|Tail|auto-trad|
|[QebQalR7fIq41wKr.htm](kingmaker-bestiary-items/QebQalR7fIq41wKr.htm)|Attack of Opportunity|auto-trad|
|[qEenLpsXQfSrqeJh.htm](kingmaker-bestiary-items/qEenLpsXQfSrqeJh.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[qeZfMzOwr8GL4Lbh.htm](kingmaker-bestiary-items/qeZfMzOwr8GL4Lbh.htm)|Jaws|auto-trad|
|[Qg6K3SNkS9IdmxUm.htm](kingmaker-bestiary-items/Qg6K3SNkS9IdmxUm.htm)|Blessed Life|auto-trad|
|[qgyml1ToPqFdJIKa.htm](kingmaker-bestiary-items/qgyml1ToPqFdJIKa.htm)|Drag Below|auto-trad|
|[qhgcqwFp1hE2jFbu.htm](kingmaker-bestiary-items/qhgcqwFp1hE2jFbu.htm)|Claw|auto-trad|
|[qhS5ZEJcqNFytqWd.htm](kingmaker-bestiary-items/qhS5ZEJcqNFytqWd.htm)|Jaws|auto-trad|
|[qiU4iKX6FIHuopg7.htm](kingmaker-bestiary-items/qiU4iKX6FIHuopg7.htm)|Sting|auto-trad|
|[QIzXNCE3GPZBj5sy.htm](kingmaker-bestiary-items/QIzXNCE3GPZBj5sy.htm)|Shortsword|auto-trad|
|[QLqI8GHyMhlnlL3h.htm](kingmaker-bestiary-items/QLqI8GHyMhlnlL3h.htm)|Rock|auto-trad|
|[qn7XE1djceSKfkuH.htm](kingmaker-bestiary-items/qn7XE1djceSKfkuH.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[QNLWcv6w045fE1oE.htm](kingmaker-bestiary-items/QNLWcv6w045fE1oE.htm)|Primal Innate Spells|auto-trad|
|[qOhxTyF0N56kRCTR.htm](kingmaker-bestiary-items/qOhxTyF0N56kRCTR.htm)|All-Around Vision|auto-trad|
|[qpAN2omg7HvCwOzk.htm](kingmaker-bestiary-items/qpAN2omg7HvCwOzk.htm)|Undetectable|auto-trad|
|[QQaSJEmqMeqq6kd9.htm](kingmaker-bestiary-items/QQaSJEmqMeqq6kd9.htm)|Lurker Claw|auto-trad|
|[qQD7sRQC5iCPloAd.htm](kingmaker-bestiary-items/qQD7sRQC5iCPloAd.htm)|Ectoplasmic Maneuver|auto-trad|
|[qqqYIHulqpSQmjAc.htm](kingmaker-bestiary-items/qqqYIHulqpSQmjAc.htm)|Primal Innate Spells|auto-trad|
|[qQY3snkHHYYEQDxW.htm](kingmaker-bestiary-items/qQY3snkHHYYEQDxW.htm)|Primal Spontaneous Spells|auto-trad|
|[QrCROiN8OwsCLDUj.htm](kingmaker-bestiary-items/QrCROiN8OwsCLDUj.htm)|Skirmish Strike|auto-trad|
|[QryAkNRvJCn0uaSN.htm](kingmaker-bestiary-items/QryAkNRvJCn0uaSN.htm)|Dagger|auto-trad|
|[QsQBz0EKCXO9IzXT.htm](kingmaker-bestiary-items/QsQBz0EKCXO9IzXT.htm)|Rapier|auto-trad|
|[QtiX9tEriHb92lTU.htm](kingmaker-bestiary-items/QtiX9tEriHb92lTU.htm)|Pounce|auto-trad|
|[qTKs06NZl2BkZiYP.htm](kingmaker-bestiary-items/qTKs06NZl2BkZiYP.htm)|+1 Status to All Saves vs. Positive|auto-trad|
|[QTnEqxIt2MaL0WlZ.htm](kingmaker-bestiary-items/QTnEqxIt2MaL0WlZ.htm)|Spirit Naga Venom|auto-trad|
|[qtXj0TB36n30L7my.htm](kingmaker-bestiary-items/qtXj0TB36n30L7my.htm)|Disarm|auto-trad|
|[qVOjbIdihxei6lTm.htm](kingmaker-bestiary-items/qVOjbIdihxei6lTm.htm)|Composite Longbow|auto-trad|
|[QXYemYqhU6zdoBsz.htm](kingmaker-bestiary-items/QXYemYqhU6zdoBsz.htm)|Reach Spell|auto-trad|
|[QXYKDwpj06Pd7sEo.htm](kingmaker-bestiary-items/QXYKDwpj06Pd7sEo.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[QXyRODDmF4tozV5P.htm](kingmaker-bestiary-items/QXyRODDmF4tozV5P.htm)|Longsword|auto-trad|
|[qYTvq8RiCpkT6wG7.htm](kingmaker-bestiary-items/qYTvq8RiCpkT6wG7.htm)|Dagger|auto-trad|
|[qztKctN0HIhpmOSy.htm](kingmaker-bestiary-items/qztKctN0HIhpmOSy.htm)|Low-Light Vision|auto-trad|
|[r0B1Lotd2T85M2u2.htm](kingmaker-bestiary-items/r0B1Lotd2T85M2u2.htm)|Low-Light Vision|auto-trad|
|[R2ZrKwkuQb3XIG4z.htm](kingmaker-bestiary-items/R2ZrKwkuQb3XIG4z.htm)|Dagger|auto-trad|
|[R3HSbrKclMuhH014.htm](kingmaker-bestiary-items/R3HSbrKclMuhH014.htm)|Planar Acclimation|auto-trad|
|[r5yruYZzaDx9MoHR.htm](kingmaker-bestiary-items/r5yruYZzaDx9MoHR.htm)|Low-Light Vision|auto-trad|
|[R6qqK2TgPEbgEcH0.htm](kingmaker-bestiary-items/R6qqK2TgPEbgEcH0.htm)|Defensive Burst|auto-trad|
|[R8f6nx9jMnFB03qP.htm](kingmaker-bestiary-items/R8f6nx9jMnFB03qP.htm)|Savage|auto-trad|
|[Ral7Zi8ooe0f3emu.htm](kingmaker-bestiary-items/Ral7Zi8ooe0f3emu.htm)|Moon Frenzy|auto-trad|
|[rbZIdY30oD5udoGi.htm](kingmaker-bestiary-items/rbZIdY30oD5udoGi.htm)|Occult Focus Spells|auto-trad|
|[rcNrrQtcdNppDhtd.htm](kingmaker-bestiary-items/rcNrrQtcdNppDhtd.htm)|Feasting Bite|auto-trad|
|[rdf4aOsP7jywGEIC.htm](kingmaker-bestiary-items/rdf4aOsP7jywGEIC.htm)|Primal Innate Spells|auto-trad|
|[RflctXAVkddCvSeq.htm](kingmaker-bestiary-items/RflctXAVkddCvSeq.htm)|Jaws|auto-trad|
|[rGbtqEmUFQBjGvOd.htm](kingmaker-bestiary-items/rGbtqEmUFQBjGvOd.htm)|Hungersense (Imprecise) 30 feet|auto-trad|
|[RiC8zTuKfW44G8Mp.htm](kingmaker-bestiary-items/RiC8zTuKfW44G8Mp.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[RjfMaoY3Fp7Ouab8.htm](kingmaker-bestiary-items/RjfMaoY3Fp7Ouab8.htm)|Tremorsense|auto-trad|
|[rk9Qa3TRqh7vnR4r.htm](kingmaker-bestiary-items/rk9Qa3TRqh7vnR4r.htm)|Deep Breath|auto-trad|
|[rkdBPs8QgQ9Tfvpj.htm](kingmaker-bestiary-items/rkdBPs8QgQ9Tfvpj.htm)|Ovinrbaane|auto-trad|
|[rl3BJy5yXHIijmZz.htm](kingmaker-bestiary-items/rl3BJy5yXHIijmZz.htm)|Darkvision|auto-trad|
|[RLutvx6GldUXGhjl.htm](kingmaker-bestiary-items/RLutvx6GldUXGhjl.htm)|Twin Parry|auto-trad|
|[rlwsvNyOSTobwhA9.htm](kingmaker-bestiary-items/rlwsvNyOSTobwhA9.htm)|Centipede Swarm Venom|auto-trad|
|[rNPNkpT1CEzgR8Ry.htm](kingmaker-bestiary-items/rNPNkpT1CEzgR8Ry.htm)|Unnerving Prowess|auto-trad|
|[rnQSJr976QIURT9w.htm](kingmaker-bestiary-items/rnQSJr976QIURT9w.htm)|Songstrike|auto-trad|
|[rog22HpLDkUnfolA.htm](kingmaker-bestiary-items/rog22HpLDkUnfolA.htm)|Goat Horns|auto-trad|
|[roURyHTV2DxU7WTQ.htm](kingmaker-bestiary-items/roURyHTV2DxU7WTQ.htm)|Darkvision|auto-trad|
|[RrDmY99gWZJtFtjo.htm](kingmaker-bestiary-items/RrDmY99gWZJtFtjo.htm)|Greater Darkvision|auto-trad|
|[rTLmI9nr5Sji2tZD.htm](kingmaker-bestiary-items/rTLmI9nr5Sji2tZD.htm)|Axe Critical Specialization|auto-trad|
|[rtPLskLwq9ldQzDC.htm](kingmaker-bestiary-items/rtPLskLwq9ldQzDC.htm)|Scalding Burst|auto-trad|
|[RUcX4XrsDpkjIiPy.htm](kingmaker-bestiary-items/RUcX4XrsDpkjIiPy.htm)|Primal Innate Spells|auto-trad|
|[rwJegRqmKknX4v9P.htm](kingmaker-bestiary-items/rwJegRqmKknX4v9P.htm)|Attack of Opportunity|auto-trad|
|[rWVM81zPBmywUXvF.htm](kingmaker-bestiary-items/rWVM81zPBmywUXvF.htm)|Occult Spontaneous Spells|auto-trad|
|[RXMeyIF1blCFYaFi.htm](kingmaker-bestiary-items/RXMeyIF1blCFYaFi.htm)|Darkvision|auto-trad|
|[rZftbooG5W57BA0g.htm](kingmaker-bestiary-items/rZftbooG5W57BA0g.htm)|Divine Prepared Spells|auto-trad|
|[S2bjckIBFgZGuliH.htm](kingmaker-bestiary-items/S2bjckIBFgZGuliH.htm)|Ogre Hook|auto-trad|
|[s2robi6KwZM7XeVw.htm](kingmaker-bestiary-items/s2robi6KwZM7XeVw.htm)|Falling Portcullis|auto-trad|
|[s5DYWIxSytUTy1x2.htm](kingmaker-bestiary-items/s5DYWIxSytUTy1x2.htm)|Fangs|auto-trad|
|[S6FJYbxVlzTTwn0T.htm](kingmaker-bestiary-items/S6FJYbxVlzTTwn0T.htm)|Smoke Vision|auto-trad|
|[S6sguxBDLkRhBNAS.htm](kingmaker-bestiary-items/S6sguxBDLkRhBNAS.htm)|Divine Prepared Spells|auto-trad|
|[S7QInhdroOaf7k84.htm](kingmaker-bestiary-items/S7QInhdroOaf7k84.htm)|Tremorsense (Precise in Area A7, Otherwise Imprecise) 60 feet|auto-trad|
|[S9OYItI2DPSsb4Za.htm](kingmaker-bestiary-items/S9OYItI2DPSsb4Za.htm)|Switch Fables|auto-trad|
|[sA9IvnRiBKdOegEF.htm](kingmaker-bestiary-items/sA9IvnRiBKdOegEF.htm)|Wing|auto-trad|
|[SaMUZistsZa0Bskd.htm](kingmaker-bestiary-items/SaMUZistsZa0Bskd.htm)|All-Around Vision|auto-trad|
|[SaS7qHeGtLYWa1rX.htm](kingmaker-bestiary-items/SaS7qHeGtLYWa1rX.htm)|Tail Claw|auto-trad|
|[SbBp90Oz1LZfllYh.htm](kingmaker-bestiary-items/SbBp90Oz1LZfllYh.htm)|Spear|auto-trad|
|[sCOLNwAp0vM1QRy9.htm](kingmaker-bestiary-items/sCOLNwAp0vM1QRy9.htm)|Baleful Gaze|auto-trad|
|[Sd3o6E29DT50HqQ5.htm](kingmaker-bestiary-items/Sd3o6E29DT50HqQ5.htm)|Greater Darkvision|auto-trad|
|[sED3umw26WGHjqa6.htm](kingmaker-bestiary-items/sED3umw26WGHjqa6.htm)|Consume Memories|auto-trad|
|[SeJXe222cuooTJUx.htm](kingmaker-bestiary-items/SeJXe222cuooTJUx.htm)|Split|auto-trad|
|[sfWdhP3dRBdtmiol.htm](kingmaker-bestiary-items/sfWdhP3dRBdtmiol.htm)|Blood Drain|auto-trad|
|[sHdlA1TMLwvXBRX0.htm](kingmaker-bestiary-items/sHdlA1TMLwvXBRX0.htm)|Composite Shortbow|auto-trad|
|[sHRh4SLqM1rBlpHb.htm](kingmaker-bestiary-items/sHRh4SLqM1rBlpHb.htm)|Attack of Opportunity|auto-trad|
|[sIdAMHJe8g1PQUF7.htm](kingmaker-bestiary-items/sIdAMHJe8g1PQUF7.htm)|Low-Light Vision|auto-trad|
|[SKOscQwReKF5ZqAY.htm](kingmaker-bestiary-items/SKOscQwReKF5ZqAY.htm)|Maming Chop|auto-trad|
|[smt8XqXd55pl6Gwi.htm](kingmaker-bestiary-items/smt8XqXd55pl6Gwi.htm)|Clamp Shut|auto-trad|
|[Sn0vYKbSWgQmv5g7.htm](kingmaker-bestiary-items/Sn0vYKbSWgQmv5g7.htm)|Cleric Domain Spells|auto-trad|
|[snOrUVs5f8v8gtuP.htm](kingmaker-bestiary-items/snOrUVs5f8v8gtuP.htm)|Graveknight's Curse|auto-trad|
|[sNUoqbThXZbfj5S7.htm](kingmaker-bestiary-items/sNUoqbThXZbfj5S7.htm)|Fade from View|auto-trad|
|[snWh3FVKpgmQAHQz.htm](kingmaker-bestiary-items/snWh3FVKpgmQAHQz.htm)|Disfigure|auto-trad|
|[spF1Kzai1HQbME5t.htm](kingmaker-bestiary-items/spF1Kzai1HQbME5t.htm)|Bloom Regeneration|auto-trad|
|[SPiKrNADm6u2qbhl.htm](kingmaker-bestiary-items/SPiKrNADm6u2qbhl.htm)|Triumphant Roar|auto-trad|
|[SPkIUm3665kXInNU.htm](kingmaker-bestiary-items/SPkIUm3665kXInNU.htm)|Primal Innate Spells|auto-trad|
|[SPTwJf1wzeADk4sn.htm](kingmaker-bestiary-items/SPTwJf1wzeADk4sn.htm)|Stinger|auto-trad|
|[SpVGiNHO8YKLWtpn.htm](kingmaker-bestiary-items/SpVGiNHO8YKLWtpn.htm)|Quick Alchemy|auto-trad|
|[SQ4DUNap1xbzs5Sq.htm](kingmaker-bestiary-items/SQ4DUNap1xbzs5Sq.htm)|Jaws|auto-trad|
|[sqceAJWlpdKGnHvx.htm](kingmaker-bestiary-items/sqceAJWlpdKGnHvx.htm)|Beak|auto-trad|
|[SqomM8yPUdX1yPY8.htm](kingmaker-bestiary-items/SqomM8yPUdX1yPY8.htm)|Attack of Opportunity|auto-trad|
|[SRv8Gjga6DHY4U8G.htm](kingmaker-bestiary-items/SRv8Gjga6DHY4U8G.htm)|Corrupt Water|auto-trad|
|[ssgy6SBt7L8uYlUi.htm](kingmaker-bestiary-items/ssgy6SBt7L8uYlUi.htm)|Arcane Innate Spells|auto-trad|
|[sT3ZX2Zurexp4hLH.htm](kingmaker-bestiary-items/sT3ZX2Zurexp4hLH.htm)|Steady Spellcasting|auto-trad|
|[sT4lUsg0seVtpUKQ.htm](kingmaker-bestiary-items/sT4lUsg0seVtpUKQ.htm)|Claw|auto-trad|
|[sU4JRJE89RWZ5Z6a.htm](kingmaker-bestiary-items/sU4JRJE89RWZ5Z6a.htm)|Scent (Precise) 120 feet|auto-trad|
|[sw2mWVFaQb93J9el.htm](kingmaker-bestiary-items/sw2mWVFaQb93J9el.htm)|Forager|auto-trad|
|[swHSxsAoATAgMMX2.htm](kingmaker-bestiary-items/swHSxsAoATAgMMX2.htm)|Ride Tick|auto-trad|
|[Swn94Gh9zgVIcHmO.htm](kingmaker-bestiary-items/Swn94Gh9zgVIcHmO.htm)|Ankle Bite|auto-trad|
|[SYMyLr9FwZW5kVcr.htm](kingmaker-bestiary-items/SYMyLr9FwZW5kVcr.htm)|Attack of Opportunity|auto-trad|
|[SZOur85Nka45xVe7.htm](kingmaker-bestiary-items/SZOur85Nka45xVe7.htm)|Frightful Moan|auto-trad|
|[T0IRXqqR5Zr2FXsp.htm](kingmaker-bestiary-items/T0IRXqqR5Zr2FXsp.htm)|Negative Healing|auto-trad|
|[T0KOWsF7QwJ1oDQq.htm](kingmaker-bestiary-items/T0KOWsF7QwJ1oDQq.htm)|Low-Light Vision|auto-trad|
|[T1dMCJU34dJYVgZw.htm](kingmaker-bestiary-items/T1dMCJU34dJYVgZw.htm)|Club|auto-trad|
|[T1JhJ4gf00b500cI.htm](kingmaker-bestiary-items/T1JhJ4gf00b500cI.htm)|Attack of Opportunity|auto-trad|
|[T1WSPSyMbIh6gzFQ.htm](kingmaker-bestiary-items/T1WSPSyMbIh6gzFQ.htm)|Catch Rock|auto-trad|
|[T3OviChVRRywo6sB.htm](kingmaker-bestiary-items/T3OviChVRRywo6sB.htm)|Mocking Laughter|auto-trad|
|[t49UcMd23mTLwrj5.htm](kingmaker-bestiary-items/t49UcMd23mTLwrj5.htm)|Paralyzing Touch|auto-trad|
|[t4BxYpWMm3lEqjxG.htm](kingmaker-bestiary-items/t4BxYpWMm3lEqjxG.htm)|Occult Spontaneous Spells|auto-trad|
|[T5dyuJhTrUcC9fqv.htm](kingmaker-bestiary-items/T5dyuJhTrUcC9fqv.htm)|Claw|auto-trad|
|[t5Pe2D0CJm0NY3E4.htm](kingmaker-bestiary-items/t5Pe2D0CJm0NY3E4.htm)|Claw|auto-trad|
|[t6KHJCiZXdNJnqxB.htm](kingmaker-bestiary-items/t6KHJCiZXdNJnqxB.htm)|Weapon Master|auto-trad|
|[T9wwE2uJLrI8NxyM.htm](kingmaker-bestiary-items/T9wwE2uJLrI8NxyM.htm)|Bear Trap|auto-trad|
|[taxWERmxNkRViJLR.htm](kingmaker-bestiary-items/taxWERmxNkRViJLR.htm)|Greataxe|auto-trad|
|[TBlBPLZKvFBHjjgU.htm](kingmaker-bestiary-items/TBlBPLZKvFBHjjgU.htm)|Slow|auto-trad|
|[TCbRZkzWxt6vXhCx.htm](kingmaker-bestiary-items/TCbRZkzWxt6vXhCx.htm)|+2 Status to All Saves vs. Fear Effects|auto-trad|
|[tDlyF5IZNsYAYYhP.htm](kingmaker-bestiary-items/tDlyF5IZNsYAYYhP.htm)|Thrown Object|auto-trad|
|[tEPc3Xc3Pufff6D5.htm](kingmaker-bestiary-items/tEPc3Xc3Pufff6D5.htm)|Drench|auto-trad|
|[TfSlL9Vupc8WC8jy.htm](kingmaker-bestiary-items/TfSlL9Vupc8WC8jy.htm)|Buck|auto-trad|
|[TGbnXBgCIKg1wFwT.htm](kingmaker-bestiary-items/TGbnXBgCIKg1wFwT.htm)|Targeting Shot|auto-trad|
|[thieXYs44gA9P3Uo.htm](kingmaker-bestiary-items/thieXYs44gA9P3Uo.htm)|Divine Innate Spells|auto-trad|
|[TIGvNHFixJK8sJCg.htm](kingmaker-bestiary-items/TIGvNHFixJK8sJCg.htm)|Low-Light Vision|auto-trad|
|[TILteUNBjDkjtfg6.htm](kingmaker-bestiary-items/TILteUNBjDkjtfg6.htm)|Inspiration|auto-trad|
|[TIWU41UBtl2ksfmx.htm](kingmaker-bestiary-items/TIWU41UBtl2ksfmx.htm)|Twin Riposte|auto-trad|
|[tJSXokhRWx6GoMJe.htm](kingmaker-bestiary-items/tJSXokhRWx6GoMJe.htm)|Bleed for Lamashtu|auto-trad|
|[tKFw0mI1NIfpLYBn.htm](kingmaker-bestiary-items/tKFw0mI1NIfpLYBn.htm)|Unbalancing Blow|auto-trad|
|[TKjiZlqXKYAz5MTX.htm](kingmaker-bestiary-items/TKjiZlqXKYAz5MTX.htm)|All-Around Vision|auto-trad|
|[tkP4RVEwAR4aHYbk.htm](kingmaker-bestiary-items/tkP4RVEwAR4aHYbk.htm)|Darkvision|auto-trad|
|[tOCS9mkYR5K1LuOZ.htm](kingmaker-bestiary-items/tOCS9mkYR5K1LuOZ.htm)|Petrifying Gaze|auto-trad|
|[tqOuI2ayN5VhcNyW.htm](kingmaker-bestiary-items/tqOuI2ayN5VhcNyW.htm)|Hand Crossbow|auto-trad|
|[TqvPH7FdjdwawFbW.htm](kingmaker-bestiary-items/TqvPH7FdjdwawFbW.htm)|Shield Block|auto-trad|
|[TR472pgOTxUJrN9B.htm](kingmaker-bestiary-items/TR472pgOTxUJrN9B.htm)|Club|auto-trad|
|[ttXiy5Zys3PzCa8f.htm](kingmaker-bestiary-items/ttXiy5Zys3PzCa8f.htm)|Tortuous Touch|auto-trad|
|[TUJZ5ff9G3KgBsCN.htm](kingmaker-bestiary-items/TUJZ5ff9G3KgBsCN.htm)|Darkvision|auto-trad|
|[TvcbacSr4PLH15CQ.htm](kingmaker-bestiary-items/TvcbacSr4PLH15CQ.htm)|Curse of the Werewolf|auto-trad|
|[tvFxmbJKPxnCnTbb.htm](kingmaker-bestiary-items/tvFxmbJKPxnCnTbb.htm)|Vermin Empathy|auto-trad|
|[tvr5xWTm58t3Dgy3.htm](kingmaker-bestiary-items/tvr5xWTm58t3Dgy3.htm)|Crumbling Edge|auto-trad|
|[TwMRnjbKEUPZwBKx.htm](kingmaker-bestiary-items/TwMRnjbKEUPZwBKx.htm)|Divine Font|auto-trad|
|[TwVHeJqYT1io5LxX.htm](kingmaker-bestiary-items/TwVHeJqYT1io5LxX.htm)|Summon Pack|auto-trad|
|[TxCWNmfsEkdTzNvw.htm](kingmaker-bestiary-items/TxCWNmfsEkdTzNvw.htm)|Outside of Time|auto-trad|
|[TxKSVVgJ5OZw3cy0.htm](kingmaker-bestiary-items/TxKSVVgJ5OZw3cy0.htm)|Outside of Time|auto-trad|
|[TyHmjC5wvxo29J5L.htm](kingmaker-bestiary-items/TyHmjC5wvxo29J5L.htm)|Grab|auto-trad|
|[tz6uqrNAY2ySZ2TY.htm](kingmaker-bestiary-items/tz6uqrNAY2ySZ2TY.htm)|Of Three Minds|auto-trad|
|[Tzi4lR0Oigo4U8Wu.htm](kingmaker-bestiary-items/Tzi4lR0Oigo4U8Wu.htm)|Emerald Beam|auto-trad|
|[u0gmeeDEvRzdbvFs.htm](kingmaker-bestiary-items/u0gmeeDEvRzdbvFs.htm)|Negative Healing|auto-trad|
|[U22LSFuhWJzxq7TO.htm](kingmaker-bestiary-items/U22LSFuhWJzxq7TO.htm)|Damsel Act|auto-trad|
|[u6tuxFGclP3Cf3u2.htm](kingmaker-bestiary-items/u6tuxFGclP3Cf3u2.htm)|Otherworldly Mind|auto-trad|
|[u8YE1BntdMHM1ks0.htm](kingmaker-bestiary-items/u8YE1BntdMHM1ks0.htm)|At-Will Spells|auto-trad|
|[uAlQecVurdHtCKds.htm](kingmaker-bestiary-items/uAlQecVurdHtCKds.htm)|Primal Innate Spells|auto-trad|
|[ubHtu9kCddCNuEjt.htm](kingmaker-bestiary-items/ubHtu9kCddCNuEjt.htm)|Low-Light Vision|auto-trad|
|[Ubytn72fPKxfwdab.htm](kingmaker-bestiary-items/Ubytn72fPKxfwdab.htm)|Dagger|auto-trad|
|[ucBBQrbesTH1rIdv.htm](kingmaker-bestiary-items/ucBBQrbesTH1rIdv.htm)|Light Hammer|auto-trad|
|[uCIO2gyqCCw5HMiZ.htm](kingmaker-bestiary-items/uCIO2gyqCCw5HMiZ.htm)|Quick Bomber|auto-trad|
|[UcQu3REAJGDNFfE0.htm](kingmaker-bestiary-items/UcQu3REAJGDNFfE0.htm)|Captivating Pollen|auto-trad|
|[uD9MleR0PzjUXugC.htm](kingmaker-bestiary-items/uD9MleR0PzjUXugC.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[UdmjBD1i3DxTYEep.htm](kingmaker-bestiary-items/UdmjBD1i3DxTYEep.htm)|Telekinetic Assault|auto-trad|
|[UGbMxXUr68ITj7ND.htm](kingmaker-bestiary-items/UGbMxXUr68ITj7ND.htm)|Drain Lintwerth|auto-trad|
|[UiApvNrZOpUiRTze.htm](kingmaker-bestiary-items/UiApvNrZOpUiRTze.htm)|Centipede Mandibles|auto-trad|
|[UIGEr5LUogZmy8ln.htm](kingmaker-bestiary-items/UIGEr5LUogZmy8ln.htm)|Phantasmagoric Fog|auto-trad|
|[uiZk5k4gZbLlAo44.htm](kingmaker-bestiary-items/uiZk5k4gZbLlAo44.htm)|Rejuvenation|auto-trad|
|[UJnXwBbuFjAcC01q.htm](kingmaker-bestiary-items/UJnXwBbuFjAcC01q.htm)|Unsettling Attention|auto-trad|
|[UkuRoGDf37QBLm8k.htm](kingmaker-bestiary-items/UkuRoGDf37QBLm8k.htm)|Ice Stride|auto-trad|
|[ulr3xQ9vjVzN0JcV.htm](kingmaker-bestiary-items/ulr3xQ9vjVzN0JcV.htm)|Constant Spells|auto-trad|
|[UlSgJk73FttvDVKx.htm](kingmaker-bestiary-items/UlSgJk73FttvDVKx.htm)|Attack of Opportunity|auto-trad|
|[uMg5j8kbgsMRkJNO.htm](kingmaker-bestiary-items/uMg5j8kbgsMRkJNO.htm)|Invoke Old Sharptooth|auto-trad|
|[uox8BdqT7SHwPAXF.htm](kingmaker-bestiary-items/uox8BdqT7SHwPAXF.htm)|Backlash|auto-trad|
|[Up8YexckH0qYu7Mg.htm](kingmaker-bestiary-items/Up8YexckH0qYu7Mg.htm)|Hatchet|auto-trad|
|[uQFiXxjHXOCxLHe8.htm](kingmaker-bestiary-items/uQFiXxjHXOCxLHe8.htm)|Vulnerability to Supernatural Darkness|auto-trad|
|[UqivmeGpdoq5TEFp.htm](kingmaker-bestiary-items/UqivmeGpdoq5TEFp.htm)|Manifest Fetch Weapon|auto-trad|
|[Us7BcRckPUy5WuaZ.htm](kingmaker-bestiary-items/Us7BcRckPUy5WuaZ.htm)|Swallow Whole|auto-trad|
|[USCoVo5LJNs12zB4.htm](kingmaker-bestiary-items/USCoVo5LJNs12zB4.htm)|Go Dark|auto-trad|
|[utrBYZarwPMS7ACv.htm](kingmaker-bestiary-items/utrBYZarwPMS7ACv.htm)|Resolve|auto-trad|
|[UtXatkaqCOacbbnj.htm](kingmaker-bestiary-items/UtXatkaqCOacbbnj.htm)|Jaws|auto-trad|
|[uuI4sId0T1tDVCla.htm](kingmaker-bestiary-items/uuI4sId0T1tDVCla.htm)|Darkvision|auto-trad|
|[uv3BAfwMvrlyZDI6.htm](kingmaker-bestiary-items/uv3BAfwMvrlyZDI6.htm)|Fist|auto-trad|
|[UVFdUW8UmQmQjvIs.htm](kingmaker-bestiary-items/UVFdUW8UmQmQjvIs.htm)|Low-Light Vision|auto-trad|
|[UW8qbPcbWR2N4HcI.htm](kingmaker-bestiary-items/UW8qbPcbWR2N4HcI.htm)|Darkvision|auto-trad|
|[uYqNB5URDvfKjzHe.htm](kingmaker-bestiary-items/uYqNB5URDvfKjzHe.htm)|Morningstar|auto-trad|
|[v0ppjSET59hXq9Z5.htm](kingmaker-bestiary-items/v0ppjSET59hXq9Z5.htm)|Dagger|auto-trad|
|[v4dtDXWYnbH5R47w.htm](kingmaker-bestiary-items/v4dtDXWYnbH5R47w.htm)|Fearsome Blizzard|auto-trad|
|[V4WvWjukxtroI8Xs.htm](kingmaker-bestiary-items/V4WvWjukxtroI8Xs.htm)|Swallow Whole|auto-trad|
|[v65HAbd7O3gZv2VP.htm](kingmaker-bestiary-items/v65HAbd7O3gZv2VP.htm)|Eerie Flexibility|auto-trad|
|[V6fjZPcQEMD98LxA.htm](kingmaker-bestiary-items/V6fjZPcQEMD98LxA.htm)|Dramatic Disarm|auto-trad|
|[v76tHhlGlywyinPF.htm](kingmaker-bestiary-items/v76tHhlGlywyinPF.htm)|Shortbow|auto-trad|
|[V7siOMUXReEpRR0u.htm](kingmaker-bestiary-items/V7siOMUXReEpRR0u.htm)|Dagger|auto-trad|
|[v8fWAG80JbMt4nOy.htm](kingmaker-bestiary-items/v8fWAG80JbMt4nOy.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[V8J7D9aK5N4MpaBI.htm](kingmaker-bestiary-items/V8J7D9aK5N4MpaBI.htm)|Lifesense|auto-trad|
|[VBMoJbMPEmzIPu5X.htm](kingmaker-bestiary-items/VBMoJbMPEmzIPu5X.htm)|Chill Breath|auto-trad|
|[vCJLJPrm6qXxNRzd.htm](kingmaker-bestiary-items/vCJLJPrm6qXxNRzd.htm)|Divine Font (Harm)|auto-trad|
|[VCxhoISThjwcy55r.htm](kingmaker-bestiary-items/VCxhoISThjwcy55r.htm)|Attack of Opportunity|auto-trad|
|[Vd2tGIDTvBddHJ36.htm](kingmaker-bestiary-items/Vd2tGIDTvBddHJ36.htm)|Arcane Focus Spells|auto-trad|
|[VDa8nmlZXUFf1Y7z.htm](kingmaker-bestiary-items/VDa8nmlZXUFf1Y7z.htm)|Jaws|auto-trad|
|[VdKXa5UE8pjAkhRN.htm](kingmaker-bestiary-items/VdKXa5UE8pjAkhRN.htm)|Staff|auto-trad|
|[VdlfnhKj8BudHVMB.htm](kingmaker-bestiary-items/VdlfnhKj8BudHVMB.htm)|Trickster's Step|auto-trad|
|[VEZWJVB54nkdpnVP.htm](kingmaker-bestiary-items/VEZWJVB54nkdpnVP.htm)|At-Will Spells|auto-trad|
|[VGrJpWKaRTR27Qyu.htm](kingmaker-bestiary-items/VGrJpWKaRTR27Qyu.htm)|Arcane Prepared Spells|auto-trad|
|[viDna1Oudpf22Ieg.htm](kingmaker-bestiary-items/viDna1Oudpf22Ieg.htm)|Blowgun|auto-trad|
|[vius8NahqzVZYRSE.htm](kingmaker-bestiary-items/vius8NahqzVZYRSE.htm)|Play the Pipes|auto-trad|
|[vJiFJxTfxGmRSWe0.htm](kingmaker-bestiary-items/vJiFJxTfxGmRSWe0.htm)|Composite Shortbow|auto-trad|
|[VKxXRGAQaygpB7SH.htm](kingmaker-bestiary-items/VKxXRGAQaygpB7SH.htm)|Jaws|auto-trad|
|[vLKZN5X8t0pUwZJd.htm](kingmaker-bestiary-items/vLKZN5X8t0pUwZJd.htm)|Improved Knockdown|auto-trad|
|[vllPNnBmoSwFEtl9.htm](kingmaker-bestiary-items/vllPNnBmoSwFEtl9.htm)|Guarded Movement|auto-trad|
|[VmmlUbjFsbcN9dM8.htm](kingmaker-bestiary-items/VmmlUbjFsbcN9dM8.htm)|Sneak Attack|auto-trad|
|[vMozJKWMEqEZvBOl.htm](kingmaker-bestiary-items/vMozJKWMEqEZvBOl.htm)|Jaws|auto-trad|
|[VPg7zeVby8h8yTMX.htm](kingmaker-bestiary-items/VPg7zeVby8h8yTMX.htm)|Bark Orders|auto-trad|
|[VPpeBvTnuLzMsNon.htm](kingmaker-bestiary-items/VPpeBvTnuLzMsNon.htm)|Primal Prepared Spells|auto-trad|
|[VPVWe9JZVLpTixZC.htm](kingmaker-bestiary-items/VPVWe9JZVLpTixZC.htm)|Change Shape|auto-trad|
|[VQW9KJrLdz5lDjJ9.htm](kingmaker-bestiary-items/VQW9KJrLdz5lDjJ9.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[VQzmSFhPlPCnWRYb.htm](kingmaker-bestiary-items/VQzmSFhPlPCnWRYb.htm)|Arcane Innate Spells|auto-trad|
|[vRSrxRGekX69z1xM.htm](kingmaker-bestiary-items/vRSrxRGekX69z1xM.htm)|Greatsword|auto-trad|
|[vrZuMJAZcSYNFRp1.htm](kingmaker-bestiary-items/vrZuMJAZcSYNFRp1.htm)|Woodland Stride|auto-trad|
|[VTjvgSGDhiMzsKdY.htm](kingmaker-bestiary-items/VTjvgSGDhiMzsKdY.htm)|Frenzied Fangs|auto-trad|
|[vTXaShzYmcNbRjzk.htm](kingmaker-bestiary-items/vTXaShzYmcNbRjzk.htm)|Bloodcurdling Screech|auto-trad|
|[vudcfpUT9un2pDOP.htm](kingmaker-bestiary-items/vudcfpUT9un2pDOP.htm)|Gnaw|auto-trad|
|[VUyMebZxthD1vUj3.htm](kingmaker-bestiary-items/VUyMebZxthD1vUj3.htm)|Composite Longbow|auto-trad|
|[vvB4kICd5X8HaFue.htm](kingmaker-bestiary-items/vvB4kICd5X8HaFue.htm)|Rage|auto-trad|
|[VViv6cOIpMtNUZzW.htm](kingmaker-bestiary-items/VViv6cOIpMtNUZzW.htm)|Brutal Gore|auto-trad|
|[vVt7KeVPbqT0jHZQ.htm](kingmaker-bestiary-items/vVt7KeVPbqT0jHZQ.htm)|Clutch|auto-trad|
|[VWC3NNUYpN6qbf09.htm](kingmaker-bestiary-items/VWC3NNUYpN6qbf09.htm)|Breath Weapon|auto-trad|
|[vYdUPrjuB8HrzGBA.htm](kingmaker-bestiary-items/vYdUPrjuB8HrzGBA.htm)|Attack of Opportunity|auto-trad|
|[VyJyNz5V3NR5Gylr.htm](kingmaker-bestiary-items/VyJyNz5V3NR5Gylr.htm)|Axe Vulnerability 20|auto-trad|
|[vYngXSj2DDVpfWFe.htm](kingmaker-bestiary-items/vYngXSj2DDVpfWFe.htm)|Meddling Tail|auto-trad|
|[vYo1emHFZe2lLLBo.htm](kingmaker-bestiary-items/vYo1emHFZe2lLLBo.htm)|Constant Spells|auto-trad|
|[VyTDZWtEYeQGBrXk.htm](kingmaker-bestiary-items/VyTDZWtEYeQGBrXk.htm)|Darkvision|auto-trad|
|[VzK5CD8g2ofWRIkk.htm](kingmaker-bestiary-items/VzK5CD8g2ofWRIkk.htm)|Improved Grab|auto-trad|
|[VzrK9L3fTzGmyHto.htm](kingmaker-bestiary-items/VzrK9L3fTzGmyHto.htm)|Druid Order Spells|auto-trad|
|[w0XMZ6GmkzJcCRyO.htm](kingmaker-bestiary-items/w0XMZ6GmkzJcCRyO.htm)|Apocalyptic Shriek|auto-trad|
|[w4Cd5RsR9MjHTRDj.htm](kingmaker-bestiary-items/w4Cd5RsR9MjHTRDj.htm)|Darkvision|auto-trad|
|[w5FhuQgOnX0i92Y3.htm](kingmaker-bestiary-items/w5FhuQgOnX0i92Y3.htm)|Jaws|auto-trad|
|[w5ySqTQIaFATIjDM.htm](kingmaker-bestiary-items/w5ySqTQIaFATIjDM.htm)|Arcane Innate Spells|auto-trad|
|[wAjwXDiTkvisDxZR.htm](kingmaker-bestiary-items/wAjwXDiTkvisDxZR.htm)|Survival of the Fittest|auto-trad|
|[wb6prflG67LiVXh1.htm](kingmaker-bestiary-items/wb6prflG67LiVXh1.htm)|Trackless Step|auto-trad|
|[wbtt2m0nhK330JwC.htm](kingmaker-bestiary-items/wbtt2m0nhK330JwC.htm)|Hatchet|auto-trad|
|[WBvBXluaSwwxLaJ4.htm](kingmaker-bestiary-items/WBvBXluaSwwxLaJ4.htm)|Sneak Attack|auto-trad|
|[wChn2PEw9GYziq2n.htm](kingmaker-bestiary-items/wChn2PEw9GYziq2n.htm)|Glow|auto-trad|
|[WdOsTqGjiU4ZnX5H.htm](kingmaker-bestiary-items/WdOsTqGjiU4ZnX5H.htm)|Darkvision|auto-trad|
|[WDpA7ff1gJiokx9N.htm](kingmaker-bestiary-items/WDpA7ff1gJiokx9N.htm)|Hunted Shot|auto-trad|
|[we4aKsadr3FYXQCP.htm](kingmaker-bestiary-items/we4aKsadr3FYXQCP.htm)|Primal Prepared Spells|auto-trad|
|[WeXirhh6gD0DCTGB.htm](kingmaker-bestiary-items/WeXirhh6gD0DCTGB.htm)|Root|auto-trad|
|[Wez6v2oJoatkzzVp.htm](kingmaker-bestiary-items/Wez6v2oJoatkzzVp.htm)|Throw Rock|auto-trad|
|[WF3kud5WAVewvKL0.htm](kingmaker-bestiary-items/WF3kud5WAVewvKL0.htm)|Low-Light Vision|auto-trad|
|[wFieL2rsS0tFWUAR.htm](kingmaker-bestiary-items/wFieL2rsS0tFWUAR.htm)|Regeneration 20 (Deactivated by Cold Iron or Lawful)|auto-trad|
|[WhCqHDOg0A8FK3IF.htm](kingmaker-bestiary-items/WhCqHDOg0A8FK3IF.htm)|Relentless Tracker|auto-trad|
|[WIPFxMzyJ87R3t7k.htm](kingmaker-bestiary-items/WIPFxMzyJ87R3t7k.htm)|Supernatural Speed|auto-trad|
|[WIPJ7Xl3dOhNuLlq.htm](kingmaker-bestiary-items/WIPJ7Xl3dOhNuLlq.htm)|Counterspell|auto-trad|
|[WJaww98OCHbzRIIP.htm](kingmaker-bestiary-items/WJaww98OCHbzRIIP.htm)|Jaws|auto-trad|
|[WjBhdxKcekKdof75.htm](kingmaker-bestiary-items/WjBhdxKcekKdof75.htm)|Darkvision|auto-trad|
|[Wjfmdyuddn5fCyVZ.htm](kingmaker-bestiary-items/Wjfmdyuddn5fCyVZ.htm)|Insult Challenges|auto-trad|
|[wkahGwEkL5eYhtZy.htm](kingmaker-bestiary-items/wkahGwEkL5eYhtZy.htm)|Tail|auto-trad|
|[WNANaLjOqPHdTPjN.htm](kingmaker-bestiary-items/WNANaLjOqPHdTPjN.htm)|Dagger|auto-trad|
|[wO3hSmCvgcCzSah3.htm](kingmaker-bestiary-items/wO3hSmCvgcCzSah3.htm)|Bloom Curse|auto-trad|
|[WOqo2UC99RfWFqA0.htm](kingmaker-bestiary-items/WOqo2UC99RfWFqA0.htm)|Darkvision|auto-trad|
|[WpEtlM3mpICnsXPm.htm](kingmaker-bestiary-items/WpEtlM3mpICnsXPm.htm)|Steady Spellcasting|auto-trad|
|[wrisTXltKN2ggF1B.htm](kingmaker-bestiary-items/wrisTXltKN2ggF1B.htm)|Immense Mandragora Venom|auto-trad|
|[wRiugNyM38JRtkeI.htm](kingmaker-bestiary-items/wRiugNyM38JRtkeI.htm)|Coven Spells|auto-trad|
|[WRLKbO1wNweUuW4n.htm](kingmaker-bestiary-items/WRLKbO1wNweUuW4n.htm)|Longspear|auto-trad|
|[WrmnFHWCqCQMpQlB.htm](kingmaker-bestiary-items/WrmnFHWCqCQMpQlB.htm)|Pin to the Sky|auto-trad|
|[wSYY3PhHktTGw8ua.htm](kingmaker-bestiary-items/wSYY3PhHktTGw8ua.htm)|Baroness's Scream|auto-trad|
|[Wt9KDemV1jGIVjE5.htm](kingmaker-bestiary-items/Wt9KDemV1jGIVjE5.htm)|Catch Rock|auto-trad|
|[WTjCVJwNHH8vUKzE.htm](kingmaker-bestiary-items/WTjCVJwNHH8vUKzE.htm)|Sorcerer Bloodline Spells|auto-trad|
|[WU4cXLzhnoCczalO.htm](kingmaker-bestiary-items/WU4cXLzhnoCczalO.htm)|Irritating Dander|auto-trad|
|[wuin1zchdZrKVQEY.htm](kingmaker-bestiary-items/wuin1zchdZrKVQEY.htm)|Dagger|auto-trad|
|[WVChw6DFgtcfsrAf.htm](kingmaker-bestiary-items/WVChw6DFgtcfsrAf.htm)|Swarming Beaks|auto-trad|
|[WvKgLpntjekJJHp7.htm](kingmaker-bestiary-items/WvKgLpntjekJJHp7.htm)|Sneak Attack|auto-trad|
|[wXPOAgEeixFbagOW.htm](kingmaker-bestiary-items/wXPOAgEeixFbagOW.htm)|Swift Summon|auto-trad|
|[wyAVtZ4X5ErNqi3C.htm](kingmaker-bestiary-items/wyAVtZ4X5ErNqi3C.htm)|Sneak Attack|auto-trad|
|[wYKABRKZFADU4eFV.htm](kingmaker-bestiary-items/wYKABRKZFADU4eFV.htm)|Fortune's Friend|auto-trad|
|[WyzkB1NHDFF0YsAm.htm](kingmaker-bestiary-items/WyzkB1NHDFF0YsAm.htm)|Improved Grab|auto-trad|
|[wzltAJwbP73KlolU.htm](kingmaker-bestiary-items/wzltAJwbP73KlolU.htm)|Itchy|auto-trad|
|[x1BxQoUjfPHH06yh.htm](kingmaker-bestiary-items/x1BxQoUjfPHH06yh.htm)|Dagger|auto-trad|
|[X1qtaRb6bkYt1N3s.htm](kingmaker-bestiary-items/X1qtaRb6bkYt1N3s.htm)|Attack of Opportunity|auto-trad|
|[X3C10c6qy59DlNdh.htm](kingmaker-bestiary-items/X3C10c6qy59DlNdh.htm)|Occult Innate Spells|auto-trad|
|[X3HpUq80mQWyiHZ3.htm](kingmaker-bestiary-items/X3HpUq80mQWyiHZ3.htm)|Hunt Prey|auto-trad|
|[x3yCpybINerCQY3Q.htm](kingmaker-bestiary-items/x3yCpybINerCQY3Q.htm)|Corrosive Touch|auto-trad|
|[X41cWt7idOHqasPx.htm](kingmaker-bestiary-items/X41cWt7idOHqasPx.htm)|Greatsword|auto-trad|
|[x46bcv7xjs0irbqk.htm](kingmaker-bestiary-items/x46bcv7xjs0irbqk.htm)|Morningstar|auto-trad|
|[x46WnJD9YKQXpRkE.htm](kingmaker-bestiary-items/x46WnJD9YKQXpRkE.htm)|Raging Storm|auto-trad|
|[x8mIj7POmkmnt5WS.htm](kingmaker-bestiary-items/x8mIj7POmkmnt5WS.htm)|Divine Recovery|auto-trad|
|[X9BvGRPY6jnbRCrG.htm](kingmaker-bestiary-items/X9BvGRPY6jnbRCrG.htm)|Throw Rock|auto-trad|
|[xBi3GMrwnT7AE4mq.htm](kingmaker-bestiary-items/xBi3GMrwnT7AE4mq.htm)|Grabbing Trunk|auto-trad|
|[xBIoPO0Yl00JwtXW.htm](kingmaker-bestiary-items/xBIoPO0Yl00JwtXW.htm)|Swamp Stride|auto-trad|
|[xBvjgSrMNlbRs2vt.htm](kingmaker-bestiary-items/xBvjgSrMNlbRs2vt.htm)|Crossbow|auto-trad|
|[Xc15ip4V31NZTNdq.htm](kingmaker-bestiary-items/Xc15ip4V31NZTNdq.htm)|Frightful Presence|auto-trad|
|[XCFOQ29kYuqzushV.htm](kingmaker-bestiary-items/XCFOQ29kYuqzushV.htm)|Claw|auto-trad|
|[XCr9jxxE59Egb2R1.htm](kingmaker-bestiary-items/XCr9jxxE59Egb2R1.htm)|Woodland Ambush|auto-trad|
|[XCuiusaM9Pgov7Hy.htm](kingmaker-bestiary-items/XCuiusaM9Pgov7Hy.htm)|Reactive|auto-trad|
|[XCvqYacJqKAyKyBy.htm](kingmaker-bestiary-items/XCvqYacJqKAyKyBy.htm)|Change Shape|auto-trad|
|[xdpY6vYKdagXO6bk.htm](kingmaker-bestiary-items/xdpY6vYKdagXO6bk.htm)|Bard Composition Spells|auto-trad|
|[XdUyVqH2LISYTKP4.htm](kingmaker-bestiary-items/XdUyVqH2LISYTKP4.htm)|Sickle|auto-trad|
|[xeAR997MOrxz4cyA.htm](kingmaker-bestiary-items/xeAR997MOrxz4cyA.htm)|Gallop|auto-trad|
|[XEczLnuwuxklAds8.htm](kingmaker-bestiary-items/XEczLnuwuxklAds8.htm)|Adjust Temperature|auto-trad|
|[xeEI4dqDE4PmxOeA.htm](kingmaker-bestiary-items/xeEI4dqDE4PmxOeA.htm)|Forest Walker|auto-trad|
|[XEeQ6n37g3yEASJB.htm](kingmaker-bestiary-items/XEeQ6n37g3yEASJB.htm)|Dagger|auto-trad|
|[xEHY10Oxi5BJJCmQ.htm](kingmaker-bestiary-items/xEHY10Oxi5BJJCmQ.htm)|Wide Swing|auto-trad|
|[XfQQjjOdt5sRiSb6.htm](kingmaker-bestiary-items/XfQQjjOdt5sRiSb6.htm)|Lion Jaws|auto-trad|
|[XFTxhLhYbwJ3pxdt.htm](kingmaker-bestiary-items/XFTxhLhYbwJ3pxdt.htm)|Unseen Sight|auto-trad|
|[XfUDUv0iccZ8hNjg.htm](kingmaker-bestiary-items/XfUDUv0iccZ8hNjg.htm)|Negative Healing|auto-trad|
|[XFWwEVmfkf4Naohf.htm](kingmaker-bestiary-items/XFWwEVmfkf4Naohf.htm)|Jaws|auto-trad|
|[XgjH4JAk9jbj9B8a.htm](kingmaker-bestiary-items/XgjH4JAk9jbj9B8a.htm)|Shamble|auto-trad|
|[XgoaORovhBAB82vg.htm](kingmaker-bestiary-items/XgoaORovhBAB82vg.htm)|Constant Spells|auto-trad|
|[xH6NrBw4bYEHU7e3.htm](kingmaker-bestiary-items/xH6NrBw4bYEHU7e3.htm)|Sacrilegious Aura|auto-trad|
|[XhhOGnxq5cUHkcEd.htm](kingmaker-bestiary-items/XhhOGnxq5cUHkcEd.htm)|Burble|auto-trad|
|[XHQxmGEm71C14Nci.htm](kingmaker-bestiary-items/XHQxmGEm71C14Nci.htm)|Grab|auto-trad|
|[XhrxL6SNJhTojhys.htm](kingmaker-bestiary-items/XhrxL6SNJhTojhys.htm)|Manifest Crystals|auto-trad|
|[xI9jqkNAsWJ8GrxC.htm](kingmaker-bestiary-items/xI9jqkNAsWJ8GrxC.htm)|Collapse|auto-trad|
|[xIXTT704xBEHdGIn.htm](kingmaker-bestiary-items/xIXTT704xBEHdGIn.htm)|Dagger|auto-trad|
|[XJXxOQSoroFvm4m9.htm](kingmaker-bestiary-items/XJXxOQSoroFvm4m9.htm)|Constant Spells|auto-trad|
|[xkPOrd9a16OIHTyl.htm](kingmaker-bestiary-items/xkPOrd9a16OIHTyl.htm)|Second Wind|auto-trad|
|[XKVb3v9QlngaKRQV.htm](kingmaker-bestiary-items/XKVb3v9QlngaKRQV.htm)|Sneak Attack|auto-trad|
|[XmKUjz23s0dNFOYW.htm](kingmaker-bestiary-items/XmKUjz23s0dNFOYW.htm)|Bog Rot|auto-trad|
|[xMM6S0SqKD29GPxJ.htm](kingmaker-bestiary-items/xMM6S0SqKD29GPxJ.htm)|Site Bound|auto-trad|
|[Xmq08HzwUTnliDuF.htm](kingmaker-bestiary-items/Xmq08HzwUTnliDuF.htm)|Fist|auto-trad|
|[XnVq5NAwuLiDa4dD.htm](kingmaker-bestiary-items/XnVq5NAwuLiDa4dD.htm)|Sudden Stormburst|auto-trad|
|[xq3V1P1OXiEGcqZs.htm](kingmaker-bestiary-items/xq3V1P1OXiEGcqZs.htm)|Angled Entry|auto-trad|
|[xqB9FvxJUsDUDs15.htm](kingmaker-bestiary-items/xqB9FvxJUsDUDs15.htm)|Thorny Vine|auto-trad|
|[xqRgagTvArv2abwD.htm](kingmaker-bestiary-items/xqRgagTvArv2abwD.htm)|Constant Spells|auto-trad|
|[XR8SUq7u8lPNwfsz.htm](kingmaker-bestiary-items/XR8SUq7u8lPNwfsz.htm)|Overhand Smash|auto-trad|
|[xRQZegRo2JOZB7hU.htm](kingmaker-bestiary-items/xRQZegRo2JOZB7hU.htm)|Low-Light Vision|auto-trad|
|[xrTNYpA1abm4LPwG.htm](kingmaker-bestiary-items/xrTNYpA1abm4LPwG.htm)|Coven|auto-trad|
|[xtGHTPAuSTsudXMG.htm](kingmaker-bestiary-items/xtGHTPAuSTsudXMG.htm)|Enhanced Familiar|auto-trad|
|[xtitjNKcLXuYUcnS.htm](kingmaker-bestiary-items/xtitjNKcLXuYUcnS.htm)|Reel In|auto-trad|
|[XU7E8CJIc6dwyRml.htm](kingmaker-bestiary-items/XU7E8CJIc6dwyRml.htm)|Knockback|auto-trad|
|[Xva9qQPkH1v1bsG1.htm](kingmaker-bestiary-items/Xva9qQPkH1v1bsG1.htm)|Floor Collapse|auto-trad|
|[xwHcXDjVTSiAD1kB.htm](kingmaker-bestiary-items/xwHcXDjVTSiAD1kB.htm)|Wing Rebuff|auto-trad|
|[xxUW9YkqVCI57nCo.htm](kingmaker-bestiary-items/xxUW9YkqVCI57nCo.htm)|Painful Caress|auto-trad|
|[xXv6eUeKYXjO80rc.htm](kingmaker-bestiary-items/xXv6eUeKYXjO80rc.htm)|Swoop|auto-trad|
|[xYyZLfzVCUdoVnQn.htm](kingmaker-bestiary-items/xYyZLfzVCUdoVnQn.htm)|Reach Spell|auto-trad|
|[xzuGv08xYVhfHdXW.htm](kingmaker-bestiary-items/xzuGv08xYVhfHdXW.htm)|Tail|auto-trad|
|[xZwLtaE6SEA50Rqf.htm](kingmaker-bestiary-items/xZwLtaE6SEA50Rqf.htm)|Twin Chop|auto-trad|
|[Y0nwjk1NkdGHxBxi.htm](kingmaker-bestiary-items/Y0nwjk1NkdGHxBxi.htm)|Change Shape|auto-trad|
|[y1uZ9I98YpDpmpQx.htm](kingmaker-bestiary-items/y1uZ9I98YpDpmpQx.htm)|At-Will Spells|auto-trad|
|[y6KKrN8Aiff22yLS.htm](kingmaker-bestiary-items/y6KKrN8Aiff22yLS.htm)|Bloodline Magic|auto-trad|
|[y8OK5g64U5E8qC0q.htm](kingmaker-bestiary-items/y8OK5g64U5E8qC0q.htm)|Vicious Ranseur|auto-trad|
|[Y9nkyrHVb6qQd6hs.htm](kingmaker-bestiary-items/Y9nkyrHVb6qQd6hs.htm)|Shame|auto-trad|
|[YaqUxn6Ybbvxv3gB.htm](kingmaker-bestiary-items/YaqUxn6Ybbvxv3gB.htm)|Quill|auto-trad|
|[yaWanh7jgvzb4bNA.htm](kingmaker-bestiary-items/yaWanh7jgvzb4bNA.htm)|Jaws|auto-trad|
|[YBRIxpMXUrxJfeV1.htm](kingmaker-bestiary-items/YBRIxpMXUrxJfeV1.htm)|Grab|auto-trad|
|[YCWMeM1zCWZqP48p.htm](kingmaker-bestiary-items/YCWMeM1zCWZqP48p.htm)|Rod of Razors|auto-trad|
|[YdimsTpSLxkBXaBh.htm](kingmaker-bestiary-items/YdimsTpSLxkBXaBh.htm)|Arcane Spontaneous Spells|auto-trad|
|[yEmvQZDwtr985C2Y.htm](kingmaker-bestiary-items/yEmvQZDwtr985C2Y.htm)|Hatchet Flurry|auto-trad|
|[YeVYuXNHpkcHdBo2.htm](kingmaker-bestiary-items/YeVYuXNHpkcHdBo2.htm)|Attack of Opportunity (Tail Only)|auto-trad|
|[YfFUNMAKaJ0wspVA.htm](kingmaker-bestiary-items/YfFUNMAKaJ0wspVA.htm)|Darkvision|auto-trad|
|[ygeIx11fATpKbQRh.htm](kingmaker-bestiary-items/ygeIx11fATpKbQRh.htm)|At-Will Spells|auto-trad|
|[yGkx5YIsqb7P1b34.htm](kingmaker-bestiary-items/yGkx5YIsqb7P1b34.htm)|Effortless Concentration|auto-trad|
|[Yh6PceHFS60uaThW.htm](kingmaker-bestiary-items/Yh6PceHFS60uaThW.htm)|Release Boulders|auto-trad|
|[YHmyiWDMvEgujfl1.htm](kingmaker-bestiary-items/YHmyiWDMvEgujfl1.htm)|Bully's Bludgeon|auto-trad|
|[yI8AXFiHc0FyemjU.htm](kingmaker-bestiary-items/yI8AXFiHc0FyemjU.htm)|Ominous Mien|auto-trad|
|[yIiN4CzNAnrIGY5R.htm](kingmaker-bestiary-items/yIiN4CzNAnrIGY5R.htm)|Low-Light Vision|auto-trad|
|[yJ37AcaU1dl5m7KT.htm](kingmaker-bestiary-items/yJ37AcaU1dl5m7KT.htm)|Dagger|auto-trad|
|[yJ6tSdgHei2VkcFn.htm](kingmaker-bestiary-items/yJ6tSdgHei2VkcFn.htm)|Swift Claw|auto-trad|
|[YJesTEWYLdHelRVi.htm](kingmaker-bestiary-items/YJesTEWYLdHelRVi.htm)|At-Will Spells|auto-trad|
|[ykE4J2hEBa7gWmVx.htm](kingmaker-bestiary-items/ykE4J2hEBa7gWmVx.htm)|Bloom Step|auto-trad|
|[yKwcZmq0W6TpvY7z.htm](kingmaker-bestiary-items/yKwcZmq0W6TpvY7z.htm)|Lance|auto-trad|
|[yLgcfIF0Rbavu6Tr.htm](kingmaker-bestiary-items/yLgcfIF0Rbavu6Tr.htm)|Glaive|auto-trad|
|[Ym0WhQCJHDGT3v4z.htm](kingmaker-bestiary-items/Ym0WhQCJHDGT3v4z.htm)|Attack of Opportunity (Special)|auto-trad|
|[YMNKladez96kdBX8.htm](kingmaker-bestiary-items/YMNKladez96kdBX8.htm)|Nimble Dodge|auto-trad|
|[yp7FDjO7715BHNPw.htm](kingmaker-bestiary-items/yp7FDjO7715BHNPw.htm)|Darkvision|auto-trad|
|[yPpDjeNgrvnyJ092.htm](kingmaker-bestiary-items/yPpDjeNgrvnyJ092.htm)|Distort Senses|auto-trad|
|[Yq84rD7Ts0Wko9Pf.htm](kingmaker-bestiary-items/Yq84rD7Ts0Wko9Pf.htm)|Rip and Drag|auto-trad|
|[yR0motfxtDNCXbQv.htm](kingmaker-bestiary-items/yR0motfxtDNCXbQv.htm)|Felling Blow|auto-trad|
|[yrKXLmbaekza05cd.htm](kingmaker-bestiary-items/yrKXLmbaekza05cd.htm)|Improved Push 10 feet|auto-trad|
|[yS5bYDhKr2Koa3mm.htm](kingmaker-bestiary-items/yS5bYDhKr2Koa3mm.htm)|No Time To Die|auto-trad|
|[yTVotZYCI6abxOvt.htm](kingmaker-bestiary-items/yTVotZYCI6abxOvt.htm)|Bastard Sword|auto-trad|
|[yTWeFCTfaEECHhCs.htm](kingmaker-bestiary-items/yTWeFCTfaEECHhCs.htm)|Falchion|auto-trad|
|[yvOTSeGrhPgBSOBZ.htm](kingmaker-bestiary-items/yvOTSeGrhPgBSOBZ.htm)|Capsize|auto-trad|
|[YwEDGRZpJYEj2PKw.htm](kingmaker-bestiary-items/YwEDGRZpJYEj2PKw.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[yWipQYXVRovUBD3A.htm](kingmaker-bestiary-items/yWipQYXVRovUBD3A.htm)|At-Will Spells|auto-trad|
|[yX5Z8bj03S67Thb5.htm](kingmaker-bestiary-items/yX5Z8bj03S67Thb5.htm)|Primal Spontaneous Spells|auto-trad|
|[YXiHAcR16OtpzApO.htm](kingmaker-bestiary-items/YXiHAcR16OtpzApO.htm)|Speed Surge|auto-trad|
|[YXm2W59CMTMhb25j.htm](kingmaker-bestiary-items/YXm2W59CMTMhb25j.htm)|Jaws|auto-trad|
|[YxX6oMlTOXzTGheb.htm](kingmaker-bestiary-items/YxX6oMlTOXzTGheb.htm)|Occult Innate Spells|auto-trad|
|[YyxKJRRXl2GdUfTt.htm](kingmaker-bestiary-items/YyxKJRRXl2GdUfTt.htm)|Darkvision|auto-trad|
|[yZCKHSiJwqO7N1sy.htm](kingmaker-bestiary-items/yZCKHSiJwqO7N1sy.htm)|Claws That Catch|auto-trad|
|[Z08HAZUvnJBGvImG.htm](kingmaker-bestiary-items/Z08HAZUvnJBGvImG.htm)|Swarm Mind|auto-trad|
|[z0QT6nvmIHbeoGHa.htm](kingmaker-bestiary-items/z0QT6nvmIHbeoGHa.htm)|Jaws|auto-trad|
|[z3giiwwqKX9RBgD8.htm](kingmaker-bestiary-items/z3giiwwqKX9RBgD8.htm)|Spew Seed Pod|auto-trad|
|[Z5F5Xx9nQAfj1TtM.htm](kingmaker-bestiary-items/Z5F5Xx9nQAfj1TtM.htm)|Fireball|auto-trad|
|[Z5Ph0zkg0xMl6iK6.htm](kingmaker-bestiary-items/Z5Ph0zkg0xMl6iK6.htm)|Occult Innate Spells|auto-trad|
|[Z65zRWuULtfR4enK.htm](kingmaker-bestiary-items/Z65zRWuULtfR4enK.htm)|Attack of Opportunity|auto-trad|
|[z8bl09rqF4yCJDy4.htm](kingmaker-bestiary-items/z8bl09rqF4yCJDy4.htm)|Gogunta's Croak|auto-trad|
|[z9bTah65DoASEDhP.htm](kingmaker-bestiary-items/z9bTah65DoASEDhP.htm)|Cobra Fangs|auto-trad|
|[Z9mCi4uEoEQi6uMv.htm](kingmaker-bestiary-items/Z9mCi4uEoEQi6uMv.htm)|Quick Block|auto-trad|
|[ZaZTsIwbHhL3ZcQy.htm](kingmaker-bestiary-items/ZaZTsIwbHhL3ZcQy.htm)|Battle Axe|auto-trad|
|[ZBxbAEeCdREK43Yu.htm](kingmaker-bestiary-items/ZBxbAEeCdREK43Yu.htm)|Bard Composition Spells|auto-trad|
|[Zc2k22zs45gbuybT.htm](kingmaker-bestiary-items/Zc2k22zs45gbuybT.htm)|Low-Light Vision|auto-trad|
|[ZCOJIf8Nr9RxA4Pj.htm](kingmaker-bestiary-items/ZCOJIf8Nr9RxA4Pj.htm)|Occult Spontaneous Spells|auto-trad|
|[zDDzvsB3OrJU9QXn.htm](kingmaker-bestiary-items/zDDzvsB3OrJU9QXn.htm)|Vigorous Shake|auto-trad|
|[zdtGqzcFir4opahs.htm](kingmaker-bestiary-items/zdtGqzcFir4opahs.htm)|Refocus Curse|auto-trad|
|[zeyB4l2CYjcqiDrH.htm](kingmaker-bestiary-items/zeyB4l2CYjcqiDrH.htm)|Claw|auto-trad|
|[zfVISHBP6rc5o8Rv.htm](kingmaker-bestiary-items/zfVISHBP6rc5o8Rv.htm)|Exile's Curse|auto-trad|
|[ZfVwcTmrbWAVaOCX.htm](kingmaker-bestiary-items/ZfVwcTmrbWAVaOCX.htm)|Improved Grab|auto-trad|
|[ZG61qjm93pm36Iw5.htm](kingmaker-bestiary-items/ZG61qjm93pm36Iw5.htm)|Darkvision|auto-trad|
|[zgdHnMuj8S2a5m8L.htm](kingmaker-bestiary-items/zgdHnMuj8S2a5m8L.htm)|Ghostly Hand|auto-trad|
|[zGXZKhk95VPsSdKN.htm](kingmaker-bestiary-items/zGXZKhk95VPsSdKN.htm)|Biting Snakes|auto-trad|
|[ZH9w82rcAvicGUV6.htm](kingmaker-bestiary-items/ZH9w82rcAvicGUV6.htm)|Menace Prey|auto-trad|
|[ziktYNU02MOBNRm7.htm](kingmaker-bestiary-items/ziktYNU02MOBNRm7.htm)|Infectious Wrath|auto-trad|
|[ZiobchoAZw8EGQAl.htm](kingmaker-bestiary-items/ZiobchoAZw8EGQAl.htm)|Recovery|auto-trad|
|[zKvBvpelIhRXPoSW.htm](kingmaker-bestiary-items/zKvBvpelIhRXPoSW.htm)|Thoughtsense|auto-trad|
|[ZlCrr2J1Yth6IZEj.htm](kingmaker-bestiary-items/ZlCrr2J1Yth6IZEj.htm)|Lamashtu's Bloom|auto-trad|
|[zm4JTE61At7s9UrX.htm](kingmaker-bestiary-items/zm4JTE61At7s9UrX.htm)|Blow Mists|auto-trad|
|[zMgcdf4D6sTVSdON.htm](kingmaker-bestiary-items/zMgcdf4D6sTVSdON.htm)|Scent|auto-trad|
|[zNeDe02hwHzcyE1s.htm](kingmaker-bestiary-items/zNeDe02hwHzcyE1s.htm)|Squirming Embrace|auto-trad|
|[zNJKK7fb7gXv78OV.htm](kingmaker-bestiary-items/zNJKK7fb7gXv78OV.htm)|Warhammer|auto-trad|
|[ZNWj00PO55PVPFmc.htm](kingmaker-bestiary-items/ZNWj00PO55PVPFmc.htm)|Shortsword|auto-trad|
|[ZNWKdUJvc61evvBc.htm](kingmaker-bestiary-items/ZNWKdUJvc61evvBc.htm)|Wide Swing|auto-trad|
|[zOBaEVlReAlrAIKE.htm](kingmaker-bestiary-items/zOBaEVlReAlrAIKE.htm)|Lonely Dirge|auto-trad|
|[ZOJd2q3zsGLrA8Xw.htm](kingmaker-bestiary-items/ZOJd2q3zsGLrA8Xw.htm)|Divine Prepared Spells|auto-trad|
|[ZPVlL1UH75oMFTCB.htm](kingmaker-bestiary-items/ZPVlL1UH75oMFTCB.htm)|Armag's Rage|auto-trad|
|[zquNaeOnacgZG38j.htm](kingmaker-bestiary-items/zquNaeOnacgZG38j.htm)|Darkvision|auto-trad|
|[ZQWoQnlsIHN6EyFc.htm](kingmaker-bestiary-items/ZQWoQnlsIHN6EyFc.htm)|Wolf Jaws|auto-trad|
|[ZR6JE7Gh0fgDobEm.htm](kingmaker-bestiary-items/ZR6JE7Gh0fgDobEm.htm)|Lurker Stance|auto-trad|
|[zRI7pbJvbWWPBJQr.htm](kingmaker-bestiary-items/zRI7pbJvbWWPBJQr.htm)|Low-Light Vision|auto-trad|
|[ZrwMBFIWJnpx6Utu.htm](kingmaker-bestiary-items/ZrwMBFIWJnpx6Utu.htm)|Dread Striker|auto-trad|
|[ZSab4ZN954h6Ckqb.htm](kingmaker-bestiary-items/ZSab4ZN954h6Ckqb.htm)|Atrophied Lich|auto-trad|
|[ztl64mYGjNx9Qab6.htm](kingmaker-bestiary-items/ztl64mYGjNx9Qab6.htm)|Sorcerer Bloodline Spells|auto-trad|
|[ZvqnxVWvm9QdJZVi.htm](kingmaker-bestiary-items/ZvqnxVWvm9QdJZVi.htm)|Blood Scent|auto-trad|
|[zWVvmrN2Kq0dZWsu.htm](kingmaker-bestiary-items/zWVvmrN2Kq0dZWsu.htm)|Freezing Wind|auto-trad|
|[ZYFSO8d8sBeN7WO7.htm](kingmaker-bestiary-items/ZYFSO8d8sBeN7WO7.htm)|Unsettling Revelation|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0sXAwqL8Tb6XqJ0H.htm](kingmaker-bestiary-items/0sXAwqL8Tb6XqJ0H.htm)|Lantern King's Glow|Resplandor del Rey Linterna|modificada|
|[H8mIfS4BQ0XpEAtY.htm](kingmaker-bestiary-items/H8mIfS4BQ0XpEAtY.htm)|Launch Marbles|Lanzar Canicas|modificada|
|[iGnRoOsUlKx1zO7Q.htm](kingmaker-bestiary-items/iGnRoOsUlKx1zO7Q.htm)|Death Throes|Estertores de muerte|modificada|
|[kZiYWe3uWt1JBdbS.htm](kingmaker-bestiary-items/kZiYWe3uWt1JBdbS.htm)|Familiar|Familiar|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0Rjcnl6ynsH5LKjO.htm](kingmaker-bestiary-items/0Rjcnl6ynsH5LKjO.htm)|Warfare Lore|vacía|
|[0UqqnUjTvhlc7sGv.htm](kingmaker-bestiary-items/0UqqnUjTvhlc7sGv.htm)|Bardic Lore|vacía|
|[2wByf3NmPbP0YRzy.htm](kingmaker-bestiary-items/2wByf3NmPbP0YRzy.htm)|Leng Ruby|vacía|
|[38JeDzTzMbmPrydc.htm](kingmaker-bestiary-items/38JeDzTzMbmPrydc.htm)|Survival|vacía|
|[4KoYrmcXEdMKXapg.htm](kingmaker-bestiary-items/4KoYrmcXEdMKXapg.htm)|Academia Lore|vacía|
|[4uhmqqCnN4EAqpVy.htm](kingmaker-bestiary-items/4uhmqqCnN4EAqpVy.htm)|Rock|vacía|
|[5c6243RybWvondkY.htm](kingmaker-bestiary-items/5c6243RybWvondkY.htm)|Silver Stag Lord Amulet|vacía|
|[5MTbqNxF7xLIRqQY.htm](kingmaker-bestiary-items/5MTbqNxF7xLIRqQY.htm)|Abaddon Lore|vacía|
|[5yj5ekSognH3S3RW.htm](kingmaker-bestiary-items/5yj5ekSognH3S3RW.htm)|Silver Stag Lord Amulet|vacía|
|[61i4ipb2qXp6RshD.htm](kingmaker-bestiary-items/61i4ipb2qXp6RshD.htm)|Lumber Lore|vacía|
|[64snLNQ9P2c8IffF.htm](kingmaker-bestiary-items/64snLNQ9P2c8IffF.htm)|Guild Lore|vacía|
|[6ofr6YtODR30jRp0.htm](kingmaker-bestiary-items/6ofr6YtODR30jRp0.htm)|Lice-Infested Furs|vacía|
|[7ovDTTZtTBqn2eJ6.htm](kingmaker-bestiary-items/7ovDTTZtTBqn2eJ6.htm)|Performance|vacía|
|[7zyckBxX0Qo3fPo5.htm](kingmaker-bestiary-items/7zyckBxX0Qo3fPo5.htm)|Chain of Lovelies|vacía|
|[8EaMfqK0UpEoUqcg.htm](kingmaker-bestiary-items/8EaMfqK0UpEoUqcg.htm)|Acrobatics|vacía|
|[8I1oL3XnWQjb1WeF.htm](kingmaker-bestiary-items/8I1oL3XnWQjb1WeF.htm)|Torture Lore|vacía|
|[8Ss1d21OHToUfFNl.htm](kingmaker-bestiary-items/8Ss1d21OHToUfFNl.htm)|Yog-Sothoth Lore|vacía|
|[9h5jUpLhqi52Tedi.htm](kingmaker-bestiary-items/9h5jUpLhqi52Tedi.htm)|Brevoy Lore|vacía|
|[a4xj4dNHkUjHRH4O.htm](kingmaker-bestiary-items/a4xj4dNHkUjHRH4O.htm)|Silver Ring|vacía|
|[aDli69WFeRXUP2U3.htm](kingmaker-bestiary-items/aDli69WFeRXUP2U3.htm)|Ceremonial Robes|vacía|
|[AJYRmsDmV1LVU4Rv.htm](kingmaker-bestiary-items/AJYRmsDmV1LVU4Rv.htm)|Silver Stag Lord Amulet|vacía|
|[ap7UmDIuWShLoSbJ.htm](kingmaker-bestiary-items/ap7UmDIuWShLoSbJ.htm)|Forest Lore|vacía|
|[AYlNJO8ziqGDNG6F.htm](kingmaker-bestiary-items/AYlNJO8ziqGDNG6F.htm)|Forest Lore|vacía|
|[b8MFwyWeJ5rqO3Vk.htm](kingmaker-bestiary-items/b8MFwyWeJ5rqO3Vk.htm)|Stealth|vacía|
|[BbhEZOpQbLUnfahO.htm](kingmaker-bestiary-items/BbhEZOpQbLUnfahO.htm)|Ruby Ring|vacía|
|[BMzs4OQEu3Mt6iZQ.htm](kingmaker-bestiary-items/BMzs4OQEu3Mt6iZQ.htm)|Gambling Lore|vacía|
|[bo6bsp0fKrJ9ddNK.htm](kingmaker-bestiary-items/bo6bsp0fKrJ9ddNK.htm)|Stealth|vacía|
|[BVk9QB1imJ9KD0Vq.htm](kingmaker-bestiary-items/BVk9QB1imJ9KD0Vq.htm)|Silver Stag Lord Amulet|vacía|
|[CBKkrJnJ9DcFA6p0.htm](kingmaker-bestiary-items/CBKkrJnJ9DcFA6p0.htm)|Hunting Lore|vacía|
|[cjGRYigrrey1TIiS.htm](kingmaker-bestiary-items/cjGRYigrrey1TIiS.htm)|Underworld Lore|vacía|
|[cu5gzpbsORryK2Zs.htm](kingmaker-bestiary-items/cu5gzpbsORryK2Zs.htm)|Athletics|vacía|
|[cuoRWPoyJkfPoJ58.htm](kingmaker-bestiary-items/cuoRWPoyJkfPoJ58.htm)|Stealth|vacía|
|[dCRhF1JVyPGvXOfW.htm](kingmaker-bestiary-items/dCRhF1JVyPGvXOfW.htm)|First World Lore|vacía|
|[dGa7fiQkgtKN8PDA.htm](kingmaker-bestiary-items/dGa7fiQkgtKN8PDA.htm)|Crafting|vacía|
|[dhHfUP1VQHRPtvdU.htm](kingmaker-bestiary-items/dhHfUP1VQHRPtvdU.htm)|Stealth|vacía|
|[DiARocJoBMTbmCN9.htm](kingmaker-bestiary-items/DiARocJoBMTbmCN9.htm)|Key to Area E4|vacía|
|[dsVVZHTsFi9iFwgD.htm](kingmaker-bestiary-items/dsVVZHTsFi9iFwgD.htm)|Hill Lore|vacía|
|[Ekik19hqrAXIezul.htm](kingmaker-bestiary-items/Ekik19hqrAXIezul.htm)|Lock of green hair bound with red twine|vacía|
|[fYdweFlxcKABfwpC.htm](kingmaker-bestiary-items/fYdweFlxcKABfwpC.htm)|Rock|vacía|
|[g6XydGLF6RtVUp5L.htm](kingmaker-bestiary-items/g6XydGLF6RtVUp5L.htm)|Stealth|vacía|
|[gS87nm7lmCmY8pzz.htm](kingmaker-bestiary-items/gS87nm7lmCmY8pzz.htm)|First World Lore|vacía|
|[GZ3aIv1wg9M7gGFU.htm](kingmaker-bestiary-items/GZ3aIv1wg9M7gGFU.htm)|Giant Lore|vacía|
|[hqd0OwbFVtZ6VGi3.htm](kingmaker-bestiary-items/hqd0OwbFVtZ6VGi3.htm)|Sapphire Earrings|vacía|
|[HZjTjHMm6PiqprBW.htm](kingmaker-bestiary-items/HZjTjHMm6PiqprBW.htm)|Stealth|vacía|
|[iGGr29rdFCwRoCHG.htm](kingmaker-bestiary-items/iGGr29rdFCwRoCHG.htm)|Turquise Earrings|vacía|
|[IP2DHIvoqbsWaRVU.htm](kingmaker-bestiary-items/IP2DHIvoqbsWaRVU.htm)|Cleaver|vacía|
|[ItuwvVtjFsk5P6Rk.htm](kingmaker-bestiary-items/ItuwvVtjFsk5P6Rk.htm)|Knight and Dragon Toys|vacía|
|[j0CQH72Y8ZIJSLQK.htm](kingmaker-bestiary-items/j0CQH72Y8ZIJSLQK.htm)|Athletics|vacía|
|[jkgZ1UCUT3j7cOf2.htm](kingmaker-bestiary-items/jkgZ1UCUT3j7cOf2.htm)|Restov Lore|vacía|
|[kCwijuNrHVlRHQr0.htm](kingmaker-bestiary-items/kCwijuNrHVlRHQr0.htm)|Flask of Wine|vacía|
|[khNk0fYgnDMF7kKq.htm](kingmaker-bestiary-items/khNk0fYgnDMF7kKq.htm)|First World Lore|vacía|
|[kJR445IOTwrpVfxm.htm](kingmaker-bestiary-items/kJR445IOTwrpVfxm.htm)|Emerald Ring|vacía|
|[KpHIq12RL6K9ukG2.htm](kingmaker-bestiary-items/KpHIq12RL6K9ukG2.htm)|First World Lore|vacía|
|[kTXlEGWWz4WnsrwF.htm](kingmaker-bestiary-items/kTXlEGWWz4WnsrwF.htm)|Warfare Lore|vacía|
|[KV65DhNy4Axid8kc.htm](kingmaker-bestiary-items/KV65DhNy4Axid8kc.htm)|Cleaver|vacía|
|[KwNXLXzskiVB9RLh.htm](kingmaker-bestiary-items/KwNXLXzskiVB9RLh.htm)|Stealth|vacía|
|[kynHGhmwcc8qJ5T8.htm](kingmaker-bestiary-items/kynHGhmwcc8qJ5T8.htm)|Stealth|vacía|
|[lbWYtWXNDRTmYH9O.htm](kingmaker-bestiary-items/lbWYtWXNDRTmYH9O.htm)|Gold Signet Ring|vacía|
|[LUdvij5KTX4oc3yH.htm](kingmaker-bestiary-items/LUdvij5KTX4oc3yH.htm)|Sailing Lore|vacía|
|[M0O2aUtdSXT0HpSQ.htm](kingmaker-bestiary-items/M0O2aUtdSXT0HpSQ.htm)|Chieftain's Necklace|vacía|
|[M5azwdU2fCtU2eR5.htm](kingmaker-bestiary-items/M5azwdU2fCtU2eR5.htm)|Gorum Lore|vacía|
|[MDHk8qTYvCueXiiR.htm](kingmaker-bestiary-items/MDHk8qTYvCueXiiR.htm)|Dwelling Lore|vacía|
|[mKdD7XKIAXJx8KnC.htm](kingmaker-bestiary-items/mKdD7XKIAXJx8KnC.htm)|Athletics|vacía|
|[n8TGGjTIP2jpXLiC.htm](kingmaker-bestiary-items/n8TGGjTIP2jpXLiC.htm)|Monstrous Bloom|vacía|
|[neZCdGIJBKYWKN5Q.htm](kingmaker-bestiary-items/neZCdGIJBKYWKN5Q.htm)|Deception|vacía|
|[nnU6e1a4DLTuRnUV.htm](kingmaker-bestiary-items/nnU6e1a4DLTuRnUV.htm)|Academia Lore|vacía|
|[o82vOevA36JTvcnU.htm](kingmaker-bestiary-items/o82vOevA36JTvcnU.htm)|Forest Lore|vacía|
|[OQv2VZ49tvgCKDhW.htm](kingmaker-bestiary-items/OQv2VZ49tvgCKDhW.htm)|First World Lore|vacía|
|[orJLwvKxUPCxgkWs.htm](kingmaker-bestiary-items/orJLwvKxUPCxgkWs.htm)|Monarch's Horn|vacía|
|[pfic0QiF1jUkDVwR.htm](kingmaker-bestiary-items/pfic0QiF1jUkDVwR.htm)|River Lore|vacía|
|[PLl0lrfdaaqgezrn.htm](kingmaker-bestiary-items/PLl0lrfdaaqgezrn.htm)|Clothing and Crown made of thorny vines and weeds|vacía|
|[PmFUHtH32XT97IlJ.htm](kingmaker-bestiary-items/PmFUHtH32XT97IlJ.htm)|Warfare Lore|vacía|
|[pmVLSDQuvnEqYH5y.htm](kingmaker-bestiary-items/pmVLSDQuvnEqYH5y.htm)|Fey Lore|vacía|
|[pu7iTks4TUgGpglG.htm](kingmaker-bestiary-items/pu7iTks4TUgGpglG.htm)|First World Lore|vacía|
|[Q2aaq7yspaPD9kAk.htm](kingmaker-bestiary-items/Q2aaq7yspaPD9kAk.htm)|Nobility Lore|vacía|
|[qesdwQLA0OfebLki.htm](kingmaker-bestiary-items/qesdwQLA0OfebLki.htm)|Intimidation|vacía|
|[qvps9GmZSlGuhdrK.htm](kingmaker-bestiary-items/qvps9GmZSlGuhdrK.htm)|Royal Outfit|vacía|
|[QW0dGg59W0r9tJtU.htm](kingmaker-bestiary-items/QW0dGg59W0r9tJtU.htm)|Stealth|vacía|
|[r0Jfu2EKhpHiK8Io.htm](kingmaker-bestiary-items/r0Jfu2EKhpHiK8Io.htm)|Athletics|vacía|
|[rf2VoAlQi6dlp1Yq.htm](kingmaker-bestiary-items/rf2VoAlQi6dlp1Yq.htm)|Silver Stag Lord Amulet|vacía|
|[RNxWSJIolrbdRbNy.htm](kingmaker-bestiary-items/RNxWSJIolrbdRbNy.htm)|Bardic Lore|vacía|
|[rs8cxUHAMnuhP7nv.htm](kingmaker-bestiary-items/rs8cxUHAMnuhP7nv.htm)|Stealth|vacía|
|[sTjHob8UlAoUPu20.htm](kingmaker-bestiary-items/sTjHob8UlAoUPu20.htm)|Yog-Sothoth Lore|vacía|
|[TSDKiQYnEhZxGO3K.htm](kingmaker-bestiary-items/TSDKiQYnEhZxGO3K.htm)|Warfare Lore|vacía|
|[tsZqAzdWTSJww5o9.htm](kingmaker-bestiary-items/tsZqAzdWTSJww5o9.htm)|First World Lore|vacía|
|[TWA2MXZuX91plGrL.htm](kingmaker-bestiary-items/TWA2MXZuX91plGrL.htm)|Art Lore|vacía|
|[U69KdW5yZ21jJT4a.htm](kingmaker-bestiary-items/U69KdW5yZ21jJT4a.htm)|Yog-Sothoth Lore|vacía|
|[UGAcwTdmLcqn2A5C.htm](kingmaker-bestiary-items/UGAcwTdmLcqn2A5C.htm)|River Lore|vacía|
|[UiU7w5ovykgz63Vf.htm](kingmaker-bestiary-items/UiU7w5ovykgz63Vf.htm)|First World Lore|vacía|
|[UZ4VlSGBzBXCEdfz.htm](kingmaker-bestiary-items/UZ4VlSGBzBXCEdfz.htm)|Religious Symbol of Lamashtu (Gold)|vacía|
|[VaiIAm2nnOW5bySh.htm](kingmaker-bestiary-items/VaiIAm2nnOW5bySh.htm)|Silver Stag Lord Amulet|vacía|
|[viyrIe0gXqX4x1ZG.htm](kingmaker-bestiary-items/viyrIe0gXqX4x1ZG.htm)|Mithral Necklace|vacía|
|[VVezMNwhjbCrp0yX.htm](kingmaker-bestiary-items/VVezMNwhjbCrp0yX.htm)|Warfare Lore|vacía|
|[WD2RKxV7k6NLl0Bn.htm](kingmaker-bestiary-items/WD2RKxV7k6NLl0Bn.htm)|Ragged Robes|vacía|
|[wFE3Lth938xGYdIL.htm](kingmaker-bestiary-items/wFE3Lth938xGYdIL.htm)|Warfare Lore|vacía|
|[wll4JHYsNNNsx4lF.htm](kingmaker-bestiary-items/wll4JHYsNNNsx4lF.htm)|Academia Lore|vacía|
|[X7v48sCoD3y3bAyO.htm](kingmaker-bestiary-items/X7v48sCoD3y3bAyO.htm)|Cultist Robes|vacía|
|[xAxV9yRmp780wKql.htm](kingmaker-bestiary-items/xAxV9yRmp780wKql.htm)|Stealth|vacía|
|[xj83ymsPlY7u87j1.htm](kingmaker-bestiary-items/xj83ymsPlY7u87j1.htm)|Athletics|vacía|
|[XlxlWOl8QpPb2Inn.htm](kingmaker-bestiary-items/XlxlWOl8QpPb2Inn.htm)|Sack of Flytrap Pods|vacía|
|[XwujWRcPvBs2BAHX.htm](kingmaker-bestiary-items/XwujWRcPvBs2BAHX.htm)|Sack for Holding Rocks|vacía|
|[xYt0Yl9LAnVB1BZ6.htm](kingmaker-bestiary-items/xYt0Yl9LAnVB1BZ6.htm)|Warfare Lore|vacía|
|[y57fpMJDVfTQ3moG.htm](kingmaker-bestiary-items/y57fpMJDVfTQ3moG.htm)|Brevoy Lore|vacía|
|[YJbZ9CTWY2HNVBXB.htm](kingmaker-bestiary-items/YJbZ9CTWY2HNVBXB.htm)|Silver Stag Lord Amulet|vacía|
|[ypEVZiKiaV4PXVPe.htm](kingmaker-bestiary-items/ypEVZiKiaV4PXVPe.htm)|Yog-Sothoth Lore|vacía|
|[ysZ3MQQymY3E22BQ.htm](kingmaker-bestiary-items/ysZ3MQQymY3E22BQ.htm)|Survival|vacía|
|[Yutc2Syx2q0EKb1P.htm](kingmaker-bestiary-items/Yutc2Syx2q0EKb1P.htm)|Heraldry Lore|vacía|
|[YXGsH5DgEDFiRIu7.htm](kingmaker-bestiary-items/YXGsH5DgEDFiRIu7.htm)|Lumber Lore|vacía|
|[YyYWWpan1IChNfUA.htm](kingmaker-bestiary-items/YyYWWpan1IChNfUA.htm)|Jade and Pearl Necklace|vacía|
|[z8haj2djGN404zAS.htm](kingmaker-bestiary-items/z8haj2djGN404zAS.htm)|Diplomacy|vacía|
|[zWFmuptIJBa1t8FT.htm](kingmaker-bestiary-items/zWFmuptIJBa1t8FT.htm)|Warfare Lore|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
