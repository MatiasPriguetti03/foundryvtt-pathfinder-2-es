# Estado de la traducción (boons-and-curses)

 * **auto-trad**: 238
 * **modificada**: 2


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[02FtcdSA8tjFNmvD.htm](boons-and-curses/02FtcdSA8tjFNmvD.htm)|Hei Feng - Major Boon|auto-trad|
|[0bM6m3Gx8Th3NxXp.htm](boons-and-curses/0bM6m3Gx8Th3NxXp.htm)|Nocticula - Minor Boon|auto-trad|
|[0hE3yCBhmReWtZGA.htm](boons-and-curses/0hE3yCBhmReWtZGA.htm)|Ghlaunder - Minor Boon|auto-trad|
|[0JDLq5rMOjUskRTG.htm](boons-and-curses/0JDLq5rMOjUskRTG.htm)|Besmara - Moderate Curse|auto-trad|
|[0kOGxG08y4bGgLto.htm](boons-and-curses/0kOGxG08y4bGgLto.htm)|Groetus - Major Curse|auto-trad|
|[0PMqydlsIjj8GNnl.htm](boons-and-curses/0PMqydlsIjj8GNnl.htm)|Norgorber - Moderate Curse|auto-trad|
|[0vmwZrIhm5rWHkYh.htm](boons-and-curses/0vmwZrIhm5rWHkYh.htm)|Pharasma - Minor Boon|auto-trad|
|[10F2EKASFp9bjsjF.htm](boons-and-curses/10F2EKASFp9bjsjF.htm)|Shizuru - Moderate Boon|auto-trad|
|[1abspMhs13VKLklY.htm](boons-and-curses/1abspMhs13VKLklY.htm)|Arazni - Moderate Curse|auto-trad|
|[1hdWNyedYvuZgtPr.htm](boons-and-curses/1hdWNyedYvuZgtPr.htm)|Pharasma - Moderate Curse|auto-trad|
|[1J5gmMhFP0Ul9o64.htm](boons-and-curses/1J5gmMhFP0Ul9o64.htm)|Besmara - Major Curse|auto-trad|
|[1nALC8yWtjBDEaOC.htm](boons-and-curses/1nALC8yWtjBDEaOC.htm)|Rovagug - Minor Curse|auto-trad|
|[1QtjogbV1MsJp7x5.htm](boons-and-curses/1QtjogbV1MsJp7x5.htm)|Achaekek - Minor Curse|auto-trad|
|[1WPIDGaeRd76EXm1.htm](boons-and-curses/1WPIDGaeRd76EXm1.htm)|Chaldira - Major Boon|auto-trad|
|[2EKgNifq3ozzKYfI.htm](boons-and-curses/2EKgNifq3ozzKYfI.htm)|Norgorber - Minor Boon|auto-trad|
|[2i4AQ2tHph54SzRD.htm](boons-and-curses/2i4AQ2tHph54SzRD.htm)|Casandalee - Major Boon|auto-trad|
|[30Xdyvplx7MfX1nA.htm](boons-and-curses/30Xdyvplx7MfX1nA.htm)|Torag - Moderate Boon|auto-trad|
|[3hZjK8dKmjWUyotV.htm](boons-and-curses/3hZjK8dKmjWUyotV.htm)|Nivi Rhombodazzle - Minor Curse|auto-trad|
|[3u3vav6qfAW6hPKE.htm](boons-and-curses/3u3vav6qfAW6hPKE.htm)|Erastil - Moderate Curse|auto-trad|
|[496UIvkjyrae8xzb.htm](boons-and-curses/496UIvkjyrae8xzb.htm)|Casandalee - Moderate Curse|auto-trad|
|[4FkVB4uQK4eHZJ6Z.htm](boons-and-curses/4FkVB4uQK4eHZJ6Z.htm)|Rovagug - Minor Boon|auto-trad|
|[4gVIjPeKV6ss1atA.htm](boons-and-curses/4gVIjPeKV6ss1atA.htm)|Brigh - Major Curse|auto-trad|
|[4m2dQ2fvVWxtokVe.htm](boons-and-curses/4m2dQ2fvVWxtokVe.htm)|Desna - Moderate Boon|auto-trad|
|[4sZ9tpmPj4LIgPvU.htm](boons-and-curses/4sZ9tpmPj4LIgPvU.htm)|Nethys - Major Curse|auto-trad|
|[5x0wEbgxYmXonBuN.htm](boons-and-curses/5x0wEbgxYmXonBuN.htm)|Lamashtu - Minor Curse|auto-trad|
|[6CLe2ZOkLSi27S2Z.htm](boons-and-curses/6CLe2ZOkLSi27S2Z.htm)|Sivanah - Moderate Curse|auto-trad|
|[6Cpm04jBSzSwe2oC.htm](boons-and-curses/6Cpm04jBSzSwe2oC.htm)|Nocticula - Moderate Curse|auto-trad|
|[6f8zTNIs5XXzkhkR.htm](boons-and-curses/6f8zTNIs5XXzkhkR.htm)|Lamashtu - Minor Boon|auto-trad|
|[6HcZezAVWArSonu0.htm](boons-and-curses/6HcZezAVWArSonu0.htm)|Chaldira - Moderate Boon|auto-trad|
|[6Iz9b01O5t31ZioP.htm](boons-and-curses/6Iz9b01O5t31ZioP.htm)|Tsukiyo - Minor Curse|auto-trad|
|[7HOxIB1abpVRUjY9.htm](boons-and-curses/7HOxIB1abpVRUjY9.htm)|Grandmother Spider - Minor Boon|auto-trad|
|[7Ky13a2fl5KUuaMd.htm](boons-and-curses/7Ky13a2fl5KUuaMd.htm)|Milani - Minor Boon|auto-trad|
|[845vvISfgkk6sei0.htm](boons-and-curses/845vvISfgkk6sei0.htm)|Torag - Minor Curse|auto-trad|
|[8Be5VeyjWRZUdxde.htm](boons-and-curses/8Be5VeyjWRZUdxde.htm)|Nivi Rhombodazzle - Minor Boon|auto-trad|
|[8G0USrxM7d4B4EVI.htm](boons-and-curses/8G0USrxM7d4B4EVI.htm)|Gorum - Major Boon|auto-trad|
|[8GElMYCPjhK5uHAj.htm](boons-and-curses/8GElMYCPjhK5uHAj.htm)|Nivi Rhombodazzle - Moderate Curse|auto-trad|
|[8GyJK2tNVMIuV8Wa.htm](boons-and-curses/8GyJK2tNVMIuV8Wa.htm)|Chaldira - Moderate Curse|auto-trad|
|[8jHjxcB33N4BULE1.htm](boons-and-curses/8jHjxcB33N4BULE1.htm)|Casandalee - Minor Curse|auto-trad|
|[8x308UNvYYDuIHXH.htm](boons-and-curses/8x308UNvYYDuIHXH.htm)|Nocticula - Moderate Boon|auto-trad|
|[8ZNtiMxoJIP1DJ9Q.htm](boons-and-curses/8ZNtiMxoJIP1DJ9Q.htm)|Nethys - Major Boon|auto-trad|
|[9EMaYf6odFEyjdSr.htm](boons-and-curses/9EMaYf6odFEyjdSr.htm)|Urgathoa - Moderate Curse|auto-trad|
|[9jFdhTtL8zElWdRC.htm](boons-and-curses/9jFdhTtL8zElWdRC.htm)|Groetus - Moderate Boon|auto-trad|
|[9jxwttnEfxrOaCsY.htm](boons-and-curses/9jxwttnEfxrOaCsY.htm)|Gruhastha - Major Boon|auto-trad|
|[9nCMvbz3e71AgsoW.htm](boons-and-curses/9nCMvbz3e71AgsoW.htm)|Besmara - Minor Boon|auto-trad|
|[9neKaRaRi9ekttts.htm](boons-and-curses/9neKaRaRi9ekttts.htm)|Chaldira - Major Curse|auto-trad|
|[9yABJNudAI1IvifR.htm](boons-and-curses/9yABJNudAI1IvifR.htm)|Kurgess - Minor Boon|auto-trad|
|[a1OZtMQSzjYOm0P3.htm](boons-and-curses/a1OZtMQSzjYOm0P3.htm)|Rovagug - Moderate Boon|auto-trad|
|[A8HRlQB7reEHp50k.htm](boons-and-curses/A8HRlQB7reEHp50k.htm)|Nivi Rhombodazzle - Moderate Boon|auto-trad|
|[AdNAeWhnq5Is3AZb.htm](boons-and-curses/AdNAeWhnq5Is3AZb.htm)|Torag - Major Boon|auto-trad|
|[AfuH02H6ib4KYS6C.htm](boons-and-curses/AfuH02H6ib4KYS6C.htm)|Torag - Moderate Curse|auto-trad|
|[AJj2o3uWKH3ARyFr.htm](boons-and-curses/AJj2o3uWKH3ARyFr.htm)|Casandalee - Major Curse|auto-trad|
|[AOBhF0grlwKJSuxi.htm](boons-and-curses/AOBhF0grlwKJSuxi.htm)|Hei Feng - Minor Boon|auto-trad|
|[AodAErjWnvGZaL4M.htm](boons-and-curses/AodAErjWnvGZaL4M.htm)|Sivanah - Moderate Boon|auto-trad|
|[AohWtOzgk2Qr9ADl.htm](boons-and-curses/AohWtOzgk2Qr9ADl.htm)|Gozreh - Moderate Curse|auto-trad|
|[aOVXhHVkLw9dLCdR.htm](boons-and-curses/aOVXhHVkLw9dLCdR.htm)|Zon-Kuthon - Moderate Curse|auto-trad|
|[auoQ66gY4r7Tk3lc.htm](boons-and-curses/auoQ66gY4r7Tk3lc.htm)|Alseta - Major Boon|auto-trad|
|[aV1I9VmMQBaFU9M9.htm](boons-and-curses/aV1I9VmMQBaFU9M9.htm)|Asmodeus - Moderate Boon|auto-trad|
|[AvjRCVzLmxapFOLV.htm](boons-and-curses/AvjRCVzLmxapFOLV.htm)|Gozreh - Moderate Boon|auto-trad|
|[AySifuRSqWzDkCDt.htm](boons-and-curses/AySifuRSqWzDkCDt.htm)|Sivanah - Major Curse|auto-trad|
|[B69aBTRn2BwSo23z.htm](boons-and-curses/B69aBTRn2BwSo23z.htm)|Nocticula - Minor Curse|auto-trad|
|[BdV4uubwrn0KgD8H.htm](boons-and-curses/BdV4uubwrn0KgD8H.htm)|Alseta - Moderate Curse|auto-trad|
|[bvBzvLDKM5sAJm9s.htm](boons-and-curses/bvBzvLDKM5sAJm9s.htm)|Achaekek - Minor Boon|auto-trad|
|[c2wwILmpgyXIjlfe.htm](boons-and-curses/c2wwILmpgyXIjlfe.htm)|Torag - Major Curse|auto-trad|
|[C6Ercv1ugcTsbVA9.htm](boons-and-curses/C6Ercv1ugcTsbVA9.htm)|Erastil - Major Curse|auto-trad|
|[Ca8ZQssvIgH6WEwZ.htm](boons-and-curses/Ca8ZQssvIgH6WEwZ.htm)|Casandalee - Minor Boon|auto-trad|
|[CboMubDaWRkG94Ff.htm](boons-and-curses/CboMubDaWRkG94Ff.htm)|Hei Feng - Minor Curse|auto-trad|
|[cFz09CWKzff2cC73.htm](boons-and-curses/cFz09CWKzff2cC73.htm)|Rovagug - Major Boon|auto-trad|
|[CjRUOscsOwCmai8D.htm](boons-and-curses/CjRUOscsOwCmai8D.htm)|Desna - Moderate Curse|auto-trad|
|[CL8lLQJWd4N89QIm.htm](boons-and-curses/CL8lLQJWd4N89QIm.htm)|Erastil - Moderate Boon|auto-trad|
|[CmHQGVyNZ7aOmMcd.htm](boons-and-curses/CmHQGVyNZ7aOmMcd.htm)|Nethys - Moderate Curse|auto-trad|
|[COLF56taxqleYkcZ.htm](boons-and-curses/COLF56taxqleYkcZ.htm)|Groetus - Minor Boon|auto-trad|
|[ctcZm0WGiBKVWVYd.htm](boons-and-curses/ctcZm0WGiBKVWVYd.htm)|Alseta - Minor Boon|auto-trad|
|[d198p8UBPK3VcKi9.htm](boons-and-curses/d198p8UBPK3VcKi9.htm)|Rovagug - Moderate Curse|auto-trad|
|[d2mkwn858zIqhLS7.htm](boons-and-curses/d2mkwn858zIqhLS7.htm)|Ghlaunder - Major Boon|auto-trad|
|[d84VbnhyQ77abLKF.htm](boons-and-curses/d84VbnhyQ77abLKF.htm)|Tsukiyo - Minor Boon|auto-trad|
|[dFWAMWqnioz6RZxc.htm](boons-and-curses/dFWAMWqnioz6RZxc.htm)|Grandmother Spider - Major Boon|auto-trad|
|[DfXlr5qqbcjJN3gh.htm](boons-and-curses/DfXlr5qqbcjJN3gh.htm)|Abadar - Moderate Boon|auto-trad|
|[dHcGihuimtCf1NOs.htm](boons-and-curses/dHcGihuimtCf1NOs.htm)|Iomedae - Major Boon|auto-trad|
|[DjVTszddTHSo9fkZ.htm](boons-and-curses/DjVTszddTHSo9fkZ.htm)|Nivi Rhombodazzle - Major Curse|auto-trad|
|[DJxDsRXhqXvABYzK.htm](boons-and-curses/DJxDsRXhqXvABYzK.htm)|Sivanah - Minor Boon|auto-trad|
|[dL6r0VfFIWlAazDW.htm](boons-and-curses/dL6r0VfFIWlAazDW.htm)|Sarenrae - Major Boon|auto-trad|
|[dLudHCFcj4p7KG3j.htm](boons-and-curses/dLudHCFcj4p7KG3j.htm)|Asmodeus - Minor Curse|auto-trad|
|[DnlAmlrrKv3jwaOq.htm](boons-and-curses/DnlAmlrrKv3jwaOq.htm)|Pharasma - Major Curse|auto-trad|
|[dRk4kWmtau9PLQAk.htm](boons-and-curses/dRk4kWmtau9PLQAk.htm)|Chaldira - Minor Curse|auto-trad|
|[DSfHCm8PDL5hLzZ0.htm](boons-and-curses/DSfHCm8PDL5hLzZ0.htm)|Irori - Minor Curse|auto-trad|
|[dTJP2Hg4ZJu4Ck2y.htm](boons-and-curses/dTJP2Hg4ZJu4Ck2y.htm)|Iomedae - Minor Boon|auto-trad|
|[DuGA57MDiwEKp7Y1.htm](boons-and-curses/DuGA57MDiwEKp7Y1.htm)|Sarenrae - Major Curse|auto-trad|
|[DYEN06j9VVTOXqFK.htm](boons-and-curses/DYEN06j9VVTOXqFK.htm)|Besmara - Minor Curse|auto-trad|
|[dZU3HdI2oO8LFjGq.htm](boons-and-curses/dZU3HdI2oO8LFjGq.htm)|Urgathoa - Major Boon|auto-trad|
|[e7Y4jKHQ3ptbSuxc.htm](boons-and-curses/e7Y4jKHQ3ptbSuxc.htm)|Torag - Minor Boon|auto-trad|
|[e9bdt8UH7ZvpRrLz.htm](boons-and-curses/e9bdt8UH7ZvpRrLz.htm)|Sarenrae - Minor Boon|auto-trad|
|[eFwCopyvVam6GiCT.htm](boons-and-curses/eFwCopyvVam6GiCT.htm)|Achaekek - Major Boon|auto-trad|
|[eibN2Uf0dsHCU5rE.htm](boons-and-curses/eibN2Uf0dsHCU5rE.htm)|Zon-Kuthon - Major Boon|auto-trad|
|[eVRjDKt0N0qSmpCn.htm](boons-and-curses/eVRjDKt0N0qSmpCn.htm)|Urgathoa - Major Curse|auto-trad|
|[FeCcooyIb1JDQhd7.htm](boons-and-curses/FeCcooyIb1JDQhd7.htm)|Calistria - Major Boon|auto-trad|
|[ffflmrfFtpFrnVqC.htm](boons-and-curses/ffflmrfFtpFrnVqC.htm)|Kurgess - Moderate Curse|auto-trad|
|[fLBNXULLPalAKlYe.htm](boons-and-curses/fLBNXULLPalAKlYe.htm)|Cayden Cailean - Minor Curse|auto-trad|
|[FRmpIgwDHeewpFkL.htm](boons-and-curses/FRmpIgwDHeewpFkL.htm)|Tsukiyo - Major Curse|auto-trad|
|[fsKVskUhWwMgvUaT.htm](boons-and-curses/fsKVskUhWwMgvUaT.htm)|Cayden Cailean - Moderate Boon|auto-trad|
|[fV0t8xagcbqwvTpQ.htm](boons-and-curses/fV0t8xagcbqwvTpQ.htm)|Asmodeus - Minor Boon|auto-trad|
|[fV0Xa1Bd3BoWACgT.htm](boons-and-curses/fV0Xa1Bd3BoWACgT.htm)|Irori - Minor Boon|auto-trad|
|[fV940VM5RCsNwUvA.htm](boons-and-curses/fV940VM5RCsNwUvA.htm)|Calistria - Major Curse|auto-trad|
|[fXEeTSVINdWaKHhw.htm](boons-and-curses/fXEeTSVINdWaKHhw.htm)|Abadar - Minor Boon|auto-trad|
|[G4Y4qWEbFJXLdI2G.htm](boons-and-curses/G4Y4qWEbFJXLdI2G.htm)|Alseta - Minor Curse|auto-trad|
|[gDBa72Y2jokr8Zzg.htm](boons-and-curses/gDBa72Y2jokr8Zzg.htm)|Lamashtu - Moderate Curse|auto-trad|
|[GJdHcY56q2c3kSiA.htm](boons-and-curses/GJdHcY56q2c3kSiA.htm)|Achaekek - Major Curse|auto-trad|
|[GKyH7IQHrmmCnuop.htm](boons-and-curses/GKyH7IQHrmmCnuop.htm)|Shizuru - Major Boon|auto-trad|
|[Gv05Y1IXI4RWK6YO.htm](boons-and-curses/Gv05Y1IXI4RWK6YO.htm)|Milani - Major Curse|auto-trad|
|[gy4ThzGHW3plFZ2a.htm](boons-and-curses/gy4ThzGHW3plFZ2a.htm)|Brigh - Minor Boon|auto-trad|
|[HdPVL76QiDhsLCA9.htm](boons-and-curses/HdPVL76QiDhsLCA9.htm)|Nethys - Moderate Boon|auto-trad|
|[hoXFKbMHeUabjW3s.htm](boons-and-curses/hoXFKbMHeUabjW3s.htm)|Sivanah - Minor Curse|auto-trad|
|[hrTl9kfSNrOQeNze.htm](boons-and-curses/hrTl9kfSNrOQeNze.htm)|Erastil - Minor Boon|auto-trad|
|[HrV31rAkNjV4KfCU.htm](boons-and-curses/HrV31rAkNjV4KfCU.htm)|Gorum - Minor Boon|auto-trad|
|[hUSknn3LYmT7eKzT.htm](boons-and-curses/hUSknn3LYmT7eKzT.htm)|Norgorber - Moderate Boon|auto-trad|
|[Hvz04dGqmV8x25bw.htm](boons-and-curses/Hvz04dGqmV8x25bw.htm)|Tsukiyo - Moderate Boon|auto-trad|
|[HXRLOkyO8F374X8R.htm](boons-and-curses/HXRLOkyO8F374X8R.htm)|Arazni - Moderate Boon|auto-trad|
|[ILtU0TAUZcOcwkkG.htm](boons-and-curses/ILtU0TAUZcOcwkkG.htm)|Shelyn - Major Curse|auto-trad|
|[innbfBPD1VsBIVDn.htm](boons-and-curses/innbfBPD1VsBIVDn.htm)|Ghlaunder - Moderate Boon|auto-trad|
|[IOaepssdwTanFooc.htm](boons-and-curses/IOaepssdwTanFooc.htm)|Iomedae - Minor Curse|auto-trad|
|[iT6MnLgzuVetCzm7.htm](boons-and-curses/iT6MnLgzuVetCzm7.htm)|Brigh - Moderate Curse|auto-trad|
|[J0aztZRSjNjJzwGy.htm](boons-and-curses/J0aztZRSjNjJzwGy.htm)|Arazni - Major Curse|auto-trad|
|[jDVTrMaswJRJRTuf.htm](boons-and-curses/jDVTrMaswJRJRTuf.htm)|Besmara - Moderate Boon|auto-trad|
|[jfWeNqYj3rn9EysM.htm](boons-and-curses/jfWeNqYj3rn9EysM.htm)|Urgathoa - Minor Boon|auto-trad|
|[jhixjUJZlCetNRjH.htm](boons-and-curses/jhixjUJZlCetNRjH.htm)|Shelyn - Minor Curse|auto-trad|
|[jUitGTczLgoUK0rv.htm](boons-and-curses/jUitGTczLgoUK0rv.htm)|Ghlaunder - Major Curse|auto-trad|
|[jVeuBZsa4iFg0wC6.htm](boons-and-curses/jVeuBZsa4iFg0wC6.htm)|Nethys - Minor Boon|auto-trad|
|[kCCrjskDIbG0e74O.htm](boons-and-curses/kCCrjskDIbG0e74O.htm)|Irori - Major Boon|auto-trad|
|[kEAw5PMLdX2gHlwR.htm](boons-and-curses/kEAw5PMLdX2gHlwR.htm)|Gozreh - Major Curse|auto-trad|
|[KjPTFfxMAqCngQFB.htm](boons-and-curses/KjPTFfxMAqCngQFB.htm)|Nethys - Minor Curse|auto-trad|
|[Ks1HaYEuSRybaq8U.htm](boons-and-curses/Ks1HaYEuSRybaq8U.htm)|Calistria - Moderate Boon|auto-trad|
|[kstqMWge32n0Lfdz.htm](boons-and-curses/kstqMWge32n0Lfdz.htm)|Gorum - Moderate Boon|auto-trad|
|[KuNcNP42LxehKnaV.htm](boons-and-curses/KuNcNP42LxehKnaV.htm)|Shizuru - Minor Boon|auto-trad|
|[l7n8n4fAoRLIn95W.htm](boons-and-curses/l7n8n4fAoRLIn95W.htm)|Shelyn - Major Boon|auto-trad|
|[La6t4fVId26PkmYk.htm](boons-and-curses/La6t4fVId26PkmYk.htm)|Cayden Cailean - Minor Boon|auto-trad|
|[LfiMah3iJnnjPi29.htm](boons-and-curses/LfiMah3iJnnjPi29.htm)|Shizuru - Minor Curse|auto-trad|
|[lI7bDExxA2zQq1sP.htm](boons-and-curses/lI7bDExxA2zQq1sP.htm)|Zon-Kuthon - Minor Curse|auto-trad|
|[LMK1RXvxxD0JP0L4.htm](boons-and-curses/LMK1RXvxxD0JP0L4.htm)|Arazni - Minor Boon|auto-trad|
|[lOIu9jYDAR2rDe4p.htm](boons-and-curses/lOIu9jYDAR2rDe4p.htm)|Norgorber - Major Boon|auto-trad|
|[LOJVm2Tt4K2XdVLs.htm](boons-and-curses/LOJVm2Tt4K2XdVLs.htm)|Milani - Major Boon|auto-trad|
|[LOlv11WEGUQWYKna.htm](boons-and-curses/LOlv11WEGUQWYKna.htm)|Desna - Minor Boon|auto-trad|
|[lR9KS3pQ4gGKNspP.htm](boons-and-curses/lR9KS3pQ4gGKNspP.htm)|Gorum - Moderate Curse|auto-trad|
|[LwXdmLWOBUIV1By0.htm](boons-and-curses/LwXdmLWOBUIV1By0.htm)|Kazutal - Moderate Boon|auto-trad|
|[m33aOQij1BDcArN9.htm](boons-and-curses/m33aOQij1BDcArN9.htm)|Abadar - Major Boon|auto-trad|
|[M78A7uQaa4Ig8pGU.htm](boons-and-curses/M78A7uQaa4Ig8pGU.htm)|Gorum - Major Curse|auto-trad|
|[MaaMu5jrfCP8w8SW.htm](boons-and-curses/MaaMu5jrfCP8w8SW.htm)|Kazutal - Moderate Curse|auto-trad|
|[McPpckNcC9hoc39a.htm](boons-and-curses/McPpckNcC9hoc39a.htm)|Hei Feng - Moderate Boon|auto-trad|
|[mcVlGufQ8rQRe61Y.htm](boons-and-curses/mcVlGufQ8rQRe61Y.htm)|Desna - Major Boon|auto-trad|
|[MLmgyIZRJX1hfaPR.htm](boons-and-curses/MLmgyIZRJX1hfaPR.htm)|Nivi Rhombodazzle - Major Boon|auto-trad|
|[MZWAF1mNNUJOzeWW.htm](boons-and-curses/MZWAF1mNNUJOzeWW.htm)|Hei Feng - Moderate Curse|auto-trad|
|[N29LCwYfRWHtEqrS.htm](boons-and-curses/N29LCwYfRWHtEqrS.htm)|Urgathoa - Moderate Boon|auto-trad|
|[n5p8QfZdptRZp0ii.htm](boons-and-curses/n5p8QfZdptRZp0ii.htm)|Abadar - Moderate Curse|auto-trad|
|[NeAA1BHGGkRAmBDe.htm](boons-and-curses/NeAA1BHGGkRAmBDe.htm)|Abadar - Minor Curse|auto-trad|
|[nFrCY6tT2B8uxaO3.htm](boons-and-curses/nFrCY6tT2B8uxaO3.htm)|Erastil - Major Boon|auto-trad|
|[NgwW7Wt2gO9SEwPX.htm](boons-and-curses/NgwW7Wt2gO9SEwPX.htm)|Gorum - Minor Curse|auto-trad|
|[nqOoUH4r8NEi1i2g.htm](boons-and-curses/nqOoUH4r8NEi1i2g.htm)|Calistria - Moderate Curse|auto-trad|
|[NV1w0rwc7Tkhhkac.htm](boons-and-curses/NV1w0rwc7Tkhhkac.htm)|Grandmother Spider - Major Curse|auto-trad|
|[nz77SkH0xWbcw5SY.htm](boons-and-curses/nz77SkH0xWbcw5SY.htm)|Gruhastha - Minor Curse|auto-trad|
|[NZFTnX1xzI40q8Qr.htm](boons-and-curses/NZFTnX1xzI40q8Qr.htm)|Chaldira - Minor Boon|auto-trad|
|[OaUt41v2OrQkHpM4.htm](boons-and-curses/OaUt41v2OrQkHpM4.htm)|Abadar - Major Curse|auto-trad|
|[oqM1fFyHNigCNBsf.htm](boons-and-curses/oqM1fFyHNigCNBsf.htm)|Milani - Moderate Curse|auto-trad|
|[OV7cKDx5b50sz875.htm](boons-and-curses/OV7cKDx5b50sz875.htm)|Milani - Moderate Boon|auto-trad|
|[P9ujY9f0o779TeEn.htm](boons-and-curses/P9ujY9f0o779TeEn.htm)|Tsukiyo - Moderate Curse|auto-trad|
|[ParIWsb6B4fdhrHF.htm](boons-and-curses/ParIWsb6B4fdhrHF.htm)|Pharasma - Minor Curse|auto-trad|
|[pEtqyTx6Oa0OqNKl.htm](boons-and-curses/pEtqyTx6Oa0OqNKl.htm)|Brigh - Moderate Boon|auto-trad|
|[pf2pAvNcz1Qcbowg.htm](boons-and-curses/pf2pAvNcz1Qcbowg.htm)|Alseta - Moderate Boon|auto-trad|
|[PoHos7qriDzQN7Gw.htm](boons-and-curses/PoHos7qriDzQN7Gw.htm)|Rovagug - Major Curse|auto-trad|
|[pt4dHeoDwfj8adUE.htm](boons-and-curses/pt4dHeoDwfj8adUE.htm)|Ghlaunder - Minor Curse|auto-trad|
|[pUtQOAKeXslaD5g3.htm](boons-and-curses/pUtQOAKeXslaD5g3.htm)|Arazni - Minor Curse|auto-trad|
|[pwmFrNkLscXrDSPN.htm](boons-and-curses/pwmFrNkLscXrDSPN.htm)|Erastil - Minor Curse|auto-trad|
|[py9v5HfCIzCoyC0C.htm](boons-and-curses/py9v5HfCIzCoyC0C.htm)|Lamashtu - Major Curse|auto-trad|
|[qbo52AnXV8KDXLnL.htm](boons-and-curses/qbo52AnXV8KDXLnL.htm)|Grandmother Spider - Minor Curse|auto-trad|
|[QGUFWuz9uF49EIhy.htm](boons-and-curses/QGUFWuz9uF49EIhy.htm)|Cayden Cailean - Major Curse|auto-trad|
|[qhVfbZCxZJS8NmB3.htm](boons-and-curses/qhVfbZCxZJS8NmB3.htm)|Irori - Moderate Curse|auto-trad|
|[qTmjEC528YnvPgXE.htm](boons-and-curses/qTmjEC528YnvPgXE.htm)|Iomedae - Moderate Boon|auto-trad|
|[QuNXNPHvh4DBmyZH.htm](boons-and-curses/QuNXNPHvh4DBmyZH.htm)|Kurgess - Major Curse|auto-trad|
|[QVEkEdxCnoGxMo9l.htm](boons-and-curses/QVEkEdxCnoGxMo9l.htm)|Zon-Kuthon - Moderate Boon|auto-trad|
|[r4VjA1Wje3mtK2M4.htm](boons-and-curses/r4VjA1Wje3mtK2M4.htm)|Nocticula - Major Boon|auto-trad|
|[r9hOxyTwN0DsrTPU.htm](boons-and-curses/r9hOxyTwN0DsrTPU.htm)|Arazni - Major Boon|auto-trad|
|[rA7ZM6WGnJNrWTmo.htm](boons-and-curses/rA7ZM6WGnJNrWTmo.htm)|Iomedae - Moderate Curse|auto-trad|
|[RLdclcC8zoNAVony.htm](boons-and-curses/RLdclcC8zoNAVony.htm)|Groetus - Minor Curse|auto-trad|
|[rN9uF0XUMm4LPFOk.htm](boons-and-curses/rN9uF0XUMm4LPFOk.htm)|Grandmother Spider - Moderate Boon|auto-trad|
|[S1qGyGG25YImRhmS.htm](boons-and-curses/S1qGyGG25YImRhmS.htm)|Sarenrae - Moderate Boon|auto-trad|
|[SaiJX9KBN3lC0RUy.htm](boons-and-curses/SaiJX9KBN3lC0RUy.htm)|Zon-Kuthon - Major Curse|auto-trad|
|[sL6WZ4EIHcIuEwVW.htm](boons-and-curses/sL6WZ4EIHcIuEwVW.htm)|Gruhastha - Moderate Curse|auto-trad|
|[sUi427H7MJnbiMxh.htm](boons-and-curses/sUi427H7MJnbiMxh.htm)|Hei Feng - Major Curse|auto-trad|
|[sy5UyOvxitqX244D.htm](boons-and-curses/sy5UyOvxitqX244D.htm)|Asmodeus - Major Boon|auto-trad|
|[T2csDhA9WsNiwpd5.htm](boons-and-curses/T2csDhA9WsNiwpd5.htm)|Kazutal - Major Boon|auto-trad|
|[tFnPBYcAZ0X3GbI5.htm](boons-and-curses/tFnPBYcAZ0X3GbI5.htm)|Lamashtu - Moderate Boon|auto-trad|
|[tilqIbJZLqeknTYo.htm](boons-and-curses/tilqIbJZLqeknTYo.htm)|Shelyn - Minor Boon|auto-trad|
|[to4pGXcJqF1VpqBt.htm](boons-and-curses/to4pGXcJqF1VpqBt.htm)|Kurgess - Major Boon|auto-trad|
|[U7ZJfuPLQPyoaj4M.htm](boons-and-curses/U7ZJfuPLQPyoaj4M.htm)|Pharasma - Major Boon|auto-trad|
|[ubXii3t9Jf9YHcCk.htm](boons-and-curses/ubXii3t9Jf9YHcCk.htm)|Kazutal - Minor Boon|auto-trad|
|[Ul5jjHfvvfB14a3z.htm](boons-and-curses/Ul5jjHfvvfB14a3z.htm)|Kurgess - Minor Curse|auto-trad|
|[uM9dzDmXfnpYCCrc.htm](boons-and-curses/uM9dzDmXfnpYCCrc.htm)|Casandalee - Moderate Boon|auto-trad|
|[unfh0BOx2PEhxWGB.htm](boons-and-curses/unfh0BOx2PEhxWGB.htm)|Besmara - Major Boon|auto-trad|
|[Up7nUIT42zwhgZf4.htm](boons-and-curses/Up7nUIT42zwhgZf4.htm)|Desna - Minor Curse|auto-trad|
|[uVZrqxtOOutr4Ss9.htm](boons-and-curses/uVZrqxtOOutr4Ss9.htm)|Gozreh - Minor Boon|auto-trad|
|[V7FYIgphCCkYsEXF.htm](boons-and-curses/V7FYIgphCCkYsEXF.htm)|Sarenrae - Moderate Curse|auto-trad|
|[vfQ7TsScMBTNiznn.htm](boons-and-curses/vfQ7TsScMBTNiznn.htm)|Gozreh - Major Boon|auto-trad|
|[vJd6Ikya7B6XH4lb.htm](boons-and-curses/vJd6Ikya7B6XH4lb.htm)|Kazutal - Minor Curse|auto-trad|
|[vU5leGDNAZSfPgTz.htm](boons-and-curses/vU5leGDNAZSfPgTz.htm)|Shelyn - Moderate Curse|auto-trad|
|[VZodqtMF3qrMl8a7.htm](boons-and-curses/VZodqtMF3qrMl8a7.htm)|Sarenrae - Minor Curse|auto-trad|
|[wEqPbkQMuqlL2TPn.htm](boons-and-curses/wEqPbkQMuqlL2TPn.htm)|Irori - Major Curse|auto-trad|
|[wIjqVRP0Hm2bIfyc.htm](boons-and-curses/wIjqVRP0Hm2bIfyc.htm)|Calistria - Minor Curse|auto-trad|
|[wlBiDpWvjKiw9k2z.htm](boons-and-curses/wlBiDpWvjKiw9k2z.htm)|Zon-Kuthon - Minor Boon|auto-trad|
|[wpOtcMUdymzpbtC4.htm](boons-and-curses/wpOtcMUdymzpbtC4.htm)|Gruhastha - Minor Boon|auto-trad|
|[wREsKC8CTifEyj6v.htm](boons-and-curses/wREsKC8CTifEyj6v.htm)|Norgorber - Major Curse|auto-trad|
|[WVlWzn918RNExxpe.htm](boons-and-curses/WVlWzn918RNExxpe.htm)|Sivanah - Major Boon|auto-trad|
|[WWyPOtmJc6lDLfgL.htm](boons-and-curses/WWyPOtmJc6lDLfgL.htm)|Groetus - Major Boon|auto-trad|
|[wXAVESzPURB1iBWW.htm](boons-and-curses/wXAVESzPURB1iBWW.htm)|Achaekek - Moderate Curse|auto-trad|
|[wZWklm7jLry2rTOk.htm](boons-and-curses/wZWklm7jLry2rTOk.htm)|Shizuru - Major Curse|auto-trad|
|[x1QrKuvZ2TbTvLUY.htm](boons-and-curses/x1QrKuvZ2TbTvLUY.htm)|Cayden Cailean - Moderate Curse|auto-trad|
|[XFd7rnb9cchNw0WN.htm](boons-and-curses/XFd7rnb9cchNw0WN.htm)|Norgorber - Minor Curse|auto-trad|
|[XFiI9qC4MNX4WUdh.htm](boons-and-curses/XFiI9qC4MNX4WUdh.htm)|Lamashtu - Major Boon|auto-trad|
|[XfMZ83VA7dtF7fL5.htm](boons-and-curses/XfMZ83VA7dtF7fL5.htm)|Urgathoa - Minor Curse|auto-trad|
|[Xlu60yqtTXutwG2G.htm](boons-and-curses/Xlu60yqtTXutwG2G.htm)|Tsukiyo - Major Boon|auto-trad|
|[XQg4eq4yIyEO7z3M.htm](boons-and-curses/XQg4eq4yIyEO7z3M.htm)|Brigh - Major Boon|auto-trad|
|[xrczhivVmi7MrEfz.htm](boons-and-curses/xrczhivVmi7MrEfz.htm)|Irori - Moderate Boon|auto-trad|
|[xrSgGobm5QtTcABQ.htm](boons-and-curses/xrSgGobm5QtTcABQ.htm)|Pharasma - Moderate Boon|auto-trad|
|[XujNRFYY4w1NgArr.htm](boons-and-curses/XujNRFYY4w1NgArr.htm)|Kurgess - Moderate Boon|auto-trad|
|[XV9hL75EXA9tIusv.htm](boons-and-curses/XV9hL75EXA9tIusv.htm)|Kazutal - Major Curse|auto-trad|
|[xwhPzOXxX1fON3j4.htm](boons-and-curses/xwhPzOXxX1fON3j4.htm)|Achaekek - Moderate Boon|auto-trad|
|[XYM7v72doPR3LwgA.htm](boons-and-curses/XYM7v72doPR3LwgA.htm)|Gozreh - Minor Curse|auto-trad|
|[y736aBzm4RKwB7RQ.htm](boons-and-curses/y736aBzm4RKwB7RQ.htm)|Ghlaunder - Moderate Curse|auto-trad|
|[y9DNpX7krUiOq0YI.htm](boons-and-curses/y9DNpX7krUiOq0YI.htm)|Asmodeus - Moderate Curse|auto-trad|
|[yBoHo1dcZpKygfJE.htm](boons-and-curses/yBoHo1dcZpKygfJE.htm)|Brigh - Minor Curse|auto-trad|
|[YeWT4MjQ8vDmSThs.htm](boons-and-curses/YeWT4MjQ8vDmSThs.htm)|Nocticula - Major Curse|auto-trad|
|[YHeCTSaw1sOnXhre.htm](boons-and-curses/YHeCTSaw1sOnXhre.htm)|Gruhastha - Major Curse|auto-trad|
|[yoXU4CqZtzgKXdlB.htm](boons-and-curses/yoXU4CqZtzgKXdlB.htm)|Asmodeus - Major Curse|auto-trad|
|[yWASKzumwmv4TgSe.htm](boons-and-curses/yWASKzumwmv4TgSe.htm)|Calistria - Minor Boon|auto-trad|
|[YXjcQMRNqJbosjan.htm](boons-and-curses/YXjcQMRNqJbosjan.htm)|Grandmother Spider - Moderate Curse|auto-trad|
|[YYc9SJ6t3gOMWYDi.htm](boons-and-curses/YYc9SJ6t3gOMWYDi.htm)|Gruhastha - Moderate Boon|auto-trad|
|[z4GfG3GatAOELKyA.htm](boons-and-curses/z4GfG3GatAOELKyA.htm)|Desna - Major Curse|auto-trad|
|[zMuLnoGROhRhqbDp.htm](boons-and-curses/zMuLnoGROhRhqbDp.htm)|Milani - Minor Curse|auto-trad|
|[zoJjzir97Ie7tbp0.htm](boons-and-curses/zoJjzir97Ie7tbp0.htm)|Shelyn - Moderate Boon|auto-trad|
|[ZPlvqkjEv9a7J76T.htm](boons-and-curses/ZPlvqkjEv9a7J76T.htm)|Cayden Cailean - Major Boon|auto-trad|
|[zu2yJaXxOus4tNqd.htm](boons-and-curses/zu2yJaXxOus4tNqd.htm)|Alseta - Major Curse|auto-trad|
|[zWshZRICuzP7DfFV.htm](boons-and-curses/zWshZRICuzP7DfFV.htm)|Iomedae - Major Curse|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[BKZRjVVt7hkvEpk3.htm](boons-and-curses/BKZRjVVt7hkvEpk3.htm)|Shizuru - Moderate Curse|Shizuru - Maldición moderada|modificada|
|[lwkqTPPWeJ6olxa3.htm](boons-and-curses/lwkqTPPWeJ6olxa3.htm)|Groetus - Moderate Curse|Groetus - Maldición moderada|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
