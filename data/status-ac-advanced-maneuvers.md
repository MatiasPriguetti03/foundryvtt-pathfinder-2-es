# Estado de la traducción (ac-advanced-maneuvers)

 * **auto-trad**: 45


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0WtqP7Wei3AxHh5M.htm](ac-advanced-maneuvers/0WtqP7Wei3AxHh5M.htm)|Frightening Display|auto-trad|
|[6LvtdtVVwtbzqUXG.htm](ac-advanced-maneuvers/6LvtdtVVwtbzqUXG.htm)|Screaming Skull|auto-trad|
|[7C9iFWBpEOJs4Uo2.htm](ac-advanced-maneuvers/7C9iFWBpEOJs4Uo2.htm)|Pterosaur Swoop|auto-trad|
|[7H8wqKtsAaSgyLyp.htm](ac-advanced-maneuvers/7H8wqKtsAaSgyLyp.htm)|Grab and Sting|auto-trad|
|[8JdETVTpbZAgtBA3.htm](ac-advanced-maneuvers/8JdETVTpbZAgtBA3.htm)|Breath Weapon|auto-trad|
|[9o7NwxGqohpa9M7r.htm](ac-advanced-maneuvers/9o7NwxGqohpa9M7r.htm)|Pick at the Meat|auto-trad|
|[aDuPDWgutiDIoWjW.htm](ac-advanced-maneuvers/aDuPDWgutiDIoWjW.htm)|Gulp Blood|auto-trad|
|[AH3kNbdGLYaRjVAr.htm](ac-advanced-maneuvers/AH3kNbdGLYaRjVAr.htm)|Bony Constriction|auto-trad|
|[ClUiCoBn3lHBBK5c.htm](ac-advanced-maneuvers/ClUiCoBn3lHBBK5c.htm)|Darting Attack|auto-trad|
|[cpMkN79PdNci3nGp.htm](ac-advanced-maneuvers/cpMkN79PdNci3nGp.htm)|Distracting Spray|auto-trad|
|[cVrW2GGLVpydj8h5.htm](ac-advanced-maneuvers/cVrW2GGLVpydj8h5.htm)|Blood Feast|auto-trad|
|[EvuMVR9ut9wIHOtq.htm](ac-advanced-maneuvers/EvuMVR9ut9wIHOtq.htm)|Bay|auto-trad|
|[EwCqdvU8WOw2SSxm.htm](ac-advanced-maneuvers/EwCqdvU8WOw2SSxm.htm)|Boar Charge|auto-trad|
|[F98ajoIakyOPEuwj.htm](ac-advanced-maneuvers/F98ajoIakyOPEuwj.htm)|Shred|auto-trad|
|[FKh7ZMNyKxgnV7kI.htm](ac-advanced-maneuvers/FKh7ZMNyKxgnV7kI.htm)|Throw Rock|auto-trad|
|[g4JEtCnKXyY4LJpm.htm](ac-advanced-maneuvers/g4JEtCnKXyY4LJpm.htm)|Feast on the Fallen|auto-trad|
|[H1ElYt6KovGYGzLD.htm](ac-advanced-maneuvers/H1ElYt6KovGYGzLD.htm)|Lumbering Knockdown|auto-trad|
|[h1Rldre8WVjUR5XO.htm](ac-advanced-maneuvers/h1Rldre8WVjUR5XO.htm)|Disgusting Gallop|auto-trad|
|[HLD4JW6SUn5Oy8C9.htm](ac-advanced-maneuvers/HLD4JW6SUn5Oy8C9.htm)|Badger Rage|auto-trad|
|[JMHoCFb886K6dT1n.htm](ac-advanced-maneuvers/JMHoCFb886K6dT1n.htm)|Rhinoceros Charge|auto-trad|
|[kKW68gi1FNJlNSOp.htm](ac-advanced-maneuvers/kKW68gi1FNJlNSOp.htm)|Constrict|auto-trad|
|[LaBfTYUsvoI3nscv.htm](ac-advanced-maneuvers/LaBfTYUsvoI3nscv.htm)|Take a Taste|auto-trad|
|[lrchn6ROZcuKCg3C.htm](ac-advanced-maneuvers/lrchn6ROZcuKCg3C.htm)|Careful Withdraw|auto-trad|
|[MQv67R7KBIrJkshN.htm](ac-advanced-maneuvers/MQv67R7KBIrJkshN.htm)|Grip Throat|auto-trad|
|[NMaMTiDB40L5O41S.htm](ac-advanced-maneuvers/NMaMTiDB40L5O41S.htm)|Telekinetic Assault|auto-trad|
|[oCgdnQP5bMDD02YC.htm](ac-advanced-maneuvers/oCgdnQP5bMDD02YC.htm)|Bear Hug|auto-trad|
|[ODGf6brAHHKrSAPo.htm](ac-advanced-maneuvers/ODGf6brAHHKrSAPo.htm)|Wing Thrash|auto-trad|
|[oisXYTTYE0TABboA.htm](ac-advanced-maneuvers/oisXYTTYE0TABboA.htm)|Gnaw|auto-trad|
|[ow3S5zhS51Nj8tsI.htm](ac-advanced-maneuvers/ow3S5zhS51Nj8tsI.htm)|Unnerving Screech|auto-trad|
|[oYEXImMSzg0eDqzR.htm](ac-advanced-maneuvers/oYEXImMSzg0eDqzR.htm)|Grabbing Trunk|auto-trad|
|[PAXJgysWqv3T0tTD.htm](ac-advanced-maneuvers/PAXJgysWqv3T0tTD.htm)|Sand Stride|auto-trad|
|[pMPtswKNokDNyYR2.htm](ac-advanced-maneuvers/pMPtswKNokDNyYR2.htm)|Defensive Curl|auto-trad|
|[q0Vh2V6KlzdMi1Pv.htm](ac-advanced-maneuvers/q0Vh2V6KlzdMi1Pv.htm)|Tongue Pull|auto-trad|
|[TCEQtO5DawzVAIyT.htm](ac-advanced-maneuvers/TCEQtO5DawzVAIyT.htm)|Tearing Clutch|auto-trad|
|[tsOaXTRWQvsKTyaL.htm](ac-advanced-maneuvers/tsOaXTRWQvsKTyaL.htm)|Gallop|auto-trad|
|[ttJ1CanrAuehoSV8.htm](ac-advanced-maneuvers/ttJ1CanrAuehoSV8.htm)|Death Roll|auto-trad|
|[U0J8J5ukmqnOskE8.htm](ac-advanced-maneuvers/U0J8J5ukmqnOskE8.htm)|Cat Pounce|auto-trad|
|[Upr7u0KhPAIMOqXv.htm](ac-advanced-maneuvers/Upr7u0KhPAIMOqXv.htm)|Snatch and Zap|auto-trad|
|[uR3j6TAJFeOVUHpY.htm](ac-advanced-maneuvers/uR3j6TAJFeOVUHpY.htm)|Lurching Rush|auto-trad|
|[Uv49tQCwUUjaMCj5.htm](ac-advanced-maneuvers/Uv49tQCwUUjaMCj5.htm)|Bouncing Slam|auto-trad|
|[WdTzlCO2i4loeYkQ.htm](ac-advanced-maneuvers/WdTzlCO2i4loeYkQ.htm)|Ultrasonic Scream|auto-trad|
|[X4VjMfVVAjZjwcrT.htm](ac-advanced-maneuvers/X4VjMfVVAjZjwcrT.htm)|Flyby Attack|auto-trad|
|[xssFjTXqGlsaddvl.htm](ac-advanced-maneuvers/xssFjTXqGlsaddvl.htm)|Knockdown|auto-trad|
|[yXw82Zd5nkcCUfRQ.htm](ac-advanced-maneuvers/yXw82Zd5nkcCUfRQ.htm)|Overwhelm|auto-trad|
|[zSlsnbvsGKdCTzL1.htm](ac-advanced-maneuvers/zSlsnbvsGKdCTzL1.htm)|Hustle|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
