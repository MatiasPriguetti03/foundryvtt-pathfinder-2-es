# Estado de la traducción (familiar-abilities)

 * **auto-trad**: 64
 * **ninguna**: 1


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[01-Rv1KTRioaZOWyQI6.htm](familiar-abilities/01-Rv1KTRioaZOWyQI6.htm)|Alchemical Gut|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[01-0Xrkk46IM43iI1Fv.htm](familiar-abilities/01-0Xrkk46IM43iI1Fv.htm)|Darkvision|auto-trad|
|[01-2fiQzoEKu6YUnrU9.htm](familiar-abilities/01-2fiQzoEKu6YUnrU9.htm)|Independent|auto-trad|
|[01-57b8u8s3fV0UCrgJ.htm](familiar-abilities/01-57b8u8s3fV0UCrgJ.htm)|Plant Form|auto-trad|
|[01-5gwqSpkRqWzrbDDU.htm](familiar-abilities/01-5gwqSpkRqWzrbDDU.htm)|Damage Avoidance: Will|auto-trad|
|[01-7QosmRHlyLLhU1hX.htm](familiar-abilities/01-7QosmRHlyLLhU1hX.htm)|Lab Assistant|auto-trad|
|[01-7ZxPS0UU7pf7wjp0.htm](familiar-abilities/01-7ZxPS0UU7pf7wjp0.htm)|Ambassador|auto-trad|
|[01-8Z1UkLEWkFWIjOF8.htm](familiar-abilities/01-8Z1UkLEWkFWIjOF8.htm)|Poison Reservoir|auto-trad|
|[01-92lgSEPFIDLvKOCF.htm](familiar-abilities/01-92lgSEPFIDLvKOCF.htm)|Accompanist|auto-trad|
|[01-9PsptrEoCC4QdM23.htm](familiar-abilities/01-9PsptrEoCC4QdM23.htm)|Valet|auto-trad|
|[01-A0C86V3MUECpX5jd.htm](familiar-abilities/01-A0C86V3MUECpX5jd.htm)|Amphibious|auto-trad|
|[01-aEKA3YWekLhEhuV8.htm](familiar-abilities/01-aEKA3YWekLhEhuV8.htm)|Threat Display|auto-trad|
|[01-Amzezp93MZckBYRZ.htm](familiar-abilities/01-Amzezp93MZckBYRZ.htm)|Wavesense|auto-trad|
|[01-asOhEdyF8CWFbR96.htm](familiar-abilities/01-asOhEdyF8CWFbR96.htm)|Spellcasting|auto-trad|
|[01-BXssJhTJjKrfojwG.htm](familiar-abilities/01-BXssJhTJjKrfojwG.htm)|Fast Movement: Land|auto-trad|
|[01-C16JgmeJJG249WXz.htm](familiar-abilities/01-C16JgmeJJG249WXz.htm)|Mask Freeze|auto-trad|
|[01-cT5octWchU4gjrhP.htm](familiar-abilities/01-cT5octWchU4gjrhP.htm)|Manual Dexterity|auto-trad|
|[01-D0ltNUJnN7UjJpA1.htm](familiar-abilities/01-D0ltNUJnN7UjJpA1.htm)|Innate Surge|auto-trad|
|[01-deC1yIM2S5szGdzT.htm](familiar-abilities/01-deC1yIM2S5szGdzT.htm)|Lifelink|auto-trad|
|[01-dpjf1CyMEILpJWyp.htm](familiar-abilities/01-dpjf1CyMEILpJWyp.htm)|Darkeater|auto-trad|
|[01-dWTfO5WbLkD5mw2H.htm](familiar-abilities/01-dWTfO5WbLkD5mw2H.htm)|Climber|auto-trad|
|[01-FcQQLMAJMgOLjnSv.htm](familiar-abilities/01-FcQQLMAJMgOLjnSv.htm)|Resistance|auto-trad|
|[01-FlRUb8U13Crj3NaA.htm](familiar-abilities/01-FlRUb8U13Crj3NaA.htm)|Scent|auto-trad|
|[01-fmsVItn94FeY5Q3X.htm](familiar-abilities/01-fmsVItn94FeY5Q3X.htm)|Snoop|auto-trad|
|[01-gPceRQqO847lvSnb.htm](familiar-abilities/01-gPceRQqO847lvSnb.htm)|Share Senses|auto-trad|
|[01-hMrxiUPHXKpKu1Ha.htm](familiar-abilities/01-hMrxiUPHXKpKu1Ha.htm)|Major Resistance|auto-trad|
|[01-j1qZiH50Bl2SJ8vT.htm](familiar-abilities/01-j1qZiH50Bl2SJ8vT.htm)|Shadow Step|auto-trad|
|[01-j9vOSbF9kLibhSIf.htm](familiar-abilities/01-j9vOSbF9kLibhSIf.htm)|Second Opinion|auto-trad|
|[01-jdlefpPcSCIe27vO.htm](familiar-abilities/01-jdlefpPcSCIe27vO.htm)|Familiar Focus|auto-trad|
|[01-jevzf9JbJJibpqaI.htm](familiar-abilities/01-jevzf9JbJJibpqaI.htm)|Skilled|auto-trad|
|[01-JRP2bdkdCdj2JDrq.htm](familiar-abilities/01-JRP2bdkdCdj2JDrq.htm)|Master's Form|auto-trad|
|[01-K5OLRDsGCfPZ6mO6.htm](familiar-abilities/01-K5OLRDsGCfPZ6mO6.htm)|Damage Avoidance: Reflex|auto-trad|
|[01-Le8UWr5BU8rV3iBf.htm](familiar-abilities/01-Le8UWr5BU8rV3iBf.htm)|Tough|auto-trad|
|[01-lpyJAl5B4j2DC7mc.htm](familiar-abilities/01-lpyJAl5B4j2DC7mc.htm)|Gills|auto-trad|
|[01-LrDnat1DsGJoAiKv.htm](familiar-abilities/01-LrDnat1DsGJoAiKv.htm)|Tremorsense|auto-trad|
|[01-mK3mAUWiRLZZYNdz.htm](familiar-abilities/01-mK3mAUWiRLZZYNdz.htm)|Damage Avoidance: Fortitude|auto-trad|
|[01-mKmBgQmPmiLOlEvw.htm](familiar-abilities/01-mKmBgQmPmiLOlEvw.htm)|Augury|auto-trad|
|[01-nrPl3Dz7fbnmas7T.htm](familiar-abilities/01-nrPl3Dz7fbnmas7T.htm)|Spirit Touch|auto-trad|
|[01-o0fxDDUu2ZSWYDTr.htm](familiar-abilities/01-o0fxDDUu2ZSWYDTr.htm)|Soul Sight|auto-trad|
|[01-O5TIjqXAuta8iVSz.htm](familiar-abilities/01-O5TIjqXAuta8iVSz.htm)|Focused Rejuvenation|auto-trad|
|[01-Q42nrq9LwDdbeZM5.htm](familiar-abilities/01-Q42nrq9LwDdbeZM5.htm)|Restorative Familiar|auto-trad|
|[01-qTxH8mSOvc4PMzrP.htm](familiar-abilities/01-qTxH8mSOvc4PMzrP.htm)|Kinspeech|auto-trad|
|[01-REJfFezELjc5Gzsy.htm](familiar-abilities/01-REJfFezELjc5Gzsy.htm)|Recall Familiar|auto-trad|
|[01-rs4Awf4k1e0Mj797.htm](familiar-abilities/01-rs4Awf4k1e0Mj797.htm)|Cantrip Connection|auto-trad|
|[01-SxWYVgqNMsq0OijU.htm](familiar-abilities/01-SxWYVgqNMsq0OijU.htm)|Fast Movement: Climb|auto-trad|
|[01-tEWAHPPZULvPgHnT.htm](familiar-abilities/01-tEWAHPPZULvPgHnT.htm)|Tattoo Transformation|auto-trad|
|[01-uUrsZ4WvhjKjFjnt.htm](familiar-abilities/01-uUrsZ4WvhjKjFjnt.htm)|Toolbearer|auto-trad|
|[01-uy15sDBuYNK48N3v.htm](familiar-abilities/01-uy15sDBuYNK48N3v.htm)|Burrower|auto-trad|
|[01-v7zE3tKQb9G6PaYU.htm](familiar-abilities/01-v7zE3tKQb9G6PaYU.htm)|Partner in Crime|auto-trad|
|[01-VHQUZcjUxfC3GcJ9.htm](familiar-abilities/01-VHQUZcjUxfC3GcJ9.htm)|Fast Movement: Fly|auto-trad|
|[01-vpw2ReYdcyQBpdqn.htm](familiar-abilities/01-vpw2ReYdcyQBpdqn.htm)|Fast Movement: Swim|auto-trad|
|[01-wOgvBymJOVQDSm1Q.htm](familiar-abilities/01-wOgvBymJOVQDSm1Q.htm)|Spell Delivery|auto-trad|
|[01-Xanjwv4YU0CBnsMw.htm](familiar-abilities/01-Xanjwv4YU0CBnsMw.htm)|Spell Battery|auto-trad|
|[01-XCqYnlVbLGqEGPeX.htm](familiar-abilities/01-XCqYnlVbLGqEGPeX.htm)|Touch Telepathy|auto-trad|
|[01-ZHSzNt3NxkXbj1mI.htm](familiar-abilities/01-ZHSzNt3NxkXbj1mI.htm)|Flier|auto-trad|
|[01-zKTL9y9et0oTHEYS.htm](familiar-abilities/01-zKTL9y9et0oTHEYS.htm)|Greater Resistance|auto-trad|
|[01-ZtKb89o1PPhwJ3Lx.htm](familiar-abilities/01-ZtKb89o1PPhwJ3Lx.htm)|Extra Reagents|auto-trad|
|[01-zyMRLQnFCQVpltiR.htm](familiar-abilities/01-zyMRLQnFCQVpltiR.htm)|Speech|auto-trad|
|[03-3y6GGXQgyJ4Hq4Yt.htm](familiar-abilities/03-3y6GGXQgyJ4Hq4Yt.htm)|Radiant|auto-trad|
|[03-cy7bdQqVANipyljS.htm](familiar-abilities/03-cy7bdQqVANipyljS.htm)|Erudite|auto-trad|
|[03-ou91pzf9TlOnIjYn.htm](familiar-abilities/03-ou91pzf9TlOnIjYn.htm)|Luminous|auto-trad|
|[03-ReIgBsaM95BTvHpN.htm](familiar-abilities/03-ReIgBsaM95BTvHpN.htm)|Medic|auto-trad|
|[04-43xB5UnexISlfRa5.htm](familiar-abilities/04-43xB5UnexISlfRa5.htm)|Purify Air|auto-trad|
|[04-LUBS9csNNgRTui4p.htm](familiar-abilities/04-LUBS9csNNgRTui4p.htm)|Grasping Tendrils|auto-trad|
|[04-SKIS1xexaOvrecdV.htm](familiar-abilities/04-SKIS1xexaOvrecdV.htm)|Verdant Burst|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
