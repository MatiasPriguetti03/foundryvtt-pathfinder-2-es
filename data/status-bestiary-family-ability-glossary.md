# Estado de la traducción (bestiary-family-ability-glossary)

 * **auto-trad**: 279
 * **modificada**: 7
 * **ninguna**: 5


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[hEeHuM3OVzJADsSa.htm](bestiary-family-ability-glossary/hEeHuM3OVzJADsSa.htm)|(Blackfrost Dead) Mindburning Gaze|
|[kLQ0eq18SvKEvggc.htm](bestiary-family-ability-glossary/kLQ0eq18SvKEvggc.htm)|(Blackfrost Dead) Blackfrost|
|[v14HanKkdPGQd7Km.htm](bestiary-family-ability-glossary/v14HanKkdPGQd7Km.htm)|(Blackfrost Dead) Shattering Death|
|[ydrDZ7lqioLPWRlB.htm](bestiary-family-ability-glossary/ydrDZ7lqioLPWRlB.htm)|(Blackfrost Dead) Blackfrost Breath|
|[yR9VLXIjJ2iOX77z.htm](bestiary-family-ability-glossary/yR9VLXIjJ2iOX77z.htm)|(Blackfrost Dead) Ice Climb|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[03knx0BWuYBNciXI.htm](bestiary-family-ability-glossary/03knx0BWuYBNciXI.htm)|(Graveknight) Empty Save for Dust|auto-trad|
|[08egiRxOvMX97XTc.htm](bestiary-family-ability-glossary/08egiRxOvMX97XTc.htm)|(Werecreature) Change Shape|auto-trad|
|[0B9eEetIbeSPhulD.htm](bestiary-family-ability-glossary/0B9eEetIbeSPhulD.htm)|(Divine Warden) Divine Domain Spells|auto-trad|
|[0K3vhX14UEPftqu8.htm](bestiary-family-ability-glossary/0K3vhX14UEPftqu8.htm)|(Ghost) Telekinetic Assault|auto-trad|
|[0MBZJdTv863X2jwz.htm](bestiary-family-ability-glossary/0MBZJdTv863X2jwz.htm)|(Graveknight) Portentous Glare|auto-trad|
|[1AnBJbwfdlvzA7SK.htm](bestiary-family-ability-glossary/1AnBJbwfdlvzA7SK.htm)|(Vampire, Jiang-Shi, Minister) Distant Steps|auto-trad|
|[1gsgecz7rhvHeiCX.htm](bestiary-family-ability-glossary/1gsgecz7rhvHeiCX.htm)|(Beheaded) Fiendish|auto-trad|
|[1p2UcxwgYDQzVqov.htm](bestiary-family-ability-glossary/1p2UcxwgYDQzVqov.htm)|(Beheaded) Furious Headbutt|auto-trad|
|[268Q7HdtylwpznvG.htm](bestiary-family-ability-glossary/268Q7HdtylwpznvG.htm)|(Vampire, Vetalarana, Basic) Mental Rebirth|auto-trad|
|[2BjqIO36k0Y6tgZf.htm](bestiary-family-ability-glossary/2BjqIO36k0Y6tgZf.htm)|(Vampire, Nosferatu Thrall) Fast Healing|auto-trad|
|[2GhEuIlgLj02gV9r.htm](bestiary-family-ability-glossary/2GhEuIlgLj02gV9r.htm)|(Skeleton) Rotten|auto-trad|
|[2k0X7JdI9MMt4pp0.htm](bestiary-family-ability-glossary/2k0X7JdI9MMt4pp0.htm)|(Zombie) Unholy Speed|auto-trad|
|[3oajfAIPSf77dSBL.htm](bestiary-family-ability-glossary/3oajfAIPSf77dSBL.htm)|(Secret Society) Get out of Jail|auto-trad|
|[3vcDqURRKk0mtdav.htm](bestiary-family-ability-glossary/3vcDqURRKk0mtdav.htm)|(Mana Wastes Mutant) Grasping Tentacle|auto-trad|
|[3VN2BLNE5zakeDCM.htm](bestiary-family-ability-glossary/3VN2BLNE5zakeDCM.htm)|(Lich) Metamagic Alteration|auto-trad|
|[49aAeGbH5p2IJ5Fz.htm](bestiary-family-ability-glossary/49aAeGbH5p2IJ5Fz.htm)|(Kuworsys) Rapid Bombardment|auto-trad|
|[4Aj4FWKObfMYoSvh.htm](bestiary-family-ability-glossary/4Aj4FWKObfMYoSvh.htm)|(Ghost) Beatific Appearance|auto-trad|
|[4AN6IgzsbhjCPYIZ.htm](bestiary-family-ability-glossary/4AN6IgzsbhjCPYIZ.htm)|(Zombie, Shock) Breath Weapon|auto-trad|
|[4B8O9QBJuDhJkVcz.htm](bestiary-family-ability-glossary/4B8O9QBJuDhJkVcz.htm)|(Vampire, Nosferatu) Dominate|auto-trad|
|[4p06ewIsPnWZwuwc.htm](bestiary-family-ability-glossary/4p06ewIsPnWZwuwc.htm)|(Ghost) Lynchpin|auto-trad|
|[4pBHIGtTK9yQmZ7h.htm](bestiary-family-ability-glossary/4pBHIGtTK9yQmZ7h.htm)|(Beheaded) Lifesense 60 feet|auto-trad|
|[4UEZY3G3CiCP78Kx.htm](bestiary-family-ability-glossary/4UEZY3G3CiCP78Kx.htm)|(Visitant) Wrestle|auto-trad|
|[5arYoSY5kcRo8TeM.htm](bestiary-family-ability-glossary/5arYoSY5kcRo8TeM.htm)|(Vampire, Vrykolakas) Vrykolakas Vulnerabilities|auto-trad|
|[5dbzWZbiTyPKgwKS.htm](bestiary-family-ability-glossary/5dbzWZbiTyPKgwKS.htm)|(Graveknight) Graveknight's Curse|auto-trad|
|[5DuBTf37u88IrphJ.htm](bestiary-family-ability-glossary/5DuBTf37u88IrphJ.htm)|(Vampire, Basic) Vampire Weaknesses|auto-trad|
|[5lv9r2ubDCov4dFn.htm](bestiary-family-ability-glossary/5lv9r2ubDCov4dFn.htm)|(Ghost) Pyre's Memory|auto-trad|
|[6Alm4mLj3ORxCXC2.htm](bestiary-family-ability-glossary/6Alm4mLj3ORxCXC2.htm)|(Ravener) Cowering Fear|auto-trad|
|[7jWANvnDmNcQvFve.htm](bestiary-family-ability-glossary/7jWANvnDmNcQvFve.htm)|(Graveknight) Ruinous Weapons|auto-trad|
|[7llQJrvVuCh7KjZO.htm](bestiary-family-ability-glossary/7llQJrvVuCh7KjZO.htm)|(Cryptid, Primeval) Stench|auto-trad|
|[7TYLgIMDGgUfbgGY.htm](bestiary-family-ability-glossary/7TYLgIMDGgUfbgGY.htm)|(Lich) Mask Death|auto-trad|
|[7V3M78uYKXxJTW37.htm](bestiary-family-ability-glossary/7V3M78uYKXxJTW37.htm)|(Castrovelian) Gaseous Adaptation|auto-trad|
|[7W6v0oULg9TCz9ym.htm](bestiary-family-ability-glossary/7W6v0oULg9TCz9ym.htm)|(Zombie) Spitting Zombie|auto-trad|
|[871jo3ZGnybF6dC8.htm](bestiary-family-ability-glossary/871jo3ZGnybF6dC8.htm)|(Lich) Blasphemous Utterances|auto-trad|
|[8IQqWkLqzvWA1JRJ.htm](bestiary-family-ability-glossary/8IQqWkLqzvWA1JRJ.htm)|(Vampire, Strigoi Progenitor) Create Spawn|auto-trad|
|[8MACWp05F4SacE7E.htm](bestiary-family-ability-glossary/8MACWp05F4SacE7E.htm)|(Vampire, Nosferatu Thrall) Weakness|auto-trad|
|[8UQWXpYfn9oE1ZHu.htm](bestiary-family-ability-glossary/8UQWXpYfn9oE1ZHu.htm)|(Vampire, Nosferatu) Command Thrall|auto-trad|
|[97Eo5oA1WeMJA7nR.htm](bestiary-family-ability-glossary/97Eo5oA1WeMJA7nR.htm)|(Skeleton) Grave Eruption|auto-trad|
|[98twzz56mMGN5Ftw.htm](bestiary-family-ability-glossary/98twzz56mMGN5Ftw.htm)|(Vampire, Vrykolakas) Drink Blood|auto-trad|
|[9oy2zWxy7Klk9FxR.htm](bestiary-family-ability-glossary/9oy2zWxy7Klk9FxR.htm)|(Skeleton) Blaze|auto-trad|
|[9w9hhvqGhrxdAzMH.htm](bestiary-family-ability-glossary/9w9hhvqGhrxdAzMH.htm)|(Secret Society) Connected|auto-trad|
|[9wX6EFimVFYFZjD7.htm](bestiary-family-ability-glossary/9wX6EFimVFYFZjD7.htm)|(Ulgrem-Axaan) Ichor Coating|auto-trad|
|[a7A4xY3NeWI2Ya4Z.htm](bestiary-family-ability-glossary/a7A4xY3NeWI2Ya4Z.htm)|(Lich) Rejuvenation|auto-trad|
|[a7SNHoP22SBoOAmA.htm](bestiary-family-ability-glossary/a7SNHoP22SBoOAmA.htm)|(Cryptid, Rumored) Hybrid Form|auto-trad|
|[aiwlPSmNY9b6Psvd.htm](bestiary-family-ability-glossary/aiwlPSmNY9b6Psvd.htm)|(Ghost) Frightful Moan|auto-trad|
|[AjTkksppymqzCivT.htm](bestiary-family-ability-glossary/AjTkksppymqzCivT.htm)|(Vampire, True) Dominate|auto-trad|
|[AkkmaixGqzw75h7W.htm](bestiary-family-ability-glossary/AkkmaixGqzw75h7W.htm)|(Graveknight) Eager for Battle|auto-trad|
|[apVBHJT95t9Fhxpb.htm](bestiary-family-ability-glossary/apVBHJT95t9Fhxpb.htm)|(Zombie) Infested Zombie|auto-trad|
|[AvoSAlqB8mZfjwr9.htm](bestiary-family-ability-glossary/AvoSAlqB8mZfjwr9.htm)|(Skeleton) Aquatic Bones|auto-trad|
|[awFTdoQfYva84HkU.htm](bestiary-family-ability-glossary/awFTdoQfYva84HkU.htm)|(Ulgrem-Axaan) Befouling Odor|auto-trad|
|[AydIHjDNbOZizj5U.htm](bestiary-family-ability-glossary/AydIHjDNbOZizj5U.htm)|(Ghost) Cold Spot|auto-trad|
|[AzvCvgx9SUAB2blo.htm](bestiary-family-ability-glossary/AzvCvgx9SUAB2blo.htm)|(Vampire, Strigoi) Domain of Dusk|auto-trad|
|[b3p8x6sgTa0BOvAb.htm](bestiary-family-ability-glossary/b3p8x6sgTa0BOvAb.htm)|(Lich) Siphon Life|auto-trad|
|[BBqjQN5Gbe4PWP56.htm](bestiary-family-ability-glossary/BBqjQN5Gbe4PWP56.htm)|(Vampire, Jiang-Shi, Basic) One More Breath|auto-trad|
|[BcSlVpaN72LoQ5BV.htm](bestiary-family-ability-glossary/BcSlVpaN72LoQ5BV.htm)|(Ghost) Site Bound|auto-trad|
|[BgbSHRdkGH7raOgA.htm](bestiary-family-ability-glossary/BgbSHRdkGH7raOgA.htm)|(Graveknight) Phantom Mount|auto-trad|
|[Bqiumr3LEo05d8x1.htm](bestiary-family-ability-glossary/Bqiumr3LEo05d8x1.htm)|(Mana Wastes Mutant) Hungry Maw|auto-trad|
|[Bqnh5wiXVymfDgTw.htm](bestiary-family-ability-glossary/Bqnh5wiXVymfDgTw.htm)|(Cryptid, Rumored) Burning Eyes|auto-trad|
|[br5Oup4USIUXQani.htm](bestiary-family-ability-glossary/br5Oup4USIUXQani.htm)|(Vampire, Vrykolakas) Swift Tracker|auto-trad|
|[BSLqIYqxcCVBb2Vp.htm](bestiary-family-ability-glossary/BSLqIYqxcCVBb2Vp.htm)|(Zombie) Unkillable|auto-trad|
|[bTJnxBQjr7G8yr30.htm](bestiary-family-ability-glossary/bTJnxBQjr7G8yr30.htm)|(Graveknight) Sacrilegious Aura|auto-trad|
|[buyzzomLTrIdVvTU.htm](bestiary-family-ability-glossary/buyzzomLTrIdVvTU.htm)|(Skeleton) Frozen|auto-trad|
|[bVZ6KizWVTLJUBXi.htm](bestiary-family-ability-glossary/bVZ6KizWVTLJUBXi.htm)|(Ghast) Paralysis|auto-trad|
|[c04ICnrzygyFG3PK.htm](bestiary-family-ability-glossary/c04ICnrzygyFG3PK.htm)|(Vampire, Basic) Drink Blood|auto-trad|
|[CdjqkgAexKk8khbB.htm](bestiary-family-ability-glossary/CdjqkgAexKk8khbB.htm)|(Zombie) Ankle Biter|auto-trad|
|[cfqRc4clMniqQNsl.htm](bestiary-family-ability-glossary/cfqRc4clMniqQNsl.htm)|(Cryptid, Mutant) Explosive End|auto-trad|
|[CxiEpXt7Gw3tSIOh.htm](bestiary-family-ability-glossary/CxiEpXt7Gw3tSIOh.htm)|(Zombie) Rotting Aura|auto-trad|
|[cYkEpJzpMu3mCrFc.htm](bestiary-family-ability-glossary/cYkEpJzpMu3mCrFc.htm)|(Ghost) Phantasmagoria|auto-trad|
|[d6eWuFd6pw6ffvPC.htm](bestiary-family-ability-glossary/d6eWuFd6pw6ffvPC.htm)|(Melfesh Monster) Swell|auto-trad|
|[DIYZNvVP28U2UnDb.htm](bestiary-family-ability-glossary/DIYZNvVP28U2UnDb.htm)|(Siabrae) Earth Glide|auto-trad|
|[DlHTe9jLssEELY6a.htm](bestiary-family-ability-glossary/DlHTe9jLssEELY6a.htm)|(Divine Warden) Divine Innate Spells|auto-trad|
|[DWku0nZzkqmYjrQ5.htm](bestiary-family-ability-glossary/DWku0nZzkqmYjrQ5.htm)|(Vampire, Nosferatu) Change Shape|auto-trad|
|[EpDyZ0mG9beLkBna.htm](bestiary-family-ability-glossary/EpDyZ0mG9beLkBna.htm)|(Vampire, Vetalarana, Basic) Drain Thoughts|auto-trad|
|[eQLGYrJhu787pEwc.htm](bestiary-family-ability-glossary/eQLGYrJhu787pEwc.htm)|(Vampire, Strigoi) Shadow Form|auto-trad|
|[Es8g7kZrLAuNdiD1.htm](bestiary-family-ability-glossary/Es8g7kZrLAuNdiD1.htm)|(Siabrae) Miasma|auto-trad|
|[EsbdyB9DwvPvhCCC.htm](bestiary-family-ability-glossary/EsbdyB9DwvPvhCCC.htm)|(Vampire, Nosferatu Overlord) Paralytic Fear|auto-trad|
|[eSp48kG7v1GNuGgh.htm](bestiary-family-ability-glossary/eSp48kG7v1GNuGgh.htm)|(Visitant) Vengeful Presence|auto-trad|
|[EwhjPJGLSW9v1Fbb.htm](bestiary-family-ability-glossary/EwhjPJGLSW9v1Fbb.htm)|(Vampire, Nosferatu) Divine Innate Spells|auto-trad|
|[eXleBXdAemiEoHA8.htm](bestiary-family-ability-glossary/eXleBXdAemiEoHA8.htm)|(Nymph Queen) Change Shape|auto-trad|
|[EyFVoDiReJsBx1rf.htm](bestiary-family-ability-glossary/EyFVoDiReJsBx1rf.htm)|(Lich) Animate Cage|auto-trad|
|[Ez8sVqv1EBcJuorK.htm](bestiary-family-ability-glossary/Ez8sVqv1EBcJuorK.htm)|(Zombie) Putrid Stench|auto-trad|
|[FA0ri2fAcMa1HgZe.htm](bestiary-family-ability-glossary/FA0ri2fAcMa1HgZe.htm)|(Werecreature) Moon Frenzy|auto-trad|
|[FdUuCIkQxVoTGM78.htm](bestiary-family-ability-glossary/FdUuCIkQxVoTGM78.htm)|(Mana Wastes Mutant) Mirror Thing|auto-trad|
|[fh7ar6QrFT8YWgQ9.htm](bestiary-family-ability-glossary/fh7ar6QrFT8YWgQ9.htm)|(Vampire, Strigoi Progenitor) Drink Essence|auto-trad|
|[fmUBaLklyVmNt3VD.htm](bestiary-family-ability-glossary/fmUBaLklyVmNt3VD.htm)|(Graveknight) Clutching Armor|auto-trad|
|[fOpw4k05kayaL13a.htm](bestiary-family-ability-glossary/fOpw4k05kayaL13a.htm)|(Mana Wastes Mutant) Magic Hunger|auto-trad|
|[fqZhojt2M5LfSKSH.htm](bestiary-family-ability-glossary/fqZhojt2M5LfSKSH.htm)|(Lich) Drain Soul Cage|auto-trad|
|[FsqVhavMWAoFro1L.htm](bestiary-family-ability-glossary/FsqVhavMWAoFro1L.htm)|(Ravener) Soul Ward|auto-trad|
|[fTk5nmm7HHtXOdSo.htm](bestiary-family-ability-glossary/fTk5nmm7HHtXOdSo.htm)|(Lich) Dark Deliverance|auto-trad|
|[fVyoHEO3fSR737M1.htm](bestiary-family-ability-glossary/fVyoHEO3fSR737M1.htm)|(Ghost) Draining Touch|auto-trad|
|[FW4KAUHb7r8WkxUc.htm](bestiary-family-ability-glossary/FW4KAUHb7r8WkxUc.htm)|(Ghoul) Paralysis|auto-trad|
|[FXHjmH1oce7Z3tZb.htm](bestiary-family-ability-glossary/FXHjmH1oce7Z3tZb.htm)|(Vampire, Basic) Coffin Restoration|auto-trad|
|[fYDrunTldWmFvfjl.htm](bestiary-family-ability-glossary/fYDrunTldWmFvfjl.htm)|(Ravener) Soulsense|auto-trad|
|[G4yIjHMzD0KVpaCm.htm](bestiary-family-ability-glossary/G4yIjHMzD0KVpaCm.htm)|(Melfesh Monster) Burrowing Grasp|auto-trad|
|[ga0Oj7mmjSwWQgmR.htm](bestiary-family-ability-glossary/ga0Oj7mmjSwWQgmR.htm)|(Ghost) Corrupting Gaze|auto-trad|
|[gbKpCw21tEmehq6e.htm](bestiary-family-ability-glossary/gbKpCw21tEmehq6e.htm)|(Siabrae) Rejuvenation|auto-trad|
|[GD1ZD4Rl2hTPhvjL.htm](bestiary-family-ability-glossary/GD1ZD4Rl2hTPhvjL.htm)|(Zombie, Shock) Arcing Strikes|auto-trad|
|[GgmLWLBLGaFvunTS.htm](bestiary-family-ability-glossary/GgmLWLBLGaFvunTS.htm)|(Vampire, Vetalarana, Basic) Thoughtsense 100 feet|auto-trad|
|[GpKQH8hnSYWiyuQU.htm](bestiary-family-ability-glossary/GpKQH8hnSYWiyuQU.htm)|(Vampire, Nosferatu) Drink Blood|auto-trad|
|[gSXxJ2FYEGrX1Psy.htm](bestiary-family-ability-glossary/gSXxJ2FYEGrX1Psy.htm)|(Secret Society) Not Today!|auto-trad|
|[h2C99bXIwPGRdZQ0.htm](bestiary-family-ability-glossary/h2C99bXIwPGRdZQ0.htm)|(Castrovelian) Extra Large|auto-trad|
|[hA6HsM4i4yPfEsDH.htm](bestiary-family-ability-glossary/hA6HsM4i4yPfEsDH.htm)|(Ghast) Ghast Fever|auto-trad|
|[HdSaMUb4uEsbtQTn.htm](bestiary-family-ability-glossary/HdSaMUb4uEsbtQTn.htm)|(Skeleton) Skeleton of Roses|auto-trad|
|[HEmOAVbJ3T9pon6T.htm](bestiary-family-ability-glossary/HEmOAVbJ3T9pon6T.htm)|(Beheaded) Entangling|auto-trad|
|[hEWAzlIoJg3NzA7Q.htm](bestiary-family-ability-glossary/hEWAzlIoJg3NzA7Q.htm)|(Spring-Heeled Jack) Change Shape|auto-trad|
|[hfT7YDwx0UX4expm.htm](bestiary-family-ability-glossary/hfT7YDwx0UX4expm.htm)|(Lich) Steal Soul|auto-trad|
|[HKFoOZZV4WdjkeeJ.htm](bestiary-family-ability-glossary/HKFoOZZV4WdjkeeJ.htm)|(Protean) Warpwave|auto-trad|
|[HLPLYJ9bOazb2ZPX.htm](bestiary-family-ability-glossary/HLPLYJ9bOazb2ZPX.htm)|(Ravener) Discorporate|auto-trad|
|[hOOXkWcTw9EFKJev.htm](bestiary-family-ability-glossary/hOOXkWcTw9EFKJev.htm)|(Vampire, Nosferatu) Plagued Coffin Restoration|auto-trad|
|[hwrZQSADT36TjDdv.htm](bestiary-family-ability-glossary/hwrZQSADT36TjDdv.htm)|(Vampire, Jiang-Shi, Basic) Warped Fulu|auto-trad|
|[Hy7bO4TSNoQNMzA6.htm](bestiary-family-ability-glossary/Hy7bO4TSNoQNMzA6.htm)|(Mana Wastes Mutant) Energy Blast|auto-trad|
|[i4WBOAb7CmY53doM.htm](bestiary-family-ability-glossary/i4WBOAb7CmY53doM.htm)|(Melfesh Monster) False Synapses|auto-trad|
|[i7m74TphAiFYvzPL.htm](bestiary-family-ability-glossary/i7m74TphAiFYvzPL.htm)|(Ghost) Ghost Storm|auto-trad|
|[iAXHLkxuuCUOwqkN.htm](bestiary-family-ability-glossary/iAXHLkxuuCUOwqkN.htm)|(Werecreature) Animal Empathy|auto-trad|
|[ICnpftxZEilrYjn0.htm](bestiary-family-ability-glossary/ICnpftxZEilrYjn0.htm)|(Werecreature) Curse of the Werecreature|auto-trad|
|[IFQd4GiHlV33p7oK.htm](bestiary-family-ability-glossary/IFQd4GiHlV33p7oK.htm)|(Nymph Queen) Inspiration|auto-trad|
|[ii98NEWkpvIJXgFZ.htm](bestiary-family-ability-glossary/ii98NEWkpvIJXgFZ.htm)|(Zombie, Shock) Lightning Rod|auto-trad|
|[iiIhLkjuPYJ93Upw.htm](bestiary-family-ability-glossary/iiIhLkjuPYJ93Upw.htm)|(Melfesh Monster) Smolderstench|auto-trad|
|[IL8wtCi23ch62zXG.htm](bestiary-family-ability-glossary/IL8wtCi23ch62zXG.htm)|(Vampire, Nosferatu Thrall) Rally|auto-trad|
|[IOk0MRs3f9FrarKL.htm](bestiary-family-ability-glossary/IOk0MRs3f9FrarKL.htm)|(Protean) Protean Anatomy|auto-trad|
|[ipGBYIXk4u47Mp1D.htm](bestiary-family-ability-glossary/ipGBYIXk4u47Mp1D.htm)|(Zombie) Feast|auto-trad|
|[iwLj14liESK5OBN8.htm](bestiary-family-ability-glossary/iwLj14liESK5OBN8.htm)|(Ghost) Malevolent Possession|auto-trad|
|[IxJbFJ8dG5RbZWBD.htm](bestiary-family-ability-glossary/IxJbFJ8dG5RbZWBD.htm)|(Graveknight) Devastating Blast|auto-trad|
|[ixPqVlqLaYTB1b23.htm](bestiary-family-ability-glossary/ixPqVlqLaYTB1b23.htm)|(Ghoul) Consume Flesh|auto-trad|
|[jciRdEHgGNGFXLsg.htm](bestiary-family-ability-glossary/jciRdEHgGNGFXLsg.htm)|(Siabrae) Blight Mastery|auto-trad|
|[jD0M3yV6gjkXafsJ.htm](bestiary-family-ability-glossary/jD0M3yV6gjkXafsJ.htm)|(Divine Warden) Divine Destruction|auto-trad|
|[jjWisLWwcdxsqv8o.htm](bestiary-family-ability-glossary/jjWisLWwcdxsqv8o.htm)|(Kallas Devil) Blameless|auto-trad|
|[JSBmboE6bYVxDT9d.htm](bestiary-family-ability-glossary/JSBmboE6bYVxDT9d.htm)|(Ghost) Inhabit Object|auto-trad|
|[jsZZJDd4ZuYMlEVV.htm](bestiary-family-ability-glossary/jsZZJDd4ZuYMlEVV.htm)|(Coven) Share Senses|auto-trad|
|[jTuK430yY8VgIByB.htm](bestiary-family-ability-glossary/jTuK430yY8VgIByB.htm)|(Ghost) Haunted House|auto-trad|
|[K0scCV18j5FzM2ei.htm](bestiary-family-ability-glossary/K0scCV18j5FzM2ei.htm)|(Cryptid, Rumored) Shifting Form|auto-trad|
|[K5bNE90vmeFzktnM.htm](bestiary-family-ability-glossary/K5bNE90vmeFzktnM.htm)|(Skeleton) Collapse|auto-trad|
|[KcUHCVhHnkMD8j3k.htm](bestiary-family-ability-glossary/KcUHCVhHnkMD8j3k.htm)|(Greater Barghest) Mutation - Toxic Breath|auto-trad|
|[kew9yPbf83smsCyL.htm](bestiary-family-ability-glossary/kew9yPbf83smsCyL.htm)|(Mana Wastes Mutant) Energy Resistance|auto-trad|
|[kG4fDd16fYEFvmgy.htm](bestiary-family-ability-glossary/kG4fDd16fYEFvmgy.htm)|(Nymph Queen) Nymph's Beauty|auto-trad|
|[KJrQdJQ0ZEmKYH4Y.htm](bestiary-family-ability-glossary/KJrQdJQ0ZEmKYH4Y.htm)|(Kallas Devil) Suffer the Children|auto-trad|
|[KLMdplDgOfXSLh6g.htm](bestiary-family-ability-glossary/KLMdplDgOfXSLh6g.htm)|(Cryptid, Rumored) Howl|auto-trad|
|[KOnVQPlRY1CqcJXy.htm](bestiary-family-ability-glossary/KOnVQPlRY1CqcJXy.htm)|(Beheaded) Bleeding|auto-trad|
|[kQZ6pzdSn6FaxWF2.htm](bestiary-family-ability-glossary/kQZ6pzdSn6FaxWF2.htm)|(Visitant) Visitant Spells|auto-trad|
|[kUApLn0cOsXQNSrL.htm](bestiary-family-ability-glossary/kUApLn0cOsXQNSrL.htm)|(Clockwork Creature) Malfunction - Damaged Propulsion|auto-trad|
|[KwmYKsaXHKfZgB2c.htm](bestiary-family-ability-glossary/KwmYKsaXHKfZgB2c.htm)|(Spring-Heeled Jack) Vanishing Leap|auto-trad|
|[l2ov5uPpfOAoyXAL.htm](bestiary-family-ability-glossary/l2ov5uPpfOAoyXAL.htm)|(Cryptid, Rumored) Obscura Vulnerability|auto-trad|
|[l5FyTQQ0OfICCS1c.htm](bestiary-family-ability-glossary/l5FyTQQ0OfICCS1c.htm)|(Cryptid, Primeval) Shockwave|auto-trad|
|[L6EypYQTdK4XPldM.htm](bestiary-family-ability-glossary/L6EypYQTdK4XPldM.htm)|(Secret Society) Prepared Trap|auto-trad|
|[L7kL2ps5k80XLtwV.htm](bestiary-family-ability-glossary/L7kL2ps5k80XLtwV.htm)|(Vampire, Jiang-Shi, Minister) Tumult of the Blood|auto-trad|
|[L80gn6WOWi9roJW3.htm](bestiary-family-ability-glossary/L80gn6WOWi9roJW3.htm)|(Divine Warden) Instrument of Faith|auto-trad|
|[lAnJ25DSs44Ya8jg.htm](bestiary-family-ability-glossary/lAnJ25DSs44Ya8jg.htm)|(Ghost) Fade|auto-trad|
|[lD9Onw05RxdcmM2e.htm](bestiary-family-ability-glossary/lD9Onw05RxdcmM2e.htm)|(Mana Wastes Mutant) Revolting Appearance|auto-trad|
|[LKxsPOf0hAS32Sp8.htm](bestiary-family-ability-glossary/LKxsPOf0hAS32Sp8.htm)|(Vampire, Nosferatu Thrall) Mindbound|auto-trad|
|[LOd8QwaKP3hnyGkc.htm](bestiary-family-ability-glossary/LOd8QwaKP3hnyGkc.htm)|(Kallas Devil) Cold Currents|auto-trad|
|[lv1T0kAmG2JS7PWs.htm](bestiary-family-ability-glossary/lv1T0kAmG2JS7PWs.htm)|(Mana Wastes Mutant) Sprouted Limb|auto-trad|
|[m6teF5ADh7vuM8Zr.htm](bestiary-family-ability-glossary/m6teF5ADh7vuM8Zr.htm)|(Ghast) Consume Flesh|auto-trad|
|[MAC97gjFcdiqLyhp.htm](bestiary-family-ability-glossary/MAC97gjFcdiqLyhp.htm)|(Skeleton) Screaming Skull|auto-trad|
|[mEbrInCpag7YThH2.htm](bestiary-family-ability-glossary/mEbrInCpag7YThH2.htm)|(Dragon) Change Shape|auto-trad|
|[MhymIeTQoxbacG1o.htm](bestiary-family-ability-glossary/MhymIeTQoxbacG1o.htm)|(Zombie) Plague-Ridden|auto-trad|
|[mmotrGIfshEHi8Rr.htm](bestiary-family-ability-glossary/mmotrGIfshEHi8Rr.htm)|(Mana Wastes Mutant) Chameleon Skin|auto-trad|
|[mqai5e7YAuK2tbB9.htm](bestiary-family-ability-glossary/mqai5e7YAuK2tbB9.htm)|(Mana Wastes Mutant) Vengeful Bite|auto-trad|
|[mwEig0MYM7EIibSU.htm](bestiary-family-ability-glossary/mwEig0MYM7EIibSU.htm)|(Greater Barghest) Mutation - Vestigial Arm Strike|auto-trad|
|[mZpX54PgTxPta5y4.htm](bestiary-family-ability-glossary/mZpX54PgTxPta5y4.htm)|(Graveknight) Graveknight's Shield|auto-trad|
|[N7UV5CZXtcoxDxCF.htm](bestiary-family-ability-glossary/N7UV5CZXtcoxDxCF.htm)|(Ghoul) Ghoul Fever|auto-trad|
|[N9OUII3LfRr6hNP8.htm](bestiary-family-ability-glossary/N9OUII3LfRr6hNP8.htm)|(Cryptid, Rumored) Vanishing Escape|auto-trad|
|[na1WJDEaoqpcQuOR.htm](bestiary-family-ability-glossary/na1WJDEaoqpcQuOR.htm)|(Kallas Devil) Infectious Water|auto-trad|
|[ndN9zEIheRZGOEUW.htm](bestiary-family-ability-glossary/ndN9zEIheRZGOEUW.htm)|(Secret Society) Tag Team|auto-trad|
|[NgiwaeUqMPfkYvQq.htm](bestiary-family-ability-glossary/NgiwaeUqMPfkYvQq.htm)|(Vampire, Nosferatu) Plague of Ancients|auto-trad|
|[Nnl5wg6smOzieTop.htm](bestiary-family-ability-glossary/Nnl5wg6smOzieTop.htm)|(Vampire, True) Turn to Mist|auto-trad|
|[noOXyIXmwYN2rRd1.htm](bestiary-family-ability-glossary/noOXyIXmwYN2rRd1.htm)|(Vampire, True) Children of the Night|auto-trad|
|[O6AXB9aZB0K14sS5.htm](bestiary-family-ability-glossary/O6AXB9aZB0K14sS5.htm)|(Vampire, Jiang-Shi, Basic) Rigor Mortis|auto-trad|
|[oD1cA9uXYQ8evT8f.htm](bestiary-family-ability-glossary/oD1cA9uXYQ8evT8f.htm)|(Melfesh Monster) Vent Flames|auto-trad|
|[OD3VPUalSKDTYFmF.htm](bestiary-family-ability-glossary/OD3VPUalSKDTYFmF.htm)|(Greater Barghest) Mutation - Wings|auto-trad|
|[OeLdzrhIrDOvsm3E.htm](bestiary-family-ability-glossary/OeLdzrhIrDOvsm3E.htm)|(Lich) Unholy Touch|auto-trad|
|[OMSsLUcnRj6ycEUa.htm](bestiary-family-ability-glossary/OMSsLUcnRj6ycEUa.htm)|(Graveknight) Channel Magic|auto-trad|
|[OPGu0WsUHpHSmu80.htm](bestiary-family-ability-glossary/OPGu0WsUHpHSmu80.htm)|(Divine Warden) Faith Bound|auto-trad|
|[OQt7nHInDBAmHaG6.htm](bestiary-family-ability-glossary/OQt7nHInDBAmHaG6.htm)|(Vampire, Nosferatu Thrall) Mortal Shield|auto-trad|
|[OSnNiMdqgARyEVuv.htm](bestiary-family-ability-glossary/OSnNiMdqgARyEVuv.htm)|(Vampire, Jiang-Shi, Basic) Breathsense 60 feet|auto-trad|
|[oXRnrQQ04oi8OkDG.htm](bestiary-family-ability-glossary/oXRnrQQ04oi8OkDG.htm)|(Vampire, Vrykolakas) Feral Possession|auto-trad|
|[P5YTG6I8ci4lwhZ1.htm](bestiary-family-ability-glossary/P5YTG6I8ci4lwhZ1.htm)|(Skeleton) Bloody|auto-trad|
|[pEJkjqjjBXj4YjYZ.htm](bestiary-family-ability-glossary/pEJkjqjjBXj4YjYZ.htm)|(Kuworsys) Careless Block|auto-trad|
|[PjYwPIUUrirQFiee.htm](bestiary-family-ability-glossary/PjYwPIUUrirQFiee.htm)|(Kallas Devil) Underwater Views|auto-trad|
|[px2XMvw2s5RLV0X1.htm](bestiary-family-ability-glossary/px2XMvw2s5RLV0X1.htm)|(Lich) Aura of Rot|auto-trad|
|[pxiSbjfWaKCG1xLD.htm](bestiary-family-ability-glossary/pxiSbjfWaKCG1xLD.htm)|(Graveknight) Betrayed Revivification|auto-trad|
|[pyBxJKGeheGA8et4.htm](bestiary-family-ability-glossary/pyBxJKGeheGA8et4.htm)|(Clockwork Creature) Malfunction - Backfire|auto-trad|
|[Qf94y985g0o6lEoN.htm](bestiary-family-ability-glossary/Qf94y985g0o6lEoN.htm)|(Zombie) Tearing Grapple|auto-trad|
|[qfDwBrCXeIYp0W8T.htm](bestiary-family-ability-glossary/qfDwBrCXeIYp0W8T.htm)|(Cryptid, Rumored) Creature Obscura|auto-trad|
|[qjgrMhwz78T31kLU.htm](bestiary-family-ability-glossary/qjgrMhwz78T31kLU.htm)|(Vampire, Strigoi) Strigoi Weaknesses|auto-trad|
|[qlNOqsfJH2gj7DCs.htm](bestiary-family-ability-glossary/qlNOqsfJH2gj7DCs.htm)|(Kallas Devil) Waterfall Torrent|auto-trad|
|[QlrbnkeZu6M4kvOy.htm](bestiary-family-ability-glossary/QlrbnkeZu6M4kvOy.htm)|(Worm That Walks) Discorporate|auto-trad|
|[QMlQ5AvcunCvjlfM.htm](bestiary-family-ability-glossary/QMlQ5AvcunCvjlfM.htm)|(Vampire, Jiang-Shi, Basic) Drain Qi|auto-trad|
|[QMWvYf8Qowj2Fzmx.htm](bestiary-family-ability-glossary/QMWvYf8Qowj2Fzmx.htm)|(Graveknight) Dark Deliverance|auto-trad|
|[qs21GajPmFYel3pc.htm](bestiary-family-ability-glossary/qs21GajPmFYel3pc.htm)|(Melfesh Monster) Corpse Bomb|auto-trad|
|[qt2exWwQTzoObKfW.htm](bestiary-family-ability-glossary/qt2exWwQTzoObKfW.htm)|(Golem) Inexorable March|auto-trad|
|[QuHeuVNDnmOCS9M1.htm](bestiary-family-ability-glossary/QuHeuVNDnmOCS9M1.htm)|(Vampire, Vrykolakas Master) Create Spawn|auto-trad|
|[qYVKpytVx46oBVox.htm](bestiary-family-ability-glossary/qYVKpytVx46oBVox.htm)|(Cryptid, Experimental) Energy Wave|auto-trad|
|[qZnZriRRtjK7FtP8.htm](bestiary-family-ability-glossary/qZnZriRRtjK7FtP8.htm)|(Zombie) Persistent Limbs|auto-trad|
|[R0tsWv6QHd2jbQON.htm](bestiary-family-ability-glossary/R0tsWv6QHd2jbQON.htm)|(Cryptid, Primeval) Grasp for Life|auto-trad|
|[R1vYhCJ2KvT8uAy1.htm](bestiary-family-ability-glossary/R1vYhCJ2KvT8uAy1.htm)|(Vampire, Jiang-Shi, Basic) Jiang-Shi Vulnerabilities|auto-trad|
|[r34QDwKiWZoVymJa.htm](bestiary-family-ability-glossary/r34QDwKiWZoVymJa.htm)|(Golem) Golem Antimagic|auto-trad|
|[R5VgwNbwc3QbqVXC.htm](bestiary-family-ability-glossary/R5VgwNbwc3QbqVXC.htm)|(Vampire, Strigoi Progenitor) Shadow Escape|auto-trad|
|[rBc9fJlMXhzvn05L.htm](bestiary-family-ability-glossary/rBc9fJlMXhzvn05L.htm)|(Vampire, Strigoi) Drink Essence|auto-trad|
|[rdQzd5ClgGPP4Qmt.htm](bestiary-family-ability-glossary/rdQzd5ClgGPP4Qmt.htm)|(Skeleton) Nimble|auto-trad|
|[RgYpzTk3XapgVeXZ.htm](bestiary-family-ability-glossary/RgYpzTk3XapgVeXZ.htm)|(Skeleton) Bone Missile|auto-trad|
|[rlNiZiUb9pRKFKQo.htm](bestiary-family-ability-glossary/rlNiZiUb9pRKFKQo.htm)|(Beheaded) Giant|auto-trad|
|[RQ9NaIerG95wkhjl.htm](bestiary-family-ability-glossary/RQ9NaIerG95wkhjl.htm)|(Ulgrem-Axaan) Fallen Victim|auto-trad|
|[RvzFTym3r22VhMrm.htm](bestiary-family-ability-glossary/RvzFTym3r22VhMrm.htm)|(Kuworsys) Smash and Grab|auto-trad|
|[rwEFqSLw4yIscGrO.htm](bestiary-family-ability-glossary/rwEFqSLw4yIscGrO.htm)|(Vampire, Vetalarana, Basic) Vetalarana Vulnerabilities|auto-trad|
|[s6JRMjgA7hSGUAYX.htm](bestiary-family-ability-glossary/s6JRMjgA7hSGUAYX.htm)|(Vampire, Vrykolakas Master) Pestilential Aura|auto-trad|
|[sajLbVE6VpCsR0Kl.htm](bestiary-family-ability-glossary/sajLbVE6VpCsR0Kl.htm)|(Skeleton) Bone Powder|auto-trad|
|[sAyR4uJbsWukZFZf.htm](bestiary-family-ability-glossary/sAyR4uJbsWukZFZf.htm)|(Vampire, True) Mist Escape|auto-trad|
|[sECjzfaYaW68vbLV.htm](bestiary-family-ability-glossary/sECjzfaYaW68vbLV.htm)|(Vampire, Vrykolakas Master) Change Shape|auto-trad|
|[SEmSk1INZDmeoB5R.htm](bestiary-family-ability-glossary/SEmSk1INZDmeoB5R.htm)|(Nymph Queen) Focus Beauty|auto-trad|
|[SEU4K3QRAUHEMRl2.htm](bestiary-family-ability-glossary/SEU4K3QRAUHEMRl2.htm)|(Cryptid, Mutant) Unusual Bane|auto-trad|
|[SEzkqVJxr2eJDsuJ.htm](bestiary-family-ability-glossary/SEzkqVJxr2eJDsuJ.htm)|(Ghast) Stench|auto-trad|
|[SFDNK6AauPO6io5B.htm](bestiary-family-ability-glossary/SFDNK6AauPO6io5B.htm)|(Mana Wastes Mutant) Eldritch Attraction|auto-trad|
|[Sj1IGDjUmlwFpC35.htm](bestiary-family-ability-glossary/Sj1IGDjUmlwFpC35.htm)|(Vampire, Nosferatu) Nosferatu Vulnerabilities|auto-trad|
|[sWDlb5vJRK092MMt.htm](bestiary-family-ability-glossary/sWDlb5vJRK092MMt.htm)|(Vampire, Vetalarana, Manipulator) Control Comatose|auto-trad|
|[tDfv2oEPal19NtSM.htm](bestiary-family-ability-glossary/tDfv2oEPal19NtSM.htm)|(Ghost) Fetch|auto-trad|
|[tdqw4QXzA2x7fDLT.htm](bestiary-family-ability-glossary/tdqw4QXzA2x7fDLT.htm)|(Lich) Pillage Mind|auto-trad|
|[TIJ7r9lXvNYSIMLI.htm](bestiary-family-ability-glossary/TIJ7r9lXvNYSIMLI.htm)|(Siabrae) Stone Antlers|auto-trad|
|[tKixfds07IRH2nnh.htm](bestiary-family-ability-glossary/tKixfds07IRH2nnh.htm)|(Vampire, Vrykolakas Master) Children of the Night|auto-trad|
|[tQY2xmcIhzCM7oCC.htm](bestiary-family-ability-glossary/tQY2xmcIhzCM7oCC.htm)|(Melfesh Monster) Torrential Advance|auto-trad|
|[trYchslD8fLokkT9.htm](bestiary-family-ability-glossary/trYchslD8fLokkT9.htm)|(Mana Wastes Mutant) Hulking Form|auto-trad|
|[uDjn2b2ZrZycQQyv.htm](bestiary-family-ability-glossary/uDjn2b2ZrZycQQyv.htm)|(Cryptid, Experimental) Clobber|auto-trad|
|[UHuA1hue5xrRM6KK.htm](bestiary-family-ability-glossary/UHuA1hue5xrRM6KK.htm)|(Nymph Queen) Tied to the Land|auto-trad|
|[UlKGxPaKtRGWuq3V.htm](bestiary-family-ability-glossary/UlKGxPaKtRGWuq3V.htm)|(Divine Warden) Faithful Weapon|auto-trad|
|[uMedzgKYui5X3Qtn.htm](bestiary-family-ability-glossary/uMedzgKYui5X3Qtn.htm)|(Vampire, Nosferatu Overlord) Divine Innate Spells|auto-trad|
|[unR8VVR4yyRnsmnB.htm](bestiary-family-ability-glossary/unR8VVR4yyRnsmnB.htm)|(Ghost) Rejuvenation|auto-trad|
|[UOqq8lkBPcPtKoCN.htm](bestiary-family-ability-glossary/UOqq8lkBPcPtKoCN.htm)|(Melfesh Monster) Flame Lash|auto-trad|
|[uQ7cI0oerU7lDLhT.htm](bestiary-family-ability-glossary/uQ7cI0oerU7lDLhT.htm)|(Cryptid, Experimental) Power Surge|auto-trad|
|[UsWJ13sDyOgOGWvm.htm](bestiary-family-ability-glossary/UsWJ13sDyOgOGWvm.htm)|(Cryptid, Experimental) Augmented|auto-trad|
|[UU1Fp3PRuTFONjC9.htm](bestiary-family-ability-glossary/UU1Fp3PRuTFONjC9.htm)|(Ghoul) Swift Leap|auto-trad|
|[uV4jf2pkMRGLdhJX.htm](bestiary-family-ability-glossary/uV4jf2pkMRGLdhJX.htm)|(Ghost) Dreamwalker|auto-trad|
|[v6he7HLxYGzaMnvL.htm](bestiary-family-ability-glossary/v6he7HLxYGzaMnvL.htm)|(Zombie) Disgusting Pustules|auto-trad|
|[VLCECgjkL5bNOxpx.htm](bestiary-family-ability-glossary/VLCECgjkL5bNOxpx.htm)|(Ravener) Consume Soul|auto-trad|
|[vOocS1EiRCXPtbgB.htm](bestiary-family-ability-glossary/vOocS1EiRCXPtbgB.htm)|(Visitant) Roar|auto-trad|
|[vOvzh8XCMmvtW0Ws.htm](bestiary-family-ability-glossary/vOvzh8XCMmvtW0Ws.htm)|(Zombie, Shock) Electromechanical Phasing|auto-trad|
|[vX0CRZWok5ghasb3.htm](bestiary-family-ability-glossary/vX0CRZWok5ghasb3.htm)|(Secret Society) Prepared Diversion|auto-trad|
|[VXKiqiW7gXfFCz1U.htm](bestiary-family-ability-glossary/VXKiqiW7gXfFCz1U.htm)|(Melfesh Monster) Mycelial Tomb|auto-trad|
|[VzKVJlX2ocv1ezzp.htm](bestiary-family-ability-glossary/VzKVJlX2ocv1ezzp.htm)|(Visitant) Noxious Breath|auto-trad|
|[W5fD1ebH6Ri8HNzh.htm](bestiary-family-ability-glossary/W5fD1ebH6Ri8HNzh.htm)|(Graveknight) Create Grave Squire|auto-trad|
|[W77QhVNE46WDyMXi.htm](bestiary-family-ability-glossary/W77QhVNE46WDyMXi.htm)|(Skeleton) Explosive Death|auto-trad|
|[wdO3EvDCGoDYW44S.htm](bestiary-family-ability-glossary/wdO3EvDCGoDYW44S.htm)|(Melfesh Monster) Death Throes|auto-trad|
|[wh2T2L5SMsa32RyE.htm](bestiary-family-ability-glossary/wh2T2L5SMsa32RyE.htm)|(Lich) Cold Beyond Cold|auto-trad|
|[wJDfLmOJ2eJTmSwQ.htm](bestiary-family-ability-glossary/wJDfLmOJ2eJTmSwQ.htm)|(Mana Wastes Mutant) Increased Speed|auto-trad|
|[WLmFKSzR6Xz9RqAu.htm](bestiary-family-ability-glossary/WLmFKSzR6Xz9RqAu.htm)|(Cryptid, Mutant) Marrowlance|auto-trad|
|[WQjFPBQPDu6vyJny.htm](bestiary-family-ability-glossary/WQjFPBQPDu6vyJny.htm)|(Ghost) Corporeal Manifestation|auto-trad|
|[wqptDG0IW6ExnISC.htm](bestiary-family-ability-glossary/wqptDG0IW6ExnISC.htm)|(Vampire, True) Create Spawn|auto-trad|
|[WtRxZEPH963RkUCj.htm](bestiary-family-ability-glossary/WtRxZEPH963RkUCj.htm)|(Kuworsys) Rain Blows|auto-trad|
|[WUXY9F8SLVQSC2b8.htm](bestiary-family-ability-glossary/WUXY9F8SLVQSC2b8.htm)|(Siabrae) Stony Shards|auto-trad|
|[wzezkgOufQev7BLK.htm](bestiary-family-ability-glossary/wzezkgOufQev7BLK.htm)|(Ghost) Revenant|auto-trad|
|[X0KoXRDEJYYtwGgK.htm](bestiary-family-ability-glossary/X0KoXRDEJYYtwGgK.htm)|(Beheaded) Whispering|auto-trad|
|[X2mdpb1Dhl890YbA.htm](bestiary-family-ability-glossary/X2mdpb1Dhl890YbA.htm)|(Worm That Walks) Swarm Shape|auto-trad|
|[XbHMVjHtbPaPr9P5.htm](bestiary-family-ability-glossary/XbHMVjHtbPaPr9P5.htm)|(Ravener) Vicious Criticals|auto-trad|
|[XHut4MN0JBgm7WaN.htm](bestiary-family-ability-glossary/XHut4MN0JBgm7WaN.htm)|(Vampire, True) Drink Blood|auto-trad|
|[XpGCN9KJN0CCIlzU.htm](bestiary-family-ability-glossary/XpGCN9KJN0CCIlzU.htm)|(Cryptid, Rumored) Stalk|auto-trad|
|[xS8ybzuqPSi3Jb8k.htm](bestiary-family-ability-glossary/xS8ybzuqPSi3Jb8k.htm)|(Clockwork Creature) Wind-Up|auto-trad|
|[xuXloICuGtfA42AN.htm](bestiary-family-ability-glossary/xuXloICuGtfA42AN.htm)|(Clockwork Creature) Malfunction - Loose Screws|auto-trad|
|[XvPm866TKSfclErJ.htm](bestiary-family-ability-glossary/XvPm866TKSfclErJ.htm)|(Lich) Paralyzing Touch|auto-trad|
|[xwbiTvx3qyVEOqh6.htm](bestiary-family-ability-glossary/xwbiTvx3qyVEOqh6.htm)|(Vampire, Jiang-Shi, Minister) Dark Enlightenment|auto-trad|
|[XWtIvqCWmg8Tfr1N.htm](bestiary-family-ability-glossary/XWtIvqCWmg8Tfr1N.htm)|(Secret Society) Skill Savvy|auto-trad|
|[xxI7QpVWLiGjdu4B.htm](bestiary-family-ability-glossary/xxI7QpVWLiGjdu4B.htm)|(Cryptid, Experimental) Operational Flaw|auto-trad|
|[Y46uZK6X7J7iMsgq.htm](bestiary-family-ability-glossary/Y46uZK6X7J7iMsgq.htm)|(Ulgrem-Axaan) Crocodile Tears|auto-trad|
|[Y7cRjlRkdo3siz5y.htm](bestiary-family-ability-glossary/Y7cRjlRkdo3siz5y.htm)|(Coven) Locate Coven|auto-trad|
|[yaDe9hO9fm2N4SqH.htm](bestiary-family-ability-glossary/yaDe9hO9fm2N4SqH.htm)|(Spring-Heeled Jack) Resonant Terror|auto-trad|
|[yDE3ZEoxRUqQmAsX.htm](bestiary-family-ability-glossary/yDE3ZEoxRUqQmAsX.htm)|(Vampire, Nosferatu Overlord) Air of Sickness|auto-trad|
|[YELSFD2oTNMFkPJ2.htm](bestiary-family-ability-glossary/YELSFD2oTNMFkPJ2.htm)|(Secret Society) Shibboleth|auto-trad|
|[yfcqbMRmCkfPWV9O.htm](bestiary-family-ability-glossary/yfcqbMRmCkfPWV9O.htm)|(Ulgrem-Axaan) Crushing Weight|auto-trad|
|[yfNDFjz7VBvvLwee.htm](bestiary-family-ability-glossary/yfNDFjz7VBvvLwee.htm)|(Vampire, Vetalarana, Manipulator) Paralyzing Claws|auto-trad|
|[yLdiZ2AnjZ8KuT7v.htm](bestiary-family-ability-glossary/yLdiZ2AnjZ8KuT7v.htm)|(Skeleton) Bone Storm|auto-trad|
|[YQmf1Otd4xoxsSGU.htm](bestiary-family-ability-glossary/YQmf1Otd4xoxsSGU.htm)|(Mana Wastes Mutant) Caustic Pustules|auto-trad|
|[Yv0mQAeVuQ2Id9vk.htm](bestiary-family-ability-glossary/Yv0mQAeVuQ2Id9vk.htm)|(Mana Wastes Mutant) Too Many Eyes|auto-trad|
|[YVw626nVHlWwm4ej.htm](bestiary-family-ability-glossary/YVw626nVHlWwm4ej.htm)|(Skeleton) Crumbling Bones|auto-trad|
|[yWNcEsEJIoeXKBnk.htm](bestiary-family-ability-glossary/yWNcEsEJIoeXKBnk.htm)|(Lich) Void Shroud|auto-trad|
|[z9yqLBExOMk1y9cg.htm](bestiary-family-ability-glossary/z9yqLBExOMk1y9cg.htm)|(Cryptid, Primeval) Broken Arsenal|auto-trad|
|[zehifmU1fTeGs2ev.htm](bestiary-family-ability-glossary/zehifmU1fTeGs2ev.htm)|(Vampire, Strigoi) Levitation|auto-trad|
|[ZIFaA4jDQjM0vq8q.htm](bestiary-family-ability-glossary/ZIFaA4jDQjM0vq8q.htm)|(Graveknight) Rejuvenation|auto-trad|
|[zkjsfsweJmsB66CS.htm](bestiary-family-ability-glossary/zkjsfsweJmsB66CS.htm)|(Coven) Contribute Spell|auto-trad|
|[ZMTnwyXjyfy7Ryi6.htm](bestiary-family-ability-glossary/ZMTnwyXjyfy7Ryi6.htm)|(Vampire, True) Change Shape|auto-trad|
|[ZRHUnhaHK2w1el8f.htm](bestiary-family-ability-glossary/ZRHUnhaHK2w1el8f.htm)|(Skeleton) Lacquered|auto-trad|
|[Zu6feO9NUZJlsKuc.htm](bestiary-family-ability-glossary/Zu6feO9NUZJlsKuc.htm)|(Worm That Walks) Squirming Embrace|auto-trad|
|[ZUxt6s54TMgydXoW.htm](bestiary-family-ability-glossary/ZUxt6s54TMgydXoW.htm)|(Cryptid, Mutant) Shifting Iridescence|auto-trad|
|[zwgUxJBNFqWOuaBX.htm](bestiary-family-ability-glossary/zwgUxJBNFqWOuaBX.htm)|(Vampire, Vrykolakas Master) Dominate Animal|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[6isn9nqnvfRrC1wW.htm](bestiary-family-ability-glossary/6isn9nqnvfRrC1wW.htm)|(Graveknight) Weapon Master|(Graveknight) Maestro de armas|modificada|
|[haMVGGDY3mLuKy9s.htm](bestiary-family-ability-glossary/haMVGGDY3mLuKy9s.htm)|(Vampire, Vrykolakas Master) Bubonic Plague|(Vampiro, Maestro brucolaco) Peste bubónica|modificada|
|[nF7RKPONY5H9kEIo.htm](bestiary-family-ability-glossary/nF7RKPONY5H9kEIo.htm)|(Ghost) Memento Mori|(Fantasma) Memento Mori|modificada|
|[NSO2l1jXK32oTnVP.htm](bestiary-family-ability-glossary/NSO2l1jXK32oTnVP.htm)|(Kallas Devil) Freezing Touch|(Kallas Devil) Freezing Touch|modificada|
|[nxF03w5vKrw1jmxQ.htm](bestiary-family-ability-glossary/nxF03w5vKrw1jmxQ.htm)|(Lich) Familiar Soul|(Lich) Alma Familiar|modificada|
|[wEL7ZDMaSWSSYQG9.htm](bestiary-family-ability-glossary/wEL7ZDMaSWSSYQG9.htm)|(Vampire, Vetalarana, Manipulator) Drain Thoughts|(Vampiro, Vetalarana, Manipulador) Drenar Pensamientos|modificada|
|[wlhvSroB6r5cSd8Y.htm](bestiary-family-ability-glossary/wlhvSroB6r5cSd8Y.htm)|(Greater Barghest) Mutation - Poison Fangs|(Barghest mayor) Mutación - Colmillos venenosos|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
