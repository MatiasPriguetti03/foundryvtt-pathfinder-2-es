# Estado de la traducción (campaign-effects)

 * **auto-trad**: 21
 * **modificada**: 2
 * **ninguna**: 1


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[W8MD8WCjVC4pLlqn.htm](campaign-effects/W8MD8WCjVC4pLlqn.htm)|Effect: Heart Break|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[4vnF6BFM4Xg4Eg0k.htm](campaign-effects/4vnF6BFM4Xg4Eg0k.htm)|Effect: Improved reflexes|auto-trad|
|[5DBYunGL2HV8aekO.htm](campaign-effects/5DBYunGL2HV8aekO.htm)|Resonant Reflection: Reflection of Stone|auto-trad|
|[cESIlNrgjo1uW71D.htm](campaign-effects/cESIlNrgjo1uW71D.htm)|Mixed Drink: Guidance|auto-trad|
|[CS1XbH7GYfP6Ve61.htm](campaign-effects/CS1XbH7GYfP6Ve61.htm)|Effect: Burned Mouth and Throat (No Singing or Yelling)|auto-trad|
|[e8K0YTKxzf0bhayh.htm](campaign-effects/e8K0YTKxzf0bhayh.htm)|Mixed Drink: Luck|auto-trad|
|[j8hA7CsmSY90tBqw.htm](campaign-effects/j8hA7CsmSY90tBqw.htm)|Mixed Drink: Prestidigitation|auto-trad|
|[JnQi8whNqY7sPW7x.htm](campaign-effects/JnQi8whNqY7sPW7x.htm)|Mixed Drink: Dancing Lights|auto-trad|
|[Luti1qjFOKYwZio0.htm](campaign-effects/Luti1qjFOKYwZio0.htm)|Effect: Extreme stomach cramps|auto-trad|
|[lZ4DA81o4kbL8nve.htm](campaign-effects/lZ4DA81o4kbL8nve.htm)|Effect: Burned Tongue (Linguistic)|auto-trad|
|[meTIFa2VsIzRVywE.htm](campaign-effects/meTIFa2VsIzRVywE.htm)|Effect: Hope or Despair (Critical Success)|auto-trad|
|[NwF6m7SyaqwjdQxT.htm](campaign-effects/NwF6m7SyaqwjdQxT.htm)|Effect: Manipulate Luck - Drusilla (Good)|auto-trad|
|[nwFhhgZRggDoDgdk.htm](campaign-effects/nwFhhgZRggDoDgdk.htm)|Effect: Burned Mouth and Throat (Linguistic)|auto-trad|
|[Px7sSipQxHdOMSjk.htm](campaign-effects/Px7sSipQxHdOMSjk.htm)|Effect: Hope or Despair (Failure or Critical Failure)|auto-trad|
|[QWW4hNCtLkOK1k71.htm](campaign-effects/QWW4hNCtLkOK1k71.htm)|Effect: Immediate and Intense Headache|auto-trad|
|[Rz35d02dUtAjKtMB.htm](campaign-effects/Rz35d02dUtAjKtMB.htm)|Effect: Heightened awareness|auto-trad|
|[scgEAXAqHEr3Egeb.htm](campaign-effects/scgEAXAqHEr3Egeb.htm)|Resonant Reflection: Reflection of Water|auto-trad|
|[SvR7Ez1lfnN4You5.htm](campaign-effects/SvR7Ez1lfnN4You5.htm)|Geb's Blessing|auto-trad|
|[uDQw7YPMiKPXCbaV.htm](campaign-effects/uDQw7YPMiKPXCbaV.htm)|Resonant Reflection: Reflection of Light|auto-trad|
|[UXBOvlJbnI76UoGp.htm](campaign-effects/UXBOvlJbnI76UoGp.htm)|Resonant Reflection: Reflection of Storm|auto-trad|
|[XOE1gp3phFyxL4Dq.htm](campaign-effects/XOE1gp3phFyxL4Dq.htm)|Effect: Burned Tongue (No Singing or Yelling)|auto-trad|
|[yJWWTfZkAF4raa4R.htm](campaign-effects/yJWWTfZkAF4raa4R.htm)|Effect: Keen insight|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[8eMGYNilWV3dFAUI.htm](campaign-effects/8eMGYNilWV3dFAUI.htm)|Resonant Reflection: Reflection of Life|Reflejo Resonante: Reflejo de Vida|modificada|
|[DOxl0FDd21VMDOMP.htm](campaign-effects/DOxl0FDd21VMDOMP.htm)|Effect: Reflection of Life (Fast Healing)|Efecto: Reflejo de Vida (Curación Rápida)|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
