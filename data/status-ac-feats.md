# Estado de la traducción (ac-feats)

 * **auto-trad**: 21


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[4gNZCu2ncgu98A0H.htm](ac-feats/4gNZCu2ncgu98A0H.htm)|Shaitan-Touched Animal Companion|auto-trad|
|[89eT819I4MYWndcu.htm](ac-feats/89eT819I4MYWndcu.htm)|Nimble Animal Companion|auto-trad|
|[BMq4ZwfyZ4dbjjRR.htm](ac-feats/BMq4ZwfyZ4dbjjRR.htm)|Djinni-Touched Animal Companion|auto-trad|
|[c04kGEFIMMwsKCEP.htm](ac-feats/c04kGEFIMMwsKCEP.htm)|Specialized - Wind Chaser|auto-trad|
|[eoQnmkqWXaLwNQhG.htm](ac-feats/eoQnmkqWXaLwNQhG.htm)|Specialized - Deep Diver (Base Speed)|auto-trad|
|[GFDog9UQpe5pFolE.htm](ac-feats/GFDog9UQpe5pFolE.htm)|Efreeti-Touched Animal Companion|auto-trad|
|[h262IUT47dLqZLFZ.htm](ac-feats/h262IUT47dLqZLFZ.htm)|Savage Animal Companion|auto-trad|
|[IbW5X5iLlPfTPVRU.htm](ac-feats/IbW5X5iLlPfTPVRU.htm)|Marid-Touched Animal Companion|auto-trad|
|[K3c8ztEUX27RHeZe.htm](ac-feats/K3c8ztEUX27RHeZe.htm)|Mature Animal Companion|auto-trad|
|[KCbNpQElh8V0QX1Z.htm](ac-feats/KCbNpQElh8V0QX1Z.htm)|Specialized - Deep Diver (Speed Bonus)|auto-trad|
|[lm4QlSITABQKhx5K.htm](ac-feats/lm4QlSITABQKhx5K.htm)|Specialized - Shade|auto-trad|
|[mJhZ1znkGreEMjTn.htm](ac-feats/mJhZ1znkGreEMjTn.htm)|Unseen Animal Companion|auto-trad|
|[OUoQGBy9Gk3DSWWi.htm](ac-feats/OUoQGBy9Gk3DSWWi.htm)|Specialized - Bully|auto-trad|
|[Pa0h9IoePPvnEvUn.htm](ac-feats/Pa0h9IoePPvnEvUn.htm)|Specialized - Daredevil|auto-trad|
|[Sb8e7jIDTGghYvEk.htm](ac-feats/Sb8e7jIDTGghYvEk.htm)|Specialized - Wildfire Scorcher|auto-trad|
|[SiFB8P1upTvdhLpx.htm](ac-feats/SiFB8P1upTvdhLpx.htm)|Specialized - Steadfast Strider|auto-trad|
|[u9dXZwKANlsC9MDM.htm](ac-feats/u9dXZwKANlsC9MDM.htm)|Indomitable Animal Companion|auto-trad|
|[umbj0foct0eZ7Jcj.htm](ac-feats/umbj0foct0eZ7Jcj.htm)|Specialized - Tracker|auto-trad|
|[wDiFVxRBktQVj7uI.htm](ac-feats/wDiFVxRBktQVj7uI.htm)|Specialized - Racer|auto-trad|
|[wRzmLu4JR4A421So.htm](ac-feats/wRzmLu4JR4A421So.htm)|Specialized - Ambusher|auto-trad|
|[wZXL6VgEK88BCF0C.htm](ac-feats/wZXL6VgEK88BCF0C.htm)|Specialized - Wrecker|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
