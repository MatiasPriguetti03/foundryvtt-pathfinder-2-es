# Estado de la traducción (agents-of-edgewatch-bestiary)

 * **auto-trad**: 176
 * **modificada**: 11


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[07AGJt4ZRjwH85Xp.htm](agents-of-edgewatch-bestiary/07AGJt4ZRjwH85Xp.htm)|Mother Venom|auto-trad|
|[0ti3f4fdcB5D2bLB.htm](agents-of-edgewatch-bestiary/0ti3f4fdcB5D2bLB.htm)|Casino Bouncer|auto-trad|
|[0UbehYHzOGlNK8Hc.htm](agents-of-edgewatch-bestiary/0UbehYHzOGlNK8Hc.htm)|Baatamidar|auto-trad|
|[10fEM7T48FUZRo6l.htm](agents-of-edgewatch-bestiary/10fEM7T48FUZRo6l.htm)|Barnacle Ghoul|auto-trad|
|[181ucNY1zpp2Lz3x.htm](agents-of-edgewatch-bestiary/181ucNY1zpp2Lz3x.htm)|Grunka|auto-trad|
|[1bPc2rjR4MghbMwD.htm](agents-of-edgewatch-bestiary/1bPc2rjR4MghbMwD.htm)|Obrousian|auto-trad|
|[1eMDYXXf2leLTYHV.htm](agents-of-edgewatch-bestiary/1eMDYXXf2leLTYHV.htm)|Eberark|auto-trad|
|[1G4OdEHRPF8GMHK8.htm](agents-of-edgewatch-bestiary/1G4OdEHRPF8GMHK8.htm)|Amateur Chemist|auto-trad|
|[1UIJ4oknQ7btkUCb.htm](agents-of-edgewatch-bestiary/1UIJ4oknQ7btkUCb.htm)|Plunger Chute|auto-trad|
|[1UtsScuNjmgwMZRn.htm](agents-of-edgewatch-bestiary/1UtsScuNjmgwMZRn.htm)|Excorion Paragon|auto-trad|
|[1xGAktpj6N2Ugh0r.htm](agents-of-edgewatch-bestiary/1xGAktpj6N2Ugh0r.htm)|Shatterling|auto-trad|
|[3GNq4Vy40vk9zwMQ.htm](agents-of-edgewatch-bestiary/3GNq4Vy40vk9zwMQ.htm)|Graem|auto-trad|
|[3If8bjsXeA2413dB.htm](agents-of-edgewatch-bestiary/3If8bjsXeA2413dB.htm)|Hegessik|auto-trad|
|[3JEiwAFwkEjCDEYa.htm](agents-of-edgewatch-bestiary/3JEiwAFwkEjCDEYa.htm)|Kolo Harvan|auto-trad|
|[3LLHyUz0Nabuw46L.htm](agents-of-edgewatch-bestiary/3LLHyUz0Nabuw46L.htm)|Boiling Fountains|auto-trad|
|[3XbjmNeUtPLzxDge.htm](agents-of-edgewatch-bestiary/3XbjmNeUtPLzxDge.htm)|Penqual|auto-trad|
|[4BXo2a305RHmspMX.htm](agents-of-edgewatch-bestiary/4BXo2a305RHmspMX.htm)|Twisted Jack|auto-trad|
|[4ffoPNBKdEwBmYgL.htm](agents-of-edgewatch-bestiary/4ffoPNBKdEwBmYgL.htm)|Battle Leader Rekarek|auto-trad|
|[4TB1eo3O22khMyDU.htm](agents-of-edgewatch-bestiary/4TB1eo3O22khMyDU.htm)|Sad Liza|auto-trad|
|[54i6ithgmhDk7Dne.htm](agents-of-edgewatch-bestiary/54i6ithgmhDk7Dne.htm)|Rigged Cubby|auto-trad|
|[5pBr5aWUb7yGCqN7.htm](agents-of-edgewatch-bestiary/5pBr5aWUb7yGCqN7.htm)|Washboard Dog Tough|auto-trad|
|[5u7luLMeRNJ4en65.htm](agents-of-edgewatch-bestiary/5u7luLMeRNJ4en65.htm)|Bone Skipper Swarm|auto-trad|
|[64TX3n5xufgjqbwK.htm](agents-of-edgewatch-bestiary/64TX3n5xufgjqbwK.htm)|Poisoned Dart Statue|auto-trad|
|[6c5CnrSxMYEgP6Fz.htm](agents-of-edgewatch-bestiary/6c5CnrSxMYEgP6Fz.htm)|Kekker|auto-trad|
|[7bMhDg4DNqoPlRsF.htm](agents-of-edgewatch-bestiary/7bMhDg4DNqoPlRsF.htm)|Avarek|auto-trad|
|[7FJ3SQuHOUcGjm1x.htm](agents-of-edgewatch-bestiary/7FJ3SQuHOUcGjm1x.htm)|Eyeball Tank|auto-trad|
|[7LN8clGKJWTpxISR.htm](agents-of-edgewatch-bestiary/7LN8clGKJWTpxISR.htm)|Blackfingers Acolyte|auto-trad|
|[7Q35HATCilOb1xXX.htm](agents-of-edgewatch-bestiary/7Q35HATCilOb1xXX.htm)|Living Paints|auto-trad|
|[82wJQmzPlKttl0sc.htm](agents-of-edgewatch-bestiary/82wJQmzPlKttl0sc.htm)|Clockwork Arms|auto-trad|
|[8AdsRaXftoR1beTk.htm](agents-of-edgewatch-bestiary/8AdsRaXftoR1beTk.htm)|Frefferth|auto-trad|
|[8eBXEszbl4gOHGdU.htm](agents-of-edgewatch-bestiary/8eBXEszbl4gOHGdU.htm)|Wynsal Starborn|auto-trad|
|[8GQ7dq7s9CetOlkg.htm](agents-of-edgewatch-bestiary/8GQ7dq7s9CetOlkg.htm)|Gang Tough|auto-trad|
|[8HTPdiH6yEk0jlNF.htm](agents-of-edgewatch-bestiary/8HTPdiH6yEk0jlNF.htm)|Pickled Punk|auto-trad|
|[8jjEOs99Bmo1v0Qc.htm](agents-of-edgewatch-bestiary/8jjEOs99Bmo1v0Qc.htm)|Teraphant|auto-trad|
|[8LlqjWcqiFq7YMmQ.htm](agents-of-edgewatch-bestiary/8LlqjWcqiFq7YMmQ.htm)|Agent of the Gray Queen|auto-trad|
|[8PRqUfkHLvu9ufGL.htm](agents-of-edgewatch-bestiary/8PRqUfkHLvu9ufGL.htm)|Skinsaw Murderer|auto-trad|
|[8VAGXECLAk91hoAv.htm](agents-of-edgewatch-bestiary/8VAGXECLAk91hoAv.htm)|Field Of Opposition|auto-trad|
|[94yI4TrNag0jMaYy.htm](agents-of-edgewatch-bestiary/94yI4TrNag0jMaYy.htm)|Lie-Master|auto-trad|
|[9M1YJxTXqya55HDx.htm](agents-of-edgewatch-bestiary/9M1YJxTXqya55HDx.htm)|Copper Hand Rogue|auto-trad|
|[9vt9Dr2a8MkkD83z.htm](agents-of-edgewatch-bestiary/9vt9Dr2a8MkkD83z.htm)|Chalky|auto-trad|
|[a3pTQfLzsJThQvI9.htm](agents-of-edgewatch-bestiary/a3pTQfLzsJThQvI9.htm)|Calennia|auto-trad|
|[aDgVmO3afhIgXQSN.htm](agents-of-edgewatch-bestiary/aDgVmO3afhIgXQSN.htm)|Ravenile Rager|auto-trad|
|[ai7Q9vBHHAGj7uFE.htm](agents-of-edgewatch-bestiary/ai7Q9vBHHAGj7uFE.htm)|Svartalfar Killer|auto-trad|
|[aLhAofozzdTuqfcg.htm](agents-of-edgewatch-bestiary/aLhAofozzdTuqfcg.htm)|Acidic Poison Cloud Trap|auto-trad|
|[ApEn56YEz9xavgug.htm](agents-of-edgewatch-bestiary/ApEn56YEz9xavgug.htm)|Life Magnet|auto-trad|
|[AYNIAAxV7TbIKPI4.htm](agents-of-edgewatch-bestiary/AYNIAAxV7TbIKPI4.htm)|Skitterstitch|auto-trad|
|[AZ6AaYdx2NwLWSwh.htm](agents-of-edgewatch-bestiary/AZ6AaYdx2NwLWSwh.htm)|Blackfinger's Prayer|auto-trad|
|[B4kIfCimsz8wfc0k.htm](agents-of-edgewatch-bestiary/B4kIfCimsz8wfc0k.htm)|Lord Guirden|auto-trad|
|[b7AnzEzoMGFpM33z.htm](agents-of-edgewatch-bestiary/b7AnzEzoMGFpM33z.htm)|Ink Drowning Vats|auto-trad|
|[BCMkxoQB4xK4BniG.htm](agents-of-edgewatch-bestiary/BCMkxoQB4xK4BniG.htm)|Secret-Keeper|auto-trad|
|[BLRsSDFSMbZHcGDQ.htm](agents-of-edgewatch-bestiary/BLRsSDFSMbZHcGDQ.htm)|Black Whale Guard|auto-trad|
|[BoFg19e3N8WiNa3Z.htm](agents-of-edgewatch-bestiary/BoFg19e3N8WiNa3Z.htm)|The Stabbing Beast|auto-trad|
|[BuPf7xtqfwCjNOQv.htm](agents-of-edgewatch-bestiary/BuPf7xtqfwCjNOQv.htm)|Reginald Vancaskerkin|auto-trad|
|[Bv1s6xJ55HS3Gxgs.htm](agents-of-edgewatch-bestiary/Bv1s6xJ55HS3Gxgs.htm)|Grick|auto-trad|
|[ce2SoJ7nRNZ1AoK6.htm](agents-of-edgewatch-bestiary/ce2SoJ7nRNZ1AoK6.htm)|Hidden Chute|auto-trad|
|[cLlOUUpCIAQwUuOP.htm](agents-of-edgewatch-bestiary/cLlOUUpCIAQwUuOP.htm)|Il'setsya Wyrmtouched|auto-trad|
|[CLntGVs7cAIL9Trk.htm](agents-of-edgewatch-bestiary/CLntGVs7cAIL9Trk.htm)|Scathka|auto-trad|
|[cQMM2Ld0IBM9GcDo.htm](agents-of-edgewatch-bestiary/cQMM2Ld0IBM9GcDo.htm)|Norgorberite Poisoner|auto-trad|
|[CTKAJG7oQaIQWCiB.htm](agents-of-edgewatch-bestiary/CTKAJG7oQaIQWCiB.htm)|Exploding Statue|auto-trad|
|[D36nL0YjCVHfjBNw.htm](agents-of-edgewatch-bestiary/D36nL0YjCVHfjBNw.htm)|Clockwork Chopper|auto-trad|
|[D5UF1i0f1IRcQcNB.htm](agents-of-edgewatch-bestiary/D5UF1i0f1IRcQcNB.htm)|Ghaele Of Kharnas|auto-trad|
|[DKReNCapWWubM3pm.htm](agents-of-edgewatch-bestiary/DKReNCapWWubM3pm.htm)|Shikwashim Mercenary|auto-trad|
|[E279VPhAy1a4ihqI.htm](agents-of-edgewatch-bestiary/E279VPhAy1a4ihqI.htm)|Living Mural|auto-trad|
|[E7NEf3kmsY3YjRrz.htm](agents-of-edgewatch-bestiary/E7NEf3kmsY3YjRrz.htm)|Kapral|auto-trad|
|[e96H6PLW11NCRK1h.htm](agents-of-edgewatch-bestiary/e96H6PLW11NCRK1h.htm)|Greater Planar Rift|auto-trad|
|[EGQgqBfV80ll3pcf.htm](agents-of-edgewatch-bestiary/EGQgqBfV80ll3pcf.htm)|Chaos Gulgamodh|auto-trad|
|[eQc0ADMuHl1JzL8z.htm](agents-of-edgewatch-bestiary/eQc0ADMuHl1JzL8z.htm)|Shredskin|auto-trad|
|[eqwAdGsAk5JZKxUY.htm](agents-of-edgewatch-bestiary/eqwAdGsAk5JZKxUY.htm)|Gage Carlyle|auto-trad|
|[FmiOJ9HEdCBDB89z.htm](agents-of-edgewatch-bestiary/FmiOJ9HEdCBDB89z.htm)|Burning Chandelier Trap|auto-trad|
|[Fmsw7P5CF3uHtD5W.htm](agents-of-edgewatch-bestiary/Fmsw7P5CF3uHtD5W.htm)|Veksciralenix|auto-trad|
|[fV5VIoXMtixmI3Wc.htm](agents-of-edgewatch-bestiary/fV5VIoXMtixmI3Wc.htm)|Clockwork Assassin|auto-trad|
|[fvOjtzuRNpmpEHXA.htm](agents-of-edgewatch-bestiary/fvOjtzuRNpmpEHXA.htm)|Binumir|auto-trad|
|[gd0sVQtCHbhP8iHI.htm](agents-of-edgewatch-bestiary/gd0sVQtCHbhP8iHI.htm)|Grospek Lavarsus|auto-trad|
|[gFVazlIgCZivKzKF.htm](agents-of-edgewatch-bestiary/gFVazlIgCZivKzKF.htm)|Inky Tendrils|auto-trad|
|[gIcReNQOZceZBBlw.htm](agents-of-edgewatch-bestiary/gIcReNQOZceZBBlw.htm)|Jonis Flakfatter|auto-trad|
|[giVkjJeVZjhbR6eA.htm](agents-of-edgewatch-bestiary/giVkjJeVZjhbR6eA.htm)|Gas Trap|auto-trad|
|[gkRxUi9VrbPWOPGC.htm](agents-of-edgewatch-bestiary/gkRxUi9VrbPWOPGC.htm)|Daemonic Infector|auto-trad|
|[gsn4NsJLwZCQUwgf.htm](agents-of-edgewatch-bestiary/gsn4NsJLwZCQUwgf.htm)|Almiraj|auto-trad|
|[GWO6vweLGT2J6q62.htm](agents-of-edgewatch-bestiary/GWO6vweLGT2J6q62.htm)|Miriel Grayleaf|auto-trad|
|[GYhV5eYNDO1Llbv2.htm](agents-of-edgewatch-bestiary/GYhV5eYNDO1Llbv2.htm)|Venom Mage|auto-trad|
|[HF0ymYmKC6KydPQ1.htm](agents-of-edgewatch-bestiary/HF0ymYmKC6KydPQ1.htm)|Franca Laurentz|auto-trad|
|[HifZEgdCuZearOG2.htm](agents-of-edgewatch-bestiary/HifZEgdCuZearOG2.htm)|Clockwork Amalgam|auto-trad|
|[HXiA7x1jWnB1BqUy.htm](agents-of-edgewatch-bestiary/HXiA7x1jWnB1BqUy.htm)|Summoning Rune (Barbazu Devil)|auto-trad|
|[hzThZ50RRdfTYTKc.htm](agents-of-edgewatch-bestiary/hzThZ50RRdfTYTKc.htm)|Clockwork Poison Bomb|auto-trad|
|[IKfYWE1NLVNLCXZm.htm](agents-of-edgewatch-bestiary/IKfYWE1NLVNLCXZm.htm)|Blune's Illusory Toady|auto-trad|
|[JdMqHbaTwtOHVE7Y.htm](agents-of-edgewatch-bestiary/JdMqHbaTwtOHVE7Y.htm)|Diobel Sweeper Chemist|auto-trad|
|[JFcEtt18SGlb5uxm.htm](agents-of-edgewatch-bestiary/JFcEtt18SGlb5uxm.htm)|Supplicant Statues|auto-trad|
|[jgSS31hwrQ1n4jVF.htm](agents-of-edgewatch-bestiary/jgSS31hwrQ1n4jVF.htm)|Waxworks Onslaught Trap|auto-trad|
|[jJ1UTNLiRCoN1O3i.htm](agents-of-edgewatch-bestiary/jJ1UTNLiRCoN1O3i.htm)|Hendrid Pratchett|auto-trad|
|[JmHyHwaPMNKXzBts.htm](agents-of-edgewatch-bestiary/JmHyHwaPMNKXzBts.htm)|Zealborn|auto-trad|
|[JnNPdvOXtkGQHyfQ.htm](agents-of-edgewatch-bestiary/JnNPdvOXtkGQHyfQ.htm)|Gloaming Will-o'-Wisp|auto-trad|
|[JsyO7yBf6YaF4YwF.htm](agents-of-edgewatch-bestiary/JsyO7yBf6YaF4YwF.htm)|Daemonic Rumormonger|auto-trad|
|[JuCLvgvxYbSRXqON.htm](agents-of-edgewatch-bestiary/JuCLvgvxYbSRXqON.htm)|Copper Hand Illusionist|auto-trad|
|[JZUYQzQtzIwOGYvd.htm](agents-of-edgewatch-bestiary/JZUYQzQtzIwOGYvd.htm)|Ralso|auto-trad|
|[k1s8syi5E2FoR3Q3.htm](agents-of-edgewatch-bestiary/k1s8syi5E2FoR3Q3.htm)|Freezing Alarm|auto-trad|
|[kcvU9CatCyBUJRr2.htm](agents-of-edgewatch-bestiary/kcvU9CatCyBUJRr2.htm)|Rhevanna|auto-trad|
|[kidMmEzwgoBcHdnR.htm](agents-of-edgewatch-bestiary/kidMmEzwgoBcHdnR.htm)|The Laughing Fiend's Greeting|auto-trad|
|[l3AqEkQwRJS8TY7f.htm](agents-of-edgewatch-bestiary/l3AqEkQwRJS8TY7f.htm)|Canopy Drop|auto-trad|
|[LAamprMlzk7k5auj.htm](agents-of-edgewatch-bestiary/LAamprMlzk7k5auj.htm)|Hestriviniaas|auto-trad|
|[LACpbwnVT7m2ZqBi.htm](agents-of-edgewatch-bestiary/LACpbwnVT7m2ZqBi.htm)|The Winder|auto-trad|
|[LAun416if9NFg3X2.htm](agents-of-edgewatch-bestiary/LAun416if9NFg3X2.htm)|Explosive Barrels|auto-trad|
|[lgbJbeSeC1f8otvH.htm](agents-of-edgewatch-bestiary/lgbJbeSeC1f8otvH.htm)|Daemonic Skinner|auto-trad|
|[LkaB8RH73DY4TO9V.htm](agents-of-edgewatch-bestiary/LkaB8RH73DY4TO9V.htm)|Priest of Blackfingers|auto-trad|
|[lzmGdArS3kjJOqT6.htm](agents-of-edgewatch-bestiary/lzmGdArS3kjJOqT6.htm)|Lyrma Swampwalker|auto-trad|
|[MfW7O5Ba8r2GR9ZQ.htm](agents-of-edgewatch-bestiary/MfW7O5Ba8r2GR9ZQ.htm)|Sordesdaemon|auto-trad|
|[MONwgTbrcZFzr6vC.htm](agents-of-edgewatch-bestiary/MONwgTbrcZFzr6vC.htm)|Antaro Boldblade|auto-trad|
|[MqfZxoxFwzqAXhTP.htm](agents-of-edgewatch-bestiary/MqfZxoxFwzqAXhTP.htm)|Prospecti Statue|auto-trad|
|[MQnhyCM9LInNYtl0.htm](agents-of-edgewatch-bestiary/MQnhyCM9LInNYtl0.htm)|Carvey|auto-trad|
|[N1IpUIW2Ry7uBN3G.htm](agents-of-edgewatch-bestiary/N1IpUIW2Ry7uBN3G.htm)|Tiderunner Aquamancer|auto-trad|
|[nfULZbf8OOUDdrZM.htm](agents-of-edgewatch-bestiary/nfULZbf8OOUDdrZM.htm)|Water Elemental Vessel|auto-trad|
|[nTn2szBbqQNdXhOr.htm](agents-of-edgewatch-bestiary/nTn2szBbqQNdXhOr.htm)|Skebs|auto-trad|
|[oFCgiGFipWeit9sl.htm](agents-of-edgewatch-bestiary/oFCgiGFipWeit9sl.htm)|Lusca|auto-trad|
|[oTa25rAxytm03T3X.htm](agents-of-edgewatch-bestiary/oTa25rAxytm03T3X.htm)|Shristi Melipdra|auto-trad|
|[oWASKud0jwlGSfJg.htm](agents-of-edgewatch-bestiary/oWASKud0jwlGSfJg.htm)|Pelmo|auto-trad|
|[P9Wg0sGcNkemOvm3.htm](agents-of-edgewatch-bestiary/P9Wg0sGcNkemOvm3.htm)|Slithering Rift|auto-trad|
|[pE5GxoB1FtXBqnF7.htm](agents-of-edgewatch-bestiary/pE5GxoB1FtXBqnF7.htm)|Gref|auto-trad|
|[PEjcy9CxelKC3Kp6.htm](agents-of-edgewatch-bestiary/PEjcy9CxelKC3Kp6.htm)|Zrukbat|auto-trad|
|[pfHOwcITyC4gdCVu.htm](agents-of-edgewatch-bestiary/pfHOwcITyC4gdCVu.htm)|Garrote Master Assassin|auto-trad|
|[phkvSUK6WXxgJOoC.htm](agents-of-edgewatch-bestiary/phkvSUK6WXxgJOoC.htm)|Alchemist Aspirant|auto-trad|
|[pK1tlsgkmzkaaCe5.htm](agents-of-edgewatch-bestiary/pK1tlsgkmzkaaCe5.htm)|Iroran Skeleton|auto-trad|
|[pTkvg8OITTl6lsJY.htm](agents-of-edgewatch-bestiary/pTkvg8OITTl6lsJY.htm)|Minchgorm|auto-trad|
|[q61A6EsMnDDtnqxH.htm](agents-of-edgewatch-bestiary/q61A6EsMnDDtnqxH.htm)|Ulressia The Blessed|auto-trad|
|[QBlBYsxySiBMxf22.htm](agents-of-edgewatch-bestiary/QBlBYsxySiBMxf22.htm)|Zeal-damned Ghoul|auto-trad|
|[QHcBauLnHzfwkDBK.htm](agents-of-edgewatch-bestiary/QHcBauLnHzfwkDBK.htm)|Summoning Rune (Cinder Rat)|auto-trad|
|[QQkbvOCif8Bm1wws.htm](agents-of-edgewatch-bestiary/QQkbvOCif8Bm1wws.htm)|Izfiitar|auto-trad|
|[QRcIVhQV2vlpADSf.htm](agents-of-edgewatch-bestiary/QRcIVhQV2vlpADSf.htm)|Camarach|auto-trad|
|[qSL6PtsHPekEEEjx.htm](agents-of-edgewatch-bestiary/qSL6PtsHPekEEEjx.htm)|Overdrive Imentesh|auto-trad|
|[qUhxXE2yT0qwSJQm.htm](agents-of-edgewatch-bestiary/qUhxXE2yT0qwSJQm.htm)|Agradaemon|auto-trad|
|[qY1NrXKmL0y18qoz.htm](agents-of-edgewatch-bestiary/qY1NrXKmL0y18qoz.htm)|Bolar Of Stonemoor|auto-trad|
|[qy53ECS2agScE7G3.htm](agents-of-edgewatch-bestiary/qy53ECS2agScE7G3.htm)|Kharnas's Lesser Glyph|auto-trad|
|[r3j5KEvULFP3fZS7.htm](agents-of-edgewatch-bestiary/r3j5KEvULFP3fZS7.htm)|Grimwold|auto-trad|
|[rAUaIxp3QFgT3bzl.htm](agents-of-edgewatch-bestiary/rAUaIxp3QFgT3bzl.htm)|Needling Stairs|auto-trad|
|[rDYDTCVUa5GS3uTE.htm](agents-of-edgewatch-bestiary/rDYDTCVUa5GS3uTE.htm)|Iron Maiden Trap|auto-trad|
|[rGTq4qItRB5H7nEk.htm](agents-of-edgewatch-bestiary/rGTq4qItRB5H7nEk.htm)|Graveknight Of Kharnas|auto-trad|
|[Rinxhe1cRXKEsXuW.htm](agents-of-edgewatch-bestiary/Rinxhe1cRXKEsXuW.htm)|Tyrroicese|auto-trad|
|[rm0iJOMwruWSE93I.htm](agents-of-edgewatch-bestiary/rm0iJOMwruWSE93I.htm)|Olansa Terimor|auto-trad|
|[rM6ix6XTroJod3Vr.htm](agents-of-edgewatch-bestiary/rM6ix6XTroJod3Vr.htm)|Fayati Alummur|auto-trad|
|[rsKf8ixrl3yBq1gb.htm](agents-of-edgewatch-bestiary/rsKf8ixrl3yBq1gb.htm)|Starwatch Commando|auto-trad|
|[RtWlzHaOrfFdJyJY.htm](agents-of-edgewatch-bestiary/RtWlzHaOrfFdJyJY.htm)|Alchemical Horror|auto-trad|
|[rXePwiS4iecWNMGU.htm](agents-of-edgewatch-bestiary/rXePwiS4iecWNMGU.htm)|Grinlowe|auto-trad|
|[RyXA4wOGY8lenKVw.htm](agents-of-edgewatch-bestiary/RyXA4wOGY8lenKVw.htm)|Nenchuuj|auto-trad|
|[S9JJsUNSeeoIClON.htm](agents-of-edgewatch-bestiary/S9JJsUNSeeoIClON.htm)|Poison Eater|auto-trad|
|[SgkV5RtcK72d0HwI.htm](agents-of-edgewatch-bestiary/SgkV5RtcK72d0HwI.htm)|Excorion|auto-trad|
|[sn9Pjkr2jlMEqc3E.htm](agents-of-edgewatch-bestiary/sn9Pjkr2jlMEqc3E.htm)|Eunice|auto-trad|
|[sUrJ7jxzBiJTbwVo.htm](agents-of-edgewatch-bestiary/sUrJ7jxzBiJTbwVo.htm)|Blune Bandersworth|auto-trad|
|[syUXLdUsEDYgni5R.htm](agents-of-edgewatch-bestiary/syUXLdUsEDYgni5R.htm)|Wrent Dicaspiron|auto-trad|
|[T6RsavB4ZZGdlNuA.htm](agents-of-edgewatch-bestiary/T6RsavB4ZZGdlNuA.htm)|Hands Of The Forgotten|auto-trad|
|[t9m4ikMZsDwo9TQ1.htm](agents-of-edgewatch-bestiary/t9m4ikMZsDwo9TQ1.htm)|Maurrisa Jonne|auto-trad|
|[TIaZIUb9Mq9B4Mf2.htm](agents-of-edgewatch-bestiary/TIaZIUb9Mq9B4Mf2.htm)|Bloody Berleth|auto-trad|
|[tm8vQ7gdAe9zVdDg.htm](agents-of-edgewatch-bestiary/tm8vQ7gdAe9zVdDg.htm)|Vaultbreaker Ooze|auto-trad|
|[u1cuwAE3xzhYW4Mi.htm](agents-of-edgewatch-bestiary/u1cuwAE3xzhYW4Mi.htm)|False Door Trap|auto-trad|
|[uFU6dQfcNeKq68YT.htm](agents-of-edgewatch-bestiary/uFU6dQfcNeKq68YT.htm)|Vargouille|auto-trad|
|[ukL9sApDCIWsVL64.htm](agents-of-edgewatch-bestiary/ukL9sApDCIWsVL64.htm)|Boiling Tub Trap|auto-trad|
|[UkmM3bjBoBld0uzS.htm](agents-of-edgewatch-bestiary/UkmM3bjBoBld0uzS.htm)|Flying Guillotine|auto-trad|
|[vPMmTtvl5UPOcCoa.htm](agents-of-edgewatch-bestiary/vPMmTtvl5UPOcCoa.htm)|Myrna Rath|auto-trad|
|[vwxNCuBksHYU2Dwf.htm](agents-of-edgewatch-bestiary/vwxNCuBksHYU2Dwf.htm)|Hundun Chaos Mage|auto-trad|
|[w2J6GpuMYM24U4sb.htm](agents-of-edgewatch-bestiary/w2J6GpuMYM24U4sb.htm)|Clockwork Injector|auto-trad|
|[Wb4Md6byPhBWe56J.htm](agents-of-edgewatch-bestiary/Wb4Md6byPhBWe56J.htm)|Cobbleswarm (AoE)|auto-trad|
|[WDTdWiC9Rdl6rqh8.htm](agents-of-edgewatch-bestiary/WDTdWiC9Rdl6rqh8.htm)|Myrucarx|auto-trad|
|[WplBGSeB9pK9AULX.htm](agents-of-edgewatch-bestiary/WplBGSeB9pK9AULX.htm)|The Rabbit Prince|auto-trad|
|[wsVW8MdOTeGgGM59.htm](agents-of-edgewatch-bestiary/wsVW8MdOTeGgGM59.htm)|Child Of Venom|auto-trad|
|[WXwvWHRpwK17YABQ.htm](agents-of-edgewatch-bestiary/WXwvWHRpwK17YABQ.htm)|Bregdi|auto-trad|
|[Xi53GFvTgBApltjp.htm](agents-of-edgewatch-bestiary/Xi53GFvTgBApltjp.htm)|Giant Bone Skipper|auto-trad|
|[xkkj0TW6BKNT3Bg4.htm](agents-of-edgewatch-bestiary/xkkj0TW6BKNT3Bg4.htm)|Diobel Sweeper Tough|auto-trad|
|[xN3FDmrCKWW0psBu.htm](agents-of-edgewatch-bestiary/xN3FDmrCKWW0psBu.htm)|Sleepless Sun Veteran|auto-trad|
|[XTHcALqbg5kgxtPw.htm](agents-of-edgewatch-bestiary/XTHcALqbg5kgxtPw.htm)|Arcane Feedback Trap|auto-trad|
|[xw7S8108irh2D1Uw.htm](agents-of-edgewatch-bestiary/xw7S8108irh2D1Uw.htm)|Dart Barrage|auto-trad|
|[y0bIU9FCWHOJxUzG.htm](agents-of-edgewatch-bestiary/y0bIU9FCWHOJxUzG.htm)|Bloody Barber Goon|auto-trad|
|[y1tw2ohNagqQJ6RV.htm](agents-of-edgewatch-bestiary/y1tw2ohNagqQJ6RV.htm)|Skinsaw Seamer|auto-trad|
|[yR6p0KVvZ3tPflRt.htm](agents-of-edgewatch-bestiary/yR6p0KVvZ3tPflRt.htm)|Kemeneles|auto-trad|
|[ySpOZlKUbcxWhKQ6.htm](agents-of-edgewatch-bestiary/ySpOZlKUbcxWhKQ6.htm)|Ofalth Zombie|auto-trad|
|[Z6R8YjgX8Jvt9Ds4.htm](agents-of-edgewatch-bestiary/Z6R8YjgX8Jvt9Ds4.htm)|Avsheros the Betrayer|auto-trad|
|[zj2sCM8tQSMG9Qm6.htm](agents-of-edgewatch-bestiary/zj2sCM8tQSMG9Qm6.htm)|Najra Lizard|auto-trad|
|[ZL2qLXwomKfBB8Eu.htm](agents-of-edgewatch-bestiary/ZL2qLXwomKfBB8Eu.htm)|Dreadsong Dancer|auto-trad|
|[zrh3MrS68H2gPlVs.htm](agents-of-edgewatch-bestiary/zrh3MrS68H2gPlVs.htm)|Tenome|auto-trad|
|[ZY3q7AV1qbwWwNl2.htm](agents-of-edgewatch-bestiary/ZY3q7AV1qbwWwNl2.htm)|Mobana|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[2uxm1SxZXaG0ynCp.htm](agents-of-edgewatch-bestiary/2uxm1SxZXaG0ynCp.htm)|Pulping Golem|Golem despulpador|modificada|
|[azyIfDNNW44jY8YX.htm](agents-of-edgewatch-bestiary/azyIfDNNW44jY8YX.htm)|Barrel Launcher|Barrel Launcher|modificada|
|[d3TzpCuRJF78xHZK.htm](agents-of-edgewatch-bestiary/d3TzpCuRJF78xHZK.htm)|Black Whale Guard (F3)|Guardia de la ballena negra (F3, garrote de pesadilla)|modificada|
|[H4bY8v6e3drOIoUe.htm](agents-of-edgewatch-bestiary/H4bY8v6e3drOIoUe.htm)|Grabble Forden|Grabble Forden|modificada|
|[JhYJUNlcxiurcZcl.htm](agents-of-edgewatch-bestiary/JhYJUNlcxiurcZcl.htm)|Water Elemental Vessel (I2)|Recipiente elemental de agua (I2)|modificada|
|[M8ONVV7yl4uu0zcz.htm](agents-of-edgewatch-bestiary/M8ONVV7yl4uu0zcz.htm)|Ixusoth|Ixusoth|modificada|
|[Sq0Kb92nGkqj19Xx.htm](agents-of-edgewatch-bestiary/Sq0Kb92nGkqj19Xx.htm)|Miogimo|Miogimo|modificada|
|[tt5eaS28C4PrHmZD.htm](agents-of-edgewatch-bestiary/tt5eaS28C4PrHmZD.htm)|The Inkmaster|El Inkmaster|modificada|
|[Uhi3wX4KveuMSARt.htm](agents-of-edgewatch-bestiary/Uhi3wX4KveuMSARt.htm)|Giant Joro Spider|Araña gigante Joro|modificada|
|[Wk2T0Wr8Sebo4br5.htm](agents-of-edgewatch-bestiary/Wk2T0Wr8Sebo4br5.htm)|Mr. Snips|Mr. Snips|modificada|
|[XDt87cqF85zWnlC8.htm](agents-of-edgewatch-bestiary/XDt87cqF85zWnlC8.htm)|Siege Shard|Esquirla de asedio|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
