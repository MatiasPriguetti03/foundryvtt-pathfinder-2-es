# Estado de la traducción (npc-gallery)

 * **auto-trad**: 99


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0Ex7rBuiJVu2NwCz.htm](npc-gallery/0Ex7rBuiJVu2NwCz.htm)|Demonologist|auto-trad|
|[0Kb4z4h8KVqfrIju.htm](npc-gallery/0Kb4z4h8KVqfrIju.htm)|Assassin|auto-trad|
|[1iz7O6DLDJqStojd.htm](npc-gallery/1iz7O6DLDJqStojd.htm)|Servant|auto-trad|
|[1NZ1ZAgcUlWKmQSs.htm](npc-gallery/1NZ1ZAgcUlWKmQSs.htm)|Bosun|auto-trad|
|[1sWw5OgmpazLVqRQ.htm](npc-gallery/1sWw5OgmpazLVqRQ.htm)|Mage For Hire|auto-trad|
|[1U1URD7IyddoD5zE.htm](npc-gallery/1U1URD7IyddoD5zE.htm)|Fence|auto-trad|
|[2SBKFCog4JY3WrOW.htm](npc-gallery/2SBKFCog4JY3WrOW.htm)|Rain-Scribe|auto-trad|
|[3lZhmvNLQkiYGAof.htm](npc-gallery/3lZhmvNLQkiYGAof.htm)|Sage|auto-trad|
|[401MnHX5aO21P2Y8.htm](npc-gallery/401MnHX5aO21P2Y8.htm)|Stone Giant Monk|auto-trad|
|[48bZvtRcd7T6FmA7.htm](npc-gallery/48bZvtRcd7T6FmA7.htm)|Privateer Captain|auto-trad|
|[51E3fdESgGjQxcMv.htm](npc-gallery/51E3fdESgGjQxcMv.htm)|Astronomer|auto-trad|
|[5LvpvIMhaYLcyAk6.htm](npc-gallery/5LvpvIMhaYLcyAk6.htm)|Pirate|auto-trad|
|[5Rqh2dBxGU8Jwf56.htm](npc-gallery/5Rqh2dBxGU8Jwf56.htm)|Azarketi Sailor|auto-trad|
|[6IADTZHYowxObqAk.htm](npc-gallery/6IADTZHYowxObqAk.htm)|Barkeep|auto-trad|
|[7eJJIIVEDB3EFFcZ.htm](npc-gallery/7eJJIIVEDB3EFFcZ.htm)|Bodyguard|auto-trad|
|[7GGHuOlSzcaF2AdL.htm](npc-gallery/7GGHuOlSzcaF2AdL.htm)|Charlatan|auto-trad|
|[7RF95b3WHkvHWLrv.htm](npc-gallery/7RF95b3WHkvHWLrv.htm)|Physician|auto-trad|
|[8coHofIpLa5ZnjAF.htm](npc-gallery/8coHofIpLa5ZnjAF.htm)|Navigator|auto-trad|
|[8WFGygPv7UHh7zdJ.htm](npc-gallery/8WFGygPv7UHh7zdJ.htm)|Apothecary|auto-trad|
|[95IcOUvxABvj5lvo.htm](npc-gallery/95IcOUvxABvj5lvo.htm)|Changeling Hellknight|auto-trad|
|[9jDT7EhtlZtNpCz7.htm](npc-gallery/9jDT7EhtlZtNpCz7.htm)|False Priest|auto-trad|
|[aJR3f8YcAkmfx7im.htm](npc-gallery/aJR3f8YcAkmfx7im.htm)|Apprentice|auto-trad|
|[Ap87yR4WOs0wKHx7.htm](npc-gallery/Ap87yR4WOs0wKHx7.htm)|Cultist|auto-trad|
|[B09JfuBZHjcRXztU.htm](npc-gallery/B09JfuBZHjcRXztU.htm)|Burglar|auto-trad|
|[B0pZAGooj735FGfw.htm](npc-gallery/B0pZAGooj735FGfw.htm)|Tomb Raider|auto-trad|
|[B13dyXTo1xWVyj2R.htm](npc-gallery/B13dyXTo1xWVyj2R.htm)|Palace Guard|auto-trad|
|[bc1jeTvmzKeYGVw9.htm](npc-gallery/bc1jeTvmzKeYGVw9.htm)|Virtuous Defender|auto-trad|
|[cAOWcPIjtZYXYZ3i.htm](npc-gallery/cAOWcPIjtZYXYZ3i.htm)|Merchant|auto-trad|
|[Cnm5zWmuTEYy6mPx.htm](npc-gallery/Cnm5zWmuTEYy6mPx.htm)|Mastermind|auto-trad|
|[crTWewxna93vEt6B.htm](npc-gallery/crTWewxna93vEt6B.htm)|Acolyte of Nethys|auto-trad|
|[DD2JNeRxO79WFlOL.htm](npc-gallery/DD2JNeRxO79WFlOL.htm)|Archer Sentry|auto-trad|
|[DFurZlcpcNrUmmER.htm](npc-gallery/DFurZlcpcNrUmmER.htm)|Ruffian|auto-trad|
|[DSA03902sWGot0ev.htm](npc-gallery/DSA03902sWGot0ev.htm)|Miner|auto-trad|
|[EMl8hARVJk8SNVyW.htm](npc-gallery/EMl8hARVJk8SNVyW.htm)|Charming Scoundrel|auto-trad|
|[EslFhpdvQf7KN8W3.htm](npc-gallery/EslFhpdvQf7KN8W3.htm)|Chronicler|auto-trad|
|[EzD6YlNXL48rN8nq.htm](npc-gallery/EzD6YlNXL48rN8nq.htm)|Despot|auto-trad|
|[F8AzuPOCcveWasza.htm](npc-gallery/F8AzuPOCcveWasza.htm)|Ethereal Sailor|auto-trad|
|[G2ftdkyJ5WDonL0C.htm](npc-gallery/G2ftdkyJ5WDonL0C.htm)|Urchin|auto-trad|
|[GoGNtiHuYycppLPk.htm](npc-gallery/GoGNtiHuYycppLPk.htm)|Bounty Hunter|auto-trad|
|[GRtEwNQgKQ9j9JPK.htm](npc-gallery/GRtEwNQgKQ9j9JPK.htm)|Warden|auto-trad|
|[gzirsGA07yG6CaG8.htm](npc-gallery/gzirsGA07yG6CaG8.htm)|Jailer|auto-trad|
|[hK8Tpg3baKWzmPEv.htm](npc-gallery/hK8Tpg3baKWzmPEv.htm)|Tax Collector|auto-trad|
|[Hle05FibgOeZr7wF.htm](npc-gallery/Hle05FibgOeZr7wF.htm)|Hunter|auto-trad|
|[hxyImo4ts3O0BrAY.htm](npc-gallery/hxyImo4ts3O0BrAY.htm)|Veteran Reclaimer|auto-trad|
|[IaSxoVNZFYatdfjI.htm](npc-gallery/IaSxoVNZFYatdfjI.htm)|Drunkard|auto-trad|
|[ImdKLPgazv4MSI0F.htm](npc-gallery/ImdKLPgazv4MSI0F.htm)|Barrister|auto-trad|
|[IQJT1Bg9FhvHHEap.htm](npc-gallery/IQJT1Bg9FhvHHEap.htm)|Torchbearer|auto-trad|
|[Jg9OEmo68KC91PgC.htm](npc-gallery/Jg9OEmo68KC91PgC.htm)|Teacher|auto-trad|
|[JsTI2SEZdg2j03gf.htm](npc-gallery/JsTI2SEZdg2j03gf.htm)|Beggar|auto-trad|
|[K2STan8izudm9eEn.htm](npc-gallery/K2STan8izudm9eEn.htm)|Antipaladin|auto-trad|
|[K8mtLJ5jgxfqxTCv.htm](npc-gallery/K8mtLJ5jgxfqxTCv.htm)|Harrow Reader|auto-trad|
|[kmHYc2fvhd4QsUEV.htm](npc-gallery/kmHYc2fvhd4QsUEV.htm)|Necromancer|auto-trad|
|[KPUDfkVpemug2gKj.htm](npc-gallery/KPUDfkVpemug2gKj.htm)|Bandit|auto-trad|
|[KUDsYCHduF0JE3yf.htm](npc-gallery/KUDsYCHduF0JE3yf.htm)|Ship Captain|auto-trad|
|[KvcFqH6H4TFCuBZA.htm](npc-gallery/KvcFqH6H4TFCuBZA.htm)|Azarketi Crab Catcher|auto-trad|
|[ldaY3QcPczuFoqBC.htm](npc-gallery/ldaY3QcPczuFoqBC.htm)|Plague Doctor|auto-trad|
|[lemVxzg2Pnx9Nu3d.htm](npc-gallery/lemVxzg2Pnx9Nu3d.htm)|Troubadour|auto-trad|
|[lfXQECIiN0zZdf95.htm](npc-gallery/lfXQECIiN0zZdf95.htm)|Dancer|auto-trad|
|[lTcX8tk6JjQBFcq1.htm](npc-gallery/lTcX8tk6JjQBFcq1.htm)|Zealot of Asmodeus|auto-trad|
|[M2Vi2mkwMZv1ZRka.htm](npc-gallery/M2Vi2mkwMZv1ZRka.htm)|Tempest-Sun Mage|auto-trad|
|[mblLgQ9NMR2mMI99.htm](npc-gallery/mblLgQ9NMR2mMI99.htm)|Reckless Scientist|auto-trad|
|[mJxgYD8TQg1W2oXC.htm](npc-gallery/mJxgYD8TQg1W2oXC.htm)|Hellknight Paravicar|auto-trad|
|[ny37LcdsPLY9Osby.htm](npc-gallery/ny37LcdsPLY9Osby.htm)|Saboteur|auto-trad|
|[o4XTf77fEEoFVTdA.htm](npc-gallery/o4XTf77fEEoFVTdA.htm)|Pathfinder Venture-Captain|auto-trad|
|[o9dAbSVn4Vi4ejjc.htm](npc-gallery/o9dAbSVn4Vi4ejjc.htm)|Smith|auto-trad|
|[OSCpJYTr6xNIxqZi.htm](npc-gallery/OSCpJYTr6xNIxqZi.htm)|Server|auto-trad|
|[p94aKz7KsiAQJscm.htm](npc-gallery/p94aKz7KsiAQJscm.htm)|Prisoner|auto-trad|
|[PLOfWPKwB7pE4arv.htm](npc-gallery/PLOfWPKwB7pE4arv.htm)|Innkeeper|auto-trad|
|[pMKrTXmrzDOc9avN.htm](npc-gallery/pMKrTXmrzDOc9avN.htm)|Cult Leader|auto-trad|
|[PoIuzIWFnlAQ8pdH.htm](npc-gallery/PoIuzIWFnlAQ8pdH.htm)|Beast Tamer|auto-trad|
|[pZOgcQRwXrX9g0s8.htm](npc-gallery/pZOgcQRwXrX9g0s8.htm)|Guildmaster|auto-trad|
|[QAodADCKmbkf53CE.htm](npc-gallery/QAodADCKmbkf53CE.htm)|Librarian|auto-trad|
|[QZmckb7O3PNgY7D6.htm](npc-gallery/QZmckb7O3PNgY7D6.htm)|Dockhand|auto-trad|
|[R5SWtNtQt8l7WLYk.htm](npc-gallery/R5SWtNtQt8l7WLYk.htm)|Noble|auto-trad|
|[rsATu823vatRe7QJ.htm](npc-gallery/rsATu823vatRe7QJ.htm)|Guard|auto-trad|
|[saUg5rtaO9kI9Vir.htm](npc-gallery/saUg5rtaO9kI9Vir.htm)|Harbormaster|auto-trad|
|[sKSfQmJMEsj8QN12.htm](npc-gallery/sKSfQmJMEsj8QN12.htm)|Adept|auto-trad|
|[SwjcZsbkcq6PhiXc.htm](npc-gallery/SwjcZsbkcq6PhiXc.htm)|Spy|auto-trad|
|[sZ9RwN8zIzpztW3N.htm](npc-gallery/sZ9RwN8zIzpztW3N.htm)|Azarketi Tide Tamer|auto-trad|
|[t7QwdZ2m7AbuRWqd.htm](npc-gallery/t7QwdZ2m7AbuRWqd.htm)|Captain Of The Guard|auto-trad|
|[tbiThWX0gAVZAMor.htm](npc-gallery/tbiThWX0gAVZAMor.htm)|Judge|auto-trad|
|[TCzxsJQjUpy02CsJ.htm](npc-gallery/TCzxsJQjUpy02CsJ.htm)|Tracker|auto-trad|
|[TgeUj2IiyoTeZHIO.htm](npc-gallery/TgeUj2IiyoTeZHIO.htm)|Watch Officer|auto-trad|
|[Tj03FbN4SSr0o953.htm](npc-gallery/Tj03FbN4SSr0o953.htm)|Acrobat|auto-trad|
|[u3tXaX3sOtCvuHW3.htm](npc-gallery/u3tXaX3sOtCvuHW3.htm)|Farmer|auto-trad|
|[UuPPceVcGk1RwSbB.htm](npc-gallery/UuPPceVcGk1RwSbB.htm)|Hellknight Armiger|auto-trad|
|[VkG5yl9xcmziwpQD.htm](npc-gallery/VkG5yl9xcmziwpQD.htm)|Pathfinder Field Agent|auto-trad|
|[vkLhqX5oR1t89puZ.htm](npc-gallery/vkLhqX5oR1t89puZ.htm)|Gang Leader|auto-trad|
|[w4VJ6h4mysbpdoN4.htm](npc-gallery/w4VJ6h4mysbpdoN4.htm)|Advisor|auto-trad|
|[W9lhKuDeS670LzLx.htm](npc-gallery/W9lhKuDeS670LzLx.htm)|Prophet|auto-trad|
|[wfsT2QDtQhsFXQfE.htm](npc-gallery/wfsT2QDtQhsFXQfE.htm)|Surgeon|auto-trad|
|[WTCFE1BYdZGWJHh7.htm](npc-gallery/WTCFE1BYdZGWJHh7.htm)|Gravedigger|auto-trad|
|[X1cSs1jhTtx1zTI4.htm](npc-gallery/X1cSs1jhTtx1zTI4.htm)|Poacher|auto-trad|
|[X7LmMMEOFUUicQ2O.htm](npc-gallery/X7LmMMEOFUUicQ2O.htm)|Guide|auto-trad|
|[XpkGaDlyMH2V5wxR.htm](npc-gallery/XpkGaDlyMH2V5wxR.htm)|Priest of Pharasma|auto-trad|
|[xY2WjwebqTNXAP0q.htm](npc-gallery/xY2WjwebqTNXAP0q.htm)|Commoner|auto-trad|
|[Za701s0CV37YPOyo.htm](npc-gallery/Za701s0CV37YPOyo.htm)|Executioner|auto-trad|
|[Zd0K8TOkthc4a4l7.htm](npc-gallery/Zd0K8TOkthc4a4l7.htm)|Grave Robber|auto-trad|
|[zQfufnnLCTzQ165S.htm](npc-gallery/zQfufnnLCTzQ165S.htm)|Monster Hunter|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
