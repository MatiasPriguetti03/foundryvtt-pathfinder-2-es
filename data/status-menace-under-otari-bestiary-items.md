# Estado de la traducción (menace-under-otari-bestiary-items)

 * **auto-trad**: 271
 * **vacía**: 13
 * **ninguna**: 1


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[fx6MEhp40JvDajQh.htm](menace-under-otari-bestiary-items/fx6MEhp40JvDajQh.htm)|Spike Trap|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0c1Yjs4t9BKCpjmr.htm](menace-under-otari-bestiary-items/0c1Yjs4t9BKCpjmr.htm)|Falling Stones|auto-trad|
|[0j2Oa8lKuFbrPVH4.htm](menace-under-otari-bestiary-items/0j2Oa8lKuFbrPVH4.htm)|Wizard Spells|auto-trad|
|[0lvwb1cNAaOUc6Be.htm](menace-under-otari-bestiary-items/0lvwb1cNAaOUc6Be.htm)|End the Charade|auto-trad|
|[0Qt9ULbGzJV8ZG3W.htm](menace-under-otari-bestiary-items/0Qt9ULbGzJV8ZG3W.htm)|Shadow Spawn|auto-trad|
|[0qvSsLdTulsncbsi.htm](menace-under-otari-bestiary-items/0qvSsLdTulsncbsi.htm)|Hurried Retreat|auto-trad|
|[0QWVJ7Of8cQNKp0N.htm](menace-under-otari-bestiary-items/0QWVJ7Of8cQNKp0N.htm)|Slink in Shadows|auto-trad|
|[1fzwu2J1jldVzBPc.htm](menace-under-otari-bestiary-items/1fzwu2J1jldVzBPc.htm)|Coil|auto-trad|
|[1GBTPnqIIGld3Wu1.htm](menace-under-otari-bestiary-items/1GBTPnqIIGld3Wu1.htm)|Dragonscaled|auto-trad|
|[1U37Lsr3NMgzLqLM.htm](menace-under-otari-bestiary-items/1U37Lsr3NMgzLqLM.htm)|Mandibles|auto-trad|
|[1V9uaQOnCMGgMxl2.htm](menace-under-otari-bestiary-items/1V9uaQOnCMGgMxl2.htm)|Darkvision|auto-trad|
|[20wHQZMjGAPl30DH.htm](menace-under-otari-bestiary-items/20wHQZMjGAPl30DH.htm)|Darkvision|auto-trad|
|[2d3ZGRIXERcjqnmJ.htm](menace-under-otari-bestiary-items/2d3ZGRIXERcjqnmJ.htm)|Grab|auto-trad|
|[38uEEYKxUvG8TDFw.htm](menace-under-otari-bestiary-items/38uEEYKxUvG8TDFw.htm)|Crossbow|auto-trad|
|[3g9yO52wil4VC4Dz.htm](menace-under-otari-bestiary-items/3g9yO52wil4VC4Dz.htm)|Fangs|auto-trad|
|[3USivzG0qztGd1dK.htm](menace-under-otari-bestiary-items/3USivzG0qztGd1dK.htm)|Knockdown|auto-trad|
|[3w5kvSRp3MF3snpw.htm](menace-under-otari-bestiary-items/3w5kvSRp3MF3snpw.htm)|Spring|auto-trad|
|[4736AvChlsLG9HVa.htm](menace-under-otari-bestiary-items/4736AvChlsLG9HVa.htm)|Horn|auto-trad|
|[4EXgRMqNIRLXxB1c.htm](menace-under-otari-bestiary-items/4EXgRMqNIRLXxB1c.htm)|Shortbow|auto-trad|
|[4gkC6A8D27Jzv7nH.htm](menace-under-otari-bestiary-items/4gkC6A8D27Jzv7nH.htm)|Broad Swipe|auto-trad|
|[4NqTHQqAkLmiUovi.htm](menace-under-otari-bestiary-items/4NqTHQqAkLmiUovi.htm)|Stench|auto-trad|
|[4tRvd9W1xA0JvgPJ.htm](menace-under-otari-bestiary-items/4tRvd9W1xA0JvgPJ.htm)|Claw|auto-trad|
|[4uWQE7ermpG8oEr3.htm](menace-under-otari-bestiary-items/4uWQE7ermpG8oEr3.htm)|Greatsword|auto-trad|
|[50Meu5uWTOPYtMu6.htm](menace-under-otari-bestiary-items/50Meu5uWTOPYtMu6.htm)|Regeneration 20 (Deactivated by Acid or Fire)|auto-trad|
|[54j0gybWlIAueFkZ.htm](menace-under-otari-bestiary-items/54j0gybWlIAueFkZ.htm)|Shortsword|auto-trad|
|[59wy5xK7MEQmYRxg.htm](menace-under-otari-bestiary-items/59wy5xK7MEQmYRxg.htm)|Earth Glide|auto-trad|
|[5e9xpAPgJWQxMqDB.htm](menace-under-otari-bestiary-items/5e9xpAPgJWQxMqDB.htm)|Low-Light Vision|auto-trad|
|[5HpQse2MIBQbs10B.htm](menace-under-otari-bestiary-items/5HpQse2MIBQbs10B.htm)|Web|auto-trad|
|[5IHmSdZpGxrw76lc.htm](menace-under-otari-bestiary-items/5IHmSdZpGxrw76lc.htm)|Darkvision|auto-trad|
|[5o4Qh26axxAvJeUp.htm](menace-under-otari-bestiary-items/5o4Qh26axxAvJeUp.htm)|Grab|auto-trad|
|[6ii2IeWpDJ46u3xM.htm](menace-under-otari-bestiary-items/6ii2IeWpDJ46u3xM.htm)|Rapier|auto-trad|
|[6yBN6jKH0nlzgvOT.htm](menace-under-otari-bestiary-items/6yBN6jKH0nlzgvOT.htm)|Darkvision|auto-trad|
|[7ODNVUAaNraSivsT.htm](menace-under-otari-bestiary-items/7ODNVUAaNraSivsT.htm)|Shortbow|auto-trad|
|[7ogTgoQe4WAvtdJ7.htm](menace-under-otari-bestiary-items/7ogTgoQe4WAvtdJ7.htm)|Light Blindness|auto-trad|
|[7RTjFlVzsxhjtwX4.htm](menace-under-otari-bestiary-items/7RTjFlVzsxhjtwX4.htm)|Consume Flesh|auto-trad|
|[7swXlOjSl2oHLuDv.htm](menace-under-otari-bestiary-items/7swXlOjSl2oHLuDv.htm)|Greatsword|auto-trad|
|[83QmDBGigZxaHbbd.htm](menace-under-otari-bestiary-items/83QmDBGigZxaHbbd.htm)|Darkvision|auto-trad|
|[8akVcjr4Y8JZR5Ht.htm](menace-under-otari-bestiary-items/8akVcjr4Y8JZR5Ht.htm)|Club|auto-trad|
|[8Hc25NUrYBwSHQuO.htm](menace-under-otari-bestiary-items/8Hc25NUrYBwSHQuO.htm)|Sneak Attack|auto-trad|
|[8jwjSmQRQW8Nk5NY.htm](menace-under-otari-bestiary-items/8jwjSmQRQW8Nk5NY.htm)|Fist|auto-trad|
|[8LYUZymM9ZN8WDw9.htm](menace-under-otari-bestiary-items/8LYUZymM9ZN8WDw9.htm)|Claw|auto-trad|
|[90eQFUneXcsHeKSK.htm](menace-under-otari-bestiary-items/90eQFUneXcsHeKSK.htm)|Staff|auto-trad|
|[91iOrkVbzeP7cK2M.htm](menace-under-otari-bestiary-items/91iOrkVbzeP7cK2M.htm)|Adhesive|auto-trad|
|[94P2S8OyxiovGujz.htm](menace-under-otari-bestiary-items/94P2S8OyxiovGujz.htm)|Twisting Tail|auto-trad|
|[9bomxaBEHSLvRrkq.htm](menace-under-otari-bestiary-items/9bomxaBEHSLvRrkq.htm)|Statue|auto-trad|
|[9IuPTxCvRCR7HHp2.htm](menace-under-otari-bestiary-items/9IuPTxCvRCR7HHp2.htm)|Light Blindness|auto-trad|
|[9nfzrmPzAAStRjAe.htm](menace-under-otari-bestiary-items/9nfzrmPzAAStRjAe.htm)|Quick Draw|auto-trad|
|[9TCF9GcaMoTKkPSj.htm](menace-under-otari-bestiary-items/9TCF9GcaMoTKkPSj.htm)|Paralysis|auto-trad|
|[9yktjCbx8rbvpSZe.htm](menace-under-otari-bestiary-items/9yktjCbx8rbvpSZe.htm)|Spear Barrage|auto-trad|
|[a4I77q6CzdLCZCG6.htm](menace-under-otari-bestiary-items/a4I77q6CzdLCZCG6.htm)|Shortbow|auto-trad|
|[A97t9GcQF8k7V7By.htm](menace-under-otari-bestiary-items/A97t9GcQF8k7V7By.htm)|Claw|auto-trad|
|[ACHlnYzXVxizzqPw.htm](menace-under-otari-bestiary-items/ACHlnYzXVxizzqPw.htm)|Light Vulnerability|auto-trad|
|[ade7S9RWaPXF7KmF.htm](menace-under-otari-bestiary-items/ade7S9RWaPXF7KmF.htm)|Darkvision|auto-trad|
|[Ae0gibnuQy3PwVCV.htm](menace-under-otari-bestiary-items/Ae0gibnuQy3PwVCV.htm)|Spear|auto-trad|
|[Af1bf54ub8OEzyPl.htm](menace-under-otari-bestiary-items/Af1bf54ub8OEzyPl.htm)|Pseudopod|auto-trad|
|[AGjtcz0cAWzBXlEx.htm](menace-under-otari-bestiary-items/AGjtcz0cAWzBXlEx.htm)|Unluck Aura|auto-trad|
|[ANrh02werSO57j4X.htm](menace-under-otari-bestiary-items/ANrh02werSO57j4X.htm)|Wing|auto-trad|
|[aoyjyhv7Dw4gIwd3.htm](menace-under-otari-bestiary-items/aoyjyhv7Dw4gIwd3.htm)|Claw|auto-trad|
|[AtTvfif8mNvTl8HD.htm](menace-under-otari-bestiary-items/AtTvfif8mNvTl8HD.htm)|Ferocity|auto-trad|
|[B1zHLf1i3W3pgrBz.htm](menace-under-otari-bestiary-items/B1zHLf1i3W3pgrBz.htm)|Hurried Retreat|auto-trad|
|[b2WLYPv43nso3X3U.htm](menace-under-otari-bestiary-items/b2WLYPv43nso3X3U.htm)|Jaws|auto-trad|
|[B9C716BDIWeqDrLI.htm](menace-under-otari-bestiary-items/B9C716BDIWeqDrLI.htm)|Jaws|auto-trad|
|[bA2QzO0CkyheAgub.htm](menace-under-otari-bestiary-items/bA2QzO0CkyheAgub.htm)|Claw|auto-trad|
|[BmhB6hl9Ivt5pnw6.htm](menace-under-otari-bestiary-items/BmhB6hl9Ivt5pnw6.htm)|Talon|auto-trad|
|[bz1ZQoFYo4soy8jN.htm](menace-under-otari-bestiary-items/bz1ZQoFYo4soy8jN.htm)|Darkvision|auto-trad|
|[C4dwFfQQc0M5gZBd.htm](menace-under-otari-bestiary-items/C4dwFfQQc0M5gZBd.htm)|Fangs|auto-trad|
|[C77jgOGQyRA9OepX.htm](menace-under-otari-bestiary-items/C77jgOGQyRA9OepX.htm)|Claw|auto-trad|
|[CellesdPVQW5Dk8g.htm](menace-under-otari-bestiary-items/CellesdPVQW5Dk8g.htm)|Ferocity|auto-trad|
|[CfzB5r8CSHETuXfA.htm](menace-under-otari-bestiary-items/CfzB5r8CSHETuXfA.htm)|Site Bound|auto-trad|
|[CHS0DAe3ekrw8ve1.htm](menace-under-otari-bestiary-items/CHS0DAe3ekrw8ve1.htm)|Shortbow|auto-trad|
|[CnmgzGmi2Avq67QA.htm](menace-under-otari-bestiary-items/CnmgzGmi2Avq67QA.htm)|Shortsword|auto-trad|
|[cprgu1LyMWRqzI0w.htm](menace-under-otari-bestiary-items/cprgu1LyMWRqzI0w.htm)|Claw|auto-trad|
|[czwGWW73rkPW7vBg.htm](menace-under-otari-bestiary-items/czwGWW73rkPW7vBg.htm)|Goblin Scuttle|auto-trad|
|[D0BJOT03V2FE7mKT.htm](menace-under-otari-bestiary-items/D0BJOT03V2FE7mKT.htm)|Sneak Attack|auto-trad|
|[d5UuF56WuHzskPO4.htm](menace-under-otari-bestiary-items/d5UuF56WuHzskPO4.htm)|Draconic Frenzy|auto-trad|
|[dB4Pr3yiNKH32fE8.htm](menace-under-otari-bestiary-items/dB4Pr3yiNKH32fE8.htm)|Knockdown|auto-trad|
|[dBX2IpwGpcj3w0iE.htm](menace-under-otari-bestiary-items/dBX2IpwGpcj3w0iE.htm)|Darkvision|auto-trad|
|[dF7ibA1On3Nvjxqk.htm](menace-under-otari-bestiary-items/dF7ibA1On3Nvjxqk.htm)|Hurried Retreat|auto-trad|
|[dKlUXGzoOpojAKGv.htm](menace-under-otari-bestiary-items/dKlUXGzoOpojAKGv.htm)|Spear|auto-trad|
|[Dkveyw8m1JIyOZch.htm](menace-under-otari-bestiary-items/Dkveyw8m1JIyOZch.htm)|Darkvision|auto-trad|
|[DmNGJHLmXcMGdmUS.htm](menace-under-otari-bestiary-items/DmNGJHLmXcMGdmUS.htm)|Battle Axe|auto-trad|
|[dp1n1eKrdXWBGI47.htm](menace-under-otari-bestiary-items/dp1n1eKrdXWBGI47.htm)|Jaws|auto-trad|
|[DqJFXur9S0e0C54j.htm](menace-under-otari-bestiary-items/DqJFXur9S0e0C54j.htm)|Darkvision|auto-trad|
|[DsvyLX53TeuZHMQ3.htm](menace-under-otari-bestiary-items/DsvyLX53TeuZHMQ3.htm)|Clawed Feet|auto-trad|
|[dvuK2slsg05eHdpZ.htm](menace-under-otari-bestiary-items/dvuK2slsg05eHdpZ.htm)|Fist|auto-trad|
|[DW4EvhApIul7gVaU.htm](menace-under-otari-bestiary-items/DW4EvhApIul7gVaU.htm)|Tail|auto-trad|
|[DyABhSgZNJ3ICo85.htm](menace-under-otari-bestiary-items/DyABhSgZNJ3ICo85.htm)|Spear|auto-trad|
|[e8BUlHLevzRJnHEc.htm](menace-under-otari-bestiary-items/e8BUlHLevzRJnHEc.htm)|Bushwhack|auto-trad|
|[EajnVKV3NKyICaQ8.htm](menace-under-otari-bestiary-items/EajnVKV3NKyICaQ8.htm)|Hand Crossbow|auto-trad|
|[EC797WDulwqw96ch.htm](menace-under-otari-bestiary-items/EC797WDulwqw96ch.htm)|Darkvision|auto-trad|
|[Eejxi0k01WzbvUUh.htm](menace-under-otari-bestiary-items/Eejxi0k01WzbvUUh.htm)|Shield Block|auto-trad|
|[ELWu5zIODys6k1lo.htm](menace-under-otari-bestiary-items/ELWu5zIODys6k1lo.htm)|Web|auto-trad|
|[emPcqeSFXdyr6CgV.htm](menace-under-otari-bestiary-items/emPcqeSFXdyr6CgV.htm)|Terrifying Charge|auto-trad|
|[eulyI60JHNUYs39w.htm](menace-under-otari-bestiary-items/eulyI60JHNUYs39w.htm)|Slow|auto-trad|
|[EzSKWtWM2tSTtZwA.htm](menace-under-otari-bestiary-items/EzSKWtWM2tSTtZwA.htm)|Claw|auto-trad|
|[f4rcDncEwu2OdC1a.htm](menace-under-otari-bestiary-items/f4rcDncEwu2OdC1a.htm)|Jaws|auto-trad|
|[Fc10tuSKXi4OAWw6.htm](menace-under-otari-bestiary-items/Fc10tuSKXi4OAWw6.htm)|Deep Plunge|auto-trad|
|[fI8bsqTQlNTlSpdF.htm](menace-under-otari-bestiary-items/fI8bsqTQlNTlSpdF.htm)|Maul|auto-trad|
|[fIAhNhYo8hJML5xU.htm](menace-under-otari-bestiary-items/fIAhNhYo8hJML5xU.htm)|Mind Reading|auto-trad|
|[FMmsMdd150OLekUM.htm](menace-under-otari-bestiary-items/FMmsMdd150OLekUM.htm)|Darkvision|auto-trad|
|[freYXZtmDhD5zIrA.htm](menace-under-otari-bestiary-items/freYXZtmDhD5zIrA.htm)|Javelin|auto-trad|
|[gCm9tVCYX0lSCKOq.htm](menace-under-otari-bestiary-items/gCm9tVCYX0lSCKOq.htm)|Darkvision|auto-trad|
|[gEfOwadNLpPfyfQ8.htm](menace-under-otari-bestiary-items/gEfOwadNLpPfyfQ8.htm)|Claw|auto-trad|
|[GhnXpFtWM1vyvu8V.htm](menace-under-otari-bestiary-items/GhnXpFtWM1vyvu8V.htm)|Construct Armor|auto-trad|
|[GhRRqxvK6T21zheo.htm](menace-under-otari-bestiary-items/GhRRqxvK6T21zheo.htm)|Pack Attack|auto-trad|
|[gjrtuvnDxfzWNWcA.htm](menace-under-otari-bestiary-items/gjrtuvnDxfzWNWcA.htm)|Darkvision|auto-trad|
|[GsQeeSV97T6FPvoY.htm](menace-under-otari-bestiary-items/GsQeeSV97T6FPvoY.htm)|Breath Weapon|auto-trad|
|[gV8plzH46OeVvlNG.htm](menace-under-otari-bestiary-items/gV8plzH46OeVvlNG.htm)|Stench|auto-trad|
|[gwswBzTnckFDYGZR.htm](menace-under-otari-bestiary-items/gwswBzTnckFDYGZR.htm)|Darkvision|auto-trad|
|[HAaABYYDbsq8LbrU.htm](menace-under-otari-bestiary-items/HAaABYYDbsq8LbrU.htm)|Jaws|auto-trad|
|[haWc7Vo7IDgvIPU9.htm](menace-under-otari-bestiary-items/haWc7Vo7IDgvIPU9.htm)|No MAP|auto-trad|
|[hDYxH5VuENin7Q0I.htm](menace-under-otari-bestiary-items/hDYxH5VuENin7Q0I.htm)|Crystal Sense 60 feet|auto-trad|
|[hnjL5DfkOGkcdjPb.htm](menace-under-otari-bestiary-items/hnjL5DfkOGkcdjPb.htm)|Cleric Spells|auto-trad|
|[hQCxv2A5wNHVDzkj.htm](menace-under-otari-bestiary-items/hQCxv2A5wNHVDzkj.htm)|Darkvision|auto-trad|
|[HvOzFzF3j47WTzIv.htm](menace-under-otari-bestiary-items/HvOzFzF3j47WTzIv.htm)|Rejuvenation|auto-trad|
|[I6lxEQY0rHjkqhZ1.htm](menace-under-otari-bestiary-items/I6lxEQY0rHjkqhZ1.htm)|Darkvision|auto-trad|
|[iF6CI9twYSN48Gfj.htm](menace-under-otari-bestiary-items/iF6CI9twYSN48Gfj.htm)|Darkvision|auto-trad|
|[IgVFPfZjnGqYSVAI.htm](menace-under-otari-bestiary-items/IgVFPfZjnGqYSVAI.htm)|Animal Talker|auto-trad|
|[iHa81hIBKMF5eXxi.htm](menace-under-otari-bestiary-items/iHa81hIBKMF5eXxi.htm)|Fangs|auto-trad|
|[iTaObjgnXpF66Sz5.htm](menace-under-otari-bestiary-items/iTaObjgnXpF66Sz5.htm)|Rapier|auto-trad|
|[iuFRjKw2PLJl3E3F.htm](menace-under-otari-bestiary-items/iuFRjKw2PLJl3E3F.htm)|Pounce|auto-trad|
|[IvEp0FjtG1bxA4Xw.htm](menace-under-otari-bestiary-items/IvEp0FjtG1bxA4Xw.htm)|Jaws|auto-trad|
|[ix1KQX8Ji2g1e9ID.htm](menace-under-otari-bestiary-items/ix1KQX8Ji2g1e9ID.htm)|Skewer|auto-trad|
|[J7jljAzKdEbTuHtB.htm](menace-under-otari-bestiary-items/J7jljAzKdEbTuHtB.htm)|Darkvision|auto-trad|
|[j90G6J5zvaIVNb1q.htm](menace-under-otari-bestiary-items/j90G6J5zvaIVNb1q.htm)|Breath Weapon|auto-trad|
|[JaJLITtJG6QOyapG.htm](menace-under-otari-bestiary-items/JaJLITtJG6QOyapG.htm)|Darkvision|auto-trad|
|[jAZY8WC1BtImPyEF.htm](menace-under-otari-bestiary-items/jAZY8WC1BtImPyEF.htm)|Armored Fist|auto-trad|
|[jL9PDQ27Dz6UAMOH.htm](menace-under-otari-bestiary-items/jL9PDQ27Dz6UAMOH.htm)|Scimitar|auto-trad|
|[jmPDVPebOQ3XVOwA.htm](menace-under-otari-bestiary-items/jmPDVPebOQ3XVOwA.htm)|Boar Charge|auto-trad|
|[jvZUoPLS8OA1SQTw.htm](menace-under-otari-bestiary-items/jvZUoPLS8OA1SQTw.htm)|Shortsword|auto-trad|
|[JWlYLXxlI4IRYbeF.htm](menace-under-otari-bestiary-items/JWlYLXxlI4IRYbeF.htm)|Greataxe|auto-trad|
|[jzku4qcmz6H3dk9C.htm](menace-under-otari-bestiary-items/jzku4qcmz6H3dk9C.htm)|Torch|auto-trad|
|[kFgfAbXaJbI8otfZ.htm](menace-under-otari-bestiary-items/kFgfAbXaJbI8otfZ.htm)|Shortsword|auto-trad|
|[Kj1uqisygxPxuaC9.htm](menace-under-otari-bestiary-items/Kj1uqisygxPxuaC9.htm)|Spider Speak|auto-trad|
|[Kjifw1jgDvfmrEGU.htm](menace-under-otari-bestiary-items/Kjifw1jgDvfmrEGU.htm)|Steal Shadow|auto-trad|
|[kkKlT3cO51Wj2tz5.htm](menace-under-otari-bestiary-items/kkKlT3cO51Wj2tz5.htm)|Draconic Momentum|auto-trad|
|[KlVVVsJad72RahoI.htm](menace-under-otari-bestiary-items/KlVVVsJad72RahoI.htm)|Javelin|auto-trad|
|[KlwGH9hdWl3XpMYw.htm](menace-under-otari-bestiary-items/KlwGH9hdWl3XpMYw.htm)|Web Trap|auto-trad|
|[kN3B1jnfTS7LsbUS.htm](menace-under-otari-bestiary-items/kN3B1jnfTS7LsbUS.htm)|Javelin|auto-trad|
|[Kpl3lLjfHM0hWhT5.htm](menace-under-otari-bestiary-items/Kpl3lLjfHM0hWhT5.htm)|Final Spite|auto-trad|
|[ksGHYkwsnnxc2Vmk.htm](menace-under-otari-bestiary-items/ksGHYkwsnnxc2Vmk.htm)|Darkvision|auto-trad|
|[KYq91Bu5HDnQoSkG.htm](menace-under-otari-bestiary-items/KYq91Bu5HDnQoSkG.htm)|Club|auto-trad|
|[L4dKqPtdmyspmZtd.htm](menace-under-otari-bestiary-items/L4dKqPtdmyspmZtd.htm)|Jaws|auto-trad|
|[l4eldMZrkZle4K3Y.htm](menace-under-otari-bestiary-items/l4eldMZrkZle4K3Y.htm)|Horns|auto-trad|
|[l6oPZE6Gcn9lJVoU.htm](menace-under-otari-bestiary-items/l6oPZE6Gcn9lJVoU.htm)|Darkvision|auto-trad|
|[L7wqc9Ph3bPMKfRq.htm](menace-under-otari-bestiary-items/L7wqc9Ph3bPMKfRq.htm)|Low-Light Vision|auto-trad|
|[LIdmD7VqztundfTh.htm](menace-under-otari-bestiary-items/LIdmD7VqztundfTh.htm)|Beak|auto-trad|
|[LJKNy957UOv4LqE5.htm](menace-under-otari-bestiary-items/LJKNy957UOv4LqE5.htm)|Change Shape|auto-trad|
|[lOUQcokkfOtGgPNG.htm](menace-under-otari-bestiary-items/lOUQcokkfOtGgPNG.htm)|Jaws|auto-trad|
|[LPBpvhfnfUxz7nWz.htm](menace-under-otari-bestiary-items/LPBpvhfnfUxz7nWz.htm)|Scythe|auto-trad|
|[lqMgsu5wZAOsEwK2.htm](menace-under-otari-bestiary-items/lqMgsu5wZAOsEwK2.htm)|Giant Viper Venom|auto-trad|
|[lRaDhQKMgeR6IjAB.htm](menace-under-otari-bestiary-items/lRaDhQKMgeR6IjAB.htm)|Low-Light Vision|auto-trad|
|[lRYUK2abQpyhIlbv.htm](menace-under-otari-bestiary-items/lRYUK2abQpyhIlbv.htm)|Darkvision|auto-trad|
|[LUPkzJMY0W8x0Vsd.htm](menace-under-otari-bestiary-items/LUPkzJMY0W8x0Vsd.htm)|Fist|auto-trad|
|[M3QwWIU2rzRHWndo.htm](menace-under-otari-bestiary-items/M3QwWIU2rzRHWndo.htm)|Mimic Object|auto-trad|
|[Mcbgf9KdTTBPuio6.htm](menace-under-otari-bestiary-items/Mcbgf9KdTTBPuio6.htm)|Quick Trap|auto-trad|
|[MCgl5uKpwtTh48E4.htm](menace-under-otari-bestiary-items/MCgl5uKpwtTh48E4.htm)|Longsword|auto-trad|
|[mdqK7RuSjtezVyS8.htm](menace-under-otari-bestiary-items/mdqK7RuSjtezVyS8.htm)|Filth Wave|auto-trad|
|[MEjtnkYdPhtWGyzF.htm](menace-under-otari-bestiary-items/MEjtnkYdPhtWGyzF.htm)|Coiled Opportunity|auto-trad|
|[MfBsuZaAMldux4Xb.htm](menace-under-otari-bestiary-items/MfBsuZaAMldux4Xb.htm)|Javelin|auto-trad|
|[mHS7Uj0GKGKhQiNo.htm](menace-under-otari-bestiary-items/mHS7Uj0GKGKhQiNo.htm)|Battle Axe|auto-trad|
|[MO9W8NpBALoHHtTS.htm](menace-under-otari-bestiary-items/MO9W8NpBALoHHtTS.htm)|Sneak Attack|auto-trad|
|[MrC7xvsyL1EpWyqb.htm](menace-under-otari-bestiary-items/MrC7xvsyL1EpWyqb.htm)|Falling Scythes|auto-trad|
|[naotn6A029XPnJc1.htm](menace-under-otari-bestiary-items/naotn6A029XPnJc1.htm)|Mauler|auto-trad|
|[Nl15bkRTLq869IPi.htm](menace-under-otari-bestiary-items/Nl15bkRTLq869IPi.htm)|Petrifying Glance|auto-trad|
|[nQAtVtNKfvmpm1T4.htm](menace-under-otari-bestiary-items/nQAtVtNKfvmpm1T4.htm)|Darkvision|auto-trad|
|[NQF0fD75864Ytav1.htm](menace-under-otari-bestiary-items/NQF0fD75864Ytav1.htm)|Captivating Song|auto-trad|
|[Ns2pok7e6UtuKUd6.htm](menace-under-otari-bestiary-items/Ns2pok7e6UtuKUd6.htm)|Object Lesson|auto-trad|
|[NS7yHszQCqQS0Bfj.htm](menace-under-otari-bestiary-items/NS7yHszQCqQS0Bfj.htm)|Sneak Attack|auto-trad|
|[nWtiD4ASp2FFsobD.htm](menace-under-otari-bestiary-items/nWtiD4ASp2FFsobD.htm)|Ghostly Hand|auto-trad|
|[NX0owCPUZbsVJ4az.htm](menace-under-otari-bestiary-items/NX0owCPUZbsVJ4az.htm)|Jaws|auto-trad|
|[Odl9UZ1OKOYG044E.htm](menace-under-otari-bestiary-items/Odl9UZ1OKOYG044E.htm)|Descend on a Web|auto-trad|
|[OgkLVLhW0uOMdbGj.htm](menace-under-otari-bestiary-items/OgkLVLhW0uOMdbGj.htm)|Formation|auto-trad|
|[OmVVuah2jgqtqVVo.htm](menace-under-otari-bestiary-items/OmVVuah2jgqtqVVo.htm)|Shortsword|auto-trad|
|[Onifi8y9OVLqK3Ea.htm](menace-under-otari-bestiary-items/Onifi8y9OVLqK3Ea.htm)|Ferocity|auto-trad|
|[oqGxkGNFqdTqq3p0.htm](menace-under-otari-bestiary-items/oqGxkGNFqdTqq3p0.htm)|Darkvision|auto-trad|
|[otqsXx9gjh7rnOpU.htm](menace-under-otari-bestiary-items/otqsXx9gjh7rnOpU.htm)|Goblin Scuttle|auto-trad|
|[OWlPaR3WuuSoTaGl.htm](menace-under-otari-bestiary-items/OWlPaR3WuuSoTaGl.htm)|Low-Light Vision|auto-trad|
|[p1OayYNJswICCP7g.htm](menace-under-otari-bestiary-items/p1OayYNJswICCP7g.htm)|Jaws|auto-trad|
|[p27dLxTbXpewe8ZR.htm](menace-under-otari-bestiary-items/p27dLxTbXpewe8ZR.htm)|Spear|auto-trad|
|[P7JMDrm2HB1QPV3N.htm](menace-under-otari-bestiary-items/P7JMDrm2HB1QPV3N.htm)|Web Lurker Venom|auto-trad|
|[pChhLSd048EpUiL0.htm](menace-under-otari-bestiary-items/pChhLSd048EpUiL0.htm)|Fangs|auto-trad|
|[PIjZTZsbQlriq5X3.htm](menace-under-otari-bestiary-items/PIjZTZsbQlriq5X3.htm)|Giant Centipede Venom|auto-trad|
|[puh52kwYDM4gZi1Z.htm](menace-under-otari-bestiary-items/puh52kwYDM4gZi1Z.htm)|Club|auto-trad|
|[PzpzmSnCSWrwxzDO.htm](menace-under-otari-bestiary-items/PzpzmSnCSWrwxzDO.htm)|Longsword|auto-trad|
|[Q5WTbR9lUXHMagS8.htm](menace-under-otari-bestiary-items/Q5WTbR9lUXHMagS8.htm)|Darkvision|auto-trad|
|[qCmKPruRObzVL1Dr.htm](menace-under-otari-bestiary-items/qCmKPruRObzVL1Dr.htm)|Screeching Advance|auto-trad|
|[qG3VBB7HAWZksxvT.htm](menace-under-otari-bestiary-items/qG3VBB7HAWZksxvT.htm)|Falling Block|auto-trad|
|[qhS5ZEJcqNFytqWd.htm](menace-under-otari-bestiary-items/qhS5ZEJcqNFytqWd.htm)|Jaws|auto-trad|
|[qjYI8SCJrXWZopn6.htm](menace-under-otari-bestiary-items/qjYI8SCJrXWZopn6.htm)|Greataxe|auto-trad|
|[Qknp3UNQSMjNTUmL.htm](menace-under-otari-bestiary-items/Qknp3UNQSMjNTUmL.htm)|Grab|auto-trad|
|[QpOsIDOXtvTTXsS8.htm](menace-under-otari-bestiary-items/QpOsIDOXtvTTXsS8.htm)|Staff|auto-trad|
|[R1hhANtemlX6gt4D.htm](menace-under-otari-bestiary-items/R1hhANtemlX6gt4D.htm)|Darkvision|auto-trad|
|[r5yruYZzaDx9MoHR.htm](menace-under-otari-bestiary-items/r5yruYZzaDx9MoHR.htm)|Low-Light Vision|auto-trad|
|[rA69Ao7ZsTSnYPbJ.htm](menace-under-otari-bestiary-items/rA69Ao7ZsTSnYPbJ.htm)|Darkvision|auto-trad|
|[RDDSANqANJv09ktQ.htm](menace-under-otari-bestiary-items/RDDSANqANJv09ktQ.htm)|Sneak Attack|auto-trad|
|[Re5HgCWQJTyzGewL.htm](menace-under-otari-bestiary-items/Re5HgCWQJTyzGewL.htm)|Web Trap|auto-trad|
|[rGgLU8CkJNTv7kLC.htm](menace-under-otari-bestiary-items/rGgLU8CkJNTv7kLC.htm)|Darkvision|auto-trad|
|[rj9VxT0ANFzZgmXI.htm](menace-under-otari-bestiary-items/rj9VxT0ANFzZgmXI.htm)|Evil Damage|auto-trad|
|[rs9VqajU6hTlLt5o.htm](menace-under-otari-bestiary-items/rs9VqajU6hTlLt5o.htm)|Darkvision|auto-trad|
|[rvraBop9oOSwcjkq.htm](menace-under-otari-bestiary-items/rvraBop9oOSwcjkq.htm)|Swift Leap|auto-trad|
|[RX6HAClLi39MgLLI.htm](menace-under-otari-bestiary-items/RX6HAClLi39MgLLI.htm)|Swipe|auto-trad|
|[RZBxog3E2iCbQY5r.htm](menace-under-otari-bestiary-items/RZBxog3E2iCbQY5r.htm)|Dagger|auto-trad|
|[scw9HnbCVwAyFzok.htm](menace-under-otari-bestiary-items/scw9HnbCVwAyFzok.htm)|Rend|auto-trad|
|[SKxww784aC4E2gzW.htm](menace-under-otari-bestiary-items/SKxww784aC4E2gzW.htm)|Sneak Attack|auto-trad|
|[SL9owkerqt7KHiAD.htm](menace-under-otari-bestiary-items/SL9owkerqt7KHiAD.htm)|Spear|auto-trad|
|[slCMgd2stlwxBKdg.htm](menace-under-otari-bestiary-items/slCMgd2stlwxBKdg.htm)|Darkvision|auto-trad|
|[SNb64KWDBMrTorTI.htm](menace-under-otari-bestiary-items/SNb64KWDBMrTorTI.htm)|Jaws|auto-trad|
|[soQbbQbVd6xMuJl6.htm](menace-under-otari-bestiary-items/soQbbQbVd6xMuJl6.htm)|Incorporeal|auto-trad|
|[sqceAJWlpdKGnHvx.htm](menace-under-otari-bestiary-items/sqceAJWlpdKGnHvx.htm)|Talon|auto-trad|
|[sXZ80MEcWSNN1Ct0.htm](menace-under-otari-bestiary-items/sXZ80MEcWSNN1Ct0.htm)|Spike Trap|auto-trad|
|[SyWmilYaLnrtDFVe.htm](menace-under-otari-bestiary-items/SyWmilYaLnrtDFVe.htm)|Darkvision|auto-trad|
|[sz9GGQOjyrluDIX3.htm](menace-under-otari-bestiary-items/sz9GGQOjyrluDIX3.htm)|Darkvision|auto-trad|
|[T6CjPHrd4XMeWpiE.htm](menace-under-otari-bestiary-items/T6CjPHrd4XMeWpiE.htm)|Darkvision|auto-trad|
|[tC9lSVhNyxDRwaml.htm](menace-under-otari-bestiary-items/tC9lSVhNyxDRwaml.htm)|Wizard Spells|auto-trad|
|[TCZQdv29obsz4Rie.htm](menace-under-otari-bestiary-items/TCZQdv29obsz4Rie.htm)|Low-Light Vision|auto-trad|
|[TnaXi3v3iBDjotwd.htm](menace-under-otari-bestiary-items/TnaXi3v3iBDjotwd.htm)|Viper Venom|auto-trad|
|[tOCS9mkYR5K1LuOZ.htm](menace-under-otari-bestiary-items/tOCS9mkYR5K1LuOZ.htm)|Petrifying Gaze|auto-trad|
|[TS7iVUZNmP0Szdfh.htm](menace-under-otari-bestiary-items/TS7iVUZNmP0Szdfh.htm)|Javelin|auto-trad|
|[tS9Kv6K4kwYFjHCT.htm](menace-under-otari-bestiary-items/tS9Kv6K4kwYFjHCT.htm)|Incorporeal|auto-trad|
|[TyHmjC5wvxo29J5L.htm](menace-under-otari-bestiary-items/TyHmjC5wvxo29J5L.htm)|Grab|auto-trad|
|[U7gmP0tQ6eQor3xT.htm](menace-under-otari-bestiary-items/U7gmP0tQ6eQor3xT.htm)|Fist|auto-trad|
|[u9tY2pkbZxNzmxoe.htm](menace-under-otari-bestiary-items/u9tY2pkbZxNzmxoe.htm)|Claw|auto-trad|
|[uEjliLhQWBsVYCsE.htm](menace-under-otari-bestiary-items/uEjliLhQWBsVYCsE.htm)|Maul|auto-trad|
|[ufap97U4FyUOrJZF.htm](menace-under-otari-bestiary-items/ufap97U4FyUOrJZF.htm)|Light Blindness|auto-trad|
|[uYQrEbjjw3XIQ1cO.htm](menace-under-otari-bestiary-items/uYQrEbjjw3XIQ1cO.htm)|Frightful Moan|auto-trad|
|[uznmLfSkP5j9qOEA.htm](menace-under-otari-bestiary-items/uznmLfSkP5j9qOEA.htm)|Quick Draw|auto-trad|
|[v2Ifd6tb1IDRFqFO.htm](menace-under-otari-bestiary-items/v2Ifd6tb1IDRFqFO.htm)|Drain Life|auto-trad|
|[v90WRjELLiKF57vr.htm](menace-under-otari-bestiary-items/v90WRjELLiKF57vr.htm)|Jaws|auto-trad|
|[v9Xt73Wj9wclc3ef.htm](menace-under-otari-bestiary-items/v9Xt73Wj9wclc3ef.htm)|Javelin|auto-trad|
|[V9zIFbIReO27zwGu.htm](menace-under-otari-bestiary-items/V9zIFbIReO27zwGu.htm)|Darkvision|auto-trad|
|[VcMwjaxqor5ucwyE.htm](menace-under-otari-bestiary-items/VcMwjaxqor5ucwyE.htm)|Javelin|auto-trad|
|[VDL4WflLI1gxDylZ.htm](menace-under-otari-bestiary-items/VDL4WflLI1gxDylZ.htm)|Spine|auto-trad|
|[VeqnECTeAGCW8urK.htm](menace-under-otari-bestiary-items/VeqnECTeAGCW8urK.htm)|Low-Light Vision|auto-trad|
|[vhcrEIRKkfTwrwDs.htm](menace-under-otari-bestiary-items/vhcrEIRKkfTwrwDs.htm)|Fetid Fumes|auto-trad|
|[vnaySgTDfC0slaHW.htm](menace-under-otari-bestiary-items/vnaySgTDfC0slaHW.htm)|Giant Spider Venom|auto-trad|
|[vTjkwNzpCJmsbg6R.htm](menace-under-otari-bestiary-items/vTjkwNzpCJmsbg6R.htm)|Shortsword|auto-trad|
|[vTXaShzYmcNbRjzk.htm](menace-under-otari-bestiary-items/vTXaShzYmcNbRjzk.htm)|Bloodcurdling Screech|auto-trad|
|[VwrlDdZczBGO5Ps4.htm](menace-under-otari-bestiary-items/VwrlDdZczBGO5Ps4.htm)|Motion Sense 60 feet|auto-trad|
|[WbJQr5XhkIENzNfG.htm](menace-under-otari-bestiary-items/WbJQr5XhkIENzNfG.htm)|Darkvision|auto-trad|
|[wBppFKxjdSadJQj2.htm](menace-under-otari-bestiary-items/wBppFKxjdSadJQj2.htm)|Shortbow|auto-trad|
|[wEVWlNgGdVwq51Y9.htm](menace-under-otari-bestiary-items/wEVWlNgGdVwq51Y9.htm)|Claw|auto-trad|
|[WjBhdxKcekKdof75.htm](menace-under-otari-bestiary-items/WjBhdxKcekKdof75.htm)|Darkvision|auto-trad|
|[wOkHQN8ELSDh7T1f.htm](menace-under-otari-bestiary-items/wOkHQN8ELSDh7T1f.htm)|Fist|auto-trad|
|[WpW23oqzbYlPDDuP.htm](menace-under-otari-bestiary-items/WpW23oqzbYlPDDuP.htm)|Slam Shut|auto-trad|
|[wqhHoIIgE3jfAgCx.htm](menace-under-otari-bestiary-items/wqhHoIIgE3jfAgCx.htm)|Smoke Vision|auto-trad|
|[x47iDfIE9albveGl.htm](menace-under-otari-bestiary-items/x47iDfIE9albveGl.htm)|Shadow Hand|auto-trad|
|[x8uwJOVGskq8pjOH.htm](menace-under-otari-bestiary-items/x8uwJOVGskq8pjOH.htm)|Circling Attack|auto-trad|
|[xa524Pmx1LWbJGLL.htm](menace-under-otari-bestiary-items/xa524Pmx1LWbJGLL.htm)|Wizard Spells|auto-trad|
|[XDq62TH6LQA4rgny.htm](menace-under-otari-bestiary-items/XDq62TH6LQA4rgny.htm)|Hand Crossbow|auto-trad|
|[xDUpwWQaZevQOkYQ.htm](menace-under-otari-bestiary-items/xDUpwWQaZevQOkYQ.htm)|Ferocity|auto-trad|
|[XJHXZOImLOORwInT.htm](menace-under-otari-bestiary-items/XJHXZOImLOORwInT.htm)|Jaws|auto-trad|
|[xN6zu0iwyGGunmVu.htm](menace-under-otari-bestiary-items/xN6zu0iwyGGunmVu.htm)|Darkvision|auto-trad|
|[xolLzAgsgpzS6Nvn.htm](menace-under-otari-bestiary-items/xolLzAgsgpzS6Nvn.htm)|Shield Block|auto-trad|
|[XPgz4pQn9aYVvddX.htm](menace-under-otari-bestiary-items/XPgz4pQn9aYVvddX.htm)|Hand Crossbow|auto-trad|
|[XqSpTVGRCbtpPfJU.htm](menace-under-otari-bestiary-items/XqSpTVGRCbtpPfJU.htm)|Darkvision|auto-trad|
|[YdD2kpKW2TNfcupL.htm](menace-under-otari-bestiary-items/YdD2kpKW2TNfcupL.htm)|Goblin Scuttle|auto-trad|
|[yDIuApGqKF4695Oq.htm](menace-under-otari-bestiary-items/yDIuApGqKF4695Oq.htm)|Spear|auto-trad|
|[yDzPXpPBRfYG5UfY.htm](menace-under-otari-bestiary-items/yDzPXpPBRfYG5UfY.htm)|Sneak Attack|auto-trad|
|[yeHohyJSYfiTwghO.htm](menace-under-otari-bestiary-items/yeHohyJSYfiTwghO.htm)|Battle Cry|auto-trad|
|[YfiKyZzHH0XtfjFx.htm](menace-under-otari-bestiary-items/YfiKyZzHH0XtfjFx.htm)|Spray|auto-trad|
|[yfZjkf1LqlK3U7HM.htm](menace-under-otari-bestiary-items/yfZjkf1LqlK3U7HM.htm)|Tusk|auto-trad|
|[YIHoSZN2nTp139Dg.htm](menace-under-otari-bestiary-items/YIHoSZN2nTp139Dg.htm)|Slink|auto-trad|
|[yLnwKltbJHYpn8qC.htm](menace-under-otari-bestiary-items/yLnwKltbJHYpn8qC.htm)|Jaws|auto-trad|
|[YXsM6AcguDWAwVIt.htm](menace-under-otari-bestiary-items/YXsM6AcguDWAwVIt.htm)|Pseudopod|auto-trad|
|[YZxhBg9R5m8ydWm1.htm](menace-under-otari-bestiary-items/YZxhBg9R5m8ydWm1.htm)|Claw|auto-trad|
|[Zdg1lW7CYLRDYorG.htm](menace-under-otari-bestiary-items/Zdg1lW7CYLRDYorG.htm)|Claw|auto-trad|
|[Zh4UT6i2zsYug3c7.htm](menace-under-otari-bestiary-items/Zh4UT6i2zsYug3c7.htm)|Darkvision|auto-trad|
|[ZTPzGQcaC9zUdjD6.htm](menace-under-otari-bestiary-items/ZTPzGQcaC9zUdjD6.htm)|Darkvision|auto-trad|
|[ZY8Teu6y1ruEGOAn.htm](menace-under-otari-bestiary-items/ZY8Teu6y1ruEGOAn.htm)|Pitfall|auto-trad|
|[zyYihJ8eYGtYRh9u.htm](menace-under-otari-bestiary-items/zyYihJ8eYGtYRh9u.htm)|Longsword|auto-trad|
|[zZ7YQvZHZDePNYfh.htm](menace-under-otari-bestiary-items/zZ7YQvZHZDePNYfh.htm)|Darkvision|auto-trad|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[4gDOqdD5endao3AS.htm](menace-under-otari-bestiary-items/4gDOqdD5endao3AS.htm)|Survival|vacía|
|[eh06w4tCybKWTx8g.htm](menace-under-otari-bestiary-items/eh06w4tCybKWTx8g.htm)|Eggshell Necklace|vacía|
|[eU0KcVkea4Q9goDX.htm](menace-under-otari-bestiary-items/eU0KcVkea4Q9goDX.htm)|Dragon Lore|vacía|
|[FRce1Q7000LFf9Gs.htm](menace-under-otari-bestiary-items/FRce1Q7000LFf9Gs.htm)|Iron Key|vacía|
|[GRUZiIR1Uu91DaoY.htm](menace-under-otari-bestiary-items/GRUZiIR1Uu91DaoY.htm)|Stealth|vacía|
|[iUzYuqnjkPvxjVq6.htm](menace-under-otari-bestiary-items/iUzYuqnjkPvxjVq6.htm)|Stealth|vacía|
|[kXXO2coSLlDjiCDu.htm](menace-under-otari-bestiary-items/kXXO2coSLlDjiCDu.htm)|Survival|vacía|
|[KYeYaMrxJZuncqJp.htm](menace-under-otari-bestiary-items/KYeYaMrxJZuncqJp.htm)|Dragon Lore|vacía|
|[pY7DRATVeNfGLzvD.htm](menace-under-otari-bestiary-items/pY7DRATVeNfGLzvD.htm)|Fire Lore|vacía|
|[UedMQrrYjW7RxjuG.htm](menace-under-otari-bestiary-items/UedMQrrYjW7RxjuG.htm)|Crafting|vacía|
|[vBFHfrDtlZ5vYw2q.htm](menace-under-otari-bestiary-items/vBFHfrDtlZ5vYw2q.htm)|Athletics|vacía|
|[XfysylreSHBRTKzu.htm](menace-under-otari-bestiary-items/XfysylreSHBRTKzu.htm)|Performance|vacía|
|[XiR8kBhdXvv7AE8L.htm](menace-under-otari-bestiary-items/XiR8kBhdXvv7AE8L.htm)|Athletics|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
