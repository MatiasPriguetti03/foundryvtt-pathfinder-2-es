# Estado de la traducción (bestiary-ability-glossary-srd)

 * **auto-trad**: 49


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0B39GdScyZMPWalX.htm](bestiary-ability-glossary-srd/0B39GdScyZMPWalX.htm)|Power Attack|auto-trad|
|[2YRDYVnC1eljaXKK.htm](bestiary-ability-glossary-srd/2YRDYVnC1eljaXKK.htm)|At-Will Spells|auto-trad|
|[3JOi2cMcGhT3eze1.htm](bestiary-ability-glossary-srd/3JOi2cMcGhT3eze1.htm)|Rend|auto-trad|
|[4Ho2xMPEC05aSxzr.htm](bestiary-ability-glossary-srd/4Ho2xMPEC05aSxzr.htm)|Greater Darkvision|auto-trad|
|[52CdldlWMiVTZk1F.htm](bestiary-ability-glossary-srd/52CdldlWMiVTZk1F.htm)|Coven|auto-trad|
|[6l7e7CHQLESlI57U.htm](bestiary-ability-glossary-srd/6l7e7CHQLESlI57U.htm)|Improved Push|auto-trad|
|[9qV49KjZujZnSp6w.htm](bestiary-ability-glossary-srd/9qV49KjZujZnSp6w.htm)|All-Around Vision|auto-trad|
|[a0tx6exmB9v9CUsj.htm](bestiary-ability-glossary-srd/a0tx6exmB9v9CUsj.htm)|Throw Rock|auto-trad|
|[AWvNPE4U0kEJSL1T.htm](bestiary-ability-glossary-srd/AWvNPE4U0kEJSL1T.htm)|Sneak Attack|auto-trad|
|[baA0nSMhQyFyJIia.htm](bestiary-ability-glossary-srd/baA0nSMhQyFyJIia.htm)|Swarm Mind|auto-trad|
|[BCLvAx4Pz4MLa2pu.htm](bestiary-ability-glossary-srd/BCLvAx4Pz4MLa2pu.htm)|Knockdown|auto-trad|
|[eQM5hQ1W3d1uen97.htm](bestiary-ability-glossary-srd/eQM5hQ1W3d1uen97.htm)|Change Shape|auto-trad|
|[etMnv73EIdEZrYYu.htm](bestiary-ability-glossary-srd/etMnv73EIdEZrYYu.htm)|Frightful Presence|auto-trad|
|[fFu2sZz4KB01fVRc.htm](bestiary-ability-glossary-srd/fFu2sZz4KB01fVRc.htm)|Low-Light Vision|auto-trad|
|[fJSNOw4zHGbIm4bZ.htm](bestiary-ability-glossary-srd/fJSNOw4zHGbIm4bZ.htm)|Fast Healing|auto-trad|
|[g26YiEIfSHCpLocV.htm](bestiary-ability-glossary-srd/g26YiEIfSHCpLocV.htm)|Constrict|auto-trad|
|[HBrBrUzjfvj2gDXB.htm](bestiary-ability-glossary-srd/HBrBrUzjfvj2gDXB.htm)|Aquatic Ambush|auto-trad|
|[hFtNbo1LKYCoDy2O.htm](bestiary-ability-glossary-srd/hFtNbo1LKYCoDy2O.htm)|Attack of Opportunity|auto-trad|
|[I0HYG0ctCLP5JRsW.htm](bestiary-ability-glossary-srd/I0HYG0ctCLP5JRsW.htm)|Light Blindness|auto-trad|
|[i18TlebMzwONyPhI.htm](bestiary-ability-glossary-srd/i18TlebMzwONyPhI.htm)|Improved Grab|auto-trad|
|[IQtb58p4EaeUzTN1.htm](bestiary-ability-glossary-srd/IQtb58p4EaeUzTN1.htm)|Retributive Strike|auto-trad|
|[j2wsK6IsW5yMW1jW.htm](bestiary-ability-glossary-srd/j2wsK6IsW5yMW1jW.htm)|Tremorsense|auto-trad|
|[JcaIJLGGmst79f1Y.htm](bestiary-ability-glossary-srd/JcaIJLGGmst79f1Y.htm)|Thoughtsense|auto-trad|
|[kakyXBG5WA8c6Zfd.htm](bestiary-ability-glossary-srd/kakyXBG5WA8c6Zfd.htm)|Constant Spells|auto-trad|
|[kdhbPaBMK1d1fpbA.htm](bestiary-ability-glossary-srd/kdhbPaBMK1d1fpbA.htm)|Telepathy|auto-trad|
|[kFv54DisTfCMTBEY.htm](bestiary-ability-glossary-srd/kFv54DisTfCMTBEY.htm)|Poison|auto-trad|
|[kquBnQ0kObZztnBc.htm](bestiary-ability-glossary-srd/kquBnQ0kObZztnBc.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[LbnsT8yXchHMQT3t.htm](bestiary-ability-glossary-srd/LbnsT8yXchHMQT3t.htm)|Improved Knockdown|auto-trad|
|[lR9R5Vld8Eu2Dha5.htm](bestiary-ability-glossary-srd/lR9R5Vld8Eu2Dha5.htm)|Regeneration|auto-trad|
|[m4HQ2o5aPxjXf2kY.htm](bestiary-ability-glossary-srd/m4HQ2o5aPxjXf2kY.htm)|Shield Block|auto-trad|
|[N1kstYbHScxgUQtN.htm](bestiary-ability-glossary-srd/N1kstYbHScxgUQtN.htm)|Ferocity|auto-trad|
|[nZMQh4AaBr291TUf.htm](bestiary-ability-glossary-srd/nZMQh4AaBr291TUf.htm)|Buck|auto-trad|
|[OmV6E3aELvq9CkK1.htm](bestiary-ability-glossary-srd/OmV6E3aELvq9CkK1.htm)|Greater Constrict|auto-trad|
|[OvqohW9YuahnFaiX.htm](bestiary-ability-glossary-srd/OvqohW9YuahnFaiX.htm)|Form Up|auto-trad|
|[qCCLZhnp2HhP3Ex6.htm](bestiary-ability-glossary-srd/qCCLZhnp2HhP3Ex6.htm)|Darkvision|auto-trad|
|[RJbI07QSiYp0SF9A.htm](bestiary-ability-glossary-srd/RJbI07QSiYp0SF9A.htm)|Troop Defenses|auto-trad|
|[rqfnQ5VHT5hxm25r.htm](bestiary-ability-glossary-srd/rqfnQ5VHT5hxm25r.htm)|Scent|auto-trad|
|[sebk9XseMCRkDqRg.htm](bestiary-ability-glossary-srd/sebk9XseMCRkDqRg.htm)|Lifesense|auto-trad|
|[t6cx9FOODmeZQNYl.htm](bestiary-ability-glossary-srd/t6cx9FOODmeZQNYl.htm)|Push|auto-trad|
|[Tkd8sH4pwFIPzqTr.htm](bestiary-ability-glossary-srd/Tkd8sH4pwFIPzqTr.htm)|Grab|auto-trad|
|[TTCw5NusiSSkJU1x.htm](bestiary-ability-glossary-srd/TTCw5NusiSSkJU1x.htm)|Negative Healing|auto-trad|
|[uG0Z8PsyZtsYuvGR.htm](bestiary-ability-glossary-srd/uG0Z8PsyZtsYuvGR.htm)|Catch Rock|auto-trad|
|[uJSseLa57HZYSMUu.htm](bestiary-ability-glossary-srd/uJSseLa57HZYSMUu.htm)|Swallow Whole|auto-trad|
|[UNah0bxXxkcZjxO3.htm](bestiary-ability-glossary-srd/UNah0bxXxkcZjxO3.htm)|Trample|auto-trad|
|[v61oEQaDdcRpaZ9X.htm](bestiary-ability-glossary-srd/v61oEQaDdcRpaZ9X.htm)|Aura|auto-trad|
|[VdSMQ6yRZ3YXNXHL.htm](bestiary-ability-glossary-srd/VdSMQ6yRZ3YXNXHL.htm)|Wavesense|auto-trad|
|[wCnsRCHvtZkZTmO0.htm](bestiary-ability-glossary-srd/wCnsRCHvtZkZTmO0.htm)|Nimble Dodge|auto-trad|
|[Z025dWgZtawbuRLI.htm](bestiary-ability-glossary-srd/Z025dWgZtawbuRLI.htm)|Disease|auto-trad|
|[zU3Ovaet4xQ5Gmvy.htm](bestiary-ability-glossary-srd/zU3Ovaet4xQ5Gmvy.htm)|Engulf|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
