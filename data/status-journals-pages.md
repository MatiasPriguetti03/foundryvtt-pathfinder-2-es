# Estado de la traducción (journals-pages)

 * **auto-trad**: 234
 * **modificada**: 55
 * **ninguna**: 3


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[OtBPlgtudM4PlTWD.htm](journals-pages/OtBPlgtudM4PlTWD.htm)|Harrower|
|[qjnUXickBOBDBu2N.htm](journals-pages/qjnUXickBOBDBu2N.htm)|Introspection Domain|
|[YQXGtBHVoiSYUjtl.htm](journals-pages/YQXGtBHVoiSYUjtl.htm)|Twilight Speaker|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[09VwtSsYldMkVzU1.htm](journals-pages/09VwtSsYldMkVzU1.htm)|Runelord|auto-trad|
|[0GwpYEjCHWyfQvgg.htm](journals-pages/0GwpYEjCHWyfQvgg.htm)|Knowledge Domain|auto-trad|
|[0pPtVXH6Duitakbp.htm](journals-pages/0pPtVXH6Duitakbp.htm)|Folklorist|auto-trad|
|[0RnXjFday2Lp4ECG.htm](journals-pages/0RnXjFday2Lp4ECG.htm)|Sample Chases|auto-trad|
|[0UrqPv7XLDDRwZ13.htm](journals-pages/0UrqPv7XLDDRwZ13.htm)|Pactbinder|auto-trad|
|[0wCEUwABKPdKPj8e.htm](journals-pages/0wCEUwABKPdKPj8e.htm)|Dreams Domain|auto-trad|
|[1F84oh5hoG86mZIw.htm](journals-pages/1F84oh5hoG86mZIw.htm)|Marshal|auto-trad|
|[3a9kFb9x9UXeLA1s.htm](journals-pages/3a9kFb9x9UXeLA1s.htm)|Quick Environmental Details|auto-trad|
|[3B1PqqSZYQj9YXac.htm](journals-pages/3B1PqqSZYQj9YXac.htm)|Hallowed Necromancer|auto-trad|
|[3H168yyaP6ze75kp.htm](journals-pages/3H168yyaP6ze75kp.htm)|Inspiring Relationships|auto-trad|
|[3P0NWwP3s7bIiidH.htm](journals-pages/3P0NWwP3s7bIiidH.htm)|Time Domain|auto-trad|
|[4gKrDFB1GlILn9la.htm](journals-pages/4gKrDFB1GlILn9la.htm)|Scroll Trickster|auto-trad|
|[4ZPZqbe0Ai8ZIfcp.htm](journals-pages/4ZPZqbe0Ai8ZIfcp.htm)|Golden League Xun|auto-trad|
|[57rElfZ9sWKFQ8Ep.htm](journals-pages/57rElfZ9sWKFQ8Ep.htm)|Knight Vigilant|auto-trad|
|[5MjSsuKOLBoiL8FB.htm](journals-pages/5MjSsuKOLBoiL8FB.htm)|Freedom Domain|auto-trad|
|[5nd8ON2kFGOsCdwQ.htm](journals-pages/5nd8ON2kFGOsCdwQ.htm)|Bullet Dancer|auto-trad|
|[5RH1CkZQHYpsOium.htm](journals-pages/5RH1CkZQHYpsOium.htm)|Edgewatch Detective|auto-trad|
|[5TqEbLR9QT3gJGe3.htm](journals-pages/5TqEbLR9QT3gJGe3.htm)|Sorrow Domain|auto-trad|
|[5v7k1XWQxaP0DoGX.htm](journals-pages/5v7k1XWQxaP0DoGX.htm)|Monk|auto-trad|
|[6BXJUVvwvzljaCFT.htm](journals-pages/6BXJUVvwvzljaCFT.htm)|Armor Traits|auto-trad|
|[6DgCLFEGEHvqmy7Y.htm](journals-pages/6DgCLFEGEHvqmy7Y.htm)|Clockwork Reanimator|auto-trad|
|[6KciIDJV6ZdJcAVa.htm](journals-pages/6KciIDJV6ZdJcAVa.htm)|Sterling Dynamo|auto-trad|
|[6qTjtFWaBO5b60zJ.htm](journals-pages/6qTjtFWaBO5b60zJ.htm)|Dust Domain|auto-trad|
|[798PFdS8FmefcOl0.htm](journals-pages/798PFdS8FmefcOl0.htm)|Death Domain|auto-trad|
|[7FsXXkrOUXHtROnq.htm](journals-pages/7FsXXkrOUXHtROnq.htm)|Falling|auto-trad|
|[7jQ2x83KUFD8Wj6w.htm](journals-pages/7jQ2x83KUFD8Wj6w.htm)|Light Effects|auto-trad|
|[7xrNAgAnBqBgE3yM.htm](journals-pages/7xrNAgAnBqBgE3yM.htm)|Change Domain|auto-trad|
|[8qYJAx4O97sSxbUF.htm](journals-pages/8qYJAx4O97sSxbUF.htm)|Basic Actions|auto-trad|
|[9g14DjBZNj27goTt.htm](journals-pages/9g14DjBZNj27goTt.htm)|Soul Warden|auto-trad|
|[9g1dNytABTpmmGkG.htm](journals-pages/9g1dNytABTpmmGkG.htm)|Glyph Domain|auto-trad|
|[9hfEG4kJmEbrjGS1.htm](journals-pages/9hfEG4kJmEbrjGS1.htm)|Encounter Building & XP Awards|auto-trad|
|[a3gPiGqSiWCZ6kGl.htm](journals-pages/a3gPiGqSiWCZ6kGl.htm)|Worldbuilding|auto-trad|
|[A5HwvubNii1d2UND.htm](journals-pages/A5HwvubNii1d2UND.htm)|Sniping Duo|auto-trad|
|[A7vErdGAweYsFcW8.htm](journals-pages/A7vErdGAweYsFcW8.htm)|Healing Domain|auto-trad|
|[aAm6jY2k5qBuWETd.htm](journals-pages/aAm6jY2k5qBuWETd.htm)|Inventor|auto-trad|
|[aBALUjLyOqXKlhrP.htm](journals-pages/aBALUjLyOqXKlhrP.htm)|Earn Income|auto-trad|
|[act12TfHpnKx6ZTu.htm](journals-pages/act12TfHpnKx6ZTu.htm)|Different Size Items|auto-trad|
|[aIpIUbupTjw2863C.htm](journals-pages/aIpIUbupTjw2863C.htm)|Ghost|auto-trad|
|[ajCEExOaxuB4C1tY.htm](journals-pages/ajCEExOaxuB4C1tY.htm)|Passion Domain|auto-trad|
|[AOQZjqgfafqqtHOB.htm](journals-pages/AOQZjqgfafqqtHOB.htm)|Destruction Domain|auto-trad|
|[ARYrFIOsT3JxpjDY.htm](journals-pages/ARYrFIOsT3JxpjDY.htm)|Hellknight Armiger|auto-trad|
|[b5VoSJNzoNuqbtvD.htm](journals-pages/b5VoSJNzoNuqbtvD.htm)|Summoner|auto-trad|
|[b7oePIMEMdFFS5TH.htm](journals-pages/b7oePIMEMdFFS5TH.htm)|Captivator|auto-trad|
|[bAIAWQ7q3ycIAnoj.htm](journals-pages/bAIAWQ7q3ycIAnoj.htm)|Relationships|auto-trad|
|[bApp2BZEMuYQCTDM.htm](journals-pages/bApp2BZEMuYQCTDM.htm)|Scout|auto-trad|
|[BHn7nSm4BMPh9QoZ.htm](journals-pages/BHn7nSm4BMPh9QoZ.htm)|Using Deep Backgrounds|auto-trad|
|[bKoV037XO3qiakGw.htm](journals-pages/bKoV037XO3qiakGw.htm)|Ghoul|auto-trad|
|[bkTCYlTFNifrM3sh.htm](journals-pages/bkTCYlTFNifrM3sh.htm)|Artillerist|auto-trad|
|[bL5cccuvnP5h7Bnn.htm](journals-pages/bL5cccuvnP5h7Bnn.htm)|Animal Trainer|auto-trad|
|[bQeLZt29NnHEn6fh.htm](journals-pages/bQeLZt29NnHEn6fh.htm)|Juggler|auto-trad|
|[BRxlFHXu8tN14TDI.htm](journals-pages/BRxlFHXu8tN14TDI.htm)|Staff Acrobat|auto-trad|
|[BsaTOw6p74DW5d8t.htm](journals-pages/BsaTOw6p74DW5d8t.htm)|Wand Prices|auto-trad|
|[bTujFcUut9RX4GCy.htm](journals-pages/bTujFcUut9RX4GCy.htm)|Travel Domain|auto-trad|
|[C4b2AvMpVpe5BcVv.htm](journals-pages/C4b2AvMpVpe5BcVv.htm)|Material Hardness, Hit Points and Broken Threshold|auto-trad|
|[c8psqIuH4YFi6msK.htm](journals-pages/c8psqIuH4YFi6msK.htm)|Family Background|auto-trad|
|[cAxBEZsej32riaY5.htm](journals-pages/cAxBEZsej32riaY5.htm)|Decay Domain|auto-trad|
|[CB9YE4P7A2Wty1IX.htm](journals-pages/CB9YE4P7A2Wty1IX.htm)|Red Mantis Assassin|auto-trad|
|[cBBwTnJrVmvaRMYq.htm](journals-pages/cBBwTnJrVmvaRMYq.htm)|Aldori Duelist|auto-trad|
|[CbsAiY68e8n5vVVN.htm](journals-pages/CbsAiY68e8n5vVVN.htm)|Repose Domain|auto-trad|
|[cdnkS3A8OpOnRjKu.htm](journals-pages/cdnkS3A8OpOnRjKu.htm)|Bellflower Tiller|auto-trad|
|[CkBvj5y1lAm1jnsc.htm](journals-pages/CkBvj5y1lAm1jnsc.htm)|Sun Domain|auto-trad|
|[CM9ZqWwl7myKn2X1.htm](journals-pages/CM9ZqWwl7myKn2X1.htm)|Darkness Domain|auto-trad|
|[COTDddn4psjIoWry.htm](journals-pages/COTDddn4psjIoWry.htm)|Lion Blade|auto-trad|
|[CSVoyUvynmM5LzPW.htm](journals-pages/CSVoyUvynmM5LzPW.htm)|Gunslinger|auto-trad|
|[Czi3XXuNOSE7ISpd.htm](journals-pages/Czi3XXuNOSE7ISpd.htm)|Perfection Domain|auto-trad|
|[DAVBjDSysgXgtVQu.htm](journals-pages/DAVBjDSysgXgtVQu.htm)|Gray Gardener|auto-trad|
|[DBxoE3TNDPtcKHxH.htm](journals-pages/DBxoE3TNDPtcKHxH.htm)|Animals & Barding|auto-trad|
|[DI3MYGIK8iEycanU.htm](journals-pages/DI3MYGIK8iEycanU.htm)|Zeal Domain|auto-trad|
|[dnbUao9yk8Bs99eT.htm](journals-pages/dnbUao9yk8Bs99eT.htm)|Weapon Traits|auto-trad|
|[dpL9RyApn4y7jJ3y.htm](journals-pages/dpL9RyApn4y7jJ3y.htm)|Homeland|auto-trad|
|[dRu66xNqJ9Ihe1or.htm](journals-pages/dRu66xNqJ9Ihe1or.htm)|Difficulty Classes|auto-trad|
|[DS95vr2zmTsjsMhU.htm](journals-pages/DS95vr2zmTsjsMhU.htm)|Magic Domain|auto-trad|
|[DUDsA3sIlOlibvIu.htm](journals-pages/DUDsA3sIlOlibvIu.htm)|High-Quality Items|auto-trad|
|[duqPy1VMQYNJrw7q.htm](journals-pages/duqPy1VMQYNJrw7q.htm)|Conditions|auto-trad|
|[Dx47K8wpx8KZUa9S.htm](journals-pages/Dx47K8wpx8KZUa9S.htm)|Protection Domain|auto-trad|
|[DXtjeVaWNB8zSjpA.htm](journals-pages/DXtjeVaWNB8zSjpA.htm)|Champion|auto-trad|
|[DztQF2FexWvnzcaE.htm](journals-pages/DztQF2FexWvnzcaE.htm)|Spell Trickster|auto-trad|
|[EC2eB0JglDG5j1gT.htm](journals-pages/EC2eB0JglDG5j1gT.htm)|Fate Domain|auto-trad|
|[egSErNozlL3HRK1y.htm](journals-pages/egSErNozlL3HRK1y.htm)|Fire Domain|auto-trad|
|[eLc6TSgWykjtSeuF.htm](journals-pages/eLc6TSgWykjtSeuF.htm)|Magaambyan Attendant|auto-trad|
|[eNTStXeuABNPSLjw.htm](journals-pages/eNTStXeuABNPSLjw.htm)|Witch|auto-trad|
|[ePfibeHcnRcjO6lC.htm](journals-pages/ePfibeHcnRcjO6lC.htm)|Reanimator|auto-trad|
|[EQfZepZX6rxxBRqG.htm](journals-pages/EQfZepZX6rxxBRqG.htm)|Toil Domain|auto-trad|
|[EwlYs1OzaMj9BB5I.htm](journals-pages/EwlYs1OzaMj9BB5I.htm)|Student of Perfection|auto-trad|
|[f2wr86966A0oUUA0.htm](journals-pages/f2wr86966A0oUUA0.htm)|Adjusting Treasure|auto-trad|
|[F7W15pGOeSeNJD0C.htm](journals-pages/F7W15pGOeSeNJD0C.htm)|Firebrand Braggart|auto-trad|
|[Fea8ZereQhNolDoP.htm](journals-pages/Fea8ZereQhNolDoP.htm)|Bounty Hunter|auto-trad|
|[FJGPBhYmD7xTFsrW.htm](journals-pages/FJGPBhYmD7xTFsrW.htm)|Spellshot (Class Archetype)|auto-trad|
|[flmxRzGxN2rRNyxZ.htm](journals-pages/flmxRzGxN2rRNyxZ.htm)|Confidence Domain|auto-trad|
|[FOI43M8DJe2lkMwl.htm](journals-pages/FOI43M8DJe2lkMwl.htm)|Structures|auto-trad|
|[fPqSekmEm5byReOk.htm](journals-pages/fPqSekmEm5byReOk.htm)|Talisman Dabbler|auto-trad|
|[FtW1gtbHgO0KofPl.htm](journals-pages/FtW1gtbHgO0KofPl.htm)|Pain Domain|auto-trad|
|[fTxZjmNLckRwZ4Po.htm](journals-pages/fTxZjmNLckRwZ4Po.htm)|Pathfinder Agent|auto-trad|
|[FzzMt7ybWy03D9qZ.htm](journals-pages/FzzMt7ybWy03D9qZ.htm)|Basic Services|auto-trad|
|[G1Rje9eYCFEByzjg.htm](journals-pages/G1Rje9eYCFEByzjg.htm)|Special Battles|auto-trad|
|[gE3x4USaU4VWdwKD.htm](journals-pages/gE3x4USaU4VWdwKD.htm)|Gamemastery|auto-trad|
|[gfhnRp2TEy9JCfHI.htm](journals-pages/gfhnRp2TEy9JCfHI.htm)|Gladiator|auto-trad|
|[GiuzDTtkQAgtGW6n.htm](journals-pages/GiuzDTtkQAgtGW6n.htm)|Indulgence Domain|auto-trad|
|[h1zdUIWX1k5PKTnc.htm](journals-pages/h1zdUIWX1k5PKTnc.htm)|Celebrity|auto-trad|
|[HDTW0vr8QL20M5ND.htm](journals-pages/HDTW0vr8QL20M5ND.htm)|Weapon Improviser|auto-trad|
|[hGoWOjdsUz16oJUm.htm](journals-pages/hGoWOjdsUz16oJUm.htm)|Plague Domain|auto-trad|
|[hH8gPjc3GxFzgLHR.htm](journals-pages/hH8gPjc3GxFzgLHR.htm)|Variant Rules|auto-trad|
|[hOsFg2VpnOshaSOi.htm](journals-pages/hOsFg2VpnOshaSOi.htm)|Environmental Damage|auto-trad|
|[I6FbkKYncDuu7eWq.htm](journals-pages/I6FbkKYncDuu7eWq.htm)|Geomancer|auto-trad|
|[I6lHRH3mWjfvhJrR.htm](journals-pages/I6lHRH3mWjfvhJrR.htm)|Resting|auto-trad|
|[IliVJaEHaOklcg86.htm](journals-pages/IliVJaEHaOklcg86.htm)|Stamina|auto-trad|
|[inxtk4rYj2UZaytg.htm](journals-pages/inxtk4rYj2UZaytg.htm)|Fighter|auto-trad|
|[INYPqM7rs9SNFRba.htm](journals-pages/INYPqM7rs9SNFRba.htm)|Downtime Events|auto-trad|
|[IQ56rAleMRc53Lcv.htm](journals-pages/IQ56rAleMRc53Lcv.htm)|Major Childhood Event|auto-trad|
|[iVY32tNvfS1tPYU2.htm](journals-pages/iVY32tNvfS1tPYU2.htm)|Formula Price|auto-trad|
|[JBPXisA4IdXI9gVn.htm](journals-pages/JBPXisA4IdXI9gVn.htm)|Eldritch Researcher|auto-trad|
|[jEf21wTIEXzGtPU6.htm](journals-pages/jEf21wTIEXzGtPU6.htm)|Detecting with Other Senses|auto-trad|
|[jFhTp57zO3ej6HDt.htm](journals-pages/jFhTp57zO3ej6HDt.htm)|Corpse Tender|auto-trad|
|[jq9O1tl76g2AzLOh.htm](journals-pages/jq9O1tl76g2AzLOh.htm)|Cold Domain|auto-trad|
|[jTiXRtNNvMOnsg98.htm](journals-pages/jTiXRtNNvMOnsg98.htm)|Beast Gunner|auto-trad|
|[k9Ebp52kt0ZLHtMl.htm](journals-pages/k9Ebp52kt0ZLHtMl.htm)|Swashbuckler|auto-trad|
|[K9Krytj8OtUvQxoc.htm](journals-pages/K9Krytj8OtUvQxoc.htm)|Thaumaturge|auto-trad|
|[Kca7UPuMm44tOo9n.htm](journals-pages/Kca7UPuMm44tOo9n.htm)|Lightning Domain|auto-trad|
|[kL8yx8vz9FMCIYC1.htm](journals-pages/kL8yx8vz9FMCIYC1.htm)|Treat Wounds|auto-trad|
|[KORSADviZaSccs2W.htm](journals-pages/KORSADviZaSccs2W.htm)|Zephyr Guard|auto-trad|
|[KRUWPDzuwG0LC26d.htm](journals-pages/KRUWPDzuwG0LC26d.htm)|Curse Maelstrom|auto-trad|
|[KXAxZfdMpnHQZQoc.htm](journals-pages/KXAxZfdMpnHQZQoc.htm)|Golarion Calendar|auto-trad|
|[kxDloJH5ImKnIV9P.htm](journals-pages/kxDloJH5ImKnIV9P.htm)|Temperature Effects|auto-trad|
|[l0rlB52Pzy6Vt1Ic.htm](journals-pages/l0rlB52Pzy6Vt1Ic.htm)|Scroll Prices|auto-trad|
|[L11XsA5G89xVKlDw.htm](journals-pages/L11XsA5G89xVKlDw.htm)|Luck Domain|auto-trad|
|[lc3iBCamQ8jvr9dp.htm](journals-pages/lc3iBCamQ8jvr9dp.htm)|Trapsmith|auto-trad|
|[lcCPztCqmXpwdmWx.htm](journals-pages/lcCPztCqmXpwdmWx.htm)|Elite/Weak Adjustments|auto-trad|
|[lgsJz7mZ1OTe340e.htm](journals-pages/lgsJz7mZ1OTe340e.htm)|Truth Domain|auto-trad|
|[LL4YD16kPqcGibKG.htm](journals-pages/LL4YD16kPqcGibKG.htm)|Ghost Hunter|auto-trad|
|[lQ9MmX3zg4Bd9hjb.htm](journals-pages/lQ9MmX3zg4Bd9hjb.htm)|GM Screen|auto-trad|
|[LWgcWM8B85HHVCtZ.htm](journals-pages/LWgcWM8B85HHVCtZ.htm)|Soulforger|auto-trad|
|[Ly8l2GT6dbvuY3A2.htm](journals-pages/Ly8l2GT6dbvuY3A2.htm)|Treasure|auto-trad|
|[mJBp4KIszuqrmnp5.htm](journals-pages/mJBp4KIszuqrmnp5.htm)|Wealth Domain|auto-trad|
|[Mk7ECwe1a971WyWl.htm](journals-pages/Mk7ECwe1a971WyWl.htm)|Demolitionist|auto-trad|
|[mmB3EkkdCpLke7Lk.htm](journals-pages/mmB3EkkdCpLke7Lk.htm)|Investigator|auto-trad|
|[mmfWcV3Iyql5nzTo.htm](journals-pages/mmfWcV3Iyql5nzTo.htm)|Counteract|auto-trad|
|[MOVMHZU1SfkhNN1K.htm](journals-pages/MOVMHZU1SfkhNN1K.htm)|Might Domain|auto-trad|
|[mtc5frNxUHORGscz.htm](journals-pages/mtc5frNxUHORGscz.htm)|Linguist|auto-trad|
|[mV0K9sxD3TWnZDcy.htm](journals-pages/mV0K9sxD3TWnZDcy.htm)|Undead Slayer|auto-trad|
|[mXywYJJCM9IVItZz.htm](journals-pages/mXywYJJCM9IVItZz.htm)|Mind Smith|auto-trad|
|[NaG4fy33coUdSFtH.htm](journals-pages/NaG4fy33coUdSFtH.htm)|Jalmeri Heavenseeker|auto-trad|
|[nmcEiM2gjqjGWp2c.htm](journals-pages/nmcEiM2gjqjGWp2c.htm)|Cavalier|auto-trad|
|[nPcma8UOqWo7xw0P.htm](journals-pages/nPcma8UOqWo7xw0P.htm)|Trick Driver|auto-trad|
|[nuywscaiVGXLQpZ1.htm](journals-pages/nuywscaiVGXLQpZ1.htm)|Wyrmkin Domain|auto-trad|
|[nVfhX1aisz6jY8qf.htm](journals-pages/nVfhX1aisz6jY8qf.htm)|Unexpected Sharpshooter|auto-trad|
|[O7fKfFxCx3e1YasX.htm](journals-pages/O7fKfFxCx3e1YasX.htm)|Spellmaster|auto-trad|
|[O9P1YgtiCgHlPNp5.htm](journals-pages/O9P1YgtiCgHlPNp5.htm)|Pirate|auto-trad|
|[oAfVg9t7GGTW7R1H.htm](journals-pages/oAfVg9t7GGTW7R1H.htm)|Challenging Relationships|auto-trad|
|[oBlKgVYRup5ORqx1.htm](journals-pages/oBlKgVYRup5ORqx1.htm)|Bard|auto-trad|
|[oBOw6E6V7ncrB1rN.htm](journals-pages/oBOw6E6V7ncrB1rN.htm)|Treasure Per Encounter|auto-trad|
|[OBTmo8rD2x8kFzeH.htm](journals-pages/OBTmo8rD2x8kFzeH.htm)|Sleepwalker|auto-trad|
|[OcioDiLTdgzvT8VX.htm](journals-pages/OcioDiLTdgzvT8VX.htm)|Knight Reclaimant|auto-trad|
|[ok2IPQGIrxj8h1vs.htm](journals-pages/ok2IPQGIrxj8h1vs.htm)|Three-Dimensional Combat|auto-trad|
|[oM0DobZe9uEkz9kb.htm](journals-pages/oM0DobZe9uEkz9kb.htm)|Force Open|auto-trad|
|[pDmUITVao0FDMnVf.htm](journals-pages/pDmUITVao0FDMnVf.htm)|Halcyon Speaker|auto-trad|
|[pEjd65isUfXKSB18.htm](journals-pages/pEjd65isUfXKSB18.htm)|Hero Points|auto-trad|
|[PFBBmN58KtjXo79k.htm](journals-pages/PFBBmN58KtjXo79k.htm)|Basic Rules|auto-trad|
|[PfoKLAsQeXHsDgzf.htm](journals-pages/PfoKLAsQeXHsDgzf.htm)|Lifestyle Expenses|auto-trad|
|[pg9fG1ikcgJZRWlk.htm](journals-pages/pg9fG1ikcgJZRWlk.htm)|Lastwall Sentry|auto-trad|
|[Ph7yWRR376aYh12T.htm](journals-pages/Ph7yWRR376aYh12T.htm)|Ritualist|auto-trad|
|[PpoPRKuwanrnhd0Y.htm](journals-pages/PpoPRKuwanrnhd0Y.htm)|Living Monolith|auto-trad|
|[PVwU1LMWcSgD7Q0m.htm](journals-pages/PVwU1LMWcSgD7Q0m.htm)|Oatia Skysage|auto-trad|
|[Q72YDLPh0urLfWPm.htm](journals-pages/Q72YDLPh0urLfWPm.htm)|Vehicle Mechanic|auto-trad|
|[qlQSYjCwnoCzfto2.htm](journals-pages/qlQSYjCwnoCzfto2.htm)|Golem Grafter|auto-trad|
|[qMNBD91gc5kIKPs2.htm](journals-pages/qMNBD91gc5kIKPs2.htm)|Spellcasting Services|auto-trad|
|[qMS6QepvY7UQQjcr.htm](journals-pages/qMS6QepvY7UQQjcr.htm)|Abomination Domain|auto-trad|
|[QWw0D2YPKdz6HJPu.htm](journals-pages/QWw0D2YPKdz6HJPu.htm)|Influential Associate|auto-trad|
|[QzsUe3Rt3SifTQvb.htm](journals-pages/QzsUe3Rt3SifTQvb.htm)|Naga Domain|auto-trad|
|[R20JXF43vU5RQyUj.htm](journals-pages/R20JXF43vU5RQyUj.htm)|Nightmares Domain|auto-trad|
|[r43lPEEL7WHOZjHL.htm](journals-pages/r43lPEEL7WHOZjHL.htm)|Lich|auto-trad|
|[r79jab1XgOdcgvKJ.htm](journals-pages/r79jab1XgOdcgvKJ.htm)|Magus|auto-trad|
|[rd0jQwvTK4jpv95o.htm](journals-pages/rd0jQwvTK4jpv95o.htm)|Swarm Domain|auto-trad|
|[RIlgBuWGfHC1rzYu.htm](journals-pages/RIlgBuWGfHC1rzYu.htm)|Undeath Domain|auto-trad|
|[rlKxNuq1obXe3m5J.htm](journals-pages/rlKxNuq1obXe3m5J.htm)|Scrounger|auto-trad|
|[RoF5NOFBefXAPftS.htm](journals-pages/RoF5NOFBefXAPftS.htm)|Mauler|auto-trad|
|[Rrjz5tMJtyVEQnh8.htm](journals-pages/Rrjz5tMJtyVEQnh8.htm)|Herbalist|auto-trad|
|[rtobUemb6vF2Yu3Y.htm](journals-pages/rtobUemb6vF2Yu3Y.htm)|Soul Domain|auto-trad|
|[RxDsPgPCCxEdjcVQ.htm](journals-pages/RxDsPgPCCxEdjcVQ.htm)|Hellknight Signifer|auto-trad|
|[rxyJUbQosVfgRjLr.htm](journals-pages/rxyJUbQosVfgRjLr.htm)|Crystal Keeper|auto-trad|
|[S1gyomjojgtCdxc3.htm](journals-pages/S1gyomjojgtCdxc3.htm)|Secrecy Domain|auto-trad|
|[S4BZW9c5n6CctDxl.htm](journals-pages/S4BZW9c5n6CctDxl.htm)|Firework Technician|auto-trad|
|[s9kLzXOCl2GNT6TY.htm](journals-pages/s9kLzXOCl2GNT6TY.htm)|Dandy|auto-trad|
|[SnSGU7DPiogZ9JOr.htm](journals-pages/SnSGU7DPiogZ9JOr.htm)|Automatic Bonus Progression|auto-trad|
|[SO9d7RSf1zqmTlmW.htm](journals-pages/SO9d7RSf1zqmTlmW.htm)|Dragon Disciple|auto-trad|
|[SpWLU1ixQRT6rkUP.htm](journals-pages/SpWLU1ixQRT6rkUP.htm)|Downtime Tasks|auto-trad|
|[StXN6IHR6evRaeXF.htm](journals-pages/StXN6IHR6evRaeXF.htm)|Vigil Domain|auto-trad|
|[SUDV5hFZ9WocxWqv.htm](journals-pages/SUDV5hFZ9WocxWqv.htm)|Blessed One|auto-trad|
|[sY25SoDaHBPIG5Jw.htm](journals-pages/sY25SoDaHBPIG5Jw.htm)|Nantambu Chime-Ringer|auto-trad|
|[T0JHj79aGphlZ4Mt.htm](journals-pages/T0JHj79aGphlZ4Mt.htm)|Tyranny Domain|auto-trad|
|[T2y0vuYibZCL7CH0.htm](journals-pages/T2y0vuYibZCL7CH0.htm)|Air Domain|auto-trad|
|[tJllU1E8g7W1QxYe.htm](journals-pages/tJllU1E8g7W1QxYe.htm)|Cover|auto-trad|
|[tJQ5f8C8m5gpZsF1.htm](journals-pages/tJQ5f8C8m5gpZsF1.htm)|Oozemorph|auto-trad|
|[TnD2hTWTyjGKlw9b.htm](journals-pages/TnD2hTWTyjGKlw9b.htm)|Sentinel|auto-trad|
|[ts7gtyAhDEI4V7Ve.htm](journals-pages/ts7gtyAhDEI4V7Ve.htm)|Character Wealth|auto-trad|
|[tuThzOCvMLbRVba8.htm](journals-pages/tuThzOCvMLbRVba8.htm)|Delirium Domain|auto-trad|
|[U8WVR6EDfmUaMCbu.htm](journals-pages/U8WVR6EDfmUaMCbu.htm)|Water Domain|auto-trad|
|[uA6XIArPfGSwBvZi.htm](journals-pages/uA6XIArPfGSwBvZi.htm)|Psychic Duelist|auto-trad|
|[uGQKjk2w4whzomky.htm](journals-pages/uGQKjk2w4whzomky.htm)|Duty Domain|auto-trad|
|[uGsGn5sgR7wn0TQD.htm](journals-pages/uGsGn5sgR7wn0TQD.htm)|Turpin Rowe Lumberjack|auto-trad|
|[Ux0sa5SUBu616i5k.htm](journals-pages/Ux0sa5SUBu616i5k.htm)|Alchemist|auto-trad|
|[vKuJVojicBmNsL1C.htm](journals-pages/vKuJVojicBmNsL1C.htm)|Butterfly Blade|auto-trad|
|[VnR8gBBq1oCG7YQh.htm](journals-pages/VnR8gBBq1oCG7YQh.htm)|Reputation|auto-trad|
|[VR9UdfhZphZGZdsx.htm](journals-pages/VR9UdfhZphZGZdsx.htm)|Bulk Conversions for Different Sizes|auto-trad|
|[vS9iMDapZN9uW33Q.htm](journals-pages/vS9iMDapZN9uW33Q.htm)|Bright Lion|auto-trad|
|[wBhgIgt47v9uspp3.htm](journals-pages/wBhgIgt47v9uspp3.htm)|Nature Domain|auto-trad|
|[wHs6IGCMaXsgFNop.htm](journals-pages/wHs6IGCMaXsgFNop.htm)|Shadowcaster|auto-trad|
|[wMUAm1PJ1HjJ8fFU.htm](journals-pages/wMUAm1PJ1HjJ8fFU.htm)|Pistol Phenom|auto-trad|
|[XCxK6OGFY0KvJPo3.htm](journals-pages/XCxK6OGFY0KvJPo3.htm)|Learn a Spell|auto-trad|
|[xgk1Qc6nzGNJ7LSz.htm](journals-pages/xgk1Qc6nzGNJ7LSz.htm)|Chronoskimmer|auto-trad|
|[XH3xabCB01FiCRal.htm](journals-pages/XH3xabCB01FiCRal.htm)|Proficiency Without Level|auto-trad|
|[xJtbGqoz3BcCjUik.htm](journals-pages/xJtbGqoz3BcCjUik.htm)|Trickery Domain|auto-trad|
|[XLug8rxIe1KPX6Nf.htm](journals-pages/XLug8rxIe1KPX6Nf.htm)|Provocator|auto-trad|
|[xLxrtbsj4acqgsyC.htm](journals-pages/xLxrtbsj4acqgsyC.htm)|Void Domain|auto-trad|
|[xMdIENo4w9bEzZuK.htm](journals-pages/xMdIENo4w9bEzZuK.htm)|Ghost Eater|auto-trad|
|[XszmLWy174esRrlg.htm](journals-pages/XszmLWy174esRrlg.htm)|Wizard|auto-trad|
|[XWkyCVISmVtJ0ZY3.htm](journals-pages/XWkyCVISmVtJ0ZY3.htm)|Medic|auto-trad|
|[Y4DiXz5I8krCh1fC.htm](journals-pages/Y4DiXz5I8krCh1fC.htm)|Swordmaster|auto-trad|
|[yaMJsfYZmWJLqbFE.htm](journals-pages/yaMJsfYZmWJLqbFE.htm)|Ambition Domain|auto-trad|
|[ygboVjCAFRpcysUb.htm](journals-pages/ygboVjCAFRpcysUb.htm)|Player Screen|auto-trad|
|[ygzv72IJNmjh0SPB.htm](journals-pages/ygzv72IJNmjh0SPB.htm)|Psychic|auto-trad|
|[yHqDPytVY9bxWo9I.htm](journals-pages/yHqDPytVY9bxWo9I.htm)|Item Quirks|auto-trad|
|[YODf1eNWi9jnR93y.htm](journals-pages/YODf1eNWi9jnR93y.htm)|Pactbound Initiate|auto-trad|
|[yOEiNjHLQ6XuOruq.htm](journals-pages/yOEiNjHLQ6XuOruq.htm)|Overwatch|auto-trad|
|[YVLMgNxXTUQuDJgp.htm](journals-pages/YVLMgNxXTUQuDJgp.htm)|Travel Speed|auto-trad|
|[yWtwNUkGyj79Q04W.htm](journals-pages/yWtwNUkGyj79Q04W.htm)|Hellknight|auto-trad|
|[Z2xXPHkxGLFTXzdI.htm](journals-pages/Z2xXPHkxGLFTXzdI.htm)|Creature Identification|auto-trad|
|[zB1mqE9IeyfyQDnn.htm](journals-pages/zB1mqE9IeyfyQDnn.htm)|Exploration Activities|auto-trad|
|[ZewC2i5YdZPsWO8X.htm](journals-pages/ZewC2i5YdZPsWO8X.htm)|Mummy|auto-trad|
|[zj9z2BwVX6TLHdx3.htm](journals-pages/zj9z2BwVX6TLHdx3.htm)|Wellspring Mage (Class Archetype)|auto-trad|
|[ZJHhPFjLnizAaUM1.htm](journals-pages/ZJHhPFjLnizAaUM1.htm)|Cleric|auto-trad|
|[zkiLWWYzzqoxmN2J.htm](journals-pages/zkiLWWYzzqoxmN2J.htm)|Earth Domain|auto-trad|
|[zVcfWP1ac0JhNEhY.htm](journals-pages/zVcfWP1ac0JhNEhY.htm)|Quick Adventure Groups|auto-trad|
|[zxY04QNfB90gjLoh.htm](journals-pages/zxY04QNfB90gjLoh.htm)|Magic Warrior|auto-trad|
|[zymZw8KQe0nkRf5L.htm](journals-pages/zymZw8KQe0nkRf5L.htm)|Victory Points|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0e2o62hyCxbIkVoC.htm](journals-pages/0e2o62hyCxbIkVoC.htm)|Assassin|Asesino|modificada|
|[0ZXiLwvBG20MzkO6.htm](journals-pages/0ZXiLwvBG20MzkO6.htm)|Scrollmaster|Scrollmaster|modificada|
|[2CbiV0VtDKZfQNte.htm](journals-pages/2CbiV0VtDKZfQNte.htm)|Viking|Vikingo|modificada|
|[4VMzG36Pm1oivS6Y.htm](journals-pages/4VMzG36Pm1oivS6Y.htm)|Beastmaster|Beastmaster|modificada|
|[4YtHynO8i1a33zo9.htm](journals-pages/4YtHynO8i1a33zo9.htm)|Flexible Spellcaster (Class Archetype)|Hechicero Flexible (Arquetipo de Clase)|modificada|
|[6A9zBmTyt8cn6ioa.htm](journals-pages/6A9zBmTyt8cn6ioa.htm)|Eldritch Archer|Eldritch Archer|modificada|
|[6bDpXy7pQdGrd2og.htm](journals-pages/6bDpXy7pQdGrd2og.htm)|Star Domain|Dominio Estrella|modificada|
|[8RYKz1WDPMJBmMNt.htm](journals-pages/8RYKz1WDPMJBmMNt.htm)|Game Hunter|Cazador|modificada|
|[ackdHdIAmSb0AiVL.htm](journals-pages/ackdHdIAmSb0AiVL.htm)|Loremaster|Loremaster|modificada|
|[BwPUY2X7TSmlJj5c.htm](journals-pages/BwPUY2X7TSmlJj5c.htm)|Duelist|Duelista|modificada|
|[C3P0TycdFCN02p9u.htm](journals-pages/C3P0TycdFCN02p9u.htm)|Bastion|Bastión|modificada|
|[cjD6YVPQpmYiOPdC.htm](journals-pages/cjD6YVPQpmYiOPdC.htm)|Terrain in Encounters|Terreno en Encuentros|modificada|
|[CMgYob7Cy4meoQKg.htm](journals-pages/CMgYob7Cy4meoQKg.htm)|Ranger|Ranger|modificada|
|[DJiYP5tFBrBMD0We.htm](journals-pages/DJiYP5tFBrBMD0We.htm)|Sorcerer|Hechicero|modificada|
|[drgCQcXZbIJU0Zhw.htm](journals-pages/drgCQcXZbIJU0Zhw.htm)|Wrestler|Luchador|modificada|
|[Edn6qQNVcEVpkhPl.htm](journals-pages/Edn6qQNVcEVpkhPl.htm)|Detecting Creatures|Detección de criaturas|modificada|
|[EWiUv3UiR3RHSGlA.htm](journals-pages/EWiUv3UiR3RHSGlA.htm)|Cathartic Mage|Cathartic Mage|modificada|
|[F5gBiVGbS8YSgPSI.htm](journals-pages/F5gBiVGbS8YSgPSI.htm)|Sixth Pillar|Sexto Pilar|modificada|
|[f7tFLR7aFonXTLQa.htm](journals-pages/f7tFLR7aFonXTLQa.htm)|Undead Master|Maestro muerto viviente|modificada|
|[G38usCLuTDmFYw7V.htm](journals-pages/G38usCLuTDmFYw7V.htm)|Zombie|Zombie|modificada|
|[G3AtnAkIKNHrahmd.htm](journals-pages/G3AtnAkIKNHrahmd.htm)|Vampire|Vampiro|modificada|
|[G9Fzy5ZK4KtAmcFb.htm](journals-pages/G9Fzy5ZK4KtAmcFb.htm)|Snarecrafter|Snarecrafter|modificada|
|[gbBf6x89m4SEFpsL.htm](journals-pages/gbBf6x89m4SEFpsL.htm)|Druid|Druida|modificada|
|[HvbDEgCsLbzuMRiR.htm](journals-pages/HvbDEgCsLbzuMRiR.htm)|Poisoner|Envenenador|modificada|
|[HZHzpATOyhTjUDrR.htm](journals-pages/HZHzpATOyhTjUDrR.htm)|Alter Ego|Alter Ego|modificada|
|[iH3Vrt35DyUbhxZj.htm](journals-pages/iH3Vrt35DyUbhxZj.htm)|Mammoth Lord|Mammoth Lord|modificada|
|[j5uZbCwoHOOEO1bC.htm](journals-pages/j5uZbCwoHOOEO1bC.htm)|Runescarred|Runescarred|modificada|
|[JzUDumJ3Dlyame4z.htm](journals-pages/JzUDumJ3Dlyame4z.htm)|Drow Shootist|Drow Shootist|modificada|
|[kAMseeY0TewNmnLQ.htm](journals-pages/kAMseeY0TewNmnLQ.htm)|Deep Backgrounds|Fondos profundos|modificada|
|[kVC4kgYKbhqPsaDt.htm](journals-pages/kVC4kgYKbhqPsaDt.htm)|Rogue|Rogue|modificada|
|[LgkfmZnFP3TWtkuy.htm](journals-pages/LgkfmZnFP3TWtkuy.htm)|Living Vessel|Recipiente Viviente|modificada|
|[M5QOocGyP4zkMo9m.htm](journals-pages/M5QOocGyP4zkMo9m.htm)|Horizon Walker|Horizon Walker|modificada|
|[MKU4d2hIpLuXGN2J.htm](journals-pages/MKU4d2hIpLuXGN2J.htm)|Shadowdancer|Shadowdancer|modificada|
|[N01RLMQPaSeoHEuL.htm](journals-pages/N01RLMQPaSeoHEuL.htm)|Martial Artist|Artista Marcial|modificada|
|[ngVnNmi1Qke3lTy0.htm](journals-pages/ngVnNmi1Qke3lTy0.htm)|Oracle|Oracle|modificada|
|[o71hqcfzhCKXcSml.htm](journals-pages/o71hqcfzhCKXcSml.htm)|Archer|Arquero|modificada|
|[O79hOcsaQyj3aQC5.htm](journals-pages/O79hOcsaQyj3aQC5.htm)|Archaeologist|Arqueólogo|modificada|
|[oMIA3aFKvpuV9f2H.htm](journals-pages/oMIA3aFKvpuV9f2H.htm)|Elementalist (Class Archetype)|Elementalista (Arquetipo de clase)|modificada|
|[PVrMSF93KIejBTMm.htm](journals-pages/PVrMSF93KIejBTMm.htm)|Critical Specialization Effects|Efectos de especialización crítica|modificada|
|[QSk78hQR3zskMlq2.htm](journals-pages/QSk78hQR3zskMlq2.htm)|Cities Domain|Dominio de las ciudades|modificada|
|[rhDvoOHAhAlABiae.htm](journals-pages/rhDvoOHAhAlABiae.htm)|Acrobat|Acróbata|modificada|
|[rtAFOZOjWHC2zRNO.htm](journals-pages/rtAFOZOjWHC2zRNO.htm)|Vigilante|Vigilante|modificada|
|[sAFWD8D0RKH4m25n.htm](journals-pages/sAFWD8D0RKH4m25n.htm)|Dual-Weapon Warrior|Guerrero de doble arma|modificada|
|[SAnmegCTIqGW9S7S.htm](journals-pages/SAnmegCTIqGW9S7S.htm)|Family Domain|Dominio familiar|modificada|
|[SiMXtLf4YQeigzIE.htm](journals-pages/SiMXtLf4YQeigzIE.htm)|Senses|Sentidos|modificada|
|[ttkG4geKXTao5pku.htm](journals-pages/ttkG4geKXTao5pku.htm)|Alkenstar Agent|Agente Alkenstar|modificada|
|[uWJfFysCcoU1o2Hs.htm](journals-pages/uWJfFysCcoU1o2Hs.htm)|Skill Actions|Acciones de Habilidad|modificada|
|[uxdPLsxY8fNrenDR.htm](journals-pages/uxdPLsxY8fNrenDR.htm)|Death and Dying|Muerte y Morir|modificada|
|[uYCTmos3yZtbD9qs.htm](journals-pages/uYCTmos3yZtbD9qs.htm)|Barbarian|Bárbaro|modificada|
|[WvleaXPfigawBvtN.htm](journals-pages/WvleaXPfigawBvtN.htm)|Exorcist|Exorcista|modificada|
|[Y3DFBCWiM9GBIlfl.htm](journals-pages/Y3DFBCWiM9GBIlfl.htm)|Moon Domain|Dominio de la Luna|modificada|
|[yaR8E2drX6pFUFCK.htm](journals-pages/yaR8E2drX6pFUFCK.htm)|Familiar Master|Maestro Familiar|modificada|
|[ydbCjJ9PPmRzZhDN.htm](journals-pages/ydbCjJ9PPmRzZhDN.htm)|Creation Domain|Dominio de la creación|modificada|
|[ydELQFQq0lGLTvrA.htm](journals-pages/ydELQFQq0lGLTvrA.htm)|Time Mage|Mago del Tiempo|modificada|
|[Yepac621abWWw5qj.htm](journals-pages/Yepac621abWWw5qj.htm)|Monster Abilities|Habilidades del Monstruo|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
