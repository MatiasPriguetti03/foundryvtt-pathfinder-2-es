# Estado de la traducción (ancestries)

 * **oficial**: 11
 * **auto-trad**: 25


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[4BL5wf1VF9feC2rY.htm](ancestries/4BL5wf1VF9feC2rY.htm)|Kitsune|auto-trad|
|[58rL5sg2y4arW1i5.htm](ancestries/58rL5sg2y4arW1i5.htm)|Skeleton|auto-trad|
|[6F2fSFC1Eo1JdpY4.htm](ancestries/6F2fSFC1Eo1JdpY4.htm)|Poppet|auto-trad|
|[c4secsSNG2AO7I5i.htm](ancestries/c4secsSNG2AO7I5i.htm)|Goloma|auto-trad|
|[cdhgByGG1WtuaK73.htm](ancestries/cdhgByGG1WtuaK73.htm)|Leshy|auto-trad|
|[cLtOGIkuSSa4UDHY.htm](ancestries/cLtOGIkuSSa4UDHY.htm)|Vanara|auto-trad|
|[dw2K1AJR9mQ25nDP.htm](ancestries/dw2K1AJR9mQ25nDP.htm)|Kashrishi|auto-trad|
|[FXlXmNBFiiz9oasi.htm](ancestries/FXlXmNBFiiz9oasi.htm)|Fleshwarp|auto-trad|
|[GfLwE884NoRC7cRi.htm](ancestries/GfLwE884NoRC7cRi.htm)|Android|auto-trad|
|[GXcC6oVa5quzgNHD.htm](ancestries/GXcC6oVa5quzgNHD.htm)|Strix|auto-trad|
|[hIA3qiUsxvLZXrFP.htm](ancestries/hIA3qiUsxvLZXrFP.htm)|Fetchling|auto-trad|
|[HWEgF7Gmoq55VhTL.htm](ancestries/HWEgF7Gmoq55VhTL.htm)|Lizardfolk|auto-trad|
|[hXM5jXezIki1cMI2.htm](ancestries/hXM5jXezIki1cMI2.htm)|Grippli|auto-trad|
|[J7T7bDLaQGoY1sMF.htm](ancestries/J7T7bDLaQGoY1sMF.htm)|Nagaji|auto-trad|
|[kYsBAJ103T44agJF.htm](ancestries/kYsBAJ103T44agJF.htm)|Automaton|auto-trad|
|[piNLXUrm9iaGqD2i.htm](ancestries/piNLXUrm9iaGqD2i.htm)|Hobgoblin|auto-trad|
|[q6rsqYARyOGXZA8F.htm](ancestries/q6rsqYARyOGXZA8F.htm)|Shoony|auto-trad|
|[TQEqWqc7BYiadUdY.htm](ancestries/TQEqWqc7BYiadUdY.htm)|Anadi|auto-trad|
|[TRqoeYfGAFjQbviF.htm](ancestries/TRqoeYfGAFjQbviF.htm)|Sprite|auto-trad|
|[tSurOqRcfumadTfr.htm](ancestries/tSurOqRcfumadTfr.htm)|Ghoran|auto-trad|
|[tZn4qIHCUA6wCdnI.htm](ancestries/tZn4qIHCUA6wCdnI.htm)|Conrasu|auto-trad|
|[u1VJEXsVlmh3Fyx0.htm](ancestries/u1VJEXsVlmh3Fyx0.htm)|Vishkanya|auto-trad|
|[vxbQ1Yw4qwgjTzqo.htm](ancestries/vxbQ1Yw4qwgjTzqo.htm)|Gnoll|auto-trad|
|[x1YinOddgUxwOLqP.htm](ancestries/x1YinOddgUxwOLqP.htm)|Shisk|auto-trad|
|[yFoojz6q3ZjvceFw.htm](ancestries/yFoojz6q3ZjvceFw.htm)|Azarketi|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[18xDKYPDBLEv2myX.htm](ancestries/18xDKYPDBLEv2myX.htm)|Tengu|Tengu|oficial|
|[7oQxL6wgsokD3QXG.htm](ancestries/7oQxL6wgsokD3QXG.htm)|Kobold|Kóbold|oficial|
|[972EkpJOPv9KkQIW.htm](ancestries/972EkpJOPv9KkQIW.htm)|Catfolk|Félido|oficial|
|[BYj5ZvlXZdpaEgA6.htm](ancestries/BYj5ZvlXZdpaEgA6.htm)|Dwarf|Enano|oficial|
|[CYlfsYLJcBOgqKtD.htm](ancestries/CYlfsYLJcBOgqKtD.htm)|Gnome|Gnomo|oficial|
|[GgZAHbrjnzWOZy2v.htm](ancestries/GgZAHbrjnzWOZy2v.htm)|Halfling|Mediano|oficial|
|[IiG7DgeLWYrSNXuX.htm](ancestries/IiG7DgeLWYrSNXuX.htm)|Human|Humano|oficial|
|[lSGWXjcbOa6O5fTx.htm](ancestries/lSGWXjcbOa6O5fTx.htm)|Orc|Orco|oficial|
|[P6PcVnCkh4XMdefw.htm](ancestries/P6PcVnCkh4XMdefw.htm)|Ratfolk|Rátido|oficial|
|[PgKmsA2aKdbLU6O0.htm](ancestries/PgKmsA2aKdbLU6O0.htm)|Elf|Elfo|oficial|
|[sQfjTMDaZbT9DThq.htm](ancestries/sQfjTMDaZbT9DThq.htm)|Goblin|Goblin|oficial|
