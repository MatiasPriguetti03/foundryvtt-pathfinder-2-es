# Estado de la traducción (extinction-curse-bestiary)

 * **auto-trad**: 109
 * **modificada**: 59


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0fe7PVMIq92fkUJK.htm](extinction-curse-bestiary/0fe7PVMIq92fkUJK.htm)|Dream Pollen Pod|auto-trad|
|[0pw0r1w8vO7aRocN.htm](extinction-curse-bestiary/0pw0r1w8vO7aRocN.htm)|Resin-seep Xulgath|auto-trad|
|[0VzVpqN3Yp0IY3C1.htm](extinction-curse-bestiary/0VzVpqN3Yp0IY3C1.htm)|Nihiris|auto-trad|
|[1CjTIaMYUvQUkQI2.htm](extinction-curse-bestiary/1CjTIaMYUvQUkQI2.htm)|Mukradi Summoning Runes|auto-trad|
|[1HIqZAGVSR2pAiY6.htm](extinction-curse-bestiary/1HIqZAGVSR2pAiY6.htm)|Axiomatic Polymorph Trap|auto-trad|
|[1HYH5BgFGtFxVMpc.htm](extinction-curse-bestiary/1HYH5BgFGtFxVMpc.htm)|Krooth Summoning Rune|auto-trad|
|[1vOre5O8t3pQPUp6.htm](extinction-curse-bestiary/1vOre5O8t3pQPUp6.htm)|Herecite of Zevgavizeb|auto-trad|
|[25BUnECSaTWkHqZQ.htm](extinction-curse-bestiary/25BUnECSaTWkHqZQ.htm)|Convergence Lattice|auto-trad|
|[29NHB8DNNAbEk5Va.htm](extinction-curse-bestiary/29NHB8DNNAbEk5Va.htm)|Counteflora|auto-trad|
|[2PxSX2cqEbSFjj3D.htm](extinction-curse-bestiary/2PxSX2cqEbSFjj3D.htm)|Aukashungi Swarm|auto-trad|
|[2XRzA5GZeDk88Y2z.htm](extinction-curse-bestiary/2XRzA5GZeDk88Y2z.htm)|War Sauropelta|auto-trad|
|[3Fxih1eU4IXABxpy.htm](extinction-curse-bestiary/3Fxih1eU4IXABxpy.htm)|Mechanical Carny|auto-trad|
|[4Abc5gg8ac5ixGx1.htm](extinction-curse-bestiary/4Abc5gg8ac5ixGx1.htm)|Hallowed Wheel|auto-trad|
|[4eQVYZ7sH7O8mw3R.htm](extinction-curse-bestiary/4eQVYZ7sH7O8mw3R.htm)|Brughadatch|auto-trad|
|[4L38nr9t17thMZrB.htm](extinction-curse-bestiary/4L38nr9t17thMZrB.htm)|Caustic Dart Trap|auto-trad|
|[4lJiW36oO2iG7jCB.htm](extinction-curse-bestiary/4lJiW36oO2iG7jCB.htm)|Xulgath Deepmouth|auto-trad|
|[5G8Iokror8QtBhbx.htm](extinction-curse-bestiary/5G8Iokror8QtBhbx.htm)|Urdefhan Dominator|auto-trad|
|[7p4RWS26W5k6vCkH.htm](extinction-curse-bestiary/7p4RWS26W5k6vCkH.htm)|Cat Sith|auto-trad|
|[7YJHi9niIKpFXXrf.htm](extinction-curse-bestiary/7YJHi9niIKpFXXrf.htm)|Celestial Menagerie Bruiser|auto-trad|
|[88VcnxbKPF2QkPiF.htm](extinction-curse-bestiary/88VcnxbKPF2QkPiF.htm)|Bitter Truth Bandit|auto-trad|
|[a2FCggU8UCQl6RDx.htm](extinction-curse-bestiary/a2FCggU8UCQl6RDx.htm)|Juvenile Boar|auto-trad|
|[aqcDvkIaKwJdMEAO.htm](extinction-curse-bestiary/aqcDvkIaKwJdMEAO.htm)|Iffdahsil|auto-trad|
|[aUDIi3Z0N7IhzaT0.htm](extinction-curse-bestiary/aUDIi3Z0N7IhzaT0.htm)|Giant Aukashungi|auto-trad|
|[B18UXeicuV8RF8kP.htm](extinction-curse-bestiary/B18UXeicuV8RF8kP.htm)|Ghost Crystal Cloud|auto-trad|
|[B2jQKaAXO7LofE7L.htm](extinction-curse-bestiary/B2jQKaAXO7LofE7L.htm)|Muurfeli|auto-trad|
|[b7ADAguVQLHCauWO.htm](extinction-curse-bestiary/b7ADAguVQLHCauWO.htm)|Poisoned Secret Door Trap|auto-trad|
|[B9fl34W1jENDoYqc.htm](extinction-curse-bestiary/B9fl34W1jENDoYqc.htm)|Ammut|auto-trad|
|[BORxkpaFBSCyB1f1.htm](extinction-curse-bestiary/BORxkpaFBSCyB1f1.htm)|Tallow Ooze|auto-trad|
|[C07vwQbz6mKwLRGY.htm](extinction-curse-bestiary/C07vwQbz6mKwLRGY.htm)|Gahlepod|auto-trad|
|[CknWKRO1xUHBL5Km.htm](extinction-curse-bestiary/CknWKRO1xUHBL5Km.htm)|Host of Spirits|auto-trad|
|[CQ2682vd4bUtvKQX.htm](extinction-curse-bestiary/CQ2682vd4bUtvKQX.htm)|Runkrunk|auto-trad|
|[D0laesU2eB1VOZTX.htm](extinction-curse-bestiary/D0laesU2eB1VOZTX.htm)|Shoony Tiller|auto-trad|
|[DJyAXPHQy7OUUw00.htm](extinction-curse-bestiary/DJyAXPHQy7OUUw00.htm)|Headless Xulgath|auto-trad|
|[dP6sDHTZrDcV2I9w.htm](extinction-curse-bestiary/dP6sDHTZrDcV2I9w.htm)|Abberton Ruffian|auto-trad|
|[DXNDZNHSZxlNXJnk.htm](extinction-curse-bestiary/DXNDZNHSZxlNXJnk.htm)|Convergent Soldier|auto-trad|
|[EBDDeBHGGZ8xvIM6.htm](extinction-curse-bestiary/EBDDeBHGGZ8xvIM6.htm)|Corrosive Lizard|auto-trad|
|[f42l4cQyhNOLs19j.htm](extinction-curse-bestiary/f42l4cQyhNOLs19j.htm)|Thunderstone Cascade Trap|auto-trad|
|[f6uVOvKEkojOf9Ab.htm](extinction-curse-bestiary/f6uVOvKEkojOf9Ab.htm)|Swardlands Delinquent|auto-trad|
|[fvGqCYfVAI3KXnSl.htm](extinction-curse-bestiary/fvGqCYfVAI3KXnSl.htm)|Xulgath Bomber|auto-trad|
|[FZqrluaaz5vhXEZ9.htm](extinction-curse-bestiary/FZqrluaaz5vhXEZ9.htm)|Xulgath Gutrager|auto-trad|
|[G2i7CUiYmyl1yjQo.htm](extinction-curse-bestiary/G2i7CUiYmyl1yjQo.htm)|Buzzing Latch Rune|auto-trad|
|[gN8VuDZ8b9dp0Ep0.htm](extinction-curse-bestiary/gN8VuDZ8b9dp0Ep0.htm)|Giant Flea|auto-trad|
|[GuJJJzmjLKbkZUur.htm](extinction-curse-bestiary/GuJJJzmjLKbkZUur.htm)|Deghuun (Child of Mhar)|auto-trad|
|[HbxPY2GSxhRu4rVi.htm](extinction-curse-bestiary/HbxPY2GSxhRu4rVi.htm)|Lion Visitant|auto-trad|
|[HCxhltvoCdy3RXlH.htm](extinction-curse-bestiary/HCxhltvoCdy3RXlH.htm)|Saurian Warmonger|auto-trad|
|[HEeRO5IF4lAGfDqE.htm](extinction-curse-bestiary/HEeRO5IF4lAGfDqE.htm)|Barking Stag|auto-trad|
|[HFAhDmrkxg6YLhdF.htm](extinction-curse-bestiary/HFAhDmrkxg6YLhdF.htm)|Blood Wolf|auto-trad|
|[I31kJb0DRkzTn8iX.htm](extinction-curse-bestiary/I31kJb0DRkzTn8iX.htm)|Xulgath Spinesnapper|auto-trad|
|[I6i1uvFI7VCTyVbM.htm](extinction-curse-bestiary/I6i1uvFI7VCTyVbM.htm)|Xulgath Herd-Tender|auto-trad|
|[i6t819VnEr1ESCAd.htm](extinction-curse-bestiary/i6t819VnEr1ESCAd.htm)|Vavakia|auto-trad|
|[igd75rbnUbYBNyYw.htm](extinction-curse-bestiary/igd75rbnUbYBNyYw.htm)|Doblagub|auto-trad|
|[IRon2oG76IG28Z4m.htm](extinction-curse-bestiary/IRon2oG76IG28Z4m.htm)|Urdefhan High Tormentor|auto-trad|
|[iUA3E379TDoEAXz7.htm](extinction-curse-bestiary/iUA3E379TDoEAXz7.htm)|Qurashith|auto-trad|
|[IVpSJcoRLGgUfqW7.htm](extinction-curse-bestiary/IVpSJcoRLGgUfqW7.htm)|Harrow Doll|auto-trad|
|[J3mRwgqkOlOi44Xv.htm](extinction-curse-bestiary/J3mRwgqkOlOi44Xv.htm)|Stabbing Sentinel|auto-trad|
|[jaUUH2i5UQZYQqab.htm](extinction-curse-bestiary/jaUUH2i5UQZYQqab.htm)|Muse Phantom|auto-trad|
|[jCcA2ca8VnnDtVU9.htm](extinction-curse-bestiary/jCcA2ca8VnnDtVU9.htm)|Bugaboo|auto-trad|
|[JRvLtSt9cda4pc8z.htm](extinction-curse-bestiary/JRvLtSt9cda4pc8z.htm)|Obsidian Golem|auto-trad|
|[jxGA1C8xX0WkJGI4.htm](extinction-curse-bestiary/jxGA1C8xX0WkJGI4.htm)|Yaganty|auto-trad|
|[k6mTbQPIlez0QhYg.htm](extinction-curse-bestiary/k6mTbQPIlez0QhYg.htm)|Xulgath Demon-Caller|auto-trad|
|[KXNSBeUWLxMfd2Zg.htm](extinction-curse-bestiary/KXNSBeUWLxMfd2Zg.htm)|Pinacosaurus|auto-trad|
|[KXY9CRct0VKhiNob.htm](extinction-curse-bestiary/KXY9CRct0VKhiNob.htm)|Raving Spirit|auto-trad|
|[M3pYUyhrmylFqS0B.htm](extinction-curse-bestiary/M3pYUyhrmylFqS0B.htm)|Guthallath Rockslide|auto-trad|
|[mlktvPVq3mb5DTpP.htm](extinction-curse-bestiary/mlktvPVq3mb5DTpP.htm)|Suffering Xulgaths|auto-trad|
|[MLYkh9pZpjBI1wpN.htm](extinction-curse-bestiary/MLYkh9pZpjBI1wpN.htm)|Imperious Darkside Mirror|auto-trad|
|[mTQYkgy9lfrv2yGG.htm](extinction-curse-bestiary/mTQYkgy9lfrv2yGG.htm)|Masks of Aroden's Guises|auto-trad|
|[N8ekzW9snBA2YAKv.htm](extinction-curse-bestiary/N8ekzW9snBA2YAKv.htm)|Cu Sith|auto-trad|
|[nFYpYIL5OUCFZCr9.htm](extinction-curse-bestiary/nFYpYIL5OUCFZCr9.htm)|Bogeyman|auto-trad|
|[NITxPebTwserqASe.htm](extinction-curse-bestiary/NITxPebTwserqASe.htm)|Xilvirek|auto-trad|
|[nJBpLZFgEuF7uZAZ.htm](extinction-curse-bestiary/nJBpLZFgEuF7uZAZ.htm)|Witch-Priests' Curse|auto-trad|
|[OCrQtfKDFpLedE13.htm](extinction-curse-bestiary/OCrQtfKDFpLedE13.htm)|Shoony Militia Member|auto-trad|
|[otSfn7djr37Bdejj.htm](extinction-curse-bestiary/otSfn7djr37Bdejj.htm)|Arskuva the Gnasher|auto-trad|
|[PpAOxz6ayvkn0fMK.htm](extinction-curse-bestiary/PpAOxz6ayvkn0fMK.htm)|Ararda|auto-trad|
|[PYvtflsrA9ITevEq.htm](extinction-curse-bestiary/PYvtflsrA9ITevEq.htm)|Urdefhan Hunter|auto-trad|
|[QcJtBai5JViNFqUC.htm](extinction-curse-bestiary/QcJtBai5JViNFqUC.htm)|Viskithrel|auto-trad|
|[qXC4lGSWxqIhO5tr.htm](extinction-curse-bestiary/qXC4lGSWxqIhO5tr.htm)|Hooklimb Xulgath|auto-trad|
|[QZHvPM0Yatn8oxsj.htm](extinction-curse-bestiary/QZHvPM0Yatn8oxsj.htm)|Bugul Noz|auto-trad|
|[R3SbxfWLp8xUvXUP.htm](extinction-curse-bestiary/R3SbxfWLp8xUvXUP.htm)|Elysian Sheep|auto-trad|
|[RIZDryL3Wnk6ucks.htm](extinction-curse-bestiary/RIZDryL3Wnk6ucks.htm)|Luminous Ooze|auto-trad|
|[Rma6WvkTkRW1H0ro.htm](extinction-curse-bestiary/Rma6WvkTkRW1H0ro.htm)|Angry Vegetation|auto-trad|
|[RVPH12vtOWMuJx0L.htm](extinction-curse-bestiary/RVPH12vtOWMuJx0L.htm)|Spiked Barricade Trap|auto-trad|
|[rxGByemLAAeM4h29.htm](extinction-curse-bestiary/rxGByemLAAeM4h29.htm)|Smoldering Leopard|auto-trad|
|[s6Yfy1AgPp4ky7QI.htm](extinction-curse-bestiary/s6Yfy1AgPp4ky7QI.htm)|Drunken Brawler|auto-trad|
|[s7447jKTaAd4r3Oa.htm](extinction-curse-bestiary/s7447jKTaAd4r3Oa.htm)|Ciza|auto-trad|
|[SAqbWugp2NcdEtrr.htm](extinction-curse-bestiary/SAqbWugp2NcdEtrr.htm)|Bone Croupier|auto-trad|
|[SBz0ylPxEaUhG2aZ.htm](extinction-curse-bestiary/SBz0ylPxEaUhG2aZ.htm)|Xulgath Roughrider|auto-trad|
|[sQvm52N0E5ulBaaq.htm](extinction-curse-bestiary/sQvm52N0E5ulBaaq.htm)|Faceless Butcher|auto-trad|
|[TdqGpBasgDOUqmNp.htm](extinction-curse-bestiary/TdqGpBasgDOUqmNp.htm)|Zuipnyrn|auto-trad|
|[tg5SrKu228LZkoC0.htm](extinction-curse-bestiary/tg5SrKu228LZkoC0.htm)|Xulgath Hardscale|auto-trad|
|[u6b7tlXDXMxmkdLO.htm](extinction-curse-bestiary/u6b7tlXDXMxmkdLO.htm)|Bogey|auto-trad|
|[UNXMz1Q7hinlaxek.htm](extinction-curse-bestiary/UNXMz1Q7hinlaxek.htm)|Maze of Mirrors|auto-trad|
|[uPWqYc05YQ0Q4i0g.htm](extinction-curse-bestiary/uPWqYc05YQ0Q4i0g.htm)|Xulgath Stoneliege|auto-trad|
|[uUcqr8gPQIw5govC.htm](extinction-curse-bestiary/uUcqr8gPQIw5govC.htm)|Xulgath Skirmisher|auto-trad|
|[V4LPaPABVm9YYqdH.htm](extinction-curse-bestiary/V4LPaPABVm9YYqdH.htm)|Crushing Gate Trap|auto-trad|
|[vBUYl8mREwhO2Vst.htm](extinction-curse-bestiary/vBUYl8mREwhO2Vst.htm)|Catacomb Cave-In|auto-trad|
|[wiNeCuB0DXt2ReAx.htm](extinction-curse-bestiary/wiNeCuB0DXt2ReAx.htm)|Xulgath Mage|auto-trad|
|[WOeSP1KLzo0qTpZu.htm](extinction-curse-bestiary/WOeSP1KLzo0qTpZu.htm)|Chimpanzee Visitant|auto-trad|
|[wQHcJGJ9rSyFQFPB.htm](extinction-curse-bestiary/wQHcJGJ9rSyFQFPB.htm)|Envenomed Thorns Trap|auto-trad|
|[wS2SN0dnQMybLzSA.htm](extinction-curse-bestiary/wS2SN0dnQMybLzSA.htm)|Flea Swarm|auto-trad|
|[x2XcPDPLeCAXITlZ.htm](extinction-curse-bestiary/x2XcPDPLeCAXITlZ.htm)|Iridescent Elephant|auto-trad|
|[Xva8XyRrsHB6S7e8.htm](extinction-curse-bestiary/Xva8XyRrsHB6S7e8.htm)|Saurian Worldwatcher|auto-trad|
|[xWsfcDcgbubVpjow.htm](extinction-curse-bestiary/xWsfcDcgbubVpjow.htm)|Xulgath Bilebearer|auto-trad|
|[YaKW9i3v9nn8Blij.htm](extinction-curse-bestiary/YaKW9i3v9nn8Blij.htm)|Xulgath Thoughtmaw|auto-trad|
|[YAKZUBOth75W2mWT.htm](extinction-curse-bestiary/YAKZUBOth75W2mWT.htm)|Vermlek|auto-trad|
|[YS8UvVGFgHP2TrY3.htm](extinction-curse-bestiary/YS8UvVGFgHP2TrY3.htm)|Shoony Hierarch|auto-trad|
|[Z3VrS25uw9vD2sek.htm](extinction-curse-bestiary/Z3VrS25uw9vD2sek.htm)|Vitalia|auto-trad|
|[Z5E6UrUkiVW2UQRO.htm](extinction-curse-bestiary/Z5E6UrUkiVW2UQRO.htm)|Explosive Furniture Trap|auto-trad|
|[ZG9ZJ5BZrZ9MS3h3.htm](extinction-curse-bestiary/ZG9ZJ5BZrZ9MS3h3.htm)|Echoes of Faith|auto-trad|
|[zPXXUNE3m9VdrF9z.htm](extinction-curse-bestiary/zPXXUNE3m9VdrF9z.htm)|Thief's Trap|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[16cZVZxsXVRHfuuQ.htm](extinction-curse-bestiary/16cZVZxsXVRHfuuQ.htm)|Jellico Bounce-Bounce|Jellico Bounce-Bounce|modificada|
|[18Y3yoYcEGoLcmDy.htm](extinction-curse-bestiary/18Y3yoYcEGoLcmDy.htm)|Death Drider|Death Drider|modificada|
|[2bu9oGbB7UUTq2sR.htm](extinction-curse-bestiary/2bu9oGbB7UUTq2sR.htm)|Kirosthrek|Kirosthrek|modificada|
|[2Ey2VZ3aQlN4FGHJ.htm](extinction-curse-bestiary/2Ey2VZ3aQlN4FGHJ.htm)|Pin Tingwheely|Pin Tingwheely|modificada|
|[4QgC23j1wzfaCecR.htm](extinction-curse-bestiary/4QgC23j1wzfaCecR.htm)|Pruana Two-punch|Pruana Dos puñetazos|modificada|
|[5tlPKUZroqzycuzD.htm](extinction-curse-bestiary/5tlPKUZroqzycuzD.htm)|Shanchek|Shanchek|modificada|
|[9cwg4cOgvFqtxIQ1.htm](extinction-curse-bestiary/9cwg4cOgvFqtxIQ1.htm)|Gluttondark Babau|Glotón oscuro Babau|modificada|
|[aIT5S2fKgMZ6pVP2.htm](extinction-curse-bestiary/aIT5S2fKgMZ6pVP2.htm)|Ginjana Mindkeeper|Ginjana Mindkeeper|modificada|
|[b5creqUAlBl0Tmmc.htm](extinction-curse-bestiary/b5creqUAlBl0Tmmc.htm)|Leandrus|Leandrus|modificada|
|[BBamjhcpvwXGhlbM.htm](extinction-curse-bestiary/BBamjhcpvwXGhlbM.htm)|Cavnakash|Cavnakash|modificada|
|[beb9LqlOBFseROnY.htm](extinction-curse-bestiary/beb9LqlOBFseROnY.htm)|Adrivallo|Adrivallo|modificada|
|[bJUlb2DxT1xyaYAp.htm](extinction-curse-bestiary/bJUlb2DxT1xyaYAp.htm)|Darklands Alchemical Golem|Darklands Alchemical Golem|modificada|
|[bpTQfx4UixMV3Fja.htm](extinction-curse-bestiary/bpTQfx4UixMV3Fja.htm)|Kharostan|Kharostan|modificada|
|[BsnU2Hf4a3MuXVPn.htm](extinction-curse-bestiary/BsnU2Hf4a3MuXVPn.htm)|Dyzallin's Golem|Dyzallin's Golem|modificada|
|[CNO54boXvXg7xSP6.htm](extinction-curse-bestiary/CNO54boXvXg7xSP6.htm)|Tanessa Fleer|Tanessa Fleer|modificada|
|[CYXoXHr9BoPInIbe.htm](extinction-curse-bestiary/CYXoXHr9BoPInIbe.htm)|Thessekka|Thessekka|modificada|
|[f4H9d0b1vJvxeFqs.htm](extinction-curse-bestiary/f4H9d0b1vJvxeFqs.htm)|Sodden Sentinel|Sodden Sentinel|modificada|
|[Ffj8PyKkBkYNV6pd.htm](extinction-curse-bestiary/Ffj8PyKkBkYNV6pd.htm)|Raptor Guard Wight|Guardia Raptor Wight|modificada|
|[fsPWWmIao1eFTAV4.htm](extinction-curse-bestiary/fsPWWmIao1eFTAV4.htm)|Sarvel Ever-Hunger|Sarvel Ever-Hunger|modificada|
|[G2ICeUU6br5Xem3P.htm](extinction-curse-bestiary/G2ICeUU6br5Xem3P.htm)|Zinogyvaz|Zinogyvaz|modificada|
|[gZBayD2gJu7iZrud.htm](extinction-curse-bestiary/gZBayD2gJu7iZrud.htm)|Ruanna Nyamma|Ruanna Nyamma|modificada|
|[HHy2GazURA6Cx1ee.htm](extinction-curse-bestiary/HHy2GazURA6Cx1ee.htm)|Starved Staff|Bastón hambriento|modificada|
|[hz2uoBS8DFOWshko.htm](extinction-curse-bestiary/hz2uoBS8DFOWshko.htm)|Darricus Stallit|Darricus Stallit|modificada|
|[i3kQzeKcSoxkNYJb.htm](extinction-curse-bestiary/i3kQzeKcSoxkNYJb.htm)|Evora Yarket|Evora Yarket|modificada|
|[IacftcXDDMBNcKbY.htm](extinction-curse-bestiary/IacftcXDDMBNcKbY.htm)|Nemmia Bramblecloak|Nemmia Bramblecloak|modificada|
|[j31HXlZiUqQrAHSB.htm](extinction-curse-bestiary/j31HXlZiUqQrAHSB.htm)|Tashlock Banyan|Tashlock Banyan|modificada|
|[JvSCGGnexk7CmVke.htm](extinction-curse-bestiary/JvSCGGnexk7CmVke.htm)|Viktor Volkano|Viktor Volkano|modificada|
|[jwcMb71QRhMw94Id.htm](extinction-curse-bestiary/jwcMb71QRhMw94Id.htm)|Convergent Kendley Nathrael|Convergente Kendley Nathrael|modificada|
|[L25SQceNMS8IstYI.htm](extinction-curse-bestiary/L25SQceNMS8IstYI.htm)|Horba|Horba|modificada|
|[lh3pcyJlUUtNpWcI.htm](extinction-curse-bestiary/lh3pcyJlUUtNpWcI.htm)|Ledorick Banyan|Ledorick Banyan|modificada|
|[lM9j6lc5MkBlGfzD.htm](extinction-curse-bestiary/lM9j6lc5MkBlGfzD.htm)|Violet|Violeta|modificada|
|[NBCqA1NLRlhMuqGl.htm](extinction-curse-bestiary/NBCqA1NLRlhMuqGl.htm)|Daring Danika|Daring Danika|modificada|
|[NgmbMRekIHiA44lg.htm](extinction-curse-bestiary/NgmbMRekIHiA44lg.htm)|Dyzallin Shraen|Dyzallin Shraen|modificada|
|[oJJspO9P2vDdtMYd.htm](extinction-curse-bestiary/oJJspO9P2vDdtMYd.htm)|The Vanish Man|The Vanish Man|modificada|
|[OqThOmNrdW3HVa7k.htm](extinction-curse-bestiary/OqThOmNrdW3HVa7k.htm)|Zashathal Head-Taker|Zashathal Head-Taker|modificada|
|[oYstle5FAR800UQT.htm](extinction-curse-bestiary/oYstle5FAR800UQT.htm)|Andera Paldreen|Andera Paldreen|modificada|
|[Plig7vUF9cEY1VBJ.htm](extinction-curse-bestiary/Plig7vUF9cEY1VBJ.htm)|Lakkai One-Fang|Lakkai Colmillo Único|modificada|
|[pOlP9ZR0eFS57c27.htm](extinction-curse-bestiary/pOlP9ZR0eFS57c27.htm)|Aives The Smoke Dragon|Aives El Dragón de Humo|modificada|
|[Q3Z0rIINoWYxVSrS.htm](extinction-curse-bestiary/Q3Z0rIINoWYxVSrS.htm)|Corrupted Retainer|Corrupted Retainer|modificada|
|[qCTU0ywtCOgiUM0q.htm](extinction-curse-bestiary/qCTU0ywtCOgiUM0q.htm)|Skarja|Skarja|modificada|
|[QFScy7QFB7PIeTEa.htm](extinction-curse-bestiary/QFScy7QFB7PIeTEa.htm)|Helg Eats-The-Eaters|Helg Eats-The-Eaters|modificada|
|[qKHkamxIPbqxEiwp.htm](extinction-curse-bestiary/qKHkamxIPbqxEiwp.htm)|Delamar Gianvin|Delamar Gianvin|modificada|
|[QMFlW9qrUKWBOF1Q.htm](extinction-curse-bestiary/QMFlW9qrUKWBOF1Q.htm)|Mistress Dusklight|Mistress Dusklight|modificada|
|[RbrUXZyhqbsglVrr.htm](extinction-curse-bestiary/RbrUXZyhqbsglVrr.htm)|Qormintur|Qormintur|modificada|
|[S5pcyiSXhMKrMjcC.htm](extinction-curse-bestiary/S5pcyiSXhMKrMjcC.htm)|Shraen Graveknight|Shraen Graveknight|modificada|
|[sPuh6Ahatn0Vhn7P.htm](extinction-curse-bestiary/sPuh6Ahatn0Vhn7P.htm)|Urushil|Urushil|modificada|
|[Sxip3Rmi0PFpzHNw.htm](extinction-curse-bestiary/Sxip3Rmi0PFpzHNw.htm)|Convergent Giant Eagle|Águila Gigante Convergente|modificada|
|[tPvr9zvUfktMIvYU.htm](extinction-curse-bestiary/tPvr9zvUfktMIvYU.htm)|Stirvyn Banyan|Stirvyn Banyan|modificada|
|[U6jOmV5RAkj32qOi.htm](extinction-curse-bestiary/U6jOmV5RAkj32qOi.htm)|Smiler|Smiler|modificada|
|[UKBR2GdXIdg66Nqm.htm](extinction-curse-bestiary/UKBR2GdXIdg66Nqm.htm)|Ulthadar|Ulthadar|modificada|
|[viTyDNdkTo0JPLb8.htm](extinction-curse-bestiary/viTyDNdkTo0JPLb8.htm)|Kalkek|Kalkek|modificada|
|[vxFQrDnQqUpGpQmK.htm](extinction-curse-bestiary/vxFQrDnQqUpGpQmK.htm)|Kimilekki|Kimilekki|modificada|
|[wBlnbLj8FfgxArQm.htm](extinction-curse-bestiary/wBlnbLj8FfgxArQm.htm)|Lyrt Cozurn|Lyrt Cozurn|modificada|
|[wi94WddixQEID9Jl.htm](extinction-curse-bestiary/wi94WddixQEID9Jl.htm)|Corrupted Priest|Sacerdote corrompido|modificada|
|[wOzDhkm9sd7IygoS.htm](extinction-curse-bestiary/wOzDhkm9sd7IygoS.htm)|Wight Cultist|Wight Cultist|modificada|
|[xGTl3DVCD0etE6MU.htm](extinction-curse-bestiary/xGTl3DVCD0etE6MU.htm)|Ledorick Banyan (Possessed)|Ledorick Banyan (posesión)|modificada|
|[XyHk8ChyuOkJuhVZ.htm](extinction-curse-bestiary/XyHk8ChyuOkJuhVZ.htm)|Guardian of the Faithful|Guardián de los Fieles|modificada|
|[yqH59ltUd0f3kLLL.htm](extinction-curse-bestiary/yqH59ltUd0f3kLLL.htm)|Drow Bodyguard Golem|Drow Guardaespaldas Golem|modificada|
|[YZcw33uhsPHWKcHM.htm](extinction-curse-bestiary/YZcw33uhsPHWKcHM.htm)|Hollow Hush|Hollow Hush|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
