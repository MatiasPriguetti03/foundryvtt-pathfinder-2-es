# Estado de la traducción (feat-effects)

 * **auto-trad**: 240
 * **oficial**: 47
 * **ninguna**: 39
 * **modificada**: 10
 * **vacía**: 2


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[3eHMqVx30JGiJqtM.htm](feat-effects/3eHMqVx30JGiJqtM.htm)|Stance: Twinned Defense|
|[5PIaLkys5ZqP2BUv.htm](feat-effects/5PIaLkys5ZqP2BUv.htm)|Effect: Primal Aegis|
|[6ctQFQfSZ6o1uyyZ.htm](feat-effects/6ctQFQfSZ6o1uyyZ.htm)|Stance: Bullet Dancer Stance|
|[94MzLpLykQIWKcA1.htm](feat-effects/94MzLpLykQIWKcA1.htm)|Effect: Deteriorated|
|[cGwFYusGTsJR3x3P.htm](feat-effects/cGwFYusGTsJR3x3P.htm)|Effect: Under Control|
|[CW4zphOOpeaLJIWc.htm](feat-effects/CW4zphOOpeaLJIWc.htm)|Effect: Recall Under Pressure|
|[er5tvDNvpbcnlbHQ.htm](feat-effects/er5tvDNvpbcnlbHQ.htm)|Stance: Inspiring Marshal Stance|
|[gN1LbKYQgi8Fx98V.htm](feat-effects/gN1LbKYQgi8Fx98V.htm)|Effect: Anadi Venom|
|[GvqB4M8LrHpzYEvl.htm](feat-effects/GvqB4M8LrHpzYEvl.htm)|Stance: Fane's Fourberie|
|[iaZ6P59YVhdnFIN8.htm](feat-effects/iaZ6P59YVhdnFIN8.htm)|Effect: Guided Skill|
|[jACKRmVfr9ATsmwg.htm](feat-effects/jACKRmVfr9ATsmwg.htm)|Effect: Devrin's Cunning Stance|
|[k8gB0eDuAlGRoeQj.htm](feat-effects/k8gB0eDuAlGRoeQj.htm)|Effect: Benevolent Spirit Deck|
|[kDTiRg9vVOYNnTyr.htm](feat-effects/kDTiRg9vVOYNnTyr.htm)|Stance: Powder Punch Stance|
|[kyrvZfZfzKK1vx9b.htm](feat-effects/kyrvZfZfzKK1vx9b.htm)|Stance: Devrin's Cunning Stance|
|[mark4VEQoynfYNBF.htm](feat-effects/mark4VEQoynfYNBF.htm)|Stance: Graceful Poise|
|[MSkspeBsbXm6LQ19.htm](feat-effects/MSkspeBsbXm6LQ19.htm)|Effect: Harrow the Fiend|
|[N2CSGvtPXloOEPrK.htm](feat-effects/N2CSGvtPXloOEPrK.htm)|Effect: Giant's Lunge|
|[NWOmJ6WJFheaGhho.htm](feat-effects/NWOmJ6WJFheaGhho.htm)|Stance: Mobile Shot Stance|
|[O8qithYQCv3e7DUQ.htm](feat-effects/O8qithYQCv3e7DUQ.htm)|Effect: Elementalist Dedication|
|[PS17dsXkTdQmOv7w.htm](feat-effects/PS17dsXkTdQmOv7w.htm)|Stance: Buckler Dance|
|[Q0DKJRnDuuUnLpvn.htm](feat-effects/Q0DKJRnDuuUnLpvn.htm)|Effect: Tail Toxin|
|[q6UokHWSEcEYWmvh.htm](feat-effects/q6UokHWSEcEYWmvh.htm)|Stance: Whirlwind Stance|
|[qBR3kqGCeKp3T2Be.htm](feat-effects/qBR3kqGCeKp3T2Be.htm)|Stance: Disruptive Stance|
|[qX62wJzDYtNxDbFv.htm](feat-effects/qX62wJzDYtNxDbFv.htm)|Stance: Dread Marshal Stance|
|[RoGEt7lrCdfaueB9.htm](feat-effects/RoGEt7lrCdfaueB9.htm)|Effect: Share Rage|
|[sCxi8lOH8tWQjLh0.htm](feat-effects/sCxi8lOH8tWQjLh0.htm)|Effect: Blade Ally|
|[sfUsodcGb4atcSyN.htm](feat-effects/sfUsodcGb4atcSyN.htm)|Effect: Reckless Abandon (Barbarian)|
|[SiegLMJpVOGuoyWJ.htm](feat-effects/SiegLMJpVOGuoyWJ.htm)|Effect: Ghost Wrangler|
|[tl94WHJ2Hg0akK2o.htm](feat-effects/tl94WHJ2Hg0akK2o.htm)|Effect: Invigorating Breath|
|[Unfl4QQURWaX2zfd.htm](feat-effects/Unfl4QQURWaX2zfd.htm)|Stance: Ricochet Stance|
|[W8CKuADdbODpBh6O.htm](feat-effects/W8CKuADdbODpBh6O.htm)|Stance: Lunging Stance|
|[wjNNHgX6ceKLbn8Q.htm](feat-effects/wjNNHgX6ceKLbn8Q.htm)|Effect: Rallying Charge|
|[wQDHpOKY3GZqvS2v.htm](feat-effects/wQDHpOKY3GZqvS2v.htm)|Effect: Seedpod|
|[X19XgqqItqZ4tfmq.htm](feat-effects/X19XgqqItqZ4tfmq.htm)|Effect: Guardian's Embrace|
|[ybc7tZwByenCzow8.htm](feat-effects/ybc7tZwByenCzow8.htm)|Effect: Creeping Ashes|
|[YkiTA74FrUUu5IvI.htm](feat-effects/YkiTA74FrUUu5IvI.htm)|Stance: Rough Terrain Stance|
|[YMdRmEcOlM3uU9Em.htm](feat-effects/YMdRmEcOlM3uU9Em.htm)|Effect: Living for the Applause|
|[zUvicEXd4OgCZ1cO.htm](feat-effects/zUvicEXd4OgCZ1cO.htm)|Effect: You're an Embarrassment|
|[zzC2qZwEKf4Ja3xD.htm](feat-effects/zzC2qZwEKf4Ja3xD.htm)|Stance: Impassable Wall Stance|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0AD7BiKjT8a6Uh92.htm](feat-effects/0AD7BiKjT8a6Uh92.htm)|Effect: Energetic Meltdown|auto-trad|
|[0JrHvdUgJBl631En.htm](feat-effects/0JrHvdUgJBl631En.htm)|Effect: Juvenile Flight|auto-trad|
|[0pq3MPLH0C9z4tj3.htm](feat-effects/0pq3MPLH0C9z4tj3.htm)|Effect: Victorious Vigor|auto-trad|
|[0r2V1nK5pV31IUPY.htm](feat-effects/0r2V1nK5pV31IUPY.htm)|Effect: Protective Mentor Boon (Revered) (PFS)|auto-trad|
|[1XlJ9xLzL19GHoOL.htm](feat-effects/1XlJ9xLzL19GHoOL.htm)|Effect: Overdrive (Critical Success)|auto-trad|
|[263Cd5JMj8Lgc9yz.htm](feat-effects/263Cd5JMj8Lgc9yz.htm)|Effect: Radiant Circuitry|auto-trad|
|[2c30Drdg84bWLcRn.htm](feat-effects/2c30Drdg84bWLcRn.htm)|Effect: Emblazon Energy (Weapon, Sonic)|auto-trad|
|[2ca1ZuqQ7VkunAh3.htm](feat-effects/2ca1ZuqQ7VkunAh3.htm)|Effect: Accept Echo|auto-trad|
|[2gVP04ZWYbQdX3uS.htm](feat-effects/2gVP04ZWYbQdX3uS.htm)|Effect: Spiral Sworn|auto-trad|
|[2RwhJ9fbJtcQjW6s.htm](feat-effects/2RwhJ9fbJtcQjW6s.htm)|Effect: Arctic Endemic Herb|auto-trad|
|[2XEYQNZTCGpdkyR6.htm](feat-effects/2XEYQNZTCGpdkyR6.htm)|Effect: Battle Medicine Immunity|auto-trad|
|[3gGBZHcUFsHLJeQH.htm](feat-effects/3gGBZHcUFsHLJeQH.htm)|Effect: Elemental Blood Magic (Self)|auto-trad|
|[3GPh6O3PJxORytAC.htm](feat-effects/3GPh6O3PJxORytAC.htm)|Effect: Shadow Sight|auto-trad|
|[3WzaQKb10AYLdTsQ.htm](feat-effects/3WzaQKb10AYLdTsQ.htm)|Effect: Corpse-Killer's Defiance|auto-trad|
|[4alr9e8w9H0RCLwI.htm](feat-effects/4alr9e8w9H0RCLwI.htm)|Effect: Tiller's Drive|auto-trad|
|[4QWayYR3JSL9bk2T.htm](feat-effects/4QWayYR3JSL9bk2T.htm)|Effect: Weapon Tampered With (Success)|auto-trad|
|[4UNQfMrwfWirdwoV.htm](feat-effects/4UNQfMrwfWirdwoV.htm)|Effect: Masterful Hunter's Edge, Flurry|auto-trad|
|[4xtHFRGI05SNe9rA.htm](feat-effects/4xtHFRGI05SNe9rA.htm)|Effect: Hungry Goblin|auto-trad|
|[4Zj71naHbY6O9ggP.htm](feat-effects/4Zj71naHbY6O9ggP.htm)|Effect: Bristle|auto-trad|
|[5bEnBqVOgdp4gROP.htm](feat-effects/5bEnBqVOgdp4gROP.htm)|Effect: Catfolk Dance|auto-trad|
|[5IGz4iheaiUWm5KR.htm](feat-effects/5IGz4iheaiUWm5KR.htm)|Effect: Eye of the Arclords|auto-trad|
|[5TzKmEqFyLHBG2ua.htm](feat-effects/5TzKmEqFyLHBG2ua.htm)|Effect: Emblazon Energy (Weapon, Cold)|auto-trad|
|[5v0ndPPMfZwhiVZF.htm](feat-effects/5v0ndPPMfZwhiVZF.htm)|Effect: Predictable!|auto-trad|
|[5veOBmMYQxywTudd.htm](feat-effects/5veOBmMYQxywTudd.htm)|Goblin Song (Success)|auto-trad|
|[6ACbQIpmmemxmoBJ.htm](feat-effects/6ACbQIpmmemxmoBJ.htm)|Effect: Saoc Astrology|auto-trad|
|[6EDoy3OSFZ4L3Vs7.htm](feat-effects/6EDoy3OSFZ4L3Vs7.htm)|Stance: Paragon's Guard|auto-trad|
|[6fb15XuSV4TNuVAT.htm](feat-effects/6fb15XuSV4TNuVAT.htm)|Effect: Hag Blood Magic|auto-trad|
|[6fObd480rDBkFwZ3.htm](feat-effects/6fObd480rDBkFwZ3.htm)|Effect: Bones Curse|auto-trad|
|[6IsZQpwRJQWIzdGx.htm](feat-effects/6IsZQpwRJQWIzdGx.htm)|Stance: Masquerade of Seasons Stance|auto-trad|
|[6YhbQmOmbmy84W1C.htm](feat-effects/6YhbQmOmbmy84W1C.htm)|Effect: Crimson Shroud|auto-trad|
|[7hQnwwsixZmXzdIT.htm](feat-effects/7hQnwwsixZmXzdIT.htm)|Effect: Channel the Godmind|auto-trad|
|[7hRgBo0fRQBxMK7g.htm](feat-effects/7hRgBo0fRQBxMK7g.htm)|Effect: Distracting Feint|auto-trad|
|[7MQLLkQACZt8cspt.htm](feat-effects/7MQLLkQACZt8cspt.htm)|Effect: Purifying Breeze|auto-trad|
|[7ogytOgDmh4h2g5d.htm](feat-effects/7ogytOgDmh4h2g5d.htm)|Effect: Heroic Recovery|auto-trad|
|[8E5SCmFndGAvgkTw.htm](feat-effects/8E5SCmFndGAvgkTw.htm)|Effect: Energize Wings|auto-trad|
|[8rDbWcWmQL0N5FFG.htm](feat-effects/8rDbWcWmQL0N5FFG.htm)|Effect: Azarketi Purification|auto-trad|
|[939OHjW9y8uCmDk3.htm](feat-effects/939OHjW9y8uCmDk3.htm)|Effect: Unleash Psyche|auto-trad|
|[9dCt0asv0kt7DR4q.htm](feat-effects/9dCt0asv0kt7DR4q.htm)|Effect: Liberating Step (vs. Fiend)|auto-trad|
|[9HPxAKpP3WJmICBx.htm](feat-effects/9HPxAKpP3WJmICBx.htm)|Stance: Point-Blank Shot|auto-trad|
|[9kNbiZPOM2wy60ao.htm](feat-effects/9kNbiZPOM2wy60ao.htm)|Effect: Ceremony of Protection|auto-trad|
|[a7qiSYdlaIRPe57i.htm](feat-effects/a7qiSYdlaIRPe57i.htm)|Effect: Watchful Gaze|auto-trad|
|[AAgoUuwMvHzqNhIN.htm](feat-effects/AAgoUuwMvHzqNhIN.htm)|Effect: Assisting Shot (Critical Hit)|auto-trad|
|[AclYG5JuBFrjCY3I.htm](feat-effects/AclYG5JuBFrjCY3I.htm)|Effect: Divine Weapon (Evil)|auto-trad|
|[aEuDaQY1GnrrnDRA.htm](feat-effects/aEuDaQY1GnrrnDRA.htm)|Effect: Aldori Parry|auto-trad|
|[AJlunjfAIOq2Sg0p.htm](feat-effects/AJlunjfAIOq2Sg0p.htm)|Effect: Underground Endemic Herbs|auto-trad|
|[AKKHagjg5bL1fMG5.htm](feat-effects/AKKHagjg5bL1fMG5.htm)|Effect: Overwatch Field|auto-trad|
|[aKRo5TIhUtu0kyEr.htm](feat-effects/aKRo5TIhUtu0kyEr.htm)|Effect: Demonic Blood Magic (Self)|auto-trad|
|[AlnxieIRjqNqsdVu.htm](feat-effects/AlnxieIRjqNqsdVu.htm)|Effect: Smite Evil|auto-trad|
|[aqnx6IDcB7ARLxS5.htm](feat-effects/aqnx6IDcB7ARLxS5.htm)|Effect: Wyrmblessed Blood Magic (Status Penalty - Target)|auto-trad|
|[aUpcWqaLBlmpnJgW.htm](feat-effects/aUpcWqaLBlmpnJgW.htm)|Effect: Legendary Monster Warden|auto-trad|
|[aWOvmdaTK1jS3H72.htm](feat-effects/aWOvmdaTK1jS3H72.htm)|Effect: Lost in the Crowd (10 Creatures)|auto-trad|
|[b2kWJuCPj1rDMdwz.htm](feat-effects/b2kWJuCPj1rDMdwz.htm)|Stance: Wolf Stance|auto-trad|
|[BHnunYPROBG5lxv4.htm](feat-effects/BHnunYPROBG5lxv4.htm)|Effect: Heroes' Call|auto-trad|
|[bIRIS6mnynr72RDw.htm](feat-effects/bIRIS6mnynr72RDw.htm)|Goblin Song (Critical Success)|auto-trad|
|[bIU1q05vzkKBtFj2.htm](feat-effects/bIU1q05vzkKBtFj2.htm)|Effect: Necrotic Infusion|auto-trad|
|[bl4HXm1e4NQ0iJs5.htm](feat-effects/bl4HXm1e4NQ0iJs5.htm)|Effect: Align Armament (Good)|auto-trad|
|[bliWctLi7jlKUTUe.htm](feat-effects/bliWctLi7jlKUTUe.htm)|Effect: Forest Endemic Herbs|auto-trad|
|[bmVwaN0C4e9fE2Sz.htm](feat-effects/bmVwaN0C4e9fE2Sz.htm)|Effect: Bolera's Interrogation (failure)|auto-trad|
|[bvk5rwYSoTtz8QGf.htm](feat-effects/bvk5rwYSoTtz8QGf.htm)|Effect: Accursed Clay Fist|auto-trad|
|[C6H3gF5HTdsIKpOC.htm](feat-effects/C6H3gF5HTdsIKpOC.htm)|Effect: Improved Poison Weapon|auto-trad|
|[cA6ps8RKE0gysEWr.htm](feat-effects/cA6ps8RKE0gysEWr.htm)|Effect: Prayer-Touched Weapon|auto-trad|
|[CNnIS8jWVj00nPwF.htm](feat-effects/CNnIS8jWVj00nPwF.htm)|Effect: Lost in the Crowd (100 Creatures)|auto-trad|
|[CQfkyJkRHw4IHWhv.htm](feat-effects/CQfkyJkRHw4IHWhv.htm)|Stance: Sky and Heaven Stance|auto-trad|
|[cqgbTZCvqaSvtQdz.htm](feat-effects/cqgbTZCvqaSvtQdz.htm)|Effect: Encroaching Presence|auto-trad|
|[ctiTtuRWFjAnWdYQ.htm](feat-effects/ctiTtuRWFjAnWdYQ.htm)|Effect: Corpse-Killer's Defiance (Lower Level)|auto-trad|
|[CtrZFI3RV0yPNzTv.htm](feat-effects/CtrZFI3RV0yPNzTv.htm)|Effect: Bon Mot (Critical Success)|auto-trad|
|[Dbr5hInQXH904Ca7.htm](feat-effects/Dbr5hInQXH904Ca7.htm)|Effect: Psychic Rapport|auto-trad|
|[DjxZpQ4xJWWvYQqY.htm](feat-effects/DjxZpQ4xJWWvYQqY.htm)|Effect: Repair Module|auto-trad|
|[dTymNXgTtnjqgYP9.htm](feat-effects/dTymNXgTtnjqgYP9.htm)|Effect: Emotional Surge|auto-trad|
|[dvOfGUuvG8ihcN8d.htm](feat-effects/dvOfGUuvG8ihcN8d.htm)|Effect: Divine Weapon (Good)|auto-trad|
|[DyX4E7KDkzRnDxzc.htm](feat-effects/DyX4E7KDkzRnDxzc.htm)|Effect: Perfect Resistance|auto-trad|
|[e6mv68aarIbQ3tXL.htm](feat-effects/e6mv68aarIbQ3tXL.htm)|Effect: Undying Conviction|auto-trad|
|[E8MiV00QEhH5n18L.htm](feat-effects/E8MiV00QEhH5n18L.htm)|Effect: Bespell Weapon|auto-trad|
|[eA14bUF7xhNCzw2v.htm](feat-effects/eA14bUF7xhNCzw2v.htm)|Effect: Align Armament (Evil)|auto-trad|
|[ed9iJxdHuft6bDFF.htm](feat-effects/ed9iJxdHuft6bDFF.htm)|Effect: Deadly Poison Weapon|auto-trad|
|[eeAlh6edygcZIz9c.htm](feat-effects/eeAlh6edygcZIz9c.htm)|Stance: Wild Winds Stance|auto-trad|
|[EfMaI6AnROP4X9lN.htm](feat-effects/EfMaI6AnROP4X9lN.htm)|Effect: Mountain Stronghold|auto-trad|
|[emSh1VxHVtTmt925.htm](feat-effects/emSh1VxHVtTmt925.htm)|Effect: Methodical Debilitations|auto-trad|
|[eMsI1lR0SuJBCYjn.htm](feat-effects/eMsI1lR0SuJBCYjn.htm)|Effect: Consume Energy (Augment Strike)|auto-trad|
|[EQCnu8DGHDDNXch0.htm](feat-effects/EQCnu8DGHDDNXch0.htm)|Effect: Reanimator Dedication|auto-trad|
|[ErLweSmVAN57QIpp.htm](feat-effects/ErLweSmVAN57QIpp.htm)|Effect: Nanite Surge|auto-trad|
|[ESnzqtwSgahLcxg2.htm](feat-effects/ESnzqtwSgahLcxg2.htm)|Effect: Hamstringing Strike|auto-trad|
|[EtFMN1ZLkL7sUk01.htm](feat-effects/EtFMN1ZLkL7sUk01.htm)|Effect: Life Curse|auto-trad|
|[EVRcdGt4awWPgXla.htm](feat-effects/EVRcdGt4awWPgXla.htm)|Effect: Arcane Propulsion|auto-trad|
|[EzgW32MCOGov9h5C.htm](feat-effects/EzgW32MCOGov9h5C.htm)|Effect: Striking Retribution|auto-trad|
|[fh8TgCfiifVk0eqU.htm](feat-effects/fh8TgCfiifVk0eqU.htm)|Effect: Magical Mentor Boon (PFS)|auto-trad|
|[FIgud5jqZgIjwkRE.htm](feat-effects/FIgud5jqZgIjwkRE.htm)|Effect: Maiden's Mending|auto-trad|
|[fILVhS5NuCtGXfri.htm](feat-effects/fILVhS5NuCtGXfri.htm)|Effect: Wyrmblessed Blood Magic (Status Bonus - Self)|auto-trad|
|[FNTTeJHiK6iOjrSq.htm](feat-effects/FNTTeJHiK6iOjrSq.htm)|Effect: Draconic Blood Magic|auto-trad|
|[FPuICuxBLiDaEbDX.htm](feat-effects/FPuICuxBLiDaEbDX.htm)|Effect: Aura of Life|auto-trad|
|[fsjO5oTKttsbpaKl.htm](feat-effects/fsjO5oTKttsbpaKl.htm)|Stance: Arcane Cascade|auto-trad|
|[FyaekbWsazkJhJda.htm](feat-effects/FyaekbWsazkJhJda.htm)|Effect: Decry Thief (Success)|auto-trad|
|[G1IRkppxJCYdfqXo.htm](feat-effects/G1IRkppxJCYdfqXo.htm)|Effect: Bespell Weapon (Negative)|auto-trad|
|[G4L49aMxHqO2yqxi.htm](feat-effects/G4L49aMxHqO2yqxi.htm)|Effect: Ashes Curse|auto-trad|
|[GbbwJhwSNLw06XpO.htm](feat-effects/GbbwJhwSNLw06XpO.htm)|Effect: Bespell Weapon (Force)|auto-trad|
|[GCEOngH5zL0rRyle.htm](feat-effects/GCEOngH5zL0rRyle.htm)|Effect: Emblazon Energy (Weapon, Fire)|auto-trad|
|[GGebXpRPyONZB3eS.htm](feat-effects/GGebXpRPyONZB3eS.htm)|Stance: Everstand Stance|auto-trad|
|[ghZFZWUh5Z20vOlR.htm](feat-effects/ghZFZWUh5Z20vOlR.htm)|Effect: Fortify Shield|auto-trad|
|[GlpZyxAGhy5QNqkm.htm](feat-effects/GlpZyxAGhy5QNqkm.htm)|Effect: Divine Weapon (Lawful)|auto-trad|
|[GoSls6SKCFmSoDxT.htm](feat-effects/GoSls6SKCFmSoDxT.htm)|Effect: Bon Mot|auto-trad|
|[gYpy9XBPScIlY93p.htm](feat-effects/gYpy9XBPScIlY93p.htm)|Stance: Mountain Stance|auto-trad|
|[h45sUZFs5jhuQdCE.htm](feat-effects/h45sUZFs5jhuQdCE.htm)|Stance: Vitality-Manipulation Stance|auto-trad|
|[HjCXHDZT6GkCyiuG.htm](feat-effects/HjCXHDZT6GkCyiuG.htm)|Effect: Plains Endemic Herbs|auto-trad|
|[HKPmrxkZwHRND5Um.htm](feat-effects/HKPmrxkZwHRND5Um.htm)|Favored Terrain (Increase Swim Speed)|auto-trad|
|[hqeR9faxHj0NDFFP.htm](feat-effects/hqeR9faxHj0NDFFP.htm)|Effect: Flames Curse|auto-trad|
|[I0g5oaSwaqZ8fFAV.htm](feat-effects/I0g5oaSwaqZ8fFAV.htm)|Effect: Tempest Curse|auto-trad|
|[I4Ozf6mTnd3X0Oax.htm](feat-effects/I4Ozf6mTnd3X0Oax.htm)|Effect: Predictable! (Critical Success)|auto-trad|
|[IfsglZ7fdegwem0E.htm](feat-effects/IfsglZ7fdegwem0E.htm)|Effect: Hydraulic Deflection|auto-trad|
|[Im5JBInybWFbHEYS.htm](feat-effects/Im5JBInybWFbHEYS.htm)|Stance: Rain of Embers Stance|auto-trad|
|[IpRfT9lL3YR6MH6w.htm](feat-effects/IpRfT9lL3YR6MH6w.htm)|Favored Terrain (Increase Climb Speed)|auto-trad|
|[iqvurepX0zyu9OlI.htm](feat-effects/iqvurepX0zyu9OlI.htm)|Effect: Masterful Hunter's Edge, Outwit|auto-trad|
|[ITvyvbB234bxceRK.htm](feat-effects/ITvyvbB234bxceRK.htm)|Effect: Mutate Weapon|auto-trad|
|[ivGiUp0EC5nWT9Hb.htm](feat-effects/ivGiUp0EC5nWT9Hb.htm)|Effect: Read Shibboleths|auto-trad|
|[iyONT1qgeRgoYHsZ.htm](feat-effects/iyONT1qgeRgoYHsZ.htm)|Effect: Liberating Step (vs. Dragon)|auto-trad|
|[JF2xCqL6t4UJZtUi.htm](feat-effects/JF2xCqL6t4UJZtUi.htm)|Effect: Blizzard Evasion|auto-trad|
|[jlZjUtrfcfIWumSe.htm](feat-effects/jlZjUtrfcfIWumSe.htm)|Effect: Renewed Vigor|auto-trad|
|[JQUoBlZKT5N5zO5k.htm](feat-effects/JQUoBlZKT5N5zO5k.htm)|Effect: Avenge in Glory|auto-trad|
|[JwDCoBIwyhOFnDGZ.htm](feat-effects/JwDCoBIwyhOFnDGZ.htm)|Effect: Augment Senses|auto-trad|
|[jwxurN6JPQm9wXug.htm](feat-effects/jwxurN6JPQm9wXug.htm)|Effect: Defensive Recovery|auto-trad|
|[JysvElDwGZ5ABQ6x.htm](feat-effects/JysvElDwGZ5ABQ6x.htm)|Effect: Emotional Fervor|auto-trad|
|[jZYRxGHyArCci6AF.htm](feat-effects/jZYRxGHyArCci6AF.htm)|Effect: Desert Endemic Herbs|auto-trad|
|[K0Sv9AHgq245hSLC.htm](feat-effects/K0Sv9AHgq245hSLC.htm)|Effect: Inspired Stratagem|auto-trad|
|[k1J2SaHPwZb2Y6Bp.htm](feat-effects/k1J2SaHPwZb2Y6Bp.htm)|Effect: Wings of Air|auto-trad|
|[kAgUld9PcI4XkHbq.htm](feat-effects/kAgUld9PcI4XkHbq.htm)|Effect: Decry Thief (Critical Success)|auto-trad|
|[KgR1myc4OLzVxfxn.htm](feat-effects/KgR1myc4OLzVxfxn.htm)|Effect: Predictable! (Critical Failure)|auto-trad|
|[KiuBRoMFxL2Npt51.htm](feat-effects/KiuBRoMFxL2Npt51.htm)|Stance: Dueling Dance|auto-trad|
|[KkbFlNfcQQUfSVXd.htm](feat-effects/KkbFlNfcQQUfSVXd.htm)|Effect: Align Armament (Lawful)|auto-trad|
|[kzEPq4aczYb6OD2h.htm](feat-effects/kzEPq4aczYb6OD2h.htm)|Effect: Inspiring Marshal Stance|auto-trad|
|[kzSjzK72CQ67wfBH.htm](feat-effects/kzSjzK72CQ67wfBH.htm)|Effect: Protective Spirit Mask|auto-trad|
|[L0hDj8vFk1IWh01L.htm](feat-effects/L0hDj8vFk1IWh01L.htm)|Effect: Aura of Righteousness|auto-trad|
|[l3S9i2UWGhSO58YX.htm](feat-effects/l3S9i2UWGhSO58YX.htm)|Effect: Cat Nap|auto-trad|
|[l4QUaedYofnfXig0.htm](feat-effects/l4QUaedYofnfXig0.htm)|Stance: Multishot Stance|auto-trad|
|[L9g3EMCT3imX650b.htm](feat-effects/L9g3EMCT3imX650b.htm)|Effect: Heaven's Thunder|auto-trad|
|[LB0PTV5yqMlBmRFj.htm](feat-effects/LB0PTV5yqMlBmRFj.htm)|Effect: Legendary Monster Hunter|auto-trad|
|[Lb4q2bBAgxamtix5.htm](feat-effects/Lb4q2bBAgxamtix5.htm)|Effect: Treat Wounds Immunity|auto-trad|
|[lbe8XDSZB8gwyg90.htm](feat-effects/lbe8XDSZB8gwyg90.htm)|Effect: Protective Mentor Boon (Admired) (PFS)|auto-trad|
|[LF8xzzFsFJKxejqv.htm](feat-effects/LF8xzzFsFJKxejqv.htm)|Effect: Enforce Oath|auto-trad|
|[Ljrx4N5XACKSk1Ks.htm](feat-effects/Ljrx4N5XACKSk1Ks.htm)|Effect: Core Cannon|auto-trad|
|[Lt5iSfx8fxHSdYXz.htm](feat-effects/Lt5iSfx8fxHSdYXz.htm)|Effect: Masterful Hunter's Edge, Precision|auto-trad|
|[ltIvO9ZQlmqGD89Y.htm](feat-effects/ltIvO9ZQlmqGD89Y.htm)|Effect: Hunter's Edge, Outwit|auto-trad|
|[LVPodfYEWKtK3fUW.htm](feat-effects/LVPodfYEWKtK3fUW.htm)|Effect: Formation Training|auto-trad|
|[lZPbv3nBRWmfbs3z.htm](feat-effects/lZPbv3nBRWmfbs3z.htm)|Effect: Strained Metabolism|auto-trad|
|[mkIamZGtQaSsUWLk.htm](feat-effects/mkIamZGtQaSsUWLk.htm)|Effect: Control Tower|auto-trad|
|[mNk0KxsZMFnDjUA0.htm](feat-effects/mNk0KxsZMFnDjUA0.htm)|Effect: Hunter's Edge, Precision|auto-trad|
|[MRo1SI1Y5PgdNf8r.htm](feat-effects/MRo1SI1Y5PgdNf8r.htm)|Effect: Deep Freeze (Critical Failure)|auto-trad|
|[MZDh3170EFIfOwTO.htm](feat-effects/MZDh3170EFIfOwTO.htm)|Effect: Overdrive (Success)|auto-trad|
|[n1vhmOd7aNiuR3nk.htm](feat-effects/n1vhmOd7aNiuR3nk.htm)|Effect: Diabolic Blood Magic (Self)|auto-trad|
|[ngwcN8u7f7CnqGXp.htm](feat-effects/ngwcN8u7f7CnqGXp.htm)|Effect: Distant Wandering|auto-trad|
|[nlaxROgSSLVHZ1hx.htm](feat-effects/nlaxROgSSLVHZ1hx.htm)|Effect: Monster Warden|auto-trad|
|[nlMZCi8xi9YSvlYR.htm](feat-effects/nlMZCi8xi9YSvlYR.htm)|Effect: Engine of Destruction|auto-trad|
|[NviQYIVZbPCSWLqT.htm](feat-effects/NviQYIVZbPCSWLqT.htm)|Effect: Aquatic Endemic Herbs|auto-trad|
|[nwkYZs6YwXYAJ4ps.htm](feat-effects/nwkYZs6YwXYAJ4ps.htm)|Stance: Crane Stance|auto-trad|
|[o7qm13OmaYOMwgib.htm](feat-effects/o7qm13OmaYOMwgib.htm)|Effect: Weapon Tampered With (Critical Success)|auto-trad|
|[OeZ0E1oUKyGPxPy0.htm](feat-effects/OeZ0E1oUKyGPxPy0.htm)|Effect: Push Back the Dead!|auto-trad|
|[OhLcaJeQy4Nf5Mwo.htm](feat-effects/OhLcaJeQy4Nf5Mwo.htm)|Favored Terrain (Gain Swim Speed)|auto-trad|
|[OK7zMlYy25JciBp6.htm](feat-effects/OK7zMlYy25JciBp6.htm)|Effect: Shed Tail|auto-trad|
|[OkcblqWj4aHVAkrp.htm](feat-effects/OkcblqWj4aHVAkrp.htm)|Effect: Divine Weapon (Chaotic)|auto-trad|
|[oKJr59FYdDORxLcR.htm](feat-effects/oKJr59FYdDORxLcR.htm)|Effect: Worldly Mentor Boon (PFS)|auto-trad|
|[OKOqC1wswrh9jXqP.htm](feat-effects/OKOqC1wswrh9jXqP.htm)|Effect: Protective Mentor Boon (Liked) (PFS)|auto-trad|
|[oSzUv21eN9VS9TC1.htm](feat-effects/oSzUv21eN9VS9TC1.htm)|Effect: Time Curse|auto-trad|
|[oX51Db6IxnUI64dT.htm](feat-effects/oX51Db6IxnUI64dT.htm)|Effect: Emblazon Energy (Weapon, Electricity)|auto-trad|
|[P6druSuWIVoLrXJR.htm](feat-effects/P6druSuWIVoLrXJR.htm)|Effect: Calculate Threats|auto-trad|
|[P80mwvCAEncR2snK.htm](feat-effects/P80mwvCAEncR2snK.htm)|Stance: Six Pillars Stance|auto-trad|
|[PMHwCrnh9W4sMu5b.htm](feat-effects/PMHwCrnh9W4sMu5b.htm)|Stance: Tangled Forest Stance|auto-trad|
|[pQ3EjUm1lZW9t3el.htm](feat-effects/pQ3EjUm1lZW9t3el.htm)|Effect: Battle Curse|auto-trad|
|[pQ9e5njvIOe5QzFa.htm](feat-effects/pQ9e5njvIOe5QzFa.htm)|Effect: Fleet Tempo|auto-trad|
|[pTYTanMHMwSgJ8TN.htm](feat-effects/pTYTanMHMwSgJ8TN.htm)|Effect: Defensive Instincts|auto-trad|
|[pwbFFD6NzDooobKo.htm](feat-effects/pwbFFD6NzDooobKo.htm)|Effect: Reflexive Shield|auto-trad|
|[PX6WdrpzEdUzKRHx.htm](feat-effects/PX6WdrpzEdUzKRHx.htm)|Effect: Enduring Debilitating Strike|auto-trad|
|[q2kY0TzXloJ8HLNO.htm](feat-effects/q2kY0TzXloJ8HLNO.htm)|Effect: Combat Mentor Boon (PFS)|auto-trad|
|[Q5FUu7yhWPJlcXei.htm](feat-effects/Q5FUu7yhWPJlcXei.htm)|Effect: Hydration|auto-trad|
|[QcReJp7kgURdQCGz.htm](feat-effects/QcReJp7kgURdQCGz.htm)|Effect: Disruptive Stare|auto-trad|
|[QDQwHxNowRwzUx9R.htm](feat-effects/QDQwHxNowRwzUx9R.htm)|Stance: Reflective Ripple Stance|auto-trad|
|[qIOEe4kUN7FOBifb.htm](feat-effects/qIOEe4kUN7FOBifb.htm)|Effect: Hybrid Shape (Beastkin)|auto-trad|
|[qM4bQfcwZ0EOS2M9.htm](feat-effects/qM4bQfcwZ0EOS2M9.htm)|Effect: Inspiring Resilience|auto-trad|
|[qSKVcw6brzrvfhUM.htm](feat-effects/qSKVcw6brzrvfhUM.htm)|Effect: Supercharge Prosthetic Eyes|auto-trad|
|[QTG73gxKSNkiEWdY.htm](feat-effects/QTG73gxKSNkiEWdY.htm)|Effect: Mountain Endemic Herbs|auto-trad|
|[qUowHpn79Dpt1hVn.htm](feat-effects/qUowHpn79Dpt1hVn.htm)|Stance: Dragon Stance|auto-trad|
|[raLQ458uiyd3lI2K.htm](feat-effects/raLQ458uiyd3lI2K.htm)|Effect: Guided by the Stars|auto-trad|
|[raoz523QRsj5WjcF.htm](feat-effects/raoz523QRsj5WjcF.htm)|Effect: Harsh Judgement|auto-trad|
|[RATDyLyxXN3qmOas.htm](feat-effects/RATDyLyxXN3qmOas.htm)|Effect: Daydream Trance|auto-trad|
|[RcxDIOa68SUGyMun.htm](feat-effects/RcxDIOa68SUGyMun.htm)|Effect: Titan's Stature|auto-trad|
|[rJpkKaPRGaH0pLse.htm](feat-effects/rJpkKaPRGaH0pLse.htm)|Effect: Fey Blood Magic|auto-trad|
|[rp1YauUSULuqW8rs.htm](feat-effects/rp1YauUSULuqW8rs.htm)|Stance: Stoked Flame Stance|auto-trad|
|[Ru4BNABCZ0hUbX7S.htm](feat-effects/Ru4BNABCZ0hUbX7S.htm)|Effect: Marshal's Aura|auto-trad|
|[RU6D7pNQSBt1zSuK.htm](feat-effects/RU6D7pNQSBt1zSuK.htm)|Effect: Propulsive Leap|auto-trad|
|[rwDsr5XsrYcH7oFT.htm](feat-effects/rwDsr5XsrYcH7oFT.htm)|Effect: Cosmos Curse|auto-trad|
|[RxDDXK52lwyHXl7v.htm](feat-effects/RxDDXK52lwyHXl7v.htm)|Effect: Scout's Warning|auto-trad|
|[RyGaB5hDRcOeb34Q.htm](feat-effects/RyGaB5hDRcOeb34Q.htm)|Effect: Emblazon Antimagic (Weapon)|auto-trad|
|[s1tulrmW6teTFjVd.htm](feat-effects/s1tulrmW6teTFjVd.htm)|Effect: Angelic Blood Magic|auto-trad|
|[s3Te8waFP3KEb2dN.htm](feat-effects/s3Te8waFP3KEb2dN.htm)|Effect: Shield Ally|auto-trad|
|[sDftJWPPSUeSZD3A.htm](feat-effects/sDftJWPPSUeSZD3A.htm)|Favored Terrain (Gain Climb Speed)|auto-trad|
|[SKjVvQcRQmnDoouw.htm](feat-effects/SKjVvQcRQmnDoouw.htm)|Effect: Skillful Mentor Boon (PFS)|auto-trad|
|[SXYcrnGzWCuj8zq7.htm](feat-effects/SXYcrnGzWCuj8zq7.htm)|Effect: Poison Weapon|auto-trad|
|[tAsFXMzNkpj964X4.htm](feat-effects/tAsFXMzNkpj964X4.htm)|Effect: Liberating Step (vs. Aberration)|auto-trad|
|[Tju9kpQlwcKkyKor.htm](feat-effects/Tju9kpQlwcKkyKor.htm)|Effect: Lore Curse|auto-trad|
|[tlft5vzk66iWCVRq.htm](feat-effects/tlft5vzk66iWCVRq.htm)|Effect: Safeguard Soul|auto-trad|
|[tPKXLtDJ3bzJcXlv.htm](feat-effects/tPKXLtDJ3bzJcXlv.htm)|Stance: Ironblood Stance|auto-trad|
|[tx0S0fnfZ6Q2o80H.htm](feat-effects/tx0S0fnfZ6Q2o80H.htm)|Effect: High-Speed Regeneration Speed Boost|auto-trad|
|[U1MpMtRnFqEDBJwd.htm](feat-effects/U1MpMtRnFqEDBJwd.htm)|Effect: Emblazon Armament (Weapon)|auto-trad|
|[uA1Ofqoyi0UiZIPk.htm](feat-effects/uA1Ofqoyi0UiZIPk.htm)|Effect: Clue In (Expertise)|auto-trad|
|[UBC6HbfqbfPQYlMq.htm](feat-effects/UBC6HbfqbfPQYlMq.htm)|Effect: Tidal Shield|auto-trad|
|[uBJsxCzNhje8m8jj.htm](feat-effects/uBJsxCzNhje8m8jj.htm)|Effect: Panache|auto-trad|
|[uFYvW3kFP9iyNfVX.htm](feat-effects/uFYvW3kFP9iyNfVX.htm)|Stance: Clinging Shadows Stance|auto-trad|
|[UQ7vZgmfK0VSFS8A.htm](feat-effects/UQ7vZgmfK0VSFS8A.htm)|Effect: Aberrant Blood Magic|auto-trad|
|[uXCU8GgriUjuj5FV.htm](feat-effects/uXCU8GgriUjuj5FV.htm)|Effect: Hunter's Edge, Flurry|auto-trad|
|[UzIamWcEJTOjwfoA.htm](feat-effects/UzIamWcEJTOjwfoA.htm)|Effect: Spin Tale|auto-trad|
|[v2HDcrxQF2Dncjbs.htm](feat-effects/v2HDcrxQF2Dncjbs.htm)|Effect: Flamboyant Cruelty|auto-trad|
|[V6lnFOq998B76Rr0.htm](feat-effects/V6lnFOq998B76Rr0.htm)|Effect: Ancestor Curse|auto-trad|
|[vguxP8ukwVTWWWaA.htm](feat-effects/vguxP8ukwVTWWWaA.htm)|Effect: Imperial Blood Magic|auto-trad|
|[vhSYlQiAQMLuXqoc.htm](feat-effects/vhSYlQiAQMLuXqoc.htm)|Effect: Clue In|auto-trad|
|[VIScVb6Hl7KwoWfH.htm](feat-effects/VIScVb6Hl7KwoWfH.htm)|Effect: Bolera's Interrogation (Critical Failure)|auto-trad|
|[VOOShYoB4gTopZtg.htm](feat-effects/VOOShYoB4gTopZtg.htm)|Effect: Aura of Faith|auto-trad|
|[vQj9I3FShzx3lNoG.htm](feat-effects/vQj9I3FShzx3lNoG.htm)|Effect: Deep Freeze (Failure)|auto-trad|
|[W2tWq0gdAcnoz2MO.htm](feat-effects/W2tWq0gdAcnoz2MO.htm)|Effect: Monster Hunter|auto-trad|
|[W8HWQ47YNHWB8kj6.htm](feat-effects/W8HWQ47YNHWB8kj6.htm)|Effect: Topple Giants|auto-trad|
|[WKbsjhjYIMv59JQg.htm](feat-effects/WKbsjhjYIMv59JQg.htm)|Effect: Deep Freeze (Success)|auto-trad|
|[wmBSuZPqiDyUNwXH.htm](feat-effects/wmBSuZPqiDyUNwXH.htm)|Effect: Dragon's Rage Wings|auto-trad|
|[X1pGyhMKrCTvHB0q.htm](feat-effects/X1pGyhMKrCTvHB0q.htm)|Effect: Favorable Winds|auto-trad|
|[x4Sb3qaMJo8x1r3X.htm](feat-effects/x4Sb3qaMJo8x1r3X.htm)|Effect: Emblazon Energy (Weapon, Acid)|auto-trad|
|[XaZdQHF9GvaJINqH.htm](feat-effects/XaZdQHF9GvaJINqH.htm)|Effect: Elemental Assault|auto-trad|
|[XC3dRbwfu35vuvmM.htm](feat-effects/XC3dRbwfu35vuvmM.htm)|Effect: Align Armament (Chaotic)|auto-trad|
|[xDT10fUWp8UStSZR.htm](feat-effects/xDT10fUWp8UStSZR.htm)|Effect: Cavalier's Banner|auto-trad|
|[XJtlvaqAHseq1yoz.htm](feat-effects/XJtlvaqAHseq1yoz.htm)|Effect: Towering Presence|auto-trad|
|[XM1AA8z5cHm8sJXM.htm](feat-effects/XM1AA8z5cHm8sJXM.htm)|Effect: Enlightened Presence|auto-trad|
|[xPg5wzzKNxJy18rU.htm](feat-effects/xPg5wzzKNxJy18rU.htm)|Effect: Brightness Seeker|auto-trad|
|[Y96a1OedsU8PVf7z.htm](feat-effects/Y96a1OedsU8PVf7z.htm)|Effect: Starlight Armor|auto-trad|
|[yBTASi3FvnReAwHy.htm](feat-effects/yBTASi3FvnReAwHy.htm)|Effect: Debilitating Strike|auto-trad|
|[yfbP64r4a9e5oyli.htm](feat-effects/yfbP64r4a9e5oyli.htm)|Effect: Demonic Blood Magic (Target)|auto-trad|
|[YKJhjkerCW0Jl6HP.htm](feat-effects/YKJhjkerCW0Jl6HP.htm)|Effect: Life-Giving Magic|auto-trad|
|[yr5ey5qC8dXH749T.htm](feat-effects/yr5ey5qC8dXH749T.htm)|Effect: Entity's Resurgence|auto-trad|
|[z3uyCMBddrPK5umr.htm](feat-effects/z3uyCMBddrPK5umr.htm)|Effect: Rage|auto-trad|
|[z4pnE8KyUdEkJmru.htm](feat-effects/z4pnE8KyUdEkJmru.htm)|Effect: Clue In (Detective's Readiness, Expertise)|auto-trad|
|[zcJii1XyOne9EvMr.htm](feat-effects/zcJii1XyOne9EvMr.htm)|Effect: Assisting Shot|auto-trad|
|[ZMFgz4GYSsFeaKKK.htm](feat-effects/ZMFgz4GYSsFeaKKK.htm)|Effect: Rugged Mentor Boon (PFS)|auto-trad|
|[ZnKnOPPq3cG54PlG.htm](feat-effects/ZnKnOPPq3cG54PlG.htm)|Effect: Liberating Step (vs. Undead)|auto-trad|
|[zQHF2kkhZRAcrQvR.htm](feat-effects/zQHF2kkhZRAcrQvR.htm)|Effect: Sniping Duo Dedication|auto-trad|
|[ZSgB3imGveukWUxs.htm](feat-effects/ZSgB3imGveukWUxs.htm)|Effect: Bespell Weapon (Mental)|auto-trad|
|[ZsO5juyylVoxUkXh.htm](feat-effects/ZsO5juyylVoxUkXh.htm)|Effect: Bone Spikes|auto-trad|
|[zZ25N1zpXA8GNhFL.htm](feat-effects/zZ25N1zpXA8GNhFL.htm)|Effect: Divine Weapon (Force)|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[6VrKQ0PhRXuteusQ.htm](feat-effects/6VrKQ0PhRXuteusQ.htm)|Effect: Giant's Stature|Efecto: Estatura de Gigante|modificada|
|[DWrsDJte9sez0Ppi.htm](feat-effects/DWrsDJte9sez0Ppi.htm)|Effect: Rampaging Form|Efecto: Forma Furiosa|modificada|
|[IfRkgjyh0JzGalIy.htm](feat-effects/IfRkgjyh0JzGalIy.htm)|Effect: Armor Tampered With (Success)|Efecto: Armadura Manipulada (Éxito)|modificada|
|[KBEJVRrie2JTHWIK.htm](feat-effects/KBEJVRrie2JTHWIK.htm)|Effect: Dread Marshal Stance|Efecto: Posición de Mariscal Temible.|modificada|
|[LxSev4GNKv26DbZw.htm](feat-effects/LxSev4GNKv26DbZw.htm)|Stance: Disarming Stance|Posición: Posición desarmante.|modificada|
|[pf9yvKNg6jZLrE30.htm](feat-effects/pf9yvKNg6jZLrE30.htm)|Stance: Tiger Stance|Posición: Posición del tigre.|modificada|
|[pkcr9w5x6bKzl3om.htm](feat-effects/pkcr9w5x6bKzl3om.htm)|Stance: Jellyfish Stance|Posición: Posición Medusa.|modificada|
|[r4kb2zDepFeczMsl.htm](feat-effects/r4kb2zDepFeczMsl.htm)|Effect: Bone Swarm|Efecto: Enjambre de Huesos|modificada|
|[rzcpTJU9MvW1x1gz.htm](feat-effects/rzcpTJU9MvW1x1gz.htm)|Effect: Armor Tampered With (Critical Success)|Efecto: Armadura Manipulada (Éxito Crítico)|modificada|
|[WRe8qbemruWxkN8d.htm](feat-effects/WRe8qbemruWxkN8d.htm)|Effect: Rampaging Form (Frozen Winds Kitsune)|Efecto: Forma arrasadora (Kitsune de los Vientos Helados).|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[m5xWMaDfV0PiTE6u.htm](feat-effects/m5xWMaDfV0PiTE6u.htm)|Effect: Ursine Avenger Form|vacía|
|[rp6hA52dWVwtuu5F.htm](feat-effects/rp6hA52dWVwtuu5F.htm)|Effect: Harrow Omen|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[18FHJoazfEmgNkfk.htm](feat-effects/18FHJoazfEmgNkfk.htm)|Effect: Aura of Preservation|Efecto: Aura de conservación|oficial|
|[1dxD3xsTzak6GNj5.htm](feat-effects/1dxD3xsTzak6GNj5.htm)|Stance: Monastic Archer Stance|Posición: Posición del arquero Monástico|oficial|
|[1nCwQErK6hpkNvfw.htm](feat-effects/1nCwQErK6hpkNvfw.htm)|Effect: Dueling Parry|Efecto: Parada de duelo|oficial|
|[2MIn8qyPTmz4ZyO1.htm](feat-effects/2MIn8qyPTmz4ZyO1.htm)|Effect: Smite Good|Efecto: Castigar el bien|oficial|
|[2Qpt0CHuOMeL48rN.htm](feat-effects/2Qpt0CHuOMeL48rN.htm)|Stance: Cobra Stance (Cobra Envenom)|Posición: Posición de la cobra (Veneno de cobra)|oficial|
|[72THfaqak0F4XnON.htm](feat-effects/72THfaqak0F4XnON.htm)|Effect: Didactic Strike|Efecto: Golpe Didáctico|oficial|
|[7BFd8A9HFrmg6vwL.htm](feat-effects/7BFd8A9HFrmg6vwL.htm)|Effect: Psychopomp Blood Magic (Self)|Efecto: Magia de sangre de psicopompo (A sí mismo)|oficial|
|[9AUcoY48H5LrVZiF.htm](feat-effects/9AUcoY48H5LrVZiF.htm)|Effect: Genie Blood Magic (Self)|Efecto: Magia de sangre del genio (A ti mismo)|oficial|
|[BC92TyFzRCWq8fu0.htm](feat-effects/BC92TyFzRCWq8fu0.htm)|Effect: Great Tengu Form|Efecto: Forma de Tengu mayor|oficial|
|[BCyGDKcplkJiSAKJ.htm](feat-effects/BCyGDKcplkJiSAKJ.htm)|Stance: Stumbling Stance|Posición: Posición del borracho|oficial|
|[CgxYa0lrLUjS2ZhI.htm](feat-effects/CgxYa0lrLUjS2ZhI.htm)|Stance: Cobra Stance|Posición: Posición de la cobra|oficial|
|[cH8JD3ub4eEKuIAD.htm](feat-effects/cH8JD3ub4eEKuIAD.htm)|Effect: Radiant Infusion|Efecto: Infusión Radiante|oficial|
|[COsdMolZraFRTdP8.htm](feat-effects/COsdMolZraFRTdP8.htm)|Effect: Prevailing Position|Efecto: Posición predominante|oficial|
|[Cumdy84uIkUHG9zF.htm](feat-effects/Cumdy84uIkUHG9zF.htm)|Effect: Resounding Bravery (vs. Fear)|Efecto: Valentía resonante (vs. Miedo)|oficial|
|[CUtvkuGSxq1raBIB.htm](feat-effects/CUtvkuGSxq1raBIB.htm)|Effect: Shared Clarity|Efecto: Claridad compartida|oficial|
|[DhvSMIFs6xifgQHX.htm](feat-effects/DhvSMIFs6xifgQHX.htm)|Effect: Current Spell (Water)|Efecto: Hechizo Actual (Agua)|oficial|
|[DvyyA11a63FBwV7x.htm](feat-effects/DvyyA11a63FBwV7x.htm)|Effect: Known Weakness|Efecto: Debilidad conocida|oficial|
|[EDpjey6SCdvIYqEc.htm](feat-effects/EDpjey6SCdvIYqEc.htm)|Effect: Twin Parry (Parry Trait)|Efecto: Parada gemela (Rasgo parada)|oficial|
|[eu2HidLHaGKe4MPn.htm](feat-effects/eu2HidLHaGKe4MPn.htm)|Effect: Twin Parry|Efecto: Parada gemela|oficial|
|[gWwG7MNAesJgpmRW.htm](feat-effects/gWwG7MNAesJgpmRW.htm)|Effect: Cut from the Air|Efecto: Corte desde el Aire|oficial|
|[HfXGhXc9D120gvl5.htm](feat-effects/HfXGhXc9D120gvl5.htm)|Effect: Celestial Wings|Efecto: Alas Celestiales|oficial|
|[JefXqvhzUeBArkAP.htm](feat-effects/JefXqvhzUeBArkAP.htm)|Stance: Whirling Blade Stance|Posición: Posición de la hoja giratoria|oficial|
|[K1IgNCf3Hh2EJwQ9.htm](feat-effects/K1IgNCf3Hh2EJwQ9.htm)|Effect: Divine Aegis|Efecto: Égida Divina|oficial|
|[KceTcamIZ4ZrQJLD.htm](feat-effects/KceTcamIZ4ZrQJLD.htm)|Effect: Educate Allies (Self)|Efecto: Educar a los aliados (A ti mismo)|oficial|
|[KVbS7AbhQdeuA0J6.htm](feat-effects/KVbS7AbhQdeuA0J6.htm)|Effect: Genie Blood Magic (Target)|Efecto: Magia de sangre del genio (objetivo).|oficial|
|[MNkIxAishE22TqL3.htm](feat-effects/MNkIxAishE22TqL3.htm)|Effect: Aura of Despair|Efecto: Aura de Desesperación|oficial|
|[MrdT7LiOZMN8J4GK.htm](feat-effects/MrdT7LiOZMN8J4GK.htm)|Effect: Fiendish Wings|Efecto: Alas infernales|oficial|
|[NMmsJyeMTawpgLVR.htm](feat-effects/NMmsJyeMTawpgLVR.htm)|Effect: Resounding Bravery|Efecto: Valentía resonante|oficial|
|[nnF7RSVlC6swbSw8.htm](feat-effects/nnF7RSVlC6swbSw8.htm)|Effect: Anoint Ally|Efecto: Ungir a un aliado|oficial|
|[Nv70aqcQgCBpDYp8.htm](feat-effects/Nv70aqcQgCBpDYp8.htm)|Effect: Shadow Blood Magic (Perception)|Efecto: Magia de sangre Sombría (Percepción)|oficial|
|[OqH6IaeOwRWkGPrk.htm](feat-effects/OqH6IaeOwRWkGPrk.htm)|Effect: Shadow Blood Magic (Stealth)|Efecto: Magia de sangre Sombrío (Sigilo)|oficial|
|[oXG7eX26FmePmwUF.htm](feat-effects/oXG7eX26FmePmwUF.htm)|Effect: Discordant Voice|Efecto: Voz discordante|oficial|
|[RozqjLocahvQWERr.htm](feat-effects/RozqjLocahvQWERr.htm)|Stance: Gorilla Stance|Posición: Posición del gorila|oficial|
|[rUKtp4q8y73AvCbo.htm](feat-effects/rUKtp4q8y73AvCbo.htm)|Effect: Clue In (Detective's Readiness)|Efecto: Pista (Preparación del detective)|oficial|
|[ruRAfGJnik7lRavk.htm](feat-effects/ruRAfGJnik7lRavk.htm)|Effect: Nymph Blood Magic (Target)|Efecto: Magia de sangre de ninfa (objetivo)|oficial|
|[rvyeOU7TQTLnKj03.htm](feat-effects/rvyeOU7TQTLnKj03.htm)|Effect: Reckless Abandon (Goblin)|Efecto: Abandono temerario (Goblin)|oficial|
|[RXbfq6oqzVnW6xOV.htm](feat-effects/RXbfq6oqzVnW6xOV.htm)|Stance: Shooting Stars Stance|Posición: Posición de la lluvia de estrellas|oficial|
|[SVGW8CLKwixFlnTv.htm](feat-effects/SVGW8CLKwixFlnTv.htm)|Effect: Nymph Blood Magic (Self)|Efecto: Magia de sangre de ninfa (A tí mismo)|oficial|
|[svVczVV174KfJRDf.htm](feat-effects/svVczVV174KfJRDf.htm)|Effect: Shared Avoidance|Efecto: Evitación compartida|oficial|
|[T5rsLTqS274B9Mly.htm](feat-effects/T5rsLTqS274B9Mly.htm)|Effect: Current Spell (Air)|Efecto: Hechizo actual (Aire)|oficial|
|[T7AJQbfmlA57y625.htm](feat-effects/T7AJQbfmlA57y625.htm)|Effect: Vivacious Bravado|Efecto: Bravuconada vivaz|oficial|
|[ugeStF0Rj8phBPWL.htm](feat-effects/ugeStF0Rj8phBPWL.htm)|Effect: Witch's Charge|Efecto: Tutela del brujo|oficial|
|[UZKIKLuwpQu47feK.htm](feat-effects/UZKIKLuwpQu47feK.htm)|Stance: Gorilla Stance (Gorilla Pound)|Posición: Posición del gorila (Porrazo de gorila)|oficial|
|[vjvcccAwdkOLA1Fc.htm](feat-effects/vjvcccAwdkOLA1Fc.htm)|Stance: Peafowl Stance|Posición: Posición del pavo real|oficial|
|[w6X7io56B2HHTOvs.htm](feat-effects/w6X7io56B2HHTOvs.htm)|Effect: Guardian's Deflection|Efecto: Desvío del guardián.|oficial|
|[WrWSieH9Acy6XuzV.htm](feat-effects/WrWSieH9Acy6XuzV.htm)|Effect: Educate Allies (Ally)|Efecto: Educar a los aliados (Aliado)|oficial|
|[ytG5XJmkOnDOTjNN.htm](feat-effects/ytG5XJmkOnDOTjNN.htm)|Effect: Soaring Flight|Efecto: Vuelo Ascendente|oficial|
