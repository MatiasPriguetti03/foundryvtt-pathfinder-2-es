# Estado de la traducción (bestiary-effects)

 * **auto-trad**: 258
 * **modificada**: 4
 * **ninguna**: 8


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[E0fYyli4aCVmevgJ.htm](bestiary-effects/E0fYyli4aCVmevgJ.htm)|Effect: Capture Magic|
|[lJGAJF9QiXCOPd2I.htm](bestiary-effects/lJGAJF9QiXCOPd2I.htm)|Effect: Draconic Breath Weapon Aura|
|[RbyXD99lybxPK6yS.htm](bestiary-effects/RbyXD99lybxPK6yS.htm)|Effect: Warning Hoot|
|[t8Yxrx3XgjS78Hxt.htm](bestiary-effects/t8Yxrx3XgjS78Hxt.htm)|Effect: Assimilate Lava|
|[WYJdjmvfm8J6sG6g.htm](bestiary-effects/WYJdjmvfm8J6sG6g.htm)|Effect: Jury-Rig|
|[XE2YhBMl7wc6nQAZ.htm](bestiary-effects/XE2YhBMl7wc6nQAZ.htm)|Effect: Replenishment of War|
|[xOD3ufpzA8H7W4sP.htm](bestiary-effects/xOD3ufpzA8H7W4sP.htm)|Effect: Aura of Good Cheer|
|[YKCsmlMgI0aS7joO.htm](bestiary-effects/YKCsmlMgI0aS7joO.htm)|Effect: Silent Aura|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0ifqiVhhKULXJyf9.htm](bestiary-effects/0ifqiVhhKULXJyf9.htm)|Effect: Rune of Flames (Longspear)|auto-trad|
|[0jAT2TJoqC1z6NCf.htm](bestiary-effects/0jAT2TJoqC1z6NCf.htm)|Effect: Consume Memories|auto-trad|
|[0jo8CUzw5lWehNg3.htm](bestiary-effects/0jo8CUzw5lWehNg3.htm)|Effect: Oceanic Armor|auto-trad|
|[1bOSJ2LbEC28aI9f.htm](bestiary-effects/1bOSJ2LbEC28aI9f.htm)|Effect: Despair|auto-trad|
|[1dwMVgBHfT4qO4OS.htm](bestiary-effects/1dwMVgBHfT4qO4OS.htm)|Effect: Resonance|auto-trad|
|[1jrrnMwsfO97LXi4.htm](bestiary-effects/1jrrnMwsfO97LXi4.htm)|Effect: Absorb Memories|auto-trad|
|[1toVzNVJZx0RwG1v.htm](bestiary-effects/1toVzNVJZx0RwG1v.htm)|Effect: Darivan's Bloodline Magic|auto-trad|
|[2ccLQxmTlTPySnOR.htm](bestiary-effects/2ccLQxmTlTPySnOR.htm)|Effect: Technology Control|auto-trad|
|[2DlOfFoYLGSlfugH.htm](bestiary-effects/2DlOfFoYLGSlfugH.htm)|Effect: Manipulate Luck (Good)|auto-trad|
|[31nnjHZqiaqaWBUi.htm](bestiary-effects/31nnjHZqiaqaWBUi.htm)|Effect: Harmonizing Aura (Allies)|auto-trad|
|[361dIAAhiZE0wg8v.htm](bestiary-effects/361dIAAhiZE0wg8v.htm)|Effect: Adaptive Strike|auto-trad|
|[37wbBpPBi5eBtNqM.htm](bestiary-effects/37wbBpPBi5eBtNqM.htm)|Effect: Lantern of Hope|auto-trad|
|[3MIZf42EhhKbIwLQ.htm](bestiary-effects/3MIZf42EhhKbIwLQ.htm)|Effect: Aura of Corruption|auto-trad|
|[3np7EyJ8pyrVlpie.htm](bestiary-effects/3np7EyJ8pyrVlpie.htm)|Effect: Claim Corpse - Skeletal|auto-trad|
|[3QUaxNvpHDmf9cTo.htm](bestiary-effects/3QUaxNvpHDmf9cTo.htm)|Effect: Rune of Flames (Rock)|auto-trad|
|[3Wtzyb0ZgkaC7vHY.htm](bestiary-effects/3Wtzyb0ZgkaC7vHY.htm)|Effect: Fanatical Frenzy|auto-trad|
|[4bR1i7qzmSJ5No6O.htm](bestiary-effects/4bR1i7qzmSJ5No6O.htm)|Effect: Bond in Light|auto-trad|
|[4FginnDcOt4wfedf.htm](bestiary-effects/4FginnDcOt4wfedf.htm)|Effect: Bittersweet Dreams|auto-trad|
|[4FMFRlEC923CnLI7.htm](bestiary-effects/4FMFRlEC923CnLI7.htm)|Effect: Fed by Wood|auto-trad|
|[4GAJHkurmZ1ttKDv.htm](bestiary-effects/4GAJHkurmZ1ttKDv.htm)|Effect: Blood Magic|auto-trad|
|[4HJJdogS751Z04lD.htm](bestiary-effects/4HJJdogS751Z04lD.htm)|Effect: Tail Lash (Skill Check)|auto-trad|
|[4M2K16mH4gndHAKa.htm](bestiary-effects/4M2K16mH4gndHAKa.htm)|Effect: Undead Mastery|auto-trad|
|[4q8Of8NM9DC8kWyK.htm](bestiary-effects/4q8Of8NM9DC8kWyK.htm)|Effect: Pry|auto-trad|
|[4tGVIEqwH4TQoF0O.htm](bestiary-effects/4tGVIEqwH4TQoF0O.htm)|Effect: Bastion Aura|auto-trad|
|[5NSWRxAsJuvwyl0E.htm](bestiary-effects/5NSWRxAsJuvwyl0E.htm)|Effect: Commander's Aura|auto-trad|
|[5roCV7EbPx1G5xOd.htm](bestiary-effects/5roCV7EbPx1G5xOd.htm)|Effect: Mangling Rend|auto-trad|
|[5syHrFGAE6lo0FUr.htm](bestiary-effects/5syHrFGAE6lo0FUr.htm)|Effect: Invigorating Passion|auto-trad|
|[5x0XpNftvx9uGbXt.htm](bestiary-effects/5x0XpNftvx9uGbXt.htm)|Effect: Death Gasp (Etioling)|auto-trad|
|[5ZK22sNW7o26aST0.htm](bestiary-effects/5ZK22sNW7o26aST0.htm)|Effect: Dirty Bomb|auto-trad|
|[64wrP9IbfHbj1mrA.htm](bestiary-effects/64wrP9IbfHbj1mrA.htm)|Effect: War Leader|auto-trad|
|[6E8bOkwFzFuQ3ZAw.htm](bestiary-effects/6E8bOkwFzFuQ3ZAw.htm)|Effect: Lurker's Glow (Critical Failure)|auto-trad|
|[6GC248hHu3LdOAtS.htm](bestiary-effects/6GC248hHu3LdOAtS.htm)|Effect: Sting of the Lash|auto-trad|
|[6rnB7nK6J6zF4vea.htm](bestiary-effects/6rnB7nK6J6zF4vea.htm)|Effect: Graveknight's Curse|auto-trad|
|[75B7z49jfQbWcSy9.htm](bestiary-effects/75B7z49jfQbWcSy9.htm)|Effect: Spray Toxic Oil|auto-trad|
|[7OJrBAEah6YFLzcK.htm](bestiary-effects/7OJrBAEah6YFLzcK.htm)|Effect: Crush Chitin|auto-trad|
|[7PjBr9LsVB0Jjzyu.htm](bestiary-effects/7PjBr9LsVB0Jjzyu.htm)|Effect: Interpose|auto-trad|
|[7PYhxmQxCD5kzMlw.htm](bestiary-effects/7PYhxmQxCD5kzMlw.htm)|Effect: Harrowing Misfortune|auto-trad|
|[7qoZauizAKfPIXeu.htm](bestiary-effects/7qoZauizAKfPIXeu.htm)|Effect: Volcanic Purge|auto-trad|
|[7wDH2q0UcFdu2w58.htm](bestiary-effects/7wDH2q0UcFdu2w58.htm)|Effect: Focused Assault|auto-trad|
|[7x0O2GqWBJiAk5PF.htm](bestiary-effects/7x0O2GqWBJiAk5PF.htm)|Effect: Brutal Rally|auto-trad|
|[81XYZZ3H0GL8thdQ.htm](bestiary-effects/81XYZZ3H0GL8thdQ.htm)|Effect: Countered by Water|auto-trad|
|[86uYNsVXzYWxqLTV.htm](bestiary-effects/86uYNsVXzYWxqLTV.htm)|Effect: Splintered Ground|auto-trad|
|[8z4Q84UlS8cMYVWu.htm](bestiary-effects/8z4Q84UlS8cMYVWu.htm)|Effect: Black Cat Curse|auto-trad|
|[9Qyu0HN5j6DO8Izc.htm](bestiary-effects/9Qyu0HN5j6DO8Izc.htm)|Effect: Sinful Bite|auto-trad|
|[a0nJK2mKHwWYzpdG.htm](bestiary-effects/a0nJK2mKHwWYzpdG.htm)|Effect: Bully's Rage|auto-trad|
|[a4bZrBNL17zPjDiH.htm](bestiary-effects/a4bZrBNL17zPjDiH.htm)|Effect: Countered by Fire|auto-trad|
|[AL7E03DYahfDhbcR.htm](bestiary-effects/AL7E03DYahfDhbcR.htm)|Effect: Guardian's Aegis|auto-trad|
|[aOpSI5tApXF5xHCM.htm](bestiary-effects/aOpSI5tApXF5xHCM.htm)|Effect: Unsettled Aura|auto-trad|
|[APJJCYdq4gNe0PF4.htm](bestiary-effects/APJJCYdq4gNe0PF4.htm)|Effect: Susceptible to Mockery (Critical Failure)|auto-trad|
|[AThOnEYzPO9ssYTB.htm](bestiary-effects/AThOnEYzPO9ssYTB.htm)|Effect: Disturbing Vision|auto-trad|
|[auJ02rr1Jr88oD3Z.htm](bestiary-effects/auJ02rr1Jr88oD3Z.htm)|Effect: Claim Corpse - Fleshy|auto-trad|
|[BmAw66zEkifSvOtg.htm](bestiary-effects/BmAw66zEkifSvOtg.htm)|Effect: Brand of the Impenitent|auto-trad|
|[BsuYuFxE20mnRxbs.htm](bestiary-effects/BsuYuFxE20mnRxbs.htm)|Effect: Defensive Slam|auto-trad|
|[BTSstQAn4Xlm1PWV.htm](bestiary-effects/BTSstQAn4Xlm1PWV.htm)|Effect: Uncanny Tinker|auto-trad|
|[bV70ZxnUAoYnfXaZ.htm](bestiary-effects/bV70ZxnUAoYnfXaZ.htm)|Effect: Rune of Smiting (Rock)|auto-trad|
|[c6SiBB3mzV8lZUQr.htm](bestiary-effects/c6SiBB3mzV8lZUQr.htm)|Effect: Protean Anatomy|auto-trad|
|[C9nb9XbnQgbnXpTq.htm](bestiary-effects/C9nb9XbnQgbnXpTq.htm)|Effect: Crystalline Dust Form|auto-trad|
|[Cc5WsKkbOPOzopCY.htm](bestiary-effects/Cc5WsKkbOPOzopCY.htm)|Effect: Dire Warning|auto-trad|
|[CiCG3r7SHYMJeUxz.htm](bestiary-effects/CiCG3r7SHYMJeUxz.htm)|Effect: Inspirational Presence|auto-trad|
|[ckL8iL7BKBPteEVL.htm](bestiary-effects/ckL8iL7BKBPteEVL.htm)|Effect: Goblin Song|auto-trad|
|[CoPPOCyfADvybcxV.htm](bestiary-effects/CoPPOCyfADvybcxV.htm)|Effect: Gleaming Armor|auto-trad|
|[CUgmb7g65vYNT2hn.htm](bestiary-effects/CUgmb7g65vYNT2hn.htm)|Effect: Curse of Boiling Blood|auto-trad|
|[dNs30jfFOJqhjFW7.htm](bestiary-effects/dNs30jfFOJqhjFW7.htm)|Effect: Entangling Train|auto-trad|
|[dRe8n1nBWMIXd8jh.htm](bestiary-effects/dRe8n1nBWMIXd8jh.htm)|Effect: Moon Frenzy|auto-trad|
|[dVVc5MYx4G3QPHoa.htm](bestiary-effects/dVVc5MYx4G3QPHoa.htm)|Effect: Guildmaster's Lead|auto-trad|
|[E10qJgInN8Fx3yhW.htm](bestiary-effects/E10qJgInN8Fx3yhW.htm)|Effect: Distracting Frolic|auto-trad|
|[e4HoYakV7SvwvcrH.htm](bestiary-effects/e4HoYakV7SvwvcrH.htm)|Effect: Divine Aegis|auto-trad|
|[EM0VP31B8CyjFD15.htm](bestiary-effects/EM0VP31B8CyjFD15.htm)|Effect: Inspired Feast|auto-trad|
|[EqdXFH2l0TyeOTQz.htm](bestiary-effects/EqdXFH2l0TyeOTQz.htm)|Effect: Bloodline Magic (Stirvyn Banyan)|auto-trad|
|[EwhWYPCNM8bIIYEI.htm](bestiary-effects/EwhWYPCNM8bIIYEI.htm)|Effect: Defensive Slice|auto-trad|
|[eZL4B1P4H8hr1CAn.htm](bestiary-effects/eZL4B1P4H8hr1CAn.htm)|Effect: Glowing Spores|auto-trad|
|[f8rncEs06bqcn3LZ.htm](bestiary-effects/f8rncEs06bqcn3LZ.htm)|Effect: Sandstorm|auto-trad|
|[fGI77mVeCQKBAP0f.htm](bestiary-effects/fGI77mVeCQKBAP0f.htm)|Effect: Drain Luck|auto-trad|
|[FISKjI7bal26AnyL.htm](bestiary-effects/FISKjI7bal26AnyL.htm)|Effect: Timely Advice|auto-trad|
|[FjcvJWPFwkyy5vEk.htm](bestiary-effects/FjcvJWPFwkyy5vEk.htm)|Effect: Artificer's Command|auto-trad|
|[fPhbDSTPuT01f0Kn.htm](bestiary-effects/fPhbDSTPuT01f0Kn.htm)|Effect: Ensnare|auto-trad|
|[FQrNDisx6noJnFaQ.htm](bestiary-effects/FQrNDisx6noJnFaQ.htm)|Effect: Fettering Fedora|auto-trad|
|[g4CRjb2zS1pkVDbk.htm](bestiary-effects/g4CRjb2zS1pkVDbk.htm)|Effect: Sticky Spores|auto-trad|
|[g5Yen1FhEMCzCJkv.htm](bestiary-effects/g5Yen1FhEMCzCJkv.htm)|Effect: Profane Gift|auto-trad|
|[GC5441z8i7N8swk1.htm](bestiary-effects/GC5441z8i7N8swk1.htm)|Effect: Pulse of Rage|auto-trad|
|[gHczZWA34WQx4px0.htm](bestiary-effects/gHczZWA34WQx4px0.htm)|Effect: Bully the Departed|auto-trad|
|[gORyINQLvplfthlm.htm](bestiary-effects/gORyINQLvplfthlm.htm)|Effect: Pollution Infusion|auto-trad|
|[GstmxSP75eKmHtCQ.htm](bestiary-effects/GstmxSP75eKmHtCQ.htm)|Effect: Hallucinogenic Pollen (Failure)|auto-trad|
|[gxBjyPaYDdhjAImf.htm](bestiary-effects/gxBjyPaYDdhjAImf.htm)|Effect: Ancestral Response|auto-trad|
|[H99PJXU5cvJ4Oycm.htm](bestiary-effects/H99PJXU5cvJ4Oycm.htm)|Effect: Fiery Form|auto-trad|
|[HafP5vrdGdoqoFMo.htm](bestiary-effects/HafP5vrdGdoqoFMo.htm)|Effect: Barbed Net|auto-trad|
|[HArljmKc2IR7rtfc.htm](bestiary-effects/HArljmKc2IR7rtfc.htm)|Effect: Swig|auto-trad|
|[hc7kR6quUE9ryRyj.htm](bestiary-effects/hc7kR6quUE9ryRyj.htm)|Effect: Splintered Ground (Critical Failure)|auto-trad|
|[hdsl0XexqEL2emE3.htm](bestiary-effects/hdsl0XexqEL2emE3.htm)|Effect: Lantern of Hope (Gestalt)|auto-trad|
|[he0QiJSgJWNeIoxt.htm](bestiary-effects/he0QiJSgJWNeIoxt.htm)|Effect: Wrath of Spurned Hospitality|auto-trad|
|[HF2kOaqrjCkaZnK9.htm](bestiary-effects/HF2kOaqrjCkaZnK9.htm)|Effect: Prepare Fangs|auto-trad|
|[hftFxE8JGJGzXAtU.htm](bestiary-effects/hftFxE8JGJGzXAtU.htm)|Effect: Song of the Swamp|auto-trad|
|[HGP0c0tOK2WnC4p5.htm](bestiary-effects/HGP0c0tOK2WnC4p5.htm)|Effect: Breach the Abyss - Fire|auto-trad|
|[HKyMZobjOsd6WFuo.htm](bestiary-effects/HKyMZobjOsd6WFuo.htm)|Effect: Hurtful Critique|auto-trad|
|[HpX3bGlOkSp1PeWg.htm](bestiary-effects/HpX3bGlOkSp1PeWg.htm)|Effect: Vent Energy|auto-trad|
|[hQsqr2sRPp9rNm4U.htm](bestiary-effects/hQsqr2sRPp9rNm4U.htm)|Effect: Putrid Blast|auto-trad|
|[Hv4NO0HADyAAdS4F.htm](bestiary-effects/Hv4NO0HADyAAdS4F.htm)|Effect: Bark Orders|auto-trad|
|[i3942RucZD9OHbAE.htm](bestiary-effects/i3942RucZD9OHbAE.htm)|Effect: Void Shroud|auto-trad|
|[i4S6h5c1KK4FLq6w.htm](bestiary-effects/i4S6h5c1KK4FLq6w.htm)|Effect: Feed|auto-trad|
|[I5Fd4TkIKRJT6WXf.htm](bestiary-effects/I5Fd4TkIKRJT6WXf.htm)|Effect: Aura of Righteousness (Planetar)|auto-trad|
|[iDLu83vhWoNIE7xt.htm](bestiary-effects/iDLu83vhWoNIE7xt.htm)|Effect: Commanding Aura (Quara)|auto-trad|
|[IfUod2VxNmZMGGPq.htm](bestiary-effects/IfUod2VxNmZMGGPq.htm)|Effect: Bodyguard's Defense|auto-trad|
|[irSKPLF5oLfnqUNW.htm](bestiary-effects/irSKPLF5oLfnqUNW.htm)|Effect: Spotlight|auto-trad|
|[IrXb05caCTkP792e.htm](bestiary-effects/IrXb05caCTkP792e.htm)|Effect: Reminder of Doom|auto-trad|
|[iSyyR4QVOaY6mxtg.htm](bestiary-effects/iSyyR4QVOaY6mxtg.htm)|Effect: Nymph Queen's Inspiration|auto-trad|
|[itdoXvcDo8wwmTME.htm](bestiary-effects/itdoXvcDo8wwmTME.htm)|Effect: Despoiler|auto-trad|
|[itVZwwvaXYN2zOR8.htm](bestiary-effects/itVZwwvaXYN2zOR8.htm)|Effect: Parry Dance|auto-trad|
|[iuy6AA6sQZUZkN9X.htm](bestiary-effects/iuy6AA6sQZUZkN9X.htm)|Effect: Rapid Evolution - Husk|auto-trad|
|[iWLAcND0iyW4NUZm.htm](bestiary-effects/iWLAcND0iyW4NUZm.htm)|Effect: Viscous Trap|auto-trad|
|[iXVsVqttUGlfdIHS.htm](bestiary-effects/iXVsVqttUGlfdIHS.htm)|Effect: Share Pain|auto-trad|
|[JlaO7fgm53Om1DxB.htm](bestiary-effects/JlaO7fgm53Om1DxB.htm)|Effect: Swell|auto-trad|
|[jrfuplUEq3PIo6j1.htm](bestiary-effects/jrfuplUEq3PIo6j1.htm)|Effect: Blasphemous Utterances|auto-trad|
|[K3r1lfbtCTf3dFhV.htm](bestiary-effects/K3r1lfbtCTf3dFhV.htm)|Effect: Stick a Fork in It|auto-trad|
|[KdV8UtN8Af5oCerj.htm](bestiary-effects/KdV8UtN8Af5oCerj.htm)|Effect: Cite Precedent|auto-trad|
|[kdZEZ6iBYQPVGzYD.htm](bestiary-effects/kdZEZ6iBYQPVGzYD.htm)|Effect: Call to Action|auto-trad|
|[kHRbOHKEedKaBFH5.htm](bestiary-effects/kHRbOHKEedKaBFH5.htm)|Effect: Transfer Protection|auto-trad|
|[kjP0J97hhoIhbF3M.htm](bestiary-effects/kjP0J97hhoIhbF3M.htm)|Effect: Necrotic Field|auto-trad|
|[KjWPSJonzrZ8jv7X.htm](bestiary-effects/KjWPSJonzrZ8jv7X.htm)|Effect: Phalanx Fighter|auto-trad|
|[kOGRUuyK04MjLc3m.htm](bestiary-effects/kOGRUuyK04MjLc3m.htm)|Effect: Spiritual Warden|auto-trad|
|[KpRRcLsAiIhJFE03.htm](bestiary-effects/KpRRcLsAiIhJFE03.htm)|Effect: Furious Possession|auto-trad|
|[l62iAFL3EO7wSsLL.htm](bestiary-effects/l62iAFL3EO7wSsLL.htm)|Effect: Form a Phalanx|auto-trad|
|[L7SiTshdimUPWfnB.htm](bestiary-effects/L7SiTshdimUPWfnB.htm)|Effect: Witchflame (Failure)|auto-trad|
|[lBpprC8VD4GRzTtg.htm](bestiary-effects/lBpprC8VD4GRzTtg.htm)|Effect: Wild Shape (Oaksteward)|auto-trad|
|[LgZx5xotO08JzhVc.htm](bestiary-effects/LgZx5xotO08JzhVc.htm)|Effect: Mutilating Bite|auto-trad|
|[lIEc8Lx3ROm17Dqa.htm](bestiary-effects/lIEc8Lx3ROm17Dqa.htm)|Effect: Fetid Screech (Critical Failure)|auto-trad|
|[LohrnRXCQ0yVt8MK.htm](bestiary-effects/LohrnRXCQ0yVt8MK.htm)|Effect: Raise Guard|auto-trad|
|[LOLoQpaAp5cC0hbm.htm](bestiary-effects/LOLoQpaAp5cC0hbm.htm)|Effect: Blood Fury|auto-trad|
|[lt2t24E4hiByHhi3.htm](bestiary-effects/lt2t24E4hiByHhi3.htm)|Effect: Lost Plates|auto-trad|
|[lyeRL7khixdlJsaf.htm](bestiary-effects/lyeRL7khixdlJsaf.htm)|Effect: Death Gasp|auto-trad|
|[McawF8weCe7O81um.htm](bestiary-effects/McawF8weCe7O81um.htm)|Effect: Witchflame (Critical Failure)|auto-trad|
|[Me16QQGxpivWE2WW.htm](bestiary-effects/Me16QQGxpivWE2WW.htm)|Effect: Gloom Aura|auto-trad|
|[mFnSXW6HTwS7Iu1k.htm](bestiary-effects/mFnSXW6HTwS7Iu1k.htm)|Effect: Hunted Fear|auto-trad|
|[MKC2ne315YiCUiWd.htm](bestiary-effects/MKC2ne315YiCUiWd.htm)|Effect: Inspire Defense (Love Siktempora)|auto-trad|
|[mo4IRyv7GGRBJihU.htm](bestiary-effects/mo4IRyv7GGRBJihU.htm)|Effect: Discerning Aura|auto-trad|
|[MxArXuo8JRCm1hus.htm](bestiary-effects/MxArXuo8JRCm1hus.htm)|Effect: Unsettling Attention|auto-trad|
|[Mz1ihIHaHngdFhsA.htm](bestiary-effects/Mz1ihIHaHngdFhsA.htm)|Effect: Musk|auto-trad|
|[n96DB8vpPr1Fu6GZ.htm](bestiary-effects/n96DB8vpPr1Fu6GZ.htm)|Effect: Smash Kneecaps|auto-trad|
|[NHwmrSyF0e8HUE3m.htm](bestiary-effects/NHwmrSyF0e8HUE3m.htm)|Effect: Witchflame (Success)|auto-trad|
|[NLjzI7gt6djXSoXc.htm](bestiary-effects/NLjzI7gt6djXSoXc.htm)|Effect: Remove Face|auto-trad|
|[No817gKxdliCVQ9K.htm](bestiary-effects/No817gKxdliCVQ9K.htm)|Effect: Energy Ward|auto-trad|
|[o0zqsH5kYopRanav.htm](bestiary-effects/o0zqsH5kYopRanav.htm)|Effect: Rope Snare|auto-trad|
|[o6ZI01zZBQDASuaw.htm](bestiary-effects/o6ZI01zZBQDASuaw.htm)|Effect: Wolf Shape|auto-trad|
|[ob00D61F0c4PIvj1.htm](bestiary-effects/ob00D61F0c4PIvj1.htm)|Effect: Utter Despair|auto-trad|
|[OMJnZspk5YHai8yn.htm](bestiary-effects/OMJnZspk5YHai8yn.htm)|Effect: Fed by Metal|auto-trad|
|[oORIapm28xioxPDp.htm](bestiary-effects/oORIapm28xioxPDp.htm)|Effect: Blood Wake|auto-trad|
|[otUESSH8BDdPKHtb.htm](bestiary-effects/otUESSH8BDdPKHtb.htm)|Effect: Hamstring|auto-trad|
|[OxOMYmlPtjsEkRtY.htm](bestiary-effects/OxOMYmlPtjsEkRtY.htm)|Effect: Aura of Command|auto-trad|
|[P4bNtDVyknQwjTPo.htm](bestiary-effects/P4bNtDVyknQwjTPo.htm)|Effect: Runic Resistance|auto-trad|
|[PFeGCDFOo2AjI6ib.htm](bestiary-effects/PFeGCDFOo2AjI6ib.htm)|Effect: Ganzi Resistance|auto-trad|
|[pfG1S7yShwkHT9e0.htm](bestiary-effects/pfG1S7yShwkHT9e0.htm)|Effect: Calming Bioluminescence|auto-trad|
|[PJxUFFeAvQJ4l52f.htm](bestiary-effects/PJxUFFeAvQJ4l52f.htm)|Effect: Choose Weakness|auto-trad|
|[pMPe01GWvfwcYwUR.htm](bestiary-effects/pMPe01GWvfwcYwUR.htm)|Effect: Defensive Assault|auto-trad|
|[pvA97TXnq7wCbXp6.htm](bestiary-effects/pvA97TXnq7wCbXp6.htm)|Effect: Call to Halt (Success)|auto-trad|
|[PxLWRiCbgpqOTLO9.htm](bestiary-effects/PxLWRiCbgpqOTLO9.htm)|Effect: Towering Stance (Disbelieved)|auto-trad|
|[q2D1QBalqBQfKzTc.htm](bestiary-effects/q2D1QBalqBQfKzTc.htm)|Effect: Hurl Net|auto-trad|
|[Qa0HP5uCuqh4vxBh.htm](bestiary-effects/Qa0HP5uCuqh4vxBh.htm)|Effect: Consume Organ|auto-trad|
|[qddahkHks1iLZ4sM.htm](bestiary-effects/qddahkHks1iLZ4sM.htm)|Effect: Praise Adachros|auto-trad|
|[QDs8t1U1IepzYlyi.htm](bestiary-effects/QDs8t1U1IepzYlyi.htm)|Effect: Sylvan Wine|auto-trad|
|[qFDiWpzk8cuwQGyH.htm](bestiary-effects/qFDiWpzk8cuwQGyH.htm)|Effect: Hypnotic Stare|auto-trad|
|[QjgLHIgRXIk3KAOY.htm](bestiary-effects/QjgLHIgRXIk3KAOY.htm)|Effect: Curl Up|auto-trad|
|[QMcmhifNQOPguv2t.htm](bestiary-effects/QMcmhifNQOPguv2t.htm)|Effect: Ectoplasmic Form (Physical)|auto-trad|
|[Qomp2EujVCbzJb4X.htm](bestiary-effects/Qomp2EujVCbzJb4X.htm)|Effect: Filth Wave|auto-trad|
|[QoneHsjZKtGHWlam.htm](bestiary-effects/QoneHsjZKtGHWlam.htm)|Effect: Aura of Misfortune|auto-trad|
|[QpG9AaYpXJzDEjZA.htm](bestiary-effects/QpG9AaYpXJzDEjZA.htm)|Effect: Devour Soul|auto-trad|
|[QqC9xymSXSVunDgl.htm](bestiary-effects/QqC9xymSXSVunDgl.htm)|Effect: Tail Lash (Attack Roll)|auto-trad|
|[QSy3KaLI1AYTR7rP.htm](bestiary-effects/QSy3KaLI1AYTR7rP.htm)|Effect: Wrath of Fate|auto-trad|
|[R8clbf7gtIlU3fIj.htm](bestiary-effects/R8clbf7gtIlU3fIj.htm)|Effect: Crush of Hundreds|auto-trad|
|[RAKgTTFKc2YEbvrc.htm](bestiary-effects/RAKgTTFKc2YEbvrc.htm)|Effect: Hallucinogenic Pollen (Critical Failure)|auto-trad|
|[RBfd5qwFkuKxNb57.htm](bestiary-effects/RBfd5qwFkuKxNb57.htm)|Effect: Rapid Evolution - Energy Gland|auto-trad|
|[rbgXQqXbpaQCcNNx.htm](bestiary-effects/rbgXQqXbpaQCcNNx.htm)|Effect: Read the Stars (Critical Success)|auto-trad|
|[RjrsiEloXs7ze1TZ.htm](bestiary-effects/RjrsiEloXs7ze1TZ.htm)|Effect: Aura of Vitality|auto-trad|
|[Rn1aE4uvq30iiH0Y.htm](bestiary-effects/Rn1aE4uvq30iiH0Y.htm)|Effect: Regression|auto-trad|
|[rpxdrOlzY2SOtMWB.htm](bestiary-effects/rpxdrOlzY2SOtMWB.htm)|Effect: Whirlwind Form|auto-trad|
|[rRgVlTHiNEJ86T7N.htm](bestiary-effects/rRgVlTHiNEJ86T7N.htm)|Effect: Nanite Surge (Glow)|auto-trad|
|[ryY6fdqpC5Ztnagd.htm](bestiary-effects/ryY6fdqpC5Ztnagd.htm)|Effect: Dirty Bomb (Critical Failure)|auto-trad|
|[s16XQpDz2HNzR9BB.htm](bestiary-effects/s16XQpDz2HNzR9BB.htm)|Effect: Aura of Protection (Solar)|auto-trad|
|[S38CM3hyr7DnsmRR.htm](bestiary-effects/S38CM3hyr7DnsmRR.htm)|Effect: Devour Soul (Victim Level 15 or Higher)|auto-trad|
|[s5mS7CyE0oOYlecv.htm](bestiary-effects/s5mS7CyE0oOYlecv.htm)|Effect: Lost Red Cap|auto-trad|
|[SA5f54t1dVah5fJm.htm](bestiary-effects/SA5f54t1dVah5fJm.htm)|Effect: Revert Form|auto-trad|
|[ScZ8tlV5zS0Jwnqn.htm](bestiary-effects/ScZ8tlV5zS0Jwnqn.htm)|Effect: Blood Frenzy|auto-trad|
|[seWuw1GMSm3kzCWV.htm](bestiary-effects/seWuw1GMSm3kzCWV.htm)|Effect: Fan Bolt|auto-trad|
|[sI73mEP9wZRvMpWw.htm](bestiary-effects/sI73mEP9wZRvMpWw.htm)|Effect: Curse of Frost|auto-trad|
|[SjrM6XVTLySToS12.htm](bestiary-effects/SjrM6XVTLySToS12.htm)|Effect: Soul Siphon|auto-trad|
|[St2QzqQvKN0IaXxG.htm](bestiary-effects/St2QzqQvKN0IaXxG.htm)|Effect: Kharna's Blessing|auto-trad|
|[svXRrZvixRInmabj.htm](bestiary-effects/svXRrZvixRInmabj.htm)|Effect: Rune of Destruction (Greatsword)|auto-trad|
|[T2tfacwoOozQfsIz.htm](bestiary-effects/T2tfacwoOozQfsIz.htm)|Effect: Infectious Aura|auto-trad|
|[T9wQ1LvsvPWTefQR.htm](bestiary-effects/T9wQ1LvsvPWTefQR.htm)|Effect: Under Command|auto-trad|
|[TbmkcfpKIs558skY.htm](bestiary-effects/TbmkcfpKIs558skY.htm)|Effect: Caustic Mucus|auto-trad|
|[TcgvhgpUkJFjVqBx.htm](bestiary-effects/TcgvhgpUkJFjVqBx.htm)|Effect: Stink Sap|auto-trad|
|[TEzLEXBZzNCp6flg.htm](bestiary-effects/TEzLEXBZzNCp6flg.htm)|Effect: Instinctual Tinker (Critical Success)|auto-trad|
|[TjRZbd52qWPjTbNT.htm](bestiary-effects/TjRZbd52qWPjTbNT.htm)|Effect: No Quarter!|auto-trad|
|[tJx9B2e3AET6PbJD.htm](bestiary-effects/tJx9B2e3AET6PbJD.htm)|Effect: Pain Frenzy|auto-trad|
|[Tkf33YglEZcZ6gOT.htm](bestiary-effects/Tkf33YglEZcZ6gOT.htm)|Effect: Curse of the Baneful Venom|auto-trad|
|[TSdrv1FmLHJnk9AD.htm](bestiary-effects/TSdrv1FmLHJnk9AD.htm)|Effect: Shadow Rapier|auto-trad|
|[tSF9z5VTeevxoww3.htm](bestiary-effects/tSF9z5VTeevxoww3.htm)|Effect: Harmonizing Aura (Enemies)|auto-trad|
|[tvybMInVm415jA3p.htm](bestiary-effects/tvybMInVm415jA3p.htm)|Effect: Bloodline Magic|auto-trad|
|[U05L8bPBcRHTtTB3.htm](bestiary-effects/U05L8bPBcRHTtTB3.htm)|Effect: Crushing Spirits|auto-trad|
|[u1Z7i2RyL6Sd9OVU.htm](bestiary-effects/u1Z7i2RyL6Sd9OVU.htm)|Effect: Take Them Down!|auto-trad|
|[u2z1BgpZsxl7Zc2z.htm](bestiary-effects/u2z1BgpZsxl7Zc2z.htm)|Effect: Poison Frenzy|auto-trad|
|[uB3CVn3momZO9h0L.htm](bestiary-effects/uB3CVn3momZO9h0L.htm)|Effect: Xulgath Stench|auto-trad|
|[ucAwSIZsGrpBLg6G.htm](bestiary-effects/ucAwSIZsGrpBLg6G.htm)|Effect: Alchemical Crossbow|auto-trad|
|[uFi8GhPkYY3ti40N.htm](bestiary-effects/uFi8GhPkYY3ti40N.htm)|Effect: Inspire Courage (Love Siktempora)|auto-trad|
|[UgoM6FBiZfc7KqCV.htm](bestiary-effects/UgoM6FBiZfc7KqCV.htm)|Effect: Curse of Stone|auto-trad|
|[UiISDbCjK6AUDsll.htm](bestiary-effects/UiISDbCjK6AUDsll.htm)|Effect: Guide's Warning|auto-trad|
|[uqjx1bwYduHwwC7d.htm](bestiary-effects/uqjx1bwYduHwwC7d.htm)|Effect: Guiding Words|auto-trad|
|[us3KiMrcGM8e6ri3.htm](bestiary-effects/us3KiMrcGM8e6ri3.htm)|Effect: Sylvan Wine (Horned Hunter)|auto-trad|
|[USMuxi8ACBJRUrdS.htm](bestiary-effects/USMuxi8ACBJRUrdS.htm)|Effect: Crocodile Form|auto-trad|
|[UuEUZU1fSUb23yOk.htm](bestiary-effects/UuEUZU1fSUb23yOk.htm)|Effect: Totems of the Past|auto-trad|
|[UxeoSRmPbD5hjiEi.htm](bestiary-effects/UxeoSRmPbD5hjiEi.htm)|Effect: Inspiring Presence|auto-trad|
|[UxNq9uo1GpDYTi5Z.htm](bestiary-effects/UxNq9uo1GpDYTi5Z.htm)|Effect: Waterlogged|auto-trad|
|[uZJOdounIHaFDC1t.htm](bestiary-effects/uZJOdounIHaFDC1t.htm)|Effect: Hydra Heads|auto-trad|
|[v1oNhc9xa8nWxmFQ.htm](bestiary-effects/v1oNhc9xa8nWxmFQ.htm)|Effect: Breach the Abyss - Acid|auto-trad|
|[v54mj5jknAynlCM9.htm](bestiary-effects/v54mj5jknAynlCM9.htm)|Effect: Call to Blood|auto-trad|
|[V60TWjo1G1r5ZtPM.htm](bestiary-effects/V60TWjo1G1r5ZtPM.htm)|Effect: Vulnerable to Curved Space|auto-trad|
|[VAy6IZ0ck7ALVL4b.htm](bestiary-effects/VAy6IZ0ck7ALVL4b.htm)|Effect: Rune of Smiting (Greatsword)|auto-trad|
|[vbxcQIjLVWa43sdt.htm](bestiary-effects/vbxcQIjLVWa43sdt.htm)|Effect: Rune of Destruction (Rock)|auto-trad|
|[vc3AOrJpacJ7T1hO.htm](bestiary-effects/vc3AOrJpacJ7T1hO.htm)|Effect: Deceitful Feast (Failure)|auto-trad|
|[VFIVdYmS3XTdd3hj.htm](bestiary-effects/VFIVdYmS3XTdd3hj.htm)|Effect: Curse of Fire|auto-trad|
|[vGL5LlFxVJqjy1gM.htm](bestiary-effects/vGL5LlFxVJqjy1gM.htm)|Effect: Breach the Abyss - Cold|auto-trad|
|[vJAHjBoEuFKGQoGs.htm](bestiary-effects/vJAHjBoEuFKGQoGs.htm)|Effect: Rune of Destruction (Longspear)|auto-trad|
|[vKTvlFD2OSgTPuvl.htm](bestiary-effects/vKTvlFD2OSgTPuvl.htm)|Effect: Breach the Abyss - Electricity|auto-trad|
|[VLOvyQiZKeXppS2L.htm](bestiary-effects/VLOvyQiZKeXppS2L.htm)|Effect: Gird in Prayer|auto-trad|
|[vuaungtl9VCBoHDK.htm](bestiary-effects/vuaungtl9VCBoHDK.htm)|Effect: Rockfall Failure|auto-trad|
|[vUFIbQ74jz22rOXw.htm](bestiary-effects/vUFIbQ74jz22rOXw.htm)|Effect: Mortal Reflection|auto-trad|
|[Vz9Cb8FEzNwfH3mE.htm](bestiary-effects/Vz9Cb8FEzNwfH3mE.htm)|Effect: Breach the Abyss - Negative|auto-trad|
|[wcNGbhwn3AuamtsX.htm](bestiary-effects/wcNGbhwn3AuamtsX.htm)|Effect: Scraping Clamor|auto-trad|
|[WfiaKdXNSxC3POcs.htm](bestiary-effects/WfiaKdXNSxC3POcs.htm)|Effect: Bosun's Command - Attack Bonus|auto-trad|
|[wHQZO70bSECtSUZn.htm](bestiary-effects/wHQZO70bSECtSUZn.htm)|Effect: Ancestral Journey|auto-trad|
|[wJsOZsYI2ZUVcGxc.htm](bestiary-effects/wJsOZsYI2ZUVcGxc.htm)|Effect: Fiddle|auto-trad|
|[wPIGBcpCqCWgrCiq.htm](bestiary-effects/wPIGBcpCqCWgrCiq.htm)|Effect: Blood Soak|auto-trad|
|[wPW6kVscJgBLPVKZ.htm](bestiary-effects/wPW6kVscJgBLPVKZ.htm)|Effect: Ghonhatine Feed|auto-trad|
|[wX9L6fbqVMLP05hn.htm](bestiary-effects/wX9L6fbqVMLP05hn.htm)|Effect: Stench|auto-trad|
|[XaYM6Td0yhx2POau.htm](bestiary-effects/XaYM6Td0yhx2POau.htm)|Effect: Haywire|auto-trad|
|[XH49blfNDjkqnH5N.htm](bestiary-effects/XH49blfNDjkqnH5N.htm)|Effect: Motivating Assault|auto-trad|
|[xiOi7btey4AS6jhe.htm](bestiary-effects/xiOi7btey4AS6jhe.htm)|Effect: Overtake Soul|auto-trad|
|[xmES7HVxM7L3j1EN.htm](bestiary-effects/xmES7HVxM7L3j1EN.htm)|Effect: Putrid Stench|auto-trad|
|[XSAmMuMgMvdjdpHc.htm](bestiary-effects/XSAmMuMgMvdjdpHc.htm)|Effect: Slime Trap|auto-trad|
|[XSXMGb5h4Um6YXiT.htm](bestiary-effects/XSXMGb5h4Um6YXiT.htm)|Effect: Air of Sickness|auto-trad|
|[xXfzzNaBOqDUbYBD.htm](bestiary-effects/xXfzzNaBOqDUbYBD.htm)|Effect: Rotting Stench|auto-trad|
|[XyhPcnlkg3mGqEwT.htm](bestiary-effects/XyhPcnlkg3mGqEwT.htm)|Effect: Rockfall Critical Failure|auto-trad|
|[Y5vsF5Kb8oNIccWo.htm](bestiary-effects/Y5vsF5Kb8oNIccWo.htm)|Effect: Rune of Smiting (Longspear)|auto-trad|
|[ya8r3IdzQ7QGNH8N.htm](bestiary-effects/ya8r3IdzQ7QGNH8N.htm)|Effect: Activate Defenses|auto-trad|
|[YDJnC3Bdc2GI6hX5.htm](bestiary-effects/YDJnC3Bdc2GI6hX5.htm)|Effect: Inspire Envoy|auto-trad|
|[yG27Ixwdpxqbrhzf.htm](bestiary-effects/yG27Ixwdpxqbrhzf.htm)|Effect: Bosun's Command - Speed Bonus|auto-trad|
|[YIXeQFdrt0vPvmsW.htm](bestiary-effects/YIXeQFdrt0vPvmsW.htm)|Effect: Snake Form|auto-trad|
|[yQd2Yoht8libCMCv.htm](bestiary-effects/yQd2Yoht8libCMCv.htm)|Effect: Fortune's Favor|auto-trad|
|[Yqq4AkZ9lrm4CcID.htm](bestiary-effects/Yqq4AkZ9lrm4CcID.htm)|Effect: Battle Cry|auto-trad|
|[yrenENpzVgBKsnNi.htm](bestiary-effects/yrenENpzVgBKsnNi.htm)|Effect: Alchemical Strike|auto-trad|
|[yxgHWK2rTZKKAzDN.htm](bestiary-effects/yxgHWK2rTZKKAzDN.htm)|Effect: Unluck Aura|auto-trad|
|[zBbmuudbWY8FaOwZ.htm](bestiary-effects/zBbmuudbWY8FaOwZ.htm)|Effect: Chug|auto-trad|
|[zBQM4rzLfkLzpET6.htm](bestiary-effects/zBQM4rzLfkLzpET6.htm)|Effect: Rune of Flames (Greatsword)|auto-trad|
|[zGWc4q0rXW9eMoYT.htm](bestiary-effects/zGWc4q0rXW9eMoYT.htm)|Effect: Phantasmagoric Fog|auto-trad|
|[zNOomUXHZHcc1PWD.htm](bestiary-effects/zNOomUXHZHcc1PWD.htm)|Effect: Vengeful Fibers|auto-trad|
|[zqgntQXIEbulBgZX.htm](bestiary-effects/zqgntQXIEbulBgZX.htm)|Effect: Share Defenses|auto-trad|
|[zTrZozbWDd1A5L5S.htm](bestiary-effects/zTrZozbWDd1A5L5S.htm)|Effect: Cat Sith's Mark|auto-trad|
|[zXIpCkTtFRkhwI4x.htm](bestiary-effects/zXIpCkTtFRkhwI4x.htm)|Effect: Sixfold Flurry|auto-trad|
|[zXzXDlh0HkyPJu8f.htm](bestiary-effects/zXzXDlh0HkyPJu8f.htm)|Effect: Ill Omen|auto-trad|
|[zzdOof9hHUf9s13H.htm](bestiary-effects/zzdOof9hHUf9s13H.htm)|Effect: Manipulate Luck (Bad)|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[ceOkHxhJNTcvZkCy.htm](bestiary-effects/ceOkHxhJNTcvZkCy.htm)|Effect: Blood Siphon (Critical Failure)|Efecto: Sifón de Sangre (Fallo Crítico)|modificada|
|[J6EwMsXtXg30eaiB.htm](bestiary-effects/J6EwMsXtXg30eaiB.htm)|Effect: Stoke the Fervent - High Tormentor|Efecto: Avivar a los fervientes - Alto Atormentador|modificada|
|[jQmfZcPD6xVYx5nb.htm](bestiary-effects/jQmfZcPD6xVYx5nb.htm)|Effect: Entangling Slime|Efecto: Limo enmarado|modificada|
|[yDut6pczQVRJqt2L.htm](bestiary-effects/yDut6pczQVRJqt2L.htm)|Effect: PFS Level Bump|Efecto: Subida de Nivel PFS|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
