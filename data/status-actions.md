# Estado de la traducción (actions)

 * **oficial**: 12
 * **modificada**: 37
 * **auto-trad**: 288
 * **ninguna**: 13


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[5tOovJGK2TQIBHUZ.htm](actions/5tOovJGK2TQIBHUZ.htm)|Grow|
|[BRR0HS8yRhgBfipz.htm](actions/BRR0HS8yRhgBfipz.htm)|A Quick Glimpse Beyond|
|[CCMemaB1RoVa0aOu.htm](actions/CCMemaB1RoVa0aOu.htm)|Bestial Clarity|
|[ePfdZYZRmycMpSWU.htm](actions/ePfdZYZRmycMpSWU.htm)|Avoid Dire Fate|
|[EuoCFSHTzi0VOfuf.htm](actions/EuoCFSHTzi0VOfuf.htm)|Harrow the Fiend|
|[GmqHs4NEhAjqyAze.htm](actions/GmqHs4NEhAjqyAze.htm)|Spiritual Aid|
|[gU93T2UqxLsX9gyF.htm](actions/gU93T2UqxLsX9gyF.htm)|Fated Not to Die|
|[lH3mlvs1rCyXw6iY.htm](actions/lH3mlvs1rCyXw6iY.htm)|Recall Under Pressure|
|[RxIPBjodIRm7Pd6W.htm](actions/RxIPBjodIRm7Pd6W.htm)|Lucky Break|
|[Xc5MLIbT5OPhnFIJ.htm](actions/Xc5MLIbT5OPhnFIJ.htm)|Indomitable Act|
|[YjrM5G8Up0wv6x0u.htm](actions/YjrM5G8Up0wv6x0u.htm)|Chaotic Destiny|
|[yM20ZnvmNxu9qMDV.htm](actions/yM20ZnvmNxu9qMDV.htm)|Compose Missive|
|[ZUAaBIAntZhFQixm.htm](actions/ZUAaBIAntZhFQixm.htm)|Deconstruct|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[06frwOOuo4HJtivl.htm](actions/06frwOOuo4HJtivl.htm)|Furious Strike|auto-trad|
|[1If9lLVoZdO8woVg.htm](actions/1If9lLVoZdO8woVg.htm)|Consume Flesh|auto-trad|
|[1kGNdIIhuglAjIp9.htm](actions/1kGNdIIhuglAjIp9.htm)|Treat Wounds|auto-trad|
|[1LDOfV8jEka09eXr.htm](actions/1LDOfV8jEka09eXr.htm)|Deceptive Sidestep|auto-trad|
|[1xRFPTFtWtGJ9ELw.htm](actions/1xRFPTFtWtGJ9ELw.htm)|Sense Motive|auto-trad|
|[21WIfSu7Xd7uKqV8.htm](actions/21WIfSu7Xd7uKqV8.htm)|Tumble Through|auto-trad|
|[2424g94rpoiN1IPh.htm](actions/2424g94rpoiN1IPh.htm)|Catharsis|auto-trad|
|[24PSsn1SKpUwYA1X.htm](actions/24PSsn1SKpUwYA1X.htm)|Retraining|auto-trad|
|[25WDi1cVUrW92sUj.htm](actions/25WDi1cVUrW92sUj.htm)|Clue In|auto-trad|
|[2EE4aF4SZpYf0R6H.htm](actions/2EE4aF4SZpYf0R6H.htm)|Pick a Lock|auto-trad|
|[2HJ4yuEFY1Cast4h.htm](actions/2HJ4yuEFY1Cast4h.htm)|High Jump|auto-trad|
|[2KV87ponbWXgGIhZ.htm](actions/2KV87ponbWXgGIhZ.htm)|Act Together|auto-trad|
|[2u915NdUyQan6uKF.htm](actions/2u915NdUyQan6uKF.htm)|Demoralize|auto-trad|
|[3cuTA58ObXhuFX2r.htm](actions/3cuTA58ObXhuFX2r.htm)|Bend Time|auto-trad|
|[3D9kGfwg4LUZBR9A.htm](actions/3D9kGfwg4LUZBR9A.htm)|Overdrive|auto-trad|
|[3f5DMFu8fPiqHpRg.htm](actions/3f5DMFu8fPiqHpRg.htm)|Sustain a Spell|auto-trad|
|[3yoajuKjwHZ9ApUY.htm](actions/3yoajuKjwHZ9ApUY.htm)|Grab an Edge|auto-trad|
|[3ZzoI9MTtJFd1Kjl.htm](actions/3ZzoI9MTtJFd1Kjl.htm)|Share Senses|auto-trad|
|[49F65W6VwuXhmv8K.htm](actions/49F65W6VwuXhmv8K.htm)|Drink Blood|auto-trad|
|[4IxCKbGaEM9nUhld.htm](actions/4IxCKbGaEM9nUhld.htm)|Finish The Job|auto-trad|
|[5bCZGpp9yFHXDz1j.htm](actions/5bCZGpp9yFHXDz1j.htm)|Entity's Resurgence|auto-trad|
|[5LW1k5DUkzbbYuYL.htm](actions/5LW1k5DUkzbbYuYL.htm)|Daydream Trance|auto-trad|
|[5p2AMqM9bOVnhwPT.htm](actions/5p2AMqM9bOVnhwPT.htm)|Recenter|auto-trad|
|[5V49l0K460CcvBOO.htm](actions/5V49l0K460CcvBOO.htm)|Defend Life|auto-trad|
|[6Oe9dg3Lu10slyeC.htm](actions/6Oe9dg3Lu10slyeC.htm)|Influence|auto-trad|
|[6SAdjml3OZw0BZnn.htm](actions/6SAdjml3OZw0BZnn.htm)|Thundering Roar|auto-trad|
|[74iat04PtfG8gn2Q.htm](actions/74iat04PtfG8gn2Q.htm)|Mighty Rage|auto-trad|
|[7blmbDrQFNfdT731.htm](actions/7blmbDrQFNfdT731.htm)|Shove|auto-trad|
|[7dq6bpYvLArJ5odx.htm](actions/7dq6bpYvLArJ5odx.htm)|Forge Documents|auto-trad|
|[7GeguyqyD1TjoC4r.htm](actions/7GeguyqyD1TjoC4r.htm)|Unleash Psyche|auto-trad|
|[8b63qGzqSUENQgOT.htm](actions/8b63qGzqSUENQgOT.htm)|Armor Up!|auto-trad|
|[8iCas8MeyqSXmE2a.htm](actions/8iCas8MeyqSXmE2a.htm)|Dream Research|auto-trad|
|[8kGpUUUSX1sB2OqQ.htm](actions/8kGpUUUSX1sB2OqQ.htm)|Call Companion|auto-trad|
|[8w6esW689NNbbq3i.htm](actions/8w6esW689NNbbq3i.htm)|Call on Ancient Blood|auto-trad|
|[9gDMkIfDifh61yLz.htm](actions/9gDMkIfDifh61yLz.htm)|Stop|auto-trad|
|[9kIvFxA5V9rvNWiH.htm](actions/9kIvFxA5V9rvNWiH.htm)|Thoughtful Reload|auto-trad|
|[9MJcQEMVVrmMyGyq.htm](actions/9MJcQEMVVrmMyGyq.htm)|Living Fortification|auto-trad|
|[9Nmz5114UB87n7Aj.htm](actions/9Nmz5114UB87n7Aj.htm)|Affix a Fulu|auto-trad|
|[9sXQf1SyppS5B6O1.htm](actions/9sXQf1SyppS5B6O1.htm)|Automaton Aim|auto-trad|
|[9X80J5RN21Uoaeiw.htm](actions/9X80J5RN21Uoaeiw.htm)|Binding Vow|auto-trad|
|[a47Npi5XHXo47y49.htm](actions/a47Npi5XHXo47y49.htm)|Blizzard Evasion|auto-trad|
|[A4L90h7FIgO5EyBx.htm](actions/A4L90h7FIgO5EyBx.htm)|Siegebreaker|auto-trad|
|[A72nHGUtNXgY5Ey9.htm](actions/A72nHGUtNXgY5Ey9.htm)|Delay|auto-trad|
|[aBQ8ajvEBByv45yz.htm](actions/aBQ8ajvEBByv45yz.htm)|Cast a Spell|auto-trad|
|[Ah5g9pDwWF9b9VW9.htm](actions/Ah5g9pDwWF9b9VW9.htm)|Rage|auto-trad|
|[aHE92WSosMDewe33.htm](actions/aHE92WSosMDewe33.htm)|Take a Taste|auto-trad|
|[AJeLwbQBt1YH3S6v.htm](actions/AJeLwbQBt1YH3S6v.htm)|Fade Into Daydreams|auto-trad|
|[AjLSHZSWQ90exdLo.htm](actions/AjLSHZSWQ90exdLo.htm)|Dismiss|auto-trad|
|[AJstokjdG6iDjVjE.htm](actions/AJstokjdG6iDjVjE.htm)|Impersonate|auto-trad|
|[akmQzZoNhyfCKFpL.htm](actions/akmQzZoNhyfCKFpL.htm)|Airy Step|auto-trad|
|[AOWwwXVDWwliFlWj.htm](actions/AOWwwXVDWwliFlWj.htm)|Blood Feast|auto-trad|
|[Aq2mXT2hLlstFL5C.htm](actions/Aq2mXT2hLlstFL5C.htm)|Invoke Celestial Privilege|auto-trad|
|[b3h3QqvwS5fnaiu1.htm](actions/b3h3QqvwS5fnaiu1.htm)|Banshee Cry|auto-trad|
|[b6CanpXyUKJgxEwq.htm](actions/b6CanpXyUKJgxEwq.htm)|Salt Wound|auto-trad|
|[B7wDxyjX66JkiCOV.htm](actions/B7wDxyjX66JkiCOV.htm)|Venom Draw|auto-trad|
|[Ba9EuLV1I2b1ue9P.htm](actions/Ba9EuLV1I2b1ue9P.htm)|Map the Area|auto-trad|
|[BCp3tn2HATUOUEdQ.htm](actions/BCp3tn2HATUOUEdQ.htm)|Gossip|auto-trad|
|[Bcxarzksqt9ezrs6.htm](actions/Bcxarzksqt9ezrs6.htm)|Stride|auto-trad|
|[bG91dbtbgOnw7Ofx.htm](actions/bG91dbtbgOnw7Ofx.htm)|Board|auto-trad|
|[BKMxC0ZvdYfdhx7C.htm](actions/BKMxC0ZvdYfdhx7C.htm)|Objection|auto-trad|
|[BlAOM2X92SI6HMtJ.htm](actions/BlAOM2X92SI6HMtJ.htm)|Seek|auto-trad|
|[bp0Up04x3dzGK5bB.htm](actions/bp0Up04x3dzGK5bB.htm)|Debilitating Strike|auto-trad|
|[bT3skovyLUtP22ME.htm](actions/bT3skovyLUtP22ME.htm)|Repair|auto-trad|
|[c8TGiZ48ygoSPofx.htm](actions/c8TGiZ48ygoSPofx.htm)|Swim|auto-trad|
|[CIqiFw9rqYnuzggq.htm](actions/CIqiFw9rqYnuzggq.htm)|Practical Research|auto-trad|
|[CPTolKAF55p7D7Sn.htm](actions/CPTolKAF55p7D7Sn.htm)|Mirror-Trickery|auto-trad|
|[CrUPaPlsxy2bswaT.htm](actions/CrUPaPlsxy2bswaT.htm)|Mesmerizing Performance|auto-trad|
|[cS9nfDRGD83bNU1p.htm](actions/cS9nfDRGD83bNU1p.htm)|Fly|auto-trad|
|[cx0juTYewwBmrYWv.htm](actions/cx0juTYewwBmrYWv.htm)|Beast's Charge|auto-trad|
|[cx5tBTy6weK6YSw9.htm](actions/cx5tBTy6weK6YSw9.htm)|Thermal Eruption|auto-trad|
|[cYdz2grcOcRt4jk6.htm](actions/cYdz2grcOcRt4jk6.htm)|Disable Device|auto-trad|
|[cYtYKa1gDEl7y2N0.htm](actions/cYtYKa1gDEl7y2N0.htm)|Defend|auto-trad|
|[D2PNfIw7U6Ks0VY4.htm](actions/D2PNfIw7U6Ks0VY4.htm)|Steel Your Resolve|auto-trad|
|[d5I6018Mci2SWokk.htm](actions/d5I6018Mci2SWokk.htm)|Leap|auto-trad|
|[D91jQs0wleU5ml4K.htm](actions/D91jQs0wleU5ml4K.htm)|Drink from the Chalice|auto-trad|
|[DCb62iCBrJXy0Ik6.htm](actions/DCb62iCBrJXy0Ik6.htm)|Request|auto-trad|
|[dCuvfq3r2K9wXY9g.htm](actions/dCuvfq3r2K9wXY9g.htm)|Basic Finisher|auto-trad|
|[DFRLID6lIRw7CzOT.htm](actions/DFRLID6lIRw7CzOT.htm)|Spring the Trap|auto-trad|
|[dLgAMt3TbkmLkUqE.htm](actions/dLgAMt3TbkmLkUqE.htm)|Ready|auto-trad|
|[dnaPJfA0CDLNrWcW.htm](actions/dnaPJfA0CDLNrWcW.htm)|Implement's Interruption|auto-trad|
|[DS9sDOWkXrz2xmHi.htm](actions/DS9sDOWkXrz2xmHi.htm)|Eldritch Shot|auto-trad|
|[Dt6B1slsBy8ipJu9.htm](actions/Dt6B1slsBy8ipJu9.htm)|Disarm|auto-trad|
|[DXIZ4DHGxhZiWNWb.htm](actions/DXIZ4DHGxhZiWNWb.htm)|Long-Term Rest|auto-trad|
|[DYn1igFjCGJEiP22.htm](actions/DYn1igFjCGJEiP22.htm)|Recall Ammunition|auto-trad|
|[e2ePMDa7ixbLRryj.htm](actions/e2ePMDa7ixbLRryj.htm)|Encouraging Words|auto-trad|
|[e5JscXqvBUC867oo.htm](actions/e5JscXqvBUC867oo.htm)|Find Fault|auto-trad|
|[EA5vuSgJfiHH7plD.htm](actions/EA5vuSgJfiHH7plD.htm)|Track|auto-trad|
|[EAP98XaChJEbgKcK.htm](actions/EAP98XaChJEbgKcK.htm)|Retributive Strike|auto-trad|
|[eBgO5gp5kKhGtmk9.htm](actions/eBgO5gp5kKhGtmk9.htm)|Water Transfer|auto-trad|
|[EcDcowQ8vS6cEfXd.htm](actions/EcDcowQ8vS6cEfXd.htm)|Scout Location|auto-trad|
|[EEDElIyin4z60PXx.htm](actions/EEDElIyin4z60PXx.htm)|Perform|auto-trad|
|[EeM0Czaep7G5ZSh5.htm](actions/EeM0Czaep7G5ZSh5.htm)|Ten Paces|auto-trad|
|[EfjoIuDmtUn4yiow.htm](actions/EfjoIuDmtUn4yiow.htm)|Opportune Riposte|auto-trad|
|[EHa0owz6mccnmSBf.htm](actions/EHa0owz6mccnmSBf.htm)|Final Surge|auto-trad|
|[EhLvRWFKhZ3HtrZO.htm](actions/EhLvRWFKhZ3HtrZO.htm)|Settle Emotions|auto-trad|
|[enQieRrITuEQZxx2.htm](actions/enQieRrITuEQZxx2.htm)|Selfish Shield|auto-trad|
|[ERNaJbLeFtdV96cZ.htm](actions/ERNaJbLeFtdV96cZ.htm)|De-Animating Gestures (True)|auto-trad|
|[ESMIHOOahLQoqxW1.htm](actions/ESMIHOOahLQoqxW1.htm)|Call Gun|auto-trad|
|[EwgTZBWsc8qKaViP.htm](actions/EwgTZBWsc8qKaViP.htm)|Investigate|auto-trad|
|[ewwCglB7XOPLUz72.htm](actions/ewwCglB7XOPLUz72.htm)|Lie|auto-trad|
|[F0JgJR2rXKOg9k1z.htm](actions/F0JgJR2rXKOg9k1z.htm)|Upstage|auto-trad|
|[F4Tz0YFz1Lr4eVZR.htm](actions/F4Tz0YFz1Lr4eVZR.htm)|De-Animating Gestures (False)|auto-trad|
|[Fe487XZBdqEI2InL.htm](actions/Fe487XZBdqEI2InL.htm)|Dispelling Bullet|auto-trad|
|[Fha8jFmfkOPxAsrZ.htm](actions/Fha8jFmfkOPxAsrZ.htm)|Calculate Threats|auto-trad|
|[fJImDBQfqfjKJOhk.htm](actions/fJImDBQfqfjKJOhk.htm)|Sense Direction|auto-trad|
|[FkfWKq9jhhPzKAbb.htm](actions/FkfWKq9jhhPzKAbb.htm)|Rampaging Ferocity|auto-trad|
|[fL7FhTBcKxMzLBAs.htm](actions/fL7FhTBcKxMzLBAs.htm)|Empty Vessel|auto-trad|
|[fodJ3zuwQsYnBbtk.htm](actions/fodJ3zuwQsYnBbtk.htm)|Exploit Vulnerability|auto-trad|
|[FzZAYGib08aEq5P2.htm](actions/FzZAYGib08aEq5P2.htm)|Accidental Shot|auto-trad|
|[gBhWEy3ToxQeCLQm.htm](actions/gBhWEy3ToxQeCLQm.htm)|Covered Reload|auto-trad|
|[ge56Lu1xXVFYUnLP.htm](actions/ge56Lu1xXVFYUnLP.htm)|Trip|auto-trad|
|[GkmbTGfg8KcgynOA.htm](actions/GkmbTGfg8KcgynOA.htm)|Create a Diversion|auto-trad|
|[GPd3hmyUSbcSBj39.htm](actions/GPd3hmyUSbcSBj39.htm)|Stellar Misfortune|auto-trad|
|[gxtq81VAhpmNvEgA.htm](actions/gxtq81VAhpmNvEgA.htm)|Tap Ley Line|auto-trad|
|[h4Tzdhqfryp5m2fO.htm](actions/h4Tzdhqfryp5m2fO.htm)|Harvest Heartsliver|auto-trad|
|[H6v1VgowHaKHnVlG.htm](actions/H6v1VgowHaKHnVlG.htm)|Burrow|auto-trad|
|[HAmGozJwLAal5v82.htm](actions/HAmGozJwLAal5v82.htm)|Intensify Vulnerability|auto-trad|
|[HbejhIywqIufrmVM.htm](actions/HbejhIywqIufrmVM.htm)|Arcane Cascade|auto-trad|
|[HCl3pzVefiv9ZKQW.htm](actions/HCl3pzVefiv9ZKQW.htm)|Aid|auto-trad|
|[hFRHPBj6wjAayNtW.htm](actions/hFRHPBj6wjAayNtW.htm)|Jinx|auto-trad|
|[hi56uHG1aAb84Zzu.htm](actions/hi56uHG1aAb84Zzu.htm)|Fight with Fear|auto-trad|
|[hPZQ5vA9QHEPtjFW.htm](actions/hPZQ5vA9QHEPtjFW.htm)|Spin Tale|auto-trad|
|[HYNhdaPtF1QmQbR3.htm](actions/HYNhdaPtF1QmQbR3.htm)|Drop Prone|auto-trad|
|[I75R9NSfsVrit6cU.htm](actions/I75R9NSfsVrit6cU.htm)|Cram|auto-trad|
|[I9k9qe4gOT8UVK4e.htm](actions/I9k9qe4gOT8UVK4e.htm)|Mist Blending|auto-trad|
|[iBf9uGn5LOHkWpZ6.htm](actions/iBf9uGn5LOHkWpZ6.htm)|Craft Disharmonic Instrument|auto-trad|
|[IE2nThCmoyhQA0Jn.htm](actions/IE2nThCmoyhQA0Jn.htm)|Avoid Notice|auto-trad|
|[iEYZ5PAim5Cvbila.htm](actions/iEYZ5PAim5Cvbila.htm)|Secure Disguises|auto-trad|
|[IGPbMkKjnlFW1w1a.htm](actions/IGPbMkKjnlFW1w1a.htm)|Bribe Contact|auto-trad|
|[iJLzVonevhsi2uPs.htm](actions/iJLzVonevhsi2uPs.htm)|Visions of Sin|auto-trad|
|[iTx0vXAhiS4lKwEi.htm](actions/iTx0vXAhiS4lKwEi.htm)|Psychic Defense|auto-trad|
|[Iuq8CeNqv3a0oWfQ.htm](actions/Iuq8CeNqv3a0oWfQ.htm)|Life Block|auto-trad|
|[IV8sgoLO6ShD3DCJ.htm](actions/IV8sgoLO6ShD3DCJ.htm)|Expel Maelstrom|auto-trad|
|[IX1VlVCL5sFTptEE.htm](actions/IX1VlVCL5sFTptEE.htm)|Liberating Step|auto-trad|
|[izvfZ561JTdeyh6i.htm](actions/izvfZ561JTdeyh6i.htm)|Goblin Jubilee|auto-trad|
|[jbmXxq56swDYw8hy.htm](actions/jbmXxq56swDYw8hy.htm)|Final Spite|auto-trad|
|[jftNJjBNxp2cleoi.htm](actions/jftNJjBNxp2cleoi.htm)|Expeditious Inspection|auto-trad|
|[JLPY5hl4qiJ1zLi1.htm](actions/JLPY5hl4qiJ1zLi1.htm)|Discover|auto-trad|
|[JpcegMRqdizrkG0m.htm](actions/JpcegMRqdizrkG0m.htm)|Slayer's Identification|auto-trad|
|[JPHWzD2soqjffeSU.htm](actions/JPHWzD2soqjffeSU.htm)|Drain Life|auto-trad|
|[jSC5AYEfliOPpO3H.htm](actions/jSC5AYEfliOPpO3H.htm)|Reloading Strike|auto-trad|
|[jtuWJa6Iyyd3gkVv.htm](actions/jtuWJa6Iyyd3gkVv.htm)|Gallop|auto-trad|
|[JuqmIAnkL9hVGai8.htm](actions/JuqmIAnkL9hVGai8.htm)|Hustle|auto-trad|
|[JUvAvruz7yRQXfz2.htm](actions/JUvAvruz7yRQXfz2.htm)|Long Jump|auto-trad|
|[JYi4MnsdFu618hPm.htm](actions/JYi4MnsdFu618hPm.htm)|Hunt Prey|auto-trad|
|[jZQoAHmGJvi53NRR.htm](actions/jZQoAHmGJvi53NRR.htm)|Psychometric Assessment|auto-trad|
|[k5TASjIxghvGCy7g.htm](actions/k5TASjIxghvGCy7g.htm)|Call to Axis|auto-trad|
|[K878asDgf1EF0B9S.htm](actions/K878asDgf1EF0B9S.htm)|Confident Finisher|auto-trad|
|[KAVf7AmRnbCAHrkT.htm](actions/KAVf7AmRnbCAHrkT.htm)|Attack of Opportunity|auto-trad|
|[kbUymGTjbesOKsV6.htm](actions/kbUymGTjbesOKsV6.htm)|Primal Roar|auto-trad|
|[KC6o1cvbr45xnMei.htm](actions/KC6o1cvbr45xnMei.htm)|Conjure Bullet|auto-trad|
|[KjoCEEmPGTeFE4hh.htm](actions/KjoCEEmPGTeFE4hh.htm)|Treat Poison|auto-trad|
|[kMcV8e5EZUxa6evt.htm](actions/kMcV8e5EZUxa6evt.htm)|Squeeze|auto-trad|
|[KMKuJ0onVS72t9Fv.htm](actions/KMKuJ0onVS72t9Fv.htm)|Manifest Soulforged Armament|auto-trad|
|[kUME7DMhhHH6eIiH.htm](actions/kUME7DMhhHH6eIiH.htm)|That's My Number|auto-trad|
|[kV3XM0YJeS2KCSOb.htm](actions/kV3XM0YJeS2KCSOb.htm)|Scout|auto-trad|
|[KvVdGkUi0iJwapvD.htm](actions/KvVdGkUi0iJwapvD.htm)|Fortify Camp|auto-trad|
|[KyKXp599tK9BgodC.htm](actions/KyKXp599tK9BgodC.htm)|Dutiful Retaliation|auto-trad|
|[lAvEK4yNL3Y7pVmr.htm](actions/lAvEK4yNL3Y7pVmr.htm)|Assume a Role|auto-trad|
|[lID4rJHAVZB6tavf.htm](actions/lID4rJHAVZB6tavf.htm)|Run Over|auto-trad|
|[lvqPQDdWT2DDO0k2.htm](actions/lvqPQDdWT2DDO0k2.htm)|Invest an Item|auto-trad|
|[LWrR6UiGm3eCAALJ.htm](actions/LWrR6UiGm3eCAALJ.htm)|Collect Spirit Remnant|auto-trad|
|[m0f2B7G9eaaTmhFL.htm](actions/m0f2B7G9eaaTmhFL.htm)|Devise a Stratagem|auto-trad|
|[M76ycLAqHoAgbcej.htm](actions/M76ycLAqHoAgbcej.htm)|Balance|auto-trad|
|[M8RCbthRhB4bxO9t.htm](actions/M8RCbthRhB4bxO9t.htm)|Iron Command|auto-trad|
|[Ma93dpT4K7JbP9gu.htm](actions/Ma93dpT4K7JbP9gu.htm)|Prove Peace|auto-trad|
|[mech0dhb4eKbCAu0.htm](actions/mech0dhb4eKbCAu0.htm)|Coughing Dragon|auto-trad|
|[Mh4Vdg6gu8g8RAjh.htm](actions/Mh4Vdg6gu8g8RAjh.htm)|Mirror's Reflection|auto-trad|
|[mk6rzaAzsBBRGJnh.htm](actions/mk6rzaAzsBBRGJnh.htm)|Call Upon the Brightness|auto-trad|
|[MLchOIG6uLvzK3r0.htm](actions/MLchOIG6uLvzK3r0.htm)|Gain Contact|auto-trad|
|[MLOkyKi1Y3N6y56Q.htm](actions/MLOkyKi1Y3N6y56Q.htm)|Raise Neck|auto-trad|
|[mVscmsZWWcVACdU5.htm](actions/mVscmsZWWcVACdU5.htm)|Soaring Flight|auto-trad|
|[MY6z2b4GPhAD2Eoa.htm](actions/MY6z2b4GPhAD2Eoa.htm)|Share Life|auto-trad|
|[n5vwBnLSlIXL9ptp.htm](actions/n5vwBnLSlIXL9ptp.htm)|Manifest Eidolon|auto-trad|
|[N6U02s9qJKQIvmQd.htm](actions/N6U02s9qJKQIvmQd.htm)|Wish for Luck|auto-trad|
|[naKVqd8POxcnGclz.htm](actions/naKVqd8POxcnGclz.htm)|Explode|auto-trad|
|[nbfNETdpee8CVM17.htm](actions/nbfNETdpee8CVM17.htm)|Flurry of Blows|auto-trad|
|[ncdryKskPwHMgHFh.htm](actions/ncdryKskPwHMgHFh.htm)|Wicked Thorns|auto-trad|
|[nSTMF6kYIt6rXhJx.htm](actions/nSTMF6kYIt6rXhJx.htm)|Seething Frenzy|auto-trad|
|[O1iircZOOCo42r9Y.htm](actions/O1iircZOOCo42r9Y.htm)|Ghost Shot|auto-trad|
|[oanRfXGLnBm6mMVg.htm](actions/oanRfXGLnBm6mMVg.htm)|Dragon Breath|auto-trad|
|[OdIUybJ3ddfL7wzj.htm](actions/OdIUybJ3ddfL7wzj.htm)|Stand|auto-trad|
|[OizxuPb44g3eHPFh.htm](actions/OizxuPb44g3eHPFh.htm)|Borrow an Arcane Spell|auto-trad|
|[OJ9cIvPukPT0rppR.htm](actions/OJ9cIvPukPT0rppR.htm)|Wyrm's Breath|auto-trad|
|[On5CQjX4euWqToly.htm](actions/On5CQjX4euWqToly.htm)|Resist Elf Magic|auto-trad|
|[OQaFzDtVEOMWizJJ.htm](actions/OQaFzDtVEOMWizJJ.htm)|Repeat a Spell|auto-trad|
|[Or6RLXeoZkN8CLdi.htm](actions/Or6RLXeoZkN8CLdi.htm)|Amulet's Abeyance|auto-trad|
|[orjJjLdm4XNAcFi8.htm](actions/orjJjLdm4XNAcFi8.htm)|Mark for Death|auto-trad|
|[OSefkMgojBLqmRDh.htm](actions/OSefkMgojBLqmRDh.htm)|Refocus|auto-trad|
|[OX4fy22hQgUHDr0q.htm](actions/OX4fy22hQgUHDr0q.htm)|Make an Impression|auto-trad|
|[plBGdZhqq5JBl1D8.htm](actions/plBGdZhqq5JBl1D8.htm)|Gather Information|auto-trad|
|[PM5jvValFkbFH3TV.htm](actions/PM5jvValFkbFH3TV.htm)|Mount|auto-trad|
|[PMbdMWc2QroouFGD.htm](actions/PMbdMWc2QroouFGD.htm)|Grapple|auto-trad|
|[pprgrYQ1QnIDGZiy.htm](actions/pprgrYQ1QnIDGZiy.htm)|Climb|auto-trad|
|[pTHMZLqWDJ3lkan9.htm](actions/pTHMZLqWDJ3lkan9.htm)|Smoke Blending|auto-trad|
|[pvQ5rY2zrtPI614F.htm](actions/pvQ5rY2zrtPI614F.htm)|Interact|auto-trad|
|[Q4kdWVOf2ztIBFg1.htm](actions/Q4kdWVOf2ztIBFg1.htm)|Identify Alchemy|auto-trad|
|[Q8H3lBgiUyGvZYTM.htm](actions/Q8H3lBgiUyGvZYTM.htm)|Telekinetic Assault|auto-trad|
|[qc0VsZ0UesnurUUB.htm](actions/qc0VsZ0UesnurUUB.htm)|Take a Breather|auto-trad|
|[QDW9H8XLIjuW2fE4.htm](actions/QDW9H8XLIjuW2fE4.htm)|Spellstrike|auto-trad|
|[Qf1ylAbdVi1rkc8M.htm](actions/Qf1ylAbdVi1rkc8M.htm)|Maneuver in Flight|auto-trad|
|[QHFMeJGzFWj2QczA.htm](actions/QHFMeJGzFWj2QczA.htm)|Quick Tincture|auto-trad|
|[QIrJJ1pl4H6DctaQ.htm](actions/QIrJJ1pl4H6DctaQ.htm)|Reconnoiter|auto-trad|
|[qm7xptMSozAinnPS.htm](actions/qm7xptMSozAinnPS.htm)|Arrest a Fall|auto-trad|
|[QNAVeNKtHA0EUw4X.htm](actions/QNAVeNKtHA0EUw4X.htm)|Feint|auto-trad|
|[r5Uth6yvCoE4tr9z.htm](actions/r5Uth6yvCoE4tr9z.htm)|Destructive Vengeance|auto-trad|
|[rI2MSXR1MQzgQUO7.htm](actions/rI2MSXR1MQzgQUO7.htm)|Grim Swagger|auto-trad|
|[Rlp7ND33yYfxiEWi.htm](actions/Rlp7ND33yYfxiEWi.htm)|Master Strike|auto-trad|
|[rmwa3OyhTZ2i2AHl.htm](actions/rmwa3OyhTZ2i2AHl.htm)|Craft|auto-trad|
|[rqT4LMH7qbfyScBi.htm](actions/rqT4LMH7qbfyScBi.htm)|Reclaim Destiny|auto-trad|
|[rSpCV0leurp2Bg2d.htm](actions/rSpCV0leurp2Bg2d.htm)|Instinctive Obfuscation|auto-trad|
|[s2RrhZx1f1X4YnYV.htm](actions/s2RrhZx1f1X4YnYV.htm)|Divert Lightning|auto-trad|
|[s4s40hJr5uRLOqix.htm](actions/s4s40hJr5uRLOqix.htm)|Change Tradition Focus|auto-trad|
|[S9PZFOVe7zhORkUc.htm](actions/S9PZFOVe7zhORkUc.htm)|Absorb into the Aegis|auto-trad|
|[SB7cMECVtE06kByk.htm](actions/SB7cMECVtE06kByk.htm)|Cover Tracks|auto-trad|
|[SjmKHgI7a5Z9JzBx.htm](actions/SjmKHgI7a5Z9JzBx.htm)|Force Open|auto-trad|
|[sL1J8cFwpy1lI359.htm](actions/sL1J8cFwpy1lI359.htm)|Study|auto-trad|
|[sn2hIy1iIJX9Vpgj.htm](actions/sn2hIy1iIJX9Vpgj.htm)|Point Out|auto-trad|
|[SPtzUNatWJvTK61y.htm](actions/SPtzUNatWJvTK61y.htm)|Spirit's Mercy|auto-trad|
|[t5nBkpjroaq7QBGK.htm](actions/t5nBkpjroaq7QBGK.htm)|Research|auto-trad|
|[TC7OcDa7JlWbqMaN.htm](actions/TC7OcDa7JlWbqMaN.htm)|Treat Disease|auto-trad|
|[tfa4Sh7wcxCEqL29.htm](actions/tfa4Sh7wcxCEqL29.htm)|Follow the Expert|auto-trad|
|[tHCqgwjtQtzNqVvd.htm](actions/tHCqgwjtQtzNqVvd.htm)|Coerce|auto-trad|
|[TiNDYUGlMmxzxBYU.htm](actions/TiNDYUGlMmxzxBYU.htm)|Search|auto-trad|
|[Tj055UcNm6UEgtCg.htm](actions/Tj055UcNm6UEgtCg.htm)|Crawl|auto-trad|
|[Tlrde2xh7AhesXNB.htm](actions/Tlrde2xh7AhesXNB.htm)|One Shot, One Kill|auto-trad|
|[tNrBIYct9l1lrW1I.htm](actions/tNrBIYct9l1lrW1I.htm)|Field of Roots|auto-trad|
|[TSDbyYRQwhIyY2Oq.htm](actions/TSDbyYRQwhIyY2Oq.htm)|Energy Shot|auto-trad|
|[tu5viJZT4zFE1sYn.htm](actions/tu5viJZT4zFE1sYn.htm)|Whirlwind Maul|auto-trad|
|[tuZnRWHixLArvaIf.htm](actions/tuZnRWHixLArvaIf.htm)|Glimpse of Redemption|auto-trad|
|[tw1KDRPdBAkg5DlS.htm](actions/tw1KDRPdBAkg5DlS.htm)|Clear a Path|auto-trad|
|[TXqTIwNGULs3j6CH.htm](actions/TXqTIwNGULs3j6CH.htm)|Bullying Press|auto-trad|
|[UAaQk93a30nx0nYY.htm](actions/UAaQk93a30nx0nYY.htm)|Affix a Talisman|auto-trad|
|[ublVm5gmCIm3eRdQ.htm](actions/ublVm5gmCIm3eRdQ.htm)|Ring Bell|auto-trad|
|[UEkGL7uAGYDPFNfK.htm](actions/UEkGL7uAGYDPFNfK.htm)|Fire in the Hole|auto-trad|
|[UHpkTuCtyaPqiCAB.htm](actions/UHpkTuCtyaPqiCAB.htm)|Step|auto-trad|
|[Ul4I0ER6pj3U5eAk.htm](actions/Ul4I0ER6pj3U5eAk.htm)|Invigorating Fear|auto-trad|
|[uMFB3uw8WTWL0LZD.htm](actions/uMFB3uw8WTWL0LZD.htm)|Draconic Frenzy|auto-trad|
|[uOlyklPCUWLtCaYI.htm](actions/uOlyklPCUWLtCaYI.htm)|Chassis Deflection|auto-trad|
|[Uq2qy9aGNQ5JcPI1.htm](actions/Uq2qy9aGNQ5JcPI1.htm)|Into the Fray|auto-trad|
|[uS3qDAgOkZ7b8ERL.htm](actions/uS3qDAgOkZ7b8ERL.htm)|Drive|auto-trad|
|[ust1jJSCZQUhBZIz.htm](actions/ust1jJSCZQUhBZIz.htm)|Take Cover|auto-trad|
|[UWdRX1VelipCzrCg.htm](actions/UWdRX1VelipCzrCg.htm)|Avert Gaze|auto-trad|
|[uWTQxEOj2pl45Kns.htm](actions/uWTQxEOj2pl45Kns.htm)|Sense Weakness|auto-trad|
|[UyMkWfVqdabLTgkH.htm](actions/UyMkWfVqdabLTgkH.htm)|Wind Them Up|auto-trad|
|[v82XtjAVN4ffgVVR.htm](actions/v82XtjAVN4ffgVVR.htm)|Drain Bonded Item|auto-trad|
|[VAcxOCFQLb3Bap7K.htm](actions/VAcxOCFQLb3Bap7K.htm)|Accept Echo|auto-trad|
|[VCUz5EnBUJF07j1a.htm](actions/VCUz5EnBUJF07j1a.htm)|Sever Conduit|auto-trad|
|[vdnuczo4ktS7ow7N.htm](actions/vdnuczo4ktS7ow7N.htm)|Prophecy's Pawn|auto-trad|
|[vFaNE7s7vFs9BxJW.htm](actions/vFaNE7s7vFs9BxJW.htm)|Set Free|auto-trad|
|[VjxZFuUXrCU94MWR.htm](actions/VjxZFuUXrCU94MWR.htm)|Strike|auto-trad|
|[VMozDqMMuK5kpoX4.htm](actions/VMozDqMMuK5kpoX4.htm)|Sneak|auto-trad|
|[VNuOwXIHafSLHvsZ.htm](actions/VNuOwXIHafSLHvsZ.htm)|Spellsling|auto-trad|
|[vO0Y1dVjNfbDyT4S.htm](actions/vO0Y1dVjNfbDyT4S.htm)|Vital Shot|auto-trad|
|[VOEWhPQfN3lvHivK.htm](actions/VOEWhPQfN3lvHivK.htm)|Foresight|auto-trad|
|[VQ5OxaDKE0lCj8Mr.htm](actions/VQ5OxaDKE0lCj8Mr.htm)|Shadow Step|auto-trad|
|[vvYsE7OiSSCXhPdr.htm](actions/vvYsE7OiSSCXhPdr.htm)|Set Explosives|auto-trad|
|[vZltxwRNvF5khf9a.htm](actions/vZltxwRNvF5khf9a.htm)|Boarding Assault|auto-trad|
|[W4M8L9WepGLamlHs.htm](actions/W4M8L9WepGLamlHs.htm)|Threatening Approach|auto-trad|
|[WpCs3QmPVn8SRbXy.htm](actions/WpCs3QmPVn8SRbXy.htm)|Touch and Go|auto-trad|
|[wQYmDStjdjn0I26t.htm](actions/wQYmDStjdjn0I26t.htm)|Release|auto-trad|
|[wt6jdjjje16Nx34f.htm](actions/wt6jdjjje16Nx34f.htm)|Jumping Jenny|auto-trad|
|[wwvPiG2kET2rkSAG.htm](actions/wwvPiG2kET2rkSAG.htm)|Change Shape (Kitsune)|auto-trad|
|[x1qSEkzHAviQ5jry.htm](actions/x1qSEkzHAviQ5jry.htm)|Lay Down Arms|auto-trad|
|[xccOiNL2W1EtfUYl.htm](actions/xccOiNL2W1EtfUYl.htm)|Pointed Question|auto-trad|
|[XeZwXzR1KBlJF770.htm](actions/XeZwXzR1KBlJF770.htm)|Resist Magic|auto-trad|
|[Xg57qG1rOfSSobke.htm](actions/Xg57qG1rOfSSobke.htm)|Breath Weapon (Riding Drake)|auto-trad|
|[xGqOIheAOV12RGU4.htm](actions/xGqOIheAOV12RGU4.htm)|Dueling Counter|auto-trad|
|[XH133DE3daobeav1.htm](actions/XH133DE3daobeav1.htm)|Screaming Skull|auto-trad|
|[xJEkXFJgEfEida27.htm](actions/xJEkXFJgEfEida27.htm)|Rally|auto-trad|
|[xjGwis0uaC2305pm.htm](actions/xjGwis0uaC2305pm.htm)|Raise a Shield|auto-trad|
|[XkrN7gxdRXTYYBkX.htm](actions/XkrN7gxdRXTYYBkX.htm)|Restore the Mind|auto-trad|
|[XMcnh4cSI32tljXa.htm](actions/XMcnh4cSI32tljXa.htm)|Hide|auto-trad|
|[xpsD4DsYHKXCB4ac.htm](actions/xpsD4DsYHKXCB4ac.htm)|Anchor|auto-trad|
|[XSGlLjI80LDCimAP.htm](actions/XSGlLjI80LDCimAP.htm)|Sustain an Activation|auto-trad|
|[xTK2zsWFyxSJvYbX.htm](actions/xTK2zsWFyxSJvYbX.htm)|Pursue a Lead|auto-trad|
|[Yb0C1uLzeHrVLl7a.htm](actions/Yb0C1uLzeHrVLl7a.htm)|Detect Magic|auto-trad|
|[Yfl6q6Pi42FttDRE.htm](actions/Yfl6q6Pi42FttDRE.htm)|Glimpse Vulnerability|auto-trad|
|[yh9O9BQjwWrAIiuf.htm](actions/yh9O9BQjwWrAIiuf.htm)|Take Control|auto-trad|
|[YMhmebfXAoOFXeSB.htm](actions/YMhmebfXAoOFXeSB.htm)|Drifter's Wake|auto-trad|
|[yOtu5X3qWfjuX8Vy.htm](actions/yOtu5X3qWfjuX8Vy.htm)|Learn Name|auto-trad|
|[YpuEmI1fJBZD3kMc.htm](actions/YpuEmI1fJBZD3kMc.htm)|Tendril Strike|auto-trad|
|[yUjuLbBMflVum8Yn.htm](actions/yUjuLbBMflVum8Yn.htm)|Launch Fireworks Display|auto-trad|
|[yXk0G8l0leaqHh1U.htm](actions/yXk0G8l0leaqHh1U.htm)|Host Spirit|auto-trad|
|[yzNJgwzV9XqEhKc6.htm](actions/yzNJgwzV9XqEhKc6.htm)|Quick Alchemy|auto-trad|
|[Z8aa6afUterlfh5i.htm](actions/Z8aa6afUterlfh5i.htm)|Travel|auto-trad|
|[ZhKXvnw7ND2hQ2pp.htm](actions/ZhKXvnw7ND2hQ2pp.htm)|Cleanse Soul Path|auto-trad|
|[ZJcc7KGOEsYvN6SE.htm](actions/ZJcc7KGOEsYvN6SE.htm)|Overload Vision|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[04VQuQih77pxX06q.htm](actions/04VQuQih77pxX06q.htm)|Fling Magic|Lanzar por los aires magia|modificada|
|[0XiPnnxKHWKFg1tN.htm](actions/0XiPnnxKHWKFg1tN.htm)|Breath Weapon (Piercing) (Cone)|Arma de Aliento (Perforante) (Cono)|modificada|
|[34E7k2YRcsOU5uyl.htm](actions/34E7k2YRcsOU5uyl.htm)|Change Shape (Anadi)|Cambiar Forma (Anadi)|modificada|
|[3UEwtMA5y8foVqYP.htm](actions/3UEwtMA5y8foVqYP.htm)|Breath Weapon (Electricity) (Line)|Arma de Aliento (Electricidad) (Línea)|modificada|
|[5Vuhf4DjvMvRqiEZ.htm](actions/5Vuhf4DjvMvRqiEZ.htm)|Breath Weapon (Negative) (Cone)|Arma de Aliento (Negativo) (Cono)|modificada|
|[6OZfIjiPjWTIe7Pr.htm](actions/6OZfIjiPjWTIe7Pr.htm)|Spinning Crush|Spinning Crush|modificada|
|[7qjfYsLNTr17Aftf.htm](actions/7qjfYsLNTr17Aftf.htm)|Energy Emanation (Acid)|Emanación de Energía (Ácido)|modificada|
|[8aIe57gXJ94mPpW4.htm](actions/8aIe57gXJ94mPpW4.htm)|Anadi Venom|Anadi Veneno|modificada|
|[90fHUm0HGZFAbG1K.htm](actions/90fHUm0HGZFAbG1K.htm)|Breath Weapon (Fire) (Line)|Arma de Aliento (Fuego) (Línea)|modificada|
|[auOTjKr8gB1dqFh4.htm](actions/auOTjKr8gB1dqFh4.htm)|Breath Weapon (Cold) (Cone)|Arma de aliento (Frío) (Cono)|modificada|
|[AWXPiqz4xDN03iPs.htm](actions/AWXPiqz4xDN03iPs.htm)|Breath Weapon (Poison) (Line)|Arma envenenada (Línea)|modificada|
|[bCDMuu3tE4d6KHrJ.htm](actions/bCDMuu3tE4d6KHrJ.htm)|Pistolero's Retort|Pistolero's Retort|modificada|
|[BKnN9la3WNrRgZ6n.htm](actions/BKnN9la3WNrRgZ6n.htm)|Conduct Energy|Conducir Energía|modificada|
|[EmFzFAwgQ0lJZwba.htm](actions/EmFzFAwgQ0lJZwba.htm)|Breath Weapon (Negative) (Line)|Arma de Aliento (Negativo) (Línea)|modificada|
|[ftG89SjTSa9DYDOD.htm](actions/ftG89SjTSa9DYDOD.htm)|Create Forgery|Elaborar Falsificar|modificada|
|[grkggNQnOxWWsjBH.htm](actions/grkggNQnOxWWsjBH.htm)|Breath Weapon (Cold) (Line)|Arma de Aliento (Frío) (Línea)|modificada|
|[ijZ0DDFpMkWqaShd.htm](actions/ijZ0DDFpMkWqaShd.htm)|Palm an Object|Escamotear un objeto|modificada|
|[JLyNyaRmxL51jebC.htm](actions/JLyNyaRmxL51jebC.htm)|Energy Emanation (Cold)|Emanación de Energía (Fría)|modificada|
|[JqThDeWlGobiAPJd.htm](actions/JqThDeWlGobiAPJd.htm)|Breath Weapon (Fire) (Cone)|Arma de Aliento (Fuego) (Cono)|modificada|
|[kKKHwVUnroKuAnOt.htm](actions/kKKHwVUnroKuAnOt.htm)|Toxic Skin|Piel tóxica|modificada|
|[kRxWINkrHUPSHHYq.htm](actions/kRxWINkrHUPSHHYq.htm)|Recall the Teachings|Recordar las Enseñanzas|modificada|
|[LfsQy2T8GjpuMqAh.htm](actions/LfsQy2T8GjpuMqAh.htm)|Breath Weapon (Electricity) (Cone)|Arma de Aliento (Electricidad) (Cono)|modificada|
|[Lt591Og3XRhx5dRx.htm](actions/Lt591Og3XRhx5dRx.htm)|Breath Weapon (Acid) (Cone)|Arma de Aliento (Ácido) (Cono)|modificada|
|[MHLuKy4nQO2Z4Am1.htm](actions/MHLuKy4nQO2Z4Am1.htm)|Administer First Aid|Prestar Primeros auxilios|modificada|
|[NEBuuhjZE9BL3I8v.htm](actions/NEBuuhjZE9BL3I8v.htm)|Breath Weapon (Poison) (Cone)|Arma envenenada (Cono)|modificada|
|[NYR2IdfUiHevdt1V.htm](actions/NYR2IdfUiHevdt1V.htm)|Break Them Down|Drive Them Down|modificada|
|[oAWNluJaMlaGysXA.htm](actions/oAWNluJaMlaGysXA.htm)|Barbed Quills|Púas punzantes|modificada|
|[QQQaV7pi9Gv2GpLj.htm](actions/QQQaV7pi9Gv2GpLj.htm)|Breath Weapon (Piercing) (Line)|Arma de Aliento (Perforante) (Línea)|modificada|
|[qVNVSmsgpKFGk9hV.htm](actions/qVNVSmsgpKFGk9hV.htm)|Conceal an Object|Ocultar un objeto|modificada|
|[RADNqsvAt4gP9FOX.htm](actions/RADNqsvAt4gP9FOX.htm)|Raconteur's Reload|Raconteur's Reload|modificada|
|[RDXXE7wMrSPCLv5k.htm](actions/RDXXE7wMrSPCLv5k.htm)|Steal|Sustraer|modificada|
|[s4V7JWSMF9JPJAeX.htm](actions/s4V7JWSMF9JPJAeX.htm)|Envenom|Envenom|modificada|
|[SkZAQRkLLkmBQNB9.htm](actions/SkZAQRkLLkmBQNB9.htm)|Escape|Huir|modificada|
|[slwllcUtVhoCdBuM.htm](actions/slwllcUtVhoCdBuM.htm)|Breath Weapon (Acid) (Line)|Arma de Aliento (Ácido) (Línea)|modificada|
|[SMF1hTWPHtmlS8Cd.htm](actions/SMF1hTWPHtmlS8Cd.htm)|Bullet Dancer Stance|Posición de bailarina de balas.|modificada|
|[toxlmkoJqOuy4Vz1.htm](actions/toxlmkoJqOuy4Vz1.htm)|Energy Emanation (Electricity)|Emanación de Energía (Electricidad)|modificada|
|[vblQt8LjvsBEB1J1.htm](actions/vblQt8LjvsBEB1J1.htm)|Energy Emanation (Fire)|Emanación de Energía (Fuego)|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[00LNVSCbwJ8pszwE.htm](actions/00LNVSCbwJ8pszwE.htm)|Mutagenic Flashback|Retropectiva Mutagénica|oficial|
|[0b3TTeQGFOiy6ykz.htm](actions/0b3TTeQGFOiy6ykz.htm)|Change Shape (Beastkin)|Cambiar la forma (Piel de bestia)|oficial|
|[1OagaWtBpVXExToo.htm](actions/1OagaWtBpVXExToo.htm)|Recall Knowledge|Recordar conocimiento|oficial|
|[49y9Ec4bDii8pcD3.htm](actions/49y9Ec4bDii8pcD3.htm)|Subsist|Subsistir|oficial|
|[7pdG8l9POMK76Lf2.htm](actions/7pdG8l9POMK76Lf2.htm)|Warding Sign|Símbolo de protección|oficial|
|[d9gbpiQjChYDYA2L.htm](actions/d9gbpiQjChYDYA2L.htm)|Decipher Writing|Descifrar escritura|oficial|
|[eReSHVEPCsdkSL4G.htm](actions/eReSHVEPCsdkSL4G.htm)|Identify Magic|Identificar magia|oficial|
|[ev8OHpBO3xq3Zt08.htm](actions/ev8OHpBO3xq3Zt08.htm)|Tail Toxin|Toxina de Cola|oficial|
|[Q5iIYCFdqJFM31GW.htm](actions/Q5iIYCFdqJFM31GW.htm)|Learn a Spell|Aprender un conjuro|oficial|
|[q9nbyIF0PEBqMtYe.htm](actions/q9nbyIF0PEBqMtYe.htm)|Command an Animal|Comandar a un animal|oficial|
|[QyzlsLrqM0EEwd7j.htm](actions/QyzlsLrqM0EEwd7j.htm)|Earn Income|Obtener ingresos|oficial|
|[TMBXArwICQRJdwT6.htm](actions/TMBXArwICQRJdwT6.htm)|Fey's Fortune|Fortuna de las hadas|oficial|
