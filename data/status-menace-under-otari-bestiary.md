# Estado de la traducción (menace-under-otari-bestiary)

 * **auto-trad**: 63


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0oWKApY5FR8IO7GG.htm](menace-under-otari-bestiary/0oWKApY5FR8IO7GG.htm)|Orc Scrapper (BB)|auto-trad|
|[0plBflWwrCWQO2RO.htm](menace-under-otari-bestiary/0plBflWwrCWQO2RO.htm)|Zombie Shambler (BB)|auto-trad|
|[2H2AEwQnfKJC0nrd.htm](menace-under-otari-bestiary/2H2AEwQnfKJC0nrd.htm)|Ghost Commoner (BB)|auto-trad|
|[4Axci50gPQArPg7d.htm](menace-under-otari-bestiary/4Axci50gPQArPg7d.htm)|Kobold Trapmaster (BB)|auto-trad|
|[4MwjCsa5O9aAjxSm.htm](menace-under-otari-bestiary/4MwjCsa5O9aAjxSm.htm)|Boar (BB)|auto-trad|
|[4O7wKZdeAemTEbvG.htm](menace-under-otari-bestiary/4O7wKZdeAemTEbvG.htm)|Slamming Door (BB)|auto-trad|
|[5H8ZX7y5IkUBhvhF.htm](menace-under-otari-bestiary/5H8ZX7y5IkUBhvhF.htm)|Skeleton Guard (BB)|auto-trad|
|[5MVBU86ZRw2ANMQn.htm](menace-under-otari-bestiary/5MVBU86ZRw2ANMQn.htm)|Skeletal Giant (BB)|auto-trad|
|[5xjmJoJvBhASkEKS.htm](menace-under-otari-bestiary/5xjmJoJvBhASkEKS.htm)|Orc Commander (BB)|auto-trad|
|[6NdqvKIlxo4cGhf8.htm](menace-under-otari-bestiary/6NdqvKIlxo4cGhf8.htm)|Giant Spider (BB)|auto-trad|
|[7VqibTAEXXX6PIhh.htm](menace-under-otari-bestiary/7VqibTAEXXX6PIhh.htm)|Scythe Blades (BB)|auto-trad|
|[93hZtLl9pRRfqI05.htm](menace-under-otari-bestiary/93hZtLl9pRRfqI05.htm)|Blue Kobold Dragon Mage (BB)|auto-trad|
|[9sa2KE4Fbh3OPH7M.htm](menace-under-otari-bestiary/9sa2KE4Fbh3OPH7M.htm)|Brine Shark (BB)|auto-trad|
|[AdQVjlOWB6rmBRVp.htm](menace-under-otari-bestiary/AdQVjlOWB6rmBRVp.htm)|Doppelganger (BB)|auto-trad|
|[aeCoh4u6c5kt1iCs.htm](menace-under-otari-bestiary/aeCoh4u6c5kt1iCs.htm)|Gargoyle (BB)|auto-trad|
|[AleeS0IRqT4tUphB.htm](menace-under-otari-bestiary/AleeS0IRqT4tUphB.htm)|Kobold Boss Zolgran (BB)|auto-trad|
|[AuCC04X2AO8oFN75.htm](menace-under-otari-bestiary/AuCC04X2AO8oFN75.htm)|Harpy (BB)|auto-trad|
|[AYwdybUfm4meGUTJ.htm](menace-under-otari-bestiary/AYwdybUfm4meGUTJ.htm)|Giant Rat (BB)|auto-trad|
|[BHq5wpQU8hQEke8D.htm](menace-under-otari-bestiary/BHq5wpQU8hQEke8D.htm)|Hidden Pit (BB)|auto-trad|
|[BKPRkJgq7ehsW7uX.htm](menace-under-otari-bestiary/BKPRkJgq7ehsW7uX.htm)|Giant Centipede (BB)|auto-trad|
|[Br1AtKUHe3nbzjnY.htm](menace-under-otari-bestiary/Br1AtKUHe3nbzjnY.htm)|Mimic (BB)|auto-trad|
|[cBHpMcVaLRPZu9po.htm](menace-under-otari-bestiary/cBHpMcVaLRPZu9po.htm)|Zephyr Hawk (BB)|auto-trad|
|[CJuHwIRCAgTB1SEl.htm](menace-under-otari-bestiary/CJuHwIRCAgTB1SEl.htm)|Red Kobold Dragon Mage (BB)|auto-trad|
|[cZDiyluplFqRxmGy.htm](menace-under-otari-bestiary/cZDiyluplFqRxmGy.htm)|Animated Armor (BB)|auto-trad|
|[EtRqBsWh1Hv1toqh.htm](menace-under-otari-bestiary/EtRqBsWh1Hv1toqh.htm)|Orc Trooper (BB)|auto-trad|
|[FaBHkmFGuEIqIYM1.htm](menace-under-otari-bestiary/FaBHkmFGuEIqIYM1.htm)|Drow Priestess (BB)|auto-trad|
|[gdXok08bITkhowDJ.htm](menace-under-otari-bestiary/gdXok08bITkhowDJ.htm)|Ogre Warrior (BB)|auto-trad|
|[gvCCATlH9mPGWbsp.htm](menace-under-otari-bestiary/gvCCATlH9mPGWbsp.htm)|Troll (BB)|auto-trad|
|[hiGwRWdxAsoCII4f.htm](menace-under-otari-bestiary/hiGwRWdxAsoCII4f.htm)|Cinder Rat (BB)|auto-trad|
|[Hkq9ZS2J2iKnT7vT.htm](menace-under-otari-bestiary/Hkq9ZS2J2iKnT7vT.htm)|Sewer Ooze (BB)|auto-trad|
|[j8qD2LVDSP2lhLUO.htm](menace-under-otari-bestiary/j8qD2LVDSP2lhLUO.htm)|Central Spears (BB)|auto-trad|
|[jeAGl6OAVrrIPgu3.htm](menace-under-otari-bestiary/jeAGl6OAVrrIPgu3.htm)|Hell Hound (BB)|auto-trad|
|[jGzVwekcRX5aQpbT.htm](menace-under-otari-bestiary/jGzVwekcRX5aQpbT.htm)|Goblin Commando (BB)|auto-trad|
|[jnmUcTs4hn1c5bz9.htm](menace-under-otari-bestiary/jnmUcTs4hn1c5bz9.htm)|Pugwampi (BB)|auto-trad|
|[jVZRROs0GzDjVrgi.htm](menace-under-otari-bestiary/jVZRROs0GzDjVrgi.htm)|Goblin Warrior (BB)|auto-trad|
|[kCRfBZqCugMQmdpd.htm](menace-under-otari-bestiary/kCRfBZqCugMQmdpd.htm)|White Kobold Dragon Mage (BB)|auto-trad|
|[KsWAIXTTh3mfNWOY.htm](menace-under-otari-bestiary/KsWAIXTTh3mfNWOY.htm)|Giant Viper (BB)|auto-trad|
|[lFlXmieuHTBIonhj.htm](menace-under-otari-bestiary/lFlXmieuHTBIonhj.htm)|Viper (BB)|auto-trad|
|[LHHgGSs0ELCR4CYK.htm](menace-under-otari-bestiary/LHHgGSs0ELCR4CYK.htm)|Ghoul (BB)|auto-trad|
|[M8oJOKJ4AgrLZcJQ.htm](menace-under-otari-bestiary/M8oJOKJ4AgrLZcJQ.htm)|Hobgoblin Warrior (BB)|auto-trad|
|[NVWaLagWOu5tCCZu.htm](menace-under-otari-bestiary/NVWaLagWOu5tCCZu.htm)|Sod Hound (BB)|auto-trad|
|[Oilfs8Atv2LjAsUS.htm](menace-under-otari-bestiary/Oilfs8Atv2LjAsUS.htm)|Wolf (BB)|auto-trad|
|[pw2NFqvkDm54lsbt.htm](menace-under-otari-bestiary/pw2NFqvkDm54lsbt.htm)|Envenomed Lock (BB)|auto-trad|
|[QaldZV2p9RpMXzzn.htm](menace-under-otari-bestiary/QaldZV2p9RpMXzzn.htm)|Green Kobold Dragon Mage (BB)|auto-trad|
|[R9eoGwQ2tudxUKxS.htm](menace-under-otari-bestiary/R9eoGwQ2tudxUKxS.htm)|Black Kobold Dragon Mage (BB)|auto-trad|
|[r9w1n85mp9Ip4QiS.htm](menace-under-otari-bestiary/r9w1n85mp9Ip4QiS.htm)|Kobold Warrior (BB)|auto-trad|
|[rPaHIh0ICnTLnRO6.htm](menace-under-otari-bestiary/rPaHIh0ICnTLnRO6.htm)|Kobold Scout (BB)|auto-trad|
|[rPHxXClTnoPYHYuZ.htm](menace-under-otari-bestiary/rPHxXClTnoPYHYuZ.htm)|Basilisk (BB)|auto-trad|
|[RTzFvmdSCf5yhguy.htm](menace-under-otari-bestiary/RTzFvmdSCf5yhguy.htm)|Xulgath Warrior (BB)|auto-trad|
|[shT19KaQjWRVrHLI.htm](menace-under-otari-bestiary/shT19KaQjWRVrHLI.htm)|Goblin Igniter (BB)|auto-trad|
|[sW8koPLrSgHalAnq.htm](menace-under-otari-bestiary/sW8koPLrSgHalAnq.htm)|Drow Warrior (BB)|auto-trad|
|[UjREHs2JQoO85Glt.htm](menace-under-otari-bestiary/UjREHs2JQoO85Glt.htm)|Bugbear Marauder (BB)|auto-trad|
|[v51J7K27abdDyLgJ.htm](menace-under-otari-bestiary/v51J7K27abdDyLgJ.htm)|Mermaid Fountain (BB)|auto-trad|
|[vlMuFskctUvjJe8X.htm](menace-under-otari-bestiary/vlMuFskctUvjJe8X.htm)|Spear Launcher (BB)|auto-trad|
|[WPsgrCUSFCqgDvJi.htm](menace-under-otari-bestiary/WPsgrCUSFCqgDvJi.htm)|Green Dragon Wyrmling (BB)|auto-trad|
|[wqPYzMNgYvrO6oEP.htm](menace-under-otari-bestiary/wqPYzMNgYvrO6oEP.htm)|Leopard (BB)|auto-trad|
|[X03vq2RWi2jiA6Ri.htm](menace-under-otari-bestiary/X03vq2RWi2jiA6Ri.htm)|Owlbear (BB)|auto-trad|
|[xKYIN88ULPFgSZmw.htm](menace-under-otari-bestiary/xKYIN88ULPFgSZmw.htm)|Drow Sneak (BB)|auto-trad|
|[XrmHgbKgcHDi4OnK.htm](menace-under-otari-bestiary/XrmHgbKgcHDi4OnK.htm)|Shadow (BB)|auto-trad|
|[YdBCG0vzOA5BgoIi.htm](menace-under-otari-bestiary/YdBCG0vzOA5BgoIi.htm)|Xulgath Boss (BB)|auto-trad|
|[Z9ggO7spfHwr8up1.htm](menace-under-otari-bestiary/Z9ggO7spfHwr8up1.htm)|Falling Ceiling (BB)|auto-trad|
|[ZMr28tFTA5NUcBTi.htm](menace-under-otari-bestiary/ZMr28tFTA5NUcBTi.htm)|Web Lurker (BB)|auto-trad|
|[ZPjQkKVMi3xoPcU0.htm](menace-under-otari-bestiary/ZPjQkKVMi3xoPcU0.htm)|Wight (BB)|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
