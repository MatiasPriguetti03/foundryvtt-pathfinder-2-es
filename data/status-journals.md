# Estado de la traducción (journals)

 * **auto-trad**: 4


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[EEZvDB1Z7ezwaxIr.htm](journals/EEZvDB1Z7ezwaxIr.htm)|Domains|auto-trad|
|[S55aqwWIzpQRFhcq.htm](journals/S55aqwWIzpQRFhcq.htm)|GM Screen|auto-trad|
|[vx5FGEG34AxI2dow.htm](journals/vx5FGEG34AxI2dow.htm)|Archetypes|auto-trad|
|[xtrW5GEtPPuXR6k2.htm](journals/xtrW5GEtPPuXR6k2.htm)|Deep Backgrounds|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
