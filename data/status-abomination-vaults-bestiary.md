# Estado de la traducción (abomination-vaults-bestiary)

 * **modificada**: 52
 * **auto-trad**: 60


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[277uvPqG9RLMQUcO.htm](abomination-vaults-bestiary/277uvPqG9RLMQUcO.htm)|Warped Brew Morlock|auto-trad|
|[2TOXR7nKS6xPng2Y.htm](abomination-vaults-bestiary/2TOXR7nKS6xPng2Y.htm)|Dread Wisp|auto-trad|
|[3d3NAcPfvn07mcGN.htm](abomination-vaults-bestiary/3d3NAcPfvn07mcGN.htm)|Afflicted Irnakurse|auto-trad|
|[3F3fPq5hFbej40T2.htm](abomination-vaults-bestiary/3F3fPq5hFbej40T2.htm)|Gibtas Spawn Swarm|auto-trad|
|[4bznEiwsJvInwZwA.htm](abomination-vaults-bestiary/4bznEiwsJvInwZwA.htm)|Torture Chamber Barbazu|auto-trad|
|[55SmPtsUXsridUjJ.htm](abomination-vaults-bestiary/55SmPtsUXsridUjJ.htm)|Dragon's Blood Puffball|auto-trad|
|[6NijRSpkIuQpSxUp.htm](abomination-vaults-bestiary/6NijRSpkIuQpSxUp.htm)|Spike Launcher|auto-trad|
|[78Vf1Lk9ph2RGDgL.htm](abomination-vaults-bestiary/78Vf1Lk9ph2RGDgL.htm)|Deepwater Dhuthorex|auto-trad|
|[8VXEF3cnjzcokCTL.htm](abomination-vaults-bestiary/8VXEF3cnjzcokCTL.htm)|Stonescale Spirits|auto-trad|
|[9BEHjBWRs0st7IVv.htm](abomination-vaults-bestiary/9BEHjBWRs0st7IVv.htm)|Images of Failure|auto-trad|
|[A4MusxxoLxwMVZua.htm](abomination-vaults-bestiary/A4MusxxoLxwMVZua.htm)|Drill Field Barbazu|auto-trad|
|[AdFuuUS7bIVqp5K7.htm](abomination-vaults-bestiary/AdFuuUS7bIVqp5K7.htm)|Rusty Grate Pit|auto-trad|
|[bfuIEdKBj9bhuOft.htm](abomination-vaults-bestiary/bfuIEdKBj9bhuOft.htm)|Groetan Candle|auto-trad|
|[BJYrYqkV7PkXgSfk.htm](abomination-vaults-bestiary/BJYrYqkV7PkXgSfk.htm)|Gibtas Bounder|auto-trad|
|[BOaM3pAuWl06Q6IZ.htm](abomination-vaults-bestiary/BOaM3pAuWl06Q6IZ.htm)|Poisoning Room Specter|auto-trad|
|[ceLvlSQsYNORH8oM.htm](abomination-vaults-bestiary/ceLvlSQsYNORH8oM.htm)|Voidglutton|auto-trad|
|[chOtDyemBuw2yNN2.htm](abomination-vaults-bestiary/chOtDyemBuw2yNN2.htm)|Watching Wall|auto-trad|
|[ChRgdkplhO1D81Lg.htm](abomination-vaults-bestiary/ChRgdkplhO1D81Lg.htm)|Bright Walker|auto-trad|
|[czQFet5Qo63IXtHl.htm](abomination-vaults-bestiary/czQFet5Qo63IXtHl.htm)|Reaper Skull Puffball|auto-trad|
|[DawBQWRyrM4cKfGI.htm](abomination-vaults-bestiary/DawBQWRyrM4cKfGI.htm)|Drow Warden|auto-trad|
|[DnAeqlJRZc5N7hve.htm](abomination-vaults-bestiary/DnAeqlJRZc5N7hve.htm)|Dreshkan|auto-trad|
|[dWOK0nzGWyc5NkNz.htm](abomination-vaults-bestiary/dWOK0nzGWyc5NkNz.htm)|Lady's Whisper|auto-trad|
|[E0FMRiGNCv5n7AVH.htm](abomination-vaults-bestiary/E0FMRiGNCv5n7AVH.htm)|Paralyzing Light Trap|auto-trad|
|[fvijt2whssxJzxCF.htm](abomination-vaults-bestiary/fvijt2whssxJzxCF.htm)|Images of Powerlessness|auto-trad|
|[gUihlg28MEloIqE3.htm](abomination-vaults-bestiary/gUihlg28MEloIqE3.htm)|Dune Candle|auto-trad|
|[HBRz8BVLVN9u9Odp.htm](abomination-vaults-bestiary/HBRz8BVLVN9u9Odp.htm)|Corpselight|auto-trad|
|[hnYckrT72oIKAuHJ.htm](abomination-vaults-bestiary/hnYckrT72oIKAuHJ.htm)|Vengeful Furnace|auto-trad|
|[k4fVLtVrgIEg9xij.htm](abomination-vaults-bestiary/k4fVLtVrgIEg9xij.htm)|Bloodsiphon|auto-trad|
|[knoWZfTiLY7xGwhB.htm](abomination-vaults-bestiary/knoWZfTiLY7xGwhB.htm)|Painful Suggestion Trap|auto-trad|
|[KSFGGxU3qxYJzWpe.htm](abomination-vaults-bestiary/KSFGGxU3qxYJzWpe.htm)|Morlock Scavenger|auto-trad|
|[kzHRJGLvjOJk5WU7.htm](abomination-vaults-bestiary/kzHRJGLvjOJk5WU7.htm)|Drow Cavern Seer|auto-trad|
|[lH2rdwiyOPStkQvZ.htm](abomination-vaults-bestiary/lH2rdwiyOPStkQvZ.htm)|Morlock Cultist|auto-trad|
|[mrxDc0fj5t5CvjJQ.htm](abomination-vaults-bestiary/mrxDc0fj5t5CvjJQ.htm)|Shanrigol Behemoth|auto-trad|
|[neNlIrZOs6zOODoe.htm](abomination-vaults-bestiary/neNlIrZOs6zOODoe.htm)|Blast Tumbler|auto-trad|
|[njfwxMPXTPA5AegD.htm](abomination-vaults-bestiary/njfwxMPXTPA5AegD.htm)|Summoning Chamber Erinys|auto-trad|
|[Nkz8Z5TrEqRePGlk.htm](abomination-vaults-bestiary/Nkz8Z5TrEqRePGlk.htm)|Scalathrax|auto-trad|
|[NXdpFypPPmRwYBT1.htm](abomination-vaults-bestiary/NXdpFypPPmRwYBT1.htm)|Spellvoid|auto-trad|
|[OloMMRPtTQKF0x16.htm](abomination-vaults-bestiary/OloMMRPtTQKF0x16.htm)|Caligni Defender|auto-trad|
|[OqoWbYOyHDGHEeHV.htm](abomination-vaults-bestiary/OqoWbYOyHDGHEeHV.htm)|Drow Hunter|auto-trad|
|[OTT7CCkonkfwhJ8Y.htm](abomination-vaults-bestiary/OTT7CCkonkfwhJ8Y.htm)|Urdefhan Blood Mage|auto-trad|
|[PphyArSCoxkaI6IS.htm](abomination-vaults-bestiary/PphyArSCoxkaI6IS.htm)|Befuddling Gas Trap|auto-trad|
|[qjuLgWr2VhPcuylI.htm](abomination-vaults-bestiary/qjuLgWr2VhPcuylI.htm)|Doom of Tomorrow|auto-trad|
|[qXT1SQDtGqMkVl7Q.htm](abomination-vaults-bestiary/qXT1SQDtGqMkVl7Q.htm)|Shanrigol Heap|auto-trad|
|[rArZ2y5xqSWeUU0G.htm](abomination-vaults-bestiary/rArZ2y5xqSWeUU0G.htm)|Drow Shootist|auto-trad|
|[ro9oVsu1cuCP8OQH.htm](abomination-vaults-bestiary/ro9oVsu1cuCP8OQH.htm)|Deadtide Skeleton Guard|auto-trad|
|[s8Ofcsub5QJJmgA5.htm](abomination-vaults-bestiary/s8Ofcsub5QJJmgA5.htm)|Daemonic Fog|auto-trad|
|[Sbxt8YT0AOFfVCPX.htm](abomination-vaults-bestiary/Sbxt8YT0AOFfVCPX.htm)|Urdefhan Lasher|auto-trad|
|[T6vOuhM1KV5Fr75F.htm](abomination-vaults-bestiary/T6vOuhM1KV5Fr75F.htm)|Gibtanius|auto-trad|
|[To0MLA0arpkiE6Cz.htm](abomination-vaults-bestiary/To0MLA0arpkiE6Cz.htm)|Deep End Sarglagon|auto-trad|
|[tZCFmJonHkhGg1Vs.htm](abomination-vaults-bestiary/tZCFmJonHkhGg1Vs.htm)|Urdefhan Death Scout|auto-trad|
|[V73Oqm1EL1KOoXOl.htm](abomination-vaults-bestiary/V73Oqm1EL1KOoXOl.htm)|Morlock Engineer|auto-trad|
|[v9B0hB5sm4YZxebY.htm](abomination-vaults-bestiary/v9B0hB5sm4YZxebY.htm)|Seugathi Servant|auto-trad|
|[vS1YISLmSnkNotkL.htm](abomination-vaults-bestiary/vS1YISLmSnkNotkL.htm)|Seugathi Reality Warper|auto-trad|
|[VsRKgjKolLsJMd0I.htm](abomination-vaults-bestiary/VsRKgjKolLsJMd0I.htm)|Shuffling Scythe Blades|auto-trad|
|[vX4SBXIICKfrM4pF.htm](abomination-vaults-bestiary/vX4SBXIICKfrM4pF.htm)|Hellforge Barbazu|auto-trad|
|[x3eOZvKUginj5Blh.htm](abomination-vaults-bestiary/x3eOZvKUginj5Blh.htm)|Flickerwisp|auto-trad|
|[YHYdZemNHp0fnWca.htm](abomination-vaults-bestiary/YHYdZemNHp0fnWca.htm)|Dread Dhuthorex|auto-trad|
|[YXF12ssz8tEh2YUe.htm](abomination-vaults-bestiary/YXF12ssz8tEh2YUe.htm)|Will-o'-the-Deep|auto-trad|
|[zkiaelYxDB1ttlCI.htm](abomination-vaults-bestiary/zkiaelYxDB1ttlCI.htm)|Dhuthorex Sage|auto-trad|
|[zsgW8hHm3JfZUgwF.htm](abomination-vaults-bestiary/zsgW8hHm3JfZUgwF.htm)|Mulventok|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[00s3MhFQ4yOp2rTf.htm](abomination-vaults-bestiary/00s3MhFQ4yOp2rTf.htm)|Elder Child Of Belcorra|Elder Child Of Belcorra|modificada|
|[0jvmec4yJH1ASfRy.htm](abomination-vaults-bestiary/0jvmec4yJH1ASfRy.htm)|Urthagul|Urthagul|modificada|
|[2K0oJcLauMwyRNZQ.htm](abomination-vaults-bestiary/2K0oJcLauMwyRNZQ.htm)|Murschen|Murschen|modificada|
|[3H1rBpUQwTcNd6xZ.htm](abomination-vaults-bestiary/3H1rBpUQwTcNd6xZ.htm)|Chandriu Invisar|Chandriu Invisar|modificada|
|[3Ll0LCZcmQmb7aV3.htm](abomination-vaults-bestiary/3Ll0LCZcmQmb7aV3.htm)|Galudu|Galudu|modificada|
|[3ry9WSvMMXHUe3kE.htm](abomination-vaults-bestiary/3ry9WSvMMXHUe3kE.htm)|Beluthus|Beluthus|modificada|
|[3vn9W5SThovdsEnY.htm](abomination-vaults-bestiary/3vn9W5SThovdsEnY.htm)|Sacuishu|Sacuishu|modificada|
|[5iuvJLceeLJPlR8O.htm](abomination-vaults-bestiary/5iuvJLceeLJPlR8O.htm)|Viscous Black Pudding|Morcilla viscosa|modificada|
|[8AVy2qIK6vh13sSj.htm](abomination-vaults-bestiary/8AVy2qIK6vh13sSj.htm)|Ysondkhelir|Ysondkhelir|modificada|
|[8Iozx4jbIGHxK4yf.htm](abomination-vaults-bestiary/8Iozx4jbIGHxK4yf.htm)|Barcumbuk|Barcumbuk|modificada|
|[9Tr4aUNr2wxxMDcg.htm](abomination-vaults-bestiary/9Tr4aUNr2wxxMDcg.htm)|Cratonys|Cratonys|modificada|
|[aIDLQY5mBPyxTjZ2.htm](abomination-vaults-bestiary/aIDLQY5mBPyxTjZ2.htm)|Witchfire Warden|Guardián Fuego de bruja|modificada|
|[bif3iQcDPi27rx6x.htm](abomination-vaults-bestiary/bif3iQcDPi27rx6x.htm)|Salaisa Malthulas|Salaisa Malthulas|modificada|
|[cMpgGvq1fGxh8wI0.htm](abomination-vaults-bestiary/cMpgGvq1fGxh8wI0.htm)|Seugathi Researcher|Investigador Seugathi|modificada|
|[DDJGNAh3rfyIupAb.htm](abomination-vaults-bestiary/DDJGNAh3rfyIupAb.htm)|Belcorra Haruvex|Belcorra Haruvex|modificada|
|[EN3mp0sVObP8ou3p.htm](abomination-vaults-bestiary/EN3mp0sVObP8ou3p.htm)|Jaul Mezmin|Jaul Mezmin|modificada|
|[eoQgRltsgJWa4aeC.htm](abomination-vaults-bestiary/eoQgRltsgJWa4aeC.htm)|Nhimbaloth's Cutter|Cortador de Nhimbaloth|modificada|
|[h0Ztbh36be4rpJCz.htm](abomination-vaults-bestiary/h0Ztbh36be4rpJCz.htm)|Dulac|Dulac|modificada|
|[hia81Ut7fEREbhkq.htm](abomination-vaults-bestiary/hia81Ut7fEREbhkq.htm)|Jarelle Kaldrian|Jarelle Kaldrian|modificada|
|[HnIyJuqKNOvK7eOJ.htm](abomination-vaults-bestiary/HnIyJuqKNOvK7eOJ.htm)|Nox|Nox|modificada|
|[jE8BEe6pcnGraw2p.htm](abomination-vaults-bestiary/jE8BEe6pcnGraw2p.htm)|Jafaki|Jafaki|modificada|
|[JrowrtDilEG8dN2s.htm](abomination-vaults-bestiary/JrowrtDilEG8dN2s.htm)|Quara Orshendiel|Quara Orshendiel|modificada|
|[kzX588Hjb3w4QPOj.htm](abomination-vaults-bestiary/kzX588Hjb3w4QPOj.htm)|Mister Beak|Mister Beak|modificada|
|[lMCEVxKkQ7XK6Nid.htm](abomination-vaults-bestiary/lMCEVxKkQ7XK6Nid.htm)|Canker Cultist|Cultista Canker|modificada|
|[Mk6Uo4Vt1HkG9EvL.htm](abomination-vaults-bestiary/Mk6Uo4Vt1HkG9EvL.htm)|Urevian|Urevian|modificada|
|[mlifDVJJWwjFtUxv.htm](abomination-vaults-bestiary/mlifDVJJWwjFtUxv.htm)|Murmur|Murmullo|modificada|
|[Np787X9Z4aQxOdCg.htm](abomination-vaults-bestiary/Np787X9Z4aQxOdCg.htm)|Khurfel|Khurfel|modificada|
|[OErk9kO3PhYwMXoJ.htm](abomination-vaults-bestiary/OErk9kO3PhYwMXoJ.htm)|Siora Fallowglade|Siora Clarobarbecho|modificada|
|[oXnpdJVN6NIE58W3.htm](abomination-vaults-bestiary/oXnpdJVN6NIE58W3.htm)|Caliddo Haruvex|Caliddo Haruvex|modificada|
|[qOkxxiM4tNf96CHQ.htm](abomination-vaults-bestiary/qOkxxiM4tNf96CHQ.htm)|Seugathi Guard|Guardia Seugathi|modificada|
|[qoyopMMlRdgPAbXZ.htm](abomination-vaults-bestiary/qoyopMMlRdgPAbXZ.htm)|Vischari|Vischari|modificada|
|[qw2pk1zjvgxbeGBO.htm](abomination-vaults-bestiary/qw2pk1zjvgxbeGBO.htm)|Padli|Padli|modificada|
|[R0EEgMDKcynpAWoa.htm](abomination-vaults-bestiary/R0EEgMDKcynpAWoa.htm)|Otari Ilvashti|Otari Ilvashti|modificada|
|[rketcmqDQJbFFYfq.htm](abomination-vaults-bestiary/rketcmqDQJbFFYfq.htm)|Bone Gladiator|Gladiador de hueso|modificada|
|[saEUzIgUtV2AzKhl.htm](abomination-vaults-bestiary/saEUzIgUtV2AzKhl.htm)|Augrael|Augrael|modificada|
|[TiAzR8SnYwhACWbj.htm](abomination-vaults-bestiary/TiAzR8SnYwhACWbj.htm)|Observation Deck Seugathi Researcher|Cubierta de Observación Seugathi Investigador|modificada|
|[tOL4rWj2oYWZ4ow2.htm](abomination-vaults-bestiary/tOL4rWj2oYWZ4ow2.htm)|Aller Rosk|Aller Rosk|modificada|
|[tXfiVIThQlBT6B1H.htm](abomination-vaults-bestiary/tXfiVIThQlBT6B1H.htm)|Voidbracken Chuul|Helecho del vacío Chuul|modificada|
|[tYzzLLUv9WBhHhQY.htm](abomination-vaults-bestiary/tYzzLLUv9WBhHhQY.htm)|Carman Rajani|Carman Rajani|modificada|
|[uWn6DCzthgUt97d7.htm](abomination-vaults-bestiary/uWn6DCzthgUt97d7.htm)|Gulzash|Gulzash|modificada|
|[uyGxJOnJ1gYwOpy5.htm](abomination-vaults-bestiary/uyGxJOnJ1gYwOpy5.htm)|Azvalvigander|Azvalvigander|modificada|
|[VeyuTE1GUJ71oMMn.htm](abomination-vaults-bestiary/VeyuTE1GUJ71oMMn.htm)|Kragala|Kragala|modificada|
|[vHei0y2PKlXfxQ8Z.htm](abomination-vaults-bestiary/vHei0y2PKlXfxQ8Z.htm)|Child of Belcorra|Niño de Belcorra|modificada|
|[w2N0foudBFcRCaHK.htm](abomination-vaults-bestiary/w2N0foudBFcRCaHK.htm)|Nhakazarin|Nhakazarin|modificada|
|[WR07Z6MjvebSHzI7.htm](abomination-vaults-bestiary/WR07Z6MjvebSHzI7.htm)|Ryta|Ryta|modificada|
|[x0NDgH3EMLTLh02r.htm](abomination-vaults-bestiary/x0NDgH3EMLTLh02r.htm)|Chafkhem|Chafkhem|modificada|
|[xAfkUwJYq5JLmSrW.htm](abomination-vaults-bestiary/xAfkUwJYq5JLmSrW.htm)|Boss Skrawng|Boss Skrawng|modificada|
|[xj1Qn0VA4H4aKSjW.htm](abomination-vaults-bestiary/xj1Qn0VA4H4aKSjW.htm)|Jaul's Wolf|Lobo de Jaul|modificada|
|[XLTPZ592Z9MXXBlA.htm](abomination-vaults-bestiary/XLTPZ592Z9MXXBlA.htm)|Blood of Belcorra|Sangre de Belcorra|modificada|
|[ZAXuvUW6kl6v3SuW.htm](abomination-vaults-bestiary/ZAXuvUW6kl6v3SuW.htm)|Volluk Azrinae|Volluk Azrinae|modificada|
|[ZDYMKYZVyR8Fqakp.htm](abomination-vaults-bestiary/ZDYMKYZVyR8Fqakp.htm)|Wrin Sivinxi|Wrin Sivinxi|modificada|
|[ZFP8RyQW4SNtJ3AE.htm](abomination-vaults-bestiary/ZFP8RyQW4SNtJ3AE.htm)|Nyzuros|Nyzuros|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
