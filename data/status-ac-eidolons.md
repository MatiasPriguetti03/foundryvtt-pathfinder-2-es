# Estado de la traducción (ac-eidolons)

 * **auto-trad**: 11


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[1YJoaMhZPvLGvj1J.htm](ac-eidolons/1YJoaMhZPvLGvj1J.htm)|Devotion Phantom Eidolon|auto-trad|
|[5IvhL9PsniwiVhEZ.htm](ac-eidolons/5IvhL9PsniwiVhEZ.htm)|Beast Eidolon|auto-trad|
|[7a1ei105k24XkJG0.htm](ac-eidolons/7a1ei105k24XkJG0.htm)|Plant Eidolon|auto-trad|
|[dnQUp0Vhg3XdZFtP.htm](ac-eidolons/dnQUp0Vhg3XdZFtP.htm)|Construct Eidolon|auto-trad|
|[fvGz8fdP3iW8IBda.htm](ac-eidolons/fvGz8fdP3iW8IBda.htm)|Psychopomp Eidolon|auto-trad|
|[mKWiBb4xmnwVhmh7.htm](ac-eidolons/mKWiBb4xmnwVhmh7.htm)|Angel Eidolon|auto-trad|
|[o3wSjAaxYUmdbDvB.htm](ac-eidolons/o3wSjAaxYUmdbDvB.htm)|Anger Phantom Eidolon|auto-trad|
|[P5GSucPtackfg6Qt.htm](ac-eidolons/P5GSucPtackfg6Qt.htm)|Demon Eidolon|auto-trad|
|[UjTUvPAdzj3Ybg8s.htm](ac-eidolons/UjTUvPAdzj3Ybg8s.htm)|Undead Eidolon|auto-trad|
|[XDxYBQ6TRCJzoKbf.htm](ac-eidolons/XDxYBQ6TRCJzoKbf.htm)|Dragon Eidolon|auto-trad|
|[zjIVbS6nGX6BS2UD.htm](ac-eidolons/zjIVbS6nGX6BS2UD.htm)|Fey Eidolon|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
