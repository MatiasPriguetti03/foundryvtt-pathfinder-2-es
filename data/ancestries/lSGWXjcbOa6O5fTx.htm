Name: Orc
Nombre: Orco
Estado: oficial

------ Description (en) ------
<p><em>Orcs are forged in the fires of violence and conflict, often from the moment they are born. As they live lives that are frequently cut brutally short, orcs revel in testing their strength against worthy foes, whether by challenging a higher-ranking member of their community for dominance or raiding a neighboring settlement. Many orcs seek glory as soon as they can walk and carry a blade or club, taming wild beasts or hunting deadly monsters.</em></p>
<hr />
<p>Orcs often struggle to gain acceptance among other communities, who frequently see them as brutes. Those who earn the loyalty of an orc friend, however, soon learn that an orc's fidelity and honesty are unparalleled. Orc barbarians, fighters, and rangers are prized as gladiators and mercenaries. While some human settlements might be hesitant to accept an entire orc community into their midst, a small handful of orc mercenaries can do the job of an entire squad of human conscripts, so long as the orcs are well fed and well paid. Though the belief that orcs are only suited for battle is pervasive among other humanoids, the harsh orc mentality comes from a long history of conflict rather than a lack of ability in other areas.</p>
<p>Orc culture teaches that they are shaped by the challenges they survive, and the most worthy survive the most hardships. Orcs who attain both a long life and great triumphs command immense respect.</p>
<p>If you want a character who is hardy, fearsome, and excels at feats of physical prowess, you should play an orc.</p>
<h2>You Might...</h2>
<ul>
<li>Eagerly meet any chance to prove your strength in a physical contest.</li>
<li>Believe that lies and treachery are for those who lack the strength to seize what they want.</li>
<li>View dying in glorious combat as preferable to a mundane death from old age or illness.</li>
</ul>
<h2>Others Probably...</h2>
<ul>
<li>See you as violent or lacking in discipline.</li>
<li>Underestimate your intellect, cunning, and knowledge.</li>
<li>Admire your forthrightness and blunt honesty.</li>
</ul>
<h2>Physical Description</h2>
<p>Orcs are tall and powerfully built, with long arms and stocky legs. Many orcs top 7 feet in height, though they tend to adopt broad, almost bow-legged stances and slouch forward at the shoulders. The combination makes for a seeming contradiction, sharing an eye level with most humanoids while simultaneously towering over them. Orcs have rough skin, thick bones, and rock-hard muscles, making them suited to war and other physically demanding tasks. Despite the roughness of their skin, orcs scar easily, and most orcs take great pride in the scars they have accumulated. Orc skin color is typically green and occasionally gray, though some orcs have other skin colors that reflect adaptations to their environments.</p>
<p>Orcs consider powerful builds and heavily scarred skin attractive, regardless of gender. A powerful orc makes the hold stronger, and scars are signs of victories won or hardships survived. Similarly, many orcs consider large, jutting tusks to be more attractive than smaller tusks, since the former make more effective weapons. Many orcs also find tattoos to be attractive, particularly large or painful ones that cover a significant amount of skin.</p>
<h2>Society</h2>
<p>Most orc communities define themselves through two things: pain and glory. Each earns respect in near equal measure, so long as the pain is borne with stoicism. An orc with many scars who walks uncomplaining with a broken leg draws as much admiration as one who wins a great victory on the battlefield.</p>
<p>Power in an orc hold comes from strength or family lineage. The structure tends to be feudal, with weaker orcs working at the behest of the strong. The Hold of Belkzen is the largest such society, and power changes hands there quickly. One mighty orc dying in battle can shake up an entire power structure, leading to squabbling and duels to decide control. Many orcs who tire of being subservient split off to form their own warbands, traveling to new territory.</p>
<p>Young orcs are typically raised by the entire community. Indeed, it would be almost impossible for orcs to raise their young any other way, since twins, triplets, and even quadruplets are quite common in orc families, as are deaths among orcs in their child-rearing years. Many orc holds conduct ceremonies when a young orc comes of age, typically around their tenth or eleventh birthday, during which the new adults are told what their role in the hold will be. For communities that practice ritual scarification or tattooing, this is often when the young orcs receive their first hold-scar or tattoo as well.</p>
<p>Orcs fear very little, but most distrust magic. Magic is seen as a tool that bypasses the physical and allows the weak to contend with the strong, a belief that runs at odds with orc values. While they respect the physical might of Gorumite warpriests, and even druids who take on the forms of great beasts, they find arcane and occult magic questionable at best and unethical on the whole. All but the most depraved orc communities see necromancy as a foul art that steals glory from the dead, and their growing struggles against the undead have given them newfound common ground with their humanoid neighbors.</p>
<h2>Alignment and Religion</h2>
<p>A common orc saying is, 'You are the scars that shape you.' Violent, chaotic lives in violent, chaotic lands mean that most orcs tend heavily toward chaotic alignments. Gorum, Lamashtu, and Rovagug are all commonly worshiped among orc communities, though less violent holds worship nature deities like Gozreh or gods like Sarenrae, whose tenets of fire, redemption, and glory all hold some appeal to orc sensibilities.</p>
<p>While there are orc deities, their worship is surprisingly uncommon among orcs. Orcs believe that if a creature has a face and a name, it can be killed, and so their own deities are targets, rather than objects of reverence. Some orc holds teach that the greatest members of the hold can earn a chance to challenge the orc deities for a place amid the pantheon. Most orcs don't waste their dying moments praising the divine or praying for a place in the afterlife, but spitting a blood-flecked warning at their deities, promising a new challenger through broken teeth.</p>
<h2>Adventurers</h2>
<p>An orc's drive to overcome challenges and prove themself spurs many orcs to become adventurers, though orcs are more likely to set out on their own or with other orcs than alongside adventurers of other ancestries.</p>
<p>Common orc backgrounds include gladiator, hunter, martial disciple, nomad, and warrior, plus bandit, outrider, and refugee. Orcs thrive in martial classes like barbarian and fighter.</p>
<h2>Names</h2>
<p>Orcs have a harsh, guttural language, and their naming conventions are no exception. Many orc names are simply the Orcish word for a particularly desirable trait, such as great strength, height, or ferocity. Orcs commonly use either their hold name or a name referencing a particularly memorable accomplishment as their surname.</p>
<h3><span style="text-decoration: underline;">Sample Names</span></h3>
<p>Arkus, Ausk, Durra, Grask, Grillgiss, Krugga, Mahja, Murdut, Ollak, Onyat, Thurk, Uirch, Unach</p>
<h2 style="border-bottom: 1px solid var(--color-underline-header);">Orc Mechanics</h2>
<p><strong>Hit Points</strong> 10</p>
<p><strong>Size</strong> Medium</p>
<p><strong>Speed</strong> 25 feet</p>
<p><strong>Ability Boosts</strong> Strength, Free</p>
<p><strong>Languages</strong> Common, Orcish</p>
<p><strong>Additional Languages</strong> equal to your Intelligence modifier (if positive). Choose from Goblin, Jotun, Terran, Undercommon, and any other languages to which you have access (such as the languages prevalent in your region).</p>
<p><strong>Senses</strong> Darkvision</p>
------ Description (es) ------
<p><em>Los orcos se forjan en los fuegos de la violencia y el conflicto, normalmente desde el momento en el que nacen. Como sus vidas suelen ser brutalmente cortas, se deleitan poniendo a prueba su fuerza contra enemigos dignos, ya sea desafiando a un miembro de mayor rango de su comunidad por el dominio o asaltando un asentamiento vecino. Muchos van en busca de la gloria en cuanto pueden caminar y empuñar una espada o un garrote, domando bestias salvajes o cazando monstruos letales.</em></p>
<hr />
<p>Los orcos a menudo luchan por ganarse la aceptación entre otras comunidades, que suelen verlos como brutos. Sin embargo, aquellos que se ganan la lealtad de un amigo orco, pronto descubren que la fidelidad y la honestidad de un orco no tienen parangón. Los bárbaros, guerreros y exploradores orcos son apreciados como gladiadores y mercenarios. Aunque algunos asentamientos humanos pueden tener dudas sobre si aceptar a toda una comunidad de orcos en su seno, unos pocos mercenarios orcos pueden hacer el trabajo de todo un escuadrón de reclutas humanos, siempre que estén bien alimentados y se les pague bien. Aunque la creencia de que sólo son aptos para la batalla está muy extendida entre otros humanoides, la dura mentalidad orca proviene de una larga historia de conflictos más que de una falta de habilidad en otras áreas.</p>
<p>La cultura orca enseña que los desafíos a los que sobreviven los moldean, y los más dignos de ellos sobreviven a las mayores dificultades. Los orcos que consiguen tanto una vida larga como grandes triunfos inspiran un inmenso respeto.</p>
<p>Si quieres un personaje resistente, temible y que destaque por sus proezas físicas, deberías elegir un orco.</p>
<h2>Puedes...</h2>
<ul>
<li>Estar dispuesto a aprovechar cualquier oportunidad para demostrar tu fuerza en una competición física.</li>
<li>Creer que las mentiras y la traición son para quienes carecen de la fuerza para conseguir lo que desean.</li>
<li>Considerar que morir en un combate glorioso es preferible a una muerte mundana por vejez o enfermedad.</li>
</ul>
<h2>Otros probablemente...</h2>
<ul>
<li>Te consideren violento o falto de disciplina.</li>
<li>Subestimen tu intelecto, tu astucia y tus conocimientos.</li>
<li>Admiren tu franqueza y tu honestidad.</li>
</ul>
<h2>Descripción física</h2>
<p>Los orcos son altos y de complexión fuerte, con los brazos largos y las piernas robustas. Muchos superan los 7 pies (2,10 m) de altura, aunque tienden a adoptar posturas encogidas, casi con las piernas arqueadas, y a encorvar los hombros hacia delante. Esta combinación supone una aparente contradicción, ya que comparten el nivel de los ojos con la mayoría de los humanoides y, al mismo tiempo, son mucho más altos que ellos. Los orcos tienen la piel áspera, los huesos anchos y unos músculos duros como rocas, lo que los hace aptos para la guerra y demás tareas físicamente exigentes. A pesar de la aspereza de su piel, cicatrizan con facilidad, y la mayoría se enorgullecen de las cicatrices que han acumulado. El color de la piel suele ser verde y, ocasionalmente, gris, aunque algunos tienen otros colores de piel que reflejan la adaptación a sus entornos.</p>
<p>Los orcos consideran atractivas las complexiones fornidas y las pieles con muchas cicatrices, independientemente del sexo.</p>
<p>Un orco poderoso fortalece al clan y las cicatrices son signos de combates ganados o de penurias sobrevividas. Del mismo modo, muchos consideran que los colmillos grandes y salientes son más atractivos que los pequeños, ya que los primeros son más eficaces como armas. Muchos también encuentran atractivos los tatuajes, especialmente si son grandes o dolorosos y cubren una gran cantidad de piel.</p>
<h2>Sociedad</h2>
<p>La mayoría de las comunidades de orcos se definen mediante dos aspectos: el dolor y la gloria. En todas ellas, el respeto se gana casi del mismo modo, aguantando el dolor con estoicismo. Un orco con muchas cicatrices que camine sin quejarse con una pierna rota causa tanta admiración como uno que logre una gran victoria en el campo de batalla.</p>
<p>El poder de un clan orco proviene de la fuerza o del linaje familiar. La estructura tiende a ser feudal, en la que los más débiles trabajan bajo las órdenes de los fuertes. El Bastión de Belkzen es la mayor sociedad de este tipo y el poder cambia de manos rápidamente. La muerte de un orco poderoso en una batalla puede hacer tambalear toda la estructura de poder, lo que lleva a disputas y duelos para decidir quién asume el control. Muchos se cansan de ser serviles y se separan para formar sus propias partidas de guerra, viajando así a nuevos territorios.</p>
<p>Los jóvenes suelen ser criados por toda la comunidad. De hecho, sería casi imposible para los orcos criar a sus crías de otra manera, ya que los gemelos, trillizos e incluso cuatrillizos son bastante comunes en sus familias, al igual que las muertes de en sus primeros años. Muchos clanes llevan a cabo ceremonias cuando un joven alcanza la mayoría de edad, normalmente alrededor de su décimo o undécimo cumpleaños, durante las cuales se explica a los nuevos adultos cuál será su papel en el clan. En las comunidades que practican la escarificación o los tatuajes rituales, suele ser el momento en el que los jóvenes reciben su primera cicatriz o tatuaje.</p>
<p>Los orcos no temen a casi nada, pero la mayoría de ellos desconfía de la magia. La magia se percibe como una herramienta que evita lo físico y permite a los débiles enfrentarse a los fuertes, una creencia que va en contra de los valores de los orcos. Aunque respetan el poder físico de los sacerdotes de guerra gorumitas, e incluso de los druidas que adoptan la forma de grandes bestias, consideran que la magia arcana y ocultista es cuestionable en el mejor de los casos y poco ética en general. Todas las comunidades, a excepción de las más depravadas, ven la nigromancia como un arte sucio que roba la gloria a los muertos, y gracias a sus crecientes luchas contra los muertos vivientes han encontrado un nuevo punto en común con sus vecinos humanoides.</p>
<h2>Alineamiento y Religión</h2>
<p>Hay un dicho común entre los orcos que dice: «Eres las cicatrices que te dan forma». Las vidas violentas y caóticas en tierras violentas y caóticas suponen que la mayoría tiendan fuertemente hacia los alineamientos caóticos. Gorum, Lamashtu y Rovagug son muy adorados entre las comunidades, aunque los menos violentos adoran a dioses de la naturaleza como Gozreh o como Sarenrae, cuyos principios de fuego, redención y gloria tienen cierto atractivo para la sensibilidad orca.</p>
<p>Aunque existen dioses orcos, su culto es sorprendentemente poco frecuente entre los propios orcos. Creen que, si una criatura tiene un rostro y un nombre, puede morir, por lo que sus propios dioses son objetivos, más que objetos de reverencia. Algunos clanes enseñan que los mejores miembros del clan pueden ganarse la oportunidad de desafiar a los dioses orcos por un lugar en el panteón. La mayoría no desperdician el momento de su muerte alabando a una divinidad o rezando por un lugar en la otra vida, sino escupiéndoles una advertencia ensangrentada a sus dioses y prometiéndoles un nuevo rival con los dientes rotos.</p>
<h2>Aventureros Orcos</h2>
<p>El afán de los orcos por superar retos y ponerse a prueba lleva a muchos orcos a convertirse en aventureros, aunque es más probable que partan en soledad o con otros orcos que con aventureros de otras ascendencias.</p>
<p>Los bagajes orcos más comunes incluyen cazador, discípulo marcial, gladiador, combatiente y nómada de las Reglas básicas, además de bandido, jinete y refugiado de este libro. Los orcos prosperan con clases marciales como bárbaro y guerrero.</p>
<h2>Nombres</h2>
<p>Los orcos tienen un idioma estridente y gutural, y sus nombres no son una excepción. Muchos nombres son simplemente la palabra orca para denominar un rasgo particularmente deseable, como gran fuerza, altura o ferocidad. Suelen utilizar como apellido el nombre de su clan o un nombre que haga referencia a algún logro especialmente memorable.</p>
<h3><span style="text-decoration: underline;">Ejemplo de nombres</span></h3>
<p>Arkus, Ausk, Durra, Grask, Grillgiss, Krugga, Mahja, Murdut, Ollak, Onyat, Thurk, Uirch, Unach</p>
<h2 style="border-bottom: 1px solid var(--color-underline-header);">Mecánica de los Orcos</h2>
<p><strong>Puntos de golpe</strong> 10</p>
<p><strong>Tamaño</strong> Mediano</p>
<p><strong>Velocidad</strong> 25 pies (7,5 m)</p>
<p><strong>Mejoras de característica</strong> Fuerza, Gratuíta</p>
<p><strong>Idiomas</strong> Común, Orco</p>
<p>Tantos idiomas adicionales como tu modificador por Inteligencia (si es positivo). Elige entre goblin, infracomún, jotun, térraro y cualquier otro idioma al que tengas acceso (como los idiomas predominantes en tu región).</p>
<p><strong>Rasgos</strong> Orco, Humanoide</p>
<p><strong>Visión en la oscuridad</strong> Puedes ver en la oscuridad y con luz tenue tan bien como con luz brillante, aunque tu visión en la oscuridad es en blanco y negro.</p>
