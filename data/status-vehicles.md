# Estado de la traducción (vehicles)

 * **auto-trad**: 48


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0H2BbJCAKauXYKE6.htm](vehicles/0H2BbJCAKauXYKE6.htm)|Sand Barge|auto-trad|
|[1ldIHIoe1aKHilX2.htm](vehicles/1ldIHIoe1aKHilX2.htm)|Sandsailer|auto-trad|
|[2dLy1sfqVbNS2inJ.htm](vehicles/2dLy1sfqVbNS2inJ.htm)|Firefly|auto-trad|
|[66dAXEMUjL7BrX8I.htm](vehicles/66dAXEMUjL7BrX8I.htm)|Carriage|auto-trad|
|[6CFijrzNSXtS8ExJ.htm](vehicles/6CFijrzNSXtS8ExJ.htm)|Sky Chariot, Light|auto-trad|
|[7ye7abxWAFdVbATr.htm](vehicles/7ye7abxWAFdVbATr.htm)|Chariot, Heavy|auto-trad|
|[bMgQftCEiSqa1pTk.htm](vehicles/bMgQftCEiSqa1pTk.htm)|Vonthos's Golden Bridge|auto-trad|
|[bUwBenWXjdUIKXkm.htm](vehicles/bUwBenWXjdUIKXkm.htm)|Clockwork Borer|auto-trad|
|[cmLOaqR3qebmLA1P.htm](vehicles/cmLOaqR3qebmLA1P.htm)|Clockwork Bumblebee|auto-trad|
|[Cr4T0TcaxMr3bUaj.htm](vehicles/Cr4T0TcaxMr3bUaj.htm)|Speedster|auto-trad|
|[cvP9ytBY9SJbaNbo.htm](vehicles/cvP9ytBY9SJbaNbo.htm)|Apparatus of the Octopus|auto-trad|
|[FbSRhHvCmXBSVkrn.htm](vehicles/FbSRhHvCmXBSVkrn.htm)|Steam Cart|auto-trad|
|[fiSvQBWyyJuXnVvn.htm](vehicles/fiSvQBWyyJuXnVvn.htm)|Automated Cycle|auto-trad|
|[fUJ1RB9l9DppWdCH.htm](vehicles/fUJ1RB9l9DppWdCH.htm)|Titanic Stomper|auto-trad|
|[GMoxs9RQ88uMxfW4.htm](vehicles/GMoxs9RQ88uMxfW4.htm)|Bathysphere|auto-trad|
|[HA93YJ0qjmRk0Nh8.htm](vehicles/HA93YJ0qjmRk0Nh8.htm)|Galley|auto-trad|
|[IhniEmlnkb3pFRZG.htm](vehicles/IhniEmlnkb3pFRZG.htm)|Clockwork Hopper|auto-trad|
|[JQAknpXhJo5bwgOM.htm](vehicles/JQAknpXhJo5bwgOM.htm)|Strider|auto-trad|
|[jrLiGrOwgOhr83PL.htm](vehicles/jrLiGrOwgOhr83PL.htm)|Clockwork Castle|auto-trad|
|[K0ThJViHQzzxRmqE.htm](vehicles/K0ThJViHQzzxRmqE.htm)|Second Kiss|auto-trad|
|[l0RfYgabnbNoHnkS.htm](vehicles/l0RfYgabnbNoHnkS.htm)|Rowboat|auto-trad|
|[MCU4ZiNNAX7GQNJb.htm](vehicles/MCU4ZiNNAX7GQNJb.htm)|Firework Pogo|auto-trad|
|[mq8bsMnrY81gUEYb.htm](vehicles/mq8bsMnrY81gUEYb.htm)|Cart|auto-trad|
|[MrGgl8HDfQYITahL.htm](vehicles/MrGgl8HDfQYITahL.htm)|Clockwork Wagon|auto-trad|
|[oq6khbojns3YRi2g.htm](vehicles/oq6khbojns3YRi2g.htm)|Cliff Crawler|auto-trad|
|[oyKhe8J0h8G9IHE3.htm](vehicles/oyKhe8J0h8G9IHE3.htm)|Glider|auto-trad|
|[Qlvei5jjRXdsXUPo.htm](vehicles/Qlvei5jjRXdsXUPo.htm)|Cutter|auto-trad|
|[QMPVAWtaClZtD2Ip.htm](vehicles/QMPVAWtaClZtD2Ip.htm)|Shark Diver|auto-trad|
|[R2ga1eyeSQA9w6KF.htm](vehicles/R2ga1eyeSQA9w6KF.htm)|Sailing Ship|auto-trad|
|[RGEmINZa7YXvbruu.htm](vehicles/RGEmINZa7YXvbruu.htm)|Clunkerjunker|auto-trad|
|[slkeDT68Zjmzz5l5.htm](vehicles/slkeDT68Zjmzz5l5.htm)|Steam Trolley|auto-trad|
|[t57KE8cj9MdG6Y5H.htm](vehicles/t57KE8cj9MdG6Y5H.htm)|Bone Ship|auto-trad|
|[tVBApIWuwKNwOics.htm](vehicles/tVBApIWuwKNwOics.htm)|Steam Giant|auto-trad|
|[UNOwkQq5lc4cYdZP.htm](vehicles/UNOwkQq5lc4cYdZP.htm)|Wagon|auto-trad|
|[V6ZuxECTGSyHAm7h.htm](vehicles/V6ZuxECTGSyHAm7h.htm)|Hillcross Glider|auto-trad|
|[vbJL4dPQcXfYigwv.htm](vehicles/vbJL4dPQcXfYigwv.htm)|Sky Chariot, Medium|auto-trad|
|[VNlJ7zPnOPThFEmR.htm](vehicles/VNlJ7zPnOPThFEmR.htm)|Airship|auto-trad|
|[VRhdfL78HS5sT4gF.htm](vehicles/VRhdfL78HS5sT4gF.htm)|Snail Coach|auto-trad|
|[vU3UnKWCRFhnvNLV.htm](vehicles/vU3UnKWCRFhnvNLV.htm)|Ambling Surveyor|auto-trad|
|[w6auGNQQ2VyMT001.htm](vehicles/w6auGNQQ2VyMT001.htm)|Sand Diver|auto-trad|
|[WBMeO6BPqEzc6Yv8.htm](vehicles/WBMeO6BPqEzc6Yv8.htm)|Chariot, Light|auto-trad|
|[WjOkOaWOGkLmuSq5.htm](vehicles/WjOkOaWOGkLmuSq5.htm)|Armored Carriage|auto-trad|
|[WsfgcVXqqmk1EVRY.htm](vehicles/WsfgcVXqqmk1EVRY.htm)|Velocipede|auto-trad|
|[WWvqHSAYOw4ysn3e.htm](vehicles/WWvqHSAYOw4ysn3e.htm)|Sleigh|auto-trad|
|[WXwFZ3DrLe2PchNf.htm](vehicles/WXwFZ3DrLe2PchNf.htm)|Cauldron of Flying|auto-trad|
|[Y2XrhWFdiGcVAUsz.htm](vehicles/Y2XrhWFdiGcVAUsz.htm)|Sky Chariot, Heavy|auto-trad|
|[ySR1Ds607BSX1tCw.htm](vehicles/ySR1Ds607BSX1tCw.htm)|Adaptable Paddleboat|auto-trad|
|[yXbxHtz3xVIMbxyh.htm](vehicles/yXbxHtz3xVIMbxyh.htm)|Mobile Inn|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
