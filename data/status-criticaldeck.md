# Estado de la traducción (criticaldeck)

 * **modificada**: 106


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0BESWYMP9vEbG3cr.htm](criticaldeck/0BESWYMP9vEbG3cr.htm)|Critical Fumble Deck #9|Critical Fumble Deck #9|modificada|
|[0tG5q8JkCaG1EY5s.htm](criticaldeck/0tG5q8JkCaG1EY5s.htm)|Critical Fumble Deck #1|Critical Fumble Deck #1|modificada|
|[1urnmEt4sn4ATDRL.htm](criticaldeck/1urnmEt4sn4ATDRL.htm)|Critical Hit Deck #29|Mazo Golpe Crítico #29|modificada|
|[1xTQ3C9aySvCCPXw.htm](criticaldeck/1xTQ3C9aySvCCPXw.htm)|Critical Hit Deck #35|Baraja de Golpe Crítico #35|modificada|
|[2CCPMTk5DlZslpOp.htm](criticaldeck/2CCPMTk5DlZslpOp.htm)|Critical Hit Deck #18|Critical Hit Deck #18|modificada|
|[2PtjynRwj4bBm4rZ.htm](criticaldeck/2PtjynRwj4bBm4rZ.htm)|Critical Fumble Deck #44|Critical Fumble Deck #44|modificada|
|[3vz4m5O9oyhVGe7t.htm](criticaldeck/3vz4m5O9oyhVGe7t.htm)|Critical Fumble Deck #26|Critical Fumble Deck #26|modificada|
|[4MkOtc3bBYsBGF97.htm](criticaldeck/4MkOtc3bBYsBGF97.htm)|Critical Fumble Deck #6|Critical Fumble Deck #6|modificada|
|[642qVWBhXlYwOhED.htm](criticaldeck/642qVWBhXlYwOhED.htm)|Critical Fumble Deck #39|Critical Fumble Deck #39|modificada|
|[6X9N5ieXqWitEVKw.htm](criticaldeck/6X9N5ieXqWitEVKw.htm)|Critical Fumble Deck #53|Critical Fumble Deck #53|modificada|
|[7RcrHguk3riysqgZ.htm](criticaldeck/7RcrHguk3riysqgZ.htm)|Critical Hit Deck #40|Mazo Golpe Crítico #40|modificada|
|[8D8NPBNMxQftBmyd.htm](criticaldeck/8D8NPBNMxQftBmyd.htm)|Critical Fumble Deck #46|Critical Fumble Deck #46|modificada|
|[8OoQs1TeJBW5UVby.htm](criticaldeck/8OoQs1TeJBW5UVby.htm)|Critical Hit Deck #21|Mazo Golpe Crítico #21|modificada|
|[99BxUbTtFlyPWkq5.htm](criticaldeck/99BxUbTtFlyPWkq5.htm)|Critical Fumble Deck #7|Critical Fumble Deck #7|modificada|
|[A0gsgMjgDpkOXtYp.htm](criticaldeck/A0gsgMjgDpkOXtYp.htm)|Critical Hit Deck #12|Critical Hit Deck #12|modificada|
|[AXZZ59kX1JNpIjKo.htm](criticaldeck/AXZZ59kX1JNpIjKo.htm)|Critical Fumble Deck #24|Critical Fumble Deck #24|modificada|
|[BgI1SePPP5QW9wSQ.htm](criticaldeck/BgI1SePPP5QW9wSQ.htm)|Critical Hit Deck #27|Mazo de Golpe Crítico #27|modificada|
|[bZF7zf43UBxS188h.htm](criticaldeck/bZF7zf43UBxS188h.htm)|Critical Hit Deck #48|Mazo Golpe Crítico #48|modificada|
|[bzIzGjyNUc52lhCS.htm](criticaldeck/bzIzGjyNUc52lhCS.htm)|Critical Hit Deck #4|Critical Hit Deck #4|modificada|
|[cPORMYYrqMaPfjnx.htm](criticaldeck/cPORMYYrqMaPfjnx.htm)|Critical Hit Deck #47|Critical Hit Deck #47|modificada|
|[cY3fpFYpmsUK9kKb.htm](criticaldeck/cY3fpFYpmsUK9kKb.htm)|Critical Hit Deck #25|Critical Hit Deck #25|modificada|
|[dCiYhvB488kKvpO0.htm](criticaldeck/dCiYhvB488kKvpO0.htm)|Critical Hit Deck #36|Mazo de Golpe Crítico #36|modificada|
|[DeZIaLR9okv231ek.htm](criticaldeck/DeZIaLR9okv231ek.htm)|Critical Fumble Deck #32|Critical Fumble Deck #32|modificada|
|[dP4Zy3ILcjWRx6ZO.htm](criticaldeck/dP4Zy3ILcjWRx6ZO.htm)|Critical Hit Deck #45|Mazo de Golpe Crítico #45|modificada|
|[DpY6k60UrpvKgjx2.htm](criticaldeck/DpY6k60UrpvKgjx2.htm)|Critical Fumble Deck #4|Critical Fumble Deck #4|modificada|
|[dvL063pJYJpCMsA6.htm](criticaldeck/dvL063pJYJpCMsA6.htm)|Critical Fumble Deck #52|Critical Fumble Deck #52|modificada|
|[DYaq4QSRuyhp4DQL.htm](criticaldeck/DYaq4QSRuyhp4DQL.htm)|Critical Hit Deck #51|Mazo de Golpe Crítico #51|modificada|
|[e08YEjoNa60xrAYS.htm](criticaldeck/e08YEjoNa60xrAYS.htm)|Critical Fumble Deck #45|Critical Fumble Deck #45|modificada|
|[e9imRGVLbIWYVUXf.htm](criticaldeck/e9imRGVLbIWYVUXf.htm)|Critical Hit Deck #1|Mazo Golpe Crítico #1|modificada|
|[EaNgK8fG3Fur9uzp.htm](criticaldeck/EaNgK8fG3Fur9uzp.htm)|Critical Hit Deck #17|Critical Hit Deck #17|modificada|
|[EP333dUdoJ8xdkzq.htm](criticaldeck/EP333dUdoJ8xdkzq.htm)|Critical Fumble Deck #5|Critical Fumble Deck #5|modificada|
|[EpWswZucGxXiLNyP.htm](criticaldeck/EpWswZucGxXiLNyP.htm)|Critical Hit Deck #43|Critical Hit Deck #43|modificada|
|[eVEZLDyvu2tqiYZn.htm](criticaldeck/eVEZLDyvu2tqiYZn.htm)|Critical Hit Deck #28|Critical Hit Deck #28|modificada|
|[f1JjbSLpmPw5McIb.htm](criticaldeck/f1JjbSLpmPw5McIb.htm)|Critical Hit Deck #42|Critical Hit Deck #42|modificada|
|[FHnfWQKntBkZJk6G.htm](criticaldeck/FHnfWQKntBkZJk6G.htm)|Critical Hit Deck #15|Critical Hit Deck #15|modificada|
|[gdYlgrtfGK8mAoXK.htm](criticaldeck/gdYlgrtfGK8mAoXK.htm)|Critical Hit Deck #9|Mazo Golpe Crítico #9|modificada|
|[gLXD4uNnLLtLoeck.htm](criticaldeck/gLXD4uNnLLtLoeck.htm)|Critical Fumble Deck #23|Critical Fumble Deck #23|modificada|
|[GUtWJynPuVmatqNF.htm](criticaldeck/GUtWJynPuVmatqNF.htm)|Critical Fumble Deck #16|Critical Fumble Deck #16|modificada|
|[gWa7wGUfUvYoIK8z.htm](criticaldeck/gWa7wGUfUvYoIK8z.htm)|Critical Fumble Deck #20|Critical Fumble Deck #20|modificada|
|[HERWPcFlzK2FTPLJ.htm](criticaldeck/HERWPcFlzK2FTPLJ.htm)|Critical Fumble Deck #47|Critical Fumble Deck #47|modificada|
|[I5omhoK6rqfCZiUZ.htm](criticaldeck/I5omhoK6rqfCZiUZ.htm)|Critical Fumble Deck #21|Critical Fumble Deck #21|modificada|
|[IrUeMJoKEYUSJKs1.htm](criticaldeck/IrUeMJoKEYUSJKs1.htm)|Critical Fumble Deck #19|Critical Fumble Deck #19|modificada|
|[IvDJkDpe3JxMHaj1.htm](criticaldeck/IvDJkDpe3JxMHaj1.htm)|Critical Fumble Deck #28|Critical Fumble Deck #28|modificada|
|[ivko8YN075ThpNsb.htm](criticaldeck/ivko8YN075ThpNsb.htm)|Critical Hit Deck #32|Critical Hit Deck #32|modificada|
|[iZYZTEtihsVkbRKR.htm](criticaldeck/iZYZTEtihsVkbRKR.htm)|Critical Hit Deck #8|Critical Hit Deck #8|modificada|
|[JfnvVoWqEHN1ymd5.htm](criticaldeck/JfnvVoWqEHN1ymd5.htm)|Critical Hit Deck #14|Mazo Golpe Crítico #14|modificada|
|[jLnyeFUJ76ZZieVJ.htm](criticaldeck/jLnyeFUJ76ZZieVJ.htm)|Critical Fumble Deck #33|Critical Fumble Deck #33|modificada|
|[K2U6PVeYrvNagqti.htm](criticaldeck/K2U6PVeYrvNagqti.htm)|Critical Fumble Deck #27|Critical Fumble Deck #27|modificada|
|[kjoBsS79JcjUUPnr.htm](criticaldeck/kjoBsS79JcjUUPnr.htm)|Critical Fumble Deck #38|Critical Fumble Deck #38|modificada|
|[kjUiMaSmcOQXxoeo.htm](criticaldeck/kjUiMaSmcOQXxoeo.htm)|Critical Fumble Deck #11|Critical Fumble Deck #11|modificada|
|[klZe3T5N3T79oQvN.htm](criticaldeck/klZe3T5N3T79oQvN.htm)|Critical Hit Deck #19|Mazo de Golpe Crítico #19|modificada|
|[kvQc6aNvJaBH6rEC.htm](criticaldeck/kvQc6aNvJaBH6rEC.htm)|Critical Hit Deck #24|Critical Hit Deck #24|modificada|
|[lAPaa4MtkDZy6nZN.htm](criticaldeck/lAPaa4MtkDZy6nZN.htm)|Critical Hit Deck #38|Critical Hit Deck #38|modificada|
|[LI8WRDVWPbwr6pcG.htm](criticaldeck/LI8WRDVWPbwr6pcG.htm)|Critical Hit Deck #52|Mazo Golpe Crítico #52|modificada|
|[lXew3cZFdIYM3NJs.htm](criticaldeck/lXew3cZFdIYM3NJs.htm)|Critical Hit Deck #10|Critical Hit Deck #10|modificada|
|[LY6T2YgrSoI79BDR.htm](criticaldeck/LY6T2YgrSoI79BDR.htm)|Critical Fumble Deck #8|Critical Fumble Deck #8|modificada|
|[maXudTqcDiAj86Ym.htm](criticaldeck/maXudTqcDiAj86Ym.htm)|Critical Hit Deck #6|Critical Hit Deck #6|modificada|
|[mcniWnifXwXkZocv.htm](criticaldeck/mcniWnifXwXkZocv.htm)|Critical Fumble Deck #41|Critical Fumble Deck #41|modificada|
|[mwz1Ukw9laGyRocF.htm](criticaldeck/mwz1Ukw9laGyRocF.htm)|Critical Fumble Deck #51|Critical Fumble Deck #51|modificada|
|[nI9pM4SeM39SRVaR.htm](criticaldeck/nI9pM4SeM39SRVaR.htm)|Critical Hit Deck #41|Mazo Golpe Crítico #41|modificada|
|[NNfsKAHcKvTuyha4.htm](criticaldeck/NNfsKAHcKvTuyha4.htm)|Critical Fumble Deck #36|Critical Fumble Deck #36|modificada|
|[nSU4nBU3fBTvb2k3.htm](criticaldeck/nSU4nBU3fBTvb2k3.htm)|Critical Hit Deck #20|Mazo de Golpes Críticos #20|modificada|
|[OvQYM50fG50aOHfR.htm](criticaldeck/OvQYM50fG50aOHfR.htm)|Critical Hit Deck #34|Mazo Golpe Crítico #34|modificada|
|[pjR0QGXE58oWS8uU.htm](criticaldeck/pjR0QGXE58oWS8uU.htm)|Critical Hit Deck #7|Critical Hit Deck #7|modificada|
|[Ps9VYxI2zJPG6PEV.htm](criticaldeck/Ps9VYxI2zJPG6PEV.htm)|Critical Fumble Deck #22|Critical Fumble Deck #22|modificada|
|[pzk8lcb4ykgtn97D.htm](criticaldeck/pzk8lcb4ykgtn97D.htm)|Critical Hit Deck #23|Critical Hit Deck #23|modificada|
|[QbnfMqilzwyzk0bf.htm](criticaldeck/QbnfMqilzwyzk0bf.htm)|Critical Fumble Deck #34|Critical Fumble Deck #34|modificada|
|[QCHhZd0F0DmvVxYP.htm](criticaldeck/QCHhZd0F0DmvVxYP.htm)|Critical Fumble Deck #37|Critical Fumble Deck #37|modificada|
|[qD4ybNUceFacSJGu.htm](criticaldeck/qD4ybNUceFacSJGu.htm)|Critical Fumble Deck #3|Critical Fumble Deck #3|modificada|
|[qD8KoshDJsayGbCB.htm](criticaldeck/qD8KoshDJsayGbCB.htm)|Critical Fumble Deck #40|Critical Fumble Deck #40|modificada|
|[qOmgdhhShYp4vlfi.htm](criticaldeck/qOmgdhhShYp4vlfi.htm)|Critical Hit Deck #3|Mazo Golpe Crítico #3|modificada|
|[QR3e1uCNURvxDzbV.htm](criticaldeck/QR3e1uCNURvxDzbV.htm)|Critical Hit Deck #50|Mazo de Golpe Crítico #50|modificada|
|[QuhotSkof0ao2kJo.htm](criticaldeck/QuhotSkof0ao2kJo.htm)|Critical Fumble Deck #50|Critical Fumble Deck #50|modificada|
|[Rkv1BGGapMuMyMHs.htm](criticaldeck/Rkv1BGGapMuMyMHs.htm)|Critical Hit Deck #37|Mazo Golpe Crítico #37|modificada|
|[rLNl2PdawSadogJd.htm](criticaldeck/rLNl2PdawSadogJd.htm)|Critical Hit Deck #2|Critical Hit Deck #2|modificada|
|[RZAYqAHzFxnbD01e.htm](criticaldeck/RZAYqAHzFxnbD01e.htm)|Critical Fumble Deck #30|Critical Fumble Deck #30|modificada|
|[SQAqU3m7BXHm8TGC.htm](criticaldeck/SQAqU3m7BXHm8TGC.htm)|Critical Hit Deck #39|Critical Hit Deck #39|modificada|
|[TbXmAqEzgW1nZV2r.htm](criticaldeck/TbXmAqEzgW1nZV2r.htm)|Critical Fumble Deck #49|Critical Fumble Deck #49|modificada|
|[tHmyeunsbv4TZAbw.htm](criticaldeck/tHmyeunsbv4TZAbw.htm)|Critical Fumble Deck #17|Critical Fumble Deck #17|modificada|
|[Toz2ZIpy7N4pDmyi.htm](criticaldeck/Toz2ZIpy7N4pDmyi.htm)|Critical Fumble Deck #18|Critical Fumble Deck #18|modificada|
|[txOOwTfhpIJj4LU1.htm](criticaldeck/txOOwTfhpIJj4LU1.htm)|Critical Fumble Deck #12|Critical Fumble Deck #12|modificada|
|[TYTNFtVvFqFBM8oy.htm](criticaldeck/TYTNFtVvFqFBM8oy.htm)|Critical Fumble Deck #43|Critical Fumble Deck #43|modificada|
|[UheptedVuKxVHSQY.htm](criticaldeck/UheptedVuKxVHSQY.htm)|Critical Fumble Deck #25|Critical Fumble Deck #25|modificada|
|[unEu0FFXZfTtvF2b.htm](criticaldeck/unEu0FFXZfTtvF2b.htm)|Critical Hit Deck #26|Mazo de Golpe Crítico #26|modificada|
|[v7aUL4DIk7lkw1EO.htm](criticaldeck/v7aUL4DIk7lkw1EO.htm)|Critical Hit Deck #44|Mazo de Golpe Crítico #44|modificada|
|[vCvtsUB6C6PEdvog.htm](criticaldeck/vCvtsUB6C6PEdvog.htm)|Critical Hit Deck #11|Critical Hit Deck #11|modificada|
|[VfijeX07Jy9QPYPb.htm](criticaldeck/VfijeX07Jy9QPYPb.htm)|Critical Fumble Deck #14|Critical Fumble Deck #14|modificada|
|[vFXt9SSkNst7qD6v.htm](criticaldeck/vFXt9SSkNst7qD6v.htm)|Critical Fumble Deck #13|Critical Fumble Deck #13|modificada|
|[vXPWnWa7B1qsrOMa.htm](criticaldeck/vXPWnWa7B1qsrOMa.htm)|Critical Hit Deck #5|Critical Hit Deck #5|modificada|
|[wcqXFhggTcAutBk6.htm](criticaldeck/wcqXFhggTcAutBk6.htm)|Critical Fumble Deck #29|Critical Fumble Deck #29|modificada|
|[wCsoyffDxSWQ2c8o.htm](criticaldeck/wCsoyffDxSWQ2c8o.htm)|Critical Hit Deck #46|Mazo de Golpe Crítico #46|modificada|
|[wemXkXGY7sl0tUQR.htm](criticaldeck/wemXkXGY7sl0tUQR.htm)|Critical Hit Deck #16|Mazo Golpe Crítico #16|modificada|
|[wga13mR4vo73tbTY.htm](criticaldeck/wga13mR4vo73tbTY.htm)|Critical Fumble Deck #10|Critical Fumble Deck #10|modificada|
|[WGBL7l5DatQnszg7.htm](criticaldeck/WGBL7l5DatQnszg7.htm)|Critical Fumble Deck #35|Critical Fumble Deck #35|modificada|
|[wHwwHq9RcOUvwFeB.htm](criticaldeck/wHwwHq9RcOUvwFeB.htm)|Critical Hit Deck #22|Mazo de Golpe Crítico #22|modificada|
|[Wn47ZhXINo7jz8JZ.htm](criticaldeck/Wn47ZhXINo7jz8JZ.htm)|Critical Fumble Deck #2|Critical Fumble Deck #2|modificada|
|[x4850Hq18S9qYGed.htm](criticaldeck/x4850Hq18S9qYGed.htm)|Critical Hit Deck #31|Critical Hit Deck #31|modificada|
|[XhX99wCRrwB1JFnl.htm](criticaldeck/XhX99wCRrwB1JFnl.htm)|Critical Hit Deck #33|Mazo Golpe Crítico #33|modificada|
|[XmpWko9I3NDN6rjJ.htm](criticaldeck/XmpWko9I3NDN6rjJ.htm)|Critical Hit Deck #53|Mazo de Golpe Crítico #53|modificada|
|[xsBlA35rvfHSzJe4.htm](criticaldeck/xsBlA35rvfHSzJe4.htm)|Critical Fumble Deck #15|Critical Fumble Deck #15|modificada|
|[y4EgjWn5MDIQC7JR.htm](criticaldeck/y4EgjWn5MDIQC7JR.htm)|Critical Hit Deck #30|Mazo de Golpe Crítico #30|modificada|
|[Y4J49n6WFOEXwaf2.htm](criticaldeck/Y4J49n6WFOEXwaf2.htm)|Critical Hit Deck #13|Critical Hit Deck #13|modificada|
|[yBE9e9l5KW7SdHZH.htm](criticaldeck/yBE9e9l5KW7SdHZH.htm)|Critical Fumble Deck #31|Critical Fumble Deck #31|modificada|
|[YcQEfWxkmpMWXUz4.htm](criticaldeck/YcQEfWxkmpMWXUz4.htm)|Critical Hit Deck #49|Mazo Golpe Crítico #49|modificada|
|[YRY6c3WdBEuKiOCv.htm](criticaldeck/YRY6c3WdBEuKiOCv.htm)|Critical Fumble Deck #42|Critical Fumble Deck #42|modificada|
|[Zx67JiRVzstgEa32.htm](criticaldeck/Zx67JiRVzstgEa32.htm)|Critical Fumble Deck #48|Critical Fumble Deck #48|modificada|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
