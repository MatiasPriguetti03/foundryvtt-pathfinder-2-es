# Estado de la traducción (gmg-srd)

 * **auto-trad**: 98


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[2cyhXgGkmoFe3Phw.htm](gmg-srd/2cyhXgGkmoFe3Phw.htm)|Trained by a Mentor|auto-trad|
|[3g3uXFsHbfcad8eA.htm](gmg-srd/3g3uXFsHbfcad8eA.htm)|The Fool|auto-trad|
|[3Uwfn0ui7tS277iE.htm](gmg-srd/3Uwfn0ui7tS277iE.htm)|Metropolis|auto-trad|
|[5KDGPcdZpdZumUYy.htm](gmg-srd/5KDGPcdZpdZumUYy.htm)|Itinerant|auto-trad|
|[652cIWAXc8zAxkac.htm](gmg-srd/652cIWAXc8zAxkac.htm)|Matter of Might|auto-trad|
|[6NOZe9gUfVEedECW.htm](gmg-srd/6NOZe9gUfVEedECW.htm)|Star|auto-trad|
|[7qsQBWD5sSGz7Fik.htm](gmg-srd/7qsQBWD5sSGz7Fik.htm)|Mercantile Expertise|auto-trad|
|[8NKe8N5NsULPShPX.htm](gmg-srd/8NKe8N5NsULPShPX.htm)|The Dead One|auto-trad|
|[a0mJO7io1hcwMAjJ.htm](gmg-srd/a0mJO7io1hcwMAjJ.htm)|Lost in the Wilderness|auto-trad|
|[AL562TPAhLCFX62S.htm](gmg-srd/AL562TPAhLCFX62S.htm)|Underground|auto-trad|
|[Alf16WU6qK8w1jmq.htm](gmg-srd/Alf16WU6qK8w1jmq.htm)|Sun|auto-trad|
|[amEb5dGmSvvAJz7J.htm](gmg-srd/amEb5dGmSvvAJz7J.htm)|Balance|auto-trad|
|[BAs1fCaUTmvRjvVT.htm](gmg-srd/BAs1fCaUTmvRjvVT.htm)|Dullard|auto-trad|
|[BbgtR5LmB9ZdPpx1.htm](gmg-srd/BbgtR5LmB9ZdPpx1.htm)|Timely Cure|auto-trad|
|[bNmSwKSFk6SWWrlj.htm](gmg-srd/bNmSwKSFk6SWWrlj.htm)|Abandoned in a Distant Land|auto-trad|
|[bRQz8uVlYtQ1cwwI.htm](gmg-srd/bRQz8uVlYtQ1cwwI.htm)|Wasteland Survivors|auto-trad|
|[bvhckAGjcCJVs0jn.htm](gmg-srd/bvhckAGjcCJVs0jn.htm)|Claimed an Inheritance|auto-trad|
|[C4KgMYzmyWVpe721.htm](gmg-srd/C4KgMYzmyWVpe721.htm)|Fool|auto-trad|
|[c8bBOo2RPxStZozM.htm](gmg-srd/c8bBOo2RPxStZozM.htm)|Moon|auto-trad|
|[ccG9pxYQ9Gg6262b.htm](gmg-srd/ccG9pxYQ9Gg6262b.htm)|Key|auto-trad|
|[cD51eAUtbGlp5UKr.htm](gmg-srd/cD51eAUtbGlp5UKr.htm)|Slander|auto-trad|
|[Cfd7ndT61VjR52OZ.htm](gmg-srd/Cfd7ndT61VjR52OZ.htm)|The Mercenary|auto-trad|
|[CfD81LilQAtGuvyx.htm](gmg-srd/CfD81LilQAtGuvyx.htm)|The Void|auto-trad|
|[cIlNXxPJuy8MtaDM.htm](gmg-srd/cIlNXxPJuy8MtaDM.htm)|Vizier|auto-trad|
|[CW53y8R730XoNQSG.htm](gmg-srd/CW53y8R730XoNQSG.htm)|The Champion|auto-trad|
|[CwyYDIAAuRll2i5R.htm](gmg-srd/CwyYDIAAuRll2i5R.htm)|The Seer|auto-trad|
|[dcP3oqjsl85p6R2Z.htm](gmg-srd/dcP3oqjsl85p6R2Z.htm)|Kidnapped|auto-trad|
|[Dyg3vJnEnReKyYHU.htm](gmg-srd/Dyg3vJnEnReKyYHU.htm)|The Fates|auto-trad|
|[e4nkDjB1Y6nqaK2G.htm](gmg-srd/e4nkDjB1Y6nqaK2G.htm)|Betrayed|auto-trad|
|[eADadfrHPkGNTjEB.htm](gmg-srd/eADadfrHPkGNTjEB.htm)|Accidental Fall|auto-trad|
|[eCB10E0MSvD9s008.htm](gmg-srd/eCB10E0MSvD9s008.htm)|Frontier|auto-trad|
|[ejRPZwAlci3jTli9.htm](gmg-srd/ejRPZwAlci3jTli9.htm)|The Liege Lord|auto-trad|
|[Erqgqv8WEtnzBtTu.htm](gmg-srd/Erqgqv8WEtnzBtTu.htm)|The Mystic|auto-trad|
|[F7Tf6APgUKjnCZgv.htm](gmg-srd/F7Tf6APgUKjnCZgv.htm)|The Crafter|auto-trad|
|[fojjq3VXShnNSOsZ.htm](gmg-srd/fojjq3VXShnNSOsZ.htm)|Academic Community|auto-trad|
|[FOs2eLbehUzPh9p9.htm](gmg-srd/FOs2eLbehUzPh9p9.htm)|Attained a Magical Gift|auto-trad|
|[G9058881e1ci9X1C.htm](gmg-srd/G9058881e1ci9X1C.htm)|Desperate Intimidation|auto-trad|
|[Gm4AtuFNRgOnzsmF.htm](gmg-srd/Gm4AtuFNRgOnzsmF.htm)|Simple Village|auto-trad|
|[Gr4v8ROPCxi4hvAO.htm](gmg-srd/Gr4v8ROPCxi4hvAO.htm)|Religious Community|auto-trad|
|[HkMzwjBTGgQgsOCW.htm](gmg-srd/HkMzwjBTGgQgsOCW.htm)|Cosmopolitan City|auto-trad|
|[iA9QWuYATBPB6thI.htm](gmg-srd/iA9QWuYATBPB6thI.htm)|Knight|auto-trad|
|[idZxjeEv8B3x44r1.htm](gmg-srd/idZxjeEv8B3x44r1.htm)|Captured by Giants|auto-trad|
|[IJ3nFDu2YcoP6ynd.htm](gmg-srd/IJ3nFDu2YcoP6ynd.htm)|Throne|auto-trad|
|[Ip6kSKGhl3XHQZ93.htm](gmg-srd/Ip6kSKGhl3XHQZ93.htm)|Robbed|auto-trad|
|[j9kx5CzfulaFTMOE.htm](gmg-srd/j9kx5CzfulaFTMOE.htm)|Academy Trained|auto-trad|
|[JHQI2skcksaLMPqA.htm](gmg-srd/JHQI2skcksaLMPqA.htm)|Kindly Witch|auto-trad|
|[jtgfTYpcv4FFpWsL.htm](gmg-srd/jtgfTYpcv4FFpWsL.htm)|Died|auto-trad|
|[jVh90bfsC6w6iVyy.htm](gmg-srd/jVh90bfsC6w6iVyy.htm)|Euryale (curse)|auto-trad|
|[KtkDaTrkBwwupKcE.htm](gmg-srd/KtkDaTrkBwwupKcE.htm)|The Wanderer|auto-trad|
|[LekxXsOgEwMODqCS.htm](gmg-srd/LekxXsOgEwMODqCS.htm)|Animal Helpers|auto-trad|
|[llfG8iMTFMzVIF9s.htm](gmg-srd/llfG8iMTFMzVIF9s.htm)|Donjon|auto-trad|
|[MCIp76IWZ2pk7dyz.htm](gmg-srd/MCIp76IWZ2pk7dyz.htm)|The Criminal|auto-trad|
|[mg7oMp0cNa0GJMCS.htm](gmg-srd/mg7oMp0cNa0GJMCS.htm)|The Lover|auto-trad|
|[mHocbJQKBzbHM790.htm](gmg-srd/mHocbJQKBzbHM790.htm)|The Confidante|auto-trad|
|[niZPxqupDMfIXxJs.htm](gmg-srd/niZPxqupDMfIXxJs.htm)|Homelessness|auto-trad|
|[NSG1YxOYAtSowNJa.htm](gmg-srd/NSG1YxOYAtSowNJa.htm)|Comet|auto-trad|
|[nsZ93vUEFucsLwRz.htm](gmg-srd/nsZ93vUEFucsLwRz.htm)|Privileged Position|auto-trad|
|[nzOpdMVLPhoNTxzR.htm](gmg-srd/nzOpdMVLPhoNTxzR.htm)|Comrade-in-Arms|auto-trad|
|[OdD3NuxI5hNAEOVx.htm](gmg-srd/OdD3NuxI5hNAEOVx.htm)|Trade Town|auto-trad|
|[oDhTOO1WjEZk1qfD.htm](gmg-srd/oDhTOO1WjEZk1qfD.htm)|Witnessed War|auto-trad|
|[OHKKQ7zTf61vz35h.htm](gmg-srd/OHKKQ7zTf61vz35h.htm)|Liberators|auto-trad|
|[pd6ZzGL5TA58Fa3p.htm](gmg-srd/pd6ZzGL5TA58Fa3p.htm)|The Hunter|auto-trad|
|[PVdcBHjBVySBchG1.htm](gmg-srd/PVdcBHjBVySBchG1.htm)|Religious Students|auto-trad|
|[pwn7lPcwqyGNZyOo.htm](gmg-srd/pwn7lPcwqyGNZyOo.htm)|Survived a Disaster|auto-trad|
|[qFgPiHDBVa3VSNgC.htm](gmg-srd/qFgPiHDBVa3VSNgC.htm)|Gem|auto-trad|
|[QhNWhs0isjxE67lu.htm](gmg-srd/QhNWhs0isjxE67lu.htm)|The Fiend|auto-trad|
|[QxQ0PheFSIJU9rfH.htm](gmg-srd/QxQ0PheFSIJU9rfH.htm)|Talons|auto-trad|
|[rDbdifseYxfEWgKg.htm](gmg-srd/rDbdifseYxfEWgKg.htm)|Patron of the Arts|auto-trad|
|[re8nmHRiQdU63AYk.htm](gmg-srd/re8nmHRiQdU63AYk.htm)|The Boss|auto-trad|
|[Rh5XWscABJ43TLme.htm](gmg-srd/Rh5XWscABJ43TLme.htm)|The Mentor|auto-trad|
|[Runwg9xhNrN79JBN.htm](gmg-srd/Runwg9xhNrN79JBN.htm)|Had an Ordinary Childhood|auto-trad|
|[SbfCzPannCdZEB7c.htm](gmg-srd/SbfCzPannCdZEB7c.htm)|Jester|auto-trad|
|[sHC4quiLAzdUe2uT.htm](gmg-srd/sHC4quiLAzdUe2uT.htm)|Accusation of Theft|auto-trad|
|[SyEl079jcULQp7Ra.htm](gmg-srd/SyEl079jcULQp7Ra.htm)|Front Lines|auto-trad|
|[TG9L9JvKpomFQUfl.htm](gmg-srd/TG9L9JvKpomFQUfl.htm)|Flames|auto-trad|
|[tmrnZkUzHVC0s6D9.htm](gmg-srd/tmrnZkUzHVC0s6D9.htm)|Had Your First Kill|auto-trad|
|[TNruMcXK4wizclQc.htm](gmg-srd/TNruMcXK4wizclQc.htm)|Raided|auto-trad|
|[UGxZ6vhfq7vQFXTt.htm](gmg-srd/UGxZ6vhfq7vQFXTt.htm)|Magician|auto-trad|
|[uK1fqqsUNk7FZsOk.htm](gmg-srd/uK1fqqsUNk7FZsOk.htm)|The Pariah|auto-trad|
|[upnKpiEflAhhFLNQ.htm](gmg-srd/upnKpiEflAhhFLNQ.htm)|Missing Child|auto-trad|
|[vD1y8mIHxxw7c8J1.htm](gmg-srd/vD1y8mIHxxw7c8J1.htm)|Coastal Community|auto-trad|
|[VEFcfA4NnWdJSRtp.htm](gmg-srd/VEFcfA4NnWdJSRtp.htm)|Spy|auto-trad|
|[vMTOwBTqs8JeE8bH.htm](gmg-srd/vMTOwBTqs8JeE8bH.htm)|Rival Trackers|auto-trad|
|[VnrGXdVthTceOlXV.htm](gmg-srd/VnrGXdVthTceOlXV.htm)|Another Ancestry's Settlement|auto-trad|
|[W2ccu0zeLNHDlCXe.htm](gmg-srd/W2ccu0zeLNHDlCXe.htm)|Bullied|auto-trad|
|[wIGRRmXtzed74qQq.htm](gmg-srd/wIGRRmXtzed74qQq.htm)|Fell In with a Bad Crowd|auto-trad|
|[WXQbdAmQM9HBjc0R.htm](gmg-srd/WXQbdAmQM9HBjc0R.htm)|Relationship Ender|auto-trad|
|[x0vU6WsHcXUNY20y.htm](gmg-srd/x0vU6WsHcXUNY20y.htm)|Ruin|auto-trad|
|[xe0iqQ3XSnDV4tLp.htm](gmg-srd/xe0iqQ3XSnDV4tLp.htm)|Rogue|auto-trad|
|[xGkT6QZGfYE3kBqn.htm](gmg-srd/xGkT6QZGfYE3kBqn.htm)|Skull|auto-trad|
|[Xi5nvMNJrzsy09ws.htm](gmg-srd/Xi5nvMNJrzsy09ws.htm)|The Well-Connected|auto-trad|
|[xmgvqyfwYPBXVELo.htm](gmg-srd/xmgvqyfwYPBXVELo.htm)|Met a Fantastic Creature|auto-trad|
|[y4quheEoo2bcmM6g.htm](gmg-srd/y4quheEoo2bcmM6g.htm)|Seeking Accolades|auto-trad|
|[yqCZWcX9e3iwmvSS.htm](gmg-srd/yqCZWcX9e3iwmvSS.htm)|Social Maneuvering|auto-trad|
|[YR0uxB5CPPueFjBs.htm](gmg-srd/YR0uxB5CPPueFjBs.htm)|The Relative|auto-trad|
|[YsY9euHOGCGc3mTE.htm](gmg-srd/YsY9euHOGCGc3mTE.htm)|Won a Competition|auto-trad|
|[yZUZDw8TXX1MvZ4P.htm](gmg-srd/yZUZDw8TXX1MvZ4P.htm)|Called Before Judges|auto-trad|
|[ziWhyyygUG3Lemdu.htm](gmg-srd/ziWhyyygUG3Lemdu.htm)|The Academic|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
