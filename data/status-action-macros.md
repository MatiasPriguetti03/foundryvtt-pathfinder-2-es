# Estado de la traducción (action-macros)

 * **auto-trad**: 60
 * **ninguna**: 10


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[50Q0DYL33Kalu1BH.htm](action-macros/50Q0DYL33Kalu1BH.htm)|Escape: Acrobatics|
|[aOrMEnTVPpb1vbzj.htm](action-macros/aOrMEnTVPpb1vbzj.htm)|Escape: Unarmed Attack|
|[EpiQHXzHv0GS1tj0.htm](action-macros/EpiQHXzHv0GS1tj0.htm)|Escape|
|[Gj68YCVlDjc75iCP.htm](action-macros/Gj68YCVlDjc75iCP.htm)|Palm an Object: Thievery|
|[l5pbgrj8SSNtRGs8.htm](action-macros/l5pbgrj8SSNtRGs8.htm)|Administer First Aid - Stabilize: Medicine|
|[lkEcQQss16SIrVxM.htm](action-macros/lkEcQQss16SIrVxM.htm)|Escape: Athletics|
|[mNphXpAkmGsMadUv.htm](action-macros/mNphXpAkmGsMadUv.htm)|Create Forgery: Society|
|[ZEWD4zcEDQwYhVT8.htm](action-macros/ZEWD4zcEDQwYhVT8.htm)|Administer First Aid - Stop Bleeding: Medicine|
|[zjovbAeuLvyuWFKd.htm](action-macros/zjovbAeuLvyuWFKd.htm)|Steal: Thievery|
|[zn0HadZeoKDALxRu.htm](action-macros/zn0HadZeoKDALxRu.htm)|Conceal an Object: Stealth|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[1AIo5UcVbCmvpRL3.htm](action-macros/1AIo5UcVbCmvpRL3.htm)|Exploration: Avoid Notice|auto-trad|
|[1JpYPlIkjyseE9JU.htm](action-macros/1JpYPlIkjyseE9JU.htm)|Create a Diversion - Trick: Deception|auto-trad|
|[1Sj2Pz3VI2SFWqZw.htm](action-macros/1Sj2Pz3VI2SFWqZw.htm)|Make an Impression: Diplomacy|auto-trad|
|[2qhYHkcSsTJoSwrJ.htm](action-macros/2qhYHkcSsTJoSwrJ.htm)|Tumble Through: Acrobatics|auto-trad|
|[4hfQEMiEOBbqelAh.htm](action-macros/4hfQEMiEOBbqelAh.htm)|Raise a Shield|auto-trad|
|[55mxH0w8UkY1o3Xv.htm](action-macros/55mxH0w8UkY1o3Xv.htm)|Balance: Acrobatics|auto-trad|
|[7zqvzpR9gvHPUDqC.htm](action-macros/7zqvzpR9gvHPUDqC.htm)|Whirling Throw: Athletics|auto-trad|
|[8YrH37NzKRuiKFbF.htm](action-macros/8YrH37NzKRuiKFbF.htm)|Pick a Lock: Thievery|auto-trad|
|[9RNumMausgG7adgL.htm](action-macros/9RNumMausgG7adgL.htm)|Coerce: Intimidation|auto-trad|
|[9Ul5Op5OceT9P5SS.htm](action-macros/9Ul5Op5OceT9P5SS.htm)|Maneuver in Flight: Acrobatics|auto-trad|
|[aDsYSdRqiC6qQIOQ.htm](action-macros/aDsYSdRqiC6qQIOQ.htm)|Create a Diversion - Distracting Words: Deception|auto-trad|
|[Al5LYMMdeDcpC9Br.htm](action-macros/Al5LYMMdeDcpC9Br.htm)|Track: Survival|auto-trad|
|[BQTA7bL264189Xla.htm](action-macros/BQTA7bL264189Xla.htm)|Repair: Crafting|auto-trad|
|[Bw3rSN774LaHLELJ.htm](action-macros/Bw3rSN774LaHLELJ.htm)|Perform - Play keyboard instrument: Performance|auto-trad|
|[dWcrojMk0d2WRPBq.htm](action-macros/dWcrojMk0d2WRPBq.htm)|Perform - Singing: Performance|auto-trad|
|[EDLftLWLBefTFDtu.htm](action-macros/EDLftLWLBefTFDtu.htm)|Bon Mot: Diplomacy|auto-trad|
|[FlM3HvpnsZpCKawG.htm](action-macros/FlM3HvpnsZpCKawG.htm)|Hide: Stealth|auto-trad|
|[gRj7xUfcpUZQLrOC.htm](action-macros/gRj7xUfcpUZQLrOC.htm)|Trip: Athletics|auto-trad|
|[HQrDnBbbSCQEXrSb.htm](action-macros/HQrDnBbbSCQEXrSb.htm)|Perform - Play wind instrument: Performance|auto-trad|
|[HSTkVuv0SjTNK3Xx.htm](action-macros/HSTkVuv0SjTNK3Xx.htm)|Sneak: Stealth|auto-trad|
|[I6MMTpk9faBOHb2T.htm](action-macros/I6MMTpk9faBOHb2T.htm)|Encouraging Words|auto-trad|
|[i6WwSpxDuV2jCwB4.htm](action-macros/i6WwSpxDuV2jCwB4.htm)|Perform - Comedy: Performance|auto-trad|
|[i95kcGLIQKOTsnv6.htm](action-macros/i95kcGLIQKOTsnv6.htm)|Grapple: Athletics|auto-trad|
|[jOM1w2GymtSuEurf.htm](action-macros/jOM1w2GymtSuEurf.htm)|Tamper: Crafting|auto-trad|
|[k5nW4jGyXD0Oq9LR.htm](action-macros/k5nW4jGyXD0Oq9LR.htm)|Impersonate: Deception|auto-trad|
|[LN67MgbGE8IHb2X0.htm](action-macros/LN67MgbGE8IHb2X0.htm)|Sense Direction: Survival|auto-trad|
|[LXCy1iJddD95Z91s.htm](action-macros/LXCy1iJddD95Z91s.htm)|Climb: Athletics|auto-trad|
|[m4iM5r3TfvQs5Y2n.htm](action-macros/m4iM5r3TfvQs5Y2n.htm)|Treat Disease: Medicine|auto-trad|
|[mkKko3CEBCyJVQw1.htm](action-macros/mkKko3CEBCyJVQw1.htm)|Subsist: Society|auto-trad|
|[nEwqNNWX6scLt4sc.htm](action-macros/nEwqNNWX6scLt4sc.htm)|Demoralize: Intimidation|auto-trad|
|[nvpRlDKUD4SjZaJ0.htm](action-macros/nvpRlDKUD4SjZaJ0.htm)|Perform - Play string instrument: Performance|auto-trad|
|[Om0rNX4K3pQcXDJn.htm](action-macros/Om0rNX4K3pQcXDJn.htm)|Perform - Oratory: Performance|auto-trad|
|[ooiO59Ch2QaebOmc.htm](action-macros/ooiO59Ch2QaebOmc.htm)|Disarm: Athletics|auto-trad|
|[oxowCzHbxSGOWRke.htm](action-macros/oxowCzHbxSGOWRke.htm)|Steel Your Resolve|auto-trad|
|[PgA74rWIAupEr7LU.htm](action-macros/PgA74rWIAupEr7LU.htm)|Perform - Dance: Performance|auto-trad|
|[PmHt7Gb5fCrlWWTr.htm](action-macros/PmHt7Gb5fCrlWWTr.htm)|Sense Motive: Perception|auto-trad|
|[QPsV0qi2zXm7syt6.htm](action-macros/QPsV0qi2zXm7syt6.htm)|Long Jump: Athletics|auto-trad|
|[R03LRl2RBbsm6EcF.htm](action-macros/R03LRl2RBbsm6EcF.htm)|Treat Poison: Medicine|auto-trad|
|[rCgGPEyXbzLFcio6.htm](action-macros/rCgGPEyXbzLFcio6.htm)|Gather Information: Diplomacy|auto-trad|
|[RjfPFjqPrNve6eeh.htm](action-macros/RjfPFjqPrNve6eeh.htm)|Feint: Deception|auto-trad|
|[RZyfkw1DiqVy3JUC.htm](action-macros/RZyfkw1DiqVy3JUC.htm)|Decipher Writing: Occultism|auto-trad|
|[sDUERv4E88G5BRPr.htm](action-macros/sDUERv4E88G5BRPr.htm)|Decipher Writing: Religion|auto-trad|
|[T2QNEoRojMWEec4a.htm](action-macros/T2QNEoRojMWEec4a.htm)|Disable Device: Thievery|auto-trad|
|[tbveXG4gaIoKnsWX.htm](action-macros/tbveXG4gaIoKnsWX.htm)|Request: Diplomacy|auto-trad|
|[tikhJ2b6AMh7wQU7.htm](action-macros/tikhJ2b6AMh7wQU7.htm)|Seek: Perception|auto-trad|
|[TIlUkCzviYxdVk4E.htm](action-macros/TIlUkCzviYxdVk4E.htm)|Swim: Athletics|auto-trad|
|[Tu7LIRelQsiOuo1l.htm](action-macros/Tu7LIRelQsiOuo1l.htm)|Craft: Crafting|auto-trad|
|[U6WjxFPn4fUqIrfl.htm](action-macros/U6WjxFPn4fUqIrfl.htm)|Decipher Writing: Arcana|auto-trad|
|[UKHPveLpG7hUs4D0.htm](action-macros/UKHPveLpG7hUs4D0.htm)|Squeeze: Acrobatics|auto-trad|
|[UoRM6ZMhhhjz1vu8.htm](action-macros/UoRM6ZMhhhjz1vu8.htm)|Perform - Play percussion instrument: Performance|auto-trad|
|[v3dlDjFlOmT5T2gC.htm](action-macros/v3dlDjFlOmT5T2gC.htm)|High Jump: Athletics|auto-trad|
|[vmoO2VasZyTgs3PH.htm](action-macros/vmoO2VasZyTgs3PH.htm)|Perform - Acting: Performance|auto-trad|
|[VTg4t8kYTvXcHROq.htm](action-macros/VTg4t8kYTvXcHROq.htm)|Lie: Deception|auto-trad|
|[xcrdOOiN0l6O1sIn.htm](action-macros/xcrdOOiN0l6O1sIn.htm)|Command an Animal: Nature|auto-trad|
|[yMTKMnaYSGtDz4wk.htm](action-macros/yMTKMnaYSGtDz4wk.htm)|Force Open: Athletics|auto-trad|
|[yNry1xMZqdWHncbV.htm](action-macros/yNry1xMZqdWHncbV.htm)|Shove: Athletics|auto-trad|
|[YWAvvDXpdW1fYPFo.htm](action-macros/YWAvvDXpdW1fYPFo.htm)|Decipher Writing: Society|auto-trad|
|[zkqh01BoXDVgydzo.htm](action-macros/zkqh01BoXDVgydzo.htm)|Subsist: Survival|auto-trad|
|[ZS5z1TqckWPIMGDN.htm](action-macros/ZS5z1TqckWPIMGDN.htm)|Athletics: Arcane Slam|auto-trad|
|[zUJ0UhuoFt5a7tiN.htm](action-macros/zUJ0UhuoFt5a7tiN.htm)|Create a Diversion - Gesture: Deception|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
