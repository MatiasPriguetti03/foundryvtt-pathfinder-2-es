# Estado de la traducción (equipment)

 * **auto-trad**: 3922
 * **vacía**: 4
 * **ninguna**: 75
 * **modificada**: 231
 * **oficial**: 20


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[armor-00-GvAGrcl14Ywcgm2I.htm](equipment/armor-00-GvAGrcl14Ywcgm2I.htm)|Armored Cloak|
|[backpack-04-6z7VUJanx7nPwhk6.htm](equipment/backpack-04-6z7VUJanx7nPwhk6.htm)|Smuggler's Sack (Type I)|
|[backpack-07-eZ2kfpjboRrFWK9R.htm](equipment/backpack-07-eZ2kfpjboRrFWK9R.htm)|Smuggler's Sack (Type II)|
|[backpack-11-fPOidyARnS0McvHx.htm](equipment/backpack-11-fPOidyARnS0McvHx.htm)|Smuggler's Sack (Type III)|
|[backpack-13-jEnDLdcuOgXDf0I6.htm](equipment/backpack-13-jEnDLdcuOgXDf0I6.htm)|Smuggler's Sack (Type IV)|
|[backpack-17-JL0j0AtJZYEaKeOa.htm](equipment/backpack-17-JL0j0AtJZYEaKeOa.htm)|Smuggler's Sack (Type V)|
|[consumable-00-useyu9PIrr7QXUUh.htm](equipment/consumable-00-useyu9PIrr7QXUUh.htm)|Spikes|
|[consumable-03-5iNNOlIqvN1zb8v7.htm](equipment/consumable-03-5iNNOlIqvN1zb8v7.htm)|Clinging Bubbles (Lesser)|
|[consumable-07-MLbE6FZGXKcoTB52.htm](equipment/consumable-07-MLbE6FZGXKcoTB52.htm)|Clinging Bubbles (Moderate)|
|[consumable-08-KsAygNRyR2FdwuOm.htm](equipment/consumable-08-KsAygNRyR2FdwuOm.htm)|Psychic Colors Elixir|
|[consumable-08-tB1NYErrB94dTFuh.htm](equipment/consumable-08-tB1NYErrB94dTFuh.htm)|Charm of the Ordinary|
|[consumable-09-CfM2q0UgtG6gkPku.htm](equipment/consumable-09-CfM2q0UgtG6gkPku.htm)|Horned Lion Amulet|
|[consumable-09-Ja0KlgXV8DsOEkif.htm](equipment/consumable-09-Ja0KlgXV8DsOEkif.htm)|Oil of Corpse Restoration|
|[consumable-10-IzniYBi8QYxHnffr.htm](equipment/consumable-10-IzniYBi8QYxHnffr.htm)|Vat-Grown Brain|
|[consumable-10-LDiF0GRYAiW2mool.htm](equipment/consumable-10-LDiF0GRYAiW2mool.htm)|Star of Cynosure|
|[consumable-11-haDkOWAZClCQA6Yq.htm](equipment/consumable-11-haDkOWAZClCQA6Yq.htm)|Clinging Bubbles (Greater)|
|[equipment-00-e0hrybnm5FBr28Su.htm](equipment/equipment-00-e0hrybnm5FBr28Su.htm)|Concealment Coin|
|[equipment-01-j42OKMXPsp6Mg7nY.htm](equipment/equipment-01-j42OKMXPsp6Mg7nY.htm)|Subtle Armor|
|[equipment-01-SBQRbWKjGNkNJeG5.htm](equipment/equipment-01-SBQRbWKjGNkNJeG5.htm)|Poison Ring|
|[equipment-01-tSAL09zFgPtlK4Dn.htm](equipment/equipment-01-tSAL09zFgPtlK4Dn.htm)|Eyecatcher|
|[equipment-02-5TX0wX3VtMqriZSu.htm](equipment/equipment-02-5TX0wX3VtMqriZSu.htm)|Quick-Change Outfit|
|[equipment-02-fEc4v3EiddH2v0kd.htm](equipment/equipment-02-fEc4v3EiddH2v0kd.htm)|Wrist Grappler|
|[equipment-02-mmQT6AZjXO1ty3dv.htm](equipment/equipment-02-mmQT6AZjXO1ty3dv.htm)|Stone of Encouragement|
|[equipment-03-kRBYoE6j2QUXQJGS.htm](equipment/equipment-03-kRBYoE6j2QUXQJGS.htm)|Wrist Grappler (Clockwork)|
|[equipment-03-SECMe8QKVl45qUie.htm](equipment/equipment-03-SECMe8QKVl45qUie.htm)|Parchment of Direct Message|
|[equipment-03-V5oTXRa6Jk8KXyXl.htm](equipment/equipment-03-V5oTXRa6Jk8KXyXl.htm)|Anylength Rope (Lesser)|
|[equipment-03-vGQ7jbZvYpsEO28r.htm](equipment/equipment-03-vGQ7jbZvYpsEO28r.htm)|Scroll Belt|
|[equipment-03-VLtEtfIggBKXuy7S.htm](equipment/equipment-03-VLtEtfIggBKXuy7S.htm)|Rappelling Kit|
|[equipment-03-WNlkRCPzc8YGQOAp.htm](equipment/equipment-03-WNlkRCPzc8YGQOAp.htm)|Ring of Observation (Lesser)|
|[equipment-04-wRZGJohTF0FFJBrq.htm](equipment/equipment-04-wRZGJohTF0FFJBrq.htm)|Diver's Gloves (Lesser)|
|[equipment-05-0T7JuFupBTfDBiHv.htm](equipment/equipment-05-0T7JuFupBTfDBiHv.htm)|Boots of Free Running (Lesser)|
|[equipment-05-HM0uhpM9t97MAABn.htm](equipment/equipment-05-HM0uhpM9t97MAABn.htm)|Cape of Illumination (Lesser)|
|[equipment-05-l4FxEma0PrvUPeMe.htm](equipment/equipment-05-l4FxEma0PrvUPeMe.htm)|Anylength Rope (Moderate)|
|[equipment-05-V3FxGX0Yy8c7ucjH.htm](equipment/equipment-05-V3FxGX0Yy8c7ucjH.htm)|Pipe of Dancing Smoke|
|[equipment-07-5rpNqGkPSMkXKv0B.htm](equipment/equipment-07-5rpNqGkPSMkXKv0B.htm)|Hallajin Key|
|[equipment-07-EVq8BRD0HO63S1GF.htm](equipment/equipment-07-EVq8BRD0HO63S1GF.htm)|Anylength Rope (Greater)|
|[equipment-07-geXmZMgU7kVbLHIj.htm](equipment/equipment-07-geXmZMgU7kVbLHIj.htm)|Ring of Observation (Moderate)|
|[equipment-08-JOswp4pSSevZFeNG.htm](equipment/equipment-08-JOswp4pSSevZFeNG.htm)|Stone of Encouragement (Greater)|
|[equipment-09-6QS9p7IWfRoplIwk.htm](equipment/equipment-09-6QS9p7IWfRoplIwk.htm)|Cape of Grand Entrances|
|[equipment-09-MlrVt5CaZ85PvEm9.htm](equipment/equipment-09-MlrVt5CaZ85PvEm9.htm)|Diver's Gloves (Moderate)|
|[equipment-09-TQsV76vrzj6WCqem.htm](equipment/equipment-09-TQsV76vrzj6WCqem.htm)|Mindsponge|
|[equipment-09-vsTtpwdYANxP4EPZ.htm](equipment/equipment-09-vsTtpwdYANxP4EPZ.htm)|Cape of Illumination (Moderate)|
|[equipment-10-24o5lk8aOcSrsas0.htm](equipment/equipment-10-24o5lk8aOcSrsas0.htm)|Rime Jar|
|[equipment-10-mO9VXq8UkQodAucJ.htm](equipment/equipment-10-mO9VXq8UkQodAucJ.htm)|Witch Token|
|[equipment-10-wc1T0d1mGSwJallY.htm](equipment/equipment-10-wc1T0d1mGSwJallY.htm)|Ring of Observation (Greater)|
|[equipment-11-5PmuusQRWQBmexGy.htm](equipment/equipment-11-5PmuusQRWQBmexGy.htm)|Ice Forge|
|[equipment-11-kcRQT67sLes1cC72.htm](equipment/equipment-11-kcRQT67sLes1cC72.htm)|Boots of Free Running (Moderate)|
|[equipment-12-KwqN8qNqp3AFwFbn.htm](equipment/equipment-12-KwqN8qNqp3AFwFbn.htm)|Telekinetic Converters|
|[equipment-15-3Q8nG1JW4FMMJ4l7.htm](equipment/equipment-15-3Q8nG1JW4FMMJ4l7.htm)|Cape of Illumination (Greater)|
|[equipment-15-XXz3Gt5IUb8QdnBS.htm](equipment/equipment-15-XXz3Gt5IUb8QdnBS.htm)|Diver's Gloves (Greater)|
|[equipment-17-NQGVqxIWaa2fize3.htm](equipment/equipment-17-NQGVqxIWaa2fize3.htm)|Boots of Free Running (Greater)|
|[equipment-20-2HByGOXi57zbP5VP.htm](equipment/equipment-20-2HByGOXi57zbP5VP.htm)|The Juggler|
|[equipment-20-cCnpBUjaGSNCT3JK.htm](equipment/equipment-20-cCnpBUjaGSNCT3JK.htm)|The Silent Hag|
|[equipment-20-dB5oALF9UzMdE0MD.htm](equipment/equipment-20-dB5oALF9UzMdE0MD.htm)|The Mountain Man|
|[equipment-20-DKQrQkx13z6yiUNp.htm](equipment/equipment-20-DKQrQkx13z6yiUNp.htm)|The Liar|
|[equipment-20-FiCTRh8rXOXlKxQB.htm](equipment/equipment-20-FiCTRh8rXOXlKxQB.htm)|The Carnival|
|[equipment-20-FposU3NmF0IbrOdz.htm](equipment/equipment-20-FposU3NmF0IbrOdz.htm)|The Brass Dwarf|
|[equipment-20-HaakvCTFbCnwFHVY.htm](equipment/equipment-20-HaakvCTFbCnwFHVY.htm)|The Vision|
|[equipment-20-IfOwz6UoG7pnfbur.htm](equipment/equipment-20-IfOwz6UoG7pnfbur.htm)|The Fiend|
|[equipment-20-jwEAO4hCOKrM6i6c.htm](equipment/equipment-20-jwEAO4hCOKrM6i6c.htm)|The Big Sky|
|[equipment-20-O1VdFSf81VZQg428.htm](equipment/equipment-20-O1VdFSf81VZQg428.htm)|The Trumpet|
|[equipment-20-owR8clECtufA8q9h.htm](equipment/equipment-20-owR8clECtufA8q9h.htm)|The Empty Throne|
|[equipment-20-Qh65GD5oXOhO5y3k.htm](equipment/equipment-20-Qh65GD5oXOhO5y3k.htm)|The Theater|
|[equipment-20-RCtvSgzBCsyaztOj.htm](equipment/equipment-20-RCtvSgzBCsyaztOj.htm)|The Snakebite|
|[equipment-20-W2yfqo9Oy8zyoOrA.htm](equipment/equipment-20-W2yfqo9Oy8zyoOrA.htm)|The Rakshasa|
|[equipment-20-XDiBtW7A9lelBGeY.htm](equipment/equipment-20-XDiBtW7A9lelBGeY.htm)|The Rabbit Prince|
|[equipment-20-XyoYrGEAhJ3iCahe.htm](equipment/equipment-20-XyoYrGEAhJ3iCahe.htm)|The Publican|
|[equipment-20-yHaZJDX7xe8gPdL6.htm](equipment/equipment-20-yHaZJDX7xe8gPdL6.htm)|The Locksmith|
|[equipment-20-ZcLscdldjI8xJnC7.htm](equipment/equipment-20-ZcLscdldjI8xJnC7.htm)|The Paladin|
|[weapon-00-HaquLIE0SFc85vWY.htm](equipment/weapon-00-HaquLIE0SFc85vWY.htm)|Shield Pistol|
|[weapon-00-tsvqlHxWfLwLQorQ.htm](equipment/weapon-00-tsvqlHxWfLwLQorQ.htm)|Wrist Launcher|
|[weapon-04-JMu3VUwhQu1SLSFd.htm](equipment/weapon-04-JMu3VUwhQu1SLSFd.htm)|Aether Marbles (Lesser)|
|[weapon-12-67pe75mLVekaVqZH.htm](equipment/weapon-12-67pe75mLVekaVqZH.htm)|Aether Marbles (Moderate)|
|[weapon-12-tWoW1BeFrWm35hmV.htm](equipment/weapon-12-tWoW1BeFrWm35hmV.htm)|Blink Blade|
|[weapon-18-5XImTvAN58GhnNIE.htm](equipment/weapon-18-5XImTvAN58GhnNIE.htm)|Aether Marbles (Greater)|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[armor-00-1k3AsSW7lpU0kEpY.htm](equipment/armor-00-1k3AsSW7lpU0kEpY.htm)|Buckler|auto-trad|
|[armor-00-2RrKSkj27NDREOKQ.htm](equipment/armor-00-2RrKSkj27NDREOKQ.htm)|Gauntlet Buckler|auto-trad|
|[armor-00-3O1Iv7ztIPKhKWwe.htm](equipment/armor-00-3O1Iv7ztIPKhKWwe.htm)|Sankeit|auto-trad|
|[armor-00-3RUL5Oief3hdyUNU.htm](equipment/armor-00-3RUL5Oief3hdyUNU.htm)|Harnessed Shield|auto-trad|
|[armor-00-3ZfGYXKfBjHOZO7d.htm](equipment/armor-00-3ZfGYXKfBjHOZO7d.htm)|Lamellar Breastplate|auto-trad|
|[armor-00-4tIVTg9wj56RrveA.htm](equipment/armor-00-4tIVTg9wj56RrveA.htm)|Leather Armor|auto-trad|
|[armor-00-56CTZheeNhNPpLo1.htm](equipment/armor-00-56CTZheeNhNPpLo1.htm)|Subterfuge Suit|auto-trad|
|[armor-00-7GEV9zi2bz1iSdaW.htm](equipment/armor-00-7GEV9zi2bz1iSdaW.htm)|Heavy Rondache|auto-trad|
|[armor-00-7xcLPvLDqOcnj0qs.htm](equipment/armor-00-7xcLPvLDqOcnj0qs.htm)|Quilted Armor|auto-trad|
|[armor-00-9WcvCnh6SbIw5Cfw.htm](equipment/armor-00-9WcvCnh6SbIw5Cfw.htm)|Caster's Targe|auto-trad|
|[armor-00-AnwzlOs0njF9Jqnr.htm](equipment/armor-00-AnwzlOs0njF9Jqnr.htm)|Hide Armor|auto-trad|
|[armor-00-bZHqPBdQjPS02Rd4.htm](equipment/armor-00-bZHqPBdQjPS02Rd4.htm)|Coral Armor|auto-trad|
|[armor-00-dDIPA1WE9ESF67EB.htm](equipment/armor-00-dDIPA1WE9ESF67EB.htm)|Clothing (Explorer's)|auto-trad|
|[armor-00-emh9kvqSJtxTX8He.htm](equipment/armor-00-emh9kvqSJtxTX8He.htm)|Buckle Armor|auto-trad|
|[armor-00-ewQZ0VeL38v3qFnN.htm](equipment/armor-00-ewQZ0VeL38v3qFnN.htm)|Studded Leather Armor|auto-trad|
|[armor-00-ezVp13Uw8cWW08Da.htm](equipment/armor-00-ezVp13Uw8cWW08Da.htm)|Wooden Shield|auto-trad|
|[armor-00-hcoBRMF0zIA986Of.htm](equipment/armor-00-hcoBRMF0zIA986Of.htm)|Razor Disc|auto-trad|
|[armor-00-Hg7btiITMu6Zf2EU.htm](equipment/armor-00-Hg7btiITMu6Zf2EU.htm)|Ceramic Plate|auto-trad|
|[armor-00-I8b3oNLl5MIMrG4r.htm](equipment/armor-00-I8b3oNLl5MIMrG4r.htm)|Niyaháat|auto-trad|
|[armor-00-IyBL38UtL4MDN6b5.htm](equipment/armor-00-IyBL38UtL4MDN6b5.htm)|Lattice Armor|auto-trad|
|[armor-00-Kf4eJEXnFPuAsseP.htm](equipment/armor-00-Kf4eJEXnFPuAsseP.htm)|Chain Mail|auto-trad|
|[armor-00-kZYyXUX8dVXnAIVp.htm](equipment/armor-00-kZYyXUX8dVXnAIVp.htm)|Salvo Shield|auto-trad|
|[armor-00-L3qye4mR9I0fU1M3.htm](equipment/armor-00-L3qye4mR9I0fU1M3.htm)|Hide Shield|auto-trad|
|[armor-00-lbAZmrRnV7OZvuol.htm](equipment/armor-00-lbAZmrRnV7OZvuol.htm)|Swordstealer Shield|auto-trad|
|[armor-00-Li0kDAfCuw7DAiSM.htm](equipment/armor-00-Li0kDAfCuw7DAiSM.htm)|Klar|auto-trad|
|[armor-00-ltundBNFAnP7bgPr.htm](equipment/armor-00-ltundBNFAnP7bgPr.htm)|Tower Shield|auto-trad|
|[armor-00-lV1BKms3SxTvsn3N.htm](equipment/armor-00-lV1BKms3SxTvsn3N.htm)|Dart Shield|auto-trad|
|[armor-00-mkMWda6ivlhnXq4d.htm](equipment/armor-00-mkMWda6ivlhnXq4d.htm)|Armored Coat|auto-trad|
|[armor-00-MPcM4Wt6KmWE2kGL.htm](equipment/armor-00-MPcM4Wt6KmWE2kGL.htm)|Chain Shirt|auto-trad|
|[armor-00-N42lmp3Ft6EsSvzg.htm](equipment/armor-00-N42lmp3Ft6EsSvzg.htm)|Power Suit|auto-trad|
|[armor-00-QPgkjNm2uIV5tsRd.htm](equipment/armor-00-QPgkjNm2uIV5tsRd.htm)|Wooden Breastplate|auto-trad|
|[armor-00-r0ifJfoz8aqf0mwk.htm](equipment/armor-00-r0ifJfoz8aqf0mwk.htm)|Breastplate|auto-trad|
|[armor-00-U5IGgD7Z225OPnhK.htm](equipment/armor-00-U5IGgD7Z225OPnhK.htm)|Reinforced Chassis|auto-trad|
|[armor-00-VJ4s6ABlkimSyjjZ.htm](equipment/armor-00-VJ4s6ABlkimSyjjZ.htm)|Scroll Robes|auto-trad|
|[armor-00-wx7WY6YGvgMphDL1.htm](equipment/armor-00-wx7WY6YGvgMphDL1.htm)|Leather Lamellar|auto-trad|
|[armor-00-XnD9zqlwRFFK1ltc.htm](equipment/armor-00-XnD9zqlwRFFK1ltc.htm)|Meteor Shield|auto-trad|
|[armor-00-yiU4osCo6ql28r6M.htm](equipment/armor-00-yiU4osCo6ql28r6M.htm)|Gi|auto-trad|
|[armor-00-YMQr577asquZIP65.htm](equipment/armor-00-YMQr577asquZIP65.htm)|Scale Mail|auto-trad|
|[armor-00-Yr9yCuJiAlFh3QEB.htm](equipment/armor-00-Yr9yCuJiAlFh3QEB.htm)|Steel Shield|auto-trad|
|[armor-00-zBYEU9E7034ENCmh.htm](equipment/armor-00-zBYEU9E7034ENCmh.htm)|Padded Armor|auto-trad|
|[armor-00-zVSBjb6o5RudKA9c.htm](equipment/armor-00-zVSBjb6o5RudKA9c.htm)|Leaf Weave|auto-trad|
|[armor-01-0KHgAaDi3tmu32Hq.htm](equipment/armor-01-0KHgAaDi3tmu32Hq.htm)|Rite of Reinforcement Exoskeleton|auto-trad|
|[armor-01-6AhDKX1dwRwFpQsU.htm](equipment/armor-01-6AhDKX1dwRwFpQsU.htm)|Splint Mail|auto-trad|
|[armor-01-7cy9gLlKX2vNja0a.htm](equipment/armor-01-7cy9gLlKX2vNja0a.htm)|Tough Skin|auto-trad|
|[armor-01-8WVYOughRWLb7kGF.htm](equipment/armor-01-8WVYOughRWLb7kGF.htm)|Mantis Shell|auto-trad|
|[armor-01-KA0Ku5qOQfXqw3BK.htm](equipment/armor-01-KA0Ku5qOQfXqw3BK.htm)|Titan Nagaji Scales|auto-trad|
|[armor-01-LF7HSNnlGxLJyDZS.htm](equipment/armor-01-LF7HSNnlGxLJyDZS.htm)|Hellknight Breastplate|auto-trad|
|[armor-01-O6cnU1Uiss81C4bG.htm](equipment/armor-01-O6cnU1Uiss81C4bG.htm)|Swarmsuit|auto-trad|
|[armor-01-pRoikbRo5HFW6YUB.htm](equipment/armor-01-pRoikbRo5HFW6YUB.htm)|Half Plate|auto-trad|
|[armor-01-tugYMolpupNz05cE.htm](equipment/armor-01-tugYMolpupNz05cE.htm)|Fortress Shield|auto-trad|
|[armor-01-U6XqhSFUUowHnnkk.htm](equipment/armor-01-U6XqhSFUUowHnnkk.htm)|Hellknight Half Plate|auto-trad|
|[armor-02-1xUIdz23mIlYWGPL.htm](equipment/armor-02-1xUIdz23mIlYWGPL.htm)|Silver Shield (Low-Grade)|auto-trad|
|[armor-02-5E6l3RheSyl99G3m.htm](equipment/armor-02-5E6l3RheSyl99G3m.htm)|Cold Iron Shield (Low-Grade)|auto-trad|
|[armor-02-aA9clnIP3deHNNjo.htm](equipment/armor-02-aA9clnIP3deHNNjo.htm)|Silver Buckler (Low-Grade)|auto-trad|
|[armor-02-ckUxr51wJVGRNAD0.htm](equipment/armor-02-ckUxr51wJVGRNAD0.htm)|Cold Iron Buckler (Low-Grade)|auto-trad|
|[armor-02-FSDEuHDr1I4JVIJg.htm](equipment/armor-02-FSDEuHDr1I4JVIJg.htm)|Fortress Plate|auto-trad|
|[armor-02-Gq1cZWSKOtJhKd2p.htm](equipment/armor-02-Gq1cZWSKOtJhKd2p.htm)|Full Plate|auto-trad|
|[armor-02-IxuDS3POB6EH8TVN.htm](equipment/armor-02-IxuDS3POB6EH8TVN.htm)|Glamorous Buckler|auto-trad|
|[armor-02-lCgXeV52eYYl5cUX.htm](equipment/armor-02-lCgXeV52eYYl5cUX.htm)|Hellknight Plate|auto-trad|
|[armor-02-M6b20zZTGNWrx5oM.htm](equipment/armor-02-M6b20zZTGNWrx5oM.htm)|Gray Maiden Plate|auto-trad|
|[armor-02-oGVtymwknrJS8gWW.htm](equipment/armor-02-oGVtymwknrJS8gWW.htm)|Bastion Plate|auto-trad|
|[armor-02-U2liOd8kJasy0Ghd.htm](equipment/armor-02-U2liOd8kJasy0Ghd.htm)|O-Yoroi|auto-trad|
|[armor-03-w0SXrrYSH1EL088n.htm](equipment/armor-03-w0SXrrYSH1EL088n.htm)|Sapling Shield (Minor)|auto-trad|
|[armor-04-7jolv0cuttvjI1JD.htm](equipment/armor-04-7jolv0cuttvjI1JD.htm)|Wovenwood Shield (Minor)|auto-trad|
|[armor-04-f9ygr5Cjrmop8LWV.htm](equipment/armor-04-f9ygr5Cjrmop8LWV.htm)|Sturdy Shield (Minor)|auto-trad|
|[armor-04-ScU1DrBKn0ONHnq4.htm](equipment/armor-04-ScU1DrBKn0ONHnq4.htm)|Pillow Shield|auto-trad|
|[armor-05-3ZBFkXQC7jgZ8kx9.htm](equipment/armor-05-3ZBFkXQC7jgZ8kx9.htm)|Ooze Skin|auto-trad|
|[armor-05-BPZPA9Y8kPewVYoW.htm](equipment/armor-05-BPZPA9Y8kPewVYoW.htm)|Magnetic Shield|auto-trad|
|[armor-05-HsfoHX1B6ocflitz.htm](equipment/armor-05-HsfoHX1B6ocflitz.htm)|Crushing Coils|auto-trad|
|[armor-05-slQRh4FVDzP8h1wj.htm](equipment/armor-05-slQRh4FVDzP8h1wj.htm)|Exploding Shield|auto-trad|
|[armor-05-uPy4iagJMWRi1vNd.htm](equipment/armor-05-uPy4iagJMWRi1vNd.htm)|Living Leaf Weave|auto-trad|
|[armor-05-WDmuP3H7jNqobraf.htm](equipment/armor-05-WDmuP3H7jNqobraf.htm)|Helmsman's Recourse|auto-trad|
|[armor-06-EHMRryMB3s6XdkRz.htm](equipment/armor-06-EHMRryMB3s6XdkRz.htm)|Clockwork Diving Suit|auto-trad|
|[armor-06-eMDt5vCQztp7cC6B.htm](equipment/armor-06-eMDt5vCQztp7cC6B.htm)|Lion's Shield|auto-trad|
|[armor-06-hRwnNTMj7wa8S4Ji.htm](equipment/armor-06-hRwnNTMj7wa8S4Ji.htm)|Ghoul Hide|auto-trad|
|[armor-06-jau3C5ao3vm2gt8c.htm](equipment/armor-06-jau3C5ao3vm2gt8c.htm)|Clockwork Disguise|auto-trad|
|[armor-06-nS4ev4BZn9OUPYgt.htm](equipment/armor-06-nS4ev4BZn9OUPYgt.htm)|Sapling Shield (Lesser)|auto-trad|
|[armor-06-OqDAx4HJ39ojVtvg.htm](equipment/armor-06-OqDAx4HJ39ojVtvg.htm)|Spellguard Shield|auto-trad|
|[armor-06-tzFMetc4VK6goDm2.htm](equipment/armor-06-tzFMetc4VK6goDm2.htm)|Trollhound Vest|auto-trad|
|[armor-06-VyQZVv31q3CjCAQz.htm](equipment/armor-06-VyQZVv31q3CjCAQz.htm)|Sarkorian God-Caller Garb|auto-trad|
|[armor-06-WOwVBsiwLQxnIJRV.htm](equipment/armor-06-WOwVBsiwLQxnIJRV.htm)|Devil's Bargain|auto-trad|
|[armor-07-19T3MHwhB6Wk4AjV.htm](equipment/armor-07-19T3MHwhB6Wk4AjV.htm)|Helmsman's Recourse (Greater)|auto-trad|
|[armor-07-3mmS3Y291h6Ckgin.htm](equipment/armor-07-3mmS3Y291h6Ckgin.htm)|Powered Full Plate|auto-trad|
|[armor-07-3uHlJFZFY7unq1EQ.htm](equipment/armor-07-3uHlJFZFY7unq1EQ.htm)|Inubrix Buckler (Standard Grade)|auto-trad|
|[armor-07-8DNpWWeL7X9MDG0i.htm](equipment/armor-07-8DNpWWeL7X9MDG0i.htm)|Silver Buckler (Standard-Grade)|auto-trad|
|[armor-07-EBcZ7ftWs8lVW7Fj.htm](equipment/armor-07-EBcZ7ftWs8lVW7Fj.htm)|Staff-Storing Shield|auto-trad|
|[armor-07-HWQEGY2X0QXj2zfN.htm](equipment/armor-07-HWQEGY2X0QXj2zfN.htm)|Martyr's Shield|auto-trad|
|[armor-07-JDgFHv334yCrBY38.htm](equipment/armor-07-JDgFHv334yCrBY38.htm)|Blade Byrnie|auto-trad|
|[armor-07-JG7p7yI12cAPuJ8X.htm](equipment/armor-07-JG7p7yI12cAPuJ8X.htm)|Arachnid Harness|auto-trad|
|[armor-07-K3cR9k4sZHNO4a12.htm](equipment/armor-07-K3cR9k4sZHNO4a12.htm)|Inubrix Shield (Standard Grade)|auto-trad|
|[armor-07-KVj9RP2qvpsHHGqE.htm](equipment/armor-07-KVj9RP2qvpsHHGqE.htm)|Silver Shield (Standard-Grade)|auto-trad|
|[armor-07-LZAwOTKk3dKwsGDd.htm](equipment/armor-07-LZAwOTKk3dKwsGDd.htm)|Wovenwood Shield (Lesser)|auto-trad|
|[armor-07-nDZX25OwoN0Imrq6.htm](equipment/armor-07-nDZX25OwoN0Imrq6.htm)|Sturdy Shield (Lesser)|auto-trad|
|[armor-07-p1uYtIUYXXoNdVKg.htm](equipment/armor-07-p1uYtIUYXXoNdVKg.htm)|Cold Iron Shield (Standard-Grade)|auto-trad|
|[armor-07-PsyfqGIzDbr1mX6d.htm](equipment/armor-07-PsyfqGIzDbr1mX6d.htm)|Cold Iron Buckler (Standard-Grade)|auto-trad|
|[armor-07-tBGhH7AHyvJQZC5d.htm](equipment/armor-07-tBGhH7AHyvJQZC5d.htm)|Wolfjaw Armor|auto-trad|
|[armor-07-vHZSx5f093Wrivzn.htm](equipment/armor-07-vHZSx5f093Wrivzn.htm)|Moonlit Chain|auto-trad|
|[armor-07-WDh4fb9N86mNLfDV.htm](equipment/armor-07-WDh4fb9N86mNLfDV.htm)|Spined Shield|auto-trad|
|[armor-08-2gMlhHlkQp3DuWw5.htm](equipment/armor-08-2gMlhHlkQp3DuWw5.htm)|Adamantine Shield (Standard-Grade)|auto-trad|
|[armor-08-3ytZSLqXQdNCuV4l.htm](equipment/armor-08-3ytZSLqXQdNCuV4l.htm)|Swarmsuit (Impenetrable)|auto-trad|
|[armor-08-4aVkppixLHPg940o.htm](equipment/armor-08-4aVkppixLHPg940o.htm)|Adamantine Buckler (Standard-Grade)|auto-trad|
|[armor-08-5494MLdHHZI5GONx.htm](equipment/armor-08-5494MLdHHZI5GONx.htm)|Abysium Shield (Standard Grade)|auto-trad|
|[armor-08-BUHV5rev5HVgCrUB.htm](equipment/armor-08-BUHV5rev5HVgCrUB.htm)|Darkwood Buckler (Standard-Grade)|auto-trad|
|[armor-08-CF9BCVE0ppxMmULb.htm](equipment/armor-08-CF9BCVE0ppxMmULb.htm)|Mithral Buckler (Standard-Grade)|auto-trad|
|[armor-08-daX1jX7XYo3mWZmP.htm](equipment/armor-08-daX1jX7XYo3mWZmP.htm)|Dragonhide Shield (Standard-Grade)|auto-trad|
|[armor-08-ETHbpwjh8aLGrXi0.htm](equipment/armor-08-ETHbpwjh8aLGrXi0.htm)|Mithral Shield (Standard-Grade)|auto-trad|
|[armor-08-hSSusxBfMUvNTYxt.htm](equipment/armor-08-hSSusxBfMUvNTYxt.htm)|Abysium Buckler (Standard Grade)|auto-trad|
|[armor-08-icLG1NjjTowYWg7N.htm](equipment/armor-08-icLG1NjjTowYWg7N.htm)|Siccatite Buckler (Standard Grade)|auto-trad|
|[armor-08-mHHvzxHtlLbkbStG.htm](equipment/armor-08-mHHvzxHtlLbkbStG.htm)|Containment Contraption|auto-trad|
|[armor-08-rh4T08EsddrwfHcs.htm](equipment/armor-08-rh4T08EsddrwfHcs.htm)|Duelist's Beacon|auto-trad|
|[armor-08-Snu8yjMrUi3ockyA.htm](equipment/armor-08-Snu8yjMrUi3ockyA.htm)|Darkwood Tower Shield (Standard-Grade)|auto-trad|
|[armor-08-UdvSJPCtBr2Y1e22.htm](equipment/armor-08-UdvSJPCtBr2Y1e22.htm)|Darkwood Shield (Standard-Grade)|auto-trad|
|[armor-08-uV8fEVUuSyMsh1xF.htm](equipment/armor-08-uV8fEVUuSyMsh1xF.htm)|Wasp Guard|auto-trad|
|[armor-08-vfzzrp0tKXznWnhY.htm](equipment/armor-08-vfzzrp0tKXznWnhY.htm)|Dragonhide Buckler (Standard-Grade)|auto-trad|
|[armor-08-voMus7iKOUKYr2KC.htm](equipment/armor-08-voMus7iKOUKYr2KC.htm)|Siccatite Shield (Standard Grade)|auto-trad|
|[armor-08-Wfu6R7y4w9iTZcDs.htm](equipment/armor-08-Wfu6R7y4w9iTZcDs.htm)|Rusting Carapace|auto-trad|
|[armor-09-3yeRZNUF4qZzCzHp.htm](equipment/armor-09-3yeRZNUF4qZzCzHp.htm)|Sapling Shield (Moderate)|auto-trad|
|[armor-09-Blp9Ha1YeIJOeFls.htm](equipment/armor-09-Blp9Ha1YeIJOeFls.htm)|Spangled Rider's Suit|auto-trad|
|[armor-09-C4XKMcHZoGzrAZBl.htm](equipment/armor-09-C4XKMcHZoGzrAZBl.htm)|Rhino Hide|auto-trad|
|[armor-09-DIzZr0K20eCbNzQo.htm](equipment/armor-09-DIzZr0K20eCbNzQo.htm)|Force Shield|auto-trad|
|[armor-09-g5TZguADBpi6hDnv.htm](equipment/armor-09-g5TZguADBpi6hDnv.htm)|Djezet Buckler (Standard Grade)|auto-trad|
|[armor-09-gawhJLVsVsUFAPd6.htm](equipment/armor-09-gawhJLVsVsUFAPd6.htm)|Cloister Robe (Lesser)|auto-trad|
|[armor-09-ip2KNbzryjauXdKC.htm](equipment/armor-09-ip2KNbzryjauXdKC.htm)|Djezet Shield (Standard Grade)|auto-trad|
|[armor-09-jAKYIqG2BNO7yffJ.htm](equipment/armor-09-jAKYIqG2BNO7yffJ.htm)|Leopard's Armor|auto-trad|
|[armor-09-kFe3JyiO27YBboJy.htm](equipment/armor-09-kFe3JyiO27YBboJy.htm)|Sanguine Klar|auto-trad|
|[armor-09-NIyJqiW1Wci6gP9m.htm](equipment/armor-09-NIyJqiW1Wci6gP9m.htm)|Arachnid Harness (Greater)|auto-trad|
|[armor-09-nVlKU2et4EeReayr.htm](equipment/armor-09-nVlKU2et4EeReayr.htm)|Victory Plate|auto-trad|
|[armor-09-Q8BscHUFiM1a86PO.htm](equipment/armor-09-Q8BscHUFiM1a86PO.htm)|Dragonslayer's Shield|auto-trad|
|[armor-09-SS1lwhzx0hne1Ljh.htm](equipment/armor-09-SS1lwhzx0hne1Ljh.htm)|Library Robes|auto-trad|
|[armor-09-TxQvgyLziolSVfqY.htm](equipment/armor-09-TxQvgyLziolSVfqY.htm)|Warding Escutcheon|auto-trad|
|[armor-09-u9BdrpYhJDUPQFCF.htm](equipment/armor-09-u9BdrpYhJDUPQFCF.htm)|Dragon Turtle Plate|auto-trad|
|[armor-09-uVmE0wsLw9tMv7hB.htm](equipment/armor-09-uVmE0wsLw9tMv7hB.htm)|Turnabout Shield|auto-trad|
|[armor-10-7qBpWdPZ0X8HoH9X.htm](equipment/armor-10-7qBpWdPZ0X8HoH9X.htm)|Bone Dreadnought Plate|auto-trad|
|[armor-10-8HxM35X8DDt2gw9d.htm](equipment/armor-10-8HxM35X8DDt2gw9d.htm)|Electric Eelskin|auto-trad|
|[armor-10-b8bVEp4qOorBDsAF.htm](equipment/armor-10-b8bVEp4qOorBDsAF.htm)|Scarab Cuirass|auto-trad|
|[armor-10-GDSclGF1yk0sw86V.htm](equipment/armor-10-GDSclGF1yk0sw86V.htm)|Numerian Steel Breastplate|auto-trad|
|[armor-10-M8QYa10fHOyViC5V.htm](equipment/armor-10-M8QYa10fHOyViC5V.htm)|Shining Shield|auto-trad|
|[armor-10-NoTUhKkiTY5BQU5T.htm](equipment/armor-10-NoTUhKkiTY5BQU5T.htm)|Forge Warden|auto-trad|
|[armor-10-nRLMg1NB2OSmzSqX.htm](equipment/armor-10-nRLMg1NB2OSmzSqX.htm)|Wovenwood Shield (Moderate)|auto-trad|
|[armor-10-oE6tZBBbP59DOWFM.htm](equipment/armor-10-oE6tZBBbP59DOWFM.htm)|Remorhaz Armor|auto-trad|
|[armor-10-pNQJ9PTOEHxEZCgp.htm](equipment/armor-10-pNQJ9PTOEHxEZCgp.htm)|Sturdy Shield (Moderate)|auto-trad|
|[armor-10-qZUo5SmGxp5BlqFN.htm](equipment/armor-10-qZUo5SmGxp5BlqFN.htm)|Barbed Vest|auto-trad|
|[armor-10-RUEAV5LMUGFHcXcW.htm](equipment/armor-10-RUEAV5LMUGFHcXcW.htm)|Breastplate of Command|auto-trad|
|[armor-10-TPiF3YBADh10JDPV.htm](equipment/armor-10-TPiF3YBADh10JDPV.htm)|Amaranthine Pavise|auto-trad|
|[armor-11-9ZGkQo739t9utj37.htm](equipment/armor-11-9ZGkQo739t9utj37.htm)|Arrow-Catching Shield|auto-trad|
|[armor-11-aBAU0sTkFLuTxO3X.htm](equipment/armor-11-aBAU0sTkFLuTxO3X.htm)|Staff-Storing Shield (Greater)|auto-trad|
|[armor-11-hY6Qn7HwKxYaGk2S.htm](equipment/armor-11-hY6Qn7HwKxYaGk2S.htm)|Guardian Shield|auto-trad|
|[armor-11-kSjfrC31qFi2qovr.htm](equipment/armor-11-kSjfrC31qFi2qovr.htm)|Helmsman's Recourse (Major)|auto-trad|
|[armor-11-S5CMsB7AyYC8iSa0.htm](equipment/armor-11-S5CMsB7AyYC8iSa0.htm)|Clockwork Shield|auto-trad|
|[armor-11-ydNocKN6afzIyVh6.htm](equipment/armor-11-ydNocKN6afzIyVh6.htm)|Floating Shield|auto-trad|
|[armor-12-63k6WWZstoY33LsH.htm](equipment/armor-12-63k6WWZstoY33LsH.htm)|Rampart Shield|auto-trad|
|[armor-12-HoL83ca2TqEnnErs.htm](equipment/armor-12-HoL83ca2TqEnnErs.htm)|Library Robes (Greater)|auto-trad|
|[armor-12-IAPztxiK2T5mC2Y5.htm](equipment/armor-12-IAPztxiK2T5mC2Y5.htm)|Bastion of the Inheritor|auto-trad|
|[armor-12-l3BVdNdtv3486bOk.htm](equipment/armor-12-l3BVdNdtv3486bOk.htm)|Autumn's Embrace|auto-trad|
|[armor-12-QR7rqHMuU72NA47A.htm](equipment/armor-12-QR7rqHMuU72NA47A.htm)|Sapling Shield (Greater)|auto-trad|
|[armor-12-uuBhzN6xrplrwKyI.htm](equipment/armor-12-uuBhzN6xrplrwKyI.htm)|Reef Heart|auto-trad|
|[armor-12-VMksM7bLAZW9LxH4.htm](equipment/armor-12-VMksM7bLAZW9LxH4.htm)|Cloister Robe (Moderate)|auto-trad|
|[armor-13-2uHcTZ40oZ62R9gy.htm](equipment/armor-13-2uHcTZ40oZ62R9gy.htm)|Celestial Armor|auto-trad|
|[armor-13-AdJgj7vVvNGvPIEj.htm](equipment/armor-13-AdJgj7vVvNGvPIEj.htm)|Mail of Luck|auto-trad|
|[armor-13-AqVJOBjXbeao5An7.htm](equipment/armor-13-AqVJOBjXbeao5An7.htm)|Energizing Lattice|auto-trad|
|[armor-13-BK1AKKi1gDMn5uBs.htm](equipment/armor-13-BK1AKKi1gDMn5uBs.htm)|Invisible Chain Shirt|auto-trad|
|[armor-13-Fr75q2wqO9AFG53z.htm](equipment/armor-13-Fr75q2wqO9AFG53z.htm)|Wovenwood Shield (Greater)|auto-trad|
|[armor-13-GAu6v14pCSgLJh2D.htm](equipment/armor-13-GAu6v14pCSgLJh2D.htm)|Noxious Jerkin|auto-trad|
|[armor-13-hYBZK1kaGPeR85CH.htm](equipment/armor-13-hYBZK1kaGPeR85CH.htm)|Demon Armor|auto-trad|
|[armor-13-iCJrRoIsjdf665yE.htm](equipment/armor-13-iCJrRoIsjdf665yE.htm)|Psychic Brigandine|auto-trad|
|[armor-13-n4kPBFzVh5LcJAMA.htm](equipment/armor-13-n4kPBFzVh5LcJAMA.htm)|Harmonic Hauberk|auto-trad|
|[armor-13-nzQKwdUxzmmwWtzT.htm](equipment/armor-13-nzQKwdUxzmmwWtzT.htm)|Medusa's Scream|auto-trad|
|[armor-13-OlGFuOcqy0bSdtTl.htm](equipment/armor-13-OlGFuOcqy0bSdtTl.htm)|Suit of Armoire|auto-trad|
|[armor-13-peAvz7u35GEfTXxp.htm](equipment/armor-13-peAvz7u35GEfTXxp.htm)|Elven Chain (Standard-Grade)|auto-trad|
|[armor-13-qdUlmO7XfBWQU3p5.htm](equipment/armor-13-qdUlmO7XfBWQU3p5.htm)|Blade Byrnie (Greater)|auto-trad|
|[armor-13-rrnWORxT2Ch4pUFb.htm](equipment/armor-13-rrnWORxT2Ch4pUFb.htm)|Sturdy Shield (Greater)|auto-trad|
|[armor-13-WgxgGLp3q9Et6BwC.htm](equipment/armor-13-WgxgGLp3q9Et6BwC.htm)|Faerie Queen's Bower|auto-trad|
|[armor-13-zPSImy0ENQjnRJNs.htm](equipment/armor-13-zPSImy0ENQjnRJNs.htm)|Forgotten Shell|auto-trad|
|[armor-14-bu4IrYBeE68KeEOW.htm](equipment/armor-14-bu4IrYBeE68KeEOW.htm)|Shared-Pain Sankeit|auto-trad|
|[armor-14-Iic9QpNemaumfkHc.htm](equipment/armor-14-Iic9QpNemaumfkHc.htm)|Medusa Armor|auto-trad|
|[armor-14-tLNsmY90aN668PMG.htm](equipment/armor-14-tLNsmY90aN668PMG.htm)|Glorious Plate|auto-trad|
|[armor-14-vkPNIDJPt2RHpS6T.htm](equipment/armor-14-vkPNIDJPt2RHpS6T.htm)|Sanguine Klar (Greater)|auto-trad|
|[armor-15-1X0E707FWf9jJgPK.htm](equipment/armor-15-1X0E707FWf9jJgPK.htm)|Troll Hide|auto-trad|
|[armor-15-aPD0z9dBsHqgiCW0.htm](equipment/armor-15-aPD0z9dBsHqgiCW0.htm)|Reforging Shield|auto-trad|
|[armor-15-cShYYreFruLxEVfA.htm](equipment/armor-15-cShYYreFruLxEVfA.htm)|Inubrix Buckler (High Grade)|auto-trad|
|[armor-15-ed2F0L3R6UNSFUmS.htm](equipment/armor-15-ed2F0L3R6UNSFUmS.htm)|Victory Plate (Greater)|auto-trad|
|[armor-15-gp5kgCySEOuntQPF.htm](equipment/armor-15-gp5kgCySEOuntQPF.htm)|Plate Armor of the Deep|auto-trad|
|[armor-15-kCjvkkuI3JP4TQ0t.htm](equipment/armor-15-kCjvkkuI3JP4TQ0t.htm)|Cold Iron Buckler (High-Grade)|auto-trad|
|[armor-15-LboYDYZ0IbDWuWMl.htm](equipment/armor-15-LboYDYZ0IbDWuWMl.htm)|Silver Buckler (High-Grade)|auto-trad|
|[armor-15-nhCHLI5SnJXF3fQE.htm](equipment/armor-15-nhCHLI5SnJXF3fQE.htm)|Staff-Storing Shield (Major)|auto-trad|
|[armor-15-nRmGErgTLZYf6WMD.htm](equipment/armor-15-nRmGErgTLZYf6WMD.htm)|Cold Iron Shield (High-Grade)|auto-trad|
|[armor-15-PGvmiE4elUC4ehMe.htm](equipment/armor-15-PGvmiE4elUC4ehMe.htm)|Inubrix Shield (High Grade)|auto-trad|
|[armor-15-pRzafUKQviDSMRJp.htm](equipment/armor-15-pRzafUKQviDSMRJp.htm)|Silver Shield (High-Grade)|auto-trad|
|[armor-15-Qm9mmqMNKBaMn5so.htm](equipment/armor-15-Qm9mmqMNKBaMn5so.htm)|Sapling Shield (Major)|auto-trad|
|[armor-15-RODC2TYO4cUg6fXu.htm](equipment/armor-15-RODC2TYO4cUg6fXu.htm)|Reef Heart (Greater)|auto-trad|
|[armor-15-RtJvTDFNPhJSPmWP.htm](equipment/armor-15-RtJvTDFNPhJSPmWP.htm)|Library Robes (Major)|auto-trad|
|[armor-15-SJaFNXiHGILhwEMk.htm](equipment/armor-15-SJaFNXiHGILhwEMk.htm)|Silkspinner's Shield|auto-trad|
|[armor-15-TJsmB2aHk9Yogqup.htm](equipment/armor-15-TJsmB2aHk9Yogqup.htm)|Fungal Armor|auto-trad|
|[armor-15-Wkh5rQx3IqInEKdT.htm](equipment/armor-15-Wkh5rQx3IqInEKdT.htm)|Robe of the Archmagi|auto-trad|
|[armor-15-XPIDpF7dP6VyGKOM.htm](equipment/armor-15-XPIDpF7dP6VyGKOM.htm)|Cloister Robe (Greater)|auto-trad|
|[armor-16-3xsQ1AA4dyMHLxpw.htm](equipment/armor-16-3xsQ1AA4dyMHLxpw.htm)|Dragonplate|auto-trad|
|[armor-16-5dBSn9brsT9NISoR.htm](equipment/armor-16-5dBSn9brsT9NISoR.htm)|Mithral Shield (High-Grade)|auto-trad|
|[armor-16-8gcuJ2Ev3euFyXnS.htm](equipment/armor-16-8gcuJ2Ev3euFyXnS.htm)|Darkwood Tower Shield (High-Grade)|auto-trad|
|[armor-16-aA2DR1BcxqJT15Cr.htm](equipment/armor-16-aA2DR1BcxqJT15Cr.htm)|Abysium Shield (High Grade)|auto-trad|
|[armor-16-AbYyx61j1K4F27tC.htm](equipment/armor-16-AbYyx61j1K4F27tC.htm)|Adamantine Buckler (High-Grade)|auto-trad|
|[armor-16-BWQzaHbGVqlBuMww.htm](equipment/armor-16-BWQzaHbGVqlBuMww.htm)|Sturdy Shield (Major)|auto-trad|
|[armor-16-DUTKrSu5iJMktfZI.htm](equipment/armor-16-DUTKrSu5iJMktfZI.htm)|Black Hole Armor|auto-trad|
|[armor-16-e9Xiei2wduOEGI5r.htm](equipment/armor-16-e9Xiei2wduOEGI5r.htm)|Wovenwood Shield (Major)|auto-trad|
|[armor-16-EKXRigmEZgqmFA62.htm](equipment/armor-16-EKXRigmEZgqmFA62.htm)|Nethysian Bulwark|auto-trad|
|[armor-16-iUKXCYr8fHCjphyf.htm](equipment/armor-16-iUKXCYr8fHCjphyf.htm)|Djezet Buckler (High Grade)|auto-trad|
|[armor-16-lhodTRc1mqKXuID4.htm](equipment/armor-16-lhodTRc1mqKXuID4.htm)|Siccatite Shield (High Grade)|auto-trad|
|[armor-16-LVixPROPOwIfHgEK.htm](equipment/armor-16-LVixPROPOwIfHgEK.htm)|Dragonhide Shield (High-Grade)|auto-trad|
|[armor-16-nLhb0w0DbtUC6weH.htm](equipment/armor-16-nLhb0w0DbtUC6weH.htm)|Armor of the Holy Warrior|auto-trad|
|[armor-16-PGOWN2ZU21yviSMA.htm](equipment/armor-16-PGOWN2ZU21yviSMA.htm)|Siccatite Buckler (High Grade)|auto-trad|
|[armor-16-POmxAltjkridB252.htm](equipment/armor-16-POmxAltjkridB252.htm)|Abysium Buckler (High Grade)|auto-trad|
|[armor-16-R1I1MEKwDYtCfZr3.htm](equipment/armor-16-R1I1MEKwDYtCfZr3.htm)|Dragonhide Buckler (High-Grade)|auto-trad|
|[armor-16-rA1hULhhqOP2u4Ze.htm](equipment/armor-16-rA1hULhhqOP2u4Ze.htm)|Adamantine Shield (High-Grade)|auto-trad|
|[armor-16-sRHR4cEo8WnowjU3.htm](equipment/armor-16-sRHR4cEo8WnowjU3.htm)|Darkwood Buckler (High-Grade)|auto-trad|
|[armor-16-TyW1XOWYMM2xHGaI.htm](equipment/armor-16-TyW1XOWYMM2xHGaI.htm)|Medusa's Scream (Greater)|auto-trad|
|[armor-16-uAs0hUUgROrndceD.htm](equipment/armor-16-uAs0hUUgROrndceD.htm)|Floating Shield (Greater)|auto-trad|
|[armor-16-V4kbHwiODxwJOJga.htm](equipment/armor-16-V4kbHwiODxwJOJga.htm)|Darkwood Shield (High-Grade)|auto-trad|
|[armor-16-Ywp05dNOBnfr40KG.htm](equipment/armor-16-Ywp05dNOBnfr40KG.htm)|Djezet Shield (High Grade)|auto-trad|
|[armor-16-ZZrCiVcoKinA5wyu.htm](equipment/armor-16-ZZrCiVcoKinA5wyu.htm)|Mithral Buckler (High-Grade)|auto-trad|
|[armor-17-630xJ5zceXFXiTi2.htm](equipment/armor-17-630xJ5zceXFXiTi2.htm)|Prismatic Plate|auto-trad|
|[armor-17-aLA7pikfeNIAAGLw.htm](equipment/armor-17-aLA7pikfeNIAAGLw.htm)|Impenetrable Scale|auto-trad|
|[armor-17-COsToVFlA36UmkB7.htm](equipment/armor-17-COsToVFlA36UmkB7.htm)|Jerkin of Liberation|auto-trad|
|[armor-17-m03NrKvfKcXrCXke.htm](equipment/armor-17-m03NrKvfKcXrCXke.htm)|Noqual Buckler (High Grade)|auto-trad|
|[armor-17-OKo8ub6D11ztZc2V.htm](equipment/armor-17-OKo8ub6D11ztZc2V.htm)|Orichalcum Buckler (High-Grade)|auto-trad|
|[armor-17-PeS3J9r4ss7gNytK.htm](equipment/armor-17-PeS3J9r4ss7gNytK.htm)|Orichalcum Shield (High-Grade)|auto-trad|
|[armor-17-VFkMAtJvtWclDNx3.htm](equipment/armor-17-VFkMAtJvtWclDNx3.htm)|Noqual Shield (High Grade)|auto-trad|
|[armor-17-VioxW97SUADl3zTt.htm](equipment/armor-17-VioxW97SUADl3zTt.htm)|Cursebreak Bulwark|auto-trad|
|[armor-18-ayST5rFSIKy2ynYk.htm](equipment/armor-18-ayST5rFSIKy2ynYk.htm)|Clockwork Shield (Greater)|auto-trad|
|[armor-18-La9qYc5NHsg423Jb.htm](equipment/armor-18-La9qYc5NHsg423Jb.htm)|Reflecting Shield|auto-trad|
|[armor-18-SUbYk6B1iPoGyyjh.htm](equipment/armor-18-SUbYk6B1iPoGyyjh.htm)|Indestructible Shield|auto-trad|
|[armor-18-VJiKPxewJNLhimEi.htm](equipment/armor-18-VJiKPxewJNLhimEi.htm)|Sapling Shield (True)|auto-trad|
|[armor-18-XKON66YXYLXlGPPg.htm](equipment/armor-18-XKON66YXYLXlGPPg.htm)|Breastplate of Command (Greater)|auto-trad|
|[armor-19-7ynjS9llFg7tPMoj.htm](equipment/armor-19-7ynjS9llFg7tPMoj.htm)|Robe of the Archmagi (Greater)|auto-trad|
|[armor-19-7Z8XXGiUiyyisKOD.htm](equipment/armor-19-7Z8XXGiUiyyisKOD.htm)|Sturdy Shield (Supreme)|auto-trad|
|[armor-19-f6antZJS94JyijFl.htm](equipment/armor-19-f6antZJS94JyijFl.htm)|Kraken's Guard|auto-trad|
|[armor-19-fHkVbTtjhITsE2NJ.htm](equipment/armor-19-fHkVbTtjhITsE2NJ.htm)|Blade Byrnie (Major)|auto-trad|
|[armor-19-mhCLoihjfhoqpiVp.htm](equipment/armor-19-mhCLoihjfhoqpiVp.htm)|Cloister Robe (Major)|auto-trad|
|[armor-19-o4U9Nt7ac7AlolNQ.htm](equipment/armor-19-o4U9Nt7ac7AlolNQ.htm)|Ouroboros Buckles|auto-trad|
|[armor-19-puPx1rmbhjNM2lcz.htm](equipment/armor-19-puPx1rmbhjNM2lcz.htm)|Starfall Shield|auto-trad|
|[armor-19-qXzP7yH72fxAChNU.htm](equipment/armor-19-qXzP7yH72fxAChNU.htm)|Wovenwood Shield (True)|auto-trad|
|[armor-19-TBmDARyj67joVe4m.htm](equipment/armor-19-TBmDARyj67joVe4m.htm)|Library Robes (True)|auto-trad|
|[armor-20-FKNgrFDIRBumr6P5.htm](equipment/armor-20-FKNgrFDIRBumr6P5.htm)|Rebounding Breastplate|auto-trad|
|[armor-20-JU6gJSmibl5Q3k6F.htm](equipment/armor-20-JU6gJSmibl5Q3k6F.htm)|Breastplate of the Mountain|auto-trad|
|[armor-20-K5IWk98rsEMyJ1Xb.htm](equipment/armor-20-K5IWk98rsEMyJ1Xb.htm)|Warding Escutcheon (Greater)|auto-trad|
|[armor-20-PlYr5AhYwHIztMw2.htm](equipment/armor-20-PlYr5AhYwHIztMw2.htm)|Shield of the Unified Legion|auto-trad|
|[armor-20-PuqdH7DmNsN79HyE.htm](equipment/armor-20-PuqdH7DmNsN79HyE.htm)|Elven Chain (High-Grade)|auto-trad|
|[armor-20-qSqx72639EJTzBam.htm](equipment/armor-20-qSqx72639EJTzBam.htm)|Immortal Bastion|auto-trad|
|[armor-20-xAOVPVH6cX3iVFlk.htm](equipment/armor-20-xAOVPVH6cX3iVFlk.htm)|Staff-Storing Shield (True)|auto-trad|
|[armor-21-XkIfwSTy0PQL3RPJ.htm](equipment/armor-21-XkIfwSTy0PQL3RPJ.htm)|Scale of Igroon|auto-trad|
|[armor-26-yNteshMHb0xIXFlC.htm](equipment/armor-26-yNteshMHb0xIXFlC.htm)|Ancestral Embrace|auto-trad|
|[backpack-00-3lgwjrFEsQVKzhh7.htm](equipment/backpack-00-3lgwjrFEsQVKzhh7.htm)|Backpack|auto-trad|
|[backpack-00-DujblC14ytJEZMaz.htm](equipment/backpack-00-DujblC14ytJEZMaz.htm)|Sack|auto-trad|
|[backpack-00-eFqKVKrf62XOGWUw.htm](equipment/backpack-00-eFqKVKrf62XOGWUw.htm)|Belt Pouch|auto-trad|
|[backpack-00-HamOU17sqb5ljiB5.htm](equipment/backpack-00-HamOU17sqb5ljiB5.htm)|Bandolier|auto-trad|
|[backpack-00-HgS4FYX0itxQvwub.htm](equipment/backpack-00-HgS4FYX0itxQvwub.htm)|Chair Storage|auto-trad|
|[backpack-00-mBziC8v2eOtTs0f5.htm](equipment/backpack-00-mBziC8v2eOtTs0f5.htm)|Saddlebags|auto-trad|
|[backpack-00-OdVDPN9vIpu3Zud3.htm](equipment/backpack-00-OdVDPN9vIpu3Zud3.htm)|Chest|auto-trad|
|[backpack-00-tYBW8TBF8WOAp3x0.htm](equipment/backpack-00-tYBW8TBF8WOAp3x0.htm)|Satchel|auto-trad|
|[backpack-02-KxUJu1IHY9FxxAGV.htm](equipment/backpack-02-KxUJu1IHY9FxxAGV.htm)|Sturdy Satchel|auto-trad|
|[backpack-03-6uHwCTK8dX1reraz.htm](equipment/backpack-03-6uHwCTK8dX1reraz.htm)|Immaculate Holsters|auto-trad|
|[backpack-03-vLsoZKRzYQV6FQve.htm](equipment/backpack-03-vLsoZKRzYQV6FQve.htm)|Gunner's Bandolier|auto-trad|
|[backpack-04-1pglC9PQx6yOgcKL.htm](equipment/backpack-04-1pglC9PQx6yOgcKL.htm)|Sleeves of Storage|auto-trad|
|[backpack-04-jaEEvuQ32GjAa8jy.htm](equipment/backpack-04-jaEEvuQ32GjAa8jy.htm)|Bag of Holding (Type I)|auto-trad|
|[backpack-04-W5znRDeklmWEGzFY.htm](equipment/backpack-04-W5znRDeklmWEGzFY.htm)|Bag of Weasels|auto-trad|
|[backpack-06-0c7zLY9c88K2n0GC.htm](equipment/backpack-06-0c7zLY9c88K2n0GC.htm)|Pathfinder's Pouch|auto-trad|
|[backpack-06-NBmOTmE2GAEI766h.htm](equipment/backpack-06-NBmOTmE2GAEI766h.htm)|Misdirecting Haversack|auto-trad|
|[backpack-07-JBMBaN9dZLytfFLQ.htm](equipment/backpack-07-JBMBaN9dZLytfFLQ.htm)|Bag of Holding (Type II)|auto-trad|
|[backpack-09-3hv6NVC2rVu4QCNt.htm](equipment/backpack-09-3hv6NVC2rVu4QCNt.htm)|Sleeves of Storage (Greater)|auto-trad|
|[backpack-09-rdF2RKgFK0vOlaeV.htm](equipment/backpack-09-rdF2RKgFK0vOlaeV.htm)|Knapsack of Halflingkind|auto-trad|
|[backpack-09-yUPUxRqyYdZaPhkF.htm](equipment/backpack-09-yUPUxRqyYdZaPhkF.htm)|Lucky Draw Bandolier|auto-trad|
|[backpack-11-jkb2WNby4mjcYqq9.htm](equipment/backpack-11-jkb2WNby4mjcYqq9.htm)|Bag of Holding (Type III)|auto-trad|
|[backpack-12-iAfqKpHyJ6beLGjB.htm](equipment/backpack-12-iAfqKpHyJ6beLGjB.htm)|Lucky Draw Bandolier (Greater)|auto-trad|
|[backpack-13-34DA4rFy7bduRAld.htm](equipment/backpack-13-34DA4rFy7bduRAld.htm)|Bag of Holding (Type IV)|auto-trad|
|[backpack-13-9cK0QVSjNd8tQtiQ.htm](equipment/backpack-13-9cK0QVSjNd8tQtiQ.htm)|Knapsack of Halflingkind (Greater)|auto-trad|
|[backpack-13-k5y89wYu3NX1AhVI.htm](equipment/backpack-13-k5y89wYu3NX1AhVI.htm)|Bomber's Saddle|auto-trad|
|[backpack-15-DtEhaF5fZcqgcQ3H.htm](equipment/backpack-15-DtEhaF5fZcqgcQ3H.htm)|Mother Maw|auto-trad|
|[backpack-15-ulgsKBzOXJbjqnmW.htm](equipment/backpack-15-ulgsKBzOXJbjqnmW.htm)|Portable Hole|auto-trad|
|[backpack-17-LRSIRUERqBAJ1HGT.htm](equipment/backpack-17-LRSIRUERqBAJ1HGT.htm)|Voyager's Pack|auto-trad|
|[backpack-20-HEvuhktgeFXdlDKm.htm](equipment/backpack-20-HEvuhktgeFXdlDKm.htm)|Extradimensional Stash|auto-trad|
|[consumable-00-11b6MN3UQ8XKqAer.htm](equipment/consumable-00-11b6MN3UQ8XKqAer.htm)|Rounds (Three Peaked Tree)|auto-trad|
|[consumable-00-154C222Y7M96nvrL.htm](equipment/consumable-00-154C222Y7M96nvrL.htm)|Rounds (Dragon Mouth Pistol)|auto-trad|
|[consumable-00-1UWYVwWVnp8Vng5t.htm](equipment/consumable-00-1UWYVwWVnp8Vng5t.htm)|Backpack Catapult Stone|auto-trad|
|[consumable-00-20h1dSg7IVJ5NxNa.htm](equipment/consumable-00-20h1dSg7IVJ5NxNa.htm)|Rounds (Pepperbox)|auto-trad|
|[consumable-00-2ayp6yyQDr3iliBp.htm](equipment/consumable-00-2ayp6yyQDr3iliBp.htm)|Rounds (Fire Lance)|auto-trad|
|[consumable-00-2DoUHoueAyyP4lMN.htm](equipment/consumable-00-2DoUHoueAyyP4lMN.htm)|Bloodeye Coffee|auto-trad|
|[consumable-00-2VS3S2E3dbqvGifH.htm](equipment/consumable-00-2VS3S2E3dbqvGifH.htm)|Fake Blood Pack|auto-trad|
|[consumable-00-3nDKWIydNEfhJ8XJ.htm](equipment/consumable-00-3nDKWIydNEfhJ8XJ.htm)|Treat (Unique)|auto-trad|
|[consumable-00-3QGZS6l7NrX4auyR.htm](equipment/consumable-00-3QGZS6l7NrX4auyR.htm)|Rounds (Flintlock Pistol)|auto-trad|
|[consumable-00-58fs0VsG71A9gyEk.htm](equipment/consumable-00-58fs0VsG71A9gyEk.htm)|Rounds (Clan Pistol)|auto-trad|
|[consumable-00-9w8MrBhFtvM7RrYN.htm](equipment/consumable-00-9w8MrBhFtvM7RrYN.htm)|Magazine with 5 Bolts|auto-trad|
|[consumable-00-AITVZmakiu3RgfKo.htm](equipment/consumable-00-AITVZmakiu3RgfKo.htm)|Bolts|auto-trad|
|[consumable-00-AOOQJfSkTPiSzfB9.htm](equipment/consumable-00-AOOQJfSkTPiSzfB9.htm)|Black Powder (Dose or Round)|auto-trad|
|[consumable-00-AqslI71DReZOzgAW.htm](equipment/consumable-00-AqslI71DReZOzgAW.htm)|Formulated Sunlight|auto-trad|
|[consumable-00-BZCzkDWQP3TU1Bbp.htm](equipment/consumable-00-BZCzkDWQP3TU1Bbp.htm)|Rounds (Harmona Gun)|auto-trad|
|[consumable-00-ckxYpfX3jTcZZOAz.htm](equipment/consumable-00-ckxYpfX3jTcZZOAz.htm)|Rounds (Gun Sword)|auto-trad|
|[consumable-00-CmWkSgFyb6HB9KVK.htm](equipment/consumable-00-CmWkSgFyb6HB9KVK.htm)|Rounds (Hand Cannon)|auto-trad|
|[consumable-00-DKsfWwWZ23PHPIa1.htm](equipment/consumable-00-DKsfWwWZ23PHPIa1.htm)|Sun Shot|auto-trad|
|[consumable-00-Dlsv1n0Bk1zVJaJM.htm](equipment/consumable-00-Dlsv1n0Bk1zVJaJM.htm)|Repeating Crossbow Magazine|auto-trad|
|[consumable-00-dSZz8B96D20IWKZ5.htm](equipment/consumable-00-dSZz8B96D20IWKZ5.htm)|Rounds (Blunderbuss)|auto-trad|
|[consumable-00-Du05UxCyCM0ZWH8z.htm](equipment/consumable-00-Du05UxCyCM0ZWH8z.htm)|Oil (1 pint)|auto-trad|
|[consumable-00-eCRZcG7ZxFRP8bpx.htm](equipment/consumable-00-eCRZcG7ZxFRP8bpx.htm)|Rounds (Jezail)|auto-trad|
|[consumable-00-F5JDZSMYYKYfyeEE.htm](equipment/consumable-00-F5JDZSMYYKYfyeEE.htm)|Repeating Heavy Crossbow Magazine|auto-trad|
|[consumable-00-FCoziiYnBoRJFVaM.htm](equipment/consumable-00-FCoziiYnBoRJFVaM.htm)|Rounds (Dagger Pistol)|auto-trad|
|[consumable-00-FKPuzveKRNJhA1mi.htm](equipment/consumable-00-FKPuzveKRNJhA1mi.htm)|Rounds (Dueling Pistol)|auto-trad|
|[consumable-00-Fy63rwYVBq0hXh0T.htm](equipment/consumable-00-Fy63rwYVBq0hXh0T.htm)|Rounds (Flintlock Musket)|auto-trad|
|[consumable-00-g190hQLpwU0jrSNa.htm](equipment/consumable-00-g190hQLpwU0jrSNa.htm)|Feed (Standard)|auto-trad|
|[consumable-00-GWTSMcXocmUsNLmN.htm](equipment/consumable-00-GWTSMcXocmUsNLmN.htm)|Poison Sedum|auto-trad|
|[consumable-00-GyzinRH2ZSHANAFc.htm](equipment/consumable-00-GyzinRH2ZSHANAFc.htm)|Rounds (Hammer Gun)|auto-trad|
|[consumable-00-h7jTMfYGQau1PBBz.htm](equipment/consumable-00-h7jTMfYGQau1PBBz.htm)|Rounds (Double-Barreled Pistol)|auto-trad|
|[consumable-00-HUgVrYvdy9SkqRZ9.htm](equipment/consumable-00-HUgVrYvdy9SkqRZ9.htm)|Rounds (Dwarven Scattergun)|auto-trad|
|[consumable-00-hXZrMJPlw1UvVzjC.htm](equipment/consumable-00-hXZrMJPlw1UvVzjC.htm)|Magazine with 8 Pellets|auto-trad|
|[consumable-00-IAeGh5qvu7ob4yad.htm](equipment/consumable-00-IAeGh5qvu7ob4yad.htm)|Grappling Arrow|auto-trad|
|[consumable-00-JPGOrHeYxjdnUogT.htm](equipment/consumable-00-JPGOrHeYxjdnUogT.htm)|Qat|auto-trad|
|[consumable-00-KODdKPhwSQ9t4LKx.htm](equipment/consumable-00-KODdKPhwSQ9t4LKx.htm)|Rounds (Black Powder Knuckle Dusters)|auto-trad|
|[consumable-00-L2df9pyWLFNgjfNh.htm](equipment/consumable-00-L2df9pyWLFNgjfNh.htm)|Rounds (Slide Pistol)|auto-trad|
|[consumable-00-L9ZV076913otGtiB.htm](equipment/consumable-00-L9ZV076913otGtiB.htm)|Rations|auto-trad|
|[consumable-00-lHyguikeoZlFlYdx.htm](equipment/consumable-00-lHyguikeoZlFlYdx.htm)|Rounds (Arquebus)|auto-trad|
|[consumable-00-nb83vPkRwm47cu3z.htm](equipment/consumable-00-nb83vPkRwm47cu3z.htm)|Feed (Unique)|auto-trad|
|[consumable-00-NELpewmAIxQXaMtv.htm](equipment/consumable-00-NELpewmAIxQXaMtv.htm)|Backpack Ballista Bolt|auto-trad|
|[consumable-00-NF8dWgLTxj8QRrmg.htm](equipment/consumable-00-NF8dWgLTxj8QRrmg.htm)|Treat (Standard)|auto-trad|
|[consumable-00-NiioDSvyhhY4IWVH.htm](equipment/consumable-00-NiioDSvyhhY4IWVH.htm)|Magazine with 6 Pellets|auto-trad|
|[consumable-00-oGKa9TWCrFC2LkBU.htm](equipment/consumable-00-oGKa9TWCrFC2LkBU.htm)|Rounds (Cane Pistol)|auto-trad|
|[consumable-00-oQr0x30JoLrWxQdA.htm](equipment/consumable-00-oQr0x30JoLrWxQdA.htm)|Rounds (Rapier Pistol)|auto-trad|
|[consumable-00-oX2Ra68qKcixAxbm.htm](equipment/consumable-00-oX2Ra68qKcixAxbm.htm)|Rounds (Coat Pistol)|auto-trad|
|[consumable-00-P5RvQfgQNCgxpdPU.htm](equipment/consumable-00-P5RvQfgQNCgxpdPU.htm)|Rounds (Double-Barreled Musket)|auto-trad|
|[consumable-00-pErNGR7u7SJDq1o0.htm](equipment/consumable-00-pErNGR7u7SJDq1o0.htm)|Practice Target|auto-trad|
|[consumable-00-PTwm2NAcReBw2iUY.htm](equipment/consumable-00-PTwm2NAcReBw2iUY.htm)|Flayleaf|auto-trad|
|[consumable-00-qaAQnuLVia6vS1LU.htm](equipment/consumable-00-qaAQnuLVia6vS1LU.htm)|Spray Pellets|auto-trad|
|[consumable-00-RQlZFVXv4LOxGyyn.htm](equipment/consumable-00-RQlZFVXv4LOxGyyn.htm)|Rounds (Flingflenser)|auto-trad|
|[consumable-00-sAI6yzB11LlxisD7.htm](equipment/consumable-00-sAI6yzB11LlxisD7.htm)|Grappling Bolt|auto-trad|
|[consumable-00-sfMHQsOv3W8jFFop.htm](equipment/consumable-00-sfMHQsOv3W8jFFop.htm)|Rounds (Piercing Wind)|auto-trad|
|[consumable-00-Ti4gWILk69LPxKuU.htm](equipment/consumable-00-Ti4gWILk69LPxKuU.htm)|Candle|auto-trad|
|[consumable-00-UMAXLDpI6YLSfYX1.htm](equipment/consumable-00-UMAXLDpI6YLSfYX1.htm)|Alcohol|auto-trad|
|[consumable-00-UZGfofcq560tw43N.htm](equipment/consumable-00-UZGfofcq560tw43N.htm)|Powder|auto-trad|
|[consumable-00-V2CfXehNhCcxApbg.htm](equipment/consumable-00-V2CfXehNhCcxApbg.htm)|Rounds (Mace Multipistol)|auto-trad|
|[consumable-00-V9QG93vXuh8d4sJQ.htm](equipment/consumable-00-V9QG93vXuh8d4sJQ.htm)|Rounds (Gnome Amalgam Musket)|auto-trad|
|[consumable-00-VctYvRnaKXjKlqQ7.htm](equipment/consumable-00-VctYvRnaKXjKlqQ7.htm)|Cutlery|auto-trad|
|[consumable-00-VeeH30MvHyTX4QAT.htm](equipment/consumable-00-VeeH30MvHyTX4QAT.htm)|Rounds (Axe Musket)|auto-trad|
|[consumable-00-w2ENw2VMPcsbif8g.htm](equipment/consumable-00-w2ENw2VMPcsbif8g.htm)|Arrows|auto-trad|
|[consumable-00-xk9bw9ccupAIx7xT.htm](equipment/consumable-00-xk9bw9ccupAIx7xT.htm)|Repeating Hand Crossbow Magazine|auto-trad|
|[consumable-00-XnU7QmGlvBHzxkiJ.htm](equipment/consumable-00-XnU7QmGlvBHzxkiJ.htm)|Rounds (Mithral Tree)|auto-trad|
|[consumable-00-xShIDyydOMkGvGNb.htm](equipment/consumable-00-xShIDyydOMkGvGNb.htm)|Chalk|auto-trad|
|[consumable-00-YrNmw0Rn3MqT0yKP.htm](equipment/consumable-00-YrNmw0Rn3MqT0yKP.htm)|Wooden Taws|auto-trad|
|[consumable-00-zxnBe0wgTAEXXzUU.htm](equipment/consumable-00-zxnBe0wgTAEXXzUU.htm)|Rounds (Explosive Dogslicer)|auto-trad|
|[consumable-01-2kFaQDsMZmPIEOct.htm](equipment/consumable-01-2kFaQDsMZmPIEOct.htm)|Impact Foam Chassis (Lesser)|auto-trad|
|[consumable-01-2RuepCemJhrpKKao.htm](equipment/consumable-01-2RuepCemJhrpKKao.htm)|Healing Potion (Minor)|auto-trad|
|[consumable-01-3cWko20JPnjeoofV.htm](equipment/consumable-01-3cWko20JPnjeoofV.htm)|Feather Token (Ladder)|auto-trad|
|[consumable-01-3V2U720YhW2nyGVx.htm](equipment/consumable-01-3V2U720YhW2nyGVx.htm)|Spike Snare|auto-trad|
|[consumable-01-3Wb0N7iqmTn6e2Xc.htm](equipment/consumable-01-3Wb0N7iqmTn6e2Xc.htm)|Ration Tonic|auto-trad|
|[consumable-01-5MKBwpE401uz4kNN.htm](equipment/consumable-01-5MKBwpE401uz4kNN.htm)|Quicksilver Mutagen (Lesser)|auto-trad|
|[consumable-01-60RuBISKBTuYquBe.htm](equipment/consumable-01-60RuBISKBTuYquBe.htm)|Appetizing Flavor Snare|auto-trad|
|[consumable-01-7e57yUVtRgYLOFwT.htm](equipment/consumable-01-7e57yUVtRgYLOFwT.htm)|Explosive Mine (Lesser)|auto-trad|
|[consumable-01-7tweUS8kQ6N45fEu.htm](equipment/consumable-01-7tweUS8kQ6N45fEu.htm)|Popdust|auto-trad|
|[consumable-01-7V5fKnnvW7DUgbAd.htm](equipment/consumable-01-7V5fKnnvW7DUgbAd.htm)|Animal Repellent (Minor)|auto-trad|
|[consumable-01-7Y2yOr4ltpP2tyuL.htm](equipment/consumable-01-7Y2yOr4ltpP2tyuL.htm)|Eagle Eye Elixir (Lesser)|auto-trad|
|[consumable-01-81XVuTsF1zD6EXmN.htm](equipment/consumable-01-81XVuTsF1zD6EXmN.htm)|Blindpepper Tube|auto-trad|
|[consumable-01-8eTGOQ9P69405jIO.htm](equipment/consumable-01-8eTGOQ9P69405jIO.htm)|Forensic Dye|auto-trad|
|[consumable-01-9EVzmy4dY9LpUgyC.htm](equipment/consumable-01-9EVzmy4dY9LpUgyC.htm)|Silver Tripod|auto-trad|
|[consumable-01-9F3d43xMDCJNIkDo.htm](equipment/consumable-01-9F3d43xMDCJNIkDo.htm)|Choker-Arm Mutagen (Lesser)|auto-trad|
|[consumable-01-AFX1V0Go9DqPWBlN.htm](equipment/consumable-01-AFX1V0Go9DqPWBlN.htm)|Alarm Snare|auto-trad|
|[consumable-01-aP5wvs60j6VaKvWL.htm](equipment/consumable-01-aP5wvs60j6VaKvWL.htm)|Flare Beacon (Lesser)|auto-trad|
|[consumable-01-apGQcVMSRprFEeKt.htm](equipment/consumable-01-apGQcVMSRprFEeKt.htm)|Potion of Acid Retaliation (Minor)|auto-trad|
|[consumable-01-arNOPU7h8nNbnZ1N.htm](equipment/consumable-01-arNOPU7h8nNbnZ1N.htm)|Bane Ammunition (Lesser)|auto-trad|
|[consumable-01-azFpL1NLBJB7xuli.htm](equipment/consumable-01-azFpL1NLBJB7xuli.htm)|Dust Pods|auto-trad|
|[consumable-01-BjCGt377LpCwbtyS.htm](equipment/consumable-01-BjCGt377LpCwbtyS.htm)|Emergency Disguise|auto-trad|
|[consumable-01-BJtpbzjSza4wUlQX.htm](equipment/consumable-01-BJtpbzjSza4wUlQX.htm)|Smoke Screen Snare (Lesser)|auto-trad|
|[consumable-01-bOPQDM54W8ZDoULY.htm](equipment/consumable-01-bOPQDM54W8ZDoULY.htm)|Serene Mutagen (Lesser)|auto-trad|
|[consumable-01-CeKPffF0FaEyhLMp.htm](equipment/consumable-01-CeKPffF0FaEyhLMp.htm)|Spear Frog Poison|auto-trad|
|[consumable-01-clyXfh0aVXgij2Hb.htm](equipment/consumable-01-clyXfh0aVXgij2Hb.htm)|Wolf Fang|auto-trad|
|[consumable-01-dymL1IbxU5zR9fqo.htm](equipment/consumable-01-dymL1IbxU5zR9fqo.htm)|Searing Suture (Lesser)|auto-trad|
|[consumable-01-eHVZBmtzEcnsHSk8.htm](equipment/consumable-01-eHVZBmtzEcnsHSk8.htm)|Skeptic's Elixir (Lesser)|auto-trad|
|[consumable-01-ev3F9qlMNlNdCOAI.htm](equipment/consumable-01-ev3F9qlMNlNdCOAI.htm)|Runestone|auto-trad|
|[consumable-01-EWujUs3YmlBu2jhm.htm](equipment/consumable-01-EWujUs3YmlBu2jhm.htm)|Shielding Salve|auto-trad|
|[consumable-01-fTQ5e4utVfgtXV1e.htm](equipment/consumable-01-fTQ5e4utVfgtXV1e.htm)|Oil of Unlife (Minor)|auto-trad|
|[consumable-01-g91dodJKrkQ2gCA6.htm](equipment/consumable-01-g91dodJKrkQ2gCA6.htm)|Self-Immolating Note|auto-trad|
|[consumable-01-GS4YvQieBS11JNYR.htm](equipment/consumable-01-GS4YvQieBS11JNYR.htm)|Drakeheart Mutagen (Lesser)|auto-trad|
|[consumable-01-H2BqcfujumZVWF69.htm](equipment/consumable-01-H2BqcfujumZVWF69.htm)|Slippery Ribbon|auto-trad|
|[consumable-01-H3KAt7cHJ6ZLTJQE.htm](equipment/consumable-01-H3KAt7cHJ6ZLTJQE.htm)|War Blood Mutagen (Lesser)|auto-trad|
|[consumable-01-hd8D1Dm6aVhMYpEL.htm](equipment/consumable-01-hd8D1Dm6aVhMYpEL.htm)|Sparkler|auto-trad|
|[consumable-01-hDLbR56Id2OtU318.htm](equipment/consumable-01-hDLbR56Id2OtU318.htm)|Elixir of Life (Minor)|auto-trad|
|[consumable-01-hGw7Q1y2IEXRGAfv.htm](equipment/consumable-01-hGw7Q1y2IEXRGAfv.htm)|Gecko Potion|auto-trad|
|[consumable-01-I2ZbXsfm0c9Eep4I.htm](equipment/consumable-01-I2ZbXsfm0c9Eep4I.htm)|Grolna|auto-trad|
|[consumable-01-IQK9N2mEOyAj3iWU.htm](equipment/consumable-01-IQK9N2mEOyAj3iWU.htm)|Bestial Mutagen (Lesser)|auto-trad|
|[consumable-01-JbkAk2j7h31izKyR.htm](equipment/consumable-01-JbkAk2j7h31izKyR.htm)|Feyfoul (Lesser)|auto-trad|
|[consumable-01-jlFx4JIBKJuaINpv.htm](equipment/consumable-01-jlFx4JIBKJuaINpv.htm)|Potion of Shared Memories|auto-trad|
|[consumable-01-JPALWW3w4z8STAYV.htm](equipment/consumable-01-JPALWW3w4z8STAYV.htm)|Deadweight Mutagen (Lesser)|auto-trad|
|[consumable-01-jTfacZ4SRuQd7Avh.htm](equipment/consumable-01-jTfacZ4SRuQd7Avh.htm)|Potion of Expeditious Retreat|auto-trad|
|[consumable-01-jVJwCB5ztkXHD5ML.htm](equipment/consumable-01-jVJwCB5ztkXHD5ML.htm)|Clockwork Goggles|auto-trad|
|[consumable-01-kF761P3ibBIFmLm9.htm](equipment/consumable-01-kF761P3ibBIFmLm9.htm)|Caltrop Snare|auto-trad|
|[consumable-01-Km4lSOsyrip5q6iD.htm](equipment/consumable-01-Km4lSOsyrip5q6iD.htm)|Hampering Snare|auto-trad|
|[consumable-01-kScHu3XS3XPvN9Db.htm](equipment/consumable-01-kScHu3XS3XPvN9Db.htm)|Theatrical Mutagen (Lesser)|auto-trad|
|[consumable-01-ktjFOp3U0wQD9t0Z.htm](equipment/consumable-01-ktjFOp3U0wQD9t0Z.htm)|Antidote (Lesser)|auto-trad|
|[consumable-01-lAmwzpbGaARQ2eP4.htm](equipment/consumable-01-lAmwzpbGaARQ2eP4.htm)|Shrine Inarizushi|auto-trad|
|[consumable-01-M0DMGUN84FQHapsF.htm](equipment/consumable-01-M0DMGUN84FQHapsF.htm)|Alchemical Fuse|auto-trad|
|[consumable-01-MoBlVd36uD9xVvZC.htm](equipment/consumable-01-MoBlVd36uD9xVvZC.htm)|Smokestick (Lesser)|auto-trad|
|[consumable-01-mYfK7M3BhB57LmAl.htm](equipment/consumable-01-mYfK7M3BhB57LmAl.htm)|Missive Mint|auto-trad|
|[consumable-01-nrAaCGSppPhPxOj3.htm](equipment/consumable-01-nrAaCGSppPhPxOj3.htm)|Sack of Rotten Fruit|auto-trad|
|[consumable-01-nybZVDfQWMhwvL2C.htm](equipment/consumable-01-nybZVDfQWMhwvL2C.htm)|Blast Boots (Lesser)|auto-trad|
|[consumable-01-nz4riYwJiko98Mva.htm](equipment/consumable-01-nz4riYwJiko98Mva.htm)|Battering Snare|auto-trad|
|[consumable-01-OIirLySQDLZgT15S.htm](equipment/consumable-01-OIirLySQDLZgT15S.htm)|Arsenic|auto-trad|
|[consumable-01-oplQpQSTyTvHDDtq.htm](equipment/consumable-01-oplQpQSTyTvHDDtq.htm)|Marking Snare|auto-trad|
|[consumable-01-pBq1cHXnic8dGxx8.htm](equipment/consumable-01-pBq1cHXnic8dGxx8.htm)|Journeybread|auto-trad|
|[consumable-01-PLun5Enmp8ZbjogV.htm](equipment/consumable-01-PLun5Enmp8ZbjogV.htm)|Bookthief Brew|auto-trad|
|[consumable-01-PNwgGnN081l0cETR.htm](equipment/consumable-01-PNwgGnN081l0cETR.htm)|Blood Sap|auto-trad|
|[consumable-01-qF2GDYQCpSZ12Nk7.htm](equipment/consumable-01-qF2GDYQCpSZ12Nk7.htm)|Ablative Shield Plating (Lesser)|auto-trad|
|[consumable-01-qkPlUzyNLmLKVtBL.htm](equipment/consumable-01-qkPlUzyNLmLKVtBL.htm)|Lady's Blessing Oil|auto-trad|
|[consumable-01-qnvq3PSTiejQTSi9.htm](equipment/consumable-01-qnvq3PSTiejQTSi9.htm)|Ghost Ink|auto-trad|
|[consumable-01-qoM7Va5GqcLLBzgu.htm](equipment/consumable-01-qoM7Va5GqcLLBzgu.htm)|Owlbear Claw|auto-trad|
|[consumable-01-RjuupS9xyXDLgyIr.htm](equipment/consumable-01-RjuupS9xyXDLgyIr.htm)|Scroll of 1st-level Spell|auto-trad|
|[consumable-01-SNMggNsvgPoAYjpU.htm](equipment/consumable-01-SNMggNsvgPoAYjpU.htm)|Addiction Suppressant (Lesser)|auto-trad|
|[consumable-01-tLa4bewBhyqzi6Ow.htm](equipment/consumable-01-tLa4bewBhyqzi6Ow.htm)|Cantrip Deck (5-pack)|auto-trad|
|[consumable-01-tspcGx4OrZEv2gQX.htm](equipment/consumable-01-tspcGx4OrZEv2gQX.htm)|Merciful Balm|auto-trad|
|[consumable-01-txmX5ghhPS72GKXy.htm](equipment/consumable-01-txmX5ghhPS72GKXy.htm)|Giant Centipede Venom|auto-trad|
|[consumable-01-tyt6rFtv32MZ4DT9.htm](equipment/consumable-01-tyt6rFtv32MZ4DT9.htm)|Cheetah's Elixir (Lesser)|auto-trad|
|[consumable-01-U9XNSFQzCOo6KMD1.htm](equipment/consumable-01-U9XNSFQzCOo6KMD1.htm)|Ablative Armor Plating (Lesser)|auto-trad|
|[consumable-01-uG3xtNrs26scOVgW.htm](equipment/consumable-01-uG3xtNrs26scOVgW.htm)|Leaper's Elixir (Lesser)|auto-trad|
|[consumable-01-ugIxgGIkJh2aXE2N.htm](equipment/consumable-01-ugIxgGIkJh2aXE2N.htm)|Potion of Fire Retaliation (Minor)|auto-trad|
|[consumable-01-ukTlC4G83aVQEg7u.htm](equipment/consumable-01-ukTlC4G83aVQEg7u.htm)|Nectar of Purification|auto-trad|
|[consumable-01-UqinuuCWePTYGhVO.htm](equipment/consumable-01-UqinuuCWePTYGhVO.htm)|Antiplague (Lesser)|auto-trad|
|[consumable-01-UYj4AxqiwtlTW8Kl.htm](equipment/consumable-01-UYj4AxqiwtlTW8Kl.htm)|Cryomister (Lesser)|auto-trad|
|[consumable-01-V35j3JEJMUbuJZNX.htm](equipment/consumable-01-V35j3JEJMUbuJZNX.htm)|Numbing Tonic (Minor)|auto-trad|
|[consumable-01-VeCNWhvEr82ZNoSV.htm](equipment/consumable-01-VeCNWhvEr82ZNoSV.htm)|Lastwall Soup|auto-trad|
|[consumable-01-VIHsu1q3078gdQut.htm](equipment/consumable-01-VIHsu1q3078gdQut.htm)|Sanguine Mutagen (Lesser)|auto-trad|
|[consumable-01-vl5dww56cbXo9QnP.htm](equipment/consumable-01-vl5dww56cbXo9QnP.htm)|Grit|auto-trad|
|[consumable-01-Vy3z2cIQ8uJRUMYw.htm](equipment/consumable-01-Vy3z2cIQ8uJRUMYw.htm)|Refined Pesh|auto-trad|
|[consumable-01-w0S1SIRsHgGdRh1d.htm](equipment/consumable-01-w0S1SIRsHgGdRh1d.htm)|Potion of Electricity Retaliation (Minor)|auto-trad|
|[consumable-01-wbr6rkyaVYnDhdgV.htm](equipment/consumable-01-wbr6rkyaVYnDhdgV.htm)|Cognitive Mutagen (Lesser)|auto-trad|
|[consumable-01-xBZCVHAa1SnR8Xul.htm](equipment/consumable-01-xBZCVHAa1SnR8Xul.htm)|Snake Oil|auto-trad|
|[consumable-01-XcD8p1o71tPohZWT.htm](equipment/consumable-01-XcD8p1o71tPohZWT.htm)|Signaling Snare|auto-trad|
|[consumable-01-xE0EdDrf734l2fQH.htm](equipment/consumable-01-xE0EdDrf734l2fQH.htm)|Juggernaut Mutagen (Lesser)|auto-trad|
|[consumable-01-xTdrhiLqFYUllrpK.htm](equipment/consumable-01-xTdrhiLqFYUllrpK.htm)|Cantrip Deck (Full Pack)|auto-trad|
|[consumable-01-y4WJY8rCbY6d1MET.htm](equipment/consumable-01-y4WJY8rCbY6d1MET.htm)|Sunrod|auto-trad|
|[consumable-01-YeF05oItxHNdOi6z.htm](equipment/consumable-01-YeF05oItxHNdOi6z.htm)|Poison Barbs Snare|auto-trad|
|[consumable-01-yi1iL9dbLDSr4NZd.htm](equipment/consumable-01-yi1iL9dbLDSr4NZd.htm)|Vaccine (Minor)|auto-trad|
|[consumable-01-ylUdMTsfOQGJ3MN3.htm](equipment/consumable-01-ylUdMTsfOQGJ3MN3.htm)|Tindertwig|auto-trad|
|[consumable-01-YwRAHWW8yUI07sy9.htm](equipment/consumable-01-YwRAHWW8yUI07sy9.htm)|Silvertongue Mutagen (Lesser)|auto-trad|
|[consumable-01-ZCsksGf6NPUKz2Uw.htm](equipment/consumable-01-ZCsksGf6NPUKz2Uw.htm)|Potency Crystal|auto-trad|
|[consumable-01-zExWH2EsY9STTORq.htm](equipment/consumable-01-zExWH2EsY9STTORq.htm)|Potion of Cold Retaliation (Minor)|auto-trad|
|[consumable-01-zHzAZRDVtl2NFqyh.htm](equipment/consumable-01-zHzAZRDVtl2NFqyh.htm)|Elemental Ammunition (Lesser)|auto-trad|
|[consumable-02-0aSdDSjJ5sMzBz1U.htm](equipment/consumable-02-0aSdDSjJ5sMzBz1U.htm)|Onyx Panther|auto-trad|
|[consumable-02-0QiHkpL9mFwNU0Ts.htm](equipment/consumable-02-0QiHkpL9mFwNU0Ts.htm)|Spring-Loaded Net Launcher|auto-trad|
|[consumable-02-1TWHN8RbimPVXM0U.htm](equipment/consumable-02-1TWHN8RbimPVXM0U.htm)|Sinew-Shock Serum (Lesser)|auto-trad|
|[consumable-02-1ZcywTbxv9UH13T3.htm](equipment/consumable-02-1ZcywTbxv9UH13T3.htm)|Wasul Reed Mask|auto-trad|
|[consumable-02-2MgFoNXTccL8Own9.htm](equipment/consumable-02-2MgFoNXTccL8Own9.htm)|Jade Cat|auto-trad|
|[consumable-02-3Klm2gPmzOw6ntVb.htm](equipment/consumable-02-3Klm2gPmzOw6ntVb.htm)|Comprehension Elixir (Lesser)|auto-trad|
|[consumable-02-6DmHDtIsGzH1s5JO.htm](equipment/consumable-02-6DmHDtIsGzH1s5JO.htm)|Oil of Weightlessness|auto-trad|
|[consumable-02-88pGCHV0uKMskTVO.htm](equipment/consumable-02-88pGCHV0uKMskTVO.htm)|Darkvision Elixir (Lesser)|auto-trad|
|[consumable-02-8KbayiwrUJtvif0a.htm](equipment/consumable-02-8KbayiwrUJtvif0a.htm)|Focus Cathartic (Lesser)|auto-trad|
|[consumable-02-9fKNH0ktIYs4f7Dl.htm](equipment/consumable-02-9fKNH0ktIYs4f7Dl.htm)|Clockwork Monkey|auto-trad|
|[consumable-02-AALcDOpwxCBN50lA.htm](equipment/consumable-02-AALcDOpwxCBN50lA.htm)|Lover's Ink|auto-trad|
|[consumable-02-aBGKmzkHsaNH07Di.htm](equipment/consumable-02-aBGKmzkHsaNH07Di.htm)|Instant Spy|auto-trad|
|[consumable-02-aKbrBW1SnFDxya5J.htm](equipment/consumable-02-aKbrBW1SnFDxya5J.htm)|Mesmerizing Opal|auto-trad|
|[consumable-02-bJVk50mhaODgsOUe.htm](equipment/consumable-02-bJVk50mhaODgsOUe.htm)|Yellow Musk Poison|auto-trad|
|[consumable-02-bqbCL8ZTeAa9EfcR.htm](equipment/consumable-02-bqbCL8ZTeAa9EfcR.htm)|Noisemaker Snare|auto-trad|
|[consumable-02-bQPRKEpnLakJBAAh.htm](equipment/consumable-02-bQPRKEpnLakJBAAh.htm)|Cat's Eye Elixir|auto-trad|
|[consumable-02-cHKqK8g9dS3GJ58F.htm](equipment/consumable-02-cHKqK8g9dS3GJ58F.htm)|Emetic Paste (Lesser)|auto-trad|
|[consumable-02-craWRj7jI2mLs1Ok.htm](equipment/consumable-02-craWRj7jI2mLs1Ok.htm)|Deadweight Snare|auto-trad|
|[consumable-02-cTBmQgWUyf50x3dY.htm](equipment/consumable-02-cTBmQgWUyf50x3dY.htm)|Black Smear Poison|auto-trad|
|[consumable-02-cuomhpenkqGM5lLG.htm](equipment/consumable-02-cuomhpenkqGM5lLG.htm)|Infiltrator's Elixir|auto-trad|
|[consumable-02-cwurQLvQaqjK70UI.htm](equipment/consumable-02-cwurQLvQaqjK70UI.htm)|Feather Token (Holly Bush)|auto-trad|
|[consumable-02-e20qv4NhYsoTlXtw.htm](equipment/consumable-02-e20qv4NhYsoTlXtw.htm)|Cauterizing Torch|auto-trad|
|[consumable-02-ENAlE5JV00GFkGjb.htm](equipment/consumable-02-ENAlE5JV00GFkGjb.htm)|Quickmelt Slick (Lesser)|auto-trad|
|[consumable-02-eVI81XML1c9jqhIx.htm](equipment/consumable-02-eVI81XML1c9jqhIx.htm)|Holy Steam Ball Refill|auto-trad|
|[consumable-02-FL8QU8TcNauBMMhD.htm](equipment/consumable-02-FL8QU8TcNauBMMhD.htm)|Belladonna|auto-trad|
|[consumable-02-fSO6mFltCGuSapsi.htm](equipment/consumable-02-fSO6mFltCGuSapsi.htm)|Ooze Ammunition (Lesser)|auto-trad|
|[consumable-02-gB9Qp85rpuNtPVGx.htm](equipment/consumable-02-gB9Qp85rpuNtPVGx.htm)|Sneezing Powder|auto-trad|
|[consumable-02-gYYlqHTW1lgwd6ri.htm](equipment/consumable-02-gYYlqHTW1lgwd6ri.htm)|Expulsion Snare|auto-trad|
|[consumable-02-H3nAm1FFvuG7j4TN.htm](equipment/consumable-02-H3nAm1FFvuG7j4TN.htm)|Adaptive Cogwheel|auto-trad|
|[consumable-02-HHELOoN5GVonUiIa.htm](equipment/consumable-02-HHELOoN5GVonUiIa.htm)|Crying Angel Pendant|auto-trad|
|[consumable-02-I0eUrgVGl8a2ZjFc.htm](equipment/consumable-02-I0eUrgVGl8a2ZjFc.htm)|Boulderhead Bock|auto-trad|
|[consumable-02-ICDQcwf2U7bSoieB.htm](equipment/consumable-02-ICDQcwf2U7bSoieB.htm)|Anticorrosion Oil|auto-trad|
|[consumable-02-iYsDDvFduPhtaIOr.htm](equipment/consumable-02-iYsDDvFduPhtaIOr.htm)|Aromatic Ammunition|auto-trad|
|[consumable-02-j2CHumvbjmlLQX2i.htm](equipment/consumable-02-j2CHumvbjmlLQX2i.htm)|Oil of Potency|auto-trad|
|[consumable-02-jdDDqv9LbEYX2wAE.htm](equipment/consumable-02-jdDDqv9LbEYX2wAE.htm)|Monkey Pin|auto-trad|
|[consumable-02-l8wky2NQduHYsY9B.htm](equipment/consumable-02-l8wky2NQduHYsY9B.htm)|Lover's Knot|auto-trad|
|[consumable-02-mDJSaarQsIMX7Opi.htm](equipment/consumable-02-mDJSaarQsIMX7Opi.htm)|Bloodhound Mask (Lesser)|auto-trad|
|[consumable-02-mDNDs5P8ZAJXYbDL.htm](equipment/consumable-02-mDNDs5P8ZAJXYbDL.htm)|Toad Tears|auto-trad|
|[consumable-02-mvrpdVyFJc5eOoE9.htm](equipment/consumable-02-mvrpdVyFJc5eOoE9.htm)|Bubbling Scale|auto-trad|
|[consumable-02-N3jcmW5XzEJZQVtJ.htm](equipment/consumable-02-N3jcmW5XzEJZQVtJ.htm)|Antivenom Potion|auto-trad|
|[consumable-02-npSWkkSsQfiKixPO.htm](equipment/consumable-02-npSWkkSsQfiKixPO.htm)|Origin Unguent|auto-trad|
|[consumable-02-NSQOijKqomyotXkj.htm](equipment/consumable-02-NSQOijKqomyotXkj.htm)|Antler Arrow|auto-trad|
|[consumable-02-nXStoLxPrrP2b6WB.htm](equipment/consumable-02-nXStoLxPrrP2b6WB.htm)|Bronze Bull Pendant|auto-trad|
|[consumable-02-Oo0KZTmFsu9iCAR9.htm](equipment/consumable-02-Oo0KZTmFsu9iCAR9.htm)|Life Shot (Minor)|auto-trad|
|[consumable-02-oZBafUU1tk4tgwye.htm](equipment/consumable-02-oZBafUU1tk4tgwye.htm)|Mender's Soup|auto-trad|
|[consumable-02-PPd89jN2a4rFoVHm.htm](equipment/consumable-02-PPd89jN2a4rFoVHm.htm)|Moon Radish Soup|auto-trad|
|[consumable-02-q5Hr2nZr8RQ9DSZM.htm](equipment/consumable-02-q5Hr2nZr8RQ9DSZM.htm)|Blaze|auto-trad|
|[consumable-02-qeTWg0TWw9CwMKCO.htm](equipment/consumable-02-qeTWg0TWw9CwMKCO.htm)|Savior Spike|auto-trad|
|[consumable-02-QyEp8sN9mn36K5El.htm](equipment/consumable-02-QyEp8sN9mn36K5El.htm)|Moonlit Ink|auto-trad|
|[consumable-02-R3AnwcaHru4PFHJ4.htm](equipment/consumable-02-R3AnwcaHru4PFHJ4.htm)|Winterstep Elixir (Minor)|auto-trad|
|[consumable-02-riLNCaVS9zGvt4Nn.htm](equipment/consumable-02-riLNCaVS9zGvt4Nn.htm)|Flare Snare|auto-trad|
|[consumable-02-ScclzFrjyB0YJlrb.htm](equipment/consumable-02-ScclzFrjyB0YJlrb.htm)|Black Adder Venom|auto-trad|
|[consumable-02-sHBk2pY0I6dCW2Mw.htm](equipment/consumable-02-sHBk2pY0I6dCW2Mw.htm)|Black Powder (Horn)|auto-trad|
|[consumable-02-T28lI7becoK1ZGqr.htm](equipment/consumable-02-T28lI7becoK1ZGqr.htm)|Cooperative Waffles|auto-trad|
|[consumable-02-T6jPmGyInYATYOzr.htm](equipment/consumable-02-T6jPmGyInYATYOzr.htm)|Smoke Fan|auto-trad|
|[consumable-02-TUya8pXhEa70ahAi.htm](equipment/consumable-02-TUya8pXhEa70ahAi.htm)|Wind-Up Cart|auto-trad|
|[consumable-02-vAMgDuD3BphJL8EJ.htm](equipment/consumable-02-vAMgDuD3BphJL8EJ.htm)|Ember Dust|auto-trad|
|[consumable-02-VPvyyQXjn2HBjnTS.htm](equipment/consumable-02-VPvyyQXjn2HBjnTS.htm)|Effervescent Ampoule|auto-trad|
|[consumable-02-wU8HH03cNxe6Pkfi.htm](equipment/consumable-02-wU8HH03cNxe6Pkfi.htm)|Moonlit Spellgun (Minor)|auto-trad|
|[consumable-02-x4QLLKEShYqUjCCn.htm](equipment/consumable-02-x4QLLKEShYqUjCCn.htm)|Red-Rib Gill Mask (Lesser)|auto-trad|
|[consumable-02-xky9NMHpP3yqzYOj.htm](equipment/consumable-02-xky9NMHpP3yqzYOj.htm)|Dragon's Blood Pudding|auto-trad|
|[consumable-02-Yew9oddFsH0KeDLh.htm](equipment/consumable-02-Yew9oddFsH0KeDLh.htm)|Hunter's Bane|auto-trad|
|[consumable-02-yUPRtHCpAPx3EBUq.htm](equipment/consumable-02-yUPRtHCpAPx3EBUq.htm)|Oil of Skating|auto-trad|
|[consumable-02-yUYr8j65fC7EN0NY.htm](equipment/consumable-02-yUYr8j65fC7EN0NY.htm)|Thunder Snare|auto-trad|
|[consumable-02-zA2UPGE9RYwlLPZJ.htm](equipment/consumable-02-zA2UPGE9RYwlLPZJ.htm)|Soothing Tonic (Lesser)|auto-trad|
|[consumable-02-zle8PCLlI8jIkgiY.htm](equipment/consumable-02-zle8PCLlI8jIkgiY.htm)|Tripline Arrow|auto-trad|
|[consumable-02-zM9VX3QwM81DzDUA.htm](equipment/consumable-02-zM9VX3QwM81DzDUA.htm)|Bravo's Brew (Lesser)|auto-trad|
|[consumable-02-zo0ophqfKunJFxZN.htm](equipment/consumable-02-zo0ophqfKunJFxZN.htm)|Lethargy Poison|auto-trad|
|[consumable-03-03Jfj5QE7twaY5nW.htm](equipment/consumable-03-03Jfj5QE7twaY5nW.htm)|Clockwork Chirper|auto-trad|
|[consumable-03-097NJos0T8o7AMLi.htm](equipment/consumable-03-097NJos0T8o7AMLi.htm)|Venomous Cure Fulu|auto-trad|
|[consumable-03-0CNSvLpeSM4aIfPJ.htm](equipment/consumable-03-0CNSvLpeSM4aIfPJ.htm)|Feather Step Stone|auto-trad|
|[consumable-03-0rxTsnAGYWYaQaMl.htm](equipment/consumable-03-0rxTsnAGYWYaQaMl.htm)|Torrent Spellgun (Lesser)|auto-trad|
|[consumable-03-1AFWnVcoYvLcKtL9.htm](equipment/consumable-03-1AFWnVcoYvLcKtL9.htm)|Colorful Coating (Red)|auto-trad|
|[consumable-03-1Nez8K5C4fwgFrTz.htm](equipment/consumable-03-1Nez8K5C4fwgFrTz.htm)|Recording Rod (Basic)|auto-trad|
|[consumable-03-3tCtnomxBkdyG43X.htm](equipment/consumable-03-3tCtnomxBkdyG43X.htm)|Diplomat's Charcuterie|auto-trad|
|[consumable-03-5KYn9J1Hj4IG3Z0X.htm](equipment/consumable-03-5KYn9J1Hj4IG3Z0X.htm)|Snapleaf|auto-trad|
|[consumable-03-67KiGZtoTMbl7nv2.htm](equipment/consumable-03-67KiGZtoTMbl7nv2.htm)|Potion of Fire Retaliation (Lesser)|auto-trad|
|[consumable-03-6P6lDnIxK7plyCEI.htm](equipment/consumable-03-6P6lDnIxK7plyCEI.htm)|Swamp Lily Quilt|auto-trad|
|[consumable-03-6VCaHBbUBk4eBlPC.htm](equipment/consumable-03-6VCaHBbUBk4eBlPC.htm)|Choker-Arm Mutagen (Moderate)|auto-trad|
|[consumable-03-6XMuLT5QKkoznKz9.htm](equipment/consumable-03-6XMuLT5QKkoznKz9.htm)|Oil of Ownership (Lesser)|auto-trad|
|[consumable-03-7WuJbX5c3SCB04Uw.htm](equipment/consumable-03-7WuJbX5c3SCB04Uw.htm)|Demolition Fulu (Lesser)|auto-trad|
|[consumable-03-89uak2oyHnEuIn0b.htm](equipment/consumable-03-89uak2oyHnEuIn0b.htm)|Fulu of Fire Suppression|auto-trad|
|[consumable-03-8g2U2PMC0lkPZEvq.htm](equipment/consumable-03-8g2U2PMC0lkPZEvq.htm)|Blood Booster (Lesser)|auto-trad|
|[consumable-03-8rQ7HkUtSa8so0Et.htm](equipment/consumable-03-8rQ7HkUtSa8so0Et.htm)|Theatrical Mutagen (Moderate)|auto-trad|
|[consumable-03-9FiSGqe8s8531Tyh.htm](equipment/consumable-03-9FiSGqe8s8531Tyh.htm)|Spirit-Sealing Fulu|auto-trad|
|[consumable-03-A4mt1d9CK9lpLiqa.htm](equipment/consumable-03-A4mt1d9CK9lpLiqa.htm)|Colorful Coating (Orange)|auto-trad|
|[consumable-03-AFFaFaAW3IbbYqkL.htm](equipment/consumable-03-AFFaFaAW3IbbYqkL.htm)|Life-Boosting Oil (Lesser)|auto-trad|
|[consumable-03-aJrMyopbSzvXn5Zl.htm](equipment/consumable-03-aJrMyopbSzvXn5Zl.htm)|Colorful Coating (Blue)|auto-trad|
|[consumable-03-AOAXZzH4AOsQqYYv.htm](equipment/consumable-03-AOAXZzH4AOsQqYYv.htm)|War Blood Mutagen (Moderate)|auto-trad|
|[consumable-03-aUzX848Ygqicy2qs.htm](equipment/consumable-03-aUzX848Ygqicy2qs.htm)|Drowsy Sun Eye Drops|auto-trad|
|[consumable-03-aY6Ukb7ZsMDelhmH.htm](equipment/consumable-03-aY6Ukb7ZsMDelhmH.htm)|Ablative Shield Plating (Moderate)|auto-trad|
|[consumable-03-aZm1x9tpvBAT8YCd.htm](equipment/consumable-03-aZm1x9tpvBAT8YCd.htm)|Cytillesh Oil|auto-trad|
|[consumable-03-bMbWbKEbKSmVgoGT.htm](equipment/consumable-03-bMbWbKEbKSmVgoGT.htm)|Breech Ejectors|auto-trad|
|[consumable-03-BSInwFNVBVkfFK0B.htm](equipment/consumable-03-BSInwFNVBVkfFK0B.htm)|Oil of Unlife (Lesser)|auto-trad|
|[consumable-03-C1mX0irc7zvLqDEs.htm](equipment/consumable-03-C1mX0irc7zvLqDEs.htm)|Witch's Finger|auto-trad|
|[consumable-03-C8kQuf7gqpjEDmib.htm](equipment/consumable-03-C8kQuf7gqpjEDmib.htm)|Merciful Charm|auto-trad|
|[consumable-03-cVntDkkZP9mcuFHU.htm](equipment/consumable-03-cVntDkkZP9mcuFHU.htm)|Trustworthy Round|auto-trad|
|[consumable-03-CwFqyOn3h5aFdF8V.htm](equipment/consumable-03-CwFqyOn3h5aFdF8V.htm)|Seeking Bracelets|auto-trad|
|[consumable-03-cYkmDxRFtwQRvwVh.htm](equipment/consumable-03-cYkmDxRFtwQRvwVh.htm)|Blindpepper Bolt|auto-trad|
|[consumable-03-Dn2KQgIeWNijaUzL.htm](equipment/consumable-03-Dn2KQgIeWNijaUzL.htm)|Feather Token (Chest)|auto-trad|
|[consumable-03-DnFmNx99xYsYxNfO.htm](equipment/consumable-03-DnFmNx99xYsYxNfO.htm)|Rock Ripper Snare|auto-trad|
|[consumable-03-e0vSAQfxhHauiAoD.htm](equipment/consumable-03-e0vSAQfxhHauiAoD.htm)|Healing Potion (Lesser)|auto-trad|
|[consumable-03-EbqQbozqVGEVxEG9.htm](equipment/consumable-03-EbqQbozqVGEVxEG9.htm)|Colorful Coating (Green)|auto-trad|
|[consumable-03-eEnzHpPEbdGgRETM.htm](equipment/consumable-03-eEnzHpPEbdGgRETM.htm)|Vine Arrow|auto-trad|
|[consumable-03-F5LJiS0wcwSqRrQ9.htm](equipment/consumable-03-F5LJiS0wcwSqRrQ9.htm)|Revealing Mist (Lesser)|auto-trad|
|[consumable-03-F8jLnpBuvzM0nBp4.htm](equipment/consumable-03-F8jLnpBuvzM0nBp4.htm)|Bane Oil|auto-trad|
|[consumable-03-fFq8nsGvSUgzVeND.htm](equipment/consumable-03-fFq8nsGvSUgzVeND.htm)|Feather Token (Bird)|auto-trad|
|[consumable-03-FVB9noeUXXyau029.htm](equipment/consumable-03-FVB9noeUXXyau029.htm)|Blast Boots (Moderate)|auto-trad|
|[consumable-03-GNM1LvqENq8WqT9C.htm](equipment/consumable-03-GNM1LvqENq8WqT9C.htm)|Tracker's Stew|auto-trad|
|[consumable-03-guUmnzyZVuzJO9oD.htm](equipment/consumable-03-guUmnzyZVuzJO9oD.htm)|Recovery Bladder|auto-trad|
|[consumable-03-gXqU9CInB05jdxh6.htm](equipment/consumable-03-gXqU9CInB05jdxh6.htm)|Fire Box|auto-trad|
|[consumable-03-hOemslrdMyHCgSBk.htm](equipment/consumable-03-hOemslrdMyHCgSBk.htm)|Impossible Cake|auto-trad|
|[consumable-03-hoX2sJiYtJrSp6BH.htm](equipment/consumable-03-hoX2sJiYtJrSp6BH.htm)|Psychic Warding Bracelet|auto-trad|
|[consumable-03-HuyWiPnkDZoAVhW9.htm](equipment/consumable-03-HuyWiPnkDZoAVhW9.htm)|Spiderfoot Brew (Lesser)|auto-trad|
|[consumable-03-HVPbv5n2e1vcT9Ug.htm](equipment/consumable-03-HVPbv5n2e1vcT9Ug.htm)|Retrieval Prism|auto-trad|
|[consumable-03-hw81uONdpyUbdWUJ.htm](equipment/consumable-03-hw81uONdpyUbdWUJ.htm)|Dream Pollen Snare|auto-trad|
|[consumable-03-hxYxRz9nOECtLak5.htm](equipment/consumable-03-hxYxRz9nOECtLak5.htm)|Grasping Tree|auto-trad|
|[consumable-03-ibxFkN7NGB1pybWx.htm](equipment/consumable-03-ibxFkN7NGB1pybWx.htm)|Indomitable Keepsake|auto-trad|
|[consumable-03-iNR6t5dDiYGQYTeA.htm](equipment/consumable-03-iNR6t5dDiYGQYTeA.htm)|Quickpatch Glue|auto-trad|
|[consumable-03-jIlFuZaIgMPEzoyP.htm](equipment/consumable-03-jIlFuZaIgMPEzoyP.htm)|Matchmaker Fulu|auto-trad|
|[consumable-03-k5A8zFQuDtGqNnk5.htm](equipment/consumable-03-k5A8zFQuDtGqNnk5.htm)|Toothwort Extract|auto-trad|
|[consumable-03-K6D2Ld8Md6PVsAaV.htm](equipment/consumable-03-K6D2Ld8Md6PVsAaV.htm)|Serpent Oil (Lesser)|auto-trad|
|[consumable-03-kO31GgqGY37MDRff.htm](equipment/consumable-03-kO31GgqGY37MDRff.htm)|Periscopic Viewfinder|auto-trad|
|[consumable-03-KPKmM9qSdQ5X587J.htm](equipment/consumable-03-KPKmM9qSdQ5X587J.htm)|Colorful Coating (Yellow)|auto-trad|
|[consumable-03-KuTwNrDpuYVAPu7c.htm](equipment/consumable-03-KuTwNrDpuYVAPu7c.htm)|Bralani Breath|auto-trad|
|[consumable-03-Lbe9L9ZDBJgjDIhT.htm](equipment/consumable-03-Lbe9L9ZDBJgjDIhT.htm)|Sprite Apple (Golden)|auto-trad|
|[consumable-03-lIExlUFBKvBue8hb.htm](equipment/consumable-03-lIExlUFBKvBue8hb.htm)|Silvertongue Mutagen (Moderate)|auto-trad|
|[consumable-03-llqztgP14IJGSxU4.htm](equipment/consumable-03-llqztgP14IJGSxU4.htm)|Metalmist Sphere (Lesser)|auto-trad|
|[consumable-03-M1rX9EahXV9pa0Ui.htm](equipment/consumable-03-M1rX9EahXV9pa0Ui.htm)|Disrupting Oil|auto-trad|
|[consumable-03-mj9i9GeQTADByNPZ.htm](equipment/consumable-03-mj9i9GeQTADByNPZ.htm)|Juggernaut Mutagen (Moderate)|auto-trad|
|[consumable-03-MMloq9cUKNJYCDLW.htm](equipment/consumable-03-MMloq9cUKNJYCDLW.htm)|Colorful Coating (Indigo)|auto-trad|
|[consumable-03-mYVUS8p9gA3mCLES.htm](equipment/consumable-03-mYVUS8p9gA3mCLES.htm)|Ectoplasmic Tracer|auto-trad|
|[consumable-03-n52BSbZsnx4Vmt2p.htm](equipment/consumable-03-n52BSbZsnx4Vmt2p.htm)|Quicksilver Mutagen (Moderate)|auto-trad|
|[consumable-03-nPWDuoe2PcgE0z2S.htm](equipment/consumable-03-nPWDuoe2PcgE0z2S.htm)|Torrent Snare|auto-trad|
|[consumable-03-NSo0bFX7DGGjqKKl.htm](equipment/consumable-03-NSo0bFX7DGGjqKKl.htm)|Spellstrike Ammunition (Type I)|auto-trad|
|[consumable-03-nzeTcOyQmZNrurVF.htm](equipment/consumable-03-nzeTcOyQmZNrurVF.htm)|Vaccine (Lesser)|auto-trad|
|[consumable-03-Oip645RjC5y57wFa.htm](equipment/consumable-03-Oip645RjC5y57wFa.htm)|Violet Venom|auto-trad|
|[consumable-03-omRmCbkQoK5YmCGv.htm](equipment/consumable-03-omRmCbkQoK5YmCGv.htm)|Potion of Acid Retaliation (Lesser)|auto-trad|
|[consumable-03-OzSfVy1SoKzCWG8q.htm](equipment/consumable-03-OzSfVy1SoKzCWG8q.htm)|Draft of Stellar Radiance|auto-trad|
|[consumable-03-P7Evi8Wv8zIwLYtU.htm](equipment/consumable-03-P7Evi8Wv8zIwLYtU.htm)|Ablative Armor Plating (Moderate)|auto-trad|
|[consumable-03-pGnoCqFy0ESPbuGv.htm](equipment/consumable-03-pGnoCqFy0ESPbuGv.htm)|Aurifying Salts|auto-trad|
|[consumable-03-pP7RqEkNYg5ayWqx.htm](equipment/consumable-03-pP7RqEkNYg5ayWqx.htm)|Sanguine Mutagen (Moderate)|auto-trad|
|[consumable-03-qhn0PSd5TPGpblbg.htm](equipment/consumable-03-qhn0PSd5TPGpblbg.htm)|Forgetful Drops|auto-trad|
|[consumable-03-qLwwxk1j04rgXSF9.htm](equipment/consumable-03-qLwwxk1j04rgXSF9.htm)|Deadweight Mutagen (Moderate)|auto-trad|
|[consumable-03-QN8UIz0nMcnLUWHu.htm](equipment/consumable-03-QN8UIz0nMcnLUWHu.htm)|Oil of Mending|auto-trad|
|[consumable-03-qpzL9UnTi4cDhy6J.htm](equipment/consumable-03-qpzL9UnTi4cDhy6J.htm)|Cognitive Mutagen (Moderate)|auto-trad|
|[consumable-03-qQ9Rros3GdPDRLBg.htm](equipment/consumable-03-qQ9Rros3GdPDRLBg.htm)|Auric Noodles|auto-trad|
|[consumable-03-qQqAY59NgGNoy2xr.htm](equipment/consumable-03-qQqAY59NgGNoy2xr.htm)|Graveroot|auto-trad|
|[consumable-03-Qx6CAAbSQ0RpruJH.htm](equipment/consumable-03-Qx6CAAbSQ0RpruJH.htm)|Hoof Stakes Snare|auto-trad|
|[consumable-03-qzT0myaHAiSqnwWW.htm](equipment/consumable-03-qzT0myaHAiSqnwWW.htm)|Parchment of Secrets|auto-trad|
|[consumable-03-R0gsYr0jD1giTzpx.htm](equipment/consumable-03-R0gsYr0jD1giTzpx.htm)|Olfactory Obfuscator (Lesser)|auto-trad|
|[consumable-03-RBYn6syZKENT0UiZ.htm](equipment/consumable-03-RBYn6syZKENT0UiZ.htm)|Camouflage Dye (Lesser)|auto-trad|
|[consumable-03-RcQ4ZIzRK2xLf4G5.htm](equipment/consumable-03-RcQ4ZIzRK2xLf4G5.htm)|Sleep Arrow|auto-trad|
|[consumable-03-RmVh6urcDFWrcLxq.htm](equipment/consumable-03-RmVh6urcDFWrcLxq.htm)|Feather Token (Ladder) (Ammunition)|auto-trad|
|[consumable-03-s0btfYXKuufJhbyx.htm](equipment/consumable-03-s0btfYXKuufJhbyx.htm)|Stumbling Fulu|auto-trad|
|[consumable-03-SRMgleqCK2tsZjvS.htm](equipment/consumable-03-SRMgleqCK2tsZjvS.htm)|Etheric Essence Disruptor (Lesser)|auto-trad|
|[consumable-03-taAjenWKjBJpQyrE.htm](equipment/consumable-03-taAjenWKjBJpQyrE.htm)|Beacon Shot|auto-trad|
|[consumable-03-TG4SnQCGOsgQfrE1.htm](equipment/consumable-03-TG4SnQCGOsgQfrE1.htm)|Potion of Electricity Retaliation (Lesser)|auto-trad|
|[consumable-03-uCt7Kc2MXzRMc0GD.htm](equipment/consumable-03-uCt7Kc2MXzRMc0GD.htm)|Soothing Scents|auto-trad|
|[consumable-03-UfoaNXFA7AWZiXxI.htm](equipment/consumable-03-UfoaNXFA7AWZiXxI.htm)|Shiver|auto-trad|
|[consumable-03-UJWiN0K3jqVjxvKk.htm](equipment/consumable-03-UJWiN0K3jqVjxvKk.htm)|Magic Wand (1st-Level Spell)|auto-trad|
|[consumable-03-Uq3rnsAHbLKyW49E.htm](equipment/consumable-03-Uq3rnsAHbLKyW49E.htm)|Potion of Cold Retaliation (Lesser)|auto-trad|
|[consumable-03-V18pnbAay4iG7O6C.htm](equipment/consumable-03-V18pnbAay4iG7O6C.htm)|Detonating Gears Snare|auto-trad|
|[consumable-03-vbH19NIBChzdEn4R.htm](equipment/consumable-03-vbH19NIBChzdEn4R.htm)|Lodestone Pellet|auto-trad|
|[consumable-03-VISk5uLPVIvNWovB.htm](equipment/consumable-03-VISk5uLPVIvNWovB.htm)|Bestial Mutagen (Moderate)|auto-trad|
|[consumable-03-vQ2XU6y1MBVDtQgr.htm](equipment/consumable-03-vQ2XU6y1MBVDtQgr.htm)|Feather Token (Puddle)|auto-trad|
|[consumable-03-WETEtFdpuXa1c7Zc.htm](equipment/consumable-03-WETEtFdpuXa1c7Zc.htm)|Neophyte's Fipple|auto-trad|
|[consumable-03-WKp751kO2ba4qPeN.htm](equipment/consumable-03-WKp751kO2ba4qPeN.htm)|Ranging Shot|auto-trad|
|[consumable-03-WQhnfj1LbrEzvh8z.htm](equipment/consumable-03-WQhnfj1LbrEzvh8z.htm)|Potion of Water Breathing|auto-trad|
|[consumable-03-wu9cqBx65txvAODd.htm](equipment/consumable-03-wu9cqBx65txvAODd.htm)|Material Essence Disruptor (Lesser)|auto-trad|
|[consumable-03-XEYveTvLH1lJ4jeI.htm](equipment/consumable-03-XEYveTvLH1lJ4jeI.htm)|Serene Mutagen (Moderate)|auto-trad|
|[consumable-03-xY2MogTwH9Fd8UPG.htm](equipment/consumable-03-xY2MogTwH9Fd8UPG.htm)|Drakeheart Mutagen (Moderate)|auto-trad|
|[consumable-03-Y7UD64foDbDMV9sx.htm](equipment/consumable-03-Y7UD64foDbDMV9sx.htm)|Scroll of 2nd-level Spell|auto-trad|
|[consumable-03-Y7VAygKtMnE6HaXJ.htm](equipment/consumable-03-Y7VAygKtMnE6HaXJ.htm)|Artevil Suspension|auto-trad|
|[consumable-03-zAfSy6fshSGUtYF6.htm](equipment/consumable-03-zAfSy6fshSGUtYF6.htm)|Seventh Prism (Triangular)|auto-trad|
|[consumable-03-ZVaiCVMdkakXagIU.htm](equipment/consumable-03-ZVaiCVMdkakXagIU.htm)|Waterproofing Wax|auto-trad|
|[consumable-03-ZvOaZLwTT4kDbrF7.htm](equipment/consumable-03-ZvOaZLwTT4kDbrF7.htm)|Fulu of Flood Suppression|auto-trad|
|[consumable-04-0lhh2l4kh3QrwYH9.htm](equipment/consumable-04-0lhh2l4kh3QrwYH9.htm)|Hobbling Snare|auto-trad|
|[consumable-04-0RTsyWutvO42O0zK.htm](equipment/consumable-04-0RTsyWutvO42O0zK.htm)|Applereed Mutagen (Lesser)|auto-trad|
|[consumable-04-1Cj3rG1ugNPcWaPA.htm](equipment/consumable-04-1Cj3rG1ugNPcWaPA.htm)|Acid Spitter|auto-trad|
|[consumable-04-1oWATIKLaZBXzw9N.htm](equipment/consumable-04-1oWATIKLaZBXzw9N.htm)|Boom Snare|auto-trad|
|[consumable-04-2EigJwHDmem3YggO.htm](equipment/consumable-04-2EigJwHDmem3YggO.htm)|Ice Slick Snare|auto-trad|
|[consumable-04-2n8pySCWPa5s5LX9.htm](equipment/consumable-04-2n8pySCWPa5s5LX9.htm)|Lion Badge|auto-trad|
|[consumable-04-3AjyXlvLUx4c2RCi.htm](equipment/consumable-04-3AjyXlvLUx4c2RCi.htm)|Sprite Apple (Pink)|auto-trad|
|[consumable-04-3Fmbw9wJkqZBV9De.htm](equipment/consumable-04-3Fmbw9wJkqZBV9De.htm)|Feather Token (Fan)|auto-trad|
|[consumable-04-3v03D7qP6ewN03SN.htm](equipment/consumable-04-3v03D7qP6ewN03SN.htm)|Admonishing Band|auto-trad|
|[consumable-04-553H2rzgaHeY1VB4.htm](equipment/consumable-04-553H2rzgaHeY1VB4.htm)|Ghost Ampoule|auto-trad|
|[consumable-04-6Frd3SmMZZ49DnUZ.htm](equipment/consumable-04-6Frd3SmMZZ49DnUZ.htm)|Clockwork Goggles (Greater)|auto-trad|
|[consumable-04-6G088rA3TjiuQ5NA.htm](equipment/consumable-04-6G088rA3TjiuQ5NA.htm)|Healing Vapor (Lesser)|auto-trad|
|[consumable-04-76vu5p2S0wN77fJw.htm](equipment/consumable-04-76vu5p2S0wN77fJw.htm)|Fire-Douse Snare|auto-trad|
|[consumable-04-7Sm8NhvQcwi84Nsh.htm](equipment/consumable-04-7Sm8NhvQcwi84Nsh.htm)|Journeybread (Power)|auto-trad|
|[consumable-04-89955RkZoEmVNo3V.htm](equipment/consumable-04-89955RkZoEmVNo3V.htm)|Anointing Oil|auto-trad|
|[consumable-04-8D71bv2HsX29ZtwC.htm](equipment/consumable-04-8D71bv2HsX29ZtwC.htm)|Quickmelt Slick (Moderate)|auto-trad|
|[consumable-04-9EZb1hmSKOGrU4Cf.htm](equipment/consumable-04-9EZb1hmSKOGrU4Cf.htm)|Biting Snare|auto-trad|
|[consumable-04-aQ4Q8Rq6ntWaHcIv.htm](equipment/consumable-04-aQ4Q8Rq6ntWaHcIv.htm)|Fortifying Pebble|auto-trad|
|[consumable-04-awavOTGutIrLRQyq.htm](equipment/consumable-04-awavOTGutIrLRQyq.htm)|Phantom Roll|auto-trad|
|[consumable-04-bc3orvzCjY28HeQi.htm](equipment/consumable-04-bc3orvzCjY28HeQi.htm)|Mortalis Coin|auto-trad|
|[consumable-04-BeX3ForBrKTQD4T5.htm](equipment/consumable-04-BeX3ForBrKTQD4T5.htm)|Fury Cocktail (Lesser)|auto-trad|
|[consumable-04-bikFUFRLwfdvX2x2.htm](equipment/consumable-04-bikFUFRLwfdvX2x2.htm)|Invisibility Potion|auto-trad|
|[consumable-04-CM0Hq7KKCwUv0BV6.htm](equipment/consumable-04-CM0Hq7KKCwUv0BV6.htm)|Impact Foam Chassis (Moderate)|auto-trad|
|[consumable-04-dwaF9zHPK9823COa.htm](equipment/consumable-04-dwaF9zHPK9823COa.htm)|Dreamtime Tea|auto-trad|
|[consumable-04-Ekk7o1gPu8RotixD.htm](equipment/consumable-04-Ekk7o1gPu8RotixD.htm)|Salamander Elixir (Lesser)|auto-trad|
|[consumable-04-eKpL2j1JA92wndO6.htm](equipment/consumable-04-eKpL2j1JA92wndO6.htm)|Zerk|auto-trad|
|[consumable-04-eONI7XvfdGF34z74.htm](equipment/consumable-04-eONI7XvfdGF34z74.htm)|Feather Token (Holly Bush) (Ammunition)|auto-trad|
|[consumable-04-eyWT7VKdba72hKMw.htm](equipment/consumable-04-eyWT7VKdba72hKMw.htm)|Colorful Coating (Violet)|auto-trad|
|[consumable-04-FqbDpztscJfM4XMe.htm](equipment/consumable-04-FqbDpztscJfM4XMe.htm)|Shrinking Potion|auto-trad|
|[consumable-04-GnXKCkgZQG0UmuHz.htm](equipment/consumable-04-GnXKCkgZQG0UmuHz.htm)|Dragon Turtle Scale|auto-trad|
|[consumable-04-GyO89RBVjAKFxsFm.htm](equipment/consumable-04-GyO89RBVjAKFxsFm.htm)|Mistform Elixir (Lesser)|auto-trad|
|[consumable-04-gza0CcZEYhPKIYEk.htm](equipment/consumable-04-gza0CcZEYhPKIYEk.htm)|Spider Mold|auto-trad|
|[consumable-04-Ha6n30Tj3TNru9Dj.htm](equipment/consumable-04-Ha6n30Tj3TNru9Dj.htm)|Timeless Salts|auto-trad|
|[consumable-04-hKed91BOXjyaNG3x.htm](equipment/consumable-04-hKed91BOXjyaNG3x.htm)|Oozepick|auto-trad|
|[consumable-04-Hm0dZYfXHMNcTCo3.htm](equipment/consumable-04-Hm0dZYfXHMNcTCo3.htm)|Cinnamon Seers|auto-trad|
|[consumable-04-I2FmQuFJiJYmsTUd.htm](equipment/consumable-04-I2FmQuFJiJYmsTUd.htm)|Magical Lock Fulu|auto-trad|
|[consumable-04-ilB279mxqXnlaSFj.htm](equipment/consumable-04-ilB279mxqXnlaSFj.htm)|Viper Arrow|auto-trad|
|[consumable-04-JkQsRx2vwFqLLJmm.htm](equipment/consumable-04-JkQsRx2vwFqLLJmm.htm)|Oxygen Ooze|auto-trad|
|[consumable-04-L1tqtTYU8kk7G7h6.htm](equipment/consumable-04-L1tqtTYU8kk7G7h6.htm)|Glue Bullet|auto-trad|
|[consumable-04-LlmRVR4qv6H2AGPT.htm](equipment/consumable-04-LlmRVR4qv6H2AGPT.htm)|Golden-Cased Bullet (Standard)|auto-trad|
|[consumable-04-MIP28AmODXgyF2D2.htm](equipment/consumable-04-MIP28AmODXgyF2D2.htm)|Ghost Oil|auto-trad|
|[consumable-04-n5L7HE9H8jn8ftQy.htm](equipment/consumable-04-n5L7HE9H8jn8ftQy.htm)|Climbing Bolt|auto-trad|
|[consumable-04-n9VrmK9Us0viv20P.htm](equipment/consumable-04-n9VrmK9Us0viv20P.htm)|Winter Wolf Elixir (Lesser)|auto-trad|
|[consumable-04-NAkbCs6NqqPWnp5H.htm](equipment/consumable-04-NAkbCs6NqqPWnp5H.htm)|Brightshade|auto-trad|
|[consumable-04-NK0wjH9m32sjaeQe.htm](equipment/consumable-04-NK0wjH9m32sjaeQe.htm)|Messenger Missive|auto-trad|
|[consumable-04-NtUB9xqRZQ2duOUo.htm](equipment/consumable-04-NtUB9xqRZQ2duOUo.htm)|Glittering Snare|auto-trad|
|[consumable-04-NtUFIAMm2raeq5lr.htm](equipment/consumable-04-NtUFIAMm2raeq5lr.htm)|Energized Cartridge|auto-trad|
|[consumable-04-nUaNtynMzD1DpHkB.htm](equipment/consumable-04-nUaNtynMzD1DpHkB.htm)|Bottled Omen|auto-trad|
|[consumable-04-omZLCSIxiF9oXyH2.htm](equipment/consumable-04-omZLCSIxiF9oXyH2.htm)|Glimmering Missive|auto-trad|
|[consumable-04-pbQXfUhBfGeg4Bc1.htm](equipment/consumable-04-pbQXfUhBfGeg4Bc1.htm)|Chameleon Suit|auto-trad|
|[consumable-04-pSAnfP76xGS9aVec.htm](equipment/consumable-04-pSAnfP76xGS9aVec.htm)|Potion Patch (Lesser)|auto-trad|
|[consumable-04-pufVrJqj63H7kvAZ.htm](equipment/consumable-04-pufVrJqj63H7kvAZ.htm)|Sure-Step Potion|auto-trad|
|[consumable-04-pvsJXFfbwGWhoG5N.htm](equipment/consumable-04-pvsJXFfbwGWhoG5N.htm)|Taster's Folly|auto-trad|
|[consumable-04-QJ1fTrX42PoEWpK5.htm](equipment/consumable-04-QJ1fTrX42PoEWpK5.htm)|Leadenleg|auto-trad|
|[consumable-04-qJMcnWmzColtya0Z.htm](equipment/consumable-04-qJMcnWmzColtya0Z.htm)|Sentry Fulu|auto-trad|
|[consumable-04-QMTW4MjXMIUGp41b.htm](equipment/consumable-04-QMTW4MjXMIUGp41b.htm)|Exsanguinating Ammunition|auto-trad|
|[consumable-04-qsPCBxSDbXacCnMB.htm](equipment/consumable-04-qsPCBxSDbXacCnMB.htm)|Mindmurk Oil|auto-trad|
|[consumable-04-rBZSDgJeCknH7G2o.htm](equipment/consumable-04-rBZSDgJeCknH7G2o.htm)|Yarrow-Root Bandage|auto-trad|
|[consumable-04-RtTzHaHMrXiml7ut.htm](equipment/consumable-04-RtTzHaHMrXiml7ut.htm)|Galvasphere|auto-trad|
|[consumable-04-s9dtRS2SRTqzGdOF.htm](equipment/consumable-04-s9dtRS2SRTqzGdOF.htm)|Stalker Bane Snare|auto-trad|
|[consumable-04-SGS7fA2hcElw1EaL.htm](equipment/consumable-04-SGS7fA2hcElw1EaL.htm)|Deteriorating Dust|auto-trad|
|[consumable-04-ShFbUrFrQg7Ung8D.htm](equipment/consumable-04-ShFbUrFrQg7Ung8D.htm)|Capsaicin Tonic|auto-trad|
|[consumable-04-skMIwUIMjWzgkZ5B.htm](equipment/consumable-04-skMIwUIMjWzgkZ5B.htm)|Blessed Ampoule|auto-trad|
|[consumable-04-SWqzv0hYCIczICeR.htm](equipment/consumable-04-SWqzv0hYCIczICeR.htm)|Trip Snare|auto-trad|
|[consumable-04-T4ouD4mVFHA3EHs6.htm](equipment/consumable-04-T4ouD4mVFHA3EHs6.htm)|Bomber's Eye Elixir (Lesser)|auto-trad|
|[consumable-04-THfWJr1qr3jrncYZ.htm](equipment/consumable-04-THfWJr1qr3jrncYZ.htm)|Grave Token|auto-trad|
|[consumable-04-tslVf3qtQE7V1YvG.htm](equipment/consumable-04-tslVf3qtQE7V1YvG.htm)|Sinew-Shock Serum (Moderate)|auto-trad|
|[consumable-04-V7UcnQ0cXh1QSwo6.htm](equipment/consumable-04-V7UcnQ0cXh1QSwo6.htm)|Saboteur's Friend|auto-trad|
|[consumable-04-VJs47kgqu1ziZBRU.htm](equipment/consumable-04-VJs47kgqu1ziZBRU.htm)|Undead Detection Dye|auto-trad|
|[consumable-04-WeiUTSRc17HLNehg.htm](equipment/consumable-04-WeiUTSRc17HLNehg.htm)|Sniper's Bead|auto-trad|
|[consumable-04-xcH5IW7ahE3q8cPq.htm](equipment/consumable-04-xcH5IW7ahE3q8cPq.htm)|Dazzling Rosary|auto-trad|
|[consumable-04-XfcSVHwMA60JxUXJ.htm](equipment/consumable-04-XfcSVHwMA60JxUXJ.htm)|Focus Cathartic (Moderate)|auto-trad|
|[consumable-04-XLJc4GDQmeAv1cmG.htm](equipment/consumable-04-XLJc4GDQmeAv1cmG.htm)|Magnetic Suit|auto-trad|
|[consumable-04-XU8u9F3uoesGDjgM.htm](equipment/consumable-04-XU8u9F3uoesGDjgM.htm)|Warning Snare|auto-trad|
|[consumable-04-Y8115p3cmQJBqk5d.htm](equipment/consumable-04-Y8115p3cmQJBqk5d.htm)|Darkvision Elixir (Moderate)|auto-trad|
|[consumable-04-y8x66ougD79IuhI3.htm](equipment/consumable-04-y8x66ougD79IuhI3.htm)|Dragonbone Arrowhead|auto-trad|
|[consumable-04-YcvSw7Zn3oyqlJaw.htm](equipment/consumable-04-YcvSw7Zn3oyqlJaw.htm)|Stone Fist Elixir|auto-trad|
|[consumable-04-Yqz5M71vM1RcvlCx.htm](equipment/consumable-04-Yqz5M71vM1RcvlCx.htm)|Camp Shroud (Minor)|auto-trad|
|[consumable-04-YVLwV9IGzNqIzbmV.htm](equipment/consumable-04-YVLwV9IGzNqIzbmV.htm)|Animal Repellent (Lesser)|auto-trad|
|[consumable-04-YYD82q2NfAbuDmgf.htm](equipment/consumable-04-YYD82q2NfAbuDmgf.htm)|Fang Snare|auto-trad|
|[consumable-04-ZAtwiAPkk1zwCf82.htm](equipment/consumable-04-ZAtwiAPkk1zwCf82.htm)|Fear Gem|auto-trad|
|[consumable-04-zC7LipQPHRYw2RXx.htm](equipment/consumable-04-zC7LipQPHRYw2RXx.htm)|Barkskin Potion|auto-trad|
|[consumable-04-zJZ5DObRUdTk6HUL.htm](equipment/consumable-04-zJZ5DObRUdTk6HUL.htm)|Tar Rocket Snare|auto-trad|
|[consumable-04-zpaWTsviTO16biJi.htm](equipment/consumable-04-zpaWTsviTO16biJi.htm)|Clinging Ooze Snare|auto-trad|
|[consumable-04-ZtfkPBCvHTmHCTix.htm](equipment/consumable-04-ZtfkPBCvHTmHCTix.htm)|Explosive Missive|auto-trad|
|[consumable-04-ZZaEVS1Vw2a8cqWS.htm](equipment/consumable-04-ZZaEVS1Vw2a8cqWS.htm)|Harpoon Bolt|auto-trad|
|[consumable-05-0G3OmRIlDaPZyurj.htm](equipment/consumable-05-0G3OmRIlDaPZyurj.htm)|Wet Shock Snare|auto-trad|
|[consumable-05-0XUENeLPEG73uiP7.htm](equipment/consumable-05-0XUENeLPEG73uiP7.htm)|Tin Cobra|auto-trad|
|[consumable-05-0Y0fmvvKSgFvUtA2.htm](equipment/consumable-05-0Y0fmvvKSgFvUtA2.htm)|Crackling Bubble Gum (Lesser)|auto-trad|
|[consumable-05-1CAPGpQczfq5exrs.htm](equipment/consumable-05-1CAPGpQczfq5exrs.htm)|Diluted Hype|auto-trad|
|[consumable-05-1v1OK06JxdXn6MP4.htm](equipment/consumable-05-1v1OK06JxdXn6MP4.htm)|Sneaky Key|auto-trad|
|[consumable-05-26bM70yn28MjDT9A.htm](equipment/consumable-05-26bM70yn28MjDT9A.htm)|Elven Absinthe|auto-trad|
|[consumable-05-2EC73UbxVP9f0SR9.htm](equipment/consumable-05-2EC73UbxVP9f0SR9.htm)|Cryomister (Moderate)|auto-trad|
|[consumable-05-2pB7AsbE0c7HvoZZ.htm](equipment/consumable-05-2pB7AsbE0c7HvoZZ.htm)|Elemental Ammunition (Moderate)|auto-trad|
|[consumable-05-39cQgs70GbP7KZdy.htm](equipment/consumable-05-39cQgs70GbP7KZdy.htm)|Dupe's Gold Nugget|auto-trad|
|[consumable-05-3uhaf2YL9hmix3pe.htm](equipment/consumable-05-3uhaf2YL9hmix3pe.htm)|Emerald Grasshopper|auto-trad|
|[consumable-05-5a6pudUj6WG8RITy.htm](equipment/consumable-05-5a6pudUj6WG8RITy.htm)|Egg Cream Fizz|auto-trad|
|[consumable-05-5ftFTiKY0BMvhzL9.htm](equipment/consumable-05-5ftFTiKY0BMvhzL9.htm)|Stone Body Mutagen (Lesser)|auto-trad|
|[consumable-05-5RGNjhDxZ0yMhTds.htm](equipment/consumable-05-5RGNjhDxZ0yMhTds.htm)|Hunting Spider Venom|auto-trad|
|[consumable-05-605M9h4bMsNCQne6.htm](equipment/consumable-05-605M9h4bMsNCQne6.htm)|Unsullied Blood (Lesser)|auto-trad|
|[consumable-05-7D6ENmL3mRfUOMwf.htm](equipment/consumable-05-7D6ENmL3mRfUOMwf.htm)|Numbing Tonic (Lesser)|auto-trad|
|[consumable-05-7fS4k3K3p6SFLxKe.htm](equipment/consumable-05-7fS4k3K3p6SFLxKe.htm)|Freeze Ammunition|auto-trad|
|[consumable-05-7VjM9vjBMxhukiTf.htm](equipment/consumable-05-7VjM9vjBMxhukiTf.htm)|Moonlit Spellgun (Lesser)|auto-trad|
|[consumable-05-8pQw1HUzJ4duZbio.htm](equipment/consumable-05-8pQw1HUzJ4duZbio.htm)|Gadget Skates|auto-trad|
|[consumable-05-8wlCeramJW5cLw6e.htm](equipment/consumable-05-8wlCeramJW5cLw6e.htm)|Golden Branding Iron|auto-trad|
|[consumable-05-9MpwTXDxblLFgx2J.htm](equipment/consumable-05-9MpwTXDxblLFgx2J.htm)|Spiritual Warhorn (Lesser)|auto-trad|
|[consumable-05-9TAYUFDOJa0kvk9m.htm](equipment/consumable-05-9TAYUFDOJa0kvk9m.htm)|Mad Mammoth's Juke|auto-trad|
|[consumable-05-aH5MLDdkZ7aJA7hP.htm](equipment/consumable-05-aH5MLDdkZ7aJA7hP.htm)|Thousand-Pains Fulu (Stone)|auto-trad|
|[consumable-05-aKrslrjq22S9uRgF.htm](equipment/consumable-05-aKrslrjq22S9uRgF.htm)|Discord Fulu|auto-trad|
|[consumable-05-AqLxu3ir4UGzdOaz.htm](equipment/consumable-05-AqLxu3ir4UGzdOaz.htm)|Cheetah's Elixir (Moderate)|auto-trad|
|[consumable-05-aVW5OAwSlz5rsjtB.htm](equipment/consumable-05-aVW5OAwSlz5rsjtB.htm)|Pummeling Snare|auto-trad|
|[consumable-05-awPkC6AWcHS3T4oz.htm](equipment/consumable-05-awPkC6AWcHS3T4oz.htm)|Cooperative Waffles (Greater)|auto-trad|
|[consumable-05-BUZ8yiAXgXH8otmM.htm](equipment/consumable-05-BUZ8yiAXgXH8otmM.htm)|Feather Token (Chest) (Ammunition)|auto-trad|
|[consumable-05-Cs96jFDDcL9sX4Hv.htm](equipment/consumable-05-Cs96jFDDcL9sX4Hv.htm)|Sampling Ammunition|auto-trad|
|[consumable-05-D2h1E7fXsQfyjFcv.htm](equipment/consumable-05-D2h1E7fXsQfyjFcv.htm)|Rhino Hide Brooch|auto-trad|
|[consumable-05-DqgkcKJL6ASqwuio.htm](equipment/consumable-05-DqgkcKJL6ASqwuio.htm)|Euphoric Loop|auto-trad|
|[consumable-05-E7BcwZy8nTpTLYf1.htm](equipment/consumable-05-E7BcwZy8nTpTLYf1.htm)|Blindpepper Bomb|auto-trad|
|[consumable-05-EKhROfCklhZ7Je7h.htm](equipment/consumable-05-EKhROfCklhZ7Je7h.htm)|Contagion Metabolizer (Lesser)|auto-trad|
|[consumable-05-fv10pyjxJtrxMaQ4.htm](equipment/consumable-05-fv10pyjxJtrxMaQ4.htm)|Goblin-Eye Orb|auto-trad|
|[consumable-05-fz2OlQ9IYIcVASiv.htm](equipment/consumable-05-fz2OlQ9IYIcVASiv.htm)|Alkenstar Ice Wine|auto-trad|
|[consumable-05-fzAkwYDi8sXWfCNu.htm](equipment/consumable-05-fzAkwYDi8sXWfCNu.htm)|Dragon's Blood Pudding (Moderate)|auto-trad|
|[consumable-05-G8h3vA0bSewbdrDS.htm](equipment/consumable-05-G8h3vA0bSewbdrDS.htm)|Fulu of the Stoic Ox|auto-trad|
|[consumable-05-gMjoALuwSUdTmqqZ.htm](equipment/consumable-05-gMjoALuwSUdTmqqZ.htm)|Malleable Clay|auto-trad|
|[consumable-05-GNy0YEusxjcGMCNn.htm](equipment/consumable-05-GNy0YEusxjcGMCNn.htm)|Green Wyrmling Breath Potion|auto-trad|
|[consumable-05-GyqCW00omWWQ2C4e.htm](equipment/consumable-05-GyqCW00omWWQ2C4e.htm)|Sky Serpent Bolt|auto-trad|
|[consumable-05-hH63OWEVoMEr4ypr.htm](equipment/consumable-05-hH63OWEVoMEr4ypr.htm)|Nightpitch|auto-trad|
|[consumable-05-hHsU2sRW47Rge8fU.htm](equipment/consumable-05-hHsU2sRW47Rge8fU.htm)|Tteokguk of Time Advancement|auto-trad|
|[consumable-05-HU9eYAAfZMYnFMd9.htm](equipment/consumable-05-HU9eYAAfZMYnFMd9.htm)|Beckoning Cat Amulet|auto-trad|
|[consumable-05-igqlI1SbRPrXMtLT.htm](equipment/consumable-05-igqlI1SbRPrXMtLT.htm)|Poracha Fulu|auto-trad|
|[consumable-05-IOurUQgkm7oKnyrt.htm](equipment/consumable-05-IOurUQgkm7oKnyrt.htm)|Sprite Apple (Chartreuse)|auto-trad|
|[consumable-05-iUOcC9vFboSFI8fU.htm](equipment/consumable-05-iUOcC9vFboSFI8fU.htm)|False Death|auto-trad|
|[consumable-05-JAOWw2GhupaYoYg9.htm](equipment/consumable-05-JAOWw2GhupaYoYg9.htm)|Sixfingers Elixir (Lesser)|auto-trad|
|[consumable-05-Jc0OJe0fRDdHXXWu.htm](equipment/consumable-05-Jc0OJe0fRDdHXXWu.htm)|Sparking Spellgun (Lesser)|auto-trad|
|[consumable-05-JgZIWU1KaVL2pnAr.htm](equipment/consumable-05-JgZIWU1KaVL2pnAr.htm)|Fungal Walk Musk|auto-trad|
|[consumable-05-JnGeiRdprh1j0qnT.htm](equipment/consumable-05-JnGeiRdprh1j0qnT.htm)|Fearcracker|auto-trad|
|[consumable-05-jrLgEJxPvUKtSMMO.htm](equipment/consumable-05-jrLgEJxPvUKtSMMO.htm)|Recording Rod (Reusable)|auto-trad|
|[consumable-05-kaIKYTrvw4qa1khc.htm](equipment/consumable-05-kaIKYTrvw4qa1khc.htm)|Flame Drake Snare|auto-trad|
|[consumable-05-kR53wN4yZ30PxysG.htm](equipment/consumable-05-kR53wN4yZ30PxysG.htm)|Weapon Shot (Lesser)|auto-trad|
|[consumable-05-KVrCsckKUW68FWcV.htm](equipment/consumable-05-KVrCsckKUW68FWcV.htm)|Grease Snare|auto-trad|
|[consumable-05-l2M9P6kI3z3xPBOa.htm](equipment/consumable-05-l2M9P6kI3z3xPBOa.htm)|Serpent Oil (Moderate)|auto-trad|
|[consumable-05-L5AaEekLZ7Xt80FZ.htm](equipment/consumable-05-L5AaEekLZ7Xt80FZ.htm)|Gecko Pads|auto-trad|
|[consumable-05-lkbmxnhvWKhO0cFt.htm](equipment/consumable-05-lkbmxnhvWKhO0cFt.htm)|Familiar Morsel|auto-trad|
|[consumable-05-LLkl4mTbV9q4iDHJ.htm](equipment/consumable-05-LLkl4mTbV9q4iDHJ.htm)|Rhino Shot|auto-trad|
|[consumable-05-lPcnDlBGz5QwCMYw.htm](equipment/consumable-05-lPcnDlBGz5QwCMYw.htm)|Eagle Eye Elixir (Moderate)|auto-trad|
|[consumable-05-lvGSfPbpAd9lHeX5.htm](equipment/consumable-05-lvGSfPbpAd9lHeX5.htm)|Wounding Oil|auto-trad|
|[consumable-05-lzjx7KiuoaQuh025.htm](equipment/consumable-05-lzjx7KiuoaQuh025.htm)|Mustard Powder|auto-trad|
|[consumable-05-mmsuA7qPxFLLghtx.htm](equipment/consumable-05-mmsuA7qPxFLLghtx.htm)|Spellstrike Ammunition (Type II)|auto-trad|
|[consumable-05-MobYbxEL4KgxVi63.htm](equipment/consumable-05-MobYbxEL4KgxVi63.htm)|Salve of Slipperiness|auto-trad|
|[consumable-05-mxnlzHb86s56F5wc.htm](equipment/consumable-05-mxnlzHb86s56F5wc.htm)|Chimera Thread|auto-trad|
|[consumable-05-mzk9ekUydT8zpy4A.htm](equipment/consumable-05-mzk9ekUydT8zpy4A.htm)|Bola Shot|auto-trad|
|[consumable-05-NJOOeJc7bpE7gkUn.htm](equipment/consumable-05-NJOOeJc7bpE7gkUn.htm)|Fulu of the Drunken Monkey|auto-trad|
|[consumable-05-NKOAAPYL5RxReuKi.htm](equipment/consumable-05-NKOAAPYL5RxReuKi.htm)|Alchemist's Damper|auto-trad|
|[consumable-05-ntrmM8yBysmeqt71.htm](equipment/consumable-05-ntrmM8yBysmeqt71.htm)|Electromuscular Stimulator|auto-trad|
|[consumable-05-o9k5L682AlZfhpRu.htm](equipment/consumable-05-o9k5L682AlZfhpRu.htm)|Oil of Revelation|auto-trad|
|[consumable-05-OymrUAv6RGFaI1nm.htm](equipment/consumable-05-OymrUAv6RGFaI1nm.htm)|Soothing Powder (Lesser)|auto-trad|
|[consumable-05-PTEQ6NCrFkYQCURJ.htm](equipment/consumable-05-PTEQ6NCrFkYQCURJ.htm)|Life Salt|auto-trad|
|[consumable-05-q8aRbsKX93R5AXNb.htm](equipment/consumable-05-q8aRbsKX93R5AXNb.htm)|Healer's Gel (Lesser)|auto-trad|
|[consumable-05-qFEoXkORtrHfoMwx.htm](equipment/consumable-05-qFEoXkORtrHfoMwx.htm)|Reducer Round|auto-trad|
|[consumable-05-QNub2kTE7LpdMPII.htm](equipment/consumable-05-QNub2kTE7LpdMPII.htm)|Potion of Disguise (Lesser)|auto-trad|
|[consumable-05-qSmhcV5NLzlBjfAz.htm](equipment/consumable-05-qSmhcV5NLzlBjfAz.htm)|Tracking Fulu|auto-trad|
|[consumable-05-QSVIv5obLhKkmy67.htm](equipment/consumable-05-QSVIv5obLhKkmy67.htm)|Universal Solvent (Moderate)|auto-trad|
|[consumable-05-rab0x5F9KpRxxlnH.htm](equipment/consumable-05-rab0x5F9KpRxxlnH.htm)|Eroding Bullet|auto-trad|
|[consumable-05-rjUJEY424jyG9dGn.htm](equipment/consumable-05-rjUJEY424jyG9dGn.htm)|Cytillesh|auto-trad|
|[consumable-05-RL2VI9IGuOtXsEX6.htm](equipment/consumable-05-RL2VI9IGuOtXsEX6.htm)|Bane Ammunition (Moderate)|auto-trad|
|[consumable-05-RLkHxGBbVRAT6AOL.htm](equipment/consumable-05-RLkHxGBbVRAT6AOL.htm)|Stage Fright Missive|auto-trad|
|[consumable-05-RM7WoJYfd2edlNZ8.htm](equipment/consumable-05-RM7WoJYfd2edlNZ8.htm)|Soothing Tonic (Moderate)|auto-trad|
|[consumable-05-rzEQvcWfhR3T4FNd.htm](equipment/consumable-05-rzEQvcWfhR3T4FNd.htm)|Potion of Leaping|auto-trad|
|[consumable-05-T1FiDItPqd2xkYpt.htm](equipment/consumable-05-T1FiDItPqd2xkYpt.htm)|Golden Chrysalis|auto-trad|
|[consumable-05-Tccy21bO1sDb6hQM.htm](equipment/consumable-05-Tccy21bO1sDb6hQM.htm)|Clown Monarch|auto-trad|
|[consumable-05-Tj4uaNw2lgevxGl7.htm](equipment/consumable-05-Tj4uaNw2lgevxGl7.htm)|Shark Tooth Charm|auto-trad|
|[consumable-05-TZUskLT7yvP7N2co.htm](equipment/consumable-05-TZUskLT7yvP7N2co.htm)|Elixir of Life (Lesser)|auto-trad|
|[consumable-05-U9LhV1IBLbRug7uz.htm](equipment/consumable-05-U9LhV1IBLbRug7uz.htm)|Heartening Missive (Butterfly)|auto-trad|
|[consumable-05-uPP678KODkgDy8UO.htm](equipment/consumable-05-uPP678KODkgDy8UO.htm)|Ginger Chew|auto-trad|
|[consumable-05-UuNVe6SDNztreNEN.htm](equipment/consumable-05-UuNVe6SDNztreNEN.htm)|Apotropaic Fulu|auto-trad|
|[consumable-05-uwVTuejjSLl82jiA.htm](equipment/consumable-05-uwVTuejjSLl82jiA.htm)|Ichthyosis Mutagen|auto-trad|
|[consumable-05-vJZ49cgi8szuQXAD.htm](equipment/consumable-05-vJZ49cgi8szuQXAD.htm)|Magic Wand (2nd-Level Spell)|auto-trad|
|[consumable-05-VmMSP1EbMVnj2pZ2.htm](equipment/consumable-05-VmMSP1EbMVnj2pZ2.htm)|Firestarter Pellets|auto-trad|
|[consumable-05-VnUXcWG4xWGcb7sc.htm](equipment/consumable-05-VnUXcWG4xWGcb7sc.htm)|Pucker Pickle|auto-trad|
|[consumable-05-vNxKM5J5TqrbJKYk.htm](equipment/consumable-05-vNxKM5J5TqrbJKYk.htm)|Eye of Enlightenment|auto-trad|
|[consumable-05-Wn0H58sAkUtIaPPd.htm](equipment/consumable-05-Wn0H58sAkUtIaPPd.htm)|Gravemist Taper|auto-trad|
|[consumable-05-xAEbIQTNonVjCg38.htm](equipment/consumable-05-xAEbIQTNonVjCg38.htm)|Oily Button|auto-trad|
|[consumable-05-XaHXxSpSCgLGYhbs.htm](equipment/consumable-05-XaHXxSpSCgLGYhbs.htm)|Imp Shot|auto-trad|
|[consumable-05-xDWUGwLpKLm8WdQP.htm](equipment/consumable-05-xDWUGwLpKLm8WdQP.htm)|Fu Water|auto-trad|
|[consumable-05-YeQjs2DarMVIPVjq.htm](equipment/consumable-05-YeQjs2DarMVIPVjq.htm)|Hippogriff in a Jar|auto-trad|
|[consumable-05-YnkfinMN0l06iR1t.htm](equipment/consumable-05-YnkfinMN0l06iR1t.htm)|Wine of the Blood|auto-trad|
|[consumable-05-Yp1eRc7NmzSa5KYZ.htm](equipment/consumable-05-Yp1eRc7NmzSa5KYZ.htm)|Depth Charge I|auto-trad|
|[consumable-05-yqbW1T3Eu9KHRs1U.htm](equipment/consumable-05-yqbW1T3Eu9KHRs1U.htm)|Copper Penny|auto-trad|
|[consumable-05-zcJgT5RS8p2MEbOB.htm](equipment/consumable-05-zcJgT5RS8p2MEbOB.htm)|Sea Touch Elixir (Lesser)|auto-trad|
|[consumable-05-ZiD7kDxU4KEkO6XH.htm](equipment/consumable-05-ZiD7kDxU4KEkO6XH.htm)|Shimmering Dust|auto-trad|
|[consumable-05-ZmefGBXGJF3CFDbn.htm](equipment/consumable-05-ZmefGBXGJF3CFDbn.htm)|Scroll of 3rd-level Spell|auto-trad|
|[consumable-05-zrZ1FaaqW6VIajj7.htm](equipment/consumable-05-zrZ1FaaqW6VIajj7.htm)|Tiger Menuki|auto-trad|
|[consumable-06-1bLvKfodvgrIG4hp.htm](equipment/consumable-06-1bLvKfodvgrIG4hp.htm)|Light Writer Plates|auto-trad|
|[consumable-06-3bZcqRUWQcPe2y8M.htm](equipment/consumable-06-3bZcqRUWQcPe2y8M.htm)|Black Powder (Keg)|auto-trad|
|[consumable-06-4owFeOy4zxy8rv7W.htm](equipment/consumable-06-4owFeOy4zxy8rv7W.htm)|Feather Token (Tree)|auto-trad|
|[consumable-06-6iqRv3TGKt6DyIBs.htm](equipment/consumable-06-6iqRv3TGKt6DyIBs.htm)|Heartening Missive (Bull)|auto-trad|
|[consumable-06-6nmOC0blUxtrlLaY.htm](equipment/consumable-06-6nmOC0blUxtrlLaY.htm)|Winterstep Elixir (Lesser)|auto-trad|
|[consumable-06-7b6jSbZ7Xu88wyi8.htm](equipment/consumable-06-7b6jSbZ7Xu88wyi8.htm)|Eidetic Potion|auto-trad|
|[consumable-06-7iZCCqXDGiTv0ar3.htm](equipment/consumable-06-7iZCCqXDGiTv0ar3.htm)|Antipode Oil|auto-trad|
|[consumable-06-8iGmSwTTUdj6gqN5.htm](equipment/consumable-06-8iGmSwTTUdj6gqN5.htm)|Dust of Appearance|auto-trad|
|[consumable-06-AgBIZtwciSsCZeNN.htm](equipment/consumable-06-AgBIZtwciSsCZeNN.htm)|Potion of Fire Retaliation (Moderate)|auto-trad|
|[consumable-06-aiL53pkt0wFrsAQb.htm](equipment/consumable-06-aiL53pkt0wFrsAQb.htm)|Explosive Mine (Moderate)|auto-trad|
|[consumable-06-AJ1dC7EtTIfBey0M.htm](equipment/consumable-06-AJ1dC7EtTIfBey0M.htm)|Antidote (Moderate)|auto-trad|
|[consumable-06-Ax8XcBd0rRd1Z7hN.htm](equipment/consumable-06-Ax8XcBd0rRd1Z7hN.htm)|Mistform Elixir (Moderate)|auto-trad|
|[consumable-06-B3QOvE43Qn1H8t8n.htm](equipment/consumable-06-B3QOvE43Qn1H8t8n.htm)|Inventor's Fulu|auto-trad|
|[consumable-06-bDnk4TSzvD5BQmE5.htm](equipment/consumable-06-bDnk4TSzvD5BQmE5.htm)|Potion of Fire Resistance (Lesser)|auto-trad|
|[consumable-06-biRbqKo2C97XLfQ0.htm](equipment/consumable-06-biRbqKo2C97XLfQ0.htm)|Antiplague (Moderate)|auto-trad|
|[consumable-06-cAyxcRSSnfscGyMa.htm](equipment/consumable-06-cAyxcRSSnfscGyMa.htm)|Potion of Electricity Resistance (Lesser)|auto-trad|
|[consumable-06-cgWT0BUa6b6V7zDg.htm](equipment/consumable-06-cgWT0BUa6b6V7zDg.htm)|Feather Token (Balloon)|auto-trad|
|[consumable-06-cWYa0i1BqhbEruD6.htm](equipment/consumable-06-cWYa0i1BqhbEruD6.htm)|Addiction Suppressant (Moderate)|auto-trad|
|[consumable-06-Eb4dEuV22QVlMumS.htm](equipment/consumable-06-Eb4dEuV22QVlMumS.htm)|Potion of Swimming (Moderate)|auto-trad|
|[consumable-06-EeqvTTUpqHobTs6u.htm](equipment/consumable-06-EeqvTTUpqHobTs6u.htm)|Sprite Apple (Teal)|auto-trad|
|[consumable-06-eSHX07MqMT4SM1zY.htm](equipment/consumable-06-eSHX07MqMT4SM1zY.htm)|Potion of Sonic Resistance (Lesser)|auto-trad|
|[consumable-06-G7haQ5gDt30ftJLC.htm](equipment/consumable-06-G7haQ5gDt30ftJLC.htm)|Healing Potion (Moderate)|auto-trad|
|[consumable-06-GiiBkaa5JaJ5msPS.htm](equipment/consumable-06-GiiBkaa5JaJ5msPS.htm)|Peachwood Talisman|auto-trad|
|[consumable-06-HAEz4sSa6OH6C7Cs.htm](equipment/consumable-06-HAEz4sSa6OH6C7Cs.htm)|Truth Potion|auto-trad|
|[consumable-06-hguIfHmI3LjhvlWO.htm](equipment/consumable-06-hguIfHmI3LjhvlWO.htm)|Life Shot (Lesser)|auto-trad|
|[consumable-06-HilBL7oeSSXqDor7.htm](equipment/consumable-06-HilBL7oeSSXqDor7.htm)|Vaccine (Moderate)|auto-trad|
|[consumable-06-j3Pe1dyszbOT9c6Y.htm](equipment/consumable-06-j3Pe1dyszbOT9c6Y.htm)|Dispersing Bullet|auto-trad|
|[consumable-06-Jgv2PAJic7LPoGQx.htm](equipment/consumable-06-Jgv2PAJic7LPoGQx.htm)|Soothing Toddy|auto-trad|
|[consumable-06-LKTbRpaw8bH3WHv1.htm](equipment/consumable-06-LKTbRpaw8bH3WHv1.htm)|Implosion Dust (Lesser)|auto-trad|
|[consumable-06-LPSSrlS1Op6l9Kn5.htm](equipment/consumable-06-LPSSrlS1Op6l9Kn5.htm)|Mirror-Ball Snare|auto-trad|
|[consumable-06-M6pES1Nck1S6SWX9.htm](equipment/consumable-06-M6pES1Nck1S6SWX9.htm)|Potion of Cold Retaliation (Moderate)|auto-trad|
|[consumable-06-M7Ful1B1IWeibSm7.htm](equipment/consumable-06-M7Ful1B1IWeibSm7.htm)|Golden Silencer (Standard)|auto-trad|
|[consumable-06-MbPboT76BBKVGepB.htm](equipment/consumable-06-MbPboT76BBKVGepB.htm)|Nauseating Snare|auto-trad|
|[consumable-06-mBwtPr9SGyuq0vPW.htm](equipment/consumable-06-mBwtPr9SGyuq0vPW.htm)|Binding Coil|auto-trad|
|[consumable-06-MMInXBlOscwZVXsm.htm](equipment/consumable-06-MMInXBlOscwZVXsm.htm)|Heartening Missive (Rabbit)|auto-trad|
|[consumable-06-mRs24OwmGPjoIDvO.htm](equipment/consumable-06-mRs24OwmGPjoIDvO.htm)|Potion of Cold Resistance (Lesser)|auto-trad|
|[consumable-06-NhCFZ043brhFTsni.htm](equipment/consumable-06-NhCFZ043brhFTsni.htm)|Singing Muse|auto-trad|
|[consumable-06-oDfucsKeWpJmmWN1.htm](equipment/consumable-06-oDfucsKeWpJmmWN1.htm)|Potion of Electricity Retaliation (Moderate)|auto-trad|
|[consumable-06-OWPOTwMsrYma9d0v.htm](equipment/consumable-06-OWPOTwMsrYma9d0v.htm)|Potion of Acid Retaliation (Moderate)|auto-trad|
|[consumable-06-PaHw4xIxBujlCaqN.htm](equipment/consumable-06-PaHw4xIxBujlCaqN.htm)|Wind Ocarina|auto-trad|
|[consumable-06-Pjxotvrj7uXe92Qc.htm](equipment/consumable-06-Pjxotvrj7uXe92Qc.htm)|Silver Crescent (Lesser)|auto-trad|
|[consumable-06-pzUHY3C7JfmUgjuO.htm](equipment/consumable-06-pzUHY3C7JfmUgjuO.htm)|Tentacle Potion (Lesser)|auto-trad|
|[consumable-06-qasFfdiZKIczoZ9p.htm](equipment/consumable-06-qasFfdiZKIczoZ9p.htm)|Bloodhound Mask (Moderate)|auto-trad|
|[consumable-06-QGXNqpP5KvSldoZz.htm](equipment/consumable-06-QGXNqpP5KvSldoZz.htm)|Giant Scorpion Venom|auto-trad|
|[consumable-06-qnR646Oph1q88RNo.htm](equipment/consumable-06-qnR646Oph1q88RNo.htm)|Snarling Badger (Lesser)|auto-trad|
|[consumable-06-RBjyD36IrrFOwFXR.htm](equipment/consumable-06-RBjyD36IrrFOwFXR.htm)|Barricade Stone (Sphere)|auto-trad|
|[consumable-06-ROdjFw7wby982qf5.htm](equipment/consumable-06-ROdjFw7wby982qf5.htm)|Terrifying Ammunition|auto-trad|
|[consumable-06-Ru4xaA4kjdZ4IFS5.htm](equipment/consumable-06-Ru4xaA4kjdZ4IFS5.htm)|Oil of Weightlessness (Greater)|auto-trad|
|[consumable-06-sAMzkU7kzUZfqTPV.htm](equipment/consumable-06-sAMzkU7kzUZfqTPV.htm)|Extended Deteriorating Dust|auto-trad|
|[consumable-06-SfqfTak3o0cuSqhL.htm](equipment/consumable-06-SfqfTak3o0cuSqhL.htm)|Ooze Ammunition (Moderate)|auto-trad|
|[consumable-06-SlQ6KsMUnmG6FhtW.htm](equipment/consumable-06-SlQ6KsMUnmG6FhtW.htm)|Scholar's Drop|auto-trad|
|[consumable-06-sM0Cbkn8cQN4pUsp.htm](equipment/consumable-06-sM0Cbkn8cQN4pUsp.htm)|Shortbread Spy|auto-trad|
|[consumable-06-sPceDzKeOA8PpK2v.htm](equipment/consumable-06-sPceDzKeOA8PpK2v.htm)|Insight Coffee (Lesser)|auto-trad|
|[consumable-06-SRaVakPZeb81ZLfL.htm](equipment/consumable-06-SRaVakPZeb81ZLfL.htm)|Heartening Missive (Turtle)|auto-trad|
|[consumable-06-StsE5POM7OSE36Ia.htm](equipment/consumable-06-StsE5POM7OSE36Ia.htm)|Iron Cube|auto-trad|
|[consumable-06-tO6nucXnHAhCpD6n.htm](equipment/consumable-06-tO6nucXnHAhCpD6n.htm)|Conduit Shot (Lesser)|auto-trad|
|[consumable-06-TZJKwkvJD4NGXCG8.htm](equipment/consumable-06-TZJKwkvJD4NGXCG8.htm)|Oil of Swiftness|auto-trad|
|[consumable-06-u1gZaIZHnEfZDc1o.htm](equipment/consumable-06-u1gZaIZHnEfZDc1o.htm)|Potion of Acid Resistance (Lesser)|auto-trad|
|[consumable-06-Upso2k53BUfmeays.htm](equipment/consumable-06-Upso2k53BUfmeays.htm)|Liquid Gold|auto-trad|
|[consumable-06-UtT7TVT7M6uJTlJH.htm](equipment/consumable-06-UtT7TVT7M6uJTlJH.htm)|Restful Sleep Fulu|auto-trad|
|[consumable-06-Uzf9hqwec8fY2j4G.htm](equipment/consumable-06-Uzf9hqwec8fY2j4G.htm)|Ghostbane Fulu|auto-trad|
|[consumable-06-V2TUEoiDwJ125qzN.htm](equipment/consumable-06-V2TUEoiDwJ125qzN.htm)|Potion of Resistance (Lesser)|auto-trad|
|[consumable-06-WAQABCTFGuHvC0yR.htm](equipment/consumable-06-WAQABCTFGuHvC0yR.htm)|Flare Beacon (Moderate)|auto-trad|
|[consumable-06-wyD9xIa4TAxoHnuQ.htm](equipment/consumable-06-wyD9xIa4TAxoHnuQ.htm)|Gearbinder Oil (Lesser)|auto-trad|
|[consumable-06-xGmX6Vuuhivyal8v.htm](equipment/consumable-06-xGmX6Vuuhivyal8v.htm)|Skeptic's Elixir (Moderate)|auto-trad|
|[consumable-06-xX9mq2iACVOas26l.htm](equipment/consumable-06-xX9mq2iACVOas26l.htm)|Fate Shot|auto-trad|
|[consumable-06-YHev1WJ2tOiTBg9o.htm](equipment/consumable-06-YHev1WJ2tOiTBg9o.htm)|Oil of Unlife (Moderate)|auto-trad|
|[consumable-06-ymSbtkmW9xg9kWyN.htm](equipment/consumable-06-ymSbtkmW9xg9kWyN.htm)|Piercing Whistle Snare|auto-trad|
|[consumable-06-yO0CjTBpv5O9HhTJ.htm](equipment/consumable-06-yO0CjTBpv5O9HhTJ.htm)|Peacemaker|auto-trad|
|[consumable-06-YoYqtOXWo5reE2ql.htm](equipment/consumable-06-YoYqtOXWo5reE2ql.htm)|Sense-Dulling Hood (Lesser)|auto-trad|
|[consumable-06-yYkxdzogiH7FPWVP.htm](equipment/consumable-06-yYkxdzogiH7FPWVP.htm)|Fulu of Concealment|auto-trad|
|[consumable-06-ZEKmCg8K2hUHbmnT.htm](equipment/consumable-06-ZEKmCg8K2hUHbmnT.htm)|Salve of Antiparalysis|auto-trad|
|[consumable-06-ZROIWl7TLxKbPDLr.htm](equipment/consumable-06-ZROIWl7TLxKbPDLr.htm)|Demon Dust|auto-trad|
|[consumable-07-0XSl2DU7JvKXOqTo.htm](equipment/consumable-07-0XSl2DU7JvKXOqTo.htm)|Knockout Dram|auto-trad|
|[consumable-07-1VZpMEBamUpWADNZ.htm](equipment/consumable-07-1VZpMEBamUpWADNZ.htm)|Depth Charge II|auto-trad|
|[consumable-07-1XoBUEUearOzrwEs.htm](equipment/consumable-07-1XoBUEUearOzrwEs.htm)|Camp Shroud (Lesser)|auto-trad|
|[consumable-07-2UQANe2ca1c8MO6Z.htm](equipment/consumable-07-2UQANe2ca1c8MO6Z.htm)|Dragonfly Fulu|auto-trad|
|[consumable-07-3fSc2e5xRjJ7w4g3.htm](equipment/consumable-07-3fSc2e5xRjJ7w4g3.htm)|Gold Dragon's Breath Potion (Young)|auto-trad|
|[consumable-07-4tnPWyApPZP1P1yO.htm](equipment/consumable-07-4tnPWyApPZP1P1yO.htm)|Swift Block Cabochon|auto-trad|
|[consumable-07-53mPIEbUw5RzQ6pc.htm](equipment/consumable-07-53mPIEbUw5RzQ6pc.htm)|Escape Fulu|auto-trad|
|[consumable-07-5OiGlKiNFKlcP4k1.htm](equipment/consumable-07-5OiGlKiNFKlcP4k1.htm)|Revealing Mist (Greater)|auto-trad|
|[consumable-07-6eQvHNHf1IC2X5Rx.htm](equipment/consumable-07-6eQvHNHf1IC2X5Rx.htm)|Leaper's Elixir (Greater)|auto-trad|
|[consumable-07-76T49dJYfxIrPvQe.htm](equipment/consumable-07-76T49dJYfxIrPvQe.htm)|Malyass Root Paste|auto-trad|
|[consumable-07-7IrQPyMm76nLVoXx.htm](equipment/consumable-07-7IrQPyMm76nLVoXx.htm)|Sovereign Glue|auto-trad|
|[consumable-07-903CuhvVUhE1lmoB.htm](equipment/consumable-07-903CuhvVUhE1lmoB.htm)|Corrosive Ammunition|auto-trad|
|[consumable-07-9ignmYCACjfzkxDQ.htm](equipment/consumable-07-9ignmYCACjfzkxDQ.htm)|Serum of Sex Shift|auto-trad|
|[consumable-07-9zyrMZF76hMxwizY.htm](equipment/consumable-07-9zyrMZF76hMxwizY.htm)|Life-Boosting Oil (Moderate)|auto-trad|
|[consumable-07-A8Rv4EWEiQEaNSEX.htm](equipment/consumable-07-A8Rv4EWEiQEaNSEX.htm)|Magnetic Shot (Lesser)|auto-trad|
|[consumable-07-aBOPYlfHAcXUmhF7.htm](equipment/consumable-07-aBOPYlfHAcXUmhF7.htm)|Giant Wasp Venom|auto-trad|
|[consumable-07-AUHgdsygq9gaOZh4.htm](equipment/consumable-07-AUHgdsygq9gaOZh4.htm)|Smoke Screen Snare (Greater)|auto-trad|
|[consumable-07-B3zoAu9NpUEHGe22.htm](equipment/consumable-07-B3zoAu9NpUEHGe22.htm)|Seventh Prism (Pentagonal)|auto-trad|
|[consumable-07-BnYABzPAgdU89nLk.htm](equipment/consumable-07-BnYABzPAgdU89nLk.htm)|Serpent Oil (Greater)|auto-trad|
|[consumable-07-BzGI4g44y6LiHEPp.htm](equipment/consumable-07-BzGI4g44y6LiHEPp.htm)|Weapon-Weird Oil|auto-trad|
|[consumable-07-C35j9PXcyDdrTsat.htm](equipment/consumable-07-C35j9PXcyDdrTsat.htm)|Dragon Throat Scale|auto-trad|
|[consumable-07-C5RNvjvyq655pphC.htm](equipment/consumable-07-C5RNvjvyq655pphC.htm)|Empath's Cordial|auto-trad|
|[consumable-07-ChPOYRnhwpkEJExQ.htm](equipment/consumable-07-ChPOYRnhwpkEJExQ.htm)|Warding Punch|auto-trad|
|[consumable-07-d49GcmPZ2xWxU8Zj.htm](equipment/consumable-07-d49GcmPZ2xWxU8Zj.htm)|Etheric Essence Disruptor (Moderate)|auto-trad|
|[consumable-07-d5aCuFS1dhKXhsZ0.htm](equipment/consumable-07-d5aCuFS1dhKXhsZ0.htm)|Addlebrain|auto-trad|
|[consumable-07-DPNuM3699xULCbmF.htm](equipment/consumable-07-DPNuM3699xULCbmF.htm)|Curled Cure Gel|auto-trad|
|[consumable-07-dUREATQUwEOGiV13.htm](equipment/consumable-07-dUREATQUwEOGiV13.htm)|Oil of Ownership (Moderate)|auto-trad|
|[consumable-07-eEIWjTvZyKsKhYaz.htm](equipment/consumable-07-eEIWjTvZyKsKhYaz.htm)|Grim Trophy|auto-trad|
|[consumable-07-ehss8yPTXxiUdVlJ.htm](equipment/consumable-07-ehss8yPTXxiUdVlJ.htm)|Smokestick (Greater)|auto-trad|
|[consumable-07-EkC2W5A5fohoIKSd.htm](equipment/consumable-07-EkC2W5A5fohoIKSd.htm)|Ration Tonic (Greater)|auto-trad|
|[consumable-07-EN6Kdc6q5aRTlPS5.htm](equipment/consumable-07-EN6Kdc6q5aRTlPS5.htm)|Envenomed Snare|auto-trad|
|[consumable-07-FgAPV0iLE6R1QMJ5.htm](equipment/consumable-07-FgAPV0iLE6R1QMJ5.htm)|Skinstitch Salve|auto-trad|
|[consumable-07-Fgcc2OXLIKXWjbSp.htm](equipment/consumable-07-Fgcc2OXLIKXWjbSp.htm)|Swirling Sand|auto-trad|
|[consumable-07-fkGHYeGVK6O2VW1s.htm](equipment/consumable-07-fkGHYeGVK6O2VW1s.htm)|Red Dragon's Breath Potion (Young)|auto-trad|
|[consumable-07-fpqYMeX0GXvTSlVQ.htm](equipment/consumable-07-fpqYMeX0GXvTSlVQ.htm)|Red-Handed Missive|auto-trad|
|[consumable-07-fVKiVp6yG85ADrR6.htm](equipment/consumable-07-fVKiVp6yG85ADrR6.htm)|Draconic Toxin Bottle|auto-trad|
|[consumable-07-G1YeBTz26wnL2Meo.htm](equipment/consumable-07-G1YeBTz26wnL2Meo.htm)|Torrent Spellgun (Moderate)|auto-trad|
|[consumable-07-HUPCdlFkJQC7Krim.htm](equipment/consumable-07-HUPCdlFkJQC7Krim.htm)|Starshot Arrow (Lesser)|auto-trad|
|[consumable-07-j08PcMwLhz7lWY3M.htm](equipment/consumable-07-j08PcMwLhz7lWY3M.htm)|Persistent Lodestone|auto-trad|
|[consumable-07-J7ZoRlhqiP6988t2.htm](equipment/consumable-07-J7ZoRlhqiP6988t2.htm)|Red-Rib Gill Mask (Moderate)|auto-trad|
|[consumable-07-JvpiniS6wTHsDoFX.htm](equipment/consumable-07-JvpiniS6wTHsDoFX.htm)|Sloughing Toxin|auto-trad|
|[consumable-07-KadHRRccZ5rWRl7c.htm](equipment/consumable-07-KadHRRccZ5rWRl7c.htm)|Fire and Iceberg|auto-trad|
|[consumable-07-Kc4d504GnbS0NcoS.htm](equipment/consumable-07-Kc4d504GnbS0NcoS.htm)|Messenger Missive (Multiple)|auto-trad|
|[consumable-07-KlrP8xrqFcbDkGLF.htm](equipment/consumable-07-KlrP8xrqFcbDkGLF.htm)|Emergency Eye|auto-trad|
|[consumable-07-ktRGlgegUBZcr0aJ.htm](equipment/consumable-07-ktRGlgegUBZcr0aJ.htm)|Saints' Balm|auto-trad|
|[consumable-07-kzfRjSrq4JNZlc32.htm](equipment/consumable-07-kzfRjSrq4JNZlc32.htm)|Whisper Briolette|auto-trad|
|[consumable-07-LBhzIWBH9TJ8A2K5.htm](equipment/consumable-07-LBhzIWBH9TJ8A2K5.htm)|Invisible Net|auto-trad|
|[consumable-07-LKwI47eeJC0Y4hGu.htm](equipment/consumable-07-LKwI47eeJC0Y4hGu.htm)|Silver Dragon's Breath Potion (Young)|auto-trad|
|[consumable-07-mAgIN0A0ywnhtefw.htm](equipment/consumable-07-mAgIN0A0ywnhtefw.htm)|Owlbear Egg|auto-trad|
|[consumable-07-MdX3CWn1nJhOkqSr.htm](equipment/consumable-07-MdX3CWn1nJhOkqSr.htm)|Lightning Rod Shot|auto-trad|
|[consumable-07-mz0GbCF2WT1DSYac.htm](equipment/consumable-07-mz0GbCF2WT1DSYac.htm)|Spellstrike Ammunition (Type III)|auto-trad|
|[consumable-07-NKRuXILXLlDbxTs9.htm](equipment/consumable-07-NKRuXILXLlDbxTs9.htm)|Faerie Dragon Liqueur (Young)|auto-trad|
|[consumable-07-O3xp1TzJgSTEeqz9.htm](equipment/consumable-07-O3xp1TzJgSTEeqz9.htm)|Energizing Treat|auto-trad|
|[consumable-07-OcBPjVplvy2GbQ8P.htm](equipment/consumable-07-OcBPjVplvy2GbQ8P.htm)|Comprehension Elixir (Greater)|auto-trad|
|[consumable-07-PblDdXHm98XWs9ba.htm](equipment/consumable-07-PblDdXHm98XWs9ba.htm)|Topology Protoplasm|auto-trad|
|[consumable-07-pF4ggWXiiNksDKsU.htm](equipment/consumable-07-pF4ggWXiiNksDKsU.htm)|Smother Shroud|auto-trad|
|[consumable-07-PhtAfMPSrppdM9wT.htm](equipment/consumable-07-PhtAfMPSrppdM9wT.htm)|Ensnaring Disk|auto-trad|
|[consumable-07-QfdcOOHve9eNpsB9.htm](equipment/consumable-07-QfdcOOHve9eNpsB9.htm)|Fairy Bullet|auto-trad|
|[consumable-07-QLmgHzDlWuwaylPm.htm](equipment/consumable-07-QLmgHzDlWuwaylPm.htm)|Hovering Potion|auto-trad|
|[consumable-07-Qm0ZiyXail7Lv0dq.htm](equipment/consumable-07-Qm0ZiyXail7Lv0dq.htm)|Ablative Armor Plating (Greater)|auto-trad|
|[consumable-07-qNIv0owESzOzo1s0.htm](equipment/consumable-07-qNIv0owESzOzo1s0.htm)|Black Dragon's Breath Potion (Young)|auto-trad|
|[consumable-07-QSQZJ5BC3DeHv153.htm](equipment/consumable-07-QSQZJ5BC3DeHv153.htm)|Scroll of 4th-level Spell|auto-trad|
|[consumable-07-qwCefJjr7AJSqYay.htm](equipment/consumable-07-qwCefJjr7AJSqYay.htm)|Succubus Kiss|auto-trad|
|[consumable-07-rfU6tyzEadCXHSSN.htm](equipment/consumable-07-rfU6tyzEadCXHSSN.htm)|Material Essence Disruptor (Moderate)|auto-trad|
|[consumable-07-SH0WmAdjIaNN3OiN.htm](equipment/consumable-07-SH0WmAdjIaNN3OiN.htm)|Grudgestone|auto-trad|
|[consumable-07-SUn5qFbHYTLobgTt.htm](equipment/consumable-07-SUn5qFbHYTLobgTt.htm)|Ablative Shield Plating (Greater)|auto-trad|
|[consumable-07-t3NpyArNDTpY6qU8.htm](equipment/consumable-07-t3NpyArNDTpY6qU8.htm)|Cold Comfort (Lesser)|auto-trad|
|[consumable-07-T7o5nMvWS8YeJOst.htm](equipment/consumable-07-T7o5nMvWS8YeJOst.htm)|Black Tendril Shot (Lesser)|auto-trad|
|[consumable-07-TqX34x9CijryfrlM.htm](equipment/consumable-07-TqX34x9CijryfrlM.htm)|Brass Dragon's Breath Potion (Young)|auto-trad|
|[consumable-07-TSbuKm91qmqdqQW3.htm](equipment/consumable-07-TSbuKm91qmqdqQW3.htm)|Putrescent Glob|auto-trad|
|[consumable-07-TzXNMv7tnFy1FYgg.htm](equipment/consumable-07-TzXNMv7tnFy1FYgg.htm)|Bronze Dragon's Breath Potion (Young)|auto-trad|
|[consumable-07-ud970e7uPVEqx1FO.htm](equipment/consumable-07-ud970e7uPVEqx1FO.htm)|Big Rock Bullet|auto-trad|
|[consumable-07-uNdRC0j1hY3F4pP1.htm](equipment/consumable-07-uNdRC0j1hY3F4pP1.htm)|Stepping Stone Shot|auto-trad|
|[consumable-07-vPcDIowg3LAqZYAE.htm](equipment/consumable-07-vPcDIowg3LAqZYAE.htm)|Copper Dragon's Breath Potion (Young)|auto-trad|
|[consumable-07-vvgCQzWkScCzte7K.htm](equipment/consumable-07-vvgCQzWkScCzte7K.htm)|Green Dragon's Breath Potion (Young)|auto-trad|
|[consumable-07-VvljzRwthKMgqUR3.htm](equipment/consumable-07-VvljzRwthKMgqUR3.htm)|Feather Token (Anchor)|auto-trad|
|[consumable-07-VwzIiWAtbJCgGOEz.htm](equipment/consumable-07-VwzIiWAtbJCgGOEz.htm)|Isolation Draught|auto-trad|
|[consumable-07-wrDmWkGxmwzYtfiA.htm](equipment/consumable-07-wrDmWkGxmwzYtfiA.htm)|Magic Wand (3rd-Level Spell)|auto-trad|
|[consumable-07-XfQSkbjxVHF82ael.htm](equipment/consumable-07-XfQSkbjxVHF82ael.htm)|Condensed Mana|auto-trad|
|[consumable-07-XpmPX3ScEOBgAoKd.htm](equipment/consumable-07-XpmPX3ScEOBgAoKd.htm)|Candle of Revealing|auto-trad|
|[consumable-07-xpVHMxLcwgXzL39e.htm](equipment/consumable-07-xpVHMxLcwgXzL39e.htm)|Blue Dragon's Breath Potion (Young)|auto-trad|
|[consumable-07-xRrMmFzpa0WU9uV4.htm](equipment/consumable-07-xRrMmFzpa0WU9uV4.htm)|White Dragon's Breath Potion (Young)|auto-trad|
|[consumable-07-ydsXPuDjG2hPspw6.htm](equipment/consumable-07-ydsXPuDjG2hPspw6.htm)|Dimensional Knot|auto-trad|
|[consumable-07-zFYyegCfUlyCJhgu.htm](equipment/consumable-07-zFYyegCfUlyCJhgu.htm)|Server's Stew|auto-trad|
|[consumable-08-0Wxiz4hJPERJPDAF.htm](equipment/consumable-08-0Wxiz4hJPERJPDAF.htm)|Detect Anathema Fulu|auto-trad|
|[consumable-08-25Rr05SIfTj0GA31.htm](equipment/consumable-08-25Rr05SIfTj0GA31.htm)|Potion of Disguise (Moderate)|auto-trad|
|[consumable-08-2koNKqbQV05myfuL.htm](equipment/consumable-08-2koNKqbQV05myfuL.htm)|Dust of Corpse Animation|auto-trad|
|[consumable-08-3hhRWYl1O2FRfAbn.htm](equipment/consumable-08-3hhRWYl1O2FRfAbn.htm)|Ghost Delivery Fulu|auto-trad|
|[consumable-08-5Q6EFM1MSRX6WNTE.htm](equipment/consumable-08-5Q6EFM1MSRX6WNTE.htm)|Exsanguinating Ammunition (Greater)|auto-trad|
|[consumable-08-5xsfj30uXMKINxnk.htm](equipment/consumable-08-5xsfj30uXMKINxnk.htm)|Shrinking Potion (Greater)|auto-trad|
|[consumable-08-61mFRFnaCLHDtvdv.htm](equipment/consumable-08-61mFRFnaCLHDtvdv.htm)|Candle of Truth|auto-trad|
|[consumable-08-6KdYdFovFBivwI8M.htm](equipment/consumable-08-6KdYdFovFBivwI8M.htm)|Striking Snare|auto-trad|
|[consumable-08-8BDBsf55gP0UW07Y.htm](equipment/consumable-08-8BDBsf55gP0UW07Y.htm)|Scour|auto-trad|
|[consumable-08-9FwGY0X3v4ZksZH7.htm](equipment/consumable-08-9FwGY0X3v4ZksZH7.htm)|Skull Bomb|auto-trad|
|[consumable-08-axU0I9xIm4xm2VPH.htm](equipment/consumable-08-axU0I9xIm4xm2VPH.htm)|Bomb Snare|auto-trad|
|[consumable-08-AYKjrJnMo6mWCt2e.htm](equipment/consumable-08-AYKjrJnMo6mWCt2e.htm)|Firefoot Popcorn|auto-trad|
|[consumable-08-bz8Y8bVUe7QfBk9g.htm](equipment/consumable-08-bz8Y8bVUe7QfBk9g.htm)|Darkvision Elixir (Greater)|auto-trad|
|[consumable-08-C0kSU5KWMzLngvTa.htm](equipment/consumable-08-C0kSU5KWMzLngvTa.htm)|Oil of Object Animation|auto-trad|
|[consumable-08-cKqZe5imzxqSnzwD.htm](equipment/consumable-08-cKqZe5imzxqSnzwD.htm)|Jade Bauble|auto-trad|
|[consumable-08-cvkKe1FUVW9kOKcC.htm](equipment/consumable-08-cvkKe1FUVW9kOKcC.htm)|Seven-Color Raw Fish Salad|auto-trad|
|[consumable-08-D8aBxqJW61WFiQM2.htm](equipment/consumable-08-D8aBxqJW61WFiQM2.htm)|Liquid Gold (Greater)|auto-trad|
|[consumable-08-etgPdPhPviKkaFO8.htm](equipment/consumable-08-etgPdPhPviKkaFO8.htm)|Octopus Bottle|auto-trad|
|[consumable-08-eVowRVEo42PBFvNK.htm](equipment/consumable-08-eVowRVEo42PBFvNK.htm)|Barricade Stone (Cube)|auto-trad|
|[consumable-08-ezSqiHhz2OYTzoF2.htm](equipment/consumable-08-ezSqiHhz2OYTzoF2.htm)|Potion of Shared Life|auto-trad|
|[consumable-08-FClD2GoWpq3qdPQl.htm](equipment/consumable-08-FClD2GoWpq3qdPQl.htm)|Unsullied Blood (Moderate)|auto-trad|
|[consumable-08-gckU3BF9uQompDOy.htm](equipment/consumable-08-gckU3BF9uQompDOy.htm)|Chain of Stars|auto-trad|
|[consumable-08-gM1ZYvJacCdZsWo0.htm](equipment/consumable-08-gM1ZYvJacCdZsWo0.htm)|Orchestral Brooch|auto-trad|
|[consumable-08-gQuQetoDwP3GLtQy.htm](equipment/consumable-08-gQuQetoDwP3GLtQy.htm)|Clockwork Spider Bomb|auto-trad|
|[consumable-08-hdxm6sdXLzI7ozul.htm](equipment/consumable-08-hdxm6sdXLzI7ozul.htm)|Potion Patch (Moderate)|auto-trad|
|[consumable-08-k1S8AdYhi5JGRLh1.htm](equipment/consumable-08-k1S8AdYhi5JGRLh1.htm)|Smoke Fan (Greater)|auto-trad|
|[consumable-08-KdeeRCrtsDCJLfgc.htm](equipment/consumable-08-KdeeRCrtsDCJLfgc.htm)|Nettleweed Residue|auto-trad|
|[consumable-08-KgF3nPT2tEskXqSS.htm](equipment/consumable-08-KgF3nPT2tEskXqSS.htm)|Metalmist Sphere (Moderate)|auto-trad|
|[consumable-08-KlJSw919hpN6V9oK.htm](equipment/consumable-08-KlJSw919hpN6V9oK.htm)|Potion of Flying (Standard)|auto-trad|
|[consumable-08-kORpovlPYicysr2g.htm](equipment/consumable-08-kORpovlPYicysr2g.htm)|Potion of Quickness|auto-trad|
|[consumable-08-LbRuAXvoACxPqHqn.htm](equipment/consumable-08-LbRuAXvoACxPqHqn.htm)|Feyfoul (Moderate)|auto-trad|
|[consumable-08-mIqL9F9e1F1FmVgF.htm](equipment/consumable-08-mIqL9F9e1F1FmVgF.htm)|Ghost Courier Fulu|auto-trad|
|[consumable-08-mQSV1gq64kKeAfuN.htm](equipment/consumable-08-mQSV1gq64kKeAfuN.htm)|Scarlet Mist|auto-trad|
|[consumable-08-Mtqc1UzOotdclIEb.htm](equipment/consumable-08-Mtqc1UzOotdclIEb.htm)|Incense Bundle of Annual Blessings|auto-trad|
|[consumable-08-MywpJQc2lVjWvAGA.htm](equipment/consumable-08-MywpJQc2lVjWvAGA.htm)|Clockwork Goggles (Major)|auto-trad|
|[consumable-08-NXYEgMq4ERIzbJKx.htm](equipment/consumable-08-NXYEgMq4ERIzbJKx.htm)|Rusting Snare|auto-trad|
|[consumable-08-o7kaj57buBADMLhV.htm](equipment/consumable-08-o7kaj57buBADMLhV.htm)|Chameleon Suit (Greater)|auto-trad|
|[consumable-08-OCAwJ6CAqS0YkHCU.htm](equipment/consumable-08-OCAwJ6CAqS0YkHCU.htm)|Stormfeather|auto-trad|
|[consumable-08-OHxRdRv4ZLBGPQi6.htm](equipment/consumable-08-OHxRdRv4ZLBGPQi6.htm)|Animal Repellent (Moderate)|auto-trad|
|[consumable-08-oYjlcrarrZoA01D0.htm](equipment/consumable-08-oYjlcrarrZoA01D0.htm)|Roaring Potion (Lesser)|auto-trad|
|[consumable-08-p070A88bOm8gi0sS.htm](equipment/consumable-08-p070A88bOm8gi0sS.htm)|Runescribed Disk|auto-trad|
|[consumable-08-p0TKEjVqYFZmfz6v.htm](equipment/consumable-08-p0TKEjVqYFZmfz6v.htm)|Malleable Mixture (Lesser)|auto-trad|
|[consumable-08-TFnr9Gq7VXPJu0GQ.htm](equipment/consumable-08-TFnr9Gq7VXPJu0GQ.htm)|Grasping Snare|auto-trad|
|[consumable-08-TXkHGUqvqIko7W9m.htm](equipment/consumable-08-TXkHGUqvqIko7W9m.htm)|Thousand-Pains Fulu (Blade)|auto-trad|
|[consumable-08-u6g7AClRFEAj4lf4.htm](equipment/consumable-08-u6g7AClRFEAj4lf4.htm)|Gallows Tooth|auto-trad|
|[consumable-08-uy2U0qftoOEOpygw.htm](equipment/consumable-08-uy2U0qftoOEOpygw.htm)|Spirit Snare|auto-trad|
|[consumable-08-VvN9EjaBLwYg13z3.htm](equipment/consumable-08-VvN9EjaBLwYg13z3.htm)|Curare|auto-trad|
|[consumable-08-WL4O32qFifxnMj0H.htm](equipment/consumable-08-WL4O32qFifxnMj0H.htm)|Wyvern Poison|auto-trad|
|[consumable-08-wRBz9mjTO9ZqXch6.htm](equipment/consumable-08-wRBz9mjTO9ZqXch6.htm)|Feather Token (Tree) (Ammunition)|auto-trad|
|[consumable-08-wtNZDBNjFsrrFjOR.htm](equipment/consumable-08-wtNZDBNjFsrrFjOR.htm)|Cayden's Brew|auto-trad|
|[consumable-08-WyOF9Zy2FHtZsCa7.htm](equipment/consumable-08-WyOF9Zy2FHtZsCa7.htm)|Quenching Potion|auto-trad|
|[consumable-08-WzsKbMMewXbf1nws.htm](equipment/consumable-08-WzsKbMMewXbf1nws.htm)|Feather Token (Swan Boat)|auto-trad|
|[consumable-08-xcwPOFTAjQpmiFsN.htm](equipment/consumable-08-xcwPOFTAjQpmiFsN.htm)|Warpwobble Poison|auto-trad|
|[consumable-08-XdHMroAomWalt7I9.htm](equipment/consumable-08-XdHMroAomWalt7I9.htm)|Poison Fizz (Lesser)|auto-trad|
|[consumable-08-Xjjj7uHQzbOwENYV.htm](equipment/consumable-08-Xjjj7uHQzbOwENYV.htm)|Healing Vapor (Moderate)|auto-trad|
|[consumable-08-Y2dyidYfphGo7qvs.htm](equipment/consumable-08-Y2dyidYfphGo7qvs.htm)|Sighting Shot|auto-trad|
|[consumable-08-YnhcVcTbXrVfiH83.htm](equipment/consumable-08-YnhcVcTbXrVfiH83.htm)|Mnemonic Acid|auto-trad|
|[consumable-08-z3IG6t2o0zsfP2XQ.htm](equipment/consumable-08-z3IG6t2o0zsfP2XQ.htm)|Hype|auto-trad|
|[consumable-08-zg4aR2ndtTzO1osa.htm](equipment/consumable-08-zg4aR2ndtTzO1osa.htm)|Galvanic Chew|auto-trad|
|[consumable-09-0GY1BYECGqz6YJ88.htm](equipment/consumable-09-0GY1BYECGqz6YJ88.htm)|Noxious Incense|auto-trad|
|[consumable-09-3uqUIZZuEyAMORUi.htm](equipment/consumable-09-3uqUIZZuEyAMORUi.htm)|Aligned Oil|auto-trad|
|[consumable-09-5jUIOETCI3QG69cS.htm](equipment/consumable-09-5jUIOETCI3QG69cS.htm)|Amnemonic Charm|auto-trad|
|[consumable-09-6UoPcaXO70MNyU6W.htm](equipment/consumable-09-6UoPcaXO70MNyU6W.htm)|Sparking Spellgun (Moderate)|auto-trad|
|[consumable-09-6xS59NsgGLTEUW4m.htm](equipment/consumable-09-6xS59NsgGLTEUW4m.htm)|Bewildering Spellgun|auto-trad|
|[consumable-09-9XSOAzq3xp9g6qkF.htm](equipment/consumable-09-9XSOAzq3xp9g6qkF.htm)|Dust of Disappearance|auto-trad|
|[consumable-09-aSvDtG5i5l7scgru.htm](equipment/consumable-09-aSvDtG5i5l7scgru.htm)|Assassin Vine Wine|auto-trad|
|[consumable-09-Bajwrol5y2n2ZIVm.htm](equipment/consumable-09-Bajwrol5y2n2ZIVm.htm)|Crackling Bubble Gum (Moderate)|auto-trad|
|[consumable-09-BFre5eREvbqQdwPC.htm](equipment/consumable-09-BFre5eREvbqQdwPC.htm)|Chromatic Jellyfish Oil (Lesser)|auto-trad|
|[consumable-09-bImVngTkDNsdWIjr.htm](equipment/consumable-09-bImVngTkDNsdWIjr.htm)|Searing Suture (Greater)|auto-trad|
|[consumable-09-bwqyL0H9Ssa7dedV.htm](equipment/consumable-09-bwqyL0H9Ssa7dedV.htm)|Blast Boots (Greater)|auto-trad|
|[consumable-09-c21stU5rhN4F2fZl.htm](equipment/consumable-09-c21stU5rhN4F2fZl.htm)|Elixir of Life (Moderate)|auto-trad|
|[consumable-09-ca2lzxfJxvuLDrKu.htm](equipment/consumable-09-ca2lzxfJxvuLDrKu.htm)|Lich Dust|auto-trad|
|[consumable-09-cjAyXfWmn0IpOsaY.htm](equipment/consumable-09-cjAyXfWmn0IpOsaY.htm)|Impact Foam Chassis (Greater)|auto-trad|
|[consumable-09-Dc9BJcvL5zOoxAJd.htm](equipment/consumable-09-Dc9BJcvL5zOoxAJd.htm)|Ixamè's Eye|auto-trad|
|[consumable-09-DZHV8R2908H2cVWM.htm](equipment/consumable-09-DZHV8R2908H2cVWM.htm)|Mistranslator's Draft|auto-trad|
|[consumable-09-F4WcgJ3NB1kiF7OL.htm](equipment/consumable-09-F4WcgJ3NB1kiF7OL.htm)|Ghostly Portal Paint|auto-trad|
|[consumable-09-g6NpdrVR56B2jI64.htm](equipment/consumable-09-g6NpdrVR56B2jI64.htm)|Mourner's Dawnlight Fulu|auto-trad|
|[consumable-09-GXH1PSioEkxTDceK.htm](equipment/consumable-09-GXH1PSioEkxTDceK.htm)|Caustic Deteriorating Dust|auto-trad|
|[consumable-09-HNacrCOSJKJ6zn5t.htm](equipment/consumable-09-HNacrCOSJKJ6zn5t.htm)|Emetic Paste (Moderate)|auto-trad|
|[consumable-09-kOHWKOo4DFS8wQbE.htm](equipment/consumable-09-kOHWKOo4DFS8wQbE.htm)|Bottled Roc|auto-trad|
|[consumable-09-kv0Hg5RPROT4xqmh.htm](equipment/consumable-09-kv0Hg5RPROT4xqmh.htm)|Silencing Ammunition|auto-trad|
|[consumable-09-L4Db3CncPcZFPzGN.htm](equipment/consumable-09-L4Db3CncPcZFPzGN.htm)|Spiderfoot Brew (Greater)|auto-trad|
|[consumable-09-LbgblC0CzbTWJTmC.htm](equipment/consumable-09-LbgblC0CzbTWJTmC.htm)|Moonlit Spellgun (Moderate)|auto-trad|
|[consumable-09-mBvMWiFb6VB0P3Fk.htm](equipment/consumable-09-mBvMWiFb6VB0P3Fk.htm)|Feast of Hungry Ghosts|auto-trad|
|[consumable-09-MlzPOG2v9bO4XoLg.htm](equipment/consumable-09-MlzPOG2v9bO4XoLg.htm)|Brewer's Regret|auto-trad|
|[consumable-09-Mqayp3cYaZItkPsj.htm](equipment/consumable-09-Mqayp3cYaZItkPsj.htm)|Frost Worm Snare|auto-trad|
|[consumable-09-mtfdonWkYLj0BuW3.htm](equipment/consumable-09-mtfdonWkYLj0BuW3.htm)|Puff Dragon|auto-trad|
|[consumable-09-nrY1yxGbl5MFUyea.htm](equipment/consumable-09-nrY1yxGbl5MFUyea.htm)|Indomitable Keepsake (Greater)|auto-trad|
|[consumable-09-nYFnlWTNS6s7EcyF.htm](equipment/consumable-09-nYFnlWTNS6s7EcyF.htm)|Rebirth Potion|auto-trad|
|[consumable-09-oOXvk18K4izaJzG7.htm](equipment/consumable-09-oOXvk18K4izaJzG7.htm)|Spider Root|auto-trad|
|[consumable-09-PTSdJqjiybZDxi4r.htm](equipment/consumable-09-PTSdJqjiybZDxi4r.htm)|Potion of Minute Echoes|auto-trad|
|[consumable-09-QbuAXdWjwXlSk1pa.htm](equipment/consumable-09-QbuAXdWjwXlSk1pa.htm)|Careless Delight|auto-trad|
|[consumable-09-qqiD1FLntgShkCVY.htm](equipment/consumable-09-qqiD1FLntgShkCVY.htm)|Watchful Portrait|auto-trad|
|[consumable-09-rEx0S0p3cIzybhvV.htm](equipment/consumable-09-rEx0S0p3cIzybhvV.htm)|Gecko Pads (Greater)|auto-trad|
|[consumable-09-RkZgEViFodxXsLu3.htm](equipment/consumable-09-RkZgEViFodxXsLu3.htm)|Impossible Cake (Greater)|auto-trad|
|[consumable-09-RpvH9EDquO0jS3Jz.htm](equipment/consumable-09-RpvH9EDquO0jS3Jz.htm)|Storm Arrow|auto-trad|
|[consumable-09-rzRI8taFlNY8lbkl.htm](equipment/consumable-09-rzRI8taFlNY8lbkl.htm)|Body Recovery Kit|auto-trad|
|[consumable-09-sav38slFFlUVu75Z.htm](equipment/consumable-09-sav38slFFlUVu75Z.htm)|Abysium Powder|auto-trad|
|[consumable-09-Sn7v9SsbEDMUIwrO.htm](equipment/consumable-09-Sn7v9SsbEDMUIwrO.htm)|Magic Wand (4th-Level Spell)|auto-trad|
|[consumable-09-t4X6GDybqLmt7UkN.htm](equipment/consumable-09-t4X6GDybqLmt7UkN.htm)|Cheetah's Elixir (Greater)|auto-trad|
|[consumable-09-tEI0hGZyPWYwVhJU.htm](equipment/consumable-09-tEI0hGZyPWYwVhJU.htm)|Rebound Fulu|auto-trad|
|[consumable-09-tjLvRWklAylFhBHQ.htm](equipment/consumable-09-tjLvRWklAylFhBHQ.htm)|Scroll of 5th-level Spell|auto-trad|
|[consumable-09-tNs0FKSthYCM7ivx.htm](equipment/consumable-09-tNs0FKSthYCM7ivx.htm)|Golden Branding Iron (Greater)|auto-trad|
|[consumable-09-TWSdWjp6dpodZkeV.htm](equipment/consumable-09-TWSdWjp6dpodZkeV.htm)|Spiritual Warhorn (Moderate)|auto-trad|
|[consumable-09-Vk5VzPB2fj3Ym1Ia.htm](equipment/consumable-09-Vk5VzPB2fj3Ym1Ia.htm)|Bralani Breath (Greater)|auto-trad|
|[consumable-09-vL6AtFbcxbipGvtf.htm](equipment/consumable-09-vL6AtFbcxbipGvtf.htm)|Basilisk Eye|auto-trad|
|[consumable-09-wDhqRxuXPQfyD0eX.htm](equipment/consumable-09-wDhqRxuXPQfyD0eX.htm)|Javelin of Lightning|auto-trad|
|[consumable-09-WnJwPbObxmTNvYJy.htm](equipment/consumable-09-WnJwPbObxmTNvYJy.htm)|Numbing Tonic (Moderate)|auto-trad|
|[consumable-09-WSZl9S0Nui7pTMa8.htm](equipment/consumable-09-WSZl9S0Nui7pTMa8.htm)|Spellstrike Ammunition (Type IV)|auto-trad|
|[consumable-09-XAuWVlZYGgCguzwz.htm](equipment/consumable-09-XAuWVlZYGgCguzwz.htm)|Feather Token (Whip)|auto-trad|
|[consumable-09-Y0oi9tSJYgfEQIxt.htm](equipment/consumable-09-Y0oi9tSJYgfEQIxt.htm)|Enfilading Arrow|auto-trad|
|[consumable-09-YCFiwaS94TyfYtOe.htm](equipment/consumable-09-YCFiwaS94TyfYtOe.htm)|Transposition Ammunition|auto-trad|
|[consumable-09-z8lwQLmqyhjcvibq.htm](equipment/consumable-09-z8lwQLmqyhjcvibq.htm)|Wooden Nickel|auto-trad|
|[consumable-09-ZxkTbyq61JTQ7Zkt.htm](equipment/consumable-09-ZxkTbyq61JTQ7Zkt.htm)|Healer's Gel (Moderate)|auto-trad|
|[consumable-10-04giYigfDL5geu5f.htm](equipment/consumable-10-04giYigfDL5geu5f.htm)|Mistform Elixir (Greater)|auto-trad|
|[consumable-10-1PH7kb7ZVSrfQsgD.htm](equipment/consumable-10-1PH7kb7ZVSrfQsgD.htm)|Delve Scale|auto-trad|
|[consumable-10-27aqRraefLK7x7m3.htm](equipment/consumable-10-27aqRraefLK7x7m3.htm)|Potion of Grounding|auto-trad|
|[consumable-10-3IknuOpIV1OSNl98.htm](equipment/consumable-10-3IknuOpIV1OSNl98.htm)|Lucky Rabbit's Foot|auto-trad|
|[consumable-10-3YnHsf98MAe3lcdH.htm](equipment/consumable-10-3YnHsf98MAe3lcdH.htm)|Unsullied Blood (Greater)|auto-trad|
|[consumable-10-5ucWbzvEQhd2eSJk.htm](equipment/consumable-10-5ucWbzvEQhd2eSJk.htm)|Golden-Cased Bullet (Greater)|auto-trad|
|[consumable-10-66yY2SClvqj0lMFX.htm](equipment/consumable-10-66yY2SClvqj0lMFX.htm)|Conduit Shot (Moderate)|auto-trad|
|[consumable-10-7TT0XIb8ovLFOP2e.htm](equipment/consumable-10-7TT0XIb8ovLFOP2e.htm)|Honeyscent|auto-trad|
|[consumable-10-98KlJPs0tPFq4qKv.htm](equipment/consumable-10-98KlJPs0tPFq4qKv.htm)|Overloaded Brain Grenade|auto-trad|
|[consumable-10-9Brjb8WQMVCT0P2v.htm](equipment/consumable-10-9Brjb8WQMVCT0P2v.htm)|Weapon Shot (Moderate)|auto-trad|
|[consumable-10-9UGxCsNE778Y5rk0.htm](equipment/consumable-10-9UGxCsNE778Y5rk0.htm)|Flare Beacon (Greater)|auto-trad|
|[consumable-10-a46f60lt7NEUvNEy.htm](equipment/consumable-10-a46f60lt7NEUvNEy.htm)|Potion of Annulment (Lesser)|auto-trad|
|[consumable-10-Ap2Styg25sZMx3wn.htm](equipment/consumable-10-Ap2Styg25sZMx3wn.htm)|Mudrock Snare|auto-trad|
|[consumable-10-BbhoGheRSwcfQ5z2.htm](equipment/consumable-10-BbhoGheRSwcfQ5z2.htm)|Feather Token (Swan Boat) (Ammunition)|auto-trad|
|[consumable-10-cEfqMV1lFaT7s0QC.htm](equipment/consumable-10-cEfqMV1lFaT7s0QC.htm)|Potion of Stable Form|auto-trad|
|[consumable-10-CnPEbCCfRE8MJmdz.htm](equipment/consumable-10-CnPEbCCfRE8MJmdz.htm)|Tyrant Ampoule|auto-trad|
|[consumable-10-D2iSFSgRq2jSov76.htm](equipment/consumable-10-D2iSFSgRq2jSov76.htm)|Rose of Loves Lost|auto-trad|
|[consumable-10-dBevXop3G2P3PGjp.htm](equipment/consumable-10-dBevXop3G2P3PGjp.htm)|Vanishing Coin|auto-trad|
|[consumable-10-DDsCScnV4QfWlfeF.htm](equipment/consumable-10-DDsCScnV4QfWlfeF.htm)|Vulture's Wing|auto-trad|
|[consumable-10-dEU38O24zT6JCfEZ.htm](equipment/consumable-10-dEU38O24zT6JCfEZ.htm)|Nevercold|auto-trad|
|[consumable-10-DFxyS72Uh2vqT2MZ.htm](equipment/consumable-10-DFxyS72Uh2vqT2MZ.htm)|Tentacle Potion (Moderate)|auto-trad|
|[consumable-10-dzfmP3WsA15puenS.htm](equipment/consumable-10-dzfmP3WsA15puenS.htm)|Potion of Resistance (Moderate)|auto-trad|
|[consumable-10-evdDpgFpiFZt9UyA.htm](equipment/consumable-10-evdDpgFpiFZt9UyA.htm)|Potion of Cold Resistance (Moderate)|auto-trad|
|[consumable-10-ewzJzyJ4Vo9lZKvp.htm](equipment/consumable-10-ewzJzyJ4Vo9lZKvp.htm)|Iron Medallion|auto-trad|
|[consumable-10-eZIOsvyFRkqU43f8.htm](equipment/consumable-10-eZIOsvyFRkqU43f8.htm)|Baleblood Draft|auto-trad|
|[consumable-10-GIsGVMzTw7JfWweP.htm](equipment/consumable-10-GIsGVMzTw7JfWweP.htm)|Bottled Screams|auto-trad|
|[consumable-10-H2Xcd1nzesEeY96V.htm](equipment/consumable-10-H2Xcd1nzesEeY96V.htm)|Stepping Stone Shot (Greater)|auto-trad|
|[consumable-10-HIPkTfJLlLu4bWE3.htm](equipment/consumable-10-HIPkTfJLlLu4bWE3.htm)|Soothing Tonic (Greater)|auto-trad|
|[consumable-10-HTH445aey1x9RoeG.htm](equipment/consumable-10-HTH445aey1x9RoeG.htm)|Shockguard Coil|auto-trad|
|[consumable-10-HwWrR4CG3Fe7QZrO.htm](equipment/consumable-10-HwWrR4CG3Fe7QZrO.htm)|Oozepick (Greater)|auto-trad|
|[consumable-10-j9G3tnrKQ1N1dLzN.htm](equipment/consumable-10-j9G3tnrKQ1N1dLzN.htm)|Snagging Hook Snare|auto-trad|
|[consumable-10-JPNKZPtsdNpoLsBT.htm](equipment/consumable-10-JPNKZPtsdNpoLsBT.htm)|Camp Shroud (Moderate)|auto-trad|
|[consumable-10-kbjspHV7kwKKULmd.htm](equipment/consumable-10-kbjspHV7kwKKULmd.htm)|Potion of Sonic Resistance (Moderate)|auto-trad|
|[consumable-10-kksRfBsWEPYt8jpN.htm](equipment/consumable-10-kksRfBsWEPYt8jpN.htm)|Ruby Capacitor|auto-trad|
|[consumable-10-l3itUlHLuWP6qlA0.htm](equipment/consumable-10-l3itUlHLuWP6qlA0.htm)|Life Shot (Moderate)|auto-trad|
|[consumable-10-m99u0zXVbyPdK8Mf.htm](equipment/consumable-10-m99u0zXVbyPdK8Mf.htm)|Potion of Fire Resistance (Moderate)|auto-trad|
|[consumable-10-MBL8rOfvokYmUvwc.htm](equipment/consumable-10-MBL8rOfvokYmUvwc.htm)|Olfactory Obfuscator (Greater)|auto-trad|
|[consumable-10-MHoghQhxbvZRyyaB.htm](equipment/consumable-10-MHoghQhxbvZRyyaB.htm)|Potion of Electricity Resistance (Moderate)|auto-trad|
|[consumable-10-MvMa010Je10GN5dx.htm](equipment/consumable-10-MvMa010Je10GN5dx.htm)|Azure Lily Pollen|auto-trad|
|[consumable-10-NQvLv4qcTfK21y3H.htm](equipment/consumable-10-NQvLv4qcTfK21y3H.htm)|Spirit-Sealing Fulu (Greater)|auto-trad|
|[consumable-10-NRoA1HpA3ElPGBEQ.htm](equipment/consumable-10-NRoA1HpA3ElPGBEQ.htm)|Wolfsbane|auto-trad|
|[consumable-10-nvjEr2dlbtMV0q3d.htm](equipment/consumable-10-nvjEr2dlbtMV0q3d.htm)|Deathless Light|auto-trad|
|[consumable-10-NZ1z3ee75XfctFwM.htm](equipment/consumable-10-NZ1z3ee75XfctFwM.htm)|Winterstep Elixir (Moderate)|auto-trad|
|[consumable-10-oCuwJ9IUDAuzsUwa.htm](equipment/consumable-10-oCuwJ9IUDAuzsUwa.htm)|Bravo's Brew (Moderate)|auto-trad|
|[consumable-10-ORDaAzMZLrf6Hn8A.htm](equipment/consumable-10-ORDaAzMZLrf6Hn8A.htm)|Antiplague (Greater)|auto-trad|
|[consumable-10-PkrpCUXdV3I1boRQ.htm](equipment/consumable-10-PkrpCUXdV3I1boRQ.htm)|Golden Silencer (Greater)|auto-trad|
|[consumable-10-R8FHP4u80ee7PgWa.htm](equipment/consumable-10-R8FHP4u80ee7PgWa.htm)|Magnetic Suit (Greater)|auto-trad|
|[consumable-10-R9EZOG8Zeeo2RnwO.htm](equipment/consumable-10-R9EZOG8Zeeo2RnwO.htm)|Sniper's Bead (Greater)|auto-trad|
|[consumable-10-RH1fNsslZ1wdeqy2.htm](equipment/consumable-10-RH1fNsslZ1wdeqy2.htm)|Fearweed|auto-trad|
|[consumable-10-rOicLo9vsSWCUSLu.htm](equipment/consumable-10-rOicLo9vsSWCUSLu.htm)|Raining Knives Snare|auto-trad|
|[consumable-10-Sg9KRxEaRBCRCUhp.htm](equipment/consumable-10-Sg9KRxEaRBCRCUhp.htm)|Depth Charge III|auto-trad|
|[consumable-10-sQM4J9Ek5GThLsjf.htm](equipment/consumable-10-sQM4J9Ek5GThLsjf.htm)|Binding Snare|auto-trad|
|[consumable-10-SsWxgxols1Q1qPVf.htm](equipment/consumable-10-SsWxgxols1Q1qPVf.htm)|Firestarter Pellets (Greater)|auto-trad|
|[consumable-10-TgZdamADHuaKyriW.htm](equipment/consumable-10-TgZdamADHuaKyriW.htm)|Elemental Gem|auto-trad|
|[consumable-10-UlZ6s3Wox11ApisX.htm](equipment/consumable-10-UlZ6s3Wox11ApisX.htm)|Immovable Potion|auto-trad|
|[consumable-10-UnDjPxFOs6bldlcM.htm](equipment/consumable-10-UnDjPxFOs6bldlcM.htm)|Mummified Bat|auto-trad|
|[consumable-10-uswFgspZL8QrHecL.htm](equipment/consumable-10-uswFgspZL8QrHecL.htm)|Barricade Stone (Cylinder)|auto-trad|
|[consumable-10-VcEd9Bh4q5vQfpLv.htm](equipment/consumable-10-VcEd9Bh4q5vQfpLv.htm)|Lastwall Soup (Improved)|auto-trad|
|[consumable-10-W9j0jc5Ekvweo7MY.htm](equipment/consumable-10-W9j0jc5Ekvweo7MY.htm)|Burning Badger Guts Snare|auto-trad|
|[consumable-10-Wjkw0lEUOhypYvzo.htm](equipment/consumable-10-Wjkw0lEUOhypYvzo.htm)|Shadow Essence|auto-trad|
|[consumable-10-xK3rWA6HtaJvKSuC.htm](equipment/consumable-10-xK3rWA6HtaJvKSuC.htm)|Retrieval Prism (Greater)|auto-trad|
|[consumable-10-xWCdsGSwLLTeX6tQ.htm](equipment/consumable-10-xWCdsGSwLLTeX6tQ.htm)|Eagle Eye Elixir (Greater)|auto-trad|
|[consumable-10-XWkeL34yJK6t5qUE.htm](equipment/consumable-10-XWkeL34yJK6t5qUE.htm)|Antidote (Greater)|auto-trad|
|[consumable-10-yG6Za8FaG3hpXUGh.htm](equipment/consumable-10-yG6Za8FaG3hpXUGh.htm)|Potion of Acid Resistance (Moderate)|auto-trad|
|[consumable-10-znITT3WIS59TYtCu.htm](equipment/consumable-10-znITT3WIS59TYtCu.htm)|Anointing Oil (Greater)|auto-trad|
|[consumable-10-ZOOrK4yEW6rFpPJ1.htm](equipment/consumable-10-ZOOrK4yEW6rFpPJ1.htm)|Addiction Suppressant (Greater)|auto-trad|
|[consumable-11-1DHjXJEdJ7GlGSzg.htm](equipment/consumable-11-1DHjXJEdJ7GlGSzg.htm)|Oil of Keen Edges|auto-trad|
|[consumable-11-2wV9SHAWbC4rUlcc.htm](equipment/consumable-11-2wV9SHAWbC4rUlcc.htm)|Silver Crescent (Moderate)|auto-trad|
|[consumable-11-2x8F5s3PyKzYsRrZ.htm](equipment/consumable-11-2x8F5s3PyKzYsRrZ.htm)|Bloodhound Mask (Greater)|auto-trad|
|[consumable-11-3kELQauVoYunyFcd.htm](equipment/consumable-11-3kELQauVoYunyFcd.htm)|Life-Boosting Oil (Greater)|auto-trad|
|[consumable-11-3TxupnJlz7qXfdIr.htm](equipment/consumable-11-3TxupnJlz7qXfdIr.htm)|Oil of Ownership (Greater)|auto-trad|
|[consumable-11-4hJDqukTYPMZv2vg.htm](equipment/consumable-11-4hJDqukTYPMZv2vg.htm)|Soothing Powder (Greater)|auto-trad|
|[consumable-11-4sGIy77COooxhQuC.htm](equipment/consumable-11-4sGIy77COooxhQuC.htm)|Scroll of 6th-level Spell|auto-trad|
|[consumable-11-4upzgnkxdGn2Fy9c.htm](equipment/consumable-11-4upzgnkxdGn2Fy9c.htm)|Lion Claw|auto-trad|
|[consumable-11-5BF7zMnrPYzyigCs.htm](equipment/consumable-11-5BF7zMnrPYzyigCs.htm)|Magic Wand (5th-Level Spell)|auto-trad|
|[consumable-11-5Lode4gkHo4QUZbQ.htm](equipment/consumable-11-5Lode4gkHo4QUZbQ.htm)|Resonating Ammunition (Bolt)|auto-trad|
|[consumable-11-6CnuU7AhzzAMGzAM.htm](equipment/consumable-11-6CnuU7AhzzAMGzAM.htm)|Pyronite|auto-trad|
|[consumable-11-7PhPuAFP4mQz9jDr.htm](equipment/consumable-11-7PhPuAFP4mQz9jDr.htm)|Thousand-Pains Fulu (Needle)|auto-trad|
|[consumable-11-9kSITyK5yPTgTaQO.htm](equipment/consumable-11-9kSITyK5yPTgTaQO.htm)|Juxtaposition Ammunition|auto-trad|
|[consumable-11-9X77WeRsr543QKfz.htm](equipment/consumable-11-9X77WeRsr543QKfz.htm)|Mindlock Shot|auto-trad|
|[consumable-11-aQMmbIF0I8zMyzN4.htm](equipment/consumable-11-aQMmbIF0I8zMyzN4.htm)|Whirlwind Vial|auto-trad|
|[consumable-11-ARY8jRkg8aUJ6Zdv.htm](equipment/consumable-11-ARY8jRkg8aUJ6Zdv.htm)|Bonmuan Swapping Stone (Lesser)|auto-trad|
|[consumable-11-BB9ErvPViT4TtiYp.htm](equipment/consumable-11-BB9ErvPViT4TtiYp.htm)|Theatrical Mutagen (Greater)|auto-trad|
|[consumable-11-bVsNvqhfvDKGbRhd.htm](equipment/consumable-11-bVsNvqhfvDKGbRhd.htm)|Fundamental Oil|auto-trad|
|[consumable-11-byFW6sILekJaPERu.htm](equipment/consumable-11-byFW6sILekJaPERu.htm)|Material Essence Disruptor (Greater)|auto-trad|
|[consumable-11-Cc4SokzVBoBCkHId.htm](equipment/consumable-11-Cc4SokzVBoBCkHId.htm)|Blightburn Resin|auto-trad|
|[consumable-11-EmuRXJpGelePyaO5.htm](equipment/consumable-11-EmuRXJpGelePyaO5.htm)|Enigma-Sight Potion|auto-trad|
|[consumable-11-eQDTsetadI8u8Kc0.htm](equipment/consumable-11-eQDTsetadI8u8Kc0.htm)|Quicksilver Mutagen (Greater)|auto-trad|
|[consumable-11-F0x5a1MI3k9Du9j3.htm](equipment/consumable-11-F0x5a1MI3k9Du9j3.htm)|Blisterwort|auto-trad|
|[consumable-11-fDLABfn3nY1R670j.htm](equipment/consumable-11-fDLABfn3nY1R670j.htm)|Chromatic Jellyfish Oil (Moderate)|auto-trad|
|[consumable-11-FHsRKAdiLlxfSOqy.htm](equipment/consumable-11-FHsRKAdiLlxfSOqy.htm)|Big Rock Bullet (Greater)|auto-trad|
|[consumable-11-fTpJc3XoC2gzhuSN.htm](equipment/consumable-11-fTpJc3XoC2gzhuSN.htm)|Reverberating Stone|auto-trad|
|[consumable-11-gDheph8YteBtnyKp.htm](equipment/consumable-11-gDheph8YteBtnyKp.htm)|Bestial Mutagen (Greater)|auto-trad|
|[consumable-11-GWMrc8hZINhmDwvt.htm](equipment/consumable-11-GWMrc8hZINhmDwvt.htm)|Contagion Metabolizer (Moderate)|auto-trad|
|[consumable-11-idZvtwLg0E3AWnur.htm](equipment/consumable-11-idZvtwLg0E3AWnur.htm)|Captivating Score|auto-trad|
|[consumable-11-ieVRS2BjiWqauly6.htm](equipment/consumable-11-ieVRS2BjiWqauly6.htm)|Sixfingers Elixir (Moderate)|auto-trad|
|[consumable-11-ioCGarugWKBHqE5L.htm](equipment/consumable-11-ioCGarugWKBHqE5L.htm)|Blending Brooch|auto-trad|
|[consumable-11-IZRfgOYlZ3HRBkYX.htm](equipment/consumable-11-IZRfgOYlZ3HRBkYX.htm)|Oil of Repulsion|auto-trad|
|[consumable-11-j7VThCkenoE4giZt.htm](equipment/consumable-11-j7VThCkenoE4giZt.htm)|Cryomister (Greater)|auto-trad|
|[consumable-11-JMvp1ca4IhLy4cGj.htm](equipment/consumable-11-JMvp1ca4IhLy4cGj.htm)|Resonating Ammunition (Arrow)|auto-trad|
|[consumable-11-knQ5MnAIvKdXSpcQ.htm](equipment/consumable-11-knQ5MnAIvKdXSpcQ.htm)|Talespinner's Lyre|auto-trad|
|[consumable-11-LcNUAglIYiTxwmNo.htm](equipment/consumable-11-LcNUAglIYiTxwmNo.htm)|Etheric Essence Disruptor (Greater)|auto-trad|
|[consumable-11-LNzJ2WwTg58g7vam.htm](equipment/consumable-11-LNzJ2WwTg58g7vam.htm)|Blood Booster (Moderate)|auto-trad|
|[consumable-11-ne00KCUcrmRT3PM9.htm](equipment/consumable-11-ne00KCUcrmRT3PM9.htm)|Anathema Fulu|auto-trad|
|[consumable-11-oaGUNWmrqtKhy1HP.htm](equipment/consumable-11-oaGUNWmrqtKhy1HP.htm)|War Blood Mutagen (Greater)|auto-trad|
|[consumable-11-OMJgFmy3jut79Iaj.htm](equipment/consumable-11-OMJgFmy3jut79Iaj.htm)|Potion of Swimming (Greater)|auto-trad|
|[consumable-11-pAldqSrzPotjYQe6.htm](equipment/consumable-11-pAldqSrzPotjYQe6.htm)|Demolition Fulu (Moderate)|auto-trad|
|[consumable-11-PkksgUAYXJeewn7T.htm](equipment/consumable-11-PkksgUAYXJeewn7T.htm)|Silencing Shot|auto-trad|
|[consumable-11-q2TpCwdlLGdcasKD.htm](equipment/consumable-11-q2TpCwdlLGdcasKD.htm)|Elemental Ammunition (Greater)|auto-trad|
|[consumable-11-QSofPEx2sjztUNao.htm](equipment/consumable-11-QSofPEx2sjztUNao.htm)|Golden Spur|auto-trad|
|[consumable-11-qZvCxp4noVqYGkT0.htm](equipment/consumable-11-qZvCxp4noVqYGkT0.htm)|Bane Ammunition (Greater)|auto-trad|
|[consumable-11-RsAoEbp28Za4hlLH.htm](equipment/consumable-11-RsAoEbp28Za4hlLH.htm)|Choker-Arm Mutagen (Greater)|auto-trad|
|[consumable-11-se2g3LNsmQSsRv9w.htm](equipment/consumable-11-se2g3LNsmQSsRv9w.htm)|Sanguine Mutagen (Greater)|auto-trad|
|[consumable-11-SjbenbdzV4rgGxZP.htm](equipment/consumable-11-SjbenbdzV4rgGxZP.htm)|Skeptic's Elixir (Greater)|auto-trad|
|[consumable-11-ThX0ntpTqonGqguT.htm](equipment/consumable-11-ThX0ntpTqonGqguT.htm)|Potion of Disguise (Greater)|auto-trad|
|[consumable-11-TMtQnY6yvIRCpK9v.htm](equipment/consumable-11-TMtQnY6yvIRCpK9v.htm)|Juggernaut Mutagen (Greater)|auto-trad|
|[consumable-11-uf05VGoYSaeKtUG1.htm](equipment/consumable-11-uf05VGoYSaeKtUG1.htm)|Elder Seed|auto-trad|
|[consumable-11-uggpIk6vguWFXVli.htm](equipment/consumable-11-uggpIk6vguWFXVli.htm)|Silvertongue Mutagen (Greater)|auto-trad|
|[consumable-11-UOt1kd4ccF9sX9bt.htm](equipment/consumable-11-UOt1kd4ccF9sX9bt.htm)|Disrupting Oil (Greater)|auto-trad|
|[consumable-11-VTuKWBEGuBS3HqF4.htm](equipment/consumable-11-VTuKWBEGuBS3HqF4.htm)|Serpent Oil (Major)|auto-trad|
|[consumable-11-WMdHOfjjBtJAEvqU.htm](equipment/consumable-11-WMdHOfjjBtJAEvqU.htm)|Dancing Lamentation|auto-trad|
|[consumable-11-WuzLBK78DgIt8SsN.htm](equipment/consumable-11-WuzLBK78DgIt8SsN.htm)|Drakeheart Mutagen (Greater)|auto-trad|
|[consumable-11-xanr7nay2UQeR4Ec.htm](equipment/consumable-11-xanr7nay2UQeR4Ec.htm)|Deadweight Mutagen (Greater)|auto-trad|
|[consumable-11-XbyPBiZnoKDK9vbH.htm](equipment/consumable-11-XbyPBiZnoKDK9vbH.htm)|Ghostshot Wrapping|auto-trad|
|[consumable-11-xBZniAmkWcLfQ48d.htm](equipment/consumable-11-xBZniAmkWcLfQ48d.htm)|Torrent Spellgun (Greater)|auto-trad|
|[consumable-11-XyrFcBF5ygIFFGes.htm](equipment/consumable-11-XyrFcBF5ygIFFGes.htm)|Spellstrike Ammunition (Type V)|auto-trad|
|[consumable-11-Yej7lnnDYDZybGqo.htm](equipment/consumable-11-Yej7lnnDYDZybGqo.htm)|Serene Mutagen (Greater)|auto-trad|
|[consumable-11-ZGojRKG1yYiVWemR.htm](equipment/consumable-11-ZGojRKG1yYiVWemR.htm)|Cognitive Mutagen (Greater)|auto-trad|
|[consumable-12-0ySQli0JRrkZySOB.htm](equipment/consumable-12-0ySQli0JRrkZySOB.htm)|Snarling Badger (Moderate)|auto-trad|
|[consumable-12-16INAWEN5mkuGTga.htm](equipment/consumable-12-16INAWEN5mkuGTga.htm)|Black Tendril Shot (Moderate)|auto-trad|
|[consumable-12-2hc1EEcb3pfr7Hac.htm](equipment/consumable-12-2hc1EEcb3pfr7Hac.htm)|Sea Touch Elixir (Moderate)|auto-trad|
|[consumable-12-2prxM8Q0F4sdSwPx.htm](equipment/consumable-12-2prxM8Q0F4sdSwPx.htm)|Winter Wolf Elixir (Moderate)|auto-trad|
|[consumable-12-2VC7FLItVrFWNe4L.htm](equipment/consumable-12-2VC7FLItVrFWNe4L.htm)|Copper Dragon's Breath Potion (Adult)|auto-trad|
|[consumable-12-4F87PHMIzLHOUABL.htm](equipment/consumable-12-4F87PHMIzLHOUABL.htm)|Stonethroat Ammunition|auto-trad|
|[consumable-12-4RKfLoqVluZGWzLc.htm](equipment/consumable-12-4RKfLoqVluZGWzLc.htm)|Incense of Distilled Death|auto-trad|
|[consumable-12-5RJivM5SShjbqFf0.htm](equipment/consumable-12-5RJivM5SShjbqFf0.htm)|Magnetic Shot (Moderate)|auto-trad|
|[consumable-12-62HxCEDwhlZaeR0Q.htm](equipment/consumable-12-62HxCEDwhlZaeR0Q.htm)|Sinew-Shock Serum (Greater)|auto-trad|
|[consumable-12-7tqSRAuOgqt3CFhM.htm](equipment/consumable-12-7tqSRAuOgqt3CFhM.htm)|Astringent Venom|auto-trad|
|[consumable-12-82f7CTK0HxvitMLP.htm](equipment/consumable-12-82f7CTK0HxvitMLP.htm)|Dragonfly Potion|auto-trad|
|[consumable-12-8e37h6HBr4ZyBSGt.htm](equipment/consumable-12-8e37h6HBr4ZyBSGt.htm)|Applereed Mutagen (Moderate)|auto-trad|
|[consumable-12-8uyRlAkWdEyfOziq.htm](equipment/consumable-12-8uyRlAkWdEyfOziq.htm)|Mage Bane|auto-trad|
|[consumable-12-8vN6vm2fKB4rE86U.htm](equipment/consumable-12-8vN6vm2fKB4rE86U.htm)|Elysian Dew|auto-trad|
|[consumable-12-9mXjEGJaESesndWh.htm](equipment/consumable-12-9mXjEGJaESesndWh.htm)|Vaccine (Greater)|auto-trad|
|[consumable-12-a2XaerM1KkPyLIPM.htm](equipment/consumable-12-a2XaerM1KkPyLIPM.htm)|Stunning Snare|auto-trad|
|[consumable-12-A3rdTH2lpqQMoX6N.htm](equipment/consumable-12-A3rdTH2lpqQMoX6N.htm)|Gearbinder Oil (Moderate)|auto-trad|
|[consumable-12-Bn1o5foPq1kqZKf0.htm](equipment/consumable-12-Bn1o5foPq1kqZKf0.htm)|Red Dragon's Breath Potion (Adult)|auto-trad|
|[consumable-12-bRqczrlQDaeUvg6b.htm](equipment/consumable-12-bRqczrlQDaeUvg6b.htm)|Plasma Hype|auto-trad|
|[consumable-12-c6HS4K7omXsmZiZT.htm](equipment/consumable-12-c6HS4K7omXsmZiZT.htm)|Exsanguinating Ammunition (Major)|auto-trad|
|[consumable-12-CkrsPua4g1IlcBO0.htm](equipment/consumable-12-CkrsPua4g1IlcBO0.htm)|Dragonscale Cameo|auto-trad|
|[consumable-12-CtePdUJWKgjwpatZ.htm](equipment/consumable-12-CtePdUJWKgjwpatZ.htm)|Faerie Dragon Liqueur (Adult)|auto-trad|
|[consumable-12-cxOf1V1kN9tnj1g9.htm](equipment/consumable-12-cxOf1V1kN9tnj1g9.htm)|Eye of Apprehension|auto-trad|
|[consumable-12-Do8vjuUBOslgPtyw.htm](equipment/consumable-12-Do8vjuUBOslgPtyw.htm)|Balisse Feather|auto-trad|
|[consumable-12-drxEWZl8mqMOZ23E.htm](equipment/consumable-12-drxEWZl8mqMOZ23E.htm)|Potion of Tongues|auto-trad|
|[consumable-12-e2Von0aDHB03Iz3f.htm](equipment/consumable-12-e2Von0aDHB03Iz3f.htm)|Explosive Mine (Greater)|auto-trad|
|[consumable-12-E8pKcXFDiCWz68Db.htm](equipment/consumable-12-E8pKcXFDiCWz68Db.htm)|Chameleon Suit (Major)|auto-trad|
|[consumable-12-EEJiqzU89Ofk7dr6.htm](equipment/consumable-12-EEJiqzU89Ofk7dr6.htm)|Salamander Elixir (Moderate)|auto-trad|
|[consumable-12-eV0owvv0lYQph0nn.htm](equipment/consumable-12-eV0owvv0lYQph0nn.htm)|Green Dragon's Breath Potion (Adult)|auto-trad|
|[consumable-12-FmfgfAW5MZ1mTH4T.htm](equipment/consumable-12-FmfgfAW5MZ1mTH4T.htm)|Captivating Bauble|auto-trad|
|[consumable-12-FrbvejTNfS5fep9f.htm](equipment/consumable-12-FrbvejTNfS5fep9f.htm)|Fade Band|auto-trad|
|[consumable-12-FW1Ml15eeMItIxS2.htm](equipment/consumable-12-FW1Ml15eeMItIxS2.htm)|Marvelous Pigment|auto-trad|
|[consumable-12-FZ6SMwiei6NnEp3e.htm](equipment/consumable-12-FZ6SMwiei6NnEp3e.htm)|Stone Body Mutagen (Moderate)|auto-trad|
|[consumable-12-GDbDpe7W7vOEwnEr.htm](equipment/consumable-12-GDbDpe7W7vOEwnEr.htm)|Poison Fizz (Moderate)|auto-trad|
|[consumable-12-hMUYvp0neF0LAFqc.htm](equipment/consumable-12-hMUYvp0neF0LAFqc.htm)|Potion of Fire Retaliation (Greater)|auto-trad|
|[consumable-12-jiDzJKmdK2BAOh1x.htm](equipment/consumable-12-jiDzJKmdK2BAOh1x.htm)|Banquet of Hungry Ghosts|auto-trad|
|[consumable-12-JmxKmYQPQmUlauO4.htm](equipment/consumable-12-JmxKmYQPQmUlauO4.htm)|Fury Cocktail (Moderate)|auto-trad|
|[consumable-12-jNdAX4hBp0pzWalL.htm](equipment/consumable-12-jNdAX4hBp0pzWalL.htm)|Fire and Iceberg (Greater)|auto-trad|
|[consumable-12-jUiva1MoZDtbi6xI.htm](equipment/consumable-12-jUiva1MoZDtbi6xI.htm)|Quickmelt Slick (Greater)|auto-trad|
|[consumable-12-jVmTtUeNxDZMP5dU.htm](equipment/consumable-12-jVmTtUeNxDZMP5dU.htm)|Potion of Acid Retaliation (Greater)|auto-trad|
|[consumable-12-KMcYZKGlsjKMjSNi.htm](equipment/consumable-12-KMcYZKGlsjKMjSNi.htm)|Dimension Shot|auto-trad|
|[consumable-12-KQfx4Ae8c3iMlwXM.htm](equipment/consumable-12-KQfx4Ae8c3iMlwXM.htm)|Sense-Dulling Hood (Greater)|auto-trad|
|[consumable-12-KrtZmC3qfZIGXu76.htm](equipment/consumable-12-KrtZmC3qfZIGXu76.htm)|Iron Equalizer|auto-trad|
|[consumable-12-KVrmsNbro6mJ9wuT.htm](equipment/consumable-12-KVrmsNbro6mJ9wuT.htm)|Oil of Animation|auto-trad|
|[consumable-12-LhlsZogLBPLx5Tcf.htm](equipment/consumable-12-LhlsZogLBPLx5Tcf.htm)|Shrieking Skull|auto-trad|
|[consumable-12-mR9RD9S08jIX1IPm.htm](equipment/consumable-12-mR9RD9S08jIX1IPm.htm)|Universal Solvent (Greater)|auto-trad|
|[consumable-12-mWXaROc9ytjdGVVP.htm](equipment/consumable-12-mWXaROc9ytjdGVVP.htm)|Scything Blade Snare|auto-trad|
|[consumable-12-NqNwLeqto2isVWNL.htm](equipment/consumable-12-NqNwLeqto2isVWNL.htm)|Gold Dragon's Breath Potion (Adult)|auto-trad|
|[consumable-12-nsNFSjNZNVGEGTK4.htm](equipment/consumable-12-nsNFSjNZNVGEGTK4.htm)|Depth Charge IV|auto-trad|
|[consumable-12-OjNaLZI13rWlyqmM.htm](equipment/consumable-12-OjNaLZI13rWlyqmM.htm)|Animal Repellent (Greater)|auto-trad|
|[consumable-12-Pp4JyxzRSqpOiQyL.htm](equipment/consumable-12-Pp4JyxzRSqpOiQyL.htm)|Weapon Shot (Greater)|auto-trad|
|[consumable-12-QsYfjRMBz48H6UAp.htm](equipment/consumable-12-QsYfjRMBz48H6UAp.htm)|Potion of Electricity Retaliation (Greater)|auto-trad|
|[consumable-12-RhOx5wenvlljzOku.htm](equipment/consumable-12-RhOx5wenvlljzOku.htm)|Oil of Unlife (Greater)|auto-trad|
|[consumable-12-rOQlcAV4m8Zaebue.htm](equipment/consumable-12-rOQlcAV4m8Zaebue.htm)|Brass Dragon's Breath Potion (Adult)|auto-trad|
|[consumable-12-SEToMRNTXAVVRwyd.htm](equipment/consumable-12-SEToMRNTXAVVRwyd.htm)|Ooze Ammunition (Greater)|auto-trad|
|[consumable-12-sMrOHuFHjtLDOzp4.htm](equipment/consumable-12-sMrOHuFHjtLDOzp4.htm)|Healing Vapor (Greater)|auto-trad|
|[consumable-12-t9wFCiFkCRcSLtaG.htm](equipment/consumable-12-t9wFCiFkCRcSLtaG.htm)|Silver Dragon's Breath Potion (Adult)|auto-trad|
|[consumable-12-TmXTijJIpOxLJLXb.htm](equipment/consumable-12-TmXTijJIpOxLJLXb.htm)|Thrice-Fried Mudwings|auto-trad|
|[consumable-12-Tqtcy25OCiNlWqF6.htm](equipment/consumable-12-Tqtcy25OCiNlWqF6.htm)|Clubhead Poison|auto-trad|
|[consumable-12-TSrbwHW8zm0mhkwb.htm](equipment/consumable-12-TSrbwHW8zm0mhkwb.htm)|Healing Potion (Greater)|auto-trad|
|[consumable-12-tW2rmGSanKQlUkiU.htm](equipment/consumable-12-tW2rmGSanKQlUkiU.htm)|Potion of Cold Retaliation (Greater)|auto-trad|
|[consumable-12-tZiCnERZB6ww4eBu.htm](equipment/consumable-12-tZiCnERZB6ww4eBu.htm)|Aged Assassin Vine Wine|auto-trad|
|[consumable-12-ve73Sb8MwLY9GDBD.htm](equipment/consumable-12-ve73Sb8MwLY9GDBD.htm)|Implosion Dust (Moderate)|auto-trad|
|[consumable-12-vMs9n8oXlZttcJkX.htm](equipment/consumable-12-vMs9n8oXlZttcJkX.htm)|Focus Cathartic (Greater)|auto-trad|
|[consumable-12-vXLZfi5iCSKb96tW.htm](equipment/consumable-12-vXLZfi5iCSKb96tW.htm)|Bronze Dragon's Breath Potion (Adult)|auto-trad|
|[consumable-12-vzP0pLxes9bn0zV8.htm](equipment/consumable-12-vzP0pLxes9bn0zV8.htm)|Insight Coffee (Moderate)|auto-trad|
|[consumable-12-WvDbdESFJY6Be2u1.htm](equipment/consumable-12-WvDbdESFJY6Be2u1.htm)|Blackfinger Blight|auto-trad|
|[consumable-12-WwWssHoEhyh7Yrrw.htm](equipment/consumable-12-WwWssHoEhyh7Yrrw.htm)|White Dragon's Breath Potion (Adult)|auto-trad|
|[consumable-12-x04huvOBBXmExTDl.htm](equipment/consumable-12-x04huvOBBXmExTDl.htm)|Black Dragon's Breath Potion (Adult)|auto-trad|
|[consumable-12-xAoRfG7uxFFjOHoT.htm](equipment/consumable-12-xAoRfG7uxFFjOHoT.htm)|Phoenix Flask|auto-trad|
|[consumable-12-xnIEWJYmJh6j58Wd.htm](equipment/consumable-12-xnIEWJYmJh6j58Wd.htm)|Dazzling Rosary (Greater)|auto-trad|
|[consumable-12-ybu2BMu2oWj74wl8.htm](equipment/consumable-12-ybu2BMu2oWj74wl8.htm)|Salve of Antiparalysis (Greater)|auto-trad|
|[consumable-12-YScPBPwB4t9sydp0.htm](equipment/consumable-12-YScPBPwB4t9sydp0.htm)|Spell-Eating Pitch|auto-trad|
|[consumable-12-z7eOUqVwyht6tj11.htm](equipment/consumable-12-z7eOUqVwyht6tj11.htm)|Slumber Wine|auto-trad|
|[consumable-12-zAlDcTlvCd3MxXmI.htm](equipment/consumable-12-zAlDcTlvCd3MxXmI.htm)|Shadow Ash|auto-trad|
|[consumable-12-zQnidYCmLCBPfdii.htm](equipment/consumable-12-zQnidYCmLCBPfdii.htm)|Blue Dragon's Breath Potion (Adult)|auto-trad|
|[consumable-13-10ddNHYJJOvEhEFD.htm](equipment/consumable-13-10ddNHYJJOvEhEFD.htm)|Whelming Scrimshaw|auto-trad|
|[consumable-13-1YROvQsCdq8WFWRj.htm](equipment/consumable-13-1YROvQsCdq8WFWRj.htm)|Time Shield Potion|auto-trad|
|[consumable-13-3M7iB9tfrsWTccZ0.htm](equipment/consumable-13-3M7iB9tfrsWTccZ0.htm)|Healer's Gel (Greater)|auto-trad|
|[consumable-13-6vyr3drpizIrd5PS.htm](equipment/consumable-13-6vyr3drpizIrd5PS.htm)|Daylight Vapor|auto-trad|
|[consumable-13-87AzvRja9uQOLJCC.htm](equipment/consumable-13-87AzvRja9uQOLJCC.htm)|Deathcap Powder|auto-trad|
|[consumable-13-AGD0qLB8zKGinKwv.htm](equipment/consumable-13-AGD0qLB8zKGinKwv.htm)|Camp Shroud (Greater)|auto-trad|
|[consumable-13-ah9jxSBerznPowW7.htm](equipment/consumable-13-ah9jxSBerznPowW7.htm)|Binding Coil (Greater)|auto-trad|
|[consumable-13-AvbQWGCBZIOsK45X.htm](equipment/consumable-13-AvbQWGCBZIOsK45X.htm)|Sparking Spellgun (Greater)|auto-trad|
|[consumable-13-bLmuVUkaMeyq5QlI.htm](equipment/consumable-13-bLmuVUkaMeyq5QlI.htm)|Djezet Dose|auto-trad|
|[consumable-13-BxzwmwQ5O4fewa4w.htm](equipment/consumable-13-BxzwmwQ5O4fewa4w.htm)|Purple Worm Repellent|auto-trad|
|[consumable-13-C0Zhu7Vwy9Aipwoh.htm](equipment/consumable-13-C0Zhu7Vwy9Aipwoh.htm)|Spectral Nightshade|auto-trad|
|[consumable-13-cRtDr8WhXhxkGrmx.htm](equipment/consumable-13-cRtDr8WhXhxkGrmx.htm)|Reflecting Shard|auto-trad|
|[consumable-13-dwRqo9KQki2iCwHu.htm](equipment/consumable-13-dwRqo9KQki2iCwHu.htm)|Ablative Shield Plating (Major)|auto-trad|
|[consumable-13-dyvwp4EHgJDzVjJh.htm](equipment/consumable-13-dyvwp4EHgJDzVjJh.htm)|Metalmist Sphere (Greater)|auto-trad|
|[consumable-13-ex1kVj3SUbTd1smt.htm](equipment/consumable-13-ex1kVj3SUbTd1smt.htm)|Crackling Bubble Gum (Greater)|auto-trad|
|[consumable-13-FETCOUQa6rYtlAHB.htm](equipment/consumable-13-FETCOUQa6rYtlAHB.htm)|Kaiju Fulu|auto-trad|
|[consumable-13-fomEZZ4MxVVK3uVu.htm](equipment/consumable-13-fomEZZ4MxVVK3uVu.htm)|Scroll of 7th-level Spell|auto-trad|
|[consumable-13-g1aarurTGfb7QDTO.htm](equipment/consumable-13-g1aarurTGfb7QDTO.htm)|Cayden's Brew (Double)|auto-trad|
|[consumable-13-Hh2qMYtJU4WTOUFC.htm](equipment/consumable-13-Hh2qMYtJU4WTOUFC.htm)|Moonlit Spellgun (Greater)|auto-trad|
|[consumable-13-iRDCa4OVSTGUD3vi.htm](equipment/consumable-13-iRDCa4OVSTGUD3vi.htm)|Purple Worm Venom|auto-trad|
|[consumable-13-JSmdTDUEGwHVbV7g.htm](equipment/consumable-13-JSmdTDUEGwHVbV7g.htm)|Mending Lattice|auto-trad|
|[consumable-13-kiXh4SUWKr166ZeM.htm](equipment/consumable-13-kiXh4SUWKr166ZeM.htm)|Magic Wand (6th-Level Spell)|auto-trad|
|[consumable-13-kVng8qcoHLBORFIc.htm](equipment/consumable-13-kVng8qcoHLBORFIc.htm)|Golden Branding Iron (Major)|auto-trad|
|[consumable-13-lQn0OkifOW0Grp9z.htm](equipment/consumable-13-lQn0OkifOW0Grp9z.htm)|Rusting Ammunition (Moderate)|auto-trad|
|[consumable-13-LRSiYsgEz3e0PEwX.htm](equipment/consumable-13-LRSiYsgEz3e0PEwX.htm)|Panacea|auto-trad|
|[consumable-13-m1XubXwbBy3JzMkY.htm](equipment/consumable-13-m1XubXwbBy3JzMkY.htm)|Pummel-Growth Toxin|auto-trad|
|[consumable-13-mMP0ycsdW2nLlCeO.htm](equipment/consumable-13-mMP0ycsdW2nLlCeO.htm)|Spiritual Warhorn (Greater)|auto-trad|
|[consumable-13-mYozcVg9fQY6zO7C.htm](equipment/consumable-13-mYozcVg9fQY6zO7C.htm)|Elixir of Life (Greater)|auto-trad|
|[consumable-13-n4FF7K3SLb7q1v6v.htm](equipment/consumable-13-n4FF7K3SLb7q1v6v.htm)|Spellstrike Ammunition (Type VI)|auto-trad|
|[consumable-13-Pde3rUX3rx7VSCHn.htm](equipment/consumable-13-Pde3rUX3rx7VSCHn.htm)|Singularity Ammunition|auto-trad|
|[consumable-13-pXV4ov0DJHzIHSEW.htm](equipment/consumable-13-pXV4ov0DJHzIHSEW.htm)|Rovagug's Mud|auto-trad|
|[consumable-13-QrjeT3xBssyjvEHQ.htm](equipment/consumable-13-QrjeT3xBssyjvEHQ.htm)|Dragon's Blood Pudding (Greater)|auto-trad|
|[consumable-13-RtU28UqJuqJQMJym.htm](equipment/consumable-13-RtU28UqJuqJQMJym.htm)|False Death Vial|auto-trad|
|[consumable-13-TBuLxrVXa4UGHzb9.htm](equipment/consumable-13-TBuLxrVXa4UGHzb9.htm)|Force Tiles|auto-trad|
|[consumable-13-u1ouo0mH0sS6QoGw.htm](equipment/consumable-13-u1ouo0mH0sS6QoGw.htm)|Void Fragment|auto-trad|
|[consumable-13-VbVw8IDiYczzbIHt.htm](equipment/consumable-13-VbVw8IDiYczzbIHt.htm)|Potion Patch (Greater)|auto-trad|
|[consumable-13-vjwdfXDWSe7L3zwl.htm](equipment/consumable-13-vjwdfXDWSe7L3zwl.htm)|Reaper's Spellgun|auto-trad|
|[consumable-13-Vm0xyg3xE4iMq3V3.htm](equipment/consumable-13-Vm0xyg3xE4iMq3V3.htm)|Roaring Potion (Moderate)|auto-trad|
|[consumable-13-vRO9Orlts0jP7nCx.htm](equipment/consumable-13-vRO9Orlts0jP7nCx.htm)|Euphoric Loop (Greater)|auto-trad|
|[consumable-13-wuVD3kolQaFAEeBK.htm](equipment/consumable-13-wuVD3kolQaFAEeBK.htm)|Numbing Tonic (Greater)|auto-trad|
|[consumable-13-xXvuQABxlRH2HXsO.htm](equipment/consumable-13-xXvuQABxlRH2HXsO.htm)|Ablative Armor Plating (Major)|auto-trad|
|[consumable-13-Xzuya7EEF5FwVGFr.htm](equipment/consumable-13-Xzuya7EEF5FwVGFr.htm)|Worm Vial|auto-trad|
|[consumable-14-1OJxRnPs0viJVrZq.htm](equipment/consumable-14-1OJxRnPs0viJVrZq.htm)|Potion of Fire Resistance (Greater)|auto-trad|
|[consumable-14-4dwBcqeuXePd89qW.htm](equipment/consumable-14-4dwBcqeuXePd89qW.htm)|Starshot Arrow (Greater)|auto-trad|
|[consumable-14-62IVHZyHA6239PHe.htm](equipment/consumable-14-62IVHZyHA6239PHe.htm)|Unsullied Blood (Major)|auto-trad|
|[consumable-14-6Qq7F1wlH5cx189e.htm](equipment/consumable-14-6Qq7F1wlH5cx189e.htm)|Dreaming Round|auto-trad|
|[consumable-14-8eKDnjLeP9KpZFD3.htm](equipment/consumable-14-8eKDnjLeP9KpZFD3.htm)|Winterstep Elixir (Greater)|auto-trad|
|[consumable-14-95ki5dSg5Porv04G.htm](equipment/consumable-14-95ki5dSg5Porv04G.htm)|Depth Charge V|auto-trad|
|[consumable-14-9wwCHxuCfBA9w6Ik.htm](equipment/consumable-14-9wwCHxuCfBA9w6Ik.htm)|Feyfoul (Greater)|auto-trad|
|[consumable-14-aBN9WkTHUCtE259N.htm](equipment/consumable-14-aBN9WkTHUCtE259N.htm)|Conduit Shot (Greater)|auto-trad|
|[consumable-14-ANzB9WzlGQXDn76G.htm](equipment/consumable-14-ANzB9WzlGQXDn76G.htm)|Antidote (Major)|auto-trad|
|[consumable-14-Bgslo3kDz8bTiYie.htm](equipment/consumable-14-Bgslo3kDz8bTiYie.htm)|Defiled Costa|auto-trad|
|[consumable-14-ClKb4YQn8TfPIclE.htm](equipment/consumable-14-ClKb4YQn8TfPIclE.htm)|Dazing Coil|auto-trad|
|[consumable-14-DIQg2Tml1wWjSC1q.htm](equipment/consumable-14-DIQg2Tml1wWjSC1q.htm)|Engulfing Snare|auto-trad|
|[consumable-14-EDjJy68ZFLu57ldN.htm](equipment/consumable-14-EDjJy68ZFLu57ldN.htm)|Noxious Incense (Greater)|auto-trad|
|[consumable-14-EmnU1xFDutJ70HaI.htm](equipment/consumable-14-EmnU1xFDutJ70HaI.htm)|Dragontooth Trophy|auto-trad|
|[consumable-14-esDSCEI1cf8Op93I.htm](equipment/consumable-14-esDSCEI1cf8Op93I.htm)|Addiction Suppressant (Major)|auto-trad|
|[consumable-14-F8vrGctUqYIEkKOz.htm](equipment/consumable-14-F8vrGctUqYIEkKOz.htm)|Potion of Electricity Resistance (Greater)|auto-trad|
|[consumable-14-GcllrP56XWEsYQ4j.htm](equipment/consumable-14-GcllrP56XWEsYQ4j.htm)|Potion of Sonic Resistance (Greater)|auto-trad|
|[consumable-14-HBlUlkSlIQ0NQA9N.htm](equipment/consumable-14-HBlUlkSlIQ0NQA9N.htm)|Vapor Sphere|auto-trad|
|[consumable-14-ipaBbiROyAN4McWA.htm](equipment/consumable-14-ipaBbiROyAN4McWA.htm)|Tentacle Potion (Greater)|auto-trad|
|[consumable-14-j77uu6eFlsYoZApx.htm](equipment/consumable-14-j77uu6eFlsYoZApx.htm)|Ruby String|auto-trad|
|[consumable-14-JukXiZWas5RJaE3M.htm](equipment/consumable-14-JukXiZWas5RJaE3M.htm)|Bonmuan Swapping Stone (Moderate)|auto-trad|
|[consumable-14-kajpTXg3oHIJStQa.htm](equipment/consumable-14-kajpTXg3oHIJStQa.htm)|Antiplague (Major)|auto-trad|
|[consumable-14-lasqzrPVEHI5MkQd.htm](equipment/consumable-14-lasqzrPVEHI5MkQd.htm)|Bomber's Eye Elixir (Greater)|auto-trad|
|[consumable-14-mJSCOIJllj2GtACC.htm](equipment/consumable-14-mJSCOIJllj2GtACC.htm)|Grinning Pugwampi|auto-trad|
|[consumable-14-MpuFL2midIke1Sbn.htm](equipment/consumable-14-MpuFL2midIke1Sbn.htm)|Life Shot (Greater)|auto-trad|
|[consumable-14-mWOxG8asXbjCRVG9.htm](equipment/consumable-14-mWOxG8asXbjCRVG9.htm)|Reflected Moonlight Fulu|auto-trad|
|[consumable-14-P3EL4ntkd5InC3YB.htm](equipment/consumable-14-P3EL4ntkd5InC3YB.htm)|Ruby Capacitor (Greater)|auto-trad|
|[consumable-14-Pb0WOuCR34FYGmuy.htm](equipment/consumable-14-Pb0WOuCR34FYGmuy.htm)|Thousand-Pains Fulu (Icicle)|auto-trad|
|[consumable-14-pV5M6IYoPDhTy6FA.htm](equipment/consumable-14-pV5M6IYoPDhTy6FA.htm)|Chopping Evisceration Snare|auto-trad|
|[consumable-14-Qcla3yTa1MzcpMjb.htm](equipment/consumable-14-Qcla3yTa1MzcpMjb.htm)|Emetic Paste (Greater)|auto-trad|
|[consumable-14-RN8TnAhWQ4tNKHgP.htm](equipment/consumable-14-RN8TnAhWQ4tNKHgP.htm)|Brewer's Regret (Greater)|auto-trad|
|[consumable-14-sjxhPujyv6rJJRWO.htm](equipment/consumable-14-sjxhPujyv6rJJRWO.htm)|Viper's Fang|auto-trad|
|[consumable-14-smPuKlJLRyoYaDpR.htm](equipment/consumable-14-smPuKlJLRyoYaDpR.htm)|Nevercold (Compressed)|auto-trad|
|[consumable-14-TKOe8epJTCef87Bj.htm](equipment/consumable-14-TKOe8epJTCef87Bj.htm)|Malleable Mixture (Greater)|auto-trad|
|[consumable-14-vDgDrbSoGuiUIz2R.htm](equipment/consumable-14-vDgDrbSoGuiUIz2R.htm)|Potion of Acid Resistance (Greater)|auto-trad|
|[consumable-14-VZmUrseCZs9xMaxt.htm](equipment/consumable-14-VZmUrseCZs9xMaxt.htm)|Potion of Cold Resistance (Greater)|auto-trad|
|[consumable-14-Wyh13xRaOL57fve7.htm](equipment/consumable-14-Wyh13xRaOL57fve7.htm)|Red-Rib Gill Mask (Greater)|auto-trad|
|[consumable-14-XvmYNnQ4vg8GREH7.htm](equipment/consumable-14-XvmYNnQ4vg8GREH7.htm)|Iron Cudgel|auto-trad|
|[consumable-14-YvmRcEky6fuIR0xp.htm](equipment/consumable-14-YvmRcEky6fuIR0xp.htm)|Impact Foam Chassis (Major)|auto-trad|
|[consumable-14-ywYt2SyZaV95KcZH.htm](equipment/consumable-14-ywYt2SyZaV95KcZH.htm)|Potion of Resistance (Greater)|auto-trad|
|[consumable-14-Zjevw0WzUs1IIUAL.htm](equipment/consumable-14-Zjevw0WzUs1IIUAL.htm)|Ghost Ammunition|auto-trad|
|[consumable-15-1DmcPUZERWeKpiMM.htm](equipment/consumable-15-1DmcPUZERWeKpiMM.htm)|Serpent Oil (True)|auto-trad|
|[consumable-15-4B8jRW7jGybSlovV.htm](equipment/consumable-15-4B8jRW7jGybSlovV.htm)|Potion of Annulment (Moderate)|auto-trad|
|[consumable-15-6swrLimere2nZtz9.htm](equipment/consumable-15-6swrLimere2nZtz9.htm)|Dragon Bile|auto-trad|
|[consumable-15-9CNxvAalHBPdSUFl.htm](equipment/consumable-15-9CNxvAalHBPdSUFl.htm)|Transposition Ammunition (Greater)|auto-trad|
|[consumable-15-AmxSqEoFhRLMYd1W.htm](equipment/consumable-15-AmxSqEoFhRLMYd1W.htm)|Elixir of Life (Major)|auto-trad|
|[consumable-15-aOFly4vZu24x85b0.htm](equipment/consumable-15-aOFly4vZu24x85b0.htm)|Eldritch Flare|auto-trad|
|[consumable-15-cqTCg6C0lvYUKQmx.htm](equipment/consumable-15-cqTCg6C0lvYUKQmx.htm)|Stone Body Mutagen (Greater)|auto-trad|
|[consumable-15-cSrCTi2zE5OU5ylH.htm](equipment/consumable-15-cSrCTi2zE5OU5ylH.htm)|Lifeblight Residue|auto-trad|
|[consumable-15-D8i3OBVMkxmCCREV.htm](equipment/consumable-15-D8i3OBVMkxmCCREV.htm)|Poison Fizz (Greater)|auto-trad|
|[consumable-15-E999hL7XlAlfZhjK.htm](equipment/consumable-15-E999hL7XlAlfZhjK.htm)|Potion of Flying (Greater)|auto-trad|
|[consumable-15-FJw54ZPLuJpChJbe.htm](equipment/consumable-15-FJw54ZPLuJpChJbe.htm)|Reverberating Stone (Greater)|auto-trad|
|[consumable-15-Fz6jQ0M3v55R8CqA.htm](equipment/consumable-15-Fz6jQ0M3v55R8CqA.htm)|Big Rock Bullet (Major)|auto-trad|
|[consumable-15-Gfew65lwkzZc3mUV.htm](equipment/consumable-15-Gfew65lwkzZc3mUV.htm)|Disintegration Bolt|auto-trad|
|[consumable-15-GMi5tw0cbMx3ZQPg.htm](equipment/consumable-15-GMi5tw0cbMx3ZQPg.htm)|Mindfog Mist|auto-trad|
|[consumable-15-hnBcuuzzrBT0L0CL.htm](equipment/consumable-15-hnBcuuzzrBT0L0CL.htm)|Mukradi Jar|auto-trad|
|[consumable-15-i4D4B7tpYv9vMvQY.htm](equipment/consumable-15-i4D4B7tpYv9vMvQY.htm)|Sea Touch Elixir (Greater)|auto-trad|
|[consumable-15-iPki3yuoucnj7bIt.htm](equipment/consumable-15-iPki3yuoucnj7bIt.htm)|Scroll of 8th-level Spell|auto-trad|
|[consumable-15-kFqPDnz0dYvK0mIG.htm](equipment/consumable-15-kFqPDnz0dYvK0mIG.htm)|Firestarter Pellets (Major)|auto-trad|
|[consumable-15-KO7ZftYQrcE5GFkJ.htm](equipment/consumable-15-KO7ZftYQrcE5GFkJ.htm)|Etheric Essence Disruptor (Major)|auto-trad|
|[consumable-15-nmXPj9zuMRQBNT60.htm](equipment/consumable-15-nmXPj9zuMRQBNT60.htm)|Magic Wand (7th-Level Spell)|auto-trad|
|[consumable-15-omWrYy6RY9rxeF50.htm](equipment/consumable-15-omWrYy6RY9rxeF50.htm)|Material Essence Disruptor (Major)|auto-trad|
|[consumable-15-QD5rDuk0OHadPGuh.htm](equipment/consumable-15-QD5rDuk0OHadPGuh.htm)|Obfuscation Oil|auto-trad|
|[consumable-15-R4UPjbSDwDEmDJBA.htm](equipment/consumable-15-R4UPjbSDwDEmDJBA.htm)|Azure Worm Repellent|auto-trad|
|[consumable-15-rEev4xrQyMvjz7W7.htm](equipment/consumable-15-rEev4xrQyMvjz7W7.htm)|Grudgestone (Greater)|auto-trad|
|[consumable-15-rpqgJkCepHecj1eA.htm](equipment/consumable-15-rpqgJkCepHecj1eA.htm)|Crackling Bubble Gum (Major)|auto-trad|
|[consumable-15-SBpUiOJiap2cZk2Z.htm](equipment/consumable-15-SBpUiOJiap2cZk2Z.htm)|Indomitable Keepsake (Major)|auto-trad|
|[consumable-15-sVTg7RMv8qFuQD3y.htm](equipment/consumable-15-sVTg7RMv8qFuQD3y.htm)|Cold Comfort (Greater)|auto-trad|
|[consumable-15-sWPkbSlWAEAStBqD.htm](equipment/consumable-15-sWPkbSlWAEAStBqD.htm)|Bravo's Brew (Greater)|auto-trad|
|[consumable-15-UOH0A4xNSMnpV6i4.htm](equipment/consumable-15-UOH0A4xNSMnpV6i4.htm)|Life-Boosting Oil (Major)|auto-trad|
|[consumable-15-VFUOEkaCaSCJsu6U.htm](equipment/consumable-15-VFUOEkaCaSCJsu6U.htm)|Bargainer's Instrument|auto-trad|
|[consumable-15-VqJuhN2Ba1DnrJNW.htm](equipment/consumable-15-VqJuhN2Ba1DnrJNW.htm)|Mind-Swap Potion|auto-trad|
|[consumable-15-WearPqN56FQofpF6.htm](equipment/consumable-15-WearPqN56FQofpF6.htm)|Spellstrike Ammunition (Type VII)|auto-trad|
|[consumable-15-wTVBD8XHPG810rlH.htm](equipment/consumable-15-wTVBD8XHPG810rlH.htm)|Stone Bullet|auto-trad|
|[consumable-15-XQjGhnBWOIh1uB2w.htm](equipment/consumable-15-XQjGhnBWOIh1uB2w.htm)|Garrote Shot|auto-trad|
|[consumable-15-zELyHWSfkD4yRUjZ.htm](equipment/consumable-15-zELyHWSfkD4yRUjZ.htm)|Torrent Spellgun (Major)|auto-trad|
|[consumable-15-ZJE92c4sE6cu8l8l.htm](equipment/consumable-15-ZJE92c4sE6cu8l8l.htm)|Heartblood Ring|auto-trad|
|[consumable-16-3joQBdbHQo66FT6S.htm](equipment/consumable-16-3joQBdbHQo66FT6S.htm)|Dragonclaw Scutcheon|auto-trad|
|[consumable-16-4Pmo9gc81JAOzdke.htm](equipment/consumable-16-4Pmo9gc81JAOzdke.htm)|Flame Navette|auto-trad|
|[consumable-16-7p3C9xf3XgUbvKHL.htm](equipment/consumable-16-7p3C9xf3XgUbvKHL.htm)|Frenzy Oil|auto-trad|
|[consumable-16-9hdT05ywPVyh9vQX.htm](equipment/consumable-16-9hdT05ywPVyh9vQX.htm)|Cerulean Scourge|auto-trad|
|[consumable-16-9nhvZ7VnDQHuyBdf.htm](equipment/consumable-16-9nhvZ7VnDQHuyBdf.htm)|Brimstone Fumes|auto-trad|
|[consumable-16-aDn5blt2iiYJpzbe.htm](equipment/consumable-16-aDn5blt2iiYJpzbe.htm)|Winter Wolf Elixir (Greater)|auto-trad|
|[consumable-16-bKc5f8dAOjv1kwQ2.htm](equipment/consumable-16-bKc5f8dAOjv1kwQ2.htm)|Golden-Cased Bullet (Major)|auto-trad|
|[consumable-16-CoMwPsQ8mPj5Evti.htm](equipment/consumable-16-CoMwPsQ8mPj5Evti.htm)|Truesight Potion|auto-trad|
|[consumable-16-CXCWF6jQQHKHFhpR.htm](equipment/consumable-16-CXCWF6jQQHKHFhpR.htm)|Gift of the Poisoned Heart|auto-trad|
|[consumable-16-dIqIuqjPbIuh9TYn.htm](equipment/consumable-16-dIqIuqjPbIuh9TYn.htm)|Tusk and Fang Chain|auto-trad|
|[consumable-16-dmQAN56aMro0gecx.htm](equipment/consumable-16-dmQAN56aMro0gecx.htm)|Ghost Dust|auto-trad|
|[consumable-16-e90XsQumDxHzqS7z.htm](equipment/consumable-16-e90XsQumDxHzqS7z.htm)|Sniper's Bead (Major)|auto-trad|
|[consumable-16-EAgWYY1C4mDRfwqo.htm](equipment/consumable-16-EAgWYY1C4mDRfwqo.htm)|Numbing Tonic (Major)|auto-trad|
|[consumable-16-EP8f2NL28vPlmX7k.htm](equipment/consumable-16-EP8f2NL28vPlmX7k.htm)|Oil of Object Animation (Greater)|auto-trad|
|[consumable-16-FZxrYMcH0uH617EQ.htm](equipment/consumable-16-FZxrYMcH0uH617EQ.htm)|False Death Vial (Greater)|auto-trad|
|[consumable-16-HgI87CKuhY8IbFWG.htm](equipment/consumable-16-HgI87CKuhY8IbFWG.htm)|Camp Shroud (Major)|auto-trad|
|[consumable-16-hlXXODDYeRzWuPCK.htm](equipment/consumable-16-hlXXODDYeRzWuPCK.htm)|Magnetic Suit (Major)|auto-trad|
|[consumable-16-jOuY6D1XmtHeax9H.htm](equipment/consumable-16-jOuY6D1XmtHeax9H.htm)|Weapon Shot (Major)|auto-trad|
|[consumable-16-kicNrnZz1KjJYRVI.htm](equipment/consumable-16-kicNrnZz1KjJYRVI.htm)|Eagle Eye Elixir (Major)|auto-trad|
|[consumable-16-KOWn72lufGfszQXr.htm](equipment/consumable-16-KOWn72lufGfszQXr.htm)|Potion of Stable Form (Greater)|auto-trad|
|[consumable-16-mamIdMfPguGM8QV7.htm](equipment/consumable-16-mamIdMfPguGM8QV7.htm)|Weeping Midnight|auto-trad|
|[consumable-16-NQuE19X1YMWjiWHl.htm](equipment/consumable-16-NQuE19X1YMWjiWHl.htm)|Stormbreaker Fulu|auto-trad|
|[consumable-16-Odl2SyKw8Zg6ckKb.htm](equipment/consumable-16-Odl2SyKw8Zg6ckKb.htm)|Hail of Arrows Snare|auto-trad|
|[consumable-16-pkb6lKSanfcxvLtQ.htm](equipment/consumable-16-pkb6lKSanfcxvLtQ.htm)|Depth Charge VI|auto-trad|
|[consumable-16-po3Lhiccc0RQYikr.htm](equipment/consumable-16-po3Lhiccc0RQYikr.htm)|Phoenix Cinder|auto-trad|
|[consumable-16-PxrYRcawI6a3lRcf.htm](equipment/consumable-16-PxrYRcawI6a3lRcf.htm)|Animal Repellent (Major)|auto-trad|
|[consumable-16-R6qfTzNv79GH1xHU.htm](equipment/consumable-16-R6qfTzNv79GH1xHU.htm)|Life Shot (Major)|auto-trad|
|[consumable-16-tnCKwIsRsKj3FtG6.htm](equipment/consumable-16-tnCKwIsRsKj3FtG6.htm)|Omnidirectional Spear Snare|auto-trad|
|[consumable-16-VJ9mXi70JprHHA0z.htm](equipment/consumable-16-VJ9mXi70JprHHA0z.htm)|Flare Beacon (Major)|auto-trad|
|[consumable-16-WAUq4TB4Q9FDxWFz.htm](equipment/consumable-16-WAUq4TB4Q9FDxWFz.htm)|Candle of Invocation|auto-trad|
|[consumable-16-ws3OXRgAawwYIKK6.htm](equipment/consumable-16-ws3OXRgAawwYIKK6.htm)|Repulsion Resin|auto-trad|
|[consumable-16-X6vNtRjyHIuN7vqj.htm](equipment/consumable-16-X6vNtRjyHIuN7vqj.htm)|Nightmare Vapor|auto-trad|
|[consumable-16-XMuLrJYL6fxv4YNA.htm](equipment/consumable-16-XMuLrJYL6fxv4YNA.htm)|Salamander Elixir (Greater)|auto-trad|
|[consumable-16-XWO5pMQcYNybqWzf.htm](equipment/consumable-16-XWO5pMQcYNybqWzf.htm)|Silver Crescent (Greater)|auto-trad|
|[consumable-16-yPFHTY1GH3rdWwds.htm](equipment/consumable-16-yPFHTY1GH3rdWwds.htm)|Dust of Corpse Animation (Greater)|auto-trad|
|[consumable-17-1kNp6yOS0aZPBPzZ.htm](equipment/consumable-17-1kNp6yOS0aZPBPzZ.htm)|Juggernaut Mutagen (Major)|auto-trad|
|[consumable-17-2JXUER4xY8s36HUv.htm](equipment/consumable-17-2JXUER4xY8s36HUv.htm)|Copper Dragon's Breath Potion (Wyrm)|auto-trad|
|[consumable-17-4GXzTN6iSDGfYEAi.htm](equipment/consumable-17-4GXzTN6iSDGfYEAi.htm)|Quicksilver Mutagen (Major)|auto-trad|
|[consumable-17-6odVabL0WL2H89Ic.htm](equipment/consumable-17-6odVabL0WL2H89Ic.htm)|Sixfingers Elixir (Greater)|auto-trad|
|[consumable-17-9Fps57Xc1XxxWK0j.htm](equipment/consumable-17-9Fps57Xc1XxxWK0j.htm)|Fire and Iceberg (Major)|auto-trad|
|[consumable-17-alUdoHA3sbaSJL9f.htm](equipment/consumable-17-alUdoHA3sbaSJL9f.htm)|Hungering Maw|auto-trad|
|[consumable-17-asuxBXmrZErr9tep.htm](equipment/consumable-17-asuxBXmrZErr9tep.htm)|Awakened Silver Shot|auto-trad|
|[consumable-17-bASMkq9syBtpaXok.htm](equipment/consumable-17-bASMkq9syBtpaXok.htm)|False Hope|auto-trad|
|[consumable-17-bvIVxDq1wh6IavHP.htm](equipment/consumable-17-bvIVxDq1wh6IavHP.htm)|Choker-Arm Mutagen (Major)|auto-trad|
|[consumable-17-ccrdVliTNBh2mNZf.htm](equipment/consumable-17-ccrdVliTNBh2mNZf.htm)|Serene Mutagen (Major)|auto-trad|
|[consumable-17-cFHomF3tty8Wi1e5.htm](equipment/consumable-17-cFHomF3tty8Wi1e5.htm)|Scroll of 9th-level Spell|auto-trad|
|[consumable-17-d2sU6nOtBdIVJMVI.htm](equipment/consumable-17-d2sU6nOtBdIVJMVI.htm)|Red Dragon's Breath Potion (Wyrm)|auto-trad|
|[consumable-17-eoSvkOAkx4YGLQ2O.htm](equipment/consumable-17-eoSvkOAkx4YGLQ2O.htm)|Bonmuan Swapping Stone (Greater)|auto-trad|
|[consumable-17-ETLSXa4Po518z9Qx.htm](equipment/consumable-17-ETLSXa4Po518z9Qx.htm)|Spiderfoot Brew (Major)|auto-trad|
|[consumable-17-H20eUVst7rgwhUb6.htm](equipment/consumable-17-H20eUVst7rgwhUb6.htm)|Breathtaking Vapor|auto-trad|
|[consumable-17-IBJIBTkTilVV0BdC.htm](equipment/consumable-17-IBJIBTkTilVV0BdC.htm)|Impossible Cake (Major)|auto-trad|
|[consumable-17-idtIMGkFmIe958Et.htm](equipment/consumable-17-idtIMGkFmIe958Et.htm)|Brass Dragon's Breath Potion (Wyrm)|auto-trad|
|[consumable-17-iYOBu0hMYzqFYvZN.htm](equipment/consumable-17-iYOBu0hMYzqFYvZN.htm)|Moonlit Spellgun (Major)|auto-trad|
|[consumable-17-JqKNiEJxSZX1sD06.htm](equipment/consumable-17-JqKNiEJxSZX1sD06.htm)|Magnetic Shot (Greater)|auto-trad|
|[consumable-17-KsoofB7FCppaBTdK.htm](equipment/consumable-17-KsoofB7FCppaBTdK.htm)|Celestial Peach (Rejuvenation)|auto-trad|
|[consumable-17-kwp0N4MdOgRpdKi6.htm](equipment/consumable-17-kwp0N4MdOgRpdKi6.htm)|Awakened Cold Iron Shot|auto-trad|
|[consumable-17-KZbAqbt7qxJPPkii.htm](equipment/consumable-17-KZbAqbt7qxJPPkii.htm)|Gold Dragon's Breath Potion (Wyrm)|auto-trad|
|[consumable-17-lJTl2SuUaluMzPBl.htm](equipment/consumable-17-lJTl2SuUaluMzPBl.htm)|Bronze Dragon's Breath Potion (Wyrm)|auto-trad|
|[consumable-17-LR7Ke2V8vHjQYBni.htm](equipment/consumable-17-LR7Ke2V8vHjQYBni.htm)|Black Dragon's Breath Potion (Wyrm)|auto-trad|
|[consumable-17-ltILKAEnrZ8vKwY0.htm](equipment/consumable-17-ltILKAEnrZ8vKwY0.htm)|Blue Dragon's Breath Potion (Wyrm)|auto-trad|
|[consumable-17-M4ZOHOlne43ArjOC.htm](equipment/consumable-17-M4ZOHOlne43ArjOC.htm)|Drakeheart Mutagen (Major)|auto-trad|
|[consumable-17-mGIJCGFkBQLkzhTg.htm](equipment/consumable-17-mGIJCGFkBQLkzhTg.htm)|Dispelling Sliver|auto-trad|
|[consumable-17-MP7updiiSct04vno.htm](equipment/consumable-17-MP7updiiSct04vno.htm)|Hemlock|auto-trad|
|[consumable-17-MpyuCFBQwbOQhhm0.htm](equipment/consumable-17-MpyuCFBQwbOQhhm0.htm)|Reflecting Shard (Greater)|auto-trad|
|[consumable-17-NfMteX8WWC5mxc52.htm](equipment/consumable-17-NfMteX8WWC5mxc52.htm)|Spiritual Warhorn (Major)|auto-trad|
|[consumable-17-NOPrIz6UNxof1M5d.htm](equipment/consumable-17-NOPrIz6UNxof1M5d.htm)|Black Tendril Shot (Greater)|auto-trad|
|[consumable-17-nS75vsM3x5jxlUqn.htm](equipment/consumable-17-nS75vsM3x5jxlUqn.htm)|Bestial Mutagen (Major)|auto-trad|
|[consumable-17-OiQDcUMrlrYXDgEf.htm](equipment/consumable-17-OiQDcUMrlrYXDgEf.htm)|White Dragon's Breath Potion (Wyrm)|auto-trad|
|[consumable-17-OJvCWeJPHEc7GpFG.htm](equipment/consumable-17-OJvCWeJPHEc7GpFG.htm)|Green Dragon's Breath Potion (Wyrm)|auto-trad|
|[consumable-17-OkYPLMDxprdm24hH.htm](equipment/consumable-17-OkYPLMDxprdm24hH.htm)|Cryomister (Major)|auto-trad|
|[consumable-17-oO0DqxI7EGkTYJzv.htm](equipment/consumable-17-oO0DqxI7EGkTYJzv.htm)|Defiled Costa (Greater)|auto-trad|
|[consumable-17-oqiC4Mc5qKNCl88e.htm](equipment/consumable-17-oqiC4Mc5qKNCl88e.htm)|Spellstrike Ammunition (Type VIII)|auto-trad|
|[consumable-17-PW3H8TnouiLaTC8G.htm](equipment/consumable-17-PW3H8TnouiLaTC8G.htm)|Silver Dragon's Breath Potion (Wyrm)|auto-trad|
|[consumable-17-Qa4qjxk5aPzx7HTR.htm](equipment/consumable-17-Qa4qjxk5aPzx7HTR.htm)|War Blood Mutagen (Major)|auto-trad|
|[consumable-17-Qs8RgNH6thRPv2jt.htm](equipment/consumable-17-Qs8RgNH6thRPv2jt.htm)|Magic Wand (8th-Level Spell)|auto-trad|
|[consumable-17-qvAtAv211cVqktgU.htm](equipment/consumable-17-qvAtAv211cVqktgU.htm)|Greengut|auto-trad|
|[consumable-17-sd3H2Nir3qLFNVI3.htm](equipment/consumable-17-sd3H2Nir3qLFNVI3.htm)|Faerie Dragon Liqueur (Wyrm)|auto-trad|
|[consumable-17-sK2zEyhxxP6UnhQk.htm](equipment/consumable-17-sK2zEyhxxP6UnhQk.htm)|Blast Boots (Major)|auto-trad|
|[consumable-17-uD5vRYVOXNJ53sEE.htm](equipment/consumable-17-uD5vRYVOXNJ53sEE.htm)|Silvertongue Mutagen (Major)|auto-trad|
|[consumable-17-UeUPNFw600HEdDgF.htm](equipment/consumable-17-UeUPNFw600HEdDgF.htm)|Soothing Tonic (Major)|auto-trad|
|[consumable-17-UOvDye8obnqjfMQG.htm](equipment/consumable-17-UOvDye8obnqjfMQG.htm)|Healing Vapor (Major)|auto-trad|
|[consumable-17-VBK9i74dry8yf8f0.htm](equipment/consumable-17-VBK9i74dry8yf8f0.htm)|Cognitive Mutagen (Major)|auto-trad|
|[consumable-17-vmwrU3lz1HFfadAS.htm](equipment/consumable-17-vmwrU3lz1HFfadAS.htm)|Sanguine Mutagen (Major)|auto-trad|
|[consumable-17-vuJi1kahoCAHHTSX.htm](equipment/consumable-17-vuJi1kahoCAHHTSX.htm)|Thousand-Pains Fulu (Burl)|auto-trad|
|[consumable-17-VXxwxLwJiDODlUZq.htm](equipment/consumable-17-VXxwxLwJiDODlUZq.htm)|Demolition Fulu (Greater)|auto-trad|
|[consumable-17-Wv0ck7AMP0s0jIue.htm](equipment/consumable-17-Wv0ck7AMP0s0jIue.htm)|Theatrical Mutagen (Major)|auto-trad|
|[consumable-17-Wx5WhSNOmeICgHSC.htm](equipment/consumable-17-Wx5WhSNOmeICgHSC.htm)|Deadweight Mutagen (Major)|auto-trad|
|[consumable-18-1LKJvK9ofzIIkQX3.htm](equipment/consumable-18-1LKJvK9ofzIIkQX3.htm)|Sun Orchid Poultice|auto-trad|
|[consumable-18-3UXsXwUbvOdpPg6b.htm](equipment/consumable-18-3UXsXwUbvOdpPg6b.htm)|Blood Booster (Greater)|auto-trad|
|[consumable-18-6CzLtRHIBfhMpyv2.htm](equipment/consumable-18-6CzLtRHIBfhMpyv2.htm)|Cloning Potion|auto-trad|
|[consumable-18-aMbJl8eTV3Ri5O6e.htm](equipment/consumable-18-aMbJl8eTV3Ri5O6e.htm)|Rovagug's Mud (Greater)|auto-trad|
|[consumable-18-aMXCMLI3spk0d7tq.htm](equipment/consumable-18-aMXCMLI3spk0d7tq.htm)|Nevercold (Refined)|auto-trad|
|[consumable-18-bCPAiqiWmH7pVxNE.htm](equipment/consumable-18-bCPAiqiWmH7pVxNE.htm)|Potion of Cold Retaliation (Major)|auto-trad|
|[consumable-18-bEaiID2KLQ8lTCai.htm](equipment/consumable-18-bEaiID2KLQ8lTCai.htm)|Potion of Electricity Retaliation (Major)|auto-trad|
|[consumable-18-ed8gAxli6Of58kAM.htm](equipment/consumable-18-ed8gAxli6Of58kAM.htm)|Lastwall Soup (Greater)|auto-trad|
|[consumable-18-fabw5fDuaTMUF0tb.htm](equipment/consumable-18-fabw5fDuaTMUF0tb.htm)|Potion of Acid Retaliation (Major)|auto-trad|
|[consumable-18-FBS2dhoOfSlcuPt6.htm](equipment/consumable-18-FBS2dhoOfSlcuPt6.htm)|Explosive Mine (Major)|auto-trad|
|[consumable-18-J3gd5vhcxwFbpCXe.htm](equipment/consumable-18-J3gd5vhcxwFbpCXe.htm)|Awakened Adamantine Shot|auto-trad|
|[consumable-18-jdkMRl7zxOVGUuGI.htm](equipment/consumable-18-jdkMRl7zxOVGUuGI.htm)|Universal Solvent (Major)|auto-trad|
|[consumable-18-JZZmDY1IJmKcdR2D.htm](equipment/consumable-18-JZZmDY1IJmKcdR2D.htm)|Vaccine (Major)|auto-trad|
|[consumable-18-Ksz7kd6P9uKR8uLo.htm](equipment/consumable-18-Ksz7kd6P9uKR8uLo.htm)|Clockwork Rejuvenator|auto-trad|
|[consumable-18-lu3kPHtKZTHPx4Fx.htm](equipment/consumable-18-lu3kPHtKZTHPx4Fx.htm)|Kraken Bottle|auto-trad|
|[consumable-18-m6oNZUSSWYMnk7ii.htm](equipment/consumable-18-m6oNZUSSWYMnk7ii.htm)|Roaring Potion (Greater)|auto-trad|
|[consumable-18-MUL0ABe8h7Ugc1Jt.htm](equipment/consumable-18-MUL0ABe8h7Ugc1Jt.htm)|Fury Cocktail (Greater)|auto-trad|
|[consumable-18-nctubaNAmzvXYDkc.htm](equipment/consumable-18-nctubaNAmzvXYDkc.htm)|Crimson Worm Repellent|auto-trad|
|[consumable-18-nkozt1tueA0Xz6VF.htm](equipment/consumable-18-nkozt1tueA0Xz6VF.htm)|Depth Charge VII|auto-trad|
|[consumable-18-nlm26zGDAHTISTh6.htm](equipment/consumable-18-nlm26zGDAHTISTh6.htm)|Avalanche of Stones Snare|auto-trad|
|[consumable-18-obER4cKi8UbGhSg7.htm](equipment/consumable-18-obER4cKi8UbGhSg7.htm)|Oil of Unlife (Major)|auto-trad|
|[consumable-18-OtFKHfUQnn97ILBC.htm](equipment/consumable-18-OtFKHfUQnn97ILBC.htm)|Gearbinder Oil (Greater)|auto-trad|
|[consumable-18-p3ppzFSsZXFRe3H8.htm](equipment/consumable-18-p3ppzFSsZXFRe3H8.htm)|Healing Potion (Major)|auto-trad|
|[consumable-18-PFr49j091jjb8363.htm](equipment/consumable-18-PFr49j091jjb8363.htm)|Ruby Capacitor (Major)|auto-trad|
|[consumable-18-Pnw4A7fhUhTS85Te.htm](equipment/consumable-18-Pnw4A7fhUhTS85Te.htm)|Focus Cathartic (Major)|auto-trad|
|[consumable-18-po9gLNA6GjystED5.htm](equipment/consumable-18-po9gLNA6GjystED5.htm)|Snarling Badger (Greater)|auto-trad|
|[consumable-18-qJaIfdtDrysjcy37.htm](equipment/consumable-18-qJaIfdtDrysjcy37.htm)|Ooze Ammunition (Major)|auto-trad|
|[consumable-18-RkI3jKKrCozvEvfr.htm](equipment/consumable-18-RkI3jKKrCozvEvfr.htm)|King's Sleep|auto-trad|
|[consumable-18-TalLb1YlvXzutuPc.htm](equipment/consumable-18-TalLb1YlvXzutuPc.htm)|Potion of Fire Retaliation (Major)|auto-trad|
|[consumable-18-TSMeh3NfrVEMulWR.htm](equipment/consumable-18-TSMeh3NfrVEMulWR.htm)|Choleric Contagion|auto-trad|
|[consumable-18-UblsH5cGyUdXypek.htm](equipment/consumable-18-UblsH5cGyUdXypek.htm)|Potion of Undetectability|auto-trad|
|[consumable-18-UpUJ6NHBin7bubeX.htm](equipment/consumable-18-UpUJ6NHBin7bubeX.htm)|Cayden's Brew (Triple)|auto-trad|
|[consumable-18-vTEWlRBUD4VzVwxK.htm](equipment/consumable-18-vTEWlRBUD4VzVwxK.htm)|Applereed Mutagen (Greater)|auto-trad|
|[consumable-18-ye6XAJY21YQqRnKH.htm](equipment/consumable-18-ye6XAJY21YQqRnKH.htm)|Implosion Dust (Greater)|auto-trad|
|[consumable-18-YIgIF1845n5UBFnE.htm](equipment/consumable-18-YIgIF1845n5UBFnE.htm)|Sinew-Shock Serum (Major)|auto-trad|
|[consumable-18-Z333awQuoTxGkbgG.htm](equipment/consumable-18-Z333awQuoTxGkbgG.htm)|Rusting Ammunition (Greater)|auto-trad|
|[consumable-19-5hGJ6CZi0A9OjMd4.htm](equipment/consumable-19-5hGJ6CZi0A9OjMd4.htm)|Oblivion Essence|auto-trad|
|[consumable-19-7VDf5tQKh01XV6HQ.htm](equipment/consumable-19-7VDf5tQKh01XV6HQ.htm)|Numbing Tonic (True)|auto-trad|
|[consumable-19-9WenEIQrBV8POQU1.htm](equipment/consumable-19-9WenEIQrBV8POQU1.htm)|Panacea Fruit|auto-trad|
|[consumable-19-aAGjV3wkopQ44VX3.htm](equipment/consumable-19-aAGjV3wkopQ44VX3.htm)|Spellstrike Ammunition (Type IX)|auto-trad|
|[consumable-19-CorMilTH3XKGW49D.htm](equipment/consumable-19-CorMilTH3XKGW49D.htm)|Insight Coffee (Greater)|auto-trad|
|[consumable-19-Fgv722039TVM5JTc.htm](equipment/consumable-19-Fgv722039TVM5JTc.htm)|Magic Wand (9th-Level Spell)|auto-trad|
|[consumable-19-gn5ITN5GILHKxGA4.htm](equipment/consumable-19-gn5ITN5GILHKxGA4.htm)|Ambrosia of Undying Hope|auto-trad|
|[consumable-19-HD0RIJcRIIKKAdkn.htm](equipment/consumable-19-HD0RIJcRIIKKAdkn.htm)|Ablative Shield Plating (True)|auto-trad|
|[consumable-19-kUvjWSGWpwBvTVax.htm](equipment/consumable-19-kUvjWSGWpwBvTVax.htm)|Dragon's Blood Pudding (Major)|auto-trad|
|[consumable-19-m16rhrqO81674IxV.htm](equipment/consumable-19-m16rhrqO81674IxV.htm)|Pale Fade|auto-trad|
|[consumable-19-o1XIHJ4MJyroAHfF.htm](equipment/consumable-19-o1XIHJ4MJyroAHfF.htm)|Scroll of 10th-level Spell|auto-trad|
|[consumable-19-ommn7wIgLyWHGL11.htm](equipment/consumable-19-ommn7wIgLyWHGL11.htm)|Contagion Metabolizer (Greater)|auto-trad|
|[consumable-19-qK964kZz1ALcysOa.htm](equipment/consumable-19-qK964kZz1ALcysOa.htm)|Elixir of Life (True)|auto-trad|
|[consumable-19-RdsQliP7Gi1xe8xH.htm](equipment/consumable-19-RdsQliP7Gi1xe8xH.htm)|Ablative Armor Plating (True)|auto-trad|
|[consumable-19-SeIweTBlvD1EipTQ.htm](equipment/consumable-19-SeIweTBlvD1EipTQ.htm)|Chromatic Jellyfish Oil (Greater)|auto-trad|
|[consumable-19-SxXy4a8uHf88iCf9.htm](equipment/consumable-19-SxXy4a8uHf88iCf9.htm)|Spell Echo Shot|auto-trad|
|[consumable-19-xQS1MSqGQz44FWUh.htm](equipment/consumable-19-xQS1MSqGQz44FWUh.htm)|Black Lotus Extract|auto-trad|
|[consumable-20-4642hELMtWF9jywN.htm](equipment/consumable-20-4642hELMtWF9jywN.htm)|Golden Breath Fulu|auto-trad|
|[consumable-20-737LQfIaLXBPEzc3.htm](equipment/consumable-20-737LQfIaLXBPEzc3.htm)|Antimagic Oil|auto-trad|
|[consumable-20-dRi775Uhzqgn7aBs.htm](equipment/consumable-20-dRi775Uhzqgn7aBs.htm)|Nightmare Salt|auto-trad|
|[consumable-20-FB7qAHjiiHTGhRnn.htm](equipment/consumable-20-FB7qAHjiiHTGhRnn.htm)|Celestial Hair|auto-trad|
|[consumable-20-Fv97oB3iEIFAyzTu.htm](equipment/consumable-20-Fv97oB3iEIFAyzTu.htm)|Philosopher's Stone|auto-trad|
|[consumable-20-gDGPvobJV1QGYBPy.htm](equipment/consumable-20-gDGPvobJV1QGYBPy.htm)|Tears of Death|auto-trad|
|[consumable-20-Im9LfiRTfAJxPvzS.htm](equipment/consumable-20-Im9LfiRTfAJxPvzS.htm)|Thousand-Pains Fulu (Void)|auto-trad|
|[consumable-20-J5LsHDGEv4CVN7vr.htm](equipment/consumable-20-J5LsHDGEv4CVN7vr.htm)|Potion of Annulment (Greater)|auto-trad|
|[consumable-20-ktAjhlI9NxpFJMdp.htm](equipment/consumable-20-ktAjhlI9NxpFJMdp.htm)|Celestial Peach (Life)|auto-trad|
|[consumable-20-NUlz1XulrXqhpd2w.htm](equipment/consumable-20-NUlz1XulrXqhpd2w.htm)|Achaekek's Kiss|auto-trad|
|[consumable-20-P9dhnMcr3lbHBn2E.htm](equipment/consumable-20-P9dhnMcr3lbHBn2E.htm)|Sun Orchid Elixir|auto-trad|
|[consumable-20-pbsrb7FMMHQBd5CO.htm](equipment/consumable-20-pbsrb7FMMHQBd5CO.htm)|Starsong Nectar|auto-trad|
|[consumable-20-qvfO0jFIvIM8hReG.htm](equipment/consumable-20-qvfO0jFIvIM8hReG.htm)|Flying Blade Wheel Snare|auto-trad|
|[consumable-20-UHrLqWCnFEUspSQj.htm](equipment/consumable-20-UHrLqWCnFEUspSQj.htm)|Elixir of Rejuvenation|auto-trad|
|[consumable-20-UzyjCLHxs3JQuFt5.htm](equipment/consumable-20-UzyjCLHxs3JQuFt5.htm)|Life Shot (True)|auto-trad|
|[consumable-20-X3DCIU2fmj97pi1J.htm](equipment/consumable-20-X3DCIU2fmj97pi1J.htm)|Bonmuan Swapping Stone (Major)|auto-trad|
|[consumable-20-xqCz2vStMNJujfpp.htm](equipment/consumable-20-xqCz2vStMNJujfpp.htm)|Instant Evisceration Snare|auto-trad|
|[consumable-20-zZZRK9AJ5VGIcbzU.htm](equipment/consumable-20-zZZRK9AJ5VGIcbzU.htm)|Death Coil|auto-trad|
|[equipment-00-02luiB9xtE1yaKzo.htm](equipment/equipment-00-02luiB9xtE1yaKzo.htm)|Splint|auto-trad|
|[equipment-00-0BEbpOMBo7ET8NsR.htm](equipment/equipment-00-0BEbpOMBo7ET8NsR.htm)|Silencer|auto-trad|
|[equipment-00-1RiYM3lQDx30XEfB.htm](equipment/equipment-00-1RiYM3lQDx30XEfB.htm)|Firearm Cleaning Kit|auto-trad|
|[equipment-00-1YkOplOqn9OgGLyg.htm](equipment/equipment-00-1YkOplOqn9OgGLyg.htm)|Extendable Pincer|auto-trad|
|[equipment-00-2c5pF6q5XkjLn8MU.htm](equipment/equipment-00-2c5pF6q5XkjLn8MU.htm)|Mask (Ordinary)|auto-trad|
|[equipment-00-2fVNJ23gb3dixVu8.htm](equipment/equipment-00-2fVNJ23gb3dixVu8.htm)|Buoyancy Vest|auto-trad|
|[equipment-00-2qJyAnnVIMFIdeTD.htm](equipment/equipment-00-2qJyAnnVIMFIdeTD.htm)|Puzzle Box (Simple) (Hollow)|auto-trad|
|[equipment-00-3MGQ8KxH7HNA8THZ.htm](equipment/equipment-00-3MGQ8KxH7HNA8THZ.htm)|Jellyfish Lamp|auto-trad|
|[equipment-00-3tOyV4VZEZhwnAMO.htm](equipment/equipment-00-3tOyV4VZEZhwnAMO.htm)|Fishing Tackle|auto-trad|
|[equipment-00-3yLu12TCwN8utY0u.htm](equipment/equipment-00-3yLu12TCwN8utY0u.htm)|Writing Set (Extra Ink and Paper)|auto-trad|
|[equipment-00-3ZHl40YXlI47uIrT.htm](equipment/equipment-00-3ZHl40YXlI47uIrT.htm)|Basic Cane|auto-trad|
|[equipment-00-44F1mfJei4GY8f2X.htm](equipment/equipment-00-44F1mfJei4GY8f2X.htm)|Crowbar|auto-trad|
|[equipment-00-4AnNkIA5Ux6ePy7r.htm](equipment/equipment-00-4AnNkIA5Ux6ePy7r.htm)|Orichalcum Ingot|auto-trad|
|[equipment-00-4QufuSeCbjDD2hMa.htm](equipment/equipment-00-4QufuSeCbjDD2hMa.htm)|Comealong|auto-trad|
|[equipment-00-5j5KyZsGOfbrJNUZ.htm](equipment/equipment-00-5j5KyZsGOfbrJNUZ.htm)|Ladder (10-foot)|auto-trad|
|[equipment-00-5NHciGwLcVJwEjQu.htm](equipment/equipment-00-5NHciGwLcVJwEjQu.htm)|Darkwood Branch|auto-trad|
|[equipment-00-6cyw0yK91cNsbvSK.htm](equipment/equipment-00-6cyw0yK91cNsbvSK.htm)|Mirror|auto-trad|
|[equipment-00-6DCy7tEF1cxaIJMy.htm](equipment/equipment-00-6DCy7tEF1cxaIJMy.htm)|Grappling Hook|auto-trad|
|[equipment-00-7C5xxZw1VTiyUg37.htm](equipment/equipment-00-7C5xxZw1VTiyUg37.htm)|Grappling Gun|auto-trad|
|[equipment-00-7fSnvJ2xoSfa6JXD.htm](equipment/equipment-00-7fSnvJ2xoSfa6JXD.htm)|Caltrops|auto-trad|
|[equipment-00-7HHKalX0yYJSHROS.htm](equipment/equipment-00-7HHKalX0yYJSHROS.htm)|Depth Gauge|auto-trad|
|[equipment-00-7XJQmpYWRZRRutc4.htm](equipment/equipment-00-7XJQmpYWRZRRutc4.htm)|Peachwood Branch|auto-trad|
|[equipment-00-81aHsD27HFGnq1Nt.htm](equipment/equipment-00-81aHsD27HFGnq1Nt.htm)|Soap|auto-trad|
|[equipment-00-85FCQF18RslpIhPs.htm](equipment/equipment-00-85FCQF18RslpIhPs.htm)|Toy Carriage|auto-trad|
|[equipment-00-8Jdw4yAzWYylGePS.htm](equipment/equipment-00-8Jdw4yAzWYylGePS.htm)|Torch|auto-trad|
|[equipment-00-8SkeqnhhYm4QGuJQ.htm](equipment/equipment-00-8SkeqnhhYm4QGuJQ.htm)|Basic Companion Chair|auto-trad|
|[equipment-00-8WH6ub3FVFYtcXCT.htm](equipment/equipment-00-8WH6ub3FVFYtcXCT.htm)|Adamantine Ingot|auto-trad|
|[equipment-00-937vrPcl9rpzdtJw.htm](equipment/equipment-00-937vrPcl9rpzdtJw.htm)|Clockwork Dial|auto-trad|
|[equipment-00-9UJbMaglf35GVzaZ.htm](equipment/equipment-00-9UJbMaglf35GVzaZ.htm)|Climbing Kit|auto-trad|
|[equipment-00-A1CDXAP4bNGgntE7.htm](equipment/equipment-00-A1CDXAP4bNGgntE7.htm)|Ball|auto-trad|
|[equipment-00-AiYlhL35OEtkqsHo.htm](equipment/equipment-00-AiYlhL35OEtkqsHo.htm)|Storage|auto-trad|
|[equipment-00-AnJiuDSgJ1uBYRaZ.htm](equipment/equipment-00-AnJiuDSgJ1uBYRaZ.htm)|Snowshoes|auto-trad|
|[equipment-00-aV7cXniPdT6tEI2l.htm](equipment/equipment-00-aV7cXniPdT6tEI2l.htm)|Waffle Iron|auto-trad|
|[equipment-00-AxUJfrOmu7dY3XHP.htm](equipment/equipment-00-AxUJfrOmu7dY3XHP.htm)|Waterproof Firearm Carrying Case|auto-trad|
|[equipment-00-B0gbs7xKQc2J7AiV.htm](equipment/equipment-00-B0gbs7xKQc2J7AiV.htm)|Mithral Chunk|auto-trad|
|[equipment-00-B3aJa0csNZOGXLXT.htm](equipment/equipment-00-B3aJa0csNZOGXLXT.htm)|Harrow Deck (Common)|auto-trad|
|[equipment-00-b4hSaH0ITd5fbR08.htm](equipment/equipment-00-b4hSaH0ITd5fbR08.htm)|Peachwood Lumber|auto-trad|
|[equipment-00-BbWTlbl43SmPNIdo.htm](equipment/equipment-00-BbWTlbl43SmPNIdo.htm)|Support|auto-trad|
|[equipment-00-bkbL0YitEh46Ne0f.htm](equipment/equipment-00-bkbL0YitEh46Ne0f.htm)|Armored Skirt|auto-trad|
|[equipment-00-Bu2xT8NfB6xaeTJa.htm](equipment/equipment-00-Bu2xT8NfB6xaeTJa.htm)|Tack|auto-trad|
|[equipment-00-C1j0Zs26TPVjplbs.htm](equipment/equipment-00-C1j0Zs26TPVjplbs.htm)|Harrow Deck (Simple)|auto-trad|
|[equipment-00-ccsgob2TZ7WqTrp7.htm](equipment/equipment-00-ccsgob2TZ7WqTrp7.htm)|Ten-Foot Pole|auto-trad|
|[equipment-00-csfJtggwGCF28U2j.htm](equipment/equipment-00-csfJtggwGCF28U2j.htm)|Musical Instrument (Heavy)|auto-trad|
|[equipment-00-DA3HgyEBGEbtRNOo.htm](equipment/equipment-00-DA3HgyEBGEbtRNOo.htm)|Dueling Cape|auto-trad|
|[equipment-00-Da78zUcKodY81Sj4.htm](equipment/equipment-00-Da78zUcKodY81Sj4.htm)|Kite|auto-trad|
|[equipment-00-DAqI0GwKuRwBagz5.htm](equipment/equipment-00-DAqI0GwKuRwBagz5.htm)|False Manacles|auto-trad|
|[equipment-00-dIRZ0LL7G31fJNYz.htm](equipment/equipment-00-dIRZ0LL7G31fJNYz.htm)|Lantern (Hooded)|auto-trad|
|[equipment-00-EcLENuK2LXZdigTu.htm](equipment/equipment-00-EcLENuK2LXZdigTu.htm)|Cage|auto-trad|
|[equipment-00-eQjibJl41aQpQ47d.htm](equipment/equipment-00-eQjibJl41aQpQ47d.htm)|Cold Iron Ingot|auto-trad|
|[equipment-00-EsqFTJXcmzfngRgL.htm](equipment/equipment-00-EsqFTJXcmzfngRgL.htm)|Playing Cards|auto-trad|
|[equipment-00-ETwv7GshGM9IXnqH.htm](equipment/equipment-00-ETwv7GshGM9IXnqH.htm)|Hollowed Hilt|auto-trad|
|[equipment-00-f0lZf1oZYDEEeyYl.htm](equipment/equipment-00-f0lZf1oZYDEEeyYl.htm)|Sovereign Steel Chunk|auto-trad|
|[equipment-00-fagzYdmfYyMQ6J77.htm](equipment/equipment-00-fagzYdmfYyMQ6J77.htm)|Bedroll|auto-trad|
|[equipment-00-fnyjcHzySnUxxSeW.htm](equipment/equipment-00-fnyjcHzySnUxxSeW.htm)|Shield Augmentation|auto-trad|
|[equipment-00-FOWF5f0tCaApv9RE.htm](equipment/equipment-00-FOWF5f0tCaApv9RE.htm)|Spellbook (Blank)|auto-trad|
|[equipment-00-FphoSIj3t6iQj3ew.htm](equipment/equipment-00-FphoSIj3t6iQj3ew.htm)|Harness|auto-trad|
|[equipment-00-fti8z0JYDxH63n0J.htm](equipment/equipment-00-fti8z0JYDxH63n0J.htm)|Ruler|auto-trad|
|[equipment-00-FWMGkUotwUJuht7i.htm](equipment/equipment-00-FWMGkUotwUJuht7i.htm)|Disguise Kit (Replacement Cosmetics)|auto-trad|
|[equipment-00-fyYnQf1NAx9fWFaS.htm](equipment/equipment-00-fyYnQf1NAx9fWFaS.htm)|Rope|auto-trad|
|[equipment-00-ge0PEbPmxFLhyy4X.htm](equipment/equipment-00-ge0PEbPmxFLhyy4X.htm)|Marbles|auto-trad|
|[equipment-00-GpcZXdiJppItyWJg.htm](equipment/equipment-00-GpcZXdiJppItyWJg.htm)|Wheelbarrow|auto-trad|
|[equipment-00-GVdbmKFLi5PlMr5R.htm](equipment/equipment-00-GVdbmKFLi5PlMr5R.htm)|Basic Hearing Aid|auto-trad|
|[equipment-00-gwP3Uums2ApH6o9K.htm](equipment/equipment-00-gwP3Uums2ApH6o9K.htm)|Pilgrim's Token|auto-trad|
|[equipment-00-h4fhKJmmx232FGd2.htm](equipment/equipment-00-h4fhKJmmx232FGd2.htm)|Basic Crutch|auto-trad|
|[equipment-00-h7gsRKv6rORYgsv0.htm](equipment/equipment-00-h7gsRKv6rORYgsv0.htm)|Snare Kit|auto-trad|
|[equipment-00-h8XkyI4OUitdxjMF.htm](equipment/equipment-00-h8XkyI4OUitdxjMF.htm)|Leash|auto-trad|
|[equipment-00-HB7VTopmeRZQGc0z.htm](equipment/equipment-00-HB7VTopmeRZQGc0z.htm)|Reinforced Surcoat|auto-trad|
|[equipment-00-HEtJeLXMcMbgamxd.htm](equipment/equipment-00-HEtJeLXMcMbgamxd.htm)|Thresholds of Truth|auto-trad|
|[equipment-00-Hl2JP8PQKrKxAfAD.htm](equipment/equipment-00-Hl2JP8PQKrKxAfAD.htm)|Bandalore|auto-trad|
|[equipment-00-HYnwTFuLsFSsUAMG.htm](equipment/equipment-00-HYnwTFuLsFSsUAMG.htm)|Splendid Skull Mask|auto-trad|
|[equipment-00-IGgK7NAdfyAPm8V0.htm](equipment/equipment-00-IGgK7NAdfyAPm8V0.htm)|Basic Face Mask|auto-trad|
|[equipment-00-ilbLQNy6TbBPW7sQ.htm](equipment/equipment-00-ilbLQNy6TbBPW7sQ.htm)|Clothing (Ordinary)|auto-trad|
|[equipment-00-IM2pz0wnO4OEMr1f.htm](equipment/equipment-00-IM2pz0wnO4OEMr1f.htm)|Blocks|auto-trad|
|[equipment-00-iYiA6CMR1N2E2Ny2.htm](equipment/equipment-00-iYiA6CMR1N2E2Ny2.htm)|Earplugs (PFS Guide)|auto-trad|
|[equipment-00-jerA2fFiK5wb32r5.htm](equipment/equipment-00-jerA2fFiK5wb32r5.htm)|Silver Ingot|auto-trad|
|[equipment-00-jJZbdMHMdh8UL2j8.htm](equipment/equipment-00-jJZbdMHMdh8UL2j8.htm)|Clothing (Fine)|auto-trad|
|[equipment-00-JlClrm1WfJ45oWcS.htm](equipment/equipment-00-JlClrm1WfJ45oWcS.htm)|Tent (Four-Person)|auto-trad|
|[equipment-00-jLVS6JezrxJ8G94C.htm](equipment/equipment-00-jLVS6JezrxJ8G94C.htm)|Paint Set|auto-trad|
|[equipment-00-JU0fzXWJIdz9WOGF.htm](equipment/equipment-00-JU0fzXWJIdz9WOGF.htm)|Tank (Stationary)|auto-trad|
|[equipment-00-Jvp0x2Sc82WVpExT.htm](equipment/equipment-00-Jvp0x2Sc82WVpExT.htm)|Disguise Kit|auto-trad|
|[equipment-00-JxT2I1jUqJ59JM8e.htm](equipment/equipment-00-JxT2I1jUqJ59JM8e.htm)|Noqual Chunk|auto-trad|
|[equipment-00-k6xZkDUpF7E1Totd.htm](equipment/equipment-00-k6xZkDUpF7E1Totd.htm)|Alchemist's Lab|auto-trad|
|[equipment-00-kDrlBnkDMVxPrahI.htm](equipment/equipment-00-kDrlBnkDMVxPrahI.htm)|Water Purifier|auto-trad|
|[equipment-00-KGA495XLcsDVonEt.htm](equipment/equipment-00-KGA495XLcsDVonEt.htm)|Siccatite Chunk|auto-trad|
|[equipment-00-KIpkxwuPWt0rOGT1.htm](equipment/equipment-00-KIpkxwuPWt0rOGT1.htm)|Swim Fins|auto-trad|
|[equipment-00-ksNUlyRHFUSK9xmz.htm](equipment/equipment-00-ksNUlyRHFUSK9xmz.htm)|Earplugs|auto-trad|
|[equipment-00-kz2Trn8ETSlGv0wy.htm](equipment/equipment-00-kz2Trn8ETSlGv0wy.htm)|Pinwheel|auto-trad|
|[equipment-00-Ld8duhvwTQ5lCQmF.htm](equipment/equipment-00-Ld8duhvwTQ5lCQmF.htm)|Silver Chunk|auto-trad|
|[equipment-00-M91ye9B7IdxDIDsS.htm](equipment/equipment-00-M91ye9B7IdxDIDsS.htm)|Brass Ear|auto-trad|
|[equipment-00-ML0GR2cIEdMn2hoD.htm](equipment/equipment-00-ML0GR2cIEdMn2hoD.htm)|Hammer|auto-trad|
|[equipment-00-MPv5Yx4w7scZGj2Y.htm](equipment/equipment-00-MPv5Yx4w7scZGj2Y.htm)|Musical Instrument (Handheld)|auto-trad|
|[equipment-00-nmSMa84RpsNv6pSt.htm](equipment/equipment-00-nmSMa84RpsNv6pSt.htm)|Djezet Mass|auto-trad|
|[equipment-00-o0ccn5HQAudGgNdU.htm](equipment/equipment-00-o0ccn5HQAudGgNdU.htm)|Merchant's Scale|auto-trad|
|[equipment-00-O28nMnWDcXpMtrMv.htm](equipment/equipment-00-O28nMnWDcXpMtrMv.htm)|Djezet Alloy Ingot|auto-trad|
|[equipment-00-o29fgVteF3yj8NKn.htm](equipment/equipment-00-o29fgVteF3yj8NKn.htm)|Parrying Scabbard|auto-trad|
|[equipment-00-O6XNICvMAolYdgFP.htm](equipment/equipment-00-O6XNICvMAolYdgFP.htm)|False-Bottomed Mug|auto-trad|
|[equipment-00-OAjI2eaXvHQRrERK.htm](equipment/equipment-00-OAjI2eaXvHQRrERK.htm)|Tear-Away Clothing|auto-trad|
|[equipment-00-oDyRlkRos1abOGz0.htm](equipment/equipment-00-oDyRlkRos1abOGz0.htm)|Handling Gloves|auto-trad|
|[equipment-00-oJZe5rRitvioUgRh.htm](equipment/equipment-00-oJZe5rRitvioUgRh.htm)|Shootist Bandolier|auto-trad|
|[equipment-00-oLLpwiNApEEAFbXF.htm](equipment/equipment-00-oLLpwiNApEEAFbXF.htm)|Abysium Chunk|auto-trad|
|[equipment-00-OnyNpbR0dowRKg3I.htm](equipment/equipment-00-OnyNpbR0dowRKg3I.htm)|Mask (Fine)|auto-trad|
|[equipment-00-OP1TkZ9ugn86W4Br.htm](equipment/equipment-00-OP1TkZ9ugn86W4Br.htm)|Harrow Deck (Fine)|auto-trad|
|[equipment-00-oX39xqMLVB8kNrY0.htm](equipment/equipment-00-oX39xqMLVB8kNrY0.htm)|Piton|auto-trad|
|[equipment-00-P4hCmpeuBochkxsJ.htm](equipment/equipment-00-P4hCmpeuBochkxsJ.htm)|Harrow Carrying Case|auto-trad|
|[equipment-00-PBlxTrEyr3qXFBqq.htm](equipment/equipment-00-PBlxTrEyr3qXFBqq.htm)|Lock (Poor)|auto-trad|
|[equipment-00-plplsXJsqrdqNQVI.htm](equipment/equipment-00-plplsXJsqrdqNQVI.htm)|Religious Symbol (Wooden)|auto-trad|
|[equipment-00-Q4KkKGGXq4bNGHh2.htm](equipment/equipment-00-Q4KkKGGXq4bNGHh2.htm)|Marked Playing Cards|auto-trad|
|[equipment-00-QbOlqr4lSkeOEfty.htm](equipment/equipment-00-QbOlqr4lSkeOEfty.htm)|Holly and Mistletoe|auto-trad|
|[equipment-00-qCEOZ6109Yo34tRx.htm](equipment/equipment-00-qCEOZ6109Yo34tRx.htm)|Formula Book (Blank)|auto-trad|
|[equipment-00-QFAHoE7FJ9TPCGWL.htm](equipment/equipment-00-QFAHoE7FJ9TPCGWL.htm)|Rubbing Set|auto-trad|
|[equipment-00-QFzn6s5w9tzhcoM6.htm](equipment/equipment-00-QFzn6s5w9tzhcoM6.htm)|Basic Corrective Lenses|auto-trad|
|[equipment-00-QJb8S927Yj81EgHH.htm](equipment/equipment-00-QJb8S927Yj81EgHH.htm)|Writing Set|auto-trad|
|[equipment-00-qnpqMwAnRc55R7lv.htm](equipment/equipment-00-qnpqMwAnRc55R7lv.htm)|Noqual Ingot|auto-trad|
|[equipment-00-QpP7Mo7ad5lMOTpv.htm](equipment/equipment-00-QpP7Mo7ad5lMOTpv.htm)|Mithral Ingot|auto-trad|
|[equipment-00-QrNvP9SgnK9DrerA.htm](equipment/equipment-00-QrNvP9SgnK9DrerA.htm)|Lantern (Bull's Eye)|auto-trad|
|[equipment-00-r9lPNhrvG69Ae88d.htm](equipment/equipment-00-r9lPNhrvG69Ae88d.htm)|Darkwood Lumber|auto-trad|
|[equipment-00-rEz9WYhlx2Pm0gKk.htm](equipment/equipment-00-rEz9WYhlx2Pm0gKk.htm)|Cold Iron Chunk|auto-trad|
|[equipment-00-rHugmTjO3kgyiTH0.htm](equipment/equipment-00-rHugmTjO3kgyiTH0.htm)|Air Bladder|auto-trad|
|[equipment-00-S0hmnalGCudgXaLG.htm](equipment/equipment-00-S0hmnalGCudgXaLG.htm)|Tank (Traveling)|auto-trad|
|[equipment-00-SLQgdLk5K9JV4ACF.htm](equipment/equipment-00-SLQgdLk5K9JV4ACF.htm)|Clothing (Winter)|auto-trad|
|[equipment-00-Sw7MBLASN3xK4Y44.htm](equipment/equipment-00-Sw7MBLASN3xK4Y44.htm)|Thieves' Tools (Replacement Picks)|auto-trad|
|[equipment-00-T8EopYZLT137CsdW.htm](equipment/equipment-00-T8EopYZLT137CsdW.htm)|Net|auto-trad|
|[equipment-00-TqqWFIiJYA5tXlSE.htm](equipment/equipment-00-TqqWFIiJYA5tXlSE.htm)|Animal Bed|auto-trad|
|[equipment-00-ty9LXqqYAnlsTh46.htm](equipment/equipment-00-ty9LXqqYAnlsTh46.htm)|Tripod|auto-trad|
|[equipment-00-UlIxxLm71UdRgCFE.htm](equipment/equipment-00-UlIxxLm71UdRgCFE.htm)|Flint and Steel|auto-trad|
|[equipment-00-upzjwQ96cZG0Xlmx.htm](equipment/equipment-00-upzjwQ96cZG0Xlmx.htm)|Religious Symbol (Silver)|auto-trad|
|[equipment-00-USHK6XQRwmq17xKh.htm](equipment/equipment-00-USHK6XQRwmq17xKh.htm)|Signal Whistle|auto-trad|
|[equipment-00-UtRH6jt3iNFCgLLG.htm](equipment/equipment-00-UtRH6jt3iNFCgLLG.htm)|Spyglass|auto-trad|
|[equipment-00-uWe8GXBjAdA0q4ad.htm](equipment/equipment-00-uWe8GXBjAdA0q4ad.htm)|Voicebox|auto-trad|
|[equipment-00-V78itAtTHI4ugy0P.htm](equipment/equipment-00-V78itAtTHI4ugy0P.htm)|Chain (10 feet)|auto-trad|
|[equipment-00-VBaxMSVNY2n6kLzT.htm](equipment/equipment-00-VBaxMSVNY2n6kLzT.htm)|Basic Prosthesis|auto-trad|
|[equipment-00-VbjANEHtdxO8kV1n.htm](equipment/equipment-00-VbjANEHtdxO8kV1n.htm)|Abysium Ingot|auto-trad|
|[equipment-00-VHxXMvBeBTq2FSdf.htm](equipment/equipment-00-VHxXMvBeBTq2FSdf.htm)|Material Component Pouch|auto-trad|
|[equipment-00-vLGDUFrg4yGzpTQX.htm](equipment/equipment-00-vLGDUFrg4yGzpTQX.htm)|Repair Kit|auto-trad|
|[equipment-00-vM88qqJrUo3Yqth4.htm](equipment/equipment-00-vM88qqJrUo3Yqth4.htm)|Traitor's Ring|auto-trad|
|[equipment-00-VnPh324pKwd2ZB66.htm](equipment/equipment-00-VnPh324pKwd2ZB66.htm)|Waterskin|auto-trad|
|[equipment-00-vQiH5HYTsSRIsdK5.htm](equipment/equipment-00-vQiH5HYTsSRIsdK5.htm)|Collar|auto-trad|
|[equipment-00-w4Hd6nunVVqw3GWj.htm](equipment/equipment-00-w4Hd6nunVVqw3GWj.htm)|Basic Crafter's Book|auto-trad|
|[equipment-00-w5pzAwbKGLOOomrK.htm](equipment/equipment-00-w5pzAwbKGLOOomrK.htm)|Wax Key Blank|auto-trad|
|[equipment-00-WAsQ6WuMYUB77Uh0.htm](equipment/equipment-00-WAsQ6WuMYUB77Uh0.htm)|Hourglass|auto-trad|
|[equipment-00-Wc9og4mTKZDH3xmz.htm](equipment/equipment-00-Wc9og4mTKZDH3xmz.htm)|Tent (Pup)|auto-trad|
|[equipment-00-wewWxypLaXcHjJ5P.htm](equipment/equipment-00-wewWxypLaXcHjJ5P.htm)|Glass Cutter|auto-trad|
|[equipment-00-WnxXVCjZcMHy4eGO.htm](equipment/equipment-00-WnxXVCjZcMHy4eGO.htm)|Clay|auto-trad|
|[equipment-00-wob6DB1FFdWs0mbp.htm](equipment/equipment-00-wob6DB1FFdWs0mbp.htm)|Compass|auto-trad|
|[equipment-00-wrpI5z7iWB8XvflQ.htm](equipment/equipment-00-wrpI5z7iWB8XvflQ.htm)|Tool (Long)|auto-trad|
|[equipment-00-wRSS5vP8ltrThsoC.htm](equipment/equipment-00-wRSS5vP8ltrThsoC.htm)|Mug|auto-trad|
|[equipment-00-WTVLL9xk0Tk87w93.htm](equipment/equipment-00-WTVLL9xk0Tk87w93.htm)|Fox Marble|auto-trad|
|[equipment-00-wvOYIeJjU3UDVgMG.htm](equipment/equipment-00-wvOYIeJjU3UDVgMG.htm)|Vial|auto-trad|
|[equipment-00-xE2CGurZp4eHc1Oc.htm](equipment/equipment-00-xE2CGurZp4eHc1Oc.htm)|Familiar Satchel|auto-trad|
|[equipment-00-XlmoKh60WMF9ou3J.htm](equipment/equipment-00-XlmoKh60WMF9ou3J.htm)|Signifier's Mask|auto-trad|
|[equipment-00-XUEoUlpB8yGaXjLc.htm](equipment/equipment-00-XUEoUlpB8yGaXjLc.htm)|Membership Cords|auto-trad|
|[equipment-00-xuTO0Lbu999HnBhK.htm](equipment/equipment-00-xuTO0Lbu999HnBhK.htm)|Sovereign Steel Ingot|auto-trad|
|[equipment-00-XV2EHt1RWHqzPeUT.htm](equipment/equipment-00-XV2EHt1RWHqzPeUT.htm)|Shield Sconce|auto-trad|
|[equipment-00-XxlCMccCPhl2pZMT.htm](equipment/equipment-00-XxlCMccCPhl2pZMT.htm)|Plated Duster|auto-trad|
|[equipment-00-Y5wqlqkmWy38vgeK.htm](equipment/equipment-00-Y5wqlqkmWy38vgeK.htm)|Siccatite Ingot|auto-trad|
|[equipment-00-y9Ln2q1EhD3Jdu9H.htm](equipment/equipment-00-y9Ln2q1EhD3Jdu9H.htm)|Inubrix Ingot|auto-trad|
|[equipment-00-Yf7maiRDmmHAyF82.htm](equipment/equipment-00-Yf7maiRDmmHAyF82.htm)|Cookware|auto-trad|
|[equipment-00-YOgUBjv2YHzPFKUb.htm](equipment/equipment-00-YOgUBjv2YHzPFKUb.htm)|Orichalcum Chunk|auto-trad|
|[equipment-00-yxDOmx3VYSWoAEMa.htm](equipment/equipment-00-yxDOmx3VYSWoAEMa.htm)|Scroll Case|auto-trad|
|[equipment-00-yXipdeJUEGMPT4dM.htm](equipment/equipment-00-yXipdeJUEGMPT4dM.htm)|Knight's Standard|auto-trad|
|[equipment-00-z1okOYtNVnpBNj9F.htm](equipment/equipment-00-z1okOYtNVnpBNj9F.htm)|Religious Text|auto-trad|
|[equipment-00-Z5xNCrAN3TgdFTOV.htm](equipment/equipment-00-Z5xNCrAN3TgdFTOV.htm)|Splendid Pyschopomp Mask|auto-trad|
|[equipment-00-zDzs24ZMlzQgmTUd.htm](equipment/equipment-00-zDzs24ZMlzQgmTUd.htm)|Folding Ladder|auto-trad|
|[equipment-00-Ze64YjceQqnwRCsU.htm](equipment/equipment-00-Ze64YjceQqnwRCsU.htm)|Harrow Mat|auto-trad|
|[equipment-00-ZEDDVQDtUZ2qOB5q.htm](equipment/equipment-00-ZEDDVQDtUZ2qOB5q.htm)|Orc Warmask|auto-trad|
|[equipment-00-zKrGossz3t0mQrQq.htm](equipment/equipment-00-zKrGossz3t0mQrQq.htm)|Inubrix Chunk|auto-trad|
|[equipment-00-Zo5MZWVBKssVPEcv.htm](equipment/equipment-00-Zo5MZWVBKssVPEcv.htm)|Adamantine Chunk|auto-trad|
|[equipment-00-ZPFDfWPmWLPUJeW1.htm](equipment/equipment-00-ZPFDfWPmWLPUJeW1.htm)|Doll|auto-trad|
|[equipment-00-ZPY2dOaMYciIJv1Q.htm](equipment/equipment-00-ZPY2dOaMYciIJv1Q.htm)|Manacles (Poor)|auto-trad|
|[equipment-00-Zr4PwTEuBLE30kkn.htm](equipment/equipment-00-Zr4PwTEuBLE30kkn.htm)|Tool (Short)|auto-trad|
|[equipment-00-ZRaZSTEOpTC1ObdE.htm](equipment/equipment-00-ZRaZSTEOpTC1ObdE.htm)|Puzzle Box (Simple)|auto-trad|
|[equipment-00-ZWTK4Q2JlvJ5Bx6A.htm](equipment/equipment-00-ZWTK4Q2JlvJ5Bx6A.htm)|Basic Chair|auto-trad|
|[equipment-00-Zycu6zaGvDsqLH5g.htm](equipment/equipment-00-Zycu6zaGvDsqLH5g.htm)|Sheath|auto-trad|
|[equipment-01-00gDg8WcPv3TKC9N.htm](equipment/equipment-01-00gDg8WcPv3TKC9N.htm)|Olfactory Stimulators|auto-trad|
|[equipment-01-20LwBpDPRaLv0dGt.htm](equipment/equipment-01-20LwBpDPRaLv0dGt.htm)|Magical Prosthetic Eye|auto-trad|
|[equipment-01-2mt15LcfMArk7JqT.htm](equipment/equipment-01-2mt15LcfMArk7JqT.htm)|Ganjay Book (1 bulk book)|auto-trad|
|[equipment-01-4CuEQqahVT8TmNld.htm](equipment/equipment-01-4CuEQqahVT8TmNld.htm)|Net Launcher|auto-trad|
|[equipment-01-4mW1K9vZ0VIIEazS.htm](equipment/equipment-01-4mW1K9vZ0VIIEazS.htm)|Weapon Harness|auto-trad|
|[equipment-01-4qhnEsaypUjLm7eP.htm](equipment/equipment-01-4qhnEsaypUjLm7eP.htm)|Burnished Plating|auto-trad|
|[equipment-01-4rJjASdTnQO2zkt2.htm](equipment/equipment-01-4rJjASdTnQO2zkt2.htm)|Impulse Control|auto-trad|
|[equipment-01-5wanv9n8UnUQ852q.htm](equipment/equipment-01-5wanv9n8UnUQ852q.htm)|Portable Weapon Mount (Monopod)|auto-trad|
|[equipment-01-6lmTRwT1dNzgWcUd.htm](equipment/equipment-01-6lmTRwT1dNzgWcUd.htm)|Puzzle Box (Complex)|auto-trad|
|[equipment-01-6NEOf1UznG4Po0Qi.htm](equipment/equipment-01-6NEOf1UznG4Po0Qi.htm)|Games (Loaded Dice)|auto-trad|
|[equipment-01-8Rt9cf86G5Ve9Ent.htm](equipment/equipment-01-8Rt9cf86G5Ve9Ent.htm)|Magnetic Construction Set|auto-trad|
|[equipment-01-8SZ9FRW4rBhjyW3j.htm](equipment/equipment-01-8SZ9FRW4rBhjyW3j.htm)|Mask (Plague)|auto-trad|
|[equipment-01-9wBwuCi6jo4sfaLb.htm](equipment/equipment-01-9wBwuCi6jo4sfaLb.htm)|Puzzle Box (Complex) (Hollow)|auto-trad|
|[equipment-01-ApMC9PSOnj7cPAGL.htm](equipment/equipment-01-ApMC9PSOnj7cPAGL.htm)|Bomb Launcher|auto-trad|
|[equipment-01-ba7Yba3Qexj4DpnD.htm](equipment/equipment-01-ba7Yba3Qexj4DpnD.htm)|Magical Hearing Aids|auto-trad|
|[equipment-01-bWcH5lQ3AuaQwZ08.htm](equipment/equipment-01-bWcH5lQ3AuaQwZ08.htm)|Mudlily|auto-trad|
|[equipment-01-C5vVNVAHO5Kfaveo.htm](equipment/equipment-01-C5vVNVAHO5Kfaveo.htm)|Traveling Companion's Chair|auto-trad|
|[equipment-01-ckGYDocGEaelHfXF.htm](equipment/equipment-01-ckGYDocGEaelHfXF.htm)|Manacles (Simple)|auto-trad|
|[equipment-01-CVTbxCY85nLoHYuw.htm](equipment/equipment-01-CVTbxCY85nLoHYuw.htm)|Aeon Stone (Dull Grey)|auto-trad|
|[equipment-01-dj6sdn6b8Rw1Taih.htm](equipment/equipment-01-dj6sdn6b8Rw1Taih.htm)|Mechanical Torch|auto-trad|
|[equipment-01-fXD42N5cGUYCtvp2.htm](equipment/equipment-01-fXD42N5cGUYCtvp2.htm)|Cloak of Feline Rest|auto-trad|
|[equipment-01-FYv6k2zkd0MJ93la.htm](equipment/equipment-01-FYv6k2zkd0MJ93la.htm)|Eye Slash|auto-trad|
|[equipment-01-geAAUwfmOc5U0qOE.htm](equipment/equipment-01-geAAUwfmOc5U0qOE.htm)|Rhythm Bone|auto-trad|
|[equipment-01-gxfIiPebrvjXeALk.htm](equipment/equipment-01-gxfIiPebrvjXeALk.htm)|Mortal Chronicle|auto-trad|
|[equipment-01-H5N2CacR3pNtanKB.htm](equipment/equipment-01-H5N2CacR3pNtanKB.htm)|Doll (Surprise)|auto-trad|
|[equipment-01-hD95fU3S507iTHcb.htm](equipment/equipment-01-hD95fU3S507iTHcb.htm)|Injection Reservoir|auto-trad|
|[equipment-01-i2JwyZJpVZhlOObL.htm](equipment/equipment-01-i2JwyZJpVZhlOObL.htm)|Waterproof Journal|auto-trad|
|[equipment-01-i5SEEbSALTzH9rgg.htm](equipment/equipment-01-i5SEEbSALTzH9rgg.htm)|Psychopomp Mask|auto-trad|
|[equipment-01-iKKZdkn8KQJSuRjY.htm](equipment/equipment-01-iKKZdkn8KQJSuRjY.htm)|Stalk Goggles|auto-trad|
|[equipment-01-JFd6QBDtSNC43dy0.htm](equipment/equipment-01-JFd6QBDtSNC43dy0.htm)|Traveler's Chair|auto-trad|
|[equipment-01-JZXCTwoIWoGKjMX6.htm](equipment/equipment-01-JZXCTwoIWoGKjMX6.htm)|Lock (Simple)|auto-trad|
|[equipment-01-k7ExX8IPY7mSzYPF.htm](equipment/equipment-01-k7ExX8IPY7mSzYPF.htm)|Reading Ring|auto-trad|
|[equipment-01-khU38JBZBOAxWzhY.htm](equipment/equipment-01-khU38JBZBOAxWzhY.htm)|Replacement Filter (Level 1)|auto-trad|
|[equipment-01-KKhw57JFYEUO4TnK.htm](equipment/equipment-01-KKhw57JFYEUO4TnK.htm)|Timepiece (Desktop Clock)|auto-trad|
|[equipment-01-LiK84TSQJoe1e6D7.htm](equipment/equipment-01-LiK84TSQJoe1e6D7.htm)|Clockwork Megaphone|auto-trad|
|[equipment-01-m4U2P5A1Dk1C1wr1.htm](equipment/equipment-01-m4U2P5A1Dk1C1wr1.htm)|Ignitor|auto-trad|
|[equipment-01-MHJLBBESka5n3Bsg.htm](equipment/equipment-01-MHJLBBESka5n3Bsg.htm)|Traveler's Cloak|auto-trad|
|[equipment-01-MJ1wJbEXRw5KcvwN.htm](equipment/equipment-01-MJ1wJbEXRw5KcvwN.htm)|Clockwork Bookshelf|auto-trad|
|[equipment-01-MpgoMNvnvy3Ysskq.htm](equipment/equipment-01-MpgoMNvnvy3Ysskq.htm)|Navigator's Star|auto-trad|
|[equipment-01-mRz8Jmk4Q06SsZpC.htm](equipment/equipment-01-mRz8Jmk4Q06SsZpC.htm)|Everburning Torch|auto-trad|
|[equipment-01-n5Or4nmtKPoFr6Hm.htm](equipment/equipment-01-n5Or4nmtKPoFr6Hm.htm)|Blade Launcher|auto-trad|
|[equipment-01-NocVi59Vtbi3kvsJ.htm](equipment/equipment-01-NocVi59Vtbi3kvsJ.htm)|Deployable Cover|auto-trad|
|[equipment-01-PIR3RduvFcou4xz6.htm](equipment/equipment-01-PIR3RduvFcou4xz6.htm)|Glittering Scarab|auto-trad|
|[equipment-01-Q2MhPIDRXgD0K6C4.htm](equipment/equipment-01-Q2MhPIDRXgD0K6C4.htm)|Memoir Map|auto-trad|
|[equipment-01-STF54Mk8rE7uSMbv.htm](equipment/equipment-01-STF54Mk8rE7uSMbv.htm)|Waffle Iron (Imprint)|auto-trad|
|[equipment-01-thWEk8pHq1hqyhMn.htm](equipment/equipment-01-thWEk8pHq1hqyhMn.htm)|Communication Bangle|auto-trad|
|[equipment-01-touk9g0Lc2REwowo.htm](equipment/equipment-01-touk9g0Lc2REwowo.htm)|Weapon Siphon|auto-trad|
|[equipment-01-TyWs8hIOYxjcm037.htm](equipment/equipment-01-TyWs8hIOYxjcm037.htm)|Grappling Gun (Clockwork)|auto-trad|
|[equipment-01-Uz5K5773R3DB3b3o.htm](equipment/equipment-01-Uz5K5773R3DB3b3o.htm)|Mask (Rubber)|auto-trad|
|[equipment-01-VKHKaCMRF30GWvZ0.htm](equipment/equipment-01-VKHKaCMRF30GWvZ0.htm)|Twining Chains|auto-trad|
|[equipment-01-WKT5mW2EccS22989.htm](equipment/equipment-01-WKT5mW2EccS22989.htm)|Guide Harness|auto-trad|
|[equipment-01-wTYxAWrdsQ6SLVr9.htm](equipment/equipment-01-wTYxAWrdsQ6SLVr9.htm)|Genealogy Mask|auto-trad|
|[equipment-01-XLuRWK7QluA2NUNn.htm](equipment/equipment-01-XLuRWK7QluA2NUNn.htm)|Sibling's Coin|auto-trad|
|[equipment-01-ydpA0pz0pPNqIUXU.htm](equipment/equipment-01-ydpA0pz0pPNqIUXU.htm)|Throwing Shield|auto-trad|
|[equipment-01-yvS4UatIZAGH4tng.htm](equipment/equipment-01-yvS4UatIZAGH4tng.htm)|Ganjay Book (Light bulk book)|auto-trad|
|[equipment-01-ZIQAzOavTXJCcCMD.htm](equipment/equipment-01-ZIQAzOavTXJCcCMD.htm)|Predictable Silver Piece|auto-trad|
|[equipment-01-Zmz2M2u7mGOCbUf4.htm](equipment/equipment-01-Zmz2M2u7mGOCbUf4.htm)|Walking Cauldron|auto-trad|
|[equipment-02-4kKFzTy2Rlwg2xJG.htm](equipment/equipment-02-4kKFzTy2Rlwg2xJG.htm)|Legerdemain Handkerchief|auto-trad|
|[equipment-02-85lwgBrZh0cB3PX4.htm](equipment/equipment-02-85lwgBrZh0cB3PX4.htm)|Day Goggles|auto-trad|
|[equipment-02-9ajoltiJ2T60MLAh.htm](equipment/equipment-02-9ajoltiJ2T60MLAh.htm)|Soulspeaker|auto-trad|
|[equipment-02-9dKzjpAQiE48AIWD.htm](equipment/equipment-02-9dKzjpAQiE48AIWD.htm)|Empathy Charm|auto-trad|
|[equipment-02-9uhJ7OB65ZTdUc8b.htm](equipment/equipment-02-9uhJ7OB65ZTdUc8b.htm)|Necklace of Knives|auto-trad|
|[equipment-02-aGkmq9k4QIEpLFs7.htm](equipment/equipment-02-aGkmq9k4QIEpLFs7.htm)|Hat of Disagreeable Disguise|auto-trad|
|[equipment-02-BANLXq8FhwqsDu0v.htm](equipment/equipment-02-BANLXq8FhwqsDu0v.htm)|Wondrous Figurine (Onyx Dog)|auto-trad|
|[equipment-02-BJxnnY9ap2wMDnRN.htm](equipment/equipment-02-BJxnnY9ap2wMDnRN.htm)|Bottomless Stein|auto-trad|
|[equipment-02-c3dFpmXsIxH2i8ug.htm](equipment/equipment-02-c3dFpmXsIxH2i8ug.htm)|Standard Book of Translation (Tien)|auto-trad|
|[equipment-02-CF2BYEQnAmSwmPPE.htm](equipment/equipment-02-CF2BYEQnAmSwmPPE.htm)|Bewitching Bloom (Lilac)|auto-trad|
|[equipment-02-DKWuJb2rSgiotOG7.htm](equipment/equipment-02-DKWuJb2rSgiotOG7.htm)|Weapon Potency (+1)|auto-trad|
|[equipment-02-e011UgQLOJtdUqoW.htm](equipment/equipment-02-e011UgQLOJtdUqoW.htm)|Paper Shredder|auto-trad|
|[equipment-02-e6lhw1SPdq3koAqG.htm](equipment/equipment-02-e6lhw1SPdq3koAqG.htm)|Gelid Shard|auto-trad|
|[equipment-02-EuEH4f3oDUQ4YFZS.htm](equipment/equipment-02-EuEH4f3oDUQ4YFZS.htm)|Stone of Weight|auto-trad|
|[equipment-02-eYemXoYEObmaeOAE.htm](equipment/equipment-02-eYemXoYEObmaeOAE.htm)|Apparition Gloves|auto-trad|
|[equipment-02-fvpLYx1Lo42cdleQ.htm](equipment/equipment-02-fvpLYx1Lo42cdleQ.htm)|Hat of Disguise|auto-trad|
|[equipment-02-gbwr57aT9ou8yKWT.htm](equipment/equipment-02-gbwr57aT9ou8yKWT.htm)|Wayfinder|auto-trad|
|[equipment-02-gdaLHiWRhB1l2Xr3.htm](equipment/equipment-02-gdaLHiWRhB1l2Xr3.htm)|Alchemist's Flamethrower|auto-trad|
|[equipment-02-hHOi8jV85CN8WRJT.htm](equipment/equipment-02-hHOi8jV85CN8WRJT.htm)|Parade Armor|auto-trad|
|[equipment-02-hyMbJb2MRF5beIaU.htm](equipment/equipment-02-hyMbJb2MRF5beIaU.htm)|Archaic Wayfinder|auto-trad|
|[equipment-02-ocqIWF5PeU9BxNlM.htm](equipment/equipment-02-ocqIWF5PeU9BxNlM.htm)|Experimental Clothing|auto-trad|
|[equipment-02-OdBdmv0Y1P7wQWTo.htm](equipment/equipment-02-OdBdmv0Y1P7wQWTo.htm)|Holy Steam Ball|auto-trad|
|[equipment-02-ONwLm7P7ICUtVN9V.htm](equipment/equipment-02-ONwLm7P7ICUtVN9V.htm)|Pocket Watch|auto-trad|
|[equipment-02-ozOVlLALtDChFJwc.htm](equipment/equipment-02-ozOVlLALtDChFJwc.htm)|Deployable Cover (Ballistic Cover)|auto-trad|
|[equipment-02-pcGdJvwun0tjrUTz.htm](equipment/equipment-02-pcGdJvwun0tjrUTz.htm)|Fanged|auto-trad|
|[equipment-02-pHeqYb5gTSUSmW0j.htm](equipment/equipment-02-pHeqYb5gTSUSmW0j.htm)|Dweomerweave Robe|auto-trad|
|[equipment-02-PSKFnWMq5daOJa1R.htm](equipment/equipment-02-PSKFnWMq5daOJa1R.htm)|Ancestral Geometry|auto-trad|
|[equipment-02-QgSPvZupjCsWhqAp.htm](equipment/equipment-02-QgSPvZupjCsWhqAp.htm)|Ursine Avenger Hood|auto-trad|
|[equipment-02-QOAjwtH9gycbDZCY.htm](equipment/equipment-02-QOAjwtH9gycbDZCY.htm)|Jack's Tattered Cape|auto-trad|
|[equipment-02-r1hgg2rweqGL1LBl.htm](equipment/equipment-02-r1hgg2rweqGL1LBl.htm)|Hand of the Mage|auto-trad|
|[equipment-02-SHhSOqzt89GRk9kL.htm](equipment/equipment-02-SHhSOqzt89GRk9kL.htm)|Toy Carriage (Windup)|auto-trad|
|[equipment-02-T99WI1hocCXE7RI0.htm](equipment/equipment-02-T99WI1hocCXE7RI0.htm)|Doll (Exquisite surprise)|auto-trad|
|[equipment-02-teSZWABjhXM4EKXM.htm](equipment/equipment-02-teSZWABjhXM4EKXM.htm)|Quick Wig|auto-trad|
|[equipment-02-tlPwOqfjJxQQsgoc.htm](equipment/equipment-02-tlPwOqfjJxQQsgoc.htm)|Frostwalker Pattern|auto-trad|
|[equipment-02-TmQalYKNNRuEdoTh.htm](equipment/equipment-02-TmQalYKNNRuEdoTh.htm)|Brooch of Shielding|auto-trad|
|[equipment-02-u4mB03FhvuJ7DcX8.htm](equipment/equipment-02-u4mB03FhvuJ7DcX8.htm)|St. Alkitarem's Eye|auto-trad|
|[equipment-02-VpoBYfVUEA8wtQAb.htm](equipment/equipment-02-VpoBYfVUEA8wtQAb.htm)|Flask of Fellowship|auto-trad|
|[equipment-02-xRQpaV9fCM7ptmVB.htm](equipment/equipment-02-xRQpaV9fCM7ptmVB.htm)|Luckless Dice|auto-trad|
|[equipment-02-xyzmQa3nhU8HxfUL.htm](equipment/equipment-02-xyzmQa3nhU8HxfUL.htm)|Tent (Pavilion)|auto-trad|
|[equipment-02-YmTGzsenhogSNDXK.htm](equipment/equipment-02-YmTGzsenhogSNDXK.htm)|Pyrite Rat|auto-trad|
|[equipment-02-YZxq8rGPiLPIKWIQ.htm](equipment/equipment-02-YZxq8rGPiLPIKWIQ.htm)|Writ of Authenticity|auto-trad|
|[equipment-02-ZIs2lAIDasxshMCf.htm](equipment/equipment-02-ZIs2lAIDasxshMCf.htm)|Triangular Teeth|auto-trad|
|[equipment-02-zSILrZ6pYWbWUm2D.htm](equipment/equipment-02-zSILrZ6pYWbWUm2D.htm)|Gunner's Saddle|auto-trad|
|[equipment-02-ZZnh7BkwApPyNyqc.htm](equipment/equipment-02-ZZnh7BkwApPyNyqc.htm)|Periscope|auto-trad|
|[equipment-03-0gxqSZhYdOeJhPrp.htm](equipment/equipment-03-0gxqSZhYdOeJhPrp.htm)|Jolt Coil|auto-trad|
|[equipment-03-0yLfPmzRtBd6Avac.htm](equipment/equipment-03-0yLfPmzRtBd6Avac.htm)|Corpse Compass|auto-trad|
|[equipment-03-15nkBxrvIrbGQCdS.htm](equipment/equipment-03-15nkBxrvIrbGQCdS.htm)|Clockwork Box Packer|auto-trad|
|[equipment-03-1M4SSDuTv2SwBltC.htm](equipment/equipment-03-1M4SSDuTv2SwBltC.htm)|Survey Map (Atlas)|auto-trad|
|[equipment-03-1r6StS0irdvi5JHY.htm](equipment/equipment-03-1r6StS0irdvi5JHY.htm)|Ventriloquist's Ring|auto-trad|
|[equipment-03-1tqyghMGI6PEYFGK.htm](equipment/equipment-03-1tqyghMGI6PEYFGK.htm)|Dawnlight|auto-trad|
|[equipment-03-1uwONfUHwHjRJp6o.htm](equipment/equipment-03-1uwONfUHwHjRJp6o.htm)|Puzzle Box (Challenging) (Hollow)|auto-trad|
|[equipment-03-2EOeljZiUdNVf8s2.htm](equipment/equipment-03-2EOeljZiUdNVf8s2.htm)|Bullhook|auto-trad|
|[equipment-03-2KnSYPFGKz4MDeiW.htm](equipment/equipment-03-2KnSYPFGKz4MDeiW.htm)|Tanglefoot Extruder|auto-trad|
|[equipment-03-2NKC67OBIukM4tC5.htm](equipment/equipment-03-2NKC67OBIukM4tC5.htm)|Wig of Holding|auto-trad|
|[equipment-03-3fHdvyWq5A9jvT9W.htm](equipment/equipment-03-3fHdvyWq5A9jvT9W.htm)|Concealed Holster|auto-trad|
|[equipment-03-3ld14dsn2RLu9owg.htm](equipment/equipment-03-3ld14dsn2RLu9owg.htm)|Musical Instrument (Virtuoso handheld)|auto-trad|
|[equipment-03-3vxoffA4slKHXtj2.htm](equipment/equipment-03-3vxoffA4slKHXtj2.htm)|Channel Protection Amulet|auto-trad|
|[equipment-03-4A8SFipG78SMWQEU.htm](equipment/equipment-03-4A8SFipG78SMWQEU.htm)|Aeon Stone (Pearly White Spindle)|auto-trad|
|[equipment-03-4kz3vhkKPUuXBpxk.htm](equipment/equipment-03-4kz3vhkKPUuXBpxk.htm)|Crowbar (Levered)|auto-trad|
|[equipment-03-4OqBL6zYcGU7ZsYw.htm](equipment/equipment-03-4OqBL6zYcGU7ZsYw.htm)|Snagging|auto-trad|
|[equipment-03-5GbC7RTgyAeaOcAI.htm](equipment/equipment-03-5GbC7RTgyAeaOcAI.htm)|Magnifying Glass|auto-trad|
|[equipment-03-5KHp3903LnOcHp8k.htm](equipment/equipment-03-5KHp3903LnOcHp8k.htm)|Hoax-Hunter's Kit|auto-trad|
|[equipment-03-5rkGXSbaqBbY4MiR.htm](equipment/equipment-03-5rkGXSbaqBbY4MiR.htm)|Scholarly Journal Compendium|auto-trad|
|[equipment-03-7vwcuBIe4BNS5uuE.htm](equipment/equipment-03-7vwcuBIe4BNS5uuE.htm)|Kin-Warding|auto-trad|
|[equipment-03-8kv4fmpE1geu4bFx.htm](equipment/equipment-03-8kv4fmpE1geu4bFx.htm)|Battle Medic's Baton|auto-trad|
|[equipment-03-9k7X59AB8yDlIJ4s.htm](equipment/equipment-03-9k7X59AB8yDlIJ4s.htm)|Wardrobe Stone (Lesser)|auto-trad|
|[equipment-03-ACa9QlFqdmW4s2Th.htm](equipment/equipment-03-ACa9QlFqdmW4s2Th.htm)|Hag Eye|auto-trad|
|[equipment-03-aDsdYMPpVc8hOnM5.htm](equipment/equipment-03-aDsdYMPpVc8hOnM5.htm)|Repair Kit (Superb)|auto-trad|
|[equipment-03-bAfyWCvgsYDyw3ff.htm](equipment/equipment-03-bAfyWCvgsYDyw3ff.htm)|Maestro's Instrument (Lesser)|auto-trad|
|[equipment-03-bbSc1VU1LiQqReKd.htm](equipment/equipment-03-bbSc1VU1LiQqReKd.htm)|Pathfinder Chronicle|auto-trad|
|[equipment-03-BhSQej6dIZh6wkWC.htm](equipment/equipment-03-BhSQej6dIZh6wkWC.htm)|Varisian Emblem (Vangloris)|auto-trad|
|[equipment-03-BKdzb8hu3kZtKH3Z.htm](equipment/equipment-03-BKdzb8hu3kZtKH3Z.htm)|Bracelet of Dashing|auto-trad|
|[equipment-03-bw2RNhvtX1vvHi0y.htm](equipment/equipment-03-bw2RNhvtX1vvHi0y.htm)|Varisian Emblem (Ragario)|auto-trad|
|[equipment-03-BznmPaRjI4Orb0IH.htm](equipment/equipment-03-BznmPaRjI4Orb0IH.htm)|Thieves' Tools (Infiltrator Picks)|auto-trad|
|[equipment-03-c9GaJKCxxqXKZokZ.htm](equipment/equipment-03-c9GaJKCxxqXKZokZ.htm)|Broken Tusk Pendant|auto-trad|
|[equipment-03-CDuN9kzyxBZ4cShq.htm](equipment/equipment-03-CDuN9kzyxBZ4cShq.htm)|Golden Legion Epaulet|auto-trad|
|[equipment-03-dmsdzOxdykeWXUHr.htm](equipment/equipment-03-dmsdzOxdykeWXUHr.htm)|Rime Crystal|auto-trad|
|[equipment-03-duiTS3Y9JJUsJByq.htm](equipment/equipment-03-duiTS3Y9JJUsJByq.htm)|Bewitching Bloom (Cherry Blossom)|auto-trad|
|[equipment-03-DwMXEqy7Ws8NYQQh.htm](equipment/equipment-03-DwMXEqy7Ws8NYQQh.htm)|Doubling Rings|auto-trad|
|[equipment-03-Epc1e1Q9M9bcwOR0.htm](equipment/equipment-03-Epc1e1Q9M9bcwOR0.htm)|Dancing Scarf|auto-trad|
|[equipment-03-eqnm5vYNmDl37V5S.htm](equipment/equipment-03-eqnm5vYNmDl37V5S.htm)|Soft-Landing|auto-trad|
|[equipment-03-eurAnvH8bK0ZctOR.htm](equipment/equipment-03-eurAnvH8bK0ZctOR.htm)|Bracers of Missile Deflection|auto-trad|
|[equipment-03-f6xV2LRekhoYJVwD.htm](equipment/equipment-03-f6xV2LRekhoYJVwD.htm)|Familiar Tattoo|auto-trad|
|[equipment-03-FkII0xws6an5vFSS.htm](equipment/equipment-03-FkII0xws6an5vFSS.htm)|Flaming Star|auto-trad|
|[equipment-03-Fq61XZxfgsn4ZDXf.htm](equipment/equipment-03-Fq61XZxfgsn4ZDXf.htm)|Manacles of Persuasion|auto-trad|
|[equipment-03-fr2K2wJnSLD0ERpo.htm](equipment/equipment-03-fr2K2wJnSLD0ERpo.htm)|Unbreakable Heart|auto-trad|
|[equipment-03-G4cmHHhAChAlypFN.htm](equipment/equipment-03-G4cmHHhAChAlypFN.htm)|Rope of Climbing (Lesser)|auto-trad|
|[equipment-03-gDfBzU6umebo5RXP.htm](equipment/equipment-03-gDfBzU6umebo5RXP.htm)|Palm Crossbow|auto-trad|
|[equipment-03-GGsECHQJ3RrnWXhV.htm](equipment/equipment-03-GGsECHQJ3RrnWXhV.htm)|Sisterstone Chunk|auto-trad|
|[equipment-03-Gh1FVqiP7uaSwCYz.htm](equipment/equipment-03-Gh1FVqiP7uaSwCYz.htm)|Preserving|auto-trad|
|[equipment-03-gScuJzOR6B0D5sHV.htm](equipment/equipment-03-gScuJzOR6B0D5sHV.htm)|Secret-Keeper's Mask (Reaper of Reputation)|auto-trad|
|[equipment-03-gSf5aYA9D8MSolc9.htm](equipment/equipment-03-gSf5aYA9D8MSolc9.htm)|Confabulator|auto-trad|
|[equipment-03-hywANJCzT7hMgWna.htm](equipment/equipment-03-hywANJCzT7hMgWna.htm)|Scholarly Journal|auto-trad|
|[equipment-03-IRqbsE8MgGLTfHLz.htm](equipment/equipment-03-IRqbsE8MgGLTfHLz.htm)|Disguise Kit (Elite Cosmetics)|auto-trad|
|[equipment-03-ItLZL9Bd6xwgfeB8.htm](equipment/equipment-03-ItLZL9Bd6xwgfeB8.htm)|Detective's Kit|auto-trad|
|[equipment-03-JhSCwXhC3tLhI347.htm](equipment/equipment-03-JhSCwXhC3tLhI347.htm)|Varisian Emblem (Carnasia)|auto-trad|
|[equipment-03-JY8X4RSfg6xIqAC9.htm](equipment/equipment-03-JY8X4RSfg6xIqAC9.htm)|Crushing|auto-trad|
|[equipment-03-k0AC0UxLKO4r2rwQ.htm](equipment/equipment-03-k0AC0UxLKO4r2rwQ.htm)|Presentable|auto-trad|
|[equipment-03-KeespXczBEt1ci26.htm](equipment/equipment-03-KeespXczBEt1ci26.htm)|Lady's Chalice|auto-trad|
|[equipment-03-KSkIKaKM3n75BpUL.htm](equipment/equipment-03-KSkIKaKM3n75BpUL.htm)|Gaffe Glasses|auto-trad|
|[equipment-03-ksorBjuXO0Bvdmhl.htm](equipment/equipment-03-ksorBjuXO0Bvdmhl.htm)|Fishing Tackle (Professional)|auto-trad|
|[equipment-03-laU7xnX42wXch2Dv.htm](equipment/equipment-03-laU7xnX42wXch2Dv.htm)|Concealed Sheath|auto-trad|
|[equipment-03-liGtU10aUlVeI6IC.htm](equipment/equipment-03-liGtU10aUlVeI6IC.htm)|Menacing|auto-trad|
|[equipment-03-Ln0gcJPFHU9sQ7Jd.htm](equipment/equipment-03-Ln0gcJPFHU9sQ7Jd.htm)|Diving Suit|auto-trad|
|[equipment-03-lN3Fn9AuNcKbXucJ.htm](equipment/equipment-03-lN3Fn9AuNcKbXucJ.htm)|Trinity Geode|auto-trad|
|[equipment-03-lu42Up1309MqFya4.htm](equipment/equipment-03-lu42Up1309MqFya4.htm)|Pickled Demon Tongue|auto-trad|
|[equipment-03-lWeADBqMFwFlVIuV.htm](equipment/equipment-03-lWeADBqMFwFlVIuV.htm)|Manacles (Average)|auto-trad|
|[equipment-03-maHPPEwKK2NxbMoV.htm](equipment/equipment-03-maHPPEwKK2NxbMoV.htm)|Raven Band|auto-trad|
|[equipment-03-mcKF8McrMlp01wUP.htm](equipment/equipment-03-mcKF8McrMlp01wUP.htm)|Wrenchgear|auto-trad|
|[equipment-03-MKupH1T018JubYJW.htm](equipment/equipment-03-MKupH1T018JubYJW.htm)|Persona Mask|auto-trad|
|[equipment-03-mvMeloQxSiEGIlhL.htm](equipment/equipment-03-mvMeloQxSiEGIlhL.htm)|Coyote Cloak|auto-trad|
|[equipment-03-naxISKMwhLQ5F9yS.htm](equipment/equipment-03-naxISKMwhLQ5F9yS.htm)|Puzzle Box (Challenging)|auto-trad|
|[equipment-03-nI4JorlalCpzsJ07.htm](equipment/equipment-03-nI4JorlalCpzsJ07.htm)|Sisterstone Ingot|auto-trad|
|[equipment-03-njHnUB4bwW26slzD.htm](equipment/equipment-03-njHnUB4bwW26slzD.htm)|Electrocable|auto-trad|
|[equipment-03-NJRACLvg6ULRMtZB.htm](equipment/equipment-03-NJRACLvg6ULRMtZB.htm)|Varisian Emblem (Voratalo)|auto-trad|
|[equipment-03-OjOpj9u1EdEUhKuX.htm](equipment/equipment-03-OjOpj9u1EdEUhKuX.htm)|Grim Sandglass|auto-trad|
|[equipment-03-OoDp1T4IUUsNnrV6.htm](equipment/equipment-03-OoDp1T4IUUsNnrV6.htm)|Perfect Droplet|auto-trad|
|[equipment-03-Ouou5j07pm2MysXI.htm](equipment/equipment-03-Ouou5j07pm2MysXI.htm)|Glasses of Sociability|auto-trad|
|[equipment-03-P2DjThbSOWwra49r.htm](equipment/equipment-03-P2DjThbSOWwra49r.htm)|Snare Kit (Specialist)|auto-trad|
|[equipment-03-P8hf8WT6oRxYmfLB.htm](equipment/equipment-03-P8hf8WT6oRxYmfLB.htm)|Blazons of Shared Power|auto-trad|
|[equipment-03-PCQtpTKiGfSSRwNV.htm](equipment/equipment-03-PCQtpTKiGfSSRwNV.htm)|Vestige Lenses|auto-trad|
|[equipment-03-pkD8SLooxP55tibr.htm](equipment/equipment-03-pkD8SLooxP55tibr.htm)|Codebreaker's Parchment|auto-trad|
|[equipment-03-PO0n5F4j8Pa0LB0Q.htm](equipment/equipment-03-PO0n5F4j8Pa0LB0Q.htm)|Portable Altar|auto-trad|
|[equipment-03-q2TYVAMaK6UfenbV.htm](equipment/equipment-03-q2TYVAMaK6UfenbV.htm)|Lock (Average)|auto-trad|
|[equipment-03-QHc7AnKoMpcqsI2d.htm](equipment/equipment-03-QHc7AnKoMpcqsI2d.htm)|Called|auto-trad|
|[equipment-03-QlFJyxBTYFSN2EA7.htm](equipment/equipment-03-QlFJyxBTYFSN2EA7.htm)|Fingerprint Kit|auto-trad|
|[equipment-03-qlunQzfnzPQpMG6U.htm](equipment/equipment-03-qlunQzfnzPQpMG6U.htm)|Returning|auto-trad|
|[equipment-03-qNMOpzpeQz51vMhf.htm](equipment/equipment-03-qNMOpzpeQz51vMhf.htm)|Varisian Emblem (Idolis)|auto-trad|
|[equipment-03-QSeDIyIr2A7PNy6u.htm](equipment/equipment-03-QSeDIyIr2A7PNy6u.htm)|Coin of Comfort|auto-trad|
|[equipment-03-QuPnoGmlgYVNXeTz.htm](equipment/equipment-03-QuPnoGmlgYVNXeTz.htm)|Extendable Tail|auto-trad|
|[equipment-03-R09AZzvyNA3Jginm.htm](equipment/equipment-03-R09AZzvyNA3Jginm.htm)|Abadar's Flawless Scale|auto-trad|
|[equipment-03-RF12ziGVG9YKGaqU.htm](equipment/equipment-03-RF12ziGVG9YKGaqU.htm)|Secret-Keeper's Mask (Blackfingers)|auto-trad|
|[equipment-03-RgNBGpBc9G2yw1C2.htm](equipment/equipment-03-RgNBGpBc9G2yw1C2.htm)|Crafter's Eyepiece|auto-trad|
|[equipment-03-rtfpj9WJHMnMtfuG.htm](equipment/equipment-03-rtfpj9WJHMnMtfuG.htm)|Polished Demon Horn|auto-trad|
|[equipment-03-RxBVbQEd4DArkpKl.htm](equipment/equipment-03-RxBVbQEd4DArkpKl.htm)|Cloak of Gnawing Leaves|auto-trad|
|[equipment-03-rXZxM7SbqEnvXyal.htm](equipment/equipment-03-rXZxM7SbqEnvXyal.htm)|Clothing (High-Fashion Fine)|auto-trad|
|[equipment-03-sG5JfXmH9wqmqW3w.htm](equipment/equipment-03-sG5JfXmH9wqmqW3w.htm)|Enhanced Hearing Aids|auto-trad|
|[equipment-03-spqcRLBsMOC9WTcd.htm](equipment/equipment-03-spqcRLBsMOC9WTcd.htm)|Keymaking Tools|auto-trad|
|[equipment-03-SswqJqeAWGtX3tTF.htm](equipment/equipment-03-SswqJqeAWGtX3tTF.htm)|Hat of the Magi|auto-trad|
|[equipment-03-td9LuqG8jhQQloG7.htm](equipment/equipment-03-td9LuqG8jhQQloG7.htm)|Sluggish Bracelet|auto-trad|
|[equipment-03-tEXUCp02ylyoJoyP.htm](equipment/equipment-03-tEXUCp02ylyoJoyP.htm)|Authorized|auto-trad|
|[equipment-03-TKr8IL5F3cxsfHPH.htm](equipment/equipment-03-TKr8IL5F3cxsfHPH.htm)|Aeon Stone (Dusty Rose Prism)|auto-trad|
|[equipment-03-tqgCxayEMsUxb1PV.htm](equipment/equipment-03-tqgCxayEMsUxb1PV.htm)|Enveloping Light|auto-trad|
|[equipment-03-tUAfKD2iIrQxHYWp.htm](equipment/equipment-03-tUAfKD2iIrQxHYWp.htm)|Ring of Torag|auto-trad|
|[equipment-03-TWv2EeDJaCjtmGUN.htm](equipment/equipment-03-TWv2EeDJaCjtmGUN.htm)|Climbing Kit (Extreme)|auto-trad|
|[equipment-03-u6BZFzBYUnCmfnRr.htm](equipment/equipment-03-u6BZFzBYUnCmfnRr.htm)|Armory Bracelet (Minor)|auto-trad|
|[equipment-03-U7UVGM93EaNc6lzY.htm](equipment/equipment-03-U7UVGM93EaNc6lzY.htm)|Stalk Goggles (Greater)|auto-trad|
|[equipment-03-Uc88bGfysDAeaAD8.htm](equipment/equipment-03-Uc88bGfysDAeaAD8.htm)|Backfire Mantle|auto-trad|
|[equipment-03-UhcRWtnjU2WLSClx.htm](equipment/equipment-03-UhcRWtnjU2WLSClx.htm)|Survey Map|auto-trad|
|[equipment-03-uhka9LHEP3wDKytG.htm](equipment/equipment-03-uhka9LHEP3wDKytG.htm)|Alchemist's Lab (Expanded)|auto-trad|
|[equipment-03-uTELMvkoRORmr5pm.htm](equipment/equipment-03-uTELMvkoRORmr5pm.htm)|Magnifying Scope|auto-trad|
|[equipment-03-uU8bqDR4oWITWZNX.htm](equipment/equipment-03-uU8bqDR4oWITWZNX.htm)|Varisian Emblem (Avidais)|auto-trad|
|[equipment-03-UXsHBUyx4b0dIPDf.htm](equipment/equipment-03-UXsHBUyx4b0dIPDf.htm)|Camouflage Suit|auto-trad|
|[equipment-03-vDQSR3fgfYqLp0aB.htm](equipment/equipment-03-vDQSR3fgfYqLp0aB.htm)|Wind-up Wings (Flutterback)|auto-trad|
|[equipment-03-Vr2h3E86Vaf5DJpZ.htm](equipment/equipment-03-Vr2h3E86Vaf5DJpZ.htm)|Portable Ram|auto-trad|
|[equipment-03-wBeljBAAFsvgkRMy.htm](equipment/equipment-03-wBeljBAAFsvgkRMy.htm)|Handcuffs (Average)|auto-trad|
|[equipment-03-WgXPnTogb3MCueKP.htm](equipment/equipment-03-WgXPnTogb3MCueKP.htm)|Heckling Tools|auto-trad|
|[equipment-03-xaWuuQoBJiMLzggR.htm](equipment/equipment-03-xaWuuQoBJiMLzggR.htm)|Thurible of Revelation (Lesser)|auto-trad|
|[equipment-03-XFUsIDb6RjVFz9Ce.htm](equipment/equipment-03-XFUsIDb6RjVFz9Ce.htm)|One Hundred Victories|auto-trad|
|[equipment-03-XLGJ1bLhMtP1jc2w.htm](equipment/equipment-03-XLGJ1bLhMtP1jc2w.htm)|Fashionable Wayfinder|auto-trad|
|[equipment-03-XndR8hsJyCsVfZOi.htm](equipment/equipment-03-XndR8hsJyCsVfZOi.htm)|Smoked Goggles|auto-trad|
|[equipment-03-xVhd8NF9KQ6VWfMu.htm](equipment/equipment-03-xVhd8NF9KQ6VWfMu.htm)|Compass (Lensatic)|auto-trad|
|[equipment-03-y0cRr28w57VapTcn.htm](equipment/equipment-03-y0cRr28w57VapTcn.htm)|Devil's Luck|auto-trad|
|[equipment-03-YhkomhtKBK3i9C7Q.htm](equipment/equipment-03-YhkomhtKBK3i9C7Q.htm)|Disguise Kit (Elite)|auto-trad|
|[equipment-03-yw1kPxsdCoDUzOaE.htm](equipment/equipment-03-yw1kPxsdCoDUzOaE.htm)|Musical Instrument (Virtuoso heavy)|auto-trad|
|[equipment-03-Z5eDsW2Lka2cLTVR.htm](equipment/equipment-03-Z5eDsW2Lka2cLTVR.htm)|Mirror Robe|auto-trad|
|[equipment-03-ZEqAx8jEc6zhX3V1.htm](equipment/equipment-03-ZEqAx8jEc6zhX3V1.htm)|Tracker's Goggles|auto-trad|
|[equipment-03-ZIt62AQb997FatRw.htm](equipment/equipment-03-ZIt62AQb997FatRw.htm)|Knight's Tabard|auto-trad|
|[equipment-03-zPhqmCvWyHO8i9ws.htm](equipment/equipment-03-zPhqmCvWyHO8i9ws.htm)|Pendant of the Occult|auto-trad|
|[equipment-03-zTR7xbJGFm3oppFA.htm](equipment/equipment-03-zTR7xbJGFm3oppFA.htm)|Varisian Emblem (Avaria)|auto-trad|
|[equipment-03-zVXDnx8i0KLf6fkF.htm](equipment/equipment-03-zVXDnx8i0KLf6fkF.htm)|Timepiece (Grand Clock)|auto-trad|
|[equipment-04-2gYZiUw9yjtb0yJY.htm](equipment/equipment-04-2gYZiUw9yjtb0yJY.htm)|Demon Mask|auto-trad|
|[equipment-04-41MNmuj0PbHnhz0M.htm](equipment/equipment-04-41MNmuj0PbHnhz0M.htm)|Rhythm Bone (Greater)|auto-trad|
|[equipment-04-4ObrlZ5GhCPA2E2s.htm](equipment/equipment-04-4ObrlZ5GhCPA2E2s.htm)|Hunter's Brooch|auto-trad|
|[equipment-04-5p4ORuCLOKePxUUR.htm](equipment/equipment-04-5p4ORuCLOKePxUUR.htm)|Five-Feather Wreath|auto-trad|
|[equipment-04-5tyQQUdb2GtspEvy.htm](equipment/equipment-04-5tyQQUdb2GtspEvy.htm)|Draxie's Recipe Book|auto-trad|
|[equipment-04-7JVgLiNTAs4clEW8.htm](equipment/equipment-04-7JVgLiNTAs4clEW8.htm)|Alchemist Goggles|auto-trad|
|[equipment-04-7tiAVxbZW8aGmKUO.htm](equipment/equipment-04-7tiAVxbZW8aGmKUO.htm)|Gyroscopic Stabilizer|auto-trad|
|[equipment-04-7ZfWiHqDyb6NllN1.htm](equipment/equipment-04-7ZfWiHqDyb6NllN1.htm)|Deck of Mischief|auto-trad|
|[equipment-04-8qR43lT8rpmlkoKs.htm](equipment/equipment-04-8qR43lT8rpmlkoKs.htm)|Oracular Hag Eye|auto-trad|
|[equipment-04-9MT4uBht72VerwfR.htm](equipment/equipment-04-9MT4uBht72VerwfR.htm)|Extraction Cauldron|auto-trad|
|[equipment-04-9zdm3EyEQXgMox8b.htm](equipment/equipment-04-9zdm3EyEQXgMox8b.htm)|Divine Scroll Case of Simplicity|auto-trad|
|[equipment-04-AhvjU4QbinWPM9t3.htm](equipment/equipment-04-AhvjU4QbinWPM9t3.htm)|Belt of Good Health|auto-trad|
|[equipment-04-AL46eDfKdAnXKQPV.htm](equipment/equipment-04-AL46eDfKdAnXKQPV.htm)|Wand Cane|auto-trad|
|[equipment-04-bCwSeD0Ub3f4BfjO.htm](equipment/equipment-04-bCwSeD0Ub3f4BfjO.htm)|Dragon's Breath (1st Level Spell)|auto-trad|
|[equipment-04-BZImrMVyqeZ0RfF8.htm](equipment/equipment-04-BZImrMVyqeZ0RfF8.htm)|Bewitching Bloom (Red Rose)|auto-trad|
|[equipment-04-cKYWot4ifTOa6ans.htm](equipment/equipment-04-cKYWot4ifTOa6ans.htm)|Alchemical Chart (Lesser)|auto-trad|
|[equipment-04-cyw2OgL4XJ9HOu0b.htm](equipment/equipment-04-cyw2OgL4XJ9HOu0b.htm)|Wand of Reaching (1st-level)|auto-trad|
|[equipment-04-d0ponRhw0JhSM4iH.htm](equipment/equipment-04-d0ponRhw0JhSM4iH.htm)|Aeon Stone (Clear Quartz Octagon)|auto-trad|
|[equipment-04-dsxATkJ2RwvYF3vm.htm](equipment/equipment-04-dsxATkJ2RwvYF3vm.htm)|Drums of War|auto-trad|
|[equipment-04-DxCuJKynlnMQZHgp.htm](equipment/equipment-04-DxCuJKynlnMQZHgp.htm)|Striking|auto-trad|
|[equipment-04-EGYcFO9eYfajGKEf.htm](equipment/equipment-04-EGYcFO9eYfajGKEf.htm)|Occult Scroll Case of Simplicity|auto-trad|
|[equipment-04-Ejmv9IHGp9Ad9dgu.htm](equipment/equipment-04-Ejmv9IHGp9Ad9dgu.htm)|Thieves' Tools (Concealable)|auto-trad|
|[equipment-04-eLsmwHVW5qiwcd7c.htm](equipment/equipment-04-eLsmwHVW5qiwcd7c.htm)|Seer's Flute|auto-trad|
|[equipment-04-f9vNVQ2v45mDI8Xr.htm](equipment/equipment-04-f9vNVQ2v45mDI8Xr.htm)|Aeon Stone (Azure Briolette)|auto-trad|
|[equipment-04-g2oaOGSpttfH1q6W.htm](equipment/equipment-04-g2oaOGSpttfH1q6W.htm)|Lifting Belt|auto-trad|
|[equipment-04-g9pOZsJykSihdyrL.htm](equipment/equipment-04-g9pOZsJykSihdyrL.htm)|Sealing Chest (Lesser)|auto-trad|
|[equipment-04-GMcU4kX1ldrSQMh6.htm](equipment/equipment-04-GMcU4kX1ldrSQMh6.htm)|Vengeful Arm|auto-trad|
|[equipment-04-GuVgH50cUeytM68r.htm](equipment/equipment-04-GuVgH50cUeytM68r.htm)|Stargazer's Spyglass|auto-trad|
|[equipment-04-gvtFrrvCSdsCOlOj.htm](equipment/equipment-04-gvtFrrvCSdsCOlOj.htm)|Air Cartridge Firing System|auto-trad|
|[equipment-04-hnbbqvzYDyhDiJnf.htm](equipment/equipment-04-hnbbqvzYDyhDiJnf.htm)|Bane|auto-trad|
|[equipment-04-IAJcB9cGEKl6HyGE.htm](equipment/equipment-04-IAJcB9cGEKl6HyGE.htm)|Eye Slash (Greater)|auto-trad|
|[equipment-04-JlI0oubjxL9WOt4p.htm](equipment/equipment-04-JlI0oubjxL9WOt4p.htm)|Wand of the Pampered Pet|auto-trad|
|[equipment-04-jOulX8kWlCF5Dveg.htm](equipment/equipment-04-jOulX8kWlCF5Dveg.htm)|Jug of Fond Remembrance|auto-trad|
|[equipment-04-Jpgl3HlEd8u20fUY.htm](equipment/equipment-04-Jpgl3HlEd8u20fUY.htm)|Entertainer's Lute|auto-trad|
|[equipment-04-JQdwHECogcTzdd8R.htm](equipment/equipment-04-JQdwHECogcTzdd8R.htm)|Ghost Touch|auto-trad|
|[equipment-04-kBCdBFb9IWlwy81B.htm](equipment/equipment-04-kBCdBFb9IWlwy81B.htm)|Wand of Pernicious Poison (Spider Sting)|auto-trad|
|[equipment-04-kILYXCczAc6ZfArJ.htm](equipment/equipment-04-kILYXCczAc6ZfArJ.htm)|Trickster's Mandolin|auto-trad|
|[equipment-04-LhpRuLfxuR3t819V.htm](equipment/equipment-04-LhpRuLfxuR3t819V.htm)|Bewitching Bloom (White Poppy)|auto-trad|
|[equipment-04-LmIpYZ1lS2UDGXvU.htm](equipment/equipment-04-LmIpYZ1lS2UDGXvU.htm)|Mortar of Hidden Meaning|auto-trad|
|[equipment-04-LSbv4rdvKfm4bPrR.htm](equipment/equipment-04-LSbv4rdvKfm4bPrR.htm)|Dread Helm|auto-trad|
|[equipment-04-mqOjTuUHv4cRdt2N.htm](equipment/equipment-04-mqOjTuUHv4cRdt2N.htm)|Primal Scroll Case of Simplicity|auto-trad|
|[equipment-04-nNinADW4vVC0roP2.htm](equipment/equipment-04-nNinADW4vVC0roP2.htm)|Pickpocket's Tailoring|auto-trad|
|[equipment-04-o1zKhvYUUc1hE2AE.htm](equipment/equipment-04-o1zKhvYUUc1hE2AE.htm)|Healer's Gloves|auto-trad|
|[equipment-04-oC4ZMEdBJ3ia4ALm.htm](equipment/equipment-04-oC4ZMEdBJ3ia4ALm.htm)|Cloak of Repute|auto-trad|
|[equipment-04-oIMtXP8y7oxvrBVP.htm](equipment/equipment-04-oIMtXP8y7oxvrBVP.htm)|Hosteling Statuette|auto-trad|
|[equipment-04-oJLQaiCSmFJx0bxe.htm](equipment/equipment-04-oJLQaiCSmFJx0bxe.htm)|Inquisitive Quill|auto-trad|
|[equipment-04-PhjutBgR1egCuHvY.htm](equipment/equipment-04-PhjutBgR1egCuHvY.htm)|Wayfinder of Rescue|auto-trad|
|[equipment-04-PttRbxopuD0EpDTI.htm](equipment/equipment-04-PttRbxopuD0EpDTI.htm)|Frightful Hag Eye|auto-trad|
|[equipment-04-qDiVe9Ob04gRXLFa.htm](equipment/equipment-04-qDiVe9Ob04gRXLFa.htm)|Warcaller's Chime of Destruction|auto-trad|
|[equipment-04-qdXwiDaPdsFtlXGH.htm](equipment/equipment-04-qdXwiDaPdsFtlXGH.htm)|Reading Glyphs|auto-trad|
|[equipment-04-QRzpVYuATBjyBojJ.htm](equipment/equipment-04-QRzpVYuATBjyBojJ.htm)|Wand of Shrouded Step|auto-trad|
|[equipment-04-r28DjJEjF6jvCcfb.htm](equipment/equipment-04-r28DjJEjF6jvCcfb.htm)|Merciful|auto-trad|
|[equipment-04-rT93xVoebhCm9uVA.htm](equipment/equipment-04-rT93xVoebhCm9uVA.htm)|Faith Tattoo|auto-trad|
|[equipment-04-rV7MTDCseZmEZKDw.htm](equipment/equipment-04-rV7MTDCseZmEZKDw.htm)|Spyglass (Fine)|auto-trad|
|[equipment-04-teMhnTeAyWnvbo2C.htm](equipment/equipment-04-teMhnTeAyWnvbo2C.htm)|Tremorsensors|auto-trad|
|[equipment-04-TmH7coBHz9pjoDvP.htm](equipment/equipment-04-TmH7coBHz9pjoDvP.htm)|Lawbringer's Lasso|auto-trad|
|[equipment-04-tnPteWjbm97bdTj5.htm](equipment/equipment-04-tnPteWjbm97bdTj5.htm)|Talisman Cord (Lesser)|auto-trad|
|[equipment-04-TtDWzugeKjtlWfZq.htm](equipment/equipment-04-TtDWzugeKjtlWfZq.htm)|Memory Guitar|auto-trad|
|[equipment-04-TTvfgU2G5n3tpTby.htm](equipment/equipment-04-TTvfgU2G5n3tpTby.htm)|Pipes of Compulsion|auto-trad|
|[equipment-04-TxMFNfkoccirCitP.htm](equipment/equipment-04-TxMFNfkoccirCitP.htm)|Thieves' Tools (Concealable Picks)|auto-trad|
|[equipment-04-UNOHV2dnRphyNJFw.htm](equipment/equipment-04-UNOHV2dnRphyNJFw.htm)|Rhinoceros Mask|auto-trad|
|[equipment-04-uOaFBqnCYWCgyCl6.htm](equipment/equipment-04-uOaFBqnCYWCgyCl6.htm)|Wildwood Ink|auto-trad|
|[equipment-04-vSiePW2xIEOmXB0H.htm](equipment/equipment-04-vSiePW2xIEOmXB0H.htm)|Wand of Mercy (1st-level)|auto-trad|
|[equipment-04-vxBcsWCjWd6DZ0Jz.htm](equipment/equipment-04-vxBcsWCjWd6DZ0Jz.htm)|Arcane Scroll Case of Simplicity|auto-trad|
|[equipment-04-WAYGyAaH7rtpX1y4.htm](equipment/equipment-04-WAYGyAaH7rtpX1y4.htm)|Wand of Crushing Leaps|auto-trad|
|[equipment-04-WKdmAhoji9Y9RC7D.htm](equipment/equipment-04-WKdmAhoji9Y9RC7D.htm)|Marvelous Calliope|auto-trad|
|[equipment-04-xTcQPSZlZ231gPWk.htm](equipment/equipment-04-xTcQPSZlZ231gPWk.htm)|Wand of Mental Purification (1st-level)|auto-trad|
|[equipment-04-XtYQFNEDlEVOr831.htm](equipment/equipment-04-XtYQFNEDlEVOr831.htm)|Sextant of the Night|auto-trad|
|[equipment-04-y0ngvQ7ArcUuoEHT.htm](equipment/equipment-04-y0ngvQ7ArcUuoEHT.htm)|Blessed Tattoo|auto-trad|
|[equipment-04-y8nPmHXIdt4KhMQU.htm](equipment/equipment-04-y8nPmHXIdt4KhMQU.htm)|Bagpipes of Turmoil|auto-trad|
|[equipment-04-z2QXO8vl0VsXaI1E.htm](equipment/equipment-04-z2QXO8vl0VsXaI1E.htm)|Wand of Legerdemain (1st-level)|auto-trad|
|[equipment-04-z6eQdPTgSxTLR1Qr.htm](equipment/equipment-04-z6eQdPTgSxTLR1Qr.htm)|Shining Wayfinder|auto-trad|
|[equipment-04-Zw3BKaJYxxxzNZ0f.htm](equipment/equipment-04-Zw3BKaJYxxxzNZ0f.htm)|Wand of Widening (1st-Level Spell)|auto-trad|
|[equipment-05-1Md7gEeGcqs7n4iU.htm](equipment/equipment-05-1Md7gEeGcqs7n4iU.htm)|Shade Hat|auto-trad|
|[equipment-05-2EzUZq9pEVP4oQ0k.htm](equipment/equipment-05-2EzUZq9pEVP4oQ0k.htm)|Guiding Cajon Drum|auto-trad|
|[equipment-05-3RPYWoUv1VKGjJ7i.htm](equipment/equipment-05-3RPYWoUv1VKGjJ7i.htm)|Ring of Minor Arcana|auto-trad|
|[equipment-05-47FmnpSOE96SJ8H4.htm](equipment/equipment-05-47FmnpSOE96SJ8H4.htm)|Aeon Stone (Agate Ellipsoid)|auto-trad|
|[equipment-05-5AHJQn2QrvdsTJsX.htm](equipment/equipment-05-5AHJQn2QrvdsTJsX.htm)|Warcaller's Chime of Resistance|auto-trad|
|[equipment-05-6rhind8MDhtJHlwq.htm](equipment/equipment-05-6rhind8MDhtJHlwq.htm)|Wondrous Figurine (Candy Constrictor)|auto-trad|
|[equipment-05-73lxT8RrbKtoHvty.htm](equipment/equipment-05-73lxT8RrbKtoHvty.htm)|Chaos Collar|auto-trad|
|[equipment-05-7Z6cpy4rXCOaYJJH.htm](equipment/equipment-05-7Z6cpy4rXCOaYJJH.htm)|Nightbreeze Machine|auto-trad|
|[equipment-05-8iet9wptkaoJzOmc.htm](equipment/equipment-05-8iet9wptkaoJzOmc.htm)|Magnifying Glass of Elucidation|auto-trad|
|[equipment-05-8WtXK4cyYcls72Yf.htm](equipment/equipment-05-8WtXK4cyYcls72Yf.htm)|Spyglass Eye|auto-trad|
|[equipment-05-98JIeYGEuE6pPV05.htm](equipment/equipment-05-98JIeYGEuE6pPV05.htm)|Floorbell|auto-trad|
|[equipment-05-9XrsaIc80eXhKaoM.htm](equipment/equipment-05-9XrsaIc80eXhKaoM.htm)|Rope of Climbing (Moderate)|auto-trad|
|[equipment-05-a60NH7OztaEaGlU8.htm](equipment/equipment-05-a60NH7OztaEaGlU8.htm)|Wand of Continuation (1st-Level Spell)|auto-trad|
|[equipment-05-AHkmoVr1NSFytJwp.htm](equipment/equipment-05-AHkmoVr1NSFytJwp.htm)|Merchant's Guile|auto-trad|
|[equipment-05-aneZJVb3sbpG2fN7.htm](equipment/equipment-05-aneZJVb3sbpG2fN7.htm)|Snowshoes of the Long Trek|auto-trad|
|[equipment-05-aUohYRQ8lHzgblxi.htm](equipment/equipment-05-aUohYRQ8lHzgblxi.htm)|Toolkit of Bronze Whispers|auto-trad|
|[equipment-05-B4wxZ7mvBDJPWPvZ.htm](equipment/equipment-05-B4wxZ7mvBDJPWPvZ.htm)|Bomb Coagulant Alembic|auto-trad|
|[equipment-05-bhpYiOmBysl8ZEYn.htm](equipment/equipment-05-bhpYiOmBysl8ZEYn.htm)|Stalwart's Ring|auto-trad|
|[equipment-05-BNelZMBHKlPAWl9Z.htm](equipment/equipment-05-BNelZMBHKlPAWl9Z.htm)|Pocket Stage|auto-trad|
|[equipment-05-c0gjWASSeNIRNmEw.htm](equipment/equipment-05-c0gjWASSeNIRNmEw.htm)|Warrior's Training Ring|auto-trad|
|[equipment-05-ClfUJ4AtaxglVGvs.htm](equipment/equipment-05-ClfUJ4AtaxglVGvs.htm)|Fleshgem (Combat)|auto-trad|
|[equipment-05-CtAg4DpSssKBIw8R.htm](equipment/equipment-05-CtAg4DpSssKBIw8R.htm)|Replacement Filter (Level 5)|auto-trad|
|[equipment-05-Dc2DRXybi5dCxmqP.htm](equipment/equipment-05-Dc2DRXybi5dCxmqP.htm)|Knight's Maintenance Kit|auto-trad|
|[equipment-05-DmzkzKB4EBleK0DA.htm](equipment/equipment-05-DmzkzKB4EBleK0DA.htm)|Armory Bracelet (Lesser)|auto-trad|
|[equipment-05-e2hDvrdz0t0hzE5y.htm](equipment/equipment-05-e2hDvrdz0t0hzE5y.htm)|Heated Cloak|auto-trad|
|[equipment-05-eTOHAULu4kgqR1S5.htm](equipment/equipment-05-eTOHAULu4kgqR1S5.htm)|Instructions for Lasting Agony|auto-trad|
|[equipment-05-Ez0byAlwJwzRXof5.htm](equipment/equipment-05-Ez0byAlwJwzRXof5.htm)|Paired|auto-trad|
|[equipment-05-fprUZviW8khm2BLo.htm](equipment/equipment-05-fprUZviW8khm2BLo.htm)|Skeleton Key|auto-trad|
|[equipment-05-gjjF0sQMpXf3CrTF.htm](equipment/equipment-05-gjjF0sQMpXf3CrTF.htm)|Advanced Book of Translation (Tien)|auto-trad|
|[equipment-05-GkyZl0UeUAS5YaHA.htm](equipment/equipment-05-GkyZl0UeUAS5YaHA.htm)|Aether Appendage|auto-trad|
|[equipment-05-GVgA0w4CUP4lanPl.htm](equipment/equipment-05-GVgA0w4CUP4lanPl.htm)|Thunder Helm|auto-trad|
|[equipment-05-HSAj9FBx2yAyjfzf.htm](equipment/equipment-05-HSAj9FBx2yAyjfzf.htm)|Spurned Lute|auto-trad|
|[equipment-05-iFkgpzf67iapKfyD.htm](equipment/equipment-05-iFkgpzf67iapKfyD.htm)|Silent Heart|auto-trad|
|[equipment-05-iS7hAQMAaThHYE8g.htm](equipment/equipment-05-iS7hAQMAaThHYE8g.htm)|Bort's Blessing|auto-trad|
|[equipment-05-iTxqImupNnm8gvoe.htm](equipment/equipment-05-iTxqImupNnm8gvoe.htm)|Glamered|auto-trad|
|[equipment-05-IVaQrUnLCzTZIfP6.htm](equipment/equipment-05-IVaQrUnLCzTZIfP6.htm)|Spring Heel|auto-trad|
|[equipment-05-JJZgRx6naNJmDa81.htm](equipment/equipment-05-JJZgRx6naNJmDa81.htm)|Diplomat's Badge|auto-trad|
|[equipment-05-JWFR4O5V06UUvx6W.htm](equipment/equipment-05-JWFR4O5V06UUvx6W.htm)|Beastmaster's Sigil|auto-trad|
|[equipment-05-JWUTLYX1zaBntC1L.htm](equipment/equipment-05-JWUTLYX1zaBntC1L.htm)|Ring of the Tiger|auto-trad|
|[equipment-05-K7VHhUamFz3kTnm5.htm](equipment/equipment-05-K7VHhUamFz3kTnm5.htm)|Goggles of Night|auto-trad|
|[equipment-05-KekfZ6eRzoZVRemw.htm](equipment/equipment-05-KekfZ6eRzoZVRemw.htm)|Tengu Feather Fan|auto-trad|
|[equipment-05-kEy7Uc1VisizGgtf.htm](equipment/equipment-05-kEy7Uc1VisizGgtf.htm)|Shadow|auto-trad|
|[equipment-05-KnI89eneLdJeURob.htm](equipment/equipment-05-KnI89eneLdJeURob.htm)|True Name Amulet (Lesser)|auto-trad|
|[equipment-05-KPWN5tGGkvZR7K3K.htm](equipment/equipment-05-KPWN5tGGkvZR7K3K.htm)|Wand of Manifold Missiles (1st-Level Spell)|auto-trad|
|[equipment-05-KqzTdCwrfoqfSR5b.htm](equipment/equipment-05-KqzTdCwrfoqfSR5b.htm)|Tome of Restorative Cleansing (Lesser)|auto-trad|
|[equipment-05-krKFJmE139HBqa9K.htm](equipment/equipment-05-krKFJmE139HBqa9K.htm)|Darkvision Scope|auto-trad|
|[equipment-05-KrmSuQIyu6OEi5ew.htm](equipment/equipment-05-KrmSuQIyu6OEi5ew.htm)|Holy Prayer Beads|auto-trad|
|[equipment-05-Lj6diNjoD5ilz7jd.htm](equipment/equipment-05-Lj6diNjoD5ilz7jd.htm)|Boozy Bottle|auto-trad|
|[equipment-05-LwQb7ryTC8FlOXgX.htm](equipment/equipment-05-LwQb7ryTC8FlOXgX.htm)|Disrupting|auto-trad|
|[equipment-05-mdNNIF01ybgs1m3s.htm](equipment/equipment-05-mdNNIF01ybgs1m3s.htm)|War Saddle|auto-trad|
|[equipment-05-me7GsgRKnqA7vaB5.htm](equipment/equipment-05-me7GsgRKnqA7vaB5.htm)|Waverider Barding|auto-trad|
|[equipment-05-N9bhBhJ6JawzVitu.htm](equipment/equipment-05-N9bhBhJ6JawzVitu.htm)|Large Bore Modifications|auto-trad|
|[equipment-05-NJtIwMIzjdRqupAM.htm](equipment/equipment-05-NJtIwMIzjdRqupAM.htm)|Stanching|auto-trad|
|[equipment-05-NZJkdq4AezyeUvGi.htm](equipment/equipment-05-NZJkdq4AezyeUvGi.htm)|Astrolabe of the Falling Stars|auto-trad|
|[equipment-05-OClYfRHzoynib6wX.htm](equipment/equipment-05-OClYfRHzoynib6wX.htm)|Earthbinding|auto-trad|
|[equipment-05-OFo1349Pems2mboB.htm](equipment/equipment-05-OFo1349Pems2mboB.htm)|Quick Runner's Shirt|auto-trad|
|[equipment-05-oOtrCBGLqNLdiOuF.htm](equipment/equipment-05-oOtrCBGLqNLdiOuF.htm)|Wand of Contagious Frailty|auto-trad|
|[equipment-05-OX71iN2SXMWMeI5R.htm](equipment/equipment-05-OX71iN2SXMWMeI5R.htm)|Secret-Keeper's Mask (Gray Master)|auto-trad|
|[equipment-05-P6v2AtJw7AUwaDzf.htm](equipment/equipment-05-P6v2AtJw7AUwaDzf.htm)|Fearsome|auto-trad|
|[equipment-05-pASszbK2LjpNUJY1.htm](equipment/equipment-05-pASszbK2LjpNUJY1.htm)|Conundrum Spectacles|auto-trad|
|[equipment-05-PQ74afm5YWessacn.htm](equipment/equipment-05-PQ74afm5YWessacn.htm)|Bravery Baldric (Fleet)|auto-trad|
|[equipment-05-q6Wdgi5fE1zovsYh.htm](equipment/equipment-05-q6Wdgi5fE1zovsYh.htm)|Tactician's Helm|auto-trad|
|[equipment-05-QiYXHgbAv29OuaWS.htm](equipment/equipment-05-QiYXHgbAv29OuaWS.htm)|Collar of the Shifting Spider|auto-trad|
|[equipment-05-QOEvD3xh2qY7v0kh.htm](equipment/equipment-05-QOEvD3xh2qY7v0kh.htm)|Wayfinderfinder|auto-trad|
|[equipment-05-QOUjYRxXHvwMkGAw.htm](equipment/equipment-05-QOUjYRxXHvwMkGAw.htm)|Folding Drums|auto-trad|
|[equipment-05-qwHO8kRNKPqCM80x.htm](equipment/equipment-05-qwHO8kRNKPqCM80x.htm)|Spirit-Singer|auto-trad|
|[equipment-05-rcJwqJ88ubcRDFJH.htm](equipment/equipment-05-rcJwqJ88ubcRDFJH.htm)|Shrieking Key|auto-trad|
|[equipment-05-RKPAHGFV3mq3a3dt.htm](equipment/equipment-05-RKPAHGFV3mq3a3dt.htm)|Portable Weapon Mount (Shielded Tripod)|auto-trad|
|[equipment-05-Rm2cojERpLEWB9B3.htm](equipment/equipment-05-Rm2cojERpLEWB9B3.htm)|Assisting|auto-trad|
|[equipment-05-Ro3g2JpJXrKXVyEr.htm](equipment/equipment-05-Ro3g2JpJXrKXVyEr.htm)|Armor Potency (+1)|auto-trad|
|[equipment-05-S9eytXwDMdwdSh2z.htm](equipment/equipment-05-S9eytXwDMdwdSh2z.htm)|Hooked|auto-trad|
|[equipment-05-SCkQb4QdFWkATiby.htm](equipment/equipment-05-SCkQb4QdFWkATiby.htm)|Vanishing Wayfinder|auto-trad|
|[equipment-05-sO3b9FRLFY43YQUc.htm](equipment/equipment-05-sO3b9FRLFY43YQUc.htm)|Hongbao of Many Things|auto-trad|
|[equipment-05-SUnKoeqNBsGvUAGe.htm](equipment/equipment-05-SUnKoeqNBsGvUAGe.htm)|Metuak's Pendant|auto-trad|
|[equipment-05-T00Xa9aDwHxd60Zh.htm](equipment/equipment-05-T00Xa9aDwHxd60Zh.htm)|Boots of Elvenkind|auto-trad|
|[equipment-05-T4gTHDKJ0HI10p3y.htm](equipment/equipment-05-T4gTHDKJ0HI10p3y.htm)|Cunning|auto-trad|
|[equipment-05-TVpEf9gMFUuwfGoU.htm](equipment/equipment-05-TVpEf9gMFUuwfGoU.htm)|Resonating Fork|auto-trad|
|[equipment-05-uQOaRpfkUFVYD0Gx.htm](equipment/equipment-05-uQOaRpfkUFVYD0Gx.htm)|Slick|auto-trad|
|[equipment-05-v935rvhKtJm7PSNF.htm](equipment/equipment-05-v935rvhKtJm7PSNF.htm)|Wondrous Figurine, Stuffed Fox|auto-trad|
|[equipment-05-vCf4otZtC4RHtXdI.htm](equipment/equipment-05-vCf4otZtC4RHtXdI.htm)|Poison Concentrator (Lesser)|auto-trad|
|[equipment-05-vx5bG5YNeC78u49n.htm](equipment/equipment-05-vx5bG5YNeC78u49n.htm)|Warding Tattoo|auto-trad|
|[equipment-05-w0iN9Q6UpqNp2xQs.htm](equipment/equipment-05-w0iN9Q6UpqNp2xQs.htm)|Homeward Swallow|auto-trad|
|[equipment-05-wJOW9YfEp5RDYHc1.htm](equipment/equipment-05-wJOW9YfEp5RDYHc1.htm)|Pact of Blood-Taking|auto-trad|
|[equipment-05-WW8uv3Nhg56UTZ35.htm](equipment/equipment-05-WW8uv3Nhg56UTZ35.htm)|Sun Wheel|auto-trad|
|[equipment-05-x4l3JjePzROLDSK6.htm](equipment/equipment-05-x4l3JjePzROLDSK6.htm)|Spectacles of Understanding|auto-trad|
|[equipment-05-xdpP0y4EfRf0ALl8.htm](equipment/equipment-05-xdpP0y4EfRf0ALl8.htm)|Grim Ring|auto-trad|
|[equipment-05-Xv6NlFohsFtIHp6K.htm](equipment/equipment-05-Xv6NlFohsFtIHp6K.htm)|Ring of the Weary Traveler|auto-trad|
|[equipment-05-y8M3fBQEtddg9Lbj.htm](equipment/equipment-05-y8M3fBQEtddg9Lbj.htm)|Remote Trigger|auto-trad|
|[equipment-05-YafmTpwzMsKiAg5b.htm](equipment/equipment-05-YafmTpwzMsKiAg5b.htm)|Mirror Goggles (Lesser)|auto-trad|
|[equipment-05-yAG9XjecF5xpOEvg.htm](equipment/equipment-05-yAG9XjecF5xpOEvg.htm)|Corrosive Engravings|auto-trad|
|[equipment-05-zVY6VVKrrf3K5TSC.htm](equipment/equipment-05-zVY6VVKrrf3K5TSC.htm)|Wind At Your Back|auto-trad|
|[equipment-06-0SSu1fch6ah0oTti.htm](equipment/equipment-06-0SSu1fch6ah0oTti.htm)|Horned Hand Rests|auto-trad|
|[equipment-06-0zTY4ogkffTxmJUj.htm](equipment/equipment-06-0zTY4ogkffTxmJUj.htm)|Deck of illusions|auto-trad|
|[equipment-06-1ii8hvKjkf71wYMx.htm](equipment/equipment-06-1ii8hvKjkf71wYMx.htm)|Storyteller's Opus|auto-trad|
|[equipment-06-2ovu1AioLLff9p8w.htm](equipment/equipment-06-2ovu1AioLLff9p8w.htm)|Hauling|auto-trad|
|[equipment-06-3sGpEBXsZwjGnoES.htm](equipment/equipment-06-3sGpEBXsZwjGnoES.htm)|Chime of Opening|auto-trad|
|[equipment-06-4dSzBQPb68ik7ul8.htm](equipment/equipment-06-4dSzBQPb68ik7ul8.htm)|Majordomo Torc|auto-trad|
|[equipment-06-65ZhBT2S8bCeEIgz.htm](equipment/equipment-06-65ZhBT2S8bCeEIgz.htm)|Poisonous Cloak Type I|auto-trad|
|[equipment-06-6irTMXWYnysLljb6.htm](equipment/equipment-06-6irTMXWYnysLljb6.htm)|Wand of Mercy (2nd-level)|auto-trad|
|[equipment-06-8y3c9suf90ww60gU.htm](equipment/equipment-06-8y3c9suf90ww60gU.htm)|Shifter Prosthesis|auto-trad|
|[equipment-06-b0kltGQGBUBHN8Ap.htm](equipment/equipment-06-b0kltGQGBUBHN8Ap.htm)|Druid's Crown|auto-trad|
|[equipment-06-BKjwg0TEGioiYpz1.htm](equipment/equipment-06-BKjwg0TEGioiYpz1.htm)|Swallow-Spike|auto-trad|
|[equipment-06-cICCgx4NJKSREeZD.htm](equipment/equipment-06-cICCgx4NJKSREeZD.htm)|Spare Wax Cylinder|auto-trad|
|[equipment-06-CKmIgVzlcN7g1YT4.htm](equipment/equipment-06-CKmIgVzlcN7g1YT4.htm)|Wand of Hawthorn (2nd-level)|auto-trad|
|[equipment-06-Duj5vC7F5588ugS6.htm](equipment/equipment-06-Duj5vC7F5588ugS6.htm)|Cryolite Eye|auto-trad|
|[equipment-06-EkZVXMdtqTTgahiJ.htm](equipment/equipment-06-EkZVXMdtqTTgahiJ.htm)|Ring of Energy Resistance|auto-trad|
|[equipment-06-esk1K85W9uqxijnb.htm](equipment/equipment-06-esk1K85W9uqxijnb.htm)|Lucky Kitchen Witch|auto-trad|
|[equipment-06-fsqAB2lS9N8MRaO5.htm](equipment/equipment-06-fsqAB2lS9N8MRaO5.htm)|Midday Lantern (Lesser)|auto-trad|
|[equipment-06-FZ9ELiYCNBlyAz6X.htm](equipment/equipment-06-FZ9ELiYCNBlyAz6X.htm)|Dragon's Breath (2nd Level Spell)|auto-trad|
|[equipment-06-gSSibF07emWGpGKw.htm](equipment/equipment-06-gSSibF07emWGpGKw.htm)|Dread (Lesser)|auto-trad|
|[equipment-06-hAPCk1A2a4kJDzhV.htm](equipment/equipment-06-hAPCk1A2a4kJDzhV.htm)|Spiritsight Ring|auto-trad|
|[equipment-06-hcGvN03ieNWlSQYa.htm](equipment/equipment-06-hcGvN03ieNWlSQYa.htm)|Horn of Fog|auto-trad|
|[equipment-06-hmmDa6LCS22dZT7P.htm](equipment/equipment-06-hmmDa6LCS22dZT7P.htm)|Clandestine Cloak|auto-trad|
|[equipment-06-I95zlGUDCply1Ydm.htm](equipment/equipment-06-I95zlGUDCply1Ydm.htm)|Insistent Door Knocker|auto-trad|
|[equipment-06-i9hF185TRK0cH8B4.htm](equipment/equipment-06-i9hF185TRK0cH8B4.htm)|Demolishing|auto-trad|
|[equipment-06-IISieImYvSQ8AqmC.htm](equipment/equipment-06-IISieImYvSQ8AqmC.htm)|Phantasmal Doorknob|auto-trad|
|[equipment-06-J5MqY1P3JWrezcQX.htm](equipment/equipment-06-J5MqY1P3JWrezcQX.htm)|Primeval Mistletoe|auto-trad|
|[equipment-06-KaMBXc0Yqn2rAUec.htm](equipment/equipment-06-KaMBXc0Yqn2rAUec.htm)|Gingerbread House|auto-trad|
|[equipment-06-KlkPJgiCiHpuyNpv.htm](equipment/equipment-06-KlkPJgiCiHpuyNpv.htm)|Chronicler Wayfinder|auto-trad|
|[equipment-06-Kx1Se9u0LU1ZS5R1.htm](equipment/equipment-06-Kx1Se9u0LU1ZS5R1.htm)|Architect's Pattern Book|auto-trad|
|[equipment-06-LBoSycoKAXrp76zA.htm](equipment/equipment-06-LBoSycoKAXrp76zA.htm)|Courtier's Pillow Book|auto-trad|
|[equipment-06-LDtIZ822rqBD88U2.htm](equipment/equipment-06-LDtIZ822rqBD88U2.htm)|Bewitching Bloom (Magnolia)|auto-trad|
|[equipment-06-LDVqBvMNNcRBucW1.htm](equipment/equipment-06-LDVqBvMNNcRBucW1.htm)|Bi-Resonant Wayfinder|auto-trad|
|[equipment-06-LM5zag6Ogv1fF5zz.htm](equipment/equipment-06-LM5zag6Ogv1fF5zz.htm)|Tooth and Claw Tattoo|auto-trad|
|[equipment-06-MhbeSE3RyBPyFQpq.htm](equipment/equipment-06-MhbeSE3RyBPyFQpq.htm)|Falconsight Eye|auto-trad|
|[equipment-06-mKlUg7SWC5LcOqaj.htm](equipment/equipment-06-mKlUg7SWC5LcOqaj.htm)|Aim-Aiding|auto-trad|
|[equipment-06-Mnj6K8aC7itqSIdg.htm](equipment/equipment-06-Mnj6K8aC7itqSIdg.htm)|Private Workshop|auto-trad|
|[equipment-06-NLC3c1mudubNNekY.htm](equipment/equipment-06-NLC3c1mudubNNekY.htm)|Clockwork Recorder|auto-trad|
|[equipment-06-nNtakXnSrcXWndBV.htm](equipment/equipment-06-nNtakXnSrcXWndBV.htm)|Traveler's Any-Tool|auto-trad|
|[equipment-06-odSvUKzphdwvDqgE.htm](equipment/equipment-06-odSvUKzphdwvDqgE.htm)|Aeon Stone (Gold Nodule)|auto-trad|
|[equipment-06-P3zAUF2yTOeS1GSH.htm](equipment/equipment-06-P3zAUF2yTOeS1GSH.htm)|Light Writer|auto-trad|
|[equipment-06-POETsrh8lz9VnBEG.htm](equipment/equipment-06-POETsrh8lz9VnBEG.htm)|Spirit-Singer (Handheld)|auto-trad|
|[equipment-06-pRjJ2aBDpCH1NKRz.htm](equipment/equipment-06-pRjJ2aBDpCH1NKRz.htm)|Brain Cylinder|auto-trad|
|[equipment-06-q3Eyp5ncO6lzvGnI.htm](equipment/equipment-06-q3Eyp5ncO6lzvGnI.htm)|Secret-Keeper's Mask (Father Skinsaw)|auto-trad|
|[equipment-06-Q5YwZv3PbCHkEImh.htm](equipment/equipment-06-Q5YwZv3PbCHkEImh.htm)|Twilight Lantern (Lesser)|auto-trad|
|[equipment-06-qMmg6e6GlMAM0F39.htm](equipment/equipment-06-qMmg6e6GlMAM0F39.htm)|Society Portrait|auto-trad|
|[equipment-06-qmWlvoIlJRJ6pAeG.htm](equipment/equipment-06-qmWlvoIlJRJ6pAeG.htm)|Wand of Widening (2nd-Level Spell)|auto-trad|
|[equipment-06-QNPwzwKervKpk6YO.htm](equipment/equipment-06-QNPwzwKervKpk6YO.htm)|Ready|auto-trad|
|[equipment-06-qO1swPrA14AnuyBD.htm](equipment/equipment-06-qO1swPrA14AnuyBD.htm)|Codex of Unimpeded Sight|auto-trad|
|[equipment-06-QPz923dZeG1TajQE.htm](equipment/equipment-06-QPz923dZeG1TajQE.htm)|Trackless|auto-trad|
|[equipment-06-Rhr6rkOwjFwCJV0T.htm](equipment/equipment-06-Rhr6rkOwjFwCJV0T.htm)|Wand of Choking Mist (Obscuring Mist)|auto-trad|
|[equipment-06-rmbvBjcDMDAZLJ7v.htm](equipment/equipment-06-rmbvBjcDMDAZLJ7v.htm)|Wand of Reaching (2nd-level)|auto-trad|
|[equipment-06-roeYtwlIe65BPMJ1.htm](equipment/equipment-06-roeYtwlIe65BPMJ1.htm)|Shifting|auto-trad|
|[equipment-06-RoySt3EKFgcicm7D.htm](equipment/equipment-06-RoySt3EKFgcicm7D.htm)|Wand of Shattering Images|auto-trad|
|[equipment-06-s97FDCHi2UcfzKGn.htm](equipment/equipment-06-s97FDCHi2UcfzKGn.htm)|Ring of the Ram|auto-trad|
|[equipment-06-slkvVyRBrUJyZQj8.htm](equipment/equipment-06-slkvVyRBrUJyZQj8.htm)|Retaliation (Lesser)|auto-trad|
|[equipment-06-TacKaUs8cIddqiCU.htm](equipment/equipment-06-TacKaUs8cIddqiCU.htm)|Choker of Elocution|auto-trad|
|[equipment-06-tNoQ2moQzt0BaHm4.htm](equipment/equipment-06-tNoQ2moQzt0BaHm4.htm)|Pathfinder's Mentor|auto-trad|
|[equipment-06-tpkkAtlMIOL8TnW6.htm](equipment/equipment-06-tpkkAtlMIOL8TnW6.htm)|Quenching|auto-trad|
|[equipment-06-U0R8AHRhK0Wu9r2i.htm](equipment/equipment-06-U0R8AHRhK0Wu9r2i.htm)|Blast Foot|auto-trad|
|[equipment-06-U4FSOEH2Z6NDpN39.htm](equipment/equipment-06-U4FSOEH2Z6NDpN39.htm)|Wand of Rolling Flames (2nd-level)|auto-trad|
|[equipment-06-UkEDBhYxHWcTaMTG.htm](equipment/equipment-06-UkEDBhYxHWcTaMTG.htm)|Warding Tattoo (Wave)|auto-trad|
|[equipment-06-V5kv7e1r2qV2k3lf.htm](equipment/equipment-06-V5kv7e1r2qV2k3lf.htm)|Aeon Stone (Western Star)|auto-trad|
|[equipment-06-vT2OibHsrq7M6MMO.htm](equipment/equipment-06-vT2OibHsrq7M6MMO.htm)|Bestiary of Metamorphosis|auto-trad|
|[equipment-06-WQ3yrHtv2fN5UO57.htm](equipment/equipment-06-WQ3yrHtv2fN5UO57.htm)|Lantern of Empty Light|auto-trad|
|[equipment-06-XFJD6eOX6H9sLcbC.htm](equipment/equipment-06-XFJD6eOX6H9sLcbC.htm)|Warding Tattoo (Trail)|auto-trad|
|[equipment-06-XrM3Cza8vM6Jzv98.htm](equipment/equipment-06-XrM3Cza8vM6Jzv98.htm)|Endless Grimoire|auto-trad|
|[equipment-06-YBKhjWqFuvgkArba.htm](equipment/equipment-06-YBKhjWqFuvgkArba.htm)|Cassisian Helmet|auto-trad|
|[equipment-06-yK5maWEcZ0pNnFwq.htm](equipment/equipment-06-yK5maWEcZ0pNnFwq.htm)|Undertaker's Manifest|auto-trad|
|[equipment-06-YoRL8PkjYInpmBWl.htm](equipment/equipment-06-YoRL8PkjYInpmBWl.htm)|Stony Hag Eye|auto-trad|
|[equipment-06-Yq3l3eB70BsbQFj6.htm](equipment/equipment-06-Yq3l3eB70BsbQFj6.htm)|Wand of Mental Purification (2nd-level)|auto-trad|
|[equipment-06-zaJ4HSNa6kMozYvM.htm](equipment/equipment-06-zaJ4HSNa6kMozYvM.htm)|Wand of Legerdemain (2nd-level)|auto-trad|
|[equipment-06-ZJ3ahspZOXL4CK4J.htm](equipment/equipment-06-ZJ3ahspZOXL4CK4J.htm)|Wand of Hopeless Night (2nd-Level Spell)|auto-trad|
|[equipment-06-zNtC1DsiAUYeHfDN.htm](equipment/equipment-06-zNtC1DsiAUYeHfDN.htm)|Wand of Hybrid Form (2nd-level)|auto-trad|
|[equipment-06-ZW8W30XqgFf1KB3I.htm](equipment/equipment-06-ZW8W30XqgFf1KB3I.htm)|Warcaller's Chime of Blasting|auto-trad|
|[equipment-07-14rbefsoClgClRQ8.htm](equipment/equipment-07-14rbefsoClgClRQ8.htm)|Ring of Sustenance|auto-trad|
|[equipment-07-3cV1kzaP1ofw3xtU.htm](equipment/equipment-07-3cV1kzaP1ofw3xtU.htm)|Fearless Sash|auto-trad|
|[equipment-07-3TQ9oSETsOHcHRBF.htm](equipment/equipment-07-3TQ9oSETsOHcHRBF.htm)|Bravery Baldric (Restoration)|auto-trad|
|[equipment-07-4DXupoMmwenFn4Kc.htm](equipment/equipment-07-4DXupoMmwenFn4Kc.htm)|Deathdrinking|auto-trad|
|[equipment-07-4J87czEOWwbGXE3r.htm](equipment/equipment-07-4J87czEOWwbGXE3r.htm)|Reflexive Tattoo|auto-trad|
|[equipment-07-5V9bgqgQY1CHLd40.htm](equipment/equipment-07-5V9bgqgQY1CHLd40.htm)|Wand of Continuation (2nd-Level Spell)|auto-trad|
|[equipment-07-6XX3tYxkyQXCMAbd.htm](equipment/equipment-07-6XX3tYxkyQXCMAbd.htm)|Called (Lastwall)|auto-trad|
|[equipment-07-7Qc3KezVroqC7Jve.htm](equipment/equipment-07-7Qc3KezVroqC7Jve.htm)|Jar of Shifting Sands|auto-trad|
|[equipment-07-7TQw7V1zZKl0a0Xz.htm](equipment/equipment-07-7TQw7V1zZKl0a0Xz.htm)|Bottled Air|auto-trad|
|[equipment-07-A4BhFOb4iQtJYYKq.htm](equipment/equipment-07-A4BhFOb4iQtJYYKq.htm)|Dragon's Eye Charm|auto-trad|
|[equipment-07-aBVrNIPoPGOYxm80.htm](equipment/equipment-07-aBVrNIPoPGOYxm80.htm)|Decanter of Endless Water|auto-trad|
|[equipment-07-babiGiHQwY6Bg4XP.htm](equipment/equipment-07-babiGiHQwY6Bg4XP.htm)|Dawnlight (Greater)|auto-trad|
|[equipment-07-bfJI3xeIWaqcYRlU.htm](equipment/equipment-07-bfJI3xeIWaqcYRlU.htm)|Winder's Ring|auto-trad|
|[equipment-07-BOjMAfhfDYc3MTLQ.htm](equipment/equipment-07-BOjMAfhfDYc3MTLQ.htm)|Collar of the Eternal Bond|auto-trad|
|[equipment-07-cHCaDiKel0qAIQmC.htm](equipment/equipment-07-cHCaDiKel0qAIQmC.htm)|Conducting|auto-trad|
|[equipment-07-dChkPrLJfZDPBvWR.htm](equipment/equipment-07-dChkPrLJfZDPBvWR.htm)|Venomed Tongue|auto-trad|
|[equipment-07-ecqz1iUGtyQEkZwy.htm](equipment/equipment-07-ecqz1iUGtyQEkZwy.htm)|Boots of Bounding|auto-trad|
|[equipment-07-ErqxXatIYbv2WcsE.htm](equipment/equipment-07-ErqxXatIYbv2WcsE.htm)|Bewitching Bloom (Bellflower)|auto-trad|
|[equipment-07-faKyy6ETkDgrUnvf.htm](equipment/equipment-07-faKyy6ETkDgrUnvf.htm)|Ring of Wizardry (Type I)|auto-trad|
|[equipment-07-ftSaD4c5io4pP4OB.htm](equipment/equipment-07-ftSaD4c5io4pP4OB.htm)|Gloves of Carelessness|auto-trad|
|[equipment-07-FUzPnIXLHZenTtYo.htm](equipment/equipment-07-FUzPnIXLHZenTtYo.htm)|Crown of Insight|auto-trad|
|[equipment-07-fWgH0JxNCOpI7SVr.htm](equipment/equipment-07-fWgH0JxNCOpI7SVr.htm)|Beastmaster's Sigil (Greater)|auto-trad|
|[equipment-07-GAffuPaxuhSku90B.htm](equipment/equipment-07-GAffuPaxuhSku90B.htm)|Ring of Sneering Charity|auto-trad|
|[equipment-07-gfBAxuitJje6NL7G.htm](equipment/equipment-07-gfBAxuitJje6NL7G.htm)|Rope of Climbing (Greater)|auto-trad|
|[equipment-07-GNX0BNOoCSOYPedi.htm](equipment/equipment-07-GNX0BNOoCSOYPedi.htm)|Flurrying|auto-trad|
|[equipment-07-GUyavibqPPxwFxyR.htm](equipment/equipment-07-GUyavibqPPxwFxyR.htm)|Swarmform Collar|auto-trad|
|[equipment-07-GW45HOqel23w465z.htm](equipment/equipment-07-GW45HOqel23w465z.htm)|Admirer's Bouquet|auto-trad|
|[equipment-07-Gzz2F90inajTPvcX.htm](equipment/equipment-07-Gzz2F90inajTPvcX.htm)|Mirror of Sleeping Vigil|auto-trad|
|[equipment-07-hBGFCZbI9nAjSdfE.htm](equipment/equipment-07-hBGFCZbI9nAjSdfE.htm)|Drover's Band|auto-trad|
|[equipment-07-Hqu2CmkLIE5Ov41i.htm](equipment/equipment-07-Hqu2CmkLIE5Ov41i.htm)|Wand of Spiritual Warfare (2nd-Level Spell)|auto-trad|
|[equipment-07-Jx7cxxTqOENzlGaj.htm](equipment/equipment-07-Jx7cxxTqOENzlGaj.htm)|Enigma Mirror|auto-trad|
|[equipment-07-K0uJJSso0WGpFzYe.htm](equipment/equipment-07-K0uJJSso0WGpFzYe.htm)|Swiftmount Saddle|auto-trad|
|[equipment-07-KAOlwH6kBCCvEYbW.htm](equipment/equipment-07-KAOlwH6kBCCvEYbW.htm)|Verdant Branch|auto-trad|
|[equipment-07-kOEZCUTCPCqCFoJf.htm](equipment/equipment-07-kOEZCUTCPCqCFoJf.htm)|Deathless|auto-trad|
|[equipment-07-KR35T5By2iaLKEwH.htm](equipment/equipment-07-KR35T5By2iaLKEwH.htm)|Hat of Disagreeable Disguise (Greater)|auto-trad|
|[equipment-07-kSaUlWgYMywIRV3C.htm](equipment/equipment-07-kSaUlWgYMywIRV3C.htm)|Aeon Stone (Tourmaline Sphere)|auto-trad|
|[equipment-07-LH9nHvta0q39JD86.htm](equipment/equipment-07-LH9nHvta0q39JD86.htm)|Wind-Catcher|auto-trad|
|[equipment-07-M2CPAgSAoEL4oawq.htm](equipment/equipment-07-M2CPAgSAoEL4oawq.htm)|Aeon Stone (Clear Spindle)|auto-trad|
|[equipment-07-M450qjybosAkGcdN.htm](equipment/equipment-07-M450qjybosAkGcdN.htm)|Hand-Hewed Face|auto-trad|
|[equipment-07-MNBnZn0b80Q7yHJM.htm](equipment/equipment-07-MNBnZn0b80Q7yHJM.htm)|Cloak of Elvenkind|auto-trad|
|[equipment-07-ngz7dYysC1NkBBRK.htm](equipment/equipment-07-ngz7dYysC1NkBBRK.htm)|Gloves of Storing|auto-trad|
|[equipment-07-nI5x32JWjcEqYCq6.htm](equipment/equipment-07-nI5x32JWjcEqYCq6.htm)|Sealing Chest (Moderate)|auto-trad|
|[equipment-07-nOI2irf1OifSdp9P.htm](equipment/equipment-07-nOI2irf1OifSdp9P.htm)|Cloak of Immolation|auto-trad|
|[equipment-07-nv9riwCAkAXUJYX2.htm](equipment/equipment-07-nv9riwCAkAXUJYX2.htm)|Ring of Ravenousness|auto-trad|
|[equipment-07-oqqHMA3eHBwJ2BlB.htm](equipment/equipment-07-oqqHMA3eHBwJ2BlB.htm)|Barding Saddle|auto-trad|
|[equipment-07-Oudcra1CGDmUq0BH.htm](equipment/equipment-07-Oudcra1CGDmUq0BH.htm)|Unmemorable Mantle|auto-trad|
|[equipment-07-OVyGHaHjYI2DBV0h.htm](equipment/equipment-07-OVyGHaHjYI2DBV0h.htm)|Doctrine of Blissful Eternity|auto-trad|
|[equipment-07-peoheZMxdPHUNo93.htm](equipment/equipment-07-peoheZMxdPHUNo93.htm)|Horseshoes of Speed|auto-trad|
|[equipment-07-QgNp6uyKuV2PiNbf.htm](equipment/equipment-07-QgNp6uyKuV2PiNbf.htm)|Wand of Fey Flames|auto-trad|
|[equipment-07-qtT0cgt7uUSKY2qK.htm](equipment/equipment-07-qtT0cgt7uUSKY2qK.htm)|Maestro's Chair|auto-trad|
|[equipment-07-rJan8OyK6gmtxXmt.htm](equipment/equipment-07-rJan8OyK6gmtxXmt.htm)|Queasy Lantern (Lesser)|auto-trad|
|[equipment-07-RjJw7iHantxqeJu1.htm](equipment/equipment-07-RjJw7iHantxqeJu1.htm)|Wondrous Figurine (Jade Serpent)|auto-trad|
|[equipment-07-skBa6D1uxb0b2USn.htm](equipment/equipment-07-skBa6D1uxb0b2USn.htm)|Slippers of Spider Climbing|auto-trad|
|[equipment-07-SW6JVF5SP59W8B0Q.htm](equipment/equipment-07-SW6JVF5SP59W8B0Q.htm)|Warding Statuette|auto-trad|
|[equipment-07-tf69NMnUoUAYrWtj.htm](equipment/equipment-07-tf69NMnUoUAYrWtj.htm)|Wand of the Spider (2nd-Level Spell)|auto-trad|
|[equipment-07-TxB1a2DrhRZABRjV.htm](equipment/equipment-07-TxB1a2DrhRZABRjV.htm)|Cloak of Thirsty Fronds|auto-trad|
|[equipment-07-UMTeKNfyj95JMwMM.htm](equipment/equipment-07-UMTeKNfyj95JMwMM.htm)|Vaporous Pipe|auto-trad|
|[equipment-07-V0ryBu9M5OPd4Wub.htm](equipment/equipment-07-V0ryBu9M5OPd4Wub.htm)|Warding Tattoo (Fiend)|auto-trad|
|[equipment-07-VMIwKvpQHh91cQoa.htm](equipment/equipment-07-VMIwKvpQHh91cQoa.htm)|Clockwork Heels|auto-trad|
|[equipment-07-WPhrH1zCbP2v10aP.htm](equipment/equipment-07-WPhrH1zCbP2v10aP.htm)|Energy Robe of Fire|auto-trad|
|[equipment-07-WPSp5MLb0VOfmUqH.htm](equipment/equipment-07-WPSp5MLb0VOfmUqH.htm)|Hat of Disguise (Greater)|auto-trad|
|[equipment-07-WvrPq0UGEX3wJg4a.htm](equipment/equipment-07-WvrPq0UGEX3wJg4a.htm)|Resonating Fork (Greater)|auto-trad|
|[equipment-07-Yed5EEEWdJyf2AVX.htm](equipment/equipment-07-Yed5EEEWdJyf2AVX.htm)|Restful Tent|auto-trad|
|[equipment-07-YO2X0EE3txascuuP.htm](equipment/equipment-07-YO2X0EE3txascuuP.htm)|Wondrous Figurine (Rubber Bear)|auto-trad|
|[equipment-07-znWf2DgWyElwzQwJ.htm](equipment/equipment-07-znWf2DgWyElwzQwJ.htm)|Detector Stone|auto-trad|
|[equipment-08-02q8s6sSicMkhs1l.htm](equipment/equipment-08-02q8s6sSicMkhs1l.htm)|Bracers of Armor I|auto-trad|
|[equipment-08-04V1qwob0JGPEx3k.htm](equipment/equipment-08-04V1qwob0JGPEx3k.htm)|Wondrous Figurine (Bismuth Leopards)|auto-trad|
|[equipment-08-0OzL3DdtTnZE91MU.htm](equipment/equipment-08-0OzL3DdtTnZE91MU.htm)|Smoky Hag Eye|auto-trad|
|[equipment-08-25WDsZPxt4tjgovE.htm](equipment/equipment-08-25WDsZPxt4tjgovE.htm)|Wand of Mental Purification (3rd-level)|auto-trad|
|[equipment-08-2APHVWkqnFvecrfx.htm](equipment/equipment-08-2APHVWkqnFvecrfx.htm)|Bootstrap Respirator|auto-trad|
|[equipment-08-2cWs7heYDt9jCSJ4.htm](equipment/equipment-08-2cWs7heYDt9jCSJ4.htm)|Fortune's Coin|auto-trad|
|[equipment-08-4TJf8zJ2yRzBy74v.htm](equipment/equipment-08-4TJf8zJ2yRzBy74v.htm)|Gossip's Eye|auto-trad|
|[equipment-08-57vUQS7HSln47EH8.htm](equipment/equipment-08-57vUQS7HSln47EH8.htm)|Harrow Spellcards|auto-trad|
|[equipment-08-5Dk8NQMW8NXFEKPy.htm](equipment/equipment-08-5Dk8NQMW8NXFEKPy.htm)|Book of Warding Prayers|auto-trad|
|[equipment-08-5W86t6I92rRNSmwR.htm](equipment/equipment-08-5W86t6I92rRNSmwR.htm)|Trinity Geode (Greater)|auto-trad|
|[equipment-08-5YZmX9AdhQxXaOzX.htm](equipment/equipment-08-5YZmX9AdhQxXaOzX.htm)|Dragon's Breath (3rd Level Spell)|auto-trad|
|[equipment-08-6wVWwpL9pYr3yQtt.htm](equipment/equipment-08-6wVWwpL9pYr3yQtt.htm)|Rod of Wonder|auto-trad|
|[equipment-08-7eggb7exEAbZABrd.htm](equipment/equipment-08-7eggb7exEAbZABrd.htm)|Bagpipes of Turmoil (Greater)|auto-trad|
|[equipment-08-8MclH3pggVkHGYnJ.htm](equipment/equipment-08-8MclH3pggVkHGYnJ.htm)|Carrion Cask|auto-trad|
|[equipment-08-8Rels5G8re0MXFAS.htm](equipment/equipment-08-8Rels5G8re0MXFAS.htm)|Aquarium Lamp|auto-trad|
|[equipment-08-8x8zqH4pJKJRlyAj.htm](equipment/equipment-08-8x8zqH4pJKJRlyAj.htm)|Moonstone Diadem|auto-trad|
|[equipment-08-A2Z7Mh8A59wZb5vv.htm](equipment/equipment-08-A2Z7Mh8A59wZb5vv.htm)|Gliding|auto-trad|
|[equipment-08-ADruUHNWh2AuxJFb.htm](equipment/equipment-08-ADruUHNWh2AuxJFb.htm)|Waffle Iron (Mithral)|auto-trad|
|[equipment-08-alV162EF3qZrbxt0.htm](equipment/equipment-08-alV162EF3qZrbxt0.htm)|Five-Feather Wreath (Greater)|auto-trad|
|[equipment-08-auEm7vmpG9V8xpAJ.htm](equipment/equipment-08-auEm7vmpG9V8xpAJ.htm)|Jolt Coil (Greater)|auto-trad|
|[equipment-08-AzLEUTp4RHYAoXIe.htm](equipment/equipment-08-AzLEUTp4RHYAoXIe.htm)|Wand of Reaching (3rd-level)|auto-trad|
|[equipment-08-bh3zC7WcVlM0qgYZ.htm](equipment/equipment-08-bh3zC7WcVlM0qgYZ.htm)|Fleshgem (Earthspeaker)|auto-trad|
|[equipment-08-bv1bQGFywtNkYWWD.htm](equipment/equipment-08-bv1bQGFywtNkYWWD.htm)|Wand of Dumbfounding Doom (3rd-level)|auto-trad|
|[equipment-08-c2Oa9UbhjwAsZaPp.htm](equipment/equipment-08-c2Oa9UbhjwAsZaPp.htm)|Wand of Smoldering Fireballs (3rd-Level Spell)|auto-trad|
|[equipment-08-c8v8gWJ87xiUVnsb.htm](equipment/equipment-08-c8v8gWJ87xiUVnsb.htm)|Horrid Figurine|auto-trad|
|[equipment-08-C9wOlvuVCjVbz1YQ.htm](equipment/equipment-08-C9wOlvuVCjVbz1YQ.htm)|Bloodbane|auto-trad|
|[equipment-08-cbbPoYOil7DZO4mk.htm](equipment/equipment-08-cbbPoYOil7DZO4mk.htm)|Stalk Goggles (Major)|auto-trad|
|[equipment-08-cHP1s9Ntki9B1HgM.htm](equipment/equipment-08-cHP1s9Ntki9B1HgM.htm)|Drums of War (Greater)|auto-trad|
|[equipment-08-Ck2nuiQXkoURspvy.htm](equipment/equipment-08-Ck2nuiQXkoURspvy.htm)|Tooth and Claw Tattoo (Greater)|auto-trad|
|[equipment-08-cvb47A6K1w7RfNiv.htm](equipment/equipment-08-cvb47A6K1w7RfNiv.htm)|Fanged (Greater)|auto-trad|
|[equipment-08-D7ARQDAr9tO61UqY.htm](equipment/equipment-08-D7ARQDAr9tO61UqY.htm)|Seer's Flute (Greater)|auto-trad|
|[equipment-08-dFYEGk1nUlHqQ5qy.htm](equipment/equipment-08-dFYEGk1nUlHqQ5qy.htm)|Lightweave Scarf|auto-trad|
|[equipment-08-DsZ48n6mn8uW5EIP.htm](equipment/equipment-08-DsZ48n6mn8uW5EIP.htm)|Tallowheart Mass|auto-trad|
|[equipment-08-dTxbaa7HSiJbIuNN.htm](equipment/equipment-08-dTxbaa7HSiJbIuNN.htm)|Thundering|auto-trad|
|[equipment-08-EiCCWG0Xta0ZBUfA.htm](equipment/equipment-08-EiCCWG0Xta0ZBUfA.htm)|Steam Winch|auto-trad|
|[equipment-08-EjV3Pb13DLzGTCcY.htm](equipment/equipment-08-EjV3Pb13DLzGTCcY.htm)|Giant-Killing|auto-trad|
|[equipment-08-eSjQgsl3pYkirOwk.htm](equipment/equipment-08-eSjQgsl3pYkirOwk.htm)|Resilient|auto-trad|
|[equipment-08-FMtUVzTDiTbxXMN9.htm](equipment/equipment-08-FMtUVzTDiTbxXMN9.htm)|Flaming Star (Greater)|auto-trad|
|[equipment-08-GkQJfRZC749piiBr.htm](equipment/equipment-08-GkQJfRZC749piiBr.htm)|Tome of Scintillating Sleet|auto-trad|
|[equipment-08-GM9Ky5LEkPHZqme0.htm](equipment/equipment-08-GM9Ky5LEkPHZqme0.htm)|Alluring Scarf|auto-trad|
|[equipment-08-gxzlLfvXeZ8BGlf4.htm](equipment/equipment-08-gxzlLfvXeZ8BGlf4.htm)|Crown of the Fire Eater|auto-trad|
|[equipment-08-HnOWpyuCHwewPZMu.htm](equipment/equipment-08-HnOWpyuCHwewPZMu.htm)|Pickled Demon Tongue (Greater)|auto-trad|
|[equipment-08-ImokJgCMIb0dFyKH.htm](equipment/equipment-08-ImokJgCMIb0dFyKH.htm)|Summoning Handscroll|auto-trad|
|[equipment-08-j6laknUcsOA6utvf.htm](equipment/equipment-08-j6laknUcsOA6utvf.htm)|Warcaller's Chime of Refuge|auto-trad|
|[equipment-08-jfLMxToNvg2VzjSw.htm](equipment/equipment-08-jfLMxToNvg2VzjSw.htm)|Tyrant's Writs|auto-trad|
|[equipment-08-k9BYXA4b7Z4Hy6lZ.htm](equipment/equipment-08-k9BYXA4b7Z4Hy6lZ.htm)|Deafening Music Box|auto-trad|
|[equipment-08-k9DHki1JS7FBG0Jx.htm](equipment/equipment-08-k9DHki1JS7FBG0Jx.htm)|Spellbook of Redundant Enchantment|auto-trad|
|[equipment-08-kJSB09q2KTAcWZy9.htm](equipment/equipment-08-kJSB09q2KTAcWZy9.htm)|Bag of Cats|auto-trad|
|[equipment-08-LD2cFdr3IntL71oP.htm](equipment/equipment-08-LD2cFdr3IntL71oP.htm)|Backfire Mantle (Greater)|auto-trad|
|[equipment-08-LiJMupjynmkM5Mld.htm](equipment/equipment-08-LiJMupjynmkM5Mld.htm)|Slick (Greater)|auto-trad|
|[equipment-08-LKtbZiY5lmD7ohcD.htm](equipment/equipment-08-LKtbZiY5lmD7ohcD.htm)|Bleachguard Doll|auto-trad|
|[equipment-08-LqAf5RwxiZJredii.htm](equipment/equipment-08-LqAf5RwxiZJredii.htm)|Brightbloom Posy|auto-trad|
|[equipment-08-Lw3B9DpnyrpXD9Di.htm](equipment/equipment-08-Lw3B9DpnyrpXD9Di.htm)|Energy-Resistant|auto-trad|
|[equipment-08-m3XmIRwYoGGxZQgu.htm](equipment/equipment-08-m3XmIRwYoGGxZQgu.htm)|Wand of Dazzling Rays (3rd-level)|auto-trad|
|[equipment-08-M5M1WJ5KzbYfRGsY.htm](equipment/equipment-08-M5M1WJ5KzbYfRGsY.htm)|Frost|auto-trad|
|[equipment-08-mb59fHB9IPExjqam.htm](equipment/equipment-08-mb59fHB9IPExjqam.htm)|Chair of Inventions|auto-trad|
|[equipment-08-MmQw3qu51AuqXKCC.htm](equipment/equipment-08-MmQw3qu51AuqXKCC.htm)|Frog Chair|auto-trad|
|[equipment-08-mOOJwTOxEFYfi9LV.htm](equipment/equipment-08-mOOJwTOxEFYfi9LV.htm)|Desolation Locket|auto-trad|
|[equipment-08-mUJc8M1gKG1HkH4m.htm](equipment/equipment-08-mUJc8M1gKG1HkH4m.htm)|Staring Skull|auto-trad|
|[equipment-08-neanlQgRTZQh2zy7.htm](equipment/equipment-08-neanlQgRTZQh2zy7.htm)|Rime Crystal (Greater)|auto-trad|
|[equipment-08-NiO3YZ6e9RmajCS7.htm](equipment/equipment-08-NiO3YZ6e9RmajCS7.htm)|Umbral Wings|auto-trad|
|[equipment-08-NmhKGflbT5NnKduz.htm](equipment/equipment-08-NmhKGflbT5NnKduz.htm)|Wand of Crackling Lightning (3rd-Level Spell)|auto-trad|
|[equipment-08-nv1OBtriMXhjQdmk.htm](equipment/equipment-08-nv1OBtriMXhjQdmk.htm)|In the Shadows of Toil|auto-trad|
|[equipment-08-NVst7e69agGG9Qwd.htm](equipment/equipment-08-NVst7e69agGG9Qwd.htm)|Shock|auto-trad|
|[equipment-08-nZE16PtiyY7DWlzd.htm](equipment/equipment-08-nZE16PtiyY7DWlzd.htm)|Lover's Gloves|auto-trad|
|[equipment-08-o7JdR8SFXIf5dLIW.htm](equipment/equipment-08-o7JdR8SFXIf5dLIW.htm)|Empathic Cords|auto-trad|
|[equipment-08-OoYfDIq8VoVX38Ds.htm](equipment/equipment-08-OoYfDIq8VoVX38Ds.htm)|Catching|auto-trad|
|[equipment-08-oU3dxIAnTYlGXJVC.htm](equipment/equipment-08-oU3dxIAnTYlGXJVC.htm)|Folding Boat|auto-trad|
|[equipment-08-pDNlJMlF4OotsVBh.htm](equipment/equipment-08-pDNlJMlF4OotsVBh.htm)|Entertainer's Lute (Greater)|auto-trad|
|[equipment-08-Pq7JhHw3QhqqxY9d.htm](equipment/equipment-08-Pq7JhHw3QhqqxY9d.htm)|Trickster's Mandolin (Greater)|auto-trad|
|[equipment-08-QDYPr19De3TBIysx.htm](equipment/equipment-08-QDYPr19De3TBIysx.htm)|Sinister Knight|auto-trad|
|[equipment-08-qJW6SU0hvLrxVbnV.htm](equipment/equipment-08-qJW6SU0hvLrxVbnV.htm)|Preserving (Greater)|auto-trad|
|[equipment-08-QnuL1UEot8ptWNb1.htm](equipment/equipment-08-QnuL1UEot8ptWNb1.htm)|Encompassing Lockpick|auto-trad|
|[equipment-08-qqT5PVPnjBST4fZb.htm](equipment/equipment-08-qqT5PVPnjBST4fZb.htm)|Wand of Paralytic Shock (3rd-level)|auto-trad|
|[equipment-08-Qquz4V1hKndtXSvw.htm](equipment/equipment-08-Qquz4V1hKndtXSvw.htm)|Wand of Rolling Flames (3rd-level)|auto-trad|
|[equipment-08-rJcynWlGp05cLsrG.htm](equipment/equipment-08-rJcynWlGp05cLsrG.htm)|Portable Gaming Hall|auto-trad|
|[equipment-08-rns5Uvpgf8rSbOEX.htm](equipment/equipment-08-rns5Uvpgf8rSbOEX.htm)|Wand of Hybrid Form (3rd-level)|auto-trad|
|[equipment-08-RRZGHR81cUOvu6Tx.htm](equipment/equipment-08-RRZGHR81cUOvu6Tx.htm)|Rhinoceros Mask (Greater)|auto-trad|
|[equipment-08-Rsoh0Y3RQD8x8ito.htm](equipment/equipment-08-Rsoh0Y3RQD8x8ito.htm)|Collar of Inconspicuousness|auto-trad|
|[equipment-08-S4w9D1naKennNO5T.htm](equipment/equipment-08-S4w9D1naKennNO5T.htm)|Eye of the Unseen|auto-trad|
|[equipment-08-SUgFSQJWnfTcM1ZJ.htm](equipment/equipment-08-SUgFSQJWnfTcM1ZJ.htm)|Tablet of Chained Souls|auto-trad|
|[equipment-08-TJaumkbZy11sIAgR.htm](equipment/equipment-08-TJaumkbZy11sIAgR.htm)|Wand of Widening (3rd-Level Spell)|auto-trad|
|[equipment-08-tWgN8hQbHlHNMTcF.htm](equipment/equipment-08-tWgN8hQbHlHNMTcF.htm)|Sun Dazzler|auto-trad|
|[equipment-08-UBfAwXBU5iQ59Bz7.htm](equipment/equipment-08-UBfAwXBU5iQ59Bz7.htm)|Cordelia's Construct Key|auto-trad|
|[equipment-08-UgfTCOJjjMsrqynr.htm](equipment/equipment-08-UgfTCOJjjMsrqynr.htm)|Clarity Goggles (Lesser)|auto-trad|
|[equipment-08-VB9hU5JVAsvNXc1w.htm](equipment/equipment-08-VB9hU5JVAsvNXc1w.htm)|Wand of Mercy (3rd-level)|auto-trad|
|[equipment-08-VDudQ4x2ozosAbTb.htm](equipment/equipment-08-VDudQ4x2ozosAbTb.htm)|Invisibility|auto-trad|
|[equipment-08-VToK4BNsTPOMAhHH.htm](equipment/equipment-08-VToK4BNsTPOMAhHH.htm)|Robe of Feathers Fall|auto-trad|
|[equipment-08-wdnQb4WoO0ghkCeP.htm](equipment/equipment-08-wdnQb4WoO0ghkCeP.htm)|Energy Robe of Cold|auto-trad|
|[equipment-08-Wm0X7Pfd1bfocPSv.htm](equipment/equipment-08-Wm0X7Pfd1bfocPSv.htm)|Corrosive|auto-trad|
|[equipment-08-X1uTEyn6CRXCzgqr.htm](equipment/equipment-08-X1uTEyn6CRXCzgqr.htm)|Perfect Droplet (Greater)|auto-trad|
|[equipment-08-X42q1s1ZnxliirGW.htm](equipment/equipment-08-X42q1s1ZnxliirGW.htm)|Pipes of Compulsion (Greater)|auto-trad|
|[equipment-08-X5QdnhQyF5Lk1VWV.htm](equipment/equipment-08-X5QdnhQyF5Lk1VWV.htm)|Armory Bracelet (Moderate)|auto-trad|
|[equipment-08-XgO0MTOXdpbfdzr6.htm](equipment/equipment-08-XgO0MTOXdpbfdzr6.htm)|Right of Retribution|auto-trad|
|[equipment-08-Xnhp7g1UOPVPZx7u.htm](equipment/equipment-08-Xnhp7g1UOPVPZx7u.htm)|Brooch of Inspiration|auto-trad|
|[equipment-08-XPqKEI246hsr9R6P.htm](equipment/equipment-08-XPqKEI246hsr9R6P.htm)|Wand of Legerdemain (3rd-level)|auto-trad|
|[equipment-08-XszNvxnymWYRaoTp.htm](equipment/equipment-08-XszNvxnymWYRaoTp.htm)|Flaming|auto-trad|
|[equipment-08-XZleXDXQ5eThTLWd.htm](equipment/equipment-08-XZleXDXQ5eThTLWd.htm)|Campaign Stable|auto-trad|
|[equipment-08-Y8kiHC9keql957uo.htm](equipment/equipment-08-Y8kiHC9keql957uo.htm)|Bottomless Purse|auto-trad|
|[equipment-08-yQTmIcXIDg7nVpJX.htm](equipment/equipment-08-yQTmIcXIDg7nVpJX.htm)|Grim Sandglass (Greater)|auto-trad|
|[equipment-08-ywsDnHaJSD9oHvh7.htm](equipment/equipment-08-ywsDnHaJSD9oHvh7.htm)|Faith Tattoo (Greater)|auto-trad|
|[equipment-08-Z5XaC2iyi5GLKYrI.htm](equipment/equipment-08-Z5XaC2iyi5GLKYrI.htm)|Warding Tablets|auto-trad|
|[equipment-08-ZeXTbx8MhQQ6ylhU.htm](equipment/equipment-08-ZeXTbx8MhQQ6ylhU.htm)|Sunflower Censer|auto-trad|
|[equipment-09-0GOHTXBxs6H6ARBz.htm](equipment/equipment-09-0GOHTXBxs6H6ARBz.htm)|Healer's Gloves (Greater)|auto-trad|
|[equipment-09-0PPQl3TEr1yNhhN6.htm](equipment/equipment-09-0PPQl3TEr1yNhhN6.htm)|Persona Mask (Greater)|auto-trad|
|[equipment-09-14LrP7bx8Q1jimHO.htm](equipment/equipment-09-14LrP7bx8Q1jimHO.htm)|Immovable Rod|auto-trad|
|[equipment-09-1bvH8zFQvDYky9tr.htm](equipment/equipment-09-1bvH8zFQvDYky9tr.htm)|Pendant of the Occult (Greater)|auto-trad|
|[equipment-09-2QZbG6FUr2AWK6km.htm](equipment/equipment-09-2QZbG6FUr2AWK6km.htm)|Immovable Arm|auto-trad|
|[equipment-09-3G2No6QaU1wSPTh6.htm](equipment/equipment-09-3G2No6QaU1wSPTh6.htm)|Urn of Ashes|auto-trad|
|[equipment-09-3Lexs7KnhbV0HgFh.htm](equipment/equipment-09-3Lexs7KnhbV0HgFh.htm)|Wand of Manifold Missiles (3rd-Level Spell)|auto-trad|
|[equipment-09-3Ucc6E5V6bzdgFLE.htm](equipment/equipment-09-3Ucc6E5V6bzdgFLE.htm)|Bewitching Bloom (Lotus)|auto-trad|
|[equipment-09-3x7YAs6nos79Dpt7.htm](equipment/equipment-09-3x7YAs6nos79Dpt7.htm)|Enveloping Light (Greater)|auto-trad|
|[equipment-09-45zjE7pj6FUHuz3K.htm](equipment/equipment-09-45zjE7pj6FUHuz3K.htm)|Advancing|auto-trad|
|[equipment-09-4CnzS57PB3Em02YN.htm](equipment/equipment-09-4CnzS57PB3Em02YN.htm)|Fulu Compendium|auto-trad|
|[equipment-09-4RBeMHw6u9pxNDpo.htm](equipment/equipment-09-4RBeMHw6u9pxNDpo.htm)|Vigilant Eye|auto-trad|
|[equipment-09-5EDJgEYfktSEa1pr.htm](equipment/equipment-09-5EDJgEYfktSEa1pr.htm)|Self-Emptying Pocket|auto-trad|
|[equipment-09-5g0rE5yMsTH3E2LJ.htm](equipment/equipment-09-5g0rE5yMsTH3E2LJ.htm)|Manacles (Good)|auto-trad|
|[equipment-09-5XfTQjS0xm0TllOE.htm](equipment/equipment-09-5XfTQjS0xm0TllOE.htm)|Wyrm on the Wing|auto-trad|
|[equipment-09-6MIBHKMJmD8uQVam.htm](equipment/equipment-09-6MIBHKMJmD8uQVam.htm)|Chain of the Stilled Spirit|auto-trad|
|[equipment-09-6xDxnI4Z9syXq8Ec.htm](equipment/equipment-09-6xDxnI4Z9syXq8Ec.htm)|Bravery Baldric (Haste)|auto-trad|
|[equipment-09-aaua3vcUXyBFpRe9.htm](equipment/equipment-09-aaua3vcUXyBFpRe9.htm)|Soaring Wings|auto-trad|
|[equipment-09-aR0tsZ3xi1iwr53u.htm](equipment/equipment-09-aR0tsZ3xi1iwr53u.htm)|Shoony Shovel|auto-trad|
|[equipment-09-B5ZfLr6Ox816FBEc.htm](equipment/equipment-09-B5ZfLr6Ox816FBEc.htm)|Crown of the Kobold King|auto-trad|
|[equipment-09-bdbBRucjvg0eYazY.htm](equipment/equipment-09-bdbBRucjvg0eYazY.htm)|Helm of Underwater Action|auto-trad|
|[equipment-09-bJORQsO9E1JCJh6i.htm](equipment/equipment-09-bJORQsO9E1JCJh6i.htm)|Extending|auto-trad|
|[equipment-09-BR2vQLRif2Pgq5eN.htm](equipment/equipment-09-BR2vQLRif2Pgq5eN.htm)|Magnifying Scope (Greater)|auto-trad|
|[equipment-09-bSm0Hki8N2L50OZw.htm](equipment/equipment-09-bSm0Hki8N2L50OZw.htm)|Shadow (Greater)|auto-trad|
|[equipment-09-BWxouItL1sfGTIUh.htm](equipment/equipment-09-BWxouItL1sfGTIUh.htm)|Pontoon|auto-trad|
|[equipment-09-byKAxLyN7AXySmw7.htm](equipment/equipment-09-byKAxLyN7AXySmw7.htm)|Lock (Good)|auto-trad|
|[equipment-09-cO7ANYLkcmfCn9c9.htm](equipment/equipment-09-cO7ANYLkcmfCn9c9.htm)|Collar of Empathy|auto-trad|
|[equipment-09-dJoYIbM6GF6wgX0b.htm](equipment/equipment-09-dJoYIbM6GF6wgX0b.htm)|Gourd Home|auto-trad|
|[equipment-09-dLTN4FU3qBoDZ5CJ.htm](equipment/equipment-09-dLTN4FU3qBoDZ5CJ.htm)|Eyes of the Eagle|auto-trad|
|[equipment-09-dolBtAdB5lpQpQpp.htm](equipment/equipment-09-dolBtAdB5lpQpQpp.htm)|Ventriloquist's Ring (Greater)|auto-trad|
|[equipment-09-dRVDcmkaB2p8zPcs.htm](equipment/equipment-09-dRVDcmkaB2p8zPcs.htm)|Stole of Civility|auto-trad|
|[equipment-09-e4lHxftAOoTmGXuG.htm](equipment/equipment-09-e4lHxftAOoTmGXuG.htm)|Unmemorable Mantle (Greater)|auto-trad|
|[equipment-09-E5gjsRSO6SFFlKpB.htm](equipment/equipment-09-E5gjsRSO6SFFlKpB.htm)|Underwater firing Mechanism|auto-trad|
|[equipment-09-ferPzbD9Qsta71wK.htm](equipment/equipment-09-ferPzbD9Qsta71wK.htm)|Energy Robe of Acid|auto-trad|
|[equipment-09-G4luLo1IEtDben39.htm](equipment/equipment-09-G4luLo1IEtDben39.htm)|Thousand-Blade Thesis|auto-trad|
|[equipment-09-GwkgqHAYRrDXSW2E.htm](equipment/equipment-09-GwkgqHAYRrDXSW2E.htm)|Nemesis Name|auto-trad|
|[equipment-09-h96LHhdZhVYnjcYA.htm](equipment/equipment-09-h96LHhdZhVYnjcYA.htm)|Key to the Stomach|auto-trad|
|[equipment-09-HAtj6AGCIZHpD7Nl.htm](equipment/equipment-09-HAtj6AGCIZHpD7Nl.htm)|Messenger's Ring|auto-trad|
|[equipment-09-Hd1AfC08ytBg67Ey.htm](equipment/equipment-09-Hd1AfC08ytBg67Ey.htm)|Hat of the Magi (Greater)|auto-trad|
|[equipment-09-idw9HdfR4QvseXsc.htm](equipment/equipment-09-idw9HdfR4QvseXsc.htm)|Cursed Dreamstone|auto-trad|
|[equipment-09-ImedbomHGVFtE3D7.htm](equipment/equipment-09-ImedbomHGVFtE3D7.htm)|Tradecraft Tattoo|auto-trad|
|[equipment-09-ioLWDzXp2dG7ZMHf.htm](equipment/equipment-09-ioLWDzXp2dG7ZMHf.htm)|Stanching (Greater)|auto-trad|
|[equipment-09-j392AMbPfkcRbCw6.htm](equipment/equipment-09-j392AMbPfkcRbCw6.htm)|Sanguine Fang|auto-trad|
|[equipment-09-JhDhFq2JGBGR2lwb.htm](equipment/equipment-09-JhDhFq2JGBGR2lwb.htm)|Elemental Wayfinder (Fire)|auto-trad|
|[equipment-09-JHq8r0fGo1rWF8NS.htm](equipment/equipment-09-JHq8r0fGo1rWF8NS.htm)|Linguist's Dictionary|auto-trad|
|[equipment-09-KLWjINVkWwJlOZEX.htm](equipment/equipment-09-KLWjINVkWwJlOZEX.htm)|Triton's Conch|auto-trad|
|[equipment-09-kXK9szN2gcYoPSdN.htm](equipment/equipment-09-kXK9szN2gcYoPSdN.htm)|Printing Press|auto-trad|
|[equipment-09-kZKK4Va3e7n3p3tv.htm](equipment/equipment-09-kZKK4Va3e7n3p3tv.htm)|Earthsight Box|auto-trad|
|[equipment-09-lkIzsqI7PcymcTEQ.htm](equipment/equipment-09-lkIzsqI7PcymcTEQ.htm)|Hunter's Hagbook|auto-trad|
|[equipment-09-lRdrk7Dh9eVlXFHi.htm](equipment/equipment-09-lRdrk7Dh9eVlXFHi.htm)|Dreamstone|auto-trad|
|[equipment-09-LV8nFuV3NiNYlxpo.htm](equipment/equipment-09-LV8nFuV3NiNYlxpo.htm)|Thunderblast Slippers|auto-trad|
|[equipment-09-M1fRjN7E3P55zvmc.htm](equipment/equipment-09-M1fRjN7E3P55zvmc.htm)|Inspiring Spotlight|auto-trad|
|[equipment-09-MBF9bZCwkJp2RZoN.htm](equipment/equipment-09-MBF9bZCwkJp2RZoN.htm)|Bountiful Cauldron|auto-trad|
|[equipment-09-MjW8mZrCUw1lEhGP.htm](equipment/equipment-09-MjW8mZrCUw1lEhGP.htm)|Shard of the Third Seal|auto-trad|
|[equipment-09-MtSUaIZ1gThCrNNj.htm](equipment/equipment-09-MtSUaIZ1gThCrNNj.htm)|Amphibious Chair|auto-trad|
|[equipment-09-nGZPhGpCxM8U1JXv.htm](equipment/equipment-09-nGZPhGpCxM8U1JXv.htm)|Bitter|auto-trad|
|[equipment-09-NjyKPRuq9siQe7gu.htm](equipment/equipment-09-NjyKPRuq9siQe7gu.htm)|Wraithweave Patch (Type I)|auto-trad|
|[equipment-09-NwyyVc5G6zUbcXRs.htm](equipment/equipment-09-NwyyVc5G6zUbcXRs.htm)|Tome of Restorative Cleansing (Moderate)|auto-trad|
|[equipment-09-nx5Y4GybdZqf9rIf.htm](equipment/equipment-09-nx5Y4GybdZqf9rIf.htm)|Saurian Spike|auto-trad|
|[equipment-09-nza9skYTNtJe2Wd3.htm](equipment/equipment-09-nza9skYTNtJe2Wd3.htm)|Lucky Keepsake|auto-trad|
|[equipment-09-O6deRhRldwTqMP97.htm](equipment/equipment-09-O6deRhRldwTqMP97.htm)|Diviner's Nose Chain|auto-trad|
|[equipment-09-ogTmuTUt52b7x5Yu.htm](equipment/equipment-09-ogTmuTUt52b7x5Yu.htm)|Wand of Thundering Echoes (3rd-Level Spell)|auto-trad|
|[equipment-09-ojbsJu6Uq269xx3U.htm](equipment/equipment-09-ojbsJu6Uq269xx3U.htm)|Silent Bell|auto-trad|
|[equipment-09-OYeYYJ4i66VtGY3O.htm](equipment/equipment-09-OYeYYJ4i66VtGY3O.htm)|Tracker's Goggles (Greater)|auto-trad|
|[equipment-09-P5nasaE0JgvkZyZp.htm](equipment/equipment-09-P5nasaE0JgvkZyZp.htm)|Bracers of Missile Deflection (Greater)|auto-trad|
|[equipment-09-p8kyiYyXDapAUyfE.htm](equipment/equipment-09-p8kyiYyXDapAUyfE.htm)|Portable Ram (Reinforced)|auto-trad|
|[equipment-09-PDEhZSibXvQdVsSq.htm](equipment/equipment-09-PDEhZSibXvQdVsSq.htm)|Oath of the Devoted|auto-trad|
|[equipment-09-PMmJ47MLUXAWmXFg.htm](equipment/equipment-09-PMmJ47MLUXAWmXFg.htm)|Elemental Wayfinder (Earth)|auto-trad|
|[equipment-09-pnUZKqJilb3aEVz5.htm](equipment/equipment-09-pnUZKqJilb3aEVz5.htm)|Anchor of Aquatic Exploration|auto-trad|
|[equipment-09-pPl2b7fqfB6dyQwf.htm](equipment/equipment-09-pPl2b7fqfB6dyQwf.htm)|Phylactery of Faithfulness|auto-trad|
|[equipment-09-QJ1PhbtbLzwhRlY0.htm](equipment/equipment-09-QJ1PhbtbLzwhRlY0.htm)|Dancing Scarf (Greater)|auto-trad|
|[equipment-09-R88HWv9rw1VNMRer.htm](equipment/equipment-09-R88HWv9rw1VNMRer.htm)|Wand of Continuation (3rd-Level Spell)|auto-trad|
|[equipment-09-sls3pfkhgCbW723f.htm](equipment/equipment-09-sls3pfkhgCbW723f.htm)|Coyote Cloak (Greater)|auto-trad|
|[equipment-09-SMBPvsZH7yAsGSQa.htm](equipment/equipment-09-SMBPvsZH7yAsGSQa.htm)|Elemental Wayfinder (Air)|auto-trad|
|[equipment-09-t6SSQYruLsDWj5Tl.htm](equipment/equipment-09-t6SSQYruLsDWj5Tl.htm)|Crushing (Greater)|auto-trad|
|[equipment-09-t8U01IpeOyKv4nDN.htm](equipment/equipment-09-t8U01IpeOyKv4nDN.htm)|Librarian's Baton|auto-trad|
|[equipment-09-tO5zXmyoBsa0lIDd.htm](equipment/equipment-09-tO5zXmyoBsa0lIDd.htm)|Bound Guardian|auto-trad|
|[equipment-09-tr2eOlJMmBKBZtI9.htm](equipment/equipment-09-tr2eOlJMmBKBZtI9.htm)|Mask of the Banshee|auto-trad|
|[equipment-09-TuPCnRFdvOboUzx7.htm](equipment/equipment-09-TuPCnRFdvOboUzx7.htm)|Thrasher Tail|auto-trad|
|[equipment-09-TYdFnzhhTlpUdtty.htm](equipment/equipment-09-TYdFnzhhTlpUdtty.htm)|Starless Scope|auto-trad|
|[equipment-09-uSwylNGP5We9uUw4.htm](equipment/equipment-09-uSwylNGP5We9uUw4.htm)|Smogger|auto-trad|
|[equipment-09-uzNB9EVGujen0cnC.htm](equipment/equipment-09-uzNB9EVGujen0cnC.htm)|Wrestler's Armbands|auto-trad|
|[equipment-09-VYXAdLJdF0XSeX5m.htm](equipment/equipment-09-VYXAdLJdF0XSeX5m.htm)|Portable|auto-trad|
|[equipment-09-WAMDYE9tt3W8obzr.htm](equipment/equipment-09-WAMDYE9tt3W8obzr.htm)|Horn of Blasting|auto-trad|
|[equipment-09-We11d1zelZ1wskvN.htm](equipment/equipment-09-We11d1zelZ1wskvN.htm)|Paired (Greater)|auto-trad|
|[equipment-09-Wf5KDIF4gauHQmy2.htm](equipment/equipment-09-Wf5KDIF4gauHQmy2.htm)|Cresset of Grisly Interrogation|auto-trad|
|[equipment-09-wI4Tj8bNwpZHequC.htm](equipment/equipment-09-wI4Tj8bNwpZHequC.htm)|Armbands of Athleticism|auto-trad|
|[equipment-09-WR8GOQSx7B5AlzNu.htm](equipment/equipment-09-WR8GOQSx7B5AlzNu.htm)|Handcuffs (Good)|auto-trad|
|[equipment-09-xe6I5BIYHE7SXSax.htm](equipment/equipment-09-xe6I5BIYHE7SXSax.htm)|Magnetite Scope|auto-trad|
|[equipment-09-xldy59c3WDGYlAYs.htm](equipment/equipment-09-xldy59c3WDGYlAYs.htm)|Banner of the Restful|auto-trad|
|[equipment-09-xnnVMQZl4i2NuYVe.htm](equipment/equipment-09-xnnVMQZl4i2NuYVe.htm)|Ring of Bestial Friendship|auto-trad|
|[equipment-09-XQFEjwQUCXl68Pva.htm](equipment/equipment-09-XQFEjwQUCXl68Pva.htm)|Taleteller's Ring|auto-trad|
|[equipment-09-xtSMGL3f9c5ZHUAm.htm](equipment/equipment-09-xtSMGL3f9c5ZHUAm.htm)|Violin of the Waves|auto-trad|
|[equipment-09-XWWosdDg34KZhrEi.htm](equipment/equipment-09-XWWosdDg34KZhrEi.htm)|Dreadsmoke Thurible|auto-trad|
|[equipment-09-xzN8mFG2Z70SaXLa.htm](equipment/equipment-09-xzN8mFG2Z70SaXLa.htm)|Cloak of Repute (Greater)|auto-trad|
|[equipment-09-y9RUbQec9zGeqqcE.htm](equipment/equipment-09-y9RUbQec9zGeqqcE.htm)|Coating|auto-trad|
|[equipment-09-yAqGhT0GuZrkvWZl.htm](equipment/equipment-09-yAqGhT0GuZrkvWZl.htm)|Crimson Fulcrum Lens|auto-trad|
|[equipment-09-YC6kyZGOKZmONbVM.htm](equipment/equipment-09-YC6kyZGOKZmONbVM.htm)|Anointed Waterskin|auto-trad|
|[equipment-09-YCjVrQnHfOtpmjYW.htm](equipment/equipment-09-YCjVrQnHfOtpmjYW.htm)|Belt of the Five Kings|auto-trad|
|[equipment-09-yhw1mr4oDhzMQc3D.htm](equipment/equipment-09-yhw1mr4oDhzMQc3D.htm)|Heartstone|auto-trad|
|[equipment-09-ySZgsSBBVf3uv8Nx.htm](equipment/equipment-09-ySZgsSBBVf3uv8Nx.htm)|Eye Slash (Major)|auto-trad|
|[equipment-09-YXTrUT7Z7emAn2gb.htm](equipment/equipment-09-YXTrUT7Z7emAn2gb.htm)|Wondrous Figurine (Ruby Hippopotamus)|auto-trad|
|[equipment-09-z8nKK4rSUGQVT2t9.htm](equipment/equipment-09-z8nKK4rSUGQVT2t9.htm)|Swarming|auto-trad|
|[equipment-09-Z8RZfkIrsifYeWg4.htm](equipment/equipment-09-Z8RZfkIrsifYeWg4.htm)|Camouflage Suit (Superb)|auto-trad|
|[equipment-09-ZxOoQ4cHfJXXbP9F.htm](equipment/equipment-09-ZxOoQ4cHfJXXbP9F.htm)|Beastmaster's Sigil (Major)|auto-trad|
|[equipment-10-0Oqf8wHY8PYJQQTx.htm](equipment/equipment-10-0Oqf8wHY8PYJQQTx.htm)|Trackless (Greater)|auto-trad|
|[equipment-10-1T1TJB929nC7wBtC.htm](equipment/equipment-10-1T1TJB929nC7wBtC.htm)|Shadow Signet|auto-trad|
|[equipment-10-2idzJUvuy8Zu6y78.htm](equipment/equipment-10-2idzJUvuy8Zu6y78.htm)|Galvanic Mortal Coil|auto-trad|
|[equipment-10-2tSDgHfSkkaX4CA4.htm](equipment/equipment-10-2tSDgHfSkkaX4CA4.htm)|Ring of Wizardry (Type II)|auto-trad|
|[equipment-10-3b0wleCdaPw3c9UX.htm](equipment/equipment-10-3b0wleCdaPw3c9UX.htm)|Sun Sight|auto-trad|
|[equipment-10-3GnTrwAfD00zW2cO.htm](equipment/equipment-10-3GnTrwAfD00zW2cO.htm)|Stone Circle|auto-trad|
|[equipment-10-3M3ZDW3MdfGLTZMC.htm](equipment/equipment-10-3M3ZDW3MdfGLTZMC.htm)|Warcaller's Chime of Dread|auto-trad|
|[equipment-10-3mprh9aZ670HfNhT.htm](equipment/equipment-10-3mprh9aZ670HfNhT.htm)|Maestro's Instrument (Moderate)|auto-trad|
|[equipment-10-3nv4JcajPAxDgGMb.htm](equipment/equipment-10-3nv4JcajPAxDgGMb.htm)|Grail of Twisted Desires|auto-trad|
|[equipment-10-4hsPZ6rBpLKOlDjm.htm](equipment/equipment-10-4hsPZ6rBpLKOlDjm.htm)|Wand of Legerdemain (4th-level)|auto-trad|
|[equipment-10-5SKGqxahAGsJgzRF.htm](equipment/equipment-10-5SKGqxahAGsJgzRF.htm)|Aeon Stone (Pearlescent Pyramid)|auto-trad|
|[equipment-10-64mxKuxc9k98FkUi.htm](equipment/equipment-10-64mxKuxc9k98FkUi.htm)|Fire-Jump Ring|auto-trad|
|[equipment-10-6jRXUXzYKIpm2uNp.htm](equipment/equipment-10-6jRXUXzYKIpm2uNp.htm)|Clandestine Cloak (Greater)|auto-trad|
|[equipment-10-6MswPHUwvqPLtN5H.htm](equipment/equipment-10-6MswPHUwvqPLtN5H.htm)|Specialist's Ring (Evocation)|auto-trad|
|[equipment-10-7dkdtY9cTXM3gMon.htm](equipment/equipment-10-7dkdtY9cTXM3gMon.htm)|Phantasmal Doorknob (Greater)|auto-trad|
|[equipment-10-7q3B8wvZFltHTpPo.htm](equipment/equipment-10-7q3B8wvZFltHTpPo.htm)|Ring of the Tiger (Greater)|auto-trad|
|[equipment-10-80HjjtcTF6DS1w30.htm](equipment/equipment-10-80HjjtcTF6DS1w30.htm)|Wand of Dumbfounding Doom (4th-level)|auto-trad|
|[equipment-10-8E7qXnNOSm51UwsG.htm](equipment/equipment-10-8E7qXnNOSm51UwsG.htm)|Miter of Communion|auto-trad|
|[equipment-10-8usnglqucr4Q0YO6.htm](equipment/equipment-10-8usnglqucr4Q0YO6.htm)|Midday Lantern (Moderate)|auto-trad|
|[equipment-10-9ftc8XfloerPcJnI.htm](equipment/equipment-10-9ftc8XfloerPcJnI.htm)|Wand of Hopeless Night (4th-Level Spell)|auto-trad|
|[equipment-10-9Y86NM6nt2WtYBOy.htm](equipment/equipment-10-9Y86NM6nt2WtYBOy.htm)|Explorer's Yurt|auto-trad|
|[equipment-10-a4RIB8GPOliFoN31.htm](equipment/equipment-10-a4RIB8GPOliFoN31.htm)|Instinct Crown (Dragon)|auto-trad|
|[equipment-10-AK4rHlYOHkcboCnq.htm](equipment/equipment-10-AK4rHlYOHkcboCnq.htm)|Voice From The Grave|auto-trad|
|[equipment-10-aKoMqPDmVzPI7Q20.htm](equipment/equipment-10-aKoMqPDmVzPI7Q20.htm)|Winged Boots|auto-trad|
|[equipment-10-BKFuMxi0dXu7yFxe.htm](equipment/equipment-10-BKFuMxi0dXu7yFxe.htm)|Ring of Truth|auto-trad|
|[equipment-10-BNShQdbxiPMisV60.htm](equipment/equipment-10-BNShQdbxiPMisV60.htm)|Poisonous Cloak Type II|auto-trad|
|[equipment-10-br4eClrcGeLTL6Ba.htm](equipment/equipment-10-br4eClrcGeLTL6Ba.htm)|Choker of Elocution (Greater)|auto-trad|
|[equipment-10-bU731I8njNmSAQhQ.htm](equipment/equipment-10-bU731I8njNmSAQhQ.htm)|Aluum Charm|auto-trad|
|[equipment-10-bxz885LMjLCkpDq3.htm](equipment/equipment-10-bxz885LMjLCkpDq3.htm)|Invisibility (Greater)|auto-trad|
|[equipment-10-CA4W0Dt4qeg8MLHA.htm](equipment/equipment-10-CA4W0Dt4qeg8MLHA.htm)|Energy Robe of Electricity|auto-trad|
|[equipment-10-cBKrXbxM02MlycpV.htm](equipment/equipment-10-cBKrXbxM02MlycpV.htm)|Ring of Counterspells|auto-trad|
|[equipment-10-cH5uD83e3hC9MEvZ.htm](equipment/equipment-10-cH5uD83e3hC9MEvZ.htm)|Wand of Dazzling Rays (4th-level)|auto-trad|
|[equipment-10-CmGfVmuUH1VGISPr.htm](equipment/equipment-10-CmGfVmuUH1VGISPr.htm)|Ghastly Cauldron|auto-trad|
|[equipment-10-d8wTwHiA09HP45IM.htm](equipment/equipment-10-d8wTwHiA09HP45IM.htm)|Golem Stylus|auto-trad|
|[equipment-10-Deyo6pk3OXthFx9Q.htm](equipment/equipment-10-Deyo6pk3OXthFx9Q.htm)|Wand of Rolling Flames (4th-level)|auto-trad|
|[equipment-10-dipcMOtBFxtmsjkS.htm](equipment/equipment-10-dipcMOtBFxtmsjkS.htm)|Cloak of the Bat|auto-trad|
|[equipment-10-divPto5fH5FdrV1V.htm](equipment/equipment-10-divPto5fH5FdrV1V.htm)|Scope of Limning|auto-trad|
|[equipment-10-eoI3M6FXtcPWeg7i.htm](equipment/equipment-10-eoI3M6FXtcPWeg7i.htm)|Barding of the Zephyr|auto-trad|
|[equipment-10-FNNfdpu1SfZtLohQ.htm](equipment/equipment-10-FNNfdpu1SfZtLohQ.htm)|Star Chart Tattoo|auto-trad|
|[equipment-10-FTTrNjLsSlrPvkLi.htm](equipment/equipment-10-FTTrNjLsSlrPvkLi.htm)|Specialist's Ring (Abjuration)|auto-trad|
|[equipment-10-fuDYKpDN9RmCX7do.htm](equipment/equipment-10-fuDYKpDN9RmCX7do.htm)|Razmiri Wayfinder|auto-trad|
|[equipment-10-G8Jy1VK6JIg1hfKp.htm](equipment/equipment-10-G8Jy1VK6JIg1hfKp.htm)|Wand of Tormented Slumber|auto-trad|
|[equipment-10-geSsIAYv85MZtppg.htm](equipment/equipment-10-geSsIAYv85MZtppg.htm)|Compass of Transpositional Awareness|auto-trad|
|[equipment-10-gKlxurDeLS95zuuY.htm](equipment/equipment-10-gKlxurDeLS95zuuY.htm)|Specialist's Ring (Enchantment)|auto-trad|
|[equipment-10-GL1C4vC6mmXFm25L.htm](equipment/equipment-10-GL1C4vC6mmXFm25L.htm)|Wand of Choking Mist (Solid Fog)|auto-trad|
|[equipment-10-gtDHRMgsyERaxf2c.htm](equipment/equipment-10-gtDHRMgsyERaxf2c.htm)|Instinct Crown (Superstition)|auto-trad|
|[equipment-10-Gw9z5RUNPOjLUdRm.htm](equipment/equipment-10-Gw9z5RUNPOjLUdRm.htm)|Dragon's Breath (4th Level Spell)|auto-trad|
|[equipment-10-H44rJpMzx6JyoKoa.htm](equipment/equipment-10-H44rJpMzx6JyoKoa.htm)|Specialist's Ring (Illusion)|auto-trad|
|[equipment-10-H9qYN48voa2ZDy3i.htm](equipment/equipment-10-H9qYN48voa2ZDy3i.htm)|Impactful|auto-trad|
|[equipment-10-hA0o5qdKXcuPJVmn.htm](equipment/equipment-10-hA0o5qdKXcuPJVmn.htm)|Instinct Crown (Giant)|auto-trad|
|[equipment-10-hgWCwA6Yfqiz9tyr.htm](equipment/equipment-10-hgWCwA6Yfqiz9tyr.htm)|Wand of Chromatic Burst (4th-level)|auto-trad|
|[equipment-10-HhtZl2pr7xChKu2c.htm](equipment/equipment-10-HhtZl2pr7xChKu2c.htm)|Quenching (Greater)|auto-trad|
|[equipment-10-hpNsjNQn9Gg6N8Kh.htm](equipment/equipment-10-hpNsjNQn9Gg6N8Kh.htm)|Twilight Lantern (Moderate)|auto-trad|
|[equipment-10-htukDa8cAycwf7zr.htm](equipment/equipment-10-htukDa8cAycwf7zr.htm)|Instinct Crown (Fury)|auto-trad|
|[equipment-10-hUhH5QeSZ8B7Yg65.htm](equipment/equipment-10-hUhH5QeSZ8B7Yg65.htm)|Lost Ember|auto-trad|
|[equipment-10-hvlEFx25ogf1K1C2.htm](equipment/equipment-10-hvlEFx25ogf1K1C2.htm)|Druid's Vestments|auto-trad|
|[equipment-10-HvmIoRCaQJmkwluG.htm](equipment/equipment-10-HvmIoRCaQJmkwluG.htm)|Talisman Cord|auto-trad|
|[equipment-10-HY6ZJpn5t0FDMTKM.htm](equipment/equipment-10-HY6ZJpn5t0FDMTKM.htm)|Conundrum Spectacles (Greater)|auto-trad|
|[equipment-10-IS1P6er2hLyKXAss.htm](equipment/equipment-10-IS1P6er2hLyKXAss.htm)|Emerald Fulcrum Lens|auto-trad|
|[equipment-10-isWSWeuJR2cYVdfi.htm](equipment/equipment-10-isWSWeuJR2cYVdfi.htm)|Standard of the Primeval Howl|auto-trad|
|[equipment-10-jrjwukkie7Y7wkxu.htm](equipment/equipment-10-jrjwukkie7Y7wkxu.htm)|Magnetizing|auto-trad|
|[equipment-10-Jt3q4i2FlRPZ14VV.htm](equipment/equipment-10-Jt3q4i2FlRPZ14VV.htm)|Specialist's Ring (Transmutation)|auto-trad|
|[equipment-10-jx1a74ytWBN2ylD9.htm](equipment/equipment-10-jx1a74ytWBN2ylD9.htm)|Wand of Hybrid Form (4th-level)|auto-trad|
|[equipment-10-kkFVSqaefWrVs50t.htm](equipment/equipment-10-kkFVSqaefWrVs50t.htm)|Menacing (Greater)|auto-trad|
|[equipment-10-mmdnWrQsh7vDspLK.htm](equipment/equipment-10-mmdnWrQsh7vDspLK.htm)|Daredevil Boots|auto-trad|
|[equipment-10-Mwwgn9WVH9WbrAT0.htm](equipment/equipment-10-Mwwgn9WVH9WbrAT0.htm)|Presentable (Greater)|auto-trad|
|[equipment-10-MxS9ifx1gr1PKxBl.htm](equipment/equipment-10-MxS9ifx1gr1PKxBl.htm)|Instinct Crown (Spirit)|auto-trad|
|[equipment-10-nE12I5dTvJFPr9at.htm](equipment/equipment-10-nE12I5dTvJFPr9at.htm)|Specialist's Ring (Conjuration)|auto-trad|
|[equipment-10-nowjVXddPXr6fMnR.htm](equipment/equipment-10-nowjVXddPXr6fMnR.htm)|Arctic Vigor|auto-trad|
|[equipment-10-ntpPBKRcihHyER5V.htm](equipment/equipment-10-ntpPBKRcihHyER5V.htm)|Wand of Mental Purification (4th-level)|auto-trad|
|[equipment-10-NYehHw93LSgGG9vD.htm](equipment/equipment-10-NYehHw93LSgGG9vD.htm)|Tooth and Claw Tattoo (Major)|auto-trad|
|[equipment-10-nyLV5YVh90kYGaBd.htm](equipment/equipment-10-nyLV5YVh90kYGaBd.htm)|Specialist's Ring (Divination)|auto-trad|
|[equipment-10-ooaErd0DHZYju9rF.htm](equipment/equipment-10-ooaErd0DHZYju9rF.htm)|Wand of Hawthorn (4th-level)|auto-trad|
|[equipment-10-OTeONq4r10Xm6gSy.htm](equipment/equipment-10-OTeONq4r10Xm6gSy.htm)|Ring of Lies|auto-trad|
|[equipment-10-oyjDCHscjcLhzall.htm](equipment/equipment-10-oyjDCHscjcLhzall.htm)|Wand of Crackling Lightning (4th-Level Spell)|auto-trad|
|[equipment-10-PjCowzEMV2aJnHkV.htm](equipment/equipment-10-PjCowzEMV2aJnHkV.htm)|Specialist's Ring (Necromancy)|auto-trad|
|[equipment-10-PMCk6NCDvJs1MBcg.htm](equipment/equipment-10-PMCk6NCDvJs1MBcg.htm)|Codebreaker's Parchment (Greater)|auto-trad|
|[equipment-10-pOoEiuEuITm4I2Il.htm](equipment/equipment-10-pOoEiuEuITm4I2Il.htm)|Weapon Potency (+2)|auto-trad|
|[equipment-10-PUeRq04dCRkYRx5s.htm](equipment/equipment-10-PUeRq04dCRkYRx5s.htm)|Poison Concentrator (Moderate)|auto-trad|
|[equipment-10-QbCraR5KFlkIJiVA.htm](equipment/equipment-10-QbCraR5KFlkIJiVA.htm)|Instinct Crown (Animal)|auto-trad|
|[equipment-10-QEPx1fCf74xdpXBH.htm](equipment/equipment-10-QEPx1fCf74xdpXBH.htm)|Thurible of Revelation (Moderate)|auto-trad|
|[equipment-10-QsRmhxeuf9v4h3Dp.htm](equipment/equipment-10-QsRmhxeuf9v4h3Dp.htm)|Immovable Tripod|auto-trad|
|[equipment-10-QT1jTPlf3ATYUh4I.htm](equipment/equipment-10-QT1jTPlf3ATYUh4I.htm)|The Whispering Reeds|auto-trad|
|[equipment-10-QZliTmapMRrEJ6q8.htm](equipment/equipment-10-QZliTmapMRrEJ6q8.htm)|Elemental Wayfinder (Water)|auto-trad|
|[equipment-10-rqwmqnCZ0xz7vF1b.htm](equipment/equipment-10-rqwmqnCZ0xz7vF1b.htm)|Dinosaur Boots|auto-trad|
|[equipment-10-RtQwbPiJs2FRyfNE.htm](equipment/equipment-10-RtQwbPiJs2FRyfNE.htm)|Specialist's Ring|auto-trad|
|[equipment-10-RTxne78VqPqTz2VN.htm](equipment/equipment-10-RTxne78VqPqTz2VN.htm)|Wondrous Figurine (Golden Lions)|auto-trad|
|[equipment-10-shKPtYN0YUOe07K2.htm](equipment/equipment-10-shKPtYN0YUOe07K2.htm)|Demon Mask (Greater)|auto-trad|
|[equipment-10-SuKERInt6Z3I3bCa.htm](equipment/equipment-10-SuKERInt6Z3I3bCa.htm)|Anchoring|auto-trad|
|[equipment-10-SV7W0lC2d8mfYuhy.htm](equipment/equipment-10-SV7W0lC2d8mfYuhy.htm)|Serrating|auto-trad|
|[equipment-10-t28PWYfoGoj1Sq3e.htm](equipment/equipment-10-t28PWYfoGoj1Sq3e.htm)|True Name Amulet (Moderate)|auto-trad|
|[equipment-10-tIZ3Wi2JInY9abu8.htm](equipment/equipment-10-tIZ3Wi2JInY9abu8.htm)|Exploration Lens (Detecting)|auto-trad|
|[equipment-10-trKFKb5BKa0kxuTJ.htm](equipment/equipment-10-trKFKb5BKa0kxuTJ.htm)|Enigma Mirror (Greater)|auto-trad|
|[equipment-10-U5MKmx60jAX4xV8x.htm](equipment/equipment-10-U5MKmx60jAX4xV8x.htm)|Book of Lingering Blaze|auto-trad|
|[equipment-10-uiJAR3jQbQHhiP3Q.htm](equipment/equipment-10-uiJAR3jQbQHhiP3Q.htm)|Cape of the Mountebank|auto-trad|
|[equipment-10-uiTJWd8IJevpkjbq.htm](equipment/equipment-10-uiTJWd8IJevpkjbq.htm)|Palanquin of Night|auto-trad|
|[equipment-10-VPjzEstF31PFSnyG.htm](equipment/equipment-10-VPjzEstF31PFSnyG.htm)|Open Mind|auto-trad|
|[equipment-10-vuvRsWXrzHbs0tF5.htm](equipment/equipment-10-vuvRsWXrzHbs0tF5.htm)|Replacement Filter (Level 10)|auto-trad|
|[equipment-10-vUXtEGa3eFLO5MCt.htm](equipment/equipment-10-vUXtEGa3eFLO5MCt.htm)|Retaliation (Moderate)|auto-trad|
|[equipment-10-whShgzN5YBzrXV3r.htm](equipment/equipment-10-whShgzN5YBzrXV3r.htm)|Alchemical Atomizer|auto-trad|
|[equipment-10-X3Nfa7bByYFrg1lU.htm](equipment/equipment-10-X3Nfa7bByYFrg1lU.htm)|Ring of Energy Resistance (Greater)|auto-trad|
|[equipment-10-XeiuCRFzxHA7mm0X.htm](equipment/equipment-10-XeiuCRFzxHA7mm0X.htm)|Endless Grimoire (Greater)|auto-trad|
|[equipment-10-XgKwydoro5eaIWC8.htm](equipment/equipment-10-XgKwydoro5eaIWC8.htm)|Wand of Reaching (4th-level)|auto-trad|
|[equipment-10-yjC2nrfqnKmFerHs.htm](equipment/equipment-10-yjC2nrfqnKmFerHs.htm)|Judgment Thurible|auto-trad|
|[equipment-10-yo6kdcUhqv5YyMM5.htm](equipment/equipment-10-yo6kdcUhqv5YyMM5.htm)|Blast Foot (Greater)|auto-trad|
|[equipment-10-YU9FFzOsMDdmKW7x.htm](equipment/equipment-10-YU9FFzOsMDdmKW7x.htm)|Wand of Mercy (4th-level)|auto-trad|
|[equipment-10-zAnxjSoJBggguWFP.htm](equipment/equipment-10-zAnxjSoJBggguWFP.htm)|Jyoti's Feather|auto-trad|
|[equipment-10-zYRzgETeR1Hs1ti1.htm](equipment/equipment-10-zYRzgETeR1Hs1ti1.htm)|Wand of Widening (4th-Level Spell)|auto-trad|
|[equipment-11-0fHBB9Xb4m3oAoko.htm](equipment/equipment-11-0fHBB9Xb4m3oAoko.htm)|Hexing Jar|auto-trad|
|[equipment-11-2Xpjq6eYMqvBL6eg.htm](equipment/equipment-11-2Xpjq6eYMqvBL6eg.htm)|Bewitching Bloom (Purple Iris)|auto-trad|
|[equipment-11-4Hb8tvvWFtykbZhG.htm](equipment/equipment-11-4Hb8tvvWFtykbZhG.htm)|Armor Potency (+2)|auto-trad|
|[equipment-11-4Rc4arQ7p61MCb0p.htm](equipment/equipment-11-4Rc4arQ7p61MCb0p.htm)|Armory Bracelet (Greater)|auto-trad|
|[equipment-11-5LfD4O0zakN0fw7b.htm](equipment/equipment-11-5LfD4O0zakN0fw7b.htm)|Bravery Baldric (Stoneskin)|auto-trad|
|[equipment-11-5uFUIRw4YUIn0OR7.htm](equipment/equipment-11-5uFUIRw4YUIn0OR7.htm)|Alchemist's Haversack|auto-trad|
|[equipment-11-62vH66XLehPiRwwo.htm](equipment/equipment-11-62vH66XLehPiRwwo.htm)|Cassock of Devotion|auto-trad|
|[equipment-11-65YL6nk1jIzCWutt.htm](equipment/equipment-11-65YL6nk1jIzCWutt.htm)|Anarchic|auto-trad|
|[equipment-11-6xu6dPIaUZ7edKEB.htm](equipment/equipment-11-6xu6dPIaUZ7edKEB.htm)|Axiomatic|auto-trad|
|[equipment-11-72gfEUjIcdKVJZg0.htm](equipment/equipment-11-72gfEUjIcdKVJZg0.htm)|Wardrobe Stone (Moderate)|auto-trad|
|[equipment-11-8NgA4PFFXZPZ9SSg.htm](equipment/equipment-11-8NgA4PFFXZPZ9SSg.htm)|Boots of Elvenkind (Greater)|auto-trad|
|[equipment-11-8nz3fSOOkNmNduMr.htm](equipment/equipment-11-8nz3fSOOkNmNduMr.htm)|Master Magus Ring|auto-trad|
|[equipment-11-8XZqdeZStFAM4XnH.htm](equipment/equipment-11-8XZqdeZStFAM4XnH.htm)|Alchemist Goggles (Greater)|auto-trad|
|[equipment-11-a5dEzZPuxsmTvlWS.htm](equipment/equipment-11-a5dEzZPuxsmTvlWS.htm)|Insistent Door Knocker (Greater)|auto-trad|
|[equipment-11-A7TCZXIwyxdqYSas.htm](equipment/equipment-11-A7TCZXIwyxdqYSas.htm)|Singing Bowl of the Versatile Stance|auto-trad|
|[equipment-11-AFE073UYI0mkWuUs.htm](equipment/equipment-11-AFE073UYI0mkWuUs.htm)|Skeleton Key (Greater)|auto-trad|
|[equipment-11-anZ6U7zFEAfiv3gh.htm](equipment/equipment-11-anZ6U7zFEAfiv3gh.htm)|Wand of Spiritual Warfare (4th-Level Spell)|auto-trad|
|[equipment-11-aU3vBG9EinYgYlWb.htm](equipment/equipment-11-aU3vBG9EinYgYlWb.htm)|Spirit-Singer (Incredible)|auto-trad|
|[equipment-11-bCsdAkffuk29ssUg.htm](equipment/equipment-11-bCsdAkffuk29ssUg.htm)|Wand of Continuation (4th-Level Spell)|auto-trad|
|[equipment-11-byIihT9MMpTFvyYT.htm](equipment/equipment-11-byIihT9MMpTFvyYT.htm)|Bravery Baldric (Restoration, Greater)|auto-trad|
|[equipment-11-bz052lqiBuGmD790.htm](equipment/equipment-11-bz052lqiBuGmD790.htm)|Armory Bracelet (Major)|auto-trad|
|[equipment-11-c0tITjJAX5nc2xXx.htm](equipment/equipment-11-c0tITjJAX5nc2xXx.htm)|Thoughtwhip Claw|auto-trad|
|[equipment-11-cMErkgi3GjwT0bTz.htm](equipment/equipment-11-cMErkgi3GjwT0bTz.htm)|Wand of Refracting Rays (4th-level)|auto-trad|
|[equipment-11-D2CGb29g3ZUNZNWk.htm](equipment/equipment-11-D2CGb29g3ZUNZNWk.htm)|Mask of Uncanny Breath|auto-trad|
|[equipment-11-DH0kB9Wbr5pDeunX.htm](equipment/equipment-11-DH0kB9Wbr5pDeunX.htm)|Holy|auto-trad|
|[equipment-11-efm6thOteY7PWEvg.htm](equipment/equipment-11-efm6thOteY7PWEvg.htm)|Calamity Glass|auto-trad|
|[equipment-11-EKYjF0GuuBkmMD1J.htm](equipment/equipment-11-EKYjF0GuuBkmMD1J.htm)|Accompaniment Cloak|auto-trad|
|[equipment-11-eoQRjG2RjppnbYGL.htm](equipment/equipment-11-eoQRjG2RjppnbYGL.htm)|Eye of the Wise|auto-trad|
|[equipment-11-fumxKes1z2hLN2U7.htm](equipment/equipment-11-fumxKes1z2hLN2U7.htm)|Ready (Greater)|auto-trad|
|[equipment-11-FW4vzcHuehTXnCLX.htm](equipment/equipment-11-FW4vzcHuehTXnCLX.htm)|Mind's Light Circlet|auto-trad|
|[equipment-11-fZLZGO0YLY8xYPh5.htm](equipment/equipment-11-fZLZGO0YLY8xYPh5.htm)|Bracers of Devotion|auto-trad|
|[equipment-11-FzwSBVaSTt1s4A66.htm](equipment/equipment-11-FzwSBVaSTt1s4A66.htm)|Amazing Pop-up Book|auto-trad|
|[equipment-11-GHaUZqivF30WJHS3.htm](equipment/equipment-11-GHaUZqivF30WJHS3.htm)|Brightbloom Posy (Greater)|auto-trad|
|[equipment-11-gmMrJREf4JSHd2dZ.htm](equipment/equipment-11-gmMrJREf4JSHd2dZ.htm)|Unholy|auto-trad|
|[equipment-11-gRQXLqUuP4GWfDWI.htm](equipment/equipment-11-gRQXLqUuP4GWfDWI.htm)|Doubling Rings (Greater)|auto-trad|
|[equipment-11-I2ul8Bzcth7sleow.htm](equipment/equipment-11-I2ul8Bzcth7sleow.htm)|Ochre Fulcrum Lens|auto-trad|
|[equipment-11-ibA3FykNQcoEPfPC.htm](equipment/equipment-11-ibA3FykNQcoEPfPC.htm)|Icy Disposition|auto-trad|
|[equipment-11-idAyVZxTkPSNLbnt.htm](equipment/equipment-11-idAyVZxTkPSNLbnt.htm)|Quill of Passage|auto-trad|
|[equipment-11-J4QuF3h13uFElZyX.htm](equipment/equipment-11-J4QuF3h13uFElZyX.htm)|Golden Goose|auto-trad|
|[equipment-11-JPVQ0qvTaKkTF2z4.htm](equipment/equipment-11-JPVQ0qvTaKkTF2z4.htm)|Sealing Chest (Greater)|auto-trad|
|[equipment-11-JSYtBZ7XblyAEFoV.htm](equipment/equipment-11-JSYtBZ7XblyAEFoV.htm)|Crafter's Eyepiece (Greater)|auto-trad|
|[equipment-11-JwHNSaiwwqF7VCC5.htm](equipment/equipment-11-JwHNSaiwwqF7VCC5.htm)|Fulcrum Lattice|auto-trad|
|[equipment-11-KFWi1lxXJi46VDmz.htm](equipment/equipment-11-KFWi1lxXJi46VDmz.htm)|Mirror Goggles (Moderate)|auto-trad|
|[equipment-11-klMBWHVZ5EFU4cTI.htm](equipment/equipment-11-klMBWHVZ5EFU4cTI.htm)|Vigilant Eye (Greater)|auto-trad|
|[equipment-11-KtpxwEfpbfDDEURF.htm](equipment/equipment-11-KtpxwEfpbfDDEURF.htm)|Dawnlight (Major)|auto-trad|
|[equipment-11-l68lJrjtjjkV7NiJ.htm](equipment/equipment-11-l68lJrjtjjkV7NiJ.htm)|Wand of Thundering Echoes (4th-Level Spell)|auto-trad|
|[equipment-11-M0tbJD3SEW4pClpb.htm](equipment/equipment-11-M0tbJD3SEW4pClpb.htm)|Brooch of Inspiration (Greater)|auto-trad|
|[equipment-11-mfhcGeS0L7pPZnex.htm](equipment/equipment-11-mfhcGeS0L7pPZnex.htm)|Energy-Absorbing|auto-trad|
|[equipment-11-NCE7g1U3q3RYwCY2.htm](equipment/equipment-11-NCE7g1U3q3RYwCY2.htm)|Hopeful|auto-trad|
|[equipment-11-nLvP7U50hLqz26Uy.htm](equipment/equipment-11-nLvP7U50hLqz26Uy.htm)|Ring of Maniacal Devices|auto-trad|
|[equipment-11-nxAMYJrG96yn9iBq.htm](equipment/equipment-11-nxAMYJrG96yn9iBq.htm)|Eidolon Cape|auto-trad|
|[equipment-11-o0XXVVymB8kluwLK.htm](equipment/equipment-11-o0XXVVymB8kluwLK.htm)|Hauling (Greater)|auto-trad|
|[equipment-11-OHs7L8CJMdp2UV0P.htm](equipment/equipment-11-OHs7L8CJMdp2UV0P.htm)|Holy Prayer Beads (Greater)|auto-trad|
|[equipment-11-OI20gS0SRKVAORqm.htm](equipment/equipment-11-OI20gS0SRKVAORqm.htm)|Fiendish Teleportation|auto-trad|
|[equipment-11-oxL8uExmIomunm5g.htm](equipment/equipment-11-oxL8uExmIomunm5g.htm)|Boots of Dancing|auto-trad|
|[equipment-11-pW3pdFtKZLSlsqSm.htm](equipment/equipment-11-pW3pdFtKZLSlsqSm.htm)|Kindled Tome|auto-trad|
|[equipment-11-R4yGXp42qXaWEhLQ.htm](equipment/equipment-11-R4yGXp42qXaWEhLQ.htm)|Bravery Baldric (Flight)|auto-trad|
|[equipment-11-rnhXFlynrb79chQ5.htm](equipment/equipment-11-rnhXFlynrb79chQ5.htm)|Dawnflower Beads|auto-trad|
|[equipment-11-SGbEZzm5MLSv1DDX.htm](equipment/equipment-11-SGbEZzm5MLSv1DDX.htm)|Envisioning Mask|auto-trad|
|[equipment-11-SsXS70dp2UnFCtdw.htm](equipment/equipment-11-SsXS70dp2UnFCtdw.htm)|Spectral Opera Glasses|auto-trad|
|[equipment-11-Tej30MsQo8Ek9aA6.htm](equipment/equipment-11-Tej30MsQo8Ek9aA6.htm)|Goggles of Night (Greater)|auto-trad|
|[equipment-11-u7NB1e1rUqYoQ0nz.htm](equipment/equipment-11-u7NB1e1rUqYoQ0nz.htm)|Saurian Spike (Greater)|auto-trad|
|[equipment-11-Ua9KWFlVjXW8sSXm.htm](equipment/equipment-11-Ua9KWFlVjXW8sSXm.htm)|Gamepiece Chariot|auto-trad|
|[equipment-11-UQpbEAeOUkascA4L.htm](equipment/equipment-11-UQpbEAeOUkascA4L.htm)|Ebon Marionette|auto-trad|
|[equipment-11-WYg2dZ8UK1N3rpgs.htm](equipment/equipment-11-WYg2dZ8UK1N3rpgs.htm)|Gorget of the Primal Roar|auto-trad|
|[equipment-11-XkjOK05Gw0o3iycr.htm](equipment/equipment-11-XkjOK05Gw0o3iycr.htm)|Implacable|auto-trad|
|[equipment-11-xS7hRZp6jI8jIga4.htm](equipment/equipment-11-xS7hRZp6jI8jIga4.htm)|Ki Channeling Beads|auto-trad|
|[equipment-11-Y23lqGpVYNacdUUl.htm](equipment/equipment-11-Y23lqGpVYNacdUUl.htm)|Corruption Cassock|auto-trad|
|[equipment-11-ZEf04zjwyiq1pLPj.htm](equipment/equipment-11-ZEf04zjwyiq1pLPj.htm)|Shadow Manse|auto-trad|
|[equipment-11-ZKFmyG9fIoQWIrql.htm](equipment/equipment-11-ZKFmyG9fIoQWIrql.htm)|Spectacles of Understanding (Greater)|auto-trad|
|[equipment-11-ZR3Kekd3hrWmAv6d.htm](equipment/equipment-11-ZR3Kekd3hrWmAv6d.htm)|Blazons of Shared Power (Greater)|auto-trad|
|[equipment-11-zSnvJub3XMlFes1G.htm](equipment/equipment-11-zSnvJub3XMlFes1G.htm)|Warden's Signet|auto-trad|
|[equipment-12-07Nu4AC0B5CI3zpc.htm](equipment/equipment-12-07Nu4AC0B5CI3zpc.htm)|Bloodline Robe|auto-trad|
|[equipment-12-0VtYhRt2BneBzh32.htm](equipment/equipment-12-0VtYhRt2BneBzh32.htm)|Splendid Floodlight|auto-trad|
|[equipment-12-18ztTUiUZNjuc7K1.htm](equipment/equipment-12-18ztTUiUZNjuc7K1.htm)|Trickster's Mandolin (Major)|auto-trad|
|[equipment-12-1erd18HS57aCyC6r.htm](equipment/equipment-12-1erd18HS57aCyC6r.htm)|Broom of Flying|auto-trad|
|[equipment-12-1oOalZRwSxcHewaJ.htm](equipment/equipment-12-1oOalZRwSxcHewaJ.htm)|Wraithweave Patch (Type II)|auto-trad|
|[equipment-12-2sr23ZSpplB1oBHd.htm](equipment/equipment-12-2sr23ZSpplB1oBHd.htm)|Trinity Geode (Major)|auto-trad|
|[equipment-12-3vKhyjBGjaN4rNyl.htm](equipment/equipment-12-3vKhyjBGjaN4rNyl.htm)|Rune of Sin (Wrath)|auto-trad|
|[equipment-12-4cbe0WVSHn1FxygQ.htm](equipment/equipment-12-4cbe0WVSHn1FxygQ.htm)|Dread (Moderate)|auto-trad|
|[equipment-12-6nJZGgIJIY3x0b1Q.htm](equipment/equipment-12-6nJZGgIJIY3x0b1Q.htm)|Spider Chair|auto-trad|
|[equipment-12-708LeeyJMXFio80R.htm](equipment/equipment-12-708LeeyJMXFio80R.htm)|Rune of Sin (Lust)|auto-trad|
|[equipment-12-7U16hk57BHVM5QYs.htm](equipment/equipment-12-7U16hk57BHVM5QYs.htm)|Reading Ring (Greater)|auto-trad|
|[equipment-12-8buhFcGwuaJRrp0y.htm](equipment/equipment-12-8buhFcGwuaJRrp0y.htm)|Fortification|auto-trad|
|[equipment-12-8CmzsexkT42I30Pl.htm](equipment/equipment-12-8CmzsexkT42I30Pl.htm)|Wand of Hybrid Form (5th-level)|auto-trad|
|[equipment-12-9CAWAKkZE7dr4FlJ.htm](equipment/equipment-12-9CAWAKkZE7dr4FlJ.htm)|Energy-Resistant (Greater)|auto-trad|
|[equipment-12-aGZJ6dEHTHxUdnXz.htm](equipment/equipment-12-aGZJ6dEHTHxUdnXz.htm)|Warcaller's Chime of Restoration|auto-trad|
|[equipment-12-aZ8HzjZCOJCV7ZWh.htm](equipment/equipment-12-aZ8HzjZCOJCV7ZWh.htm)|Marvelous Medicines|auto-trad|
|[equipment-12-bCOxPYXLL6uGuVCn.htm](equipment/equipment-12-bCOxPYXLL6uGuVCn.htm)|Rune of Sin (Greed)|auto-trad|
|[equipment-12-beHqICAYt9ZuyBay.htm](equipment/equipment-12-beHqICAYt9ZuyBay.htm)|Grim Sandglass (Major)|auto-trad|
|[equipment-12-ciykvIC4SFFxIfUw.htm](equipment/equipment-12-ciykvIC4SFFxIfUw.htm)|Swallow-Spike (Greater)|auto-trad|
|[equipment-12-clcqWFuj42N28oeT.htm](equipment/equipment-12-clcqWFuj42N28oeT.htm)|Wand of Mercy (5th-level)|auto-trad|
|[equipment-12-CTiLugp5ptSItEqN.htm](equipment/equipment-12-CTiLugp5ptSItEqN.htm)|Ebon Fulcrum Lens|auto-trad|
|[equipment-12-CXjHjdGVlOoXeYpr.htm](equipment/equipment-12-CXjHjdGVlOoXeYpr.htm)|Wand of Dazzling Rays (5th-level)|auto-trad|
|[equipment-12-DdqGs9aSwjSl0XyY.htm](equipment/equipment-12-DdqGs9aSwjSl0XyY.htm)|Nosoi Charm|auto-trad|
|[equipment-12-dPwRgQKEFLLDF2iB.htm](equipment/equipment-12-dPwRgQKEFLLDF2iB.htm)|Wand of Reaching (5th-level)|auto-trad|
|[equipment-12-dtQnQn4LBSIAzQCZ.htm](equipment/equipment-12-dtQnQn4LBSIAzQCZ.htm)|Alchemical Chart (Moderate)|auto-trad|
|[equipment-12-DwLaGtbBdCh2NFbT.htm](equipment/equipment-12-DwLaGtbBdCh2NFbT.htm)|Berserker's Cloak|auto-trad|
|[equipment-12-EDIbewxih3ihNdQC.htm](equipment/equipment-12-EDIbewxih3ihNdQC.htm)|Rune of Sin (Pride)|auto-trad|
|[equipment-12-ENF3Er7cJvb4oXMM.htm](equipment/equipment-12-ENF3Er7cJvb4oXMM.htm)|Old Tillimaquin|auto-trad|
|[equipment-12-f9ASlZXozFrknwd9.htm](equipment/equipment-12-f9ASlZXozFrknwd9.htm)|Rune of Sin (Envy)|auto-trad|
|[equipment-12-fdntl5OOhtYIPSBi.htm](equipment/equipment-12-fdntl5OOhtYIPSBi.htm)|Sanguine Fang (Greater)|auto-trad|
|[equipment-12-fFx6SEyZlHZtcLGX.htm](equipment/equipment-12-fFx6SEyZlHZtcLGX.htm)|Ring of Swimming|auto-trad|
|[equipment-12-FSo4D0bnDeRAURip.htm](equipment/equipment-12-FSo4D0bnDeRAURip.htm)|Ring of Stoneshifting|auto-trad|
|[equipment-12-fUzFWIPK4mdZSGyH.htm](equipment/equipment-12-fUzFWIPK4mdZSGyH.htm)|Stampede Medallion|auto-trad|
|[equipment-12-GpuzMoZYehYvZ50E.htm](equipment/equipment-12-GpuzMoZYehYvZ50E.htm)|Ring of Climbing|auto-trad|
|[equipment-12-hLVRI4JS5vsyfPoV.htm](equipment/equipment-12-hLVRI4JS5vsyfPoV.htm)|Faith Tattoo (Major)|auto-trad|
|[equipment-12-Hu9n7JNutzFarHe7.htm](equipment/equipment-12-Hu9n7JNutzFarHe7.htm)|Hat of Many Minds|auto-trad|
|[equipment-12-hwszIqJguVfoc3ZD.htm](equipment/equipment-12-hwszIqJguVfoc3ZD.htm)|Quick Runner's Shirt (Greater)|auto-trad|
|[equipment-12-J7vQowV531Sz73Gw.htm](equipment/equipment-12-J7vQowV531Sz73Gw.htm)|Aeon Stone (Black Pearl)|auto-trad|
|[equipment-12-jYQy2jFxbQL5y36L.htm](equipment/equipment-12-jYQy2jFxbQL5y36L.htm)|Cloak of Devouring Thorns|auto-trad|
|[equipment-12-kakeEbGrFPwW6Rhu.htm](equipment/equipment-12-kakeEbGrFPwW6Rhu.htm)|Pipes of Compulsion (Major)|auto-trad|
|[equipment-12-KCQRFvUgbyclvOGi.htm](equipment/equipment-12-KCQRFvUgbyclvOGi.htm)|Striking (Greater)|auto-trad|
|[equipment-12-kRemZF5AgTzHRHM6.htm](equipment/equipment-12-kRemZF5AgTzHRHM6.htm)|Wand of Rolling Flames (5th-level)|auto-trad|
|[equipment-12-kvfdd4YiO3Lyywuv.htm](equipment/equipment-12-kvfdd4YiO3Lyywuv.htm)|Dragon's Breath (5th Level Spell)|auto-trad|
|[equipment-12-LbdnZFlyLFdAE617.htm](equipment/equipment-12-LbdnZFlyLFdAE617.htm)|Brilliant|auto-trad|
|[equipment-12-lJ3BzUZkGTOCWleL.htm](equipment/equipment-12-lJ3BzUZkGTOCWleL.htm)|Aeon Stone (Pink Rhomboid)|auto-trad|
|[equipment-12-LKNsUuhm1CcUIWMh.htm](equipment/equipment-12-LKNsUuhm1CcUIWMh.htm)|Alluring Scarf (Greater)|auto-trad|
|[equipment-12-lqzVHgmpCDnOVZs3.htm](equipment/equipment-12-lqzVHgmpCDnOVZs3.htm)|Queasy Lantern (Moderate)|auto-trad|
|[equipment-12-MHvmgUSQlMAevvoF.htm](equipment/equipment-12-MHvmgUSQlMAevvoF.htm)|Spirit-Singer (Incredible Handheld)|auto-trad|
|[equipment-12-mxI7Ah8eZNmonNKj.htm](equipment/equipment-12-mxI7Ah8eZNmonNKj.htm)|Spectacles of Inquiry|auto-trad|
|[equipment-12-n8nLwFR4VFFmAny5.htm](equipment/equipment-12-n8nLwFR4VFFmAny5.htm)|Immovable|auto-trad|
|[equipment-12-NEbr7nKuiluJgBHT.htm](equipment/equipment-12-NEbr7nKuiluJgBHT.htm)|Lich Soul Cage|auto-trad|
|[equipment-12-nhyXfERJ0SNFmcb0.htm](equipment/equipment-12-nhyXfERJ0SNFmcb0.htm)|Bagpipes of Turmoil (Major)|auto-trad|
|[equipment-12-nQIIO6LIot0ISQXH.htm](equipment/equipment-12-nQIIO6LIot0ISQXH.htm)|Codex of Unimpeded Sight (Greater)|auto-trad|
|[equipment-12-nXq9pxB3RVnGb0rB.htm](equipment/equipment-12-nXq9pxB3RVnGb0rB.htm)|Drums of War (Major)|auto-trad|
|[equipment-12-o5jn0wvPaSh65p3J.htm](equipment/equipment-12-o5jn0wvPaSh65p3J.htm)|Hummingbird Wayfinder|auto-trad|
|[equipment-12-otCgznS0L14Z46rf.htm](equipment/equipment-12-otCgznS0L14Z46rf.htm)|Ring of Wizardry (Type III)|auto-trad|
|[equipment-12-pdsepgrBRgdZ4DWm.htm](equipment/equipment-12-pdsepgrBRgdZ4DWm.htm)|Wand of Legerdemain (5th-level)|auto-trad|
|[equipment-12-PxHexTD0gzPtMFj6.htm](equipment/equipment-12-PxHexTD0gzPtMFj6.htm)|Wind-up Wings (Homing)|auto-trad|
|[equipment-12-pXOPMJC9NAuwXz3w.htm](equipment/equipment-12-pXOPMJC9NAuwXz3w.htm)|Violet Ray|auto-trad|
|[equipment-12-pZhqdggB36MpDUwh.htm](equipment/equipment-12-pZhqdggB36MpDUwh.htm)|Ghost Lantern|auto-trad|
|[equipment-12-qFVPeeJ70B1u2n81.htm](equipment/equipment-12-qFVPeeJ70B1u2n81.htm)|Soul Cage|auto-trad|
|[equipment-12-qHwbdYOPZ9mwmru1.htm](equipment/equipment-12-qHwbdYOPZ9mwmru1.htm)|Crown of the Fire Eater (Greater)|auto-trad|
|[equipment-12-QqN3JHXXNveMnjnd.htm](equipment/equipment-12-QqN3JHXXNveMnjnd.htm)|Seer's Flute (Major)|auto-trad|
|[equipment-12-r81OfbBRPTXi0Fwd.htm](equipment/equipment-12-r81OfbBRPTXi0Fwd.htm)|Wyrm Claw|auto-trad|
|[equipment-12-RpsT84dZxLWHcoFk.htm](equipment/equipment-12-RpsT84dZxLWHcoFk.htm)|Exploration Lens (Investigating)|auto-trad|
|[equipment-12-rUxQi4pUbMeHrnSX.htm](equipment/equipment-12-rUxQi4pUbMeHrnSX.htm)|Cordelia's Greater Construct Key|auto-trad|
|[equipment-12-Shbg6XeBx94DXbcd.htm](equipment/equipment-12-Shbg6XeBx94DXbcd.htm)|Resonating Fork (Major)|auto-trad|
|[equipment-12-TGxZ3acyWjjTvfU9.htm](equipment/equipment-12-TGxZ3acyWjjTvfU9.htm)|Wand of Widening (5th-Level Spell)|auto-trad|
|[equipment-12-TucjFO5NQHvUUc7a.htm](equipment/equipment-12-TucjFO5NQHvUUc7a.htm)|Jolt Coil (Major)|auto-trad|
|[equipment-12-Ug0lQVTJlGKrHQgQ.htm](equipment/equipment-12-Ug0lQVTJlGKrHQgQ.htm)|Pickled Demon Tongue (Major)|auto-trad|
|[equipment-12-uz3JCjRvkE44jxMd.htm](equipment/equipment-12-uz3JCjRvkE44jxMd.htm)|Fearsome (Greater)|auto-trad|
|[equipment-12-v7YHA9UhDgsvQ47Q.htm](equipment/equipment-12-v7YHA9UhDgsvQ47Q.htm)|Ghostcaller's Planchette|auto-trad|
|[equipment-12-V8cYi1Gawe71d0rE.htm](equipment/equipment-12-V8cYi1Gawe71d0rE.htm)|Curtain Call Cloak|auto-trad|
|[equipment-12-Vjyt4f9XK3HXtGl2.htm](equipment/equipment-12-Vjyt4f9XK3HXtGl2.htm)|Rune of Sin (Gluttony)|auto-trad|
|[equipment-12-VmmlkU5qJHcTunXV.htm](equipment/equipment-12-VmmlkU5qJHcTunXV.htm)|Five-Feather Wreath (Major)|auto-trad|
|[equipment-12-wjrYnQcqimzFdswE.htm](equipment/equipment-12-wjrYnQcqimzFdswE.htm)|Linguist's Dictionary (Greater)|auto-trad|
|[equipment-12-xC4W6YAcu0rQFmgm.htm](equipment/equipment-12-xC4W6YAcu0rQFmgm.htm)|Wand of Dumbfounding Doom (5th-level)|auto-trad|
|[equipment-12-XJZTTkI3ERnu6iFf.htm](equipment/equipment-12-XJZTTkI3ERnu6iFf.htm)|Flaming Star (Major)|auto-trad|
|[equipment-12-xVQkxZJYx2tonOqO.htm](equipment/equipment-12-xVQkxZJYx2tonOqO.htm)|Wand of Mental Purification (5th-level)|auto-trad|
|[equipment-12-y8DbetZzNdng2LSi.htm](equipment/equipment-12-y8DbetZzNdng2LSi.htm)|Rune of Sin|auto-trad|
|[equipment-12-YG6HRwfMVK3jdslZ.htm](equipment/equipment-12-YG6HRwfMVK3jdslZ.htm)|Perfect Droplet (Major)|auto-trad|
|[equipment-12-Yguqipt5N29Bkz0d.htm](equipment/equipment-12-Yguqipt5N29Bkz0d.htm)|Bullhook (Greater)|auto-trad|
|[equipment-12-yI6L5ZHRxWpZmOP0.htm](equipment/equipment-12-yI6L5ZHRxWpZmOP0.htm)|Cloak of the False Foe|auto-trad|
|[equipment-12-yl0r4LYPZK7DSZuW.htm](equipment/equipment-12-yl0r4LYPZK7DSZuW.htm)|Rune of Sin (Sloth)|auto-trad|
|[equipment-12-ytGUz1nsPSsyuEsJ.htm](equipment/equipment-12-ytGUz1nsPSsyuEsJ.htm)|Entertainer's Lute (Major)|auto-trad|
|[equipment-12-Yv6WDpfxcukReBl4.htm](equipment/equipment-12-Yv6WDpfxcukReBl4.htm)|Bring Me Near|auto-trad|
|[equipment-12-ZCWhnOmij4AFaytj.htm](equipment/equipment-12-ZCWhnOmij4AFaytj.htm)|Pallette of Masterstrokes|auto-trad|
|[equipment-12-ZelCRLDI6M5IfjAI.htm](equipment/equipment-12-ZelCRLDI6M5IfjAI.htm)|Cloak of Elvenkind (Greater)|auto-trad|
|[equipment-12-zFdo0aBUw6cHEucT.htm](equipment/equipment-12-zFdo0aBUw6cHEucT.htm)|Spell Duelist's Siphon|auto-trad|
|[equipment-12-ZTlTXWcCV4FGgyOa.htm](equipment/equipment-12-ZTlTXWcCV4FGgyOa.htm)|Phantom Piano|auto-trad|
|[equipment-12-zToq18jKonWIp48U.htm](equipment/equipment-12-zToq18jKonWIp48U.htm)|Wand of Smoldering Fireballs (5th-Level Spell)|auto-trad|
|[equipment-13-0Ju6ETQiDvz9qpi5.htm](equipment/equipment-13-0Ju6ETQiDvz9qpi5.htm)|Communication Pendants|auto-trad|
|[equipment-13-1cIPXV6brhfQW1cD.htm](equipment/equipment-13-1cIPXV6brhfQW1cD.htm)|Spellbreaking (Necromancy)|auto-trad|
|[equipment-13-1IeT2jIpBxxUo1uD.htm](equipment/equipment-13-1IeT2jIpBxxUo1uD.htm)|Slates of Distant Letters|auto-trad|
|[equipment-13-1n22FbWdDNC7tLT6.htm](equipment/equipment-13-1n22FbWdDNC7tLT6.htm)|Rock-Braced|auto-trad|
|[equipment-13-2HCiVszUnIPJwvQV.htm](equipment/equipment-13-2HCiVszUnIPJwvQV.htm)|Blightburn Necklace|auto-trad|
|[equipment-13-2jUWEHkn0XmNuGCh.htm](equipment/equipment-13-2jUWEHkn0XmNuGCh.htm)|Aeon Stone (Rainbow Prism)|auto-trad|
|[equipment-13-4tBK20Atzi5AgCd4.htm](equipment/equipment-13-4tBK20Atzi5AgCd4.htm)|Sandstorm Top|auto-trad|
|[equipment-13-9PoAibWdzGehHxKT.htm](equipment/equipment-13-9PoAibWdzGehHxKT.htm)|Spellbreaking (Enchantment)|auto-trad|
|[equipment-13-adFsNoFwHnWuMgPm.htm](equipment/equipment-13-adFsNoFwHnWuMgPm.htm)|Spell Bastion|auto-trad|
|[equipment-13-ap9Bn8MLUJa8AtGj.htm](equipment/equipment-13-ap9Bn8MLUJa8AtGj.htm)|Warding Statuette (Greater)|auto-trad|
|[equipment-13-B4R0LK8bgw0Vjtf0.htm](equipment/equipment-13-B4R0LK8bgw0Vjtf0.htm)|Aeon Stone (Pale Lavender Ellipsoid)|auto-trad|
|[equipment-13-Ck5k13uTNqibLFJk.htm](equipment/equipment-13-Ck5k13uTNqibLFJk.htm)|Boots of Speed|auto-trad|
|[equipment-13-DAWaXFtevHLUJxHB.htm](equipment/equipment-13-DAWaXFtevHLUJxHB.htm)|Energy Adaptive|auto-trad|
|[equipment-13-DCPsilr8wbPXxTUv.htm](equipment/equipment-13-DCPsilr8wbPXxTUv.htm)|Dancing|auto-trad|
|[equipment-13-ds7j3D8IIyxWd2XI.htm](equipment/equipment-13-ds7j3D8IIyxWd2XI.htm)|Winged|auto-trad|
|[equipment-13-f8AI23xzAHyuU35m.htm](equipment/equipment-13-f8AI23xzAHyuU35m.htm)|Poisonous Cloak Type III|auto-trad|
|[equipment-13-fv67rtvtBZyuduD8.htm](equipment/equipment-13-fv67rtvtBZyuduD8.htm)|Spellbreaking (Conjuration)|auto-trad|
|[equipment-13-hEIqSfSNBWwEkldc.htm](equipment/equipment-13-hEIqSfSNBWwEkldc.htm)|Broken Tusk Pendant (Greater)|auto-trad|
|[equipment-13-hg3IogR8ue2IWwgS.htm](equipment/equipment-13-hg3IogR8ue2IWwgS.htm)|Keen|auto-trad|
|[equipment-13-hGreHAX3WuNwhaHU.htm](equipment/equipment-13-hGreHAX3WuNwhaHU.htm)|Wizard's Tower|auto-trad|
|[equipment-13-Jp9K5Q9t9ZiDGSaI.htm](equipment/equipment-13-Jp9K5Q9t9ZiDGSaI.htm)|Skarja's Heartstone|auto-trad|
|[equipment-13-keFeNqSR7W35aCeT.htm](equipment/equipment-13-keFeNqSR7W35aCeT.htm)|Blightburn Ward|auto-trad|
|[equipment-13-kETKToR9pbO6fAsT.htm](equipment/equipment-13-kETKToR9pbO6fAsT.htm)|Spellbreaking (Transmutation)|auto-trad|
|[equipment-13-lKMSAlp0ZoFUK4FV.htm](equipment/equipment-13-lKMSAlp0ZoFUK4FV.htm)|Eye of Fortune|auto-trad|
|[equipment-13-lWb2bzRja54wNXOH.htm](equipment/equipment-13-lWb2bzRja54wNXOH.htm)|Spellbreaking (Abjuration)|auto-trad|
|[equipment-13-mDIxIVMhbZ7voLhU.htm](equipment/equipment-13-mDIxIVMhbZ7voLhU.htm)|Clarity Goggles (Moderate)|auto-trad|
|[equipment-13-mp20ggzn4790lalQ.htm](equipment/equipment-13-mp20ggzn4790lalQ.htm)|Helm of Underwater Action (Greater)|auto-trad|
|[equipment-13-ONzSIJgJV27SHIMH.htm](equipment/equipment-13-ONzSIJgJV27SHIMH.htm)|Vigilant Eye (Major)|auto-trad|
|[equipment-13-oSltacqbeotmzLNJ.htm](equipment/equipment-13-oSltacqbeotmzLNJ.htm)|Ring of the Ram (Greater)|auto-trad|
|[equipment-13-p6tS5mzfGkQJLUDp.htm](equipment/equipment-13-p6tS5mzfGkQJLUDp.htm)|Rime Crystal (Major)|auto-trad|
|[equipment-13-payq4TwkN2BRF6fs.htm](equipment/equipment-13-payq4TwkN2BRF6fs.htm)|Spell-Storing|auto-trad|
|[equipment-13-PJZje7wucRqjuBNc.htm](equipment/equipment-13-PJZje7wucRqjuBNc.htm)|Spellbreaking (Illusion)|auto-trad|
|[equipment-13-RH4e2ceNN7Jy5ioq.htm](equipment/equipment-13-RH4e2ceNN7Jy5ioq.htm)|Damaj's Gloves|auto-trad|
|[equipment-13-sFIKeflh6Jaac9FI.htm](equipment/equipment-13-sFIKeflh6Jaac9FI.htm)|Wand of Thundering Echoes (5th-Level Spell)|auto-trad|
|[equipment-13-sRpA9sGZe1P0EERB.htm](equipment/equipment-13-sRpA9sGZe1P0EERB.htm)|Homeward Wayfinder|auto-trad|
|[equipment-13-TAHcYiBzvg8F1AwY.htm](equipment/equipment-13-TAHcYiBzvg8F1AwY.htm)|Cube of Force|auto-trad|
|[equipment-13-tDEi3zLVpxwA74qz.htm](equipment/equipment-13-tDEi3zLVpxwA74qz.htm)|Wand of Continuation (5th-Level Spell)|auto-trad|
|[equipment-13-titA5C0yWJqO7coB.htm](equipment/equipment-13-titA5C0yWJqO7coB.htm)|Spellbreaking (Divination)|auto-trad|
|[equipment-13-TO36RKFoiPWJtuTA.htm](equipment/equipment-13-TO36RKFoiPWJtuTA.htm)|Wyrm on the Wing (Greater)|auto-trad|
|[equipment-13-TXPQfQriNJqMYcfp.htm](equipment/equipment-13-TXPQfQriNJqMYcfp.htm)|Wondrous Figurine (Marble Elephant)|auto-trad|
|[equipment-13-UASUklwtsoAVeK3O.htm](equipment/equipment-13-UASUklwtsoAVeK3O.htm)|Spellbreaking (Evocation)|auto-trad|
|[equipment-13-uQNw11Osk2pwx6d8.htm](equipment/equipment-13-uQNw11Osk2pwx6d8.htm)|Horned Hand Rests (Greater)|auto-trad|
|[equipment-13-vBgElXswf6xBx4k9.htm](equipment/equipment-13-vBgElXswf6xBx4k9.htm)|Hellfire Boots|auto-trad|
|[equipment-13-VqT8sHEf2gcY6arL.htm](equipment/equipment-13-VqT8sHEf2gcY6arL.htm)|Paired (Major)|auto-trad|
|[equipment-13-WC0MDw9oLnAtLmh5.htm](equipment/equipment-13-WC0MDw9oLnAtLmh5.htm)|Desolation Locket (Greater)|auto-trad|
|[equipment-13-WHwprq9Xym2DOr2x.htm](equipment/equipment-13-WHwprq9Xym2DOr2x.htm)|Extending (Greater)|auto-trad|
|[equipment-13-XPkhjxSh6tBWk50f.htm](equipment/equipment-13-XPkhjxSh6tBWk50f.htm)|Horn of the Sun Aurochs|auto-trad|
|[equipment-13-y6if8e8OB3RUywF8.htm](equipment/equipment-13-y6if8e8OB3RUywF8.htm)|Wand of Manifold Missiles (5th-Level Spell)|auto-trad|
|[equipment-13-YoPUrPBjITTTgMKS.htm](equipment/equipment-13-YoPUrPBjITTTgMKS.htm)|Scope of Truth|auto-trad|
|[equipment-13-yzgMOspvPTLDe6Ln.htm](equipment/equipment-13-yzgMOspvPTLDe6Ln.htm)|Ivory Baton|auto-trad|
|[equipment-13-Zo3uztXAa7kEUQmC.htm](equipment/equipment-13-Zo3uztXAa7kEUQmC.htm)|Enigma Mirror (Major)|auto-trad|
|[equipment-13-ZTdRDRew1B0zTGiU.htm](equipment/equipment-13-ZTdRDRew1B0zTGiU.htm)|Stanching (Major)|auto-trad|
|[equipment-14-0wcZzk6ZgvxRBsBx.htm](equipment/equipment-14-0wcZzk6ZgvxRBsBx.htm)|Communion Mat|auto-trad|
|[equipment-14-1SdtzFpjKIwuQ7Nh.htm](equipment/equipment-14-1SdtzFpjKIwuQ7Nh.htm)|Wand of Crackling Lightning (6th-Level Spell)|auto-trad|
|[equipment-14-31knfVD7lEd8BPrQ.htm](equipment/equipment-14-31knfVD7lEd8BPrQ.htm)|Boots of Bounding (Greater)|auto-trad|
|[equipment-14-3rlu75EjB2SVAuOI.htm](equipment/equipment-14-3rlu75EjB2SVAuOI.htm)|Quenching (Major)|auto-trad|
|[equipment-14-4yUa5JicrhnHUggC.htm](equipment/equipment-14-4yUa5JicrhnHUggC.htm)|Undead Compendium|auto-trad|
|[equipment-14-7CCGLWYzsRuodCMM.htm](equipment/equipment-14-7CCGLWYzsRuodCMM.htm)|Eye of the Unseen (Greater)|auto-trad|
|[equipment-14-8anSflDImoYUv2UK.htm](equipment/equipment-14-8anSflDImoYUv2UK.htm)|Illuminated Folio|auto-trad|
|[equipment-14-8jPsriZqWY1hAgob.htm](equipment/equipment-14-8jPsriZqWY1hAgob.htm)|Ring of Wizardry (Type IV)|auto-trad|
|[equipment-14-8mhSUxEvNuXDP8Ki.htm](equipment/equipment-14-8mhSUxEvNuXDP8Ki.htm)|Bracers of Armor II|auto-trad|
|[equipment-14-CJtn848AL7Q0Lxf2.htm](equipment/equipment-14-CJtn848AL7Q0Lxf2.htm)|Soaring|auto-trad|
|[equipment-14-dn53uqBi6MXg2gIM.htm](equipment/equipment-14-dn53uqBi6MXg2gIM.htm)|Wand of Legerdemain (6th-level)|auto-trad|
|[equipment-14-fCdcyCkGlmp0c34A.htm](equipment/equipment-14-fCdcyCkGlmp0c34A.htm)|Resilient (Greater)|auto-trad|
|[equipment-14-gEenEoxLjL5z28rG.htm](equipment/equipment-14-gEenEoxLjL5z28rG.htm)|Rod of Negation|auto-trad|
|[equipment-14-GtSPUCDpIMUEq9B0.htm](equipment/equipment-14-GtSPUCDpIMUEq9B0.htm)|Jyoti's Feather (Greater)|auto-trad|
|[equipment-14-gvMmQh9icawtnmlR.htm](equipment/equipment-14-gvMmQh9icawtnmlR.htm)|Midday Lantern (Greater)|auto-trad|
|[equipment-14-HZDkF6MlNQ6yYobD.htm](equipment/equipment-14-HZDkF6MlNQ6yYobD.htm)|Primeval Mistletoe (Greater)|auto-trad|
|[equipment-14-iwC6V10pTd6Sk0ML.htm](equipment/equipment-14-iwC6V10pTd6Sk0ML.htm)|Sealing Chest (Major)|auto-trad|
|[equipment-14-JDQ4jqp6O8SurQGe.htm](equipment/equipment-14-JDQ4jqp6O8SurQGe.htm)|Wand of Widening (6th-Level Spell)|auto-trad|
|[equipment-14-JzGuXGSjsfWDMu7P.htm](equipment/equipment-14-JzGuXGSjsfWDMu7P.htm)|Wand of the Ash Puppet|auto-trad|
|[equipment-14-KyIYS1RDED6gL33N.htm](equipment/equipment-14-KyIYS1RDED6gL33N.htm)|Memory Palace|auto-trad|
|[equipment-14-m9v1YKUvQuFGVZ10.htm](equipment/equipment-14-m9v1YKUvQuFGVZ10.htm)|Tome of Restorative Cleansing (Greater)|auto-trad|
|[equipment-14-MjiZtQW7QBLFbq7W.htm](equipment/equipment-14-MjiZtQW7QBLFbq7W.htm)|Wand of Mercy (6th-level)|auto-trad|
|[equipment-14-NAQ7HcM826y9fsBH.htm](equipment/equipment-14-NAQ7HcM826y9fsBH.htm)|Exploration Lens (Searching)|auto-trad|
|[equipment-14-oCwTZg3JuwLSA7Yj.htm](equipment/equipment-14-oCwTZg3JuwLSA7Yj.htm)|Soaring Wings (Greater)|auto-trad|
|[equipment-14-OmiMLervOVa8G8zK.htm](equipment/equipment-14-OmiMLervOVa8G8zK.htm)|Twilight Lantern (Greater)|auto-trad|
|[equipment-14-OMoKdkWjmT59LCJo.htm](equipment/equipment-14-OMoKdkWjmT59LCJo.htm)|Tattletale Orb (Clear Quartz)|auto-trad|
|[equipment-14-oVrVzML63VFvVfKk.htm](equipment/equipment-14-oVrVzML63VFvVfKk.htm)|Disrupting (Greater)|auto-trad|
|[equipment-14-OzChAzd0UR37bEOR.htm](equipment/equipment-14-OzChAzd0UR37bEOR.htm)|Wand of Dazzling Rays (6th-level)|auto-trad|
|[equipment-14-pCr0zPdJoXZW3I6y.htm](equipment/equipment-14-pCr0zPdJoXZW3I6y.htm)|Wand of Reaching (6th-level)|auto-trad|
|[equipment-14-pRgnCoqOntDqZchB.htm](equipment/equipment-14-pRgnCoqOntDqZchB.htm)|Retaliation (Greater)|auto-trad|
|[equipment-14-RM3SR1omNZfppZGl.htm](equipment/equipment-14-RM3SR1omNZfppZGl.htm)|Crystal Ball (Clear Quartz)|auto-trad|
|[equipment-14-SNxXDIoD0XE9WVQ7.htm](equipment/equipment-14-SNxXDIoD0XE9WVQ7.htm)|Dragon's Breath (6th Level Spell)|auto-trad|
|[equipment-14-t0pzdb54KD2JQcEa.htm](equipment/equipment-14-t0pzdb54KD2JQcEa.htm)|Horseshoes of Speed (Greater)|auto-trad|
|[equipment-14-tFhNIL308HpiDFtS.htm](equipment/equipment-14-tFhNIL308HpiDFtS.htm)|Wand of Hybrid Form (6th-level)|auto-trad|
|[equipment-14-tI2bx8U4Ibwq25ya.htm](equipment/equipment-14-tI2bx8U4Ibwq25ya.htm)|Wand of Mental Purification (6th-level)|auto-trad|
|[equipment-14-uo6fVL0r0S1vM61n.htm](equipment/equipment-14-uo6fVL0r0S1vM61n.htm)|Wand of Hawthorn (6th-level)|auto-trad|
|[equipment-14-ut83Grf73Z8ZTaV1.htm](equipment/equipment-14-ut83Grf73Z8ZTaV1.htm)|Wand of the Snowfields (5th-Level Spell)|auto-trad|
|[equipment-14-W6QXdPS9d0eoBAuw.htm](equipment/equipment-14-W6QXdPS9d0eoBAuw.htm)|Wand of Dumbfounding Doom (6th-level)|auto-trad|
|[equipment-14-wbCw7hNyQZSuy4QL.htm](equipment/equipment-14-wbCw7hNyQZSuy4QL.htm)|Wind-Catcher (Greater)|auto-trad|
|[equipment-14-WT9Yh0UNBHBlLNsJ.htm](equipment/equipment-14-WT9Yh0UNBHBlLNsJ.htm)|Endless Grimoire (Major)|auto-trad|
|[equipment-14-X0pjLMXjW8RGr8pg.htm](equipment/equipment-14-X0pjLMXjW8RGr8pg.htm)|Fortune's Coin (Platinum)|auto-trad|
|[equipment-14-z5NBeuQezm3ABup2.htm](equipment/equipment-14-z5NBeuQezm3ABup2.htm)|Wand of Rolling Flames (6th-level)|auto-trad|
|[equipment-14-zyAzx6fLsPurRFQO.htm](equipment/equipment-14-zyAzx6fLsPurRFQO.htm)|Ring of Energy Resistance (Major)|auto-trad|
|[equipment-15-0Tu55QMhTXAijFvl.htm](equipment/equipment-15-0Tu55QMhTXAijFvl.htm)|Saurian Spike (Major)|auto-trad|
|[equipment-15-35rLqxDWgiDIkL8e.htm](equipment/equipment-15-35rLqxDWgiDIkL8e.htm)|Wand of Continuation (6th-Level Spell)|auto-trad|
|[equipment-15-3pKIEBtZBVmSmVPl.htm](equipment/equipment-15-3pKIEBtZBVmSmVPl.htm)|Earthglide Cloak|auto-trad|
|[equipment-15-4LnRc8lZjprPPQ0z.htm](equipment/equipment-15-4LnRc8lZjprPPQ0z.htm)|Oculus of Abaddon|auto-trad|
|[equipment-15-7AdpzgqC9wCVyeoe.htm](equipment/equipment-15-7AdpzgqC9wCVyeoe.htm)|Wand of Toxic Blades (6th-level)|auto-trad|
|[equipment-15-7cOczTK4yQkuK5J9.htm](equipment/equipment-15-7cOczTK4yQkuK5J9.htm)|Aeon Stone (Mottled Ellipsoid)|auto-trad|
|[equipment-15-7YkaMoB3YfYRZ21G.htm](equipment/equipment-15-7YkaMoB3YfYRZ21G.htm)|Unfathomable Stargazer|auto-trad|
|[equipment-15-9BXiGLvOFxDBmQrQ.htm](equipment/equipment-15-9BXiGLvOFxDBmQrQ.htm)|Phantasmal Doorknob (Major)|auto-trad|
|[equipment-15-9IckeffVxmxX0cwV.htm](equipment/equipment-15-9IckeffVxmxX0cwV.htm)|Book of Lost Days|auto-trad|
|[equipment-15-bjjp7rXqikby6NU5.htm](equipment/equipment-15-bjjp7rXqikby6NU5.htm)|Wand of Spiritual Warfare (6th-Level Spell)|auto-trad|
|[equipment-15-bTLmJMATrrtq8NuT.htm](equipment/equipment-15-bTLmJMATrrtq8NuT.htm)|Dragonscale Amulet|auto-trad|
|[equipment-15-cEXLUGa9qTh8Y6kX.htm](equipment/equipment-15-cEXLUGa9qTh8Y6kX.htm)|Thunderblast Slippers (Greater)|auto-trad|
|[equipment-15-cIYjYb9e5ieQmHo7.htm](equipment/equipment-15-cIYjYb9e5ieQmHo7.htm)|Wand of Refracting Rays (7th-level)|auto-trad|
|[equipment-15-CxadMTEjnuXyqmcQ.htm](equipment/equipment-15-CxadMTEjnuXyqmcQ.htm)|Giant-Killing (Greater)|auto-trad|
|[equipment-15-dABsOA9Qiy0nkGgV.htm](equipment/equipment-15-dABsOA9Qiy0nkGgV.htm)|True Name Amulet (Greater)|auto-trad|
|[equipment-15-dgwgVWUvXGurnsQD.htm](equipment/equipment-15-dgwgVWUvXGurnsQD.htm)|Tattletale Orb (Selenite)|auto-trad|
|[equipment-15-EcdwnJMuYRqQ9uh7.htm](equipment/equipment-15-EcdwnJMuYRqQ9uh7.htm)|Bewitching Bloom (Amaranth)|auto-trad|
|[equipment-15-FbboejktKOMCm181.htm](equipment/equipment-15-FbboejktKOMCm181.htm)|Lini's Leafstick|auto-trad|
|[equipment-15-FQnK379odYh4Tors.htm](equipment/equipment-15-FQnK379odYh4Tors.htm)|Folding Boat (Greater)|auto-trad|
|[equipment-15-hEnITJ2bJVPrqfGG.htm](equipment/equipment-15-hEnITJ2bJVPrqfGG.htm)|Lightweave Scarf (Greater)|auto-trad|
|[equipment-15-iPgSLZ44804wrXQC.htm](equipment/equipment-15-iPgSLZ44804wrXQC.htm)|Darkvision Scope (Greater)|auto-trad|
|[equipment-15-Lb7F2BR9X9TF1vjX.htm](equipment/equipment-15-Lb7F2BR9X9TF1vjX.htm)|Thundering (Greater)|auto-trad|
|[equipment-15-lSpoAPC5VeGcSoDG.htm](equipment/equipment-15-lSpoAPC5VeGcSoDG.htm)|Wyrm Claw (Greater)|auto-trad|
|[equipment-15-MNaLwkkyKx86NKKq.htm](equipment/equipment-15-MNaLwkkyKx86NKKq.htm)|Whistle of Calling|auto-trad|
|[equipment-15-NLF4Z7jn44Sf3RGS.htm](equipment/equipment-15-NLF4Z7jn44Sf3RGS.htm)|Necklace of Strangulation|auto-trad|
|[equipment-15-nSG5bwiZBEjj53Ya.htm](equipment/equipment-15-nSG5bwiZBEjj53Ya.htm)|Arctic Vigor (Greater)|auto-trad|
|[equipment-15-o02lg3k1RBoFXVFV.htm](equipment/equipment-15-o02lg3k1RBoFXVFV.htm)|Antimagic|auto-trad|
|[equipment-15-Oj3TuNNUT3ayL3Ea.htm](equipment/equipment-15-Oj3TuNNUT3ayL3Ea.htm)|Energy-Absorbing (Greater)|auto-trad|
|[equipment-15-oL8G6OqITPJ5Fd6A.htm](equipment/equipment-15-oL8G6OqITPJ5Fd6A.htm)|Ancestral Echoing|auto-trad|
|[equipment-15-Q4o8pEdxSY0bnJQZ.htm](equipment/equipment-15-Q4o8pEdxSY0bnJQZ.htm)|Demilich Eye Gem|auto-trad|
|[equipment-15-QbyF8jJ93SnN6NVL.htm](equipment/equipment-15-QbyF8jJ93SnN6NVL.htm)|Steamflight Pack|auto-trad|
|[equipment-15-qL1S3vGfv8Dh5yAE.htm](equipment/equipment-15-qL1S3vGfv8Dh5yAE.htm)|Fanged (Major)|auto-trad|
|[equipment-15-RnpzfRWsOW6zmAgN.htm](equipment/equipment-15-RnpzfRWsOW6zmAgN.htm)|Wondrous Figurine (Obsidian Steed)|auto-trad|
|[equipment-15-RSZwUlCzUX7Nb4UA.htm](equipment/equipment-15-RSZwUlCzUX7Nb4UA.htm)|Flaming (Greater)|auto-trad|
|[equipment-15-SemkRkQPVBljg9Id.htm](equipment/equipment-15-SemkRkQPVBljg9Id.htm)|Infernal Health|auto-trad|
|[equipment-15-Sexud7FdxIrg50vU.htm](equipment/equipment-15-Sexud7FdxIrg50vU.htm)|Frost (Greater)|auto-trad|
|[equipment-15-TEa1oKZbwsOvC6TZ.htm](equipment/equipment-15-TEa1oKZbwsOvC6TZ.htm)|Shock (Greater)|auto-trad|
|[equipment-15-tFgzn2ATau3UJ8Fd.htm](equipment/equipment-15-tFgzn2ATau3UJ8Fd.htm)|Wraithweave Patch (Type III)|auto-trad|
|[equipment-15-TieVxNRNuXsU7m7R.htm](equipment/equipment-15-TieVxNRNuXsU7m7R.htm)|Poison Concentrator (Greater)|auto-trad|
|[equipment-15-UBWWeT6u5uHD5vT9.htm](equipment/equipment-15-UBWWeT6u5uHD5vT9.htm)|Wand of Thundering Echoes (6th-Level Spell)|auto-trad|
|[equipment-15-VDMYyVQUWJAjVyru.htm](equipment/equipment-15-VDMYyVQUWJAjVyru.htm)|Crystal Ball (Selenite)|auto-trad|
|[equipment-15-vp2q2h2KoVjRs8nL.htm](equipment/equipment-15-vp2q2h2KoVjRs8nL.htm)|Stampede Medallion (Greater)|auto-trad|
|[equipment-15-vQUIUAFOTOWj3ohh.htm](equipment/equipment-15-vQUIUAFOTOWj3ohh.htm)|Corrosive (Greater)|auto-trad|
|[equipment-15-z5u7z3zBHZcCxfyX.htm](equipment/equipment-15-z5u7z3zBHZcCxfyX.htm)|Busine of Divine Reinforcements|auto-trad|
|[equipment-16-0KaC1NryNfckdS7T.htm](equipment/equipment-16-0KaC1NryNfckdS7T.htm)|Wand of Chromatic Burst (7th-level)|auto-trad|
|[equipment-16-1neYjXMc4srH7KQ0.htm](equipment/equipment-16-1neYjXMc4srH7KQ0.htm)|Advancing (Greater)|auto-trad|
|[equipment-16-2smAtaNAi2fIVTLF.htm](equipment/equipment-16-2smAtaNAi2fIVTLF.htm)|Fiddle of the Maestro|auto-trad|
|[equipment-16-3Nb9R3lIOy1tiMqv.htm](equipment/equipment-16-3Nb9R3lIOy1tiMqv.htm)|Aeon Stone (Amber Sphere)|auto-trad|
|[equipment-16-3xoboDAmMZcmvK59.htm](equipment/equipment-16-3xoboDAmMZcmvK59.htm)|Wand of Hybrid Form (7th-level)|auto-trad|
|[equipment-16-62ZOnGqCMlXCHngg.htm](equipment/equipment-16-62ZOnGqCMlXCHngg.htm)|Talisman Cord (Greater)|auto-trad|
|[equipment-16-68rHNRZmlnyaUbBF.htm](equipment/equipment-16-68rHNRZmlnyaUbBF.htm)|Misleading|auto-trad|
|[equipment-16-6epYSje4gOgYPvK0.htm](equipment/equipment-16-6epYSje4gOgYPvK0.htm)|Dinosaur Boots (Greater)|auto-trad|
|[equipment-16-9imz3VgBXCg13RfT.htm](equipment/equipment-16-9imz3VgBXCg13RfT.htm)|Slick (Major)|auto-trad|
|[equipment-16-AgDNThyJHtsp1Vjt.htm](equipment/equipment-16-AgDNThyJHtsp1Vjt.htm)|Bloodthirsty|auto-trad|
|[equipment-16-AYIel6a1nARjqygh.htm](equipment/equipment-16-AYIel6a1nARjqygh.htm)|Wand of Legerdemain (7th-level)|auto-trad|
|[equipment-16-bgCBFy3yybTYo4Ec.htm](equipment/equipment-16-bgCBFy3yybTYo4Ec.htm)|Dragon's Breath (7th Level Spell)|auto-trad|
|[equipment-16-CjfBdn0fOIarWzBc.htm](equipment/equipment-16-CjfBdn0fOIarWzBc.htm)|Wand of Slaying (7th-Level Spell)|auto-trad|
|[equipment-16-ElHAsHIzsVQZp8WW.htm](equipment/equipment-16-ElHAsHIzsVQZp8WW.htm)|Mountain to the Sky|auto-trad|
|[equipment-16-EXXqJp8rU6HR5Ufg.htm](equipment/equipment-16-EXXqJp8rU6HR5Ufg.htm)|Instant Fortress|auto-trad|
|[equipment-16-K8T5OYXjmniOvhmA.htm](equipment/equipment-16-K8T5OYXjmniOvhmA.htm)|Wand of Mercy (7th-level)|auto-trad|
|[equipment-16-kNfdGNIGzF0fW7aq.htm](equipment/equipment-16-kNfdGNIGzF0fW7aq.htm)|Wand of Widening (7th-Level Spell)|auto-trad|
|[equipment-16-KnZL0xPWDzQx9vWQ.htm](equipment/equipment-16-KnZL0xPWDzQx9vWQ.htm)|Speed|auto-trad|
|[equipment-16-kSsjGICDkNRuD7fV.htm](equipment/equipment-16-kSsjGICDkNRuD7fV.htm)|Wand of Rolling Flames (7th-level)|auto-trad|
|[equipment-16-LxaNamrRrGzJo6cL.htm](equipment/equipment-16-LxaNamrRrGzJo6cL.htm)|Pocket Gala|auto-trad|
|[equipment-16-MO8J2IcBhiTnm9D8.htm](equipment/equipment-16-MO8J2IcBhiTnm9D8.htm)|Aeon Stone (Orange Prism)|auto-trad|
|[equipment-16-OpEFPyX1jwuV7ljp.htm](equipment/equipment-16-OpEFPyX1jwuV7ljp.htm)|Wand of Clinging Rime (7th-level)|auto-trad|
|[equipment-16-oqZszBqqanONR0ng.htm](equipment/equipment-16-oqZszBqqanONR0ng.htm)|Grimoire of Unknown Necessities|auto-trad|
|[equipment-16-oRQFgqcl8L5ix8h1.htm](equipment/equipment-16-oRQFgqcl8L5ix8h1.htm)|Alluring Scarf (Major)|auto-trad|
|[equipment-16-qeLAYEwUXNbri5eB.htm](equipment/equipment-16-qeLAYEwUXNbri5eB.htm)|Wand of Reaching (7th-level)|auto-trad|
|[equipment-16-RFFUd06PQH4csvpr.htm](equipment/equipment-16-RFFUd06PQH4csvpr.htm)|Tattletale Orb (Moonstone)|auto-trad|
|[equipment-16-RRFyASbHcdclympe.htm](equipment/equipment-16-RRFyASbHcdclympe.htm)|Swallow-Spike (Major)|auto-trad|
|[equipment-16-rvItjGQaYT7Luwtm.htm](equipment/equipment-16-rvItjGQaYT7Luwtm.htm)|Weapon Potency (+3)|auto-trad|
|[equipment-16-S0IshWO7Vx29PKaq.htm](equipment/equipment-16-S0IshWO7Vx29PKaq.htm)|Crystal Ball (Moonstone)|auto-trad|
|[equipment-16-SkBZG0AO2wpuKBLT.htm](equipment/equipment-16-SkBZG0AO2wpuKBLT.htm)|Wand of Dumbfounding Doom (7th-level)|auto-trad|
|[equipment-16-TGCzwDIrAqo3HUhr.htm](equipment/equipment-16-TGCzwDIrAqo3HUhr.htm)|Guiding Chisel|auto-trad|
|[equipment-16-TPQjZzF9yioMr9Bu.htm](equipment/equipment-16-TPQjZzF9yioMr9Bu.htm)|Waffle Iron (High-grade Mithral)|auto-trad|
|[equipment-16-vfVpgUuieLdPoqY7.htm](equipment/equipment-16-vfVpgUuieLdPoqY7.htm)|Crown of the Fire Eater (Major)|auto-trad|
|[equipment-16-vVJy2xHuzOgm7e6Y.htm](equipment/equipment-16-vVJy2xHuzOgm7e6Y.htm)|Wand of Mental Purification (7th-level)|auto-trad|
|[equipment-16-xswNe3hrxy0f01QQ.htm](equipment/equipment-16-xswNe3hrxy0f01QQ.htm)|Radiant Prism|auto-trad|
|[equipment-16-y2nb4D6iQOoc5LEX.htm](equipment/equipment-16-y2nb4D6iQOoc5LEX.htm)|Miogimo's Mask|auto-trad|
|[equipment-16-y4dGIbK5wGUJTufR.htm](equipment/equipment-16-y4dGIbK5wGUJTufR.htm)|Jyoti's Feather (Major)|auto-trad|
|[equipment-16-YPBMrIvJOZ6zSZVm.htm](equipment/equipment-16-YPBMrIvJOZ6zSZVm.htm)|Wand of Dazzling Rays (7th-level)|auto-trad|
|[equipment-16-yqAVU3Y9w4O19e24.htm](equipment/equipment-16-yqAVU3Y9w4O19e24.htm)|Faith Tattoo (True)|auto-trad|
|[equipment-16-YR8IAV94fPo0kfBz.htm](equipment/equipment-16-YR8IAV94fPo0kfBz.htm)|Wand of Smoldering Fireballs (7th-Level Spell)|auto-trad|
|[equipment-16-ZjcRzYBjtPtb01MB.htm](equipment/equipment-16-ZjcRzYBjtPtb01MB.htm)|Wand of Paralytic Shock (7th-level)|auto-trad|
|[equipment-17-0yeM77XLNrB0a0LF.htm](equipment/equipment-17-0yeM77XLNrB0a0LF.htm)|Circlet of Persuasion|auto-trad|
|[equipment-17-0zBuCoOeNkIwi8bj.htm](equipment/equipment-17-0zBuCoOeNkIwi8bj.htm)|Tradecraft Tattoo (Greater)|auto-trad|
|[equipment-17-1FKDq4Gfev5GObDT.htm](equipment/equipment-17-1FKDq4Gfev5GObDT.htm)|Diadem of Intellect|auto-trad|
|[equipment-17-2FjdEflsVldnuebM.htm](equipment/equipment-17-2FjdEflsVldnuebM.htm)|Shadow (Major)|auto-trad|
|[equipment-17-3BTIKYHck3JIAPiH.htm](equipment/equipment-17-3BTIKYHck3JIAPiH.htm)|Insistent Door Knocker (Major)|auto-trad|
|[equipment-17-5zkoqnp6X5yPCXVy.htm](equipment/equipment-17-5zkoqnp6X5yPCXVy.htm)|Manacles (Superior)|auto-trad|
|[equipment-17-6xaxxKfvXED6LfIY.htm](equipment/equipment-17-6xaxxKfvXED6LfIY.htm)|Vorpal|auto-trad|
|[equipment-17-755XG3gP2MWYBXy5.htm](equipment/equipment-17-755XG3gP2MWYBXy5.htm)|Avalanche Boots|auto-trad|
|[equipment-17-7utuH8VJjKEzKtNw.htm](equipment/equipment-17-7utuH8VJjKEzKtNw.htm)|Belt of Giant Strength|auto-trad|
|[equipment-17-7uxALss5g34y49HF.htm](equipment/equipment-17-7uxALss5g34y49HF.htm)|Magnifying Scope (Major)|auto-trad|
|[equipment-17-8xWA0demi2yQsl3C.htm](equipment/equipment-17-8xWA0demi2yQsl3C.htm)|Lock (Superior)|auto-trad|
|[equipment-17-9IwSktj0Xj7A2Ruh.htm](equipment/equipment-17-9IwSktj0Xj7A2Ruh.htm)|Anklets of Alacrity|auto-trad|
|[equipment-17-A9po9KInIcQXMzyp.htm](equipment/equipment-17-A9po9KInIcQXMzyp.htm)|Wyrm on the Wing (Major)|auto-trad|
|[equipment-17-b9YHyjOL4sg7tjI4.htm](equipment/equipment-17-b9YHyjOL4sg7tjI4.htm)|Messenger's Ring (Greater)|auto-trad|
|[equipment-17-bnmfBLXOBd3ah6GK.htm](equipment/equipment-17-bnmfBLXOBd3ah6GK.htm)|Alchemist Goggles (Major)|auto-trad|
|[equipment-17-ByRyBRjNBcK5rwQ9.htm](equipment/equipment-17-ByRyBRjNBcK5rwQ9.htm)|Crystal Ball (Peridot)|auto-trad|
|[equipment-17-dLYifig01WulSNVF.htm](equipment/equipment-17-dLYifig01WulSNVF.htm)|Stanching (True)|auto-trad|
|[equipment-17-dxRKFkFeCDQLLEyT.htm](equipment/equipment-17-dxRKFkFeCDQLLEyT.htm)|Armbands of the Gorgon|auto-trad|
|[equipment-17-ffQwBOmyy2NyatnC.htm](equipment/equipment-17-ffQwBOmyy2NyatnC.htm)|Aeon Stone (Black Disc)|auto-trad|
|[equipment-17-FNccUmKsyXKmfe5c.htm](equipment/equipment-17-FNccUmKsyXKmfe5c.htm)|Armbands of Athleticism (Greater)|auto-trad|
|[equipment-17-GyDASe4CV3q3H6kh.htm](equipment/equipment-17-GyDASe4CV3q3H6kh.htm)|Eye Slash (True)|auto-trad|
|[equipment-17-H1XGrl6Z0bzXN2oi.htm](equipment/equipment-17-H1XGrl6Z0bzXN2oi.htm)|Wand of Continuation (7th-Level Spell)|auto-trad|
|[equipment-17-h7OCAvvnUnCIM9Aj.htm](equipment/equipment-17-h7OCAvvnUnCIM9Aj.htm)|Troubadour's Cap|auto-trad|
|[equipment-17-HcjEb07UjWchysx5.htm](equipment/equipment-17-HcjEb07UjWchysx5.htm)|Unmemorable Mantle (Major)|auto-trad|
|[equipment-17-hjfoSyfsGSTLpPMr.htm](equipment/equipment-17-hjfoSyfsGSTLpPMr.htm)|Headband of Inspired Wisdom|auto-trad|
|[equipment-17-hrG2w4IfF1QZhSzw.htm](equipment/equipment-17-hrG2w4IfF1QZhSzw.htm)|Phylactery of Faithfulness (Greater)|auto-trad|
|[equipment-17-ifAp8wHKBZltgHG0.htm](equipment/equipment-17-ifAp8wHKBZltgHG0.htm)|Cloak of the Bat (Greater)|auto-trad|
|[equipment-17-IxcUx7trxUdhF9DQ.htm](equipment/equipment-17-IxcUx7trxUdhF9DQ.htm)|Brooch of Inspiration (Major)|auto-trad|
|[equipment-17-JnNZjAk32269e8VO.htm](equipment/equipment-17-JnNZjAk32269e8VO.htm)|Pilferer's Gloves|auto-trad|
|[equipment-17-K2rMmiBlzcysNuj6.htm](equipment/equipment-17-K2rMmiBlzcysNuj6.htm)|Belt of Regeneration|auto-trad|
|[equipment-17-KFfM3Y8SbhdxpbQI.htm](equipment/equipment-17-KFfM3Y8SbhdxpbQI.htm)|Robe of Eyes|auto-trad|
|[equipment-17-kjFFmqci69k2zMXF.htm](equipment/equipment-17-kjFFmqci69k2zMXF.htm)|Daredevil Boots (Greater)|auto-trad|
|[equipment-17-m7oTstzUfkFfaaLc.htm](equipment/equipment-17-m7oTstzUfkFfaaLc.htm)|Wand of Toxic Blades (7th-level)|auto-trad|
|[equipment-17-mNgScZVAq037JuNb.htm](equipment/equipment-17-mNgScZVAq037JuNb.htm)|Amulet of the Third Eye|auto-trad|
|[equipment-17-NFFgDfLBkCjkj0dc.htm](equipment/equipment-17-NFFgDfLBkCjkj0dc.htm)|Cloak of Repute (Major)|auto-trad|
|[equipment-17-pDw2wi0znVb8Dysg.htm](equipment/equipment-17-pDw2wi0znVb8Dysg.htm)|Dread Blindfold|auto-trad|
|[equipment-17-pLAggVa6iDN4cpsz.htm](equipment/equipment-17-pLAggVa6iDN4cpsz.htm)|Stone of Unrivaled Skill|auto-trad|
|[equipment-17-q70WXJO1rswduHuT.htm](equipment/equipment-17-q70WXJO1rswduHuT.htm)|Ethereal|auto-trad|
|[equipment-17-ri9QkRCD6cAbQ6t3.htm](equipment/equipment-17-ri9QkRCD6cAbQ6t3.htm)|Impactful (Greater)|auto-trad|
|[equipment-17-skQIdQ0RKXGCHJGM.htm](equipment/equipment-17-skQIdQ0RKXGCHJGM.htm)|Tattletale Orb (Peridot)|auto-trad|
|[equipment-17-tajXzwAgjUBZLD6b.htm](equipment/equipment-17-tajXzwAgjUBZLD6b.htm)|Judgment Thurible (Greater)|auto-trad|
|[equipment-17-ToSrWLocAD4AffSB.htm](equipment/equipment-17-ToSrWLocAD4AffSB.htm)|Queasy Lantern (Greater)|auto-trad|
|[equipment-17-UlvlWbC7iipuENa1.htm](equipment/equipment-17-UlvlWbC7iipuENa1.htm)|Codebreaker's Parchment (Major)|auto-trad|
|[equipment-17-UvABdSnNZ9gr2IkF.htm](equipment/equipment-17-UvABdSnNZ9gr2IkF.htm)|Cauldron of Nightmares|auto-trad|
|[equipment-17-VcZ5VVYMvnQQXzX9.htm](equipment/equipment-17-VcZ5VVYMvnQQXzX9.htm)|Wand of Thundering Echoes (7th-Level Spell)|auto-trad|
|[equipment-17-VVymhIF6UBThdHP9.htm](equipment/equipment-17-VVymhIF6UBThdHP9.htm)|Artificer Spectacles|auto-trad|
|[equipment-17-WeX7rAO2kAyP0QnG.htm](equipment/equipment-17-WeX7rAO2kAyP0QnG.htm)|Wand of Manifold Missiles (7th-Level Spell)|auto-trad|
|[equipment-17-WUICjaC9LYnLRIAE.htm](equipment/equipment-17-WUICjaC9LYnLRIAE.htm)|Phantom Shroud|auto-trad|
|[equipment-17-XbQrf5aYEWweje30.htm](equipment/equipment-17-XbQrf5aYEWweje30.htm)|Poisonous Cloak Type IV|auto-trad|
|[equipment-17-XXALNl2JjJket1vr.htm](equipment/equipment-17-XXALNl2JjJket1vr.htm)|Handcuffs (Superior)|auto-trad|
|[equipment-17-YBmrRRh0jJTOZoUe.htm](equipment/equipment-17-YBmrRRh0jJTOZoUe.htm)|Magnetite Scope (Greater)|auto-trad|
|[equipment-17-zJKEDSxn2lhPamKS.htm](equipment/equipment-17-zJKEDSxn2lhPamKS.htm)|Clockwork Helm|auto-trad|
|[equipment-18-0Bp6uVAX4AvjhsOI.htm](equipment/equipment-18-0Bp6uVAX4AvjhsOI.htm)|Twilight Lantern (Major)|auto-trad|
|[equipment-18-1U382qwyTktH9h3j.htm](equipment/equipment-18-1U382qwyTktH9h3j.htm)|Wand of Wearing Dance|auto-trad|
|[equipment-18-20nQTcGzpUv8jJ6R.htm](equipment/equipment-18-20nQTcGzpUv8jJ6R.htm)|Wand of Widening (8th-Level Spell)|auto-trad|
|[equipment-18-2LO9eMNpQiku5UDn.htm](equipment/equipment-18-2LO9eMNpQiku5UDn.htm)|Taljjae's Mask (The Hero)|auto-trad|
|[equipment-18-34D6lFZ2gpZiyUU6.htm](equipment/equipment-18-34D6lFZ2gpZiyUU6.htm)|Wand of Legerdemain (8th-level)|auto-trad|
|[equipment-18-3DGLuHRME4e2XgvX.htm](equipment/equipment-18-3DGLuHRME4e2XgvX.htm)|Wand of Mercy (8th-level)|auto-trad|
|[equipment-18-4jzASWzF2riszO3U.htm](equipment/equipment-18-4jzASWzF2riszO3U.htm)|Taljjae's Mask (The Nobleman)|auto-trad|
|[equipment-18-6ELjUFY0sEJ7nZlZ.htm](equipment/equipment-18-6ELjUFY0sEJ7nZlZ.htm)|Armor Potency (+3)|auto-trad|
|[equipment-18-6PW3zAn8fWW3IYA0.htm](equipment/equipment-18-6PW3zAn8fWW3IYA0.htm)|Dread (Greater)|auto-trad|
|[equipment-18-8QValKGj7oZKnqw1.htm](equipment/equipment-18-8QValKGj7oZKnqw1.htm)|Wand of Dazzling Rays (8th-level)|auto-trad|
|[equipment-18-aQEQkwuAabZG8wY1.htm](equipment/equipment-18-aQEQkwuAabZG8wY1.htm)|Sage's Lash|auto-trad|
|[equipment-18-aR4rXlJnDg61SUBL.htm](equipment/equipment-18-aR4rXlJnDg61SUBL.htm)|Stampede Medallion (Major)|auto-trad|
|[equipment-18-at6rKBw85IkdbmOO.htm](equipment/equipment-18-at6rKBw85IkdbmOO.htm)|Sandals of the Stag|auto-trad|
|[equipment-18-atWlXr78lWJSNbws.htm](equipment/equipment-18-atWlXr78lWJSNbws.htm)|Mantle of the Grogrisant|auto-trad|
|[equipment-18-cVxHaDAouFhm0bXP.htm](equipment/equipment-18-cVxHaDAouFhm0bXP.htm)|Emberheart|auto-trad|
|[equipment-18-d8w28UjKHALM2GNu.htm](equipment/equipment-18-d8w28UjKHALM2GNu.htm)|Taljjae's Mask (The Hermit)|auto-trad|
|[equipment-18-dFG2yWRg5yNCfugI.htm](equipment/equipment-18-dFG2yWRg5yNCfugI.htm)|Mercurial Mantle|auto-trad|
|[equipment-18-dPg0hrkIaCdMKQoK.htm](equipment/equipment-18-dPg0hrkIaCdMKQoK.htm)|Taljjae's Mask (The Beast)|auto-trad|
|[equipment-18-eFGpWmM8ehW9mkI4.htm](equipment/equipment-18-eFGpWmM8ehW9mkI4.htm)|Wand of Reaching (8th-level)|auto-trad|
|[equipment-18-erP57YMboay9Ns4L.htm](equipment/equipment-18-erP57YMboay9Ns4L.htm)|Titan's Grasp|auto-trad|
|[equipment-18-FffMnXbwNHczb6bp.htm](equipment/equipment-18-FffMnXbwNHczb6bp.htm)|Maw of Hungry Shadows|auto-trad|
|[equipment-18-fw1lODFkJJk3umz0.htm](equipment/equipment-18-fw1lODFkJJk3umz0.htm)|Cape of the Open Sky|auto-trad|
|[equipment-18-He5idiYCqLJJU83h.htm](equipment/equipment-18-He5idiYCqLJJU83h.htm)|Dragon's Breath (8th Level Spell)|auto-trad|
|[equipment-18-hGZNrPMdxsabNFLx.htm](equipment/equipment-18-hGZNrPMdxsabNFLx.htm)|Quenching (True)|auto-trad|
|[equipment-18-hL6z9KnndTO2L12f.htm](equipment/equipment-18-hL6z9KnndTO2L12f.htm)|Mask of Allure|auto-trad|
|[equipment-18-JSbBiTCtFctuJFhS.htm](equipment/equipment-18-JSbBiTCtFctuJFhS.htm)|Taljjae's Mask (The General)|auto-trad|
|[equipment-18-JvIqB42cTweMtxMO.htm](equipment/equipment-18-JvIqB42cTweMtxMO.htm)|Tornado Trompo|auto-trad|
|[equipment-18-kY41VIXUSEJYEznp.htm](equipment/equipment-18-kY41VIXUSEJYEznp.htm)|Anchoring (Greater)|auto-trad|
|[equipment-18-KY9gDuSeB1tgT3cp.htm](equipment/equipment-18-KY9gDuSeB1tgT3cp.htm)|Wand of Mental Purification (8th-level)|auto-trad|
|[equipment-18-LnXYSaLxWMyAT3Ef.htm](equipment/equipment-18-LnXYSaLxWMyAT3Ef.htm)|Wand of Slaying (8th-Level Spell)|auto-trad|
|[equipment-18-ma4BlTSjaMHrQmoz.htm](equipment/equipment-18-ma4BlTSjaMHrQmoz.htm)|Taljjae's Mask (The Grandmother)|auto-trad|
|[equipment-18-MnKL30xS3FHlwdS1.htm](equipment/equipment-18-MnKL30xS3FHlwdS1.htm)|Alchemical Chart (Greater)|auto-trad|
|[equipment-18-n8MonEa4ZBdvEovc.htm](equipment/equipment-18-n8MonEa4ZBdvEovc.htm)|Brilliant (Greater)|auto-trad|
|[equipment-18-nbBtszla32wRC1bp.htm](equipment/equipment-18-nbBtszla32wRC1bp.htm)|Taljjae's Mask (The Wanderer)|auto-trad|
|[equipment-18-oJBJW19ulUBMEPEY.htm](equipment/equipment-18-oJBJW19ulUBMEPEY.htm)|Endless Grimoire (True)|auto-trad|
|[equipment-18-POKglmR5IdpEEefR.htm](equipment/equipment-18-POKglmR5IdpEEefR.htm)|Radiant Prism (Greater)|auto-trad|
|[equipment-18-q11aZe8Gc09QkP0y.htm](equipment/equipment-18-q11aZe8Gc09QkP0y.htm)|Purloining Cloak|auto-trad|
|[equipment-18-q2kE0mEUAEL3gQv0.htm](equipment/equipment-18-q2kE0mEUAEL3gQv0.htm)|Wand of the Snowfields (7th-Level Spell)|auto-trad|
|[equipment-18-QZaOQ8luuxWXpFqJ.htm](equipment/equipment-18-QZaOQ8luuxWXpFqJ.htm)|Genius Diadem|auto-trad|
|[equipment-18-rlDIbl6EQYXQpWVs.htm](equipment/equipment-18-rlDIbl6EQYXQpWVs.htm)|Maestro's Instrument (Greater)|auto-trad|
|[equipment-18-rXXNw6dwVn96giDi.htm](equipment/equipment-18-rXXNw6dwVn96giDi.htm)|Goggles of Night (Major)|auto-trad|
|[equipment-18-rZKiSwhCT5xkBVLy.htm](equipment/equipment-18-rZKiSwhCT5xkBVLy.htm)|Wand of Hawthorn (8th-level)|auto-trad|
|[equipment-18-S9IVIHLrjnRu1x3o.htm](equipment/equipment-18-S9IVIHLrjnRu1x3o.htm)|Wand of Hybrid Form (8th-level)|auto-trad|
|[equipment-18-SmMfd6rYUJ8vDGzM.htm](equipment/equipment-18-SmMfd6rYUJ8vDGzM.htm)|Wand of Rolling Flames (8th-level)|auto-trad|
|[equipment-18-U28jkj5ZDl2drtEH.htm](equipment/equipment-18-U28jkj5ZDl2drtEH.htm)|Wand of Crackling Lightning (8th-Level Spell)|auto-trad|
|[equipment-18-UC7YCJmKbuau54fS.htm](equipment/equipment-18-UC7YCJmKbuau54fS.htm)|Wand of Dumbfounding Doom (8th-level)|auto-trad|
|[equipment-18-uHUQnRGSKy655bTt.htm](equipment/equipment-18-uHUQnRGSKy655bTt.htm)|Spectacles of Piercing Sight|auto-trad|
|[equipment-18-ujWnpVMkbTljMGN9.htm](equipment/equipment-18-ujWnpVMkbTljMGN9.htm)|Fortification (Greater)|auto-trad|
|[equipment-18-Uk342C6qpSkdjHBb.htm](equipment/equipment-18-Uk342C6qpSkdjHBb.htm)|Lightweave Scarf (Major)|auto-trad|
|[equipment-18-uT8HoQ3BKI5La8Fu.htm](equipment/equipment-18-uT8HoQ3BKI5La8Fu.htm)|Mirror Goggles (Greater)|auto-trad|
|[equipment-18-utRZnCD2XI9Q1dWE.htm](equipment/equipment-18-utRZnCD2XI9Q1dWE.htm)|Ghostcaller's Planchette (Greater)|auto-trad|
|[equipment-18-V9iVPhIk980GT6A2.htm](equipment/equipment-18-V9iVPhIk980GT6A2.htm)|Inexplicable Apparatus|auto-trad|
|[equipment-18-VBCk7JXGsuG0cug1.htm](equipment/equipment-18-VBCk7JXGsuG0cug1.htm)|Thurible of Revelation (Greater)|auto-trad|
|[equipment-18-vZaXDOrp5Faxw5fS.htm](equipment/equipment-18-vZaXDOrp5Faxw5fS.htm)|Possibility Tome|auto-trad|
|[equipment-18-W0tqTUmRE5lEhWUl.htm](equipment/equipment-18-W0tqTUmRE5lEhWUl.htm)|Midday Lantern (Major)|auto-trad|
|[equipment-18-Wkm2VYPsfGjWBtQe.htm](equipment/equipment-18-Wkm2VYPsfGjWBtQe.htm)|Ring of Maniacal Devices (Greater)|auto-trad|
|[equipment-18-wYpWQCD2IYqDtqpc.htm](equipment/equipment-18-wYpWQCD2IYqDtqpc.htm)|Marvelous Medicines (Greater)|auto-trad|
|[equipment-18-x0FUkGzr6vBZk8m4.htm](equipment/equipment-18-x0FUkGzr6vBZk8m4.htm)|Nosoi Charm (Greater)|auto-trad|
|[equipment-18-XORmdC62imVhz1BD.htm](equipment/equipment-18-XORmdC62imVhz1BD.htm)|Archivist's Gaze|auto-trad|
|[equipment-18-Z8o1V947lwkhPhzI.htm](equipment/equipment-18-Z8o1V947lwkhPhzI.htm)|Wand of Clinging Rime (8th-level)|auto-trad|
|[equipment-19-0NZuvpCVr21WcQtH.htm](equipment/equipment-19-0NZuvpCVr21WcQtH.htm)|Locket of Sealed Nightmares|auto-trad|
|[equipment-19-3HR6rJT9NrIRUwe2.htm](equipment/equipment-19-3HR6rJT9NrIRUwe2.htm)|Shadowmist Cape|auto-trad|
|[equipment-19-3p4fk9pbwDeue1NG.htm](equipment/equipment-19-3p4fk9pbwDeue1NG.htm)|Desolation Locket (Major)|auto-trad|
|[equipment-19-4j5tJ27qfBN7Xd6d.htm](equipment/equipment-19-4j5tJ27qfBN7Xd6d.htm)|Tattletale Orb (Obsidian)|auto-trad|
|[equipment-19-5sMAAIymln2yIl4q.htm](equipment/equipment-19-5sMAAIymln2yIl4q.htm)|Aeon Stone (Lavender and Green Ellipsoid)|auto-trad|
|[equipment-19-7MTjAlCVVLsNFo7w.htm](equipment/equipment-19-7MTjAlCVVLsNFo7w.htm)|Third Eye|auto-trad|
|[equipment-19-adqiLRzIEHiG356b.htm](equipment/equipment-19-adqiLRzIEHiG356b.htm)|Stone Circle (Greater)|auto-trad|
|[equipment-19-aJ3mYuV0rjFBPOsg.htm](equipment/equipment-19-aJ3mYuV0rjFBPOsg.htm)|Mantle of Amazing Health|auto-trad|
|[equipment-19-b2shhButUcNimET9.htm](equipment/equipment-19-b2shhButUcNimET9.htm)|Mask of the Banshee (Greater)|auto-trad|
|[equipment-19-dR6d7wAWP9KtpWLA.htm](equipment/equipment-19-dR6d7wAWP9KtpWLA.htm)|Wand of Spiritual Warfare (8th-Level Spell)|auto-trad|
|[equipment-19-fbhRZskdWPrh4ML8.htm](equipment/equipment-19-fbhRZskdWPrh4ML8.htm)|Clarity Goggles (Greater)|auto-trad|
|[equipment-19-ISsagQkLvsgpoLxP.htm](equipment/equipment-19-ISsagQkLvsgpoLxP.htm)|Ivory Baton (Greater)|auto-trad|
|[equipment-19-KMqHzKfpPq5H8GOo.htm](equipment/equipment-19-KMqHzKfpPq5H8GOo.htm)|Wand of Continuation (8th-Level Spell)|auto-trad|
|[equipment-19-OGKI8NS8Er3qumJS.htm](equipment/equipment-19-OGKI8NS8Er3qumJS.htm)|Berserker's Cloak (Greater)|auto-trad|
|[equipment-19-ogvooWFhMgB50iXE.htm](equipment/equipment-19-ogvooWFhMgB50iXE.htm)|Umbraex Eye|auto-trad|
|[equipment-19-orChmPy2wJ6DWwHU.htm](equipment/equipment-19-orChmPy2wJ6DWwHU.htm)|Wyrm Claw (Major)|auto-trad|
|[equipment-19-pIy2L56i70OUd2HZ.htm](equipment/equipment-19-pIy2L56i70OUd2HZ.htm)|Soaring Wings (Major)|auto-trad|
|[equipment-19-qnj1999r0BXo4C31.htm](equipment/equipment-19-qnj1999r0BXo4C31.htm)|Cube of Recall|auto-trad|
|[equipment-19-s9ciKYFPOepnHpxG.htm](equipment/equipment-19-s9ciKYFPOepnHpxG.htm)|Wand of Toxic Blades (8th-level)|auto-trad|
|[equipment-19-tOYitD9BanOPovcD.htm](equipment/equipment-19-tOYitD9BanOPovcD.htm)|Poison Concentrator (Major)|auto-trad|
|[equipment-19-U1GAAGhbzX9Lynq0.htm](equipment/equipment-19-U1GAAGhbzX9Lynq0.htm)|Aeon Stone (Pale Orange Rhomboid)|auto-trad|
|[equipment-19-WKdI4LbwgcNHhMdp.htm](equipment/equipment-19-WKdI4LbwgcNHhMdp.htm)|Crystal Ball (Obsidian)|auto-trad|
|[equipment-19-woxl2FrrgAcJDu0t.htm](equipment/equipment-19-woxl2FrrgAcJDu0t.htm)|Striking (Major)|auto-trad|
|[equipment-19-XazXltJ0tsYy1xXQ.htm](equipment/equipment-19-XazXltJ0tsYy1xXQ.htm)|Brightbloom Posy (Major)|auto-trad|
|[equipment-19-XpS6X7QKtMQCVkdY.htm](equipment/equipment-19-XpS6X7QKtMQCVkdY.htm)|Retaliation (Major)|auto-trad|
|[equipment-19-Z7xfDpdf1qwkJR94.htm](equipment/equipment-19-Z7xfDpdf1qwkJR94.htm)|Wand of Thundering Echoes (8th-Level Spell)|auto-trad|
|[equipment-19-Ztb4xv4UGZbF32TE.htm](equipment/equipment-19-Ztb4xv4UGZbF32TE.htm)|Winged (Greater)|auto-trad|
|[equipment-20-1JHPSOUD7Ij75OAf.htm](equipment/equipment-20-1JHPSOUD7Ij75OAf.htm)|Vial of the Immortal Wellspring|auto-trad|
|[equipment-20-1wrRaLsT6nuz8gj6.htm](equipment/equipment-20-1wrRaLsT6nuz8gj6.htm)|Dullahan Codex|auto-trad|
|[equipment-20-4BDf1LVyQyNyOwka.htm](equipment/equipment-20-4BDf1LVyQyNyOwka.htm)|Beguiling Crown|auto-trad|
|[equipment-20-4PtxcrCPnBhhYmg4.htm](equipment/equipment-20-4PtxcrCPnBhhYmg4.htm)|Judgment Thurible (Major)|auto-trad|
|[equipment-20-5Q5MfHX43Sjm8C6Z.htm](equipment/equipment-20-5Q5MfHX43Sjm8C6Z.htm)|Final Blade|auto-trad|
|[equipment-20-8htAYiRxpAmVn9uK.htm](equipment/equipment-20-8htAYiRxpAmVn9uK.htm)|Ring Of Recalcitrant Wishes|auto-trad|
|[equipment-20-9o8DDJMWoaZiC5N2.htm](equipment/equipment-20-9o8DDJMWoaZiC5N2.htm)|Drum of Upheaval|auto-trad|
|[equipment-20-a2yYccCg6ZWvJkdo.htm](equipment/equipment-20-a2yYccCg6ZWvJkdo.htm)|Soulspark Candle|auto-trad|
|[equipment-20-AVBhZdhq6qCp96Se.htm](equipment/equipment-20-AVBhZdhq6qCp96Se.htm)|Codex of Destruction and Renewal|auto-trad|
|[equipment-20-cFJUxi5kCcBV6lz1.htm](equipment/equipment-20-cFJUxi5kCcBV6lz1.htm)|Dragon's Breath (9th Level Spell)|auto-trad|
|[equipment-20-ckJkldBWN8n1Hq2a.htm](equipment/equipment-20-ckJkldBWN8n1Hq2a.htm)|Lesser Cube of Nex|auto-trad|
|[equipment-20-D9OGy69XaLBwjdd1.htm](equipment/equipment-20-D9OGy69XaLBwjdd1.htm)|True Name Amulet (Major)|auto-trad|
|[equipment-20-dAPTUxg2NZBwzQ9T.htm](equipment/equipment-20-dAPTUxg2NZBwzQ9T.htm)|Laurel of the Empath|auto-trad|
|[equipment-20-FagkWG1cXRucHcQ6.htm](equipment/equipment-20-FagkWG1cXRucHcQ6.htm)|Wand of Dumbfounding Doom (9th-level)|auto-trad|
|[equipment-20-gbDVj8XRLN9OVVjL.htm](equipment/equipment-20-gbDVj8XRLN9OVVjL.htm)|Wardrobe Stone (Greater)|auto-trad|
|[equipment-20-gswNvLPc2D0n0AOB.htm](equipment/equipment-20-gswNvLPc2D0n0AOB.htm)|Phoenix Necklace|auto-trad|
|[equipment-20-gv5IRHrmGoSu7Dzv.htm](equipment/equipment-20-gv5IRHrmGoSu7Dzv.htm)|Wand of Dazzling Rays (9th-level)|auto-trad|
|[equipment-20-h2aVryWbNP24gC05.htm](equipment/equipment-20-h2aVryWbNP24gC05.htm)|Planar Ribbon|auto-trad|
|[equipment-20-HuGZspUvJqR09Y8u.htm](equipment/equipment-20-HuGZspUvJqR09Y8u.htm)|Rod of Cancellation|auto-trad|
|[equipment-20-imBDGoQJnvfWYuLC.htm](equipment/equipment-20-imBDGoQJnvfWYuLC.htm)|Talisman of the Sphere|auto-trad|
|[equipment-20-Ivqd84dkWA8DW8YJ.htm](equipment/equipment-20-Ivqd84dkWA8DW8YJ.htm)|Ring of Spell Turning|auto-trad|
|[equipment-20-LSf18TMRtHV6SMs9.htm](equipment/equipment-20-LSf18TMRtHV6SMs9.htm)|Wand of Mercy (9th-level)|auto-trad|
|[equipment-20-MBMiRXM0xC4aS6Xj.htm](equipment/equipment-20-MBMiRXM0xC4aS6Xj.htm)|Wand of Rolling Flames (9th-level)|auto-trad|
|[equipment-20-mMZWdoHvP2yYJzrR.htm](equipment/equipment-20-mMZWdoHvP2yYJzrR.htm)|Wand of Slaying (9th-Level Spell)|auto-trad|
|[equipment-20-P112tFaNhgJrBhmu.htm](equipment/equipment-20-P112tFaNhgJrBhmu.htm)|Wand of Clinging Rime (9th-level)|auto-trad|
|[equipment-20-qoNaajuoAnKRrFyb.htm](equipment/equipment-20-qoNaajuoAnKRrFyb.htm)|Wand of Legerdemain (9th-level)|auto-trad|
|[equipment-20-QWiXrqhSCkvdHbsi.htm](equipment/equipment-20-QWiXrqhSCkvdHbsi.htm)|Monkey's Paw|auto-trad|
|[equipment-20-rqJzQawe3CbXiWnG.htm](equipment/equipment-20-rqJzQawe3CbXiWnG.htm)|Bracers of Armor III|auto-trad|
|[equipment-20-sa9UGUMWYiZkTPjA.htm](equipment/equipment-20-sa9UGUMWYiZkTPjA.htm)|Wand of Reaching (9th-level)|auto-trad|
|[equipment-20-sKyJDfHdKacfbNOG.htm](equipment/equipment-20-sKyJDfHdKacfbNOG.htm)|Whisper of the First Lie|auto-trad|
|[equipment-20-t5978mZ6CqfUDCP6.htm](equipment/equipment-20-t5978mZ6CqfUDCP6.htm)|Wand of Widening (9th-Level Spell)|auto-trad|
|[equipment-20-tBwMPimZ6A93XpHf.htm](equipment/equipment-20-tBwMPimZ6A93XpHf.htm)|Wand of Smoldering Fireballs (9th-Level Spell)|auto-trad|
|[equipment-20-tV0vr2oVVl9fWyD9.htm](equipment/equipment-20-tV0vr2oVVl9fWyD9.htm)|Radiant Prism (Major)|auto-trad|
|[equipment-20-uU4VC8OlhDHslT4i.htm](equipment/equipment-20-uU4VC8OlhDHslT4i.htm)|Impossible|auto-trad|
|[equipment-20-v8Dx5ABBpPFafIgy.htm](equipment/equipment-20-v8Dx5ABBpPFafIgy.htm)|Wand of Hybrid Form (9th-level)|auto-trad|
|[equipment-20-vhhjN70J1j3RfECJ.htm](equipment/equipment-20-vhhjN70J1j3RfECJ.htm)|Silhouette Cloak|auto-trad|
|[equipment-20-WKcvvaZ0LxwYreb7.htm](equipment/equipment-20-WKcvvaZ0LxwYreb7.htm)|Resilient (Major)|auto-trad|
|[equipment-20-wUBJ6osUKvNnOtft.htm](equipment/equipment-20-wUBJ6osUKvNnOtft.htm)|Golden Rod Memento|auto-trad|
|[equipment-20-Y12bRscT79LCpz4k.htm](equipment/equipment-20-Y12bRscT79LCpz4k.htm)|Gauntlight|auto-trad|
|[equipment-20-YBWZ1Qexhe6PuiuO.htm](equipment/equipment-20-YBWZ1Qexhe6PuiuO.htm)|Wand of Mental Purification (9th-level)|auto-trad|
|[equipment-20-YiitcoNxawdhTO1e.htm](equipment/equipment-20-YiitcoNxawdhTO1e.htm)|Void Mirror|auto-trad|
|[equipment-21-9SmQvhTWvk7kuvJl.htm](equipment/equipment-21-9SmQvhTWvk7kuvJl.htm)|Forgotten Signet|auto-trad|
|[equipment-21-CGcNXv33k83Gr5lf.htm](equipment/equipment-21-CGcNXv33k83Gr5lf.htm)|Halcyon Heart|auto-trad|
|[equipment-21-JM5qwlv2bi7Zgm7n.htm](equipment/equipment-21-JM5qwlv2bi7Zgm7n.htm)|Jahan Waystone|auto-trad|
|[equipment-22-BSIdE57Zuvv5iX93.htm](equipment/equipment-22-BSIdE57Zuvv5iX93.htm)|Perfected Robes|auto-trad|
|[equipment-22-Gyi4IVrAVJRPJF2s.htm](equipment/equipment-22-Gyi4IVrAVJRPJF2s.htm)|Deck of Many Things|auto-trad|
|[equipment-22-jqaSdm5zp8STSW27.htm](equipment/equipment-22-jqaSdm5zp8STSW27.htm)|Shadewither Key|auto-trad|
|[equipment-22-kOanU8MQt6KFmRy0.htm](equipment/equipment-22-kOanU8MQt6KFmRy0.htm)|Shadowed Scale, the Jungle Secret|auto-trad|
|[equipment-23-6o6zvitwlNHNaNJM.htm](equipment/equipment-23-6o6zvitwlNHNaNJM.htm)|Shot of the First Vault|auto-trad|
|[equipment-23-8q9ylEH525g6XqFC.htm](equipment/equipment-23-8q9ylEH525g6XqFC.htm)|Primordial Flame|auto-trad|
|[equipment-23-Ub1GoxOzuTR1l1r4.htm](equipment/equipment-23-Ub1GoxOzuTR1l1r4.htm)|Redsand Hourglass|auto-trad|
|[equipment-24-jpQcKMmP1I5674P7.htm](equipment/equipment-24-jpQcKMmP1I5674P7.htm)|Forgefather's Seal|auto-trad|
|[equipment-24-oPZ2s4KmwaNXiUcc.htm](equipment/equipment-24-oPZ2s4KmwaNXiUcc.htm)|Starfaring Cloak|auto-trad|
|[equipment-25-1kLz3leyfccLcAMK.htm](equipment/equipment-25-1kLz3leyfccLcAMK.htm)|Cube of Nex|auto-trad|
|[equipment-25-1ZOTwnqA9ccfdrey.htm](equipment/equipment-25-1ZOTwnqA9ccfdrey.htm)|Aroden's Hearthstone|auto-trad|
|[equipment-25-2VpuNz8EyXpvUf7P.htm](equipment/equipment-25-2VpuNz8EyXpvUf7P.htm)|Tears of the Last Azlanti|auto-trad|
|[equipment-25-9Ptn7yy2QeM8taU8.htm](equipment/equipment-25-9Ptn7yy2QeM8taU8.htm)|Flawed Orb of Gold Dragonkind|auto-trad|
|[equipment-25-D836jcTlNR4PHgXS.htm](equipment/equipment-25-D836jcTlNR4PHgXS.htm)|Radiant Spark|auto-trad|
|[equipment-25-KEICt6Tusa3JdTE8.htm](equipment/equipment-25-KEICt6Tusa3JdTE8.htm)|Elder Sign|auto-trad|
|[equipment-25-L8OButuVM3PFxgrZ.htm](equipment/equipment-25-L8OButuVM3PFxgrZ.htm)|Orb of Dragonkind|auto-trad|
|[equipment-25-YeSWRltYzLYBUJbN.htm](equipment/equipment-25-YeSWRltYzLYBUJbN.htm)|Passage Pane|auto-trad|
|[equipment-25-Ywt7p5Fyx18lK8km.htm](equipment/equipment-25-Ywt7p5Fyx18lK8km.htm)|Mirror of Sorshen|auto-trad|
|[equipment-26-SHzZSENlkz3Qy46j.htm](equipment/equipment-26-SHzZSENlkz3Qy46j.htm)|Horns of Naraga|auto-trad|
|[equipment-27-B4DnQNcGl6nFVKHl.htm](equipment/equipment-27-B4DnQNcGl6nFVKHl.htm)|Sphere of Annihilation|auto-trad|
|[equipment-28-kwo4VKC9Qkplusxs.htm](equipment/equipment-28-kwo4VKC9Qkplusxs.htm)|Essence Prism|auto-trad|
|[equipment-28-mH3LImCgJAkfKAA3.htm](equipment/equipment-28-mH3LImCgJAkfKAA3.htm)|Philosopher's Extractor|auto-trad|
|[kit-2req0jGaxz8hScdB.htm](equipment/kit-2req0jGaxz8hScdB.htm)|Adventurer's Pack|auto-trad|
|[kit-5WT7gdzHNb5BG1J0.htm](equipment/kit-5WT7gdzHNb5BG1J0.htm)|Class Kit (Witch)|auto-trad|
|[kit-7E3sWotW56biNttA.htm](equipment/kit-7E3sWotW56biNttA.htm)|Class Kit (Ranger)|auto-trad|
|[kit-9RxYSGp86HKtK8R8.htm](equipment/kit-9RxYSGp86HKtK8R8.htm)|Class Kit (Swashbuckler)|auto-trad|
|[kit-Ac0BSiDJhtMmPnge.htm](equipment/kit-Ac0BSiDJhtMmPnge.htm)|Class Kit (Oracle)|auto-trad|
|[kit-adNgRqqmkDDKhMRT.htm](equipment/kit-adNgRqqmkDDKhMRT.htm)|Class Kit (Barbarian)|auto-trad|
|[kit-asQAvJXm3DSzPQtA.htm](equipment/kit-asQAvJXm3DSzPQtA.htm)|Class Kit (Bard)|auto-trad|
|[kit-bJKgKrIQ5puuifcz.htm](equipment/kit-bJKgKrIQ5puuifcz.htm)|Class Kit (Sorcerer)|auto-trad|
|[kit-cozhl69heIF0vjUt.htm](equipment/kit-cozhl69heIF0vjUt.htm)|Class Kit (Fighter)|auto-trad|
|[kit-drY7DJ9rMQEP0et8.htm](equipment/kit-drY7DJ9rMQEP0et8.htm)|Class Kit (Monk)|auto-trad|
|[kit-gB4kCTgR3b6SJw7n.htm](equipment/kit-gB4kCTgR3b6SJw7n.htm)|Class Kit (Champion)|auto-trad|
|[kit-gCvGa77UZjhsbuOJ.htm](equipment/kit-gCvGa77UZjhsbuOJ.htm)|Class Kit (Druid)|auto-trad|
|[kit-iKnFPR1X8mOAeVCV.htm](equipment/kit-iKnFPR1X8mOAeVCV.htm)|Class Kit (Cleric)|auto-trad|
|[kit-KVXcrw446rzYRdpy.htm](equipment/kit-KVXcrw446rzYRdpy.htm)|Class Kit (Alchemist)|auto-trad|
|[kit-nPGrNslTdc6VBuCB.htm](equipment/kit-nPGrNslTdc6VBuCB.htm)|Class Kit (Rogue)|auto-trad|
|[kit-tHWnYpDZlscldOX0.htm](equipment/kit-tHWnYpDZlscldOX0.htm)|Class Kit (Wizard)|auto-trad|
|[kit-YQLWR9cCXQY5xaaG.htm](equipment/kit-YQLWR9cCXQY5xaaG.htm)|Cartographer's Kit|auto-trad|
|[kit-zcNBG8cMnILqXMFd.htm](equipment/kit-zcNBG8cMnILqXMFd.htm)|Class Kit (Investigator)|auto-trad|
|[treasure-00-02sF1QlxEx6H6zEP.htm](equipment/treasure-00-02sF1QlxEx6H6zEP.htm)|Hematite|auto-trad|
|[treasure-00-0b6BUtoFD4DotZHR.htm](equipment/treasure-00-0b6BUtoFD4DotZHR.htm)|Gilded ceremonial armor|auto-trad|
|[treasure-00-0zVMeeklYZ7hcgXw.htm](equipment/treasure-00-0zVMeeklYZ7hcgXw.htm)|Emerald, brilliant green|auto-trad|
|[treasure-00-2MPfYdRWfUNnt5JK.htm](equipment/treasure-00-2MPfYdRWfUNnt5JK.htm)|Gold dragon statuette|auto-trad|
|[treasure-00-2PXwkWD3ymM7s0Ul.htm](equipment/treasure-00-2PXwkWD3ymM7s0Ul.htm)|Ivory|auto-trad|
|[treasure-00-2Rb9MOLENbZ7yyRL.htm](equipment/treasure-00-2Rb9MOLENbZ7yyRL.htm)|Star sapphire|auto-trad|
|[treasure-00-2T33bWTWXj1v2J03.htm](equipment/treasure-00-2T33bWTWXj1v2J03.htm)|Simple sculpture|auto-trad|
|[treasure-00-3l30rcsNpDM7moKu.htm](equipment/treasure-00-3l30rcsNpDM7moKu.htm)|Onyx|auto-trad|
|[treasure-00-3UOS4sien8IQbPNV.htm](equipment/treasure-00-3UOS4sien8IQbPNV.htm)|Ruby, large|auto-trad|
|[treasure-00-5CqtgygdvqecMoRU.htm](equipment/treasure-00-5CqtgygdvqecMoRU.htm)|Thought Lens of Astral Essence|auto-trad|
|[treasure-00-5dibOpDYOhJY6Ssn.htm](equipment/treasure-00-5dibOpDYOhJY6Ssn.htm)|Silver mirror with gilded frame|auto-trad|
|[treasure-00-5Ew82vBF9YfaiY9f.htm](equipment/treasure-00-5Ew82vBF9YfaiY9f.htm)|Silver Pieces|auto-trad|
|[treasure-00-5EYlhm8ZbR6dLUHF.htm](equipment/treasure-00-5EYlhm8ZbR6dLUHF.htm)|Silver statuette of a raven|auto-trad|
|[treasure-00-5ix9inqdHLksqeoR.htm](equipment/treasure-00-5ix9inqdHLksqeoR.htm)|Gold necklace with peridots|auto-trad|
|[treasure-00-69IWSryF5BWkwWXY.htm](equipment/treasure-00-69IWSryF5BWkwWXY.htm)|Set of six ivory dice|auto-trad|
|[treasure-00-6vb56WClb06JrpuJ.htm](equipment/treasure-00-6vb56WClb06JrpuJ.htm)|Shell|auto-trad|
|[treasure-00-6wCA8h0jpMnqPbib.htm](equipment/treasure-00-6wCA8h0jpMnqPbib.htm)|Enormous tapestry of a major battle|auto-trad|
|[treasure-00-7iVFlnr2ZbBdtOx8.htm](equipment/treasure-00-7iVFlnr2ZbBdtOx8.htm)|Pearl, saltwater|auto-trad|
|[treasure-00-7OuVXvNRbNoKzDSy.htm](equipment/treasure-00-7OuVXvNRbNoKzDSy.htm)|Diamond, large|auto-trad|
|[treasure-00-7SPJO9xr89N8E23s.htm](equipment/treasure-00-7SPJO9xr89N8E23s.htm)|Agate|auto-trad|
|[treasure-00-7Uk6LMmzsCxuhhA6.htm](equipment/treasure-00-7Uk6LMmzsCxuhhA6.htm)|Ancient dragon skull etched with mystic sigils|auto-trad|
|[treasure-00-8aPckmJTsyUfBndP.htm](equipment/treasure-00-8aPckmJTsyUfBndP.htm)|Turquoise|auto-trad|
|[treasure-00-8P87jwOAC3zMuzoF.htm](equipment/treasure-00-8P87jwOAC3zMuzoF.htm)|Scrimshaw whale bone|auto-trad|
|[treasure-00-8RH0e9UqXIxAPJJG.htm](equipment/treasure-00-8RH0e9UqXIxAPJJG.htm)|Silk mask decorated with citrines|auto-trad|
|[treasure-00-9FsyNDq3VOsFumhz.htm](equipment/treasure-00-9FsyNDq3VOsFumhz.htm)|Gold rapier with amethysts|auto-trad|
|[treasure-00-Ab8yDhtYDJnFrjI4.htm](equipment/treasure-00-Ab8yDhtYDJnFrjI4.htm)|Colorful velvet half mask|auto-trad|
|[treasure-00-aGJJwRpFRMgneROG.htm](equipment/treasure-00-aGJJwRpFRMgneROG.htm)|Quality sculpture by an unknown|auto-trad|
|[treasure-00-AgsZScsqv3puTmZx.htm](equipment/treasure-00-AgsZScsqv3puTmZx.htm)|Sardonyx|auto-trad|
|[treasure-00-aqhEATP0vu4kx15I.htm](equipment/treasure-00-aqhEATP0vu4kx15I.htm)|Saint's bone with lost scriptures|auto-trad|
|[treasure-00-B6B7tBWJSqOBz5zz.htm](equipment/treasure-00-B6B7tBWJSqOBz5zz.htm)|Gold Pieces|auto-trad|
|[treasure-00-bgswgiXV12HW0aEz.htm](equipment/treasure-00-bgswgiXV12HW0aEz.htm)|Simple painting|auto-trad|
|[treasure-00-BlqDXtRZcevCYRNz.htm](equipment/treasure-00-BlqDXtRZcevCYRNz.htm)|Jeweled mithral crown|auto-trad|
|[treasure-00-CNLt74pUxM0GGjyl.htm](equipment/treasure-00-CNLt74pUxM0GGjyl.htm)|Simple silver circlet|auto-trad|
|[treasure-00-CRTNNgrmssD6bQqQ.htm](equipment/treasure-00-CRTNNgrmssD6bQqQ.htm)|Jet|auto-trad|
|[treasure-00-cSBupUqjsHJwivbp.htm](equipment/treasure-00-cSBupUqjsHJwivbp.htm)|Etched copper ewer|auto-trad|
|[treasure-00-D8gzHTvP0uFxjwyA.htm](equipment/treasure-00-D8gzHTvP0uFxjwyA.htm)|Bloodstone|auto-trad|
|[treasure-00-dBQQZUXaMhCAFkpY.htm](equipment/treasure-00-dBQQZUXaMhCAFkpY.htm)|Aquamarine|auto-trad|
|[treasure-00-DE4880DRtsRqax1N.htm](equipment/treasure-00-DE4880DRtsRqax1N.htm)|Lapis lazuli|auto-trad|
|[treasure-00-DFNmzduKX6NSuPrT.htm](equipment/treasure-00-DFNmzduKX6NSuPrT.htm)|Tiger's‑eye|auto-trad|
|[treasure-00-dloHmPtGDr8zSwJu.htm](equipment/treasure-00-dloHmPtGDr8zSwJu.htm)|Leather flagon with Caydenite symbol|auto-trad|
|[treasure-00-Ds8GJEQyRYamRIiB.htm](equipment/treasure-00-Ds8GJEQyRYamRIiB.htm)|Malachite|auto-trad|
|[treasure-00-DWzoqVeEamX118Qk.htm](equipment/treasure-00-DWzoqVeEamX118Qk.htm)|Jet and white gold game set|auto-trad|
|[treasure-00-dzsJfE1JIWUMCiK4.htm](equipment/treasure-00-dzsJfE1JIWUMCiK4.htm)|Plain brass censer|auto-trad|
|[treasure-00-e6arpbDxMlmogfee.htm](equipment/treasure-00-e6arpbDxMlmogfee.htm)|Gold and aquamarine diadem|auto-trad|
|[treasure-00-EKgLX0TCyFik3gPS.htm](equipment/treasure-00-EKgLX0TCyFik3gPS.htm)|Coral idol of an elemental lord|auto-trad|
|[treasure-00-epTuWsVDuPzUXMk9.htm](equipment/treasure-00-epTuWsVDuPzUXMk9.htm)|Amethyst|auto-trad|
|[treasure-00-eUXQTjezsuJ6EB4u.htm](equipment/treasure-00-eUXQTjezsuJ6EB4u.htm)|Towering sculpture by a master|auto-trad|
|[treasure-00-F4YgvB74p8kwif1t.htm](equipment/treasure-00-F4YgvB74p8kwif1t.htm)|Ceremonial shortsword with spinels|auto-trad|
|[treasure-00-FI8PLxBFmHF6o0pH.htm](equipment/treasure-00-FI8PLxBFmHF6o0pH.htm)|Silver flagon inscribed with fields|auto-trad|
|[treasure-00-fUENXLX3tzi33CcB.htm](equipment/treasure-00-fUENXLX3tzi33CcB.htm)|Silk ceremonial armor|auto-trad|
|[treasure-00-FW1AEh214U1mMWh7.htm](equipment/treasure-00-FW1AEh214U1mMWh7.htm)|Chrysoprase|auto-trad|
|[treasure-00-FwYqacDEAmXk5jB5.htm](equipment/treasure-00-FwYqacDEAmXk5jB5.htm)|Star sapphire necklace|auto-trad|
|[treasure-00-G5WuYX1ghrZcJ1J1.htm](equipment/treasure-00-G5WuYX1ghrZcJ1J1.htm)|Illustrated book|auto-trad|
|[treasure-00-GgwlIaNPJVsyRwzz.htm](equipment/treasure-00-GgwlIaNPJVsyRwzz.htm)|Parade armor with flourishes|auto-trad|
|[treasure-00-Gi4vx84LhppOQAxr.htm](equipment/treasure-00-Gi4vx84LhppOQAxr.htm)|Silver coronet with peridots|auto-trad|
|[treasure-00-H8BXvZ1DUg3uJ53K.htm](equipment/treasure-00-H8BXvZ1DUg3uJ53K.htm)|Gold urn with scenes of judgment|auto-trad|
|[treasure-00-hEBNs4mYaVWz3UsX.htm](equipment/treasure-00-hEBNs4mYaVWz3UsX.htm)|Jeweled orrery of the planes|auto-trad|
|[treasure-00-hhJfZgLhPaPh5V7e.htm](equipment/treasure-00-hhJfZgLhPaPh5V7e.htm)|Pearl, black|auto-trad|
|[treasure-00-HIxtCuH5YXNqit0W.htm](equipment/treasure-00-HIxtCuH5YXNqit0W.htm)|Tankard owned by Cayden Cailean|auto-trad|
|[treasure-00-hJc0dOcHJ2nkJcik.htm](equipment/treasure-00-hJc0dOcHJ2nkJcik.htm)|Spinel, deep blue|auto-trad|
|[treasure-00-HMdfNM7ibp1XtcVT.htm](equipment/treasure-00-HMdfNM7ibp1XtcVT.htm)|Platinum dragon statuette|auto-trad|
|[treasure-00-hMIG86aqKcVYJGsg.htm](equipment/treasure-00-hMIG86aqKcVYJGsg.htm)|Emerald|auto-trad|
|[treasure-00-hnzNKdD5hjQc1NUx.htm](equipment/treasure-00-hnzNKdD5hjQc1NUx.htm)|Alabaster and obsidian game set|auto-trad|
|[treasure-00-I3KlOUruQ2CZdcJ6.htm](equipment/treasure-00-I3KlOUruQ2CZdcJ6.htm)|Bronze bowl with wave imagery|auto-trad|
|[treasure-00-ilnc82SKDYHcoEMH.htm](equipment/treasure-00-ilnc82SKDYHcoEMH.htm)|Opal|auto-trad|
|[treasure-00-iz7Mv2xGwqyicNiA.htm](equipment/treasure-00-iz7Mv2xGwqyicNiA.htm)|Quality painting by an unknown|auto-trad|
|[treasure-00-j4DU62QY4lJ4WEPQ.htm](equipment/treasure-00-j4DU62QY4lJ4WEPQ.htm)|Jasper|auto-trad|
|[treasure-00-ja5rYjVRwwUXWtOd.htm](equipment/treasure-00-ja5rYjVRwwUXWtOd.htm)|Bronze chalice with bloodstones|auto-trad|
|[treasure-00-JECXvZ0bKTDr79mo.htm](equipment/treasure-00-JECXvZ0bKTDr79mo.htm)|Porcelain vase inlaid with gold|auto-trad|
|[treasure-00-JP7G2gOS3OuMNzeE.htm](equipment/treasure-00-JP7G2gOS3OuMNzeE.htm)|Hand mirror with decorated frame|auto-trad|
|[treasure-00-JuNPeK5Qm1w6wpb4.htm](equipment/treasure-00-JuNPeK5Qm1w6wpb4.htm)|Platinum Pieces|auto-trad|
|[treasure-00-JYxnuXg9MV6y1gpQ.htm](equipment/treasure-00-JYxnuXg9MV6y1gpQ.htm)|Amphora with lavish scenes|auto-trad|
|[treasure-00-K44zDGRTSecRjEv0.htm](equipment/treasure-00-K44zDGRTSecRjEv0.htm)|Gilded scepter with sapphire|auto-trad|
|[treasure-00-K7KzNh3YsXBrDlyj.htm](equipment/treasure-00-K7KzNh3YsXBrDlyj.htm)|Inscribed crocodile skull|auto-trad|
|[treasure-00-l6Eifu4DScg0OBxb.htm](equipment/treasure-00-l6Eifu4DScg0OBxb.htm)|Brass scepter with amethyst head|auto-trad|
|[treasure-00-l8dFZIYYngDOIYHb.htm](equipment/treasure-00-l8dFZIYYngDOIYHb.htm)|Illuminated manuscript|auto-trad|
|[treasure-00-L8IzHSz2jYGFpp4N.htm](equipment/treasure-00-L8IzHSz2jYGFpp4N.htm)|Elegant cloth doll|auto-trad|
|[treasure-00-laXnb43oznxeSLT9.htm](equipment/treasure-00-laXnb43oznxeSLT9.htm)|Previously lost volume from a legendary author|auto-trad|
|[treasure-00-LEfLVcrbhZPmyUyI.htm](equipment/treasure-00-LEfLVcrbhZPmyUyI.htm)|Famous portrait by a master|auto-trad|
|[treasure-00-LJxNB3Yy2sTJaUbd.htm](equipment/treasure-00-LJxNB3Yy2sTJaUbd.htm)|Splendid lyre of world‑famous lyrist|auto-trad|
|[treasure-00-lvrvCGrmnMmE4TxU.htm](equipment/treasure-00-lvrvCGrmnMmE4TxU.htm)|Bronze brazier with Asmodean artwork|auto-trad|
|[treasure-00-LyQQvO7COYAvFRNw.htm](equipment/treasure-00-LyQQvO7COYAvFRNw.htm)|Jade|auto-trad|
|[treasure-00-LzHxy9YPEaCueLB9.htm](equipment/treasure-00-LzHxy9YPEaCueLB9.htm)|Platinum image of a fey noble with a bit of orichalcum|auto-trad|
|[treasure-00-lzJ8AVhRcbFul5fh.htm](equipment/treasure-00-lzJ8AVhRcbFul5fh.htm)|Copper Pieces|auto-trad|
|[treasure-00-m26FikfAdZouHzhq.htm](equipment/treasure-00-m26FikfAdZouHzhq.htm)|Tourmaline|auto-trad|
|[treasure-00-mixyMNYUClKfqURh.htm](equipment/treasure-00-mixyMNYUClKfqURh.htm)|Sard|auto-trad|
|[treasure-00-MK6nnMZNWHmChAXZ.htm](equipment/treasure-00-MK6nnMZNWHmChAXZ.htm)|Chandelier crafted from dreams|auto-trad|
|[treasure-00-Mm3xgz27Q94wZhti.htm](equipment/treasure-00-Mm3xgz27Q94wZhti.htm)|Engraved copper ring|auto-trad|
|[treasure-00-Mp6StcWjlKCpD8Jx.htm](equipment/treasure-00-Mp6StcWjlKCpD8Jx.htm)|Moonstone and onyx game set|auto-trad|
|[treasure-00-nBY7ueX18tcSiica.htm](equipment/treasure-00-nBY7ueX18tcSiica.htm)|Platinum‑framed monocle|auto-trad|
|[treasure-00-nIlx1IQhYJfQtpVF.htm](equipment/treasure-00-nIlx1IQhYJfQtpVF.htm)|Alabaster|auto-trad|
|[treasure-00-NLcnptEjjiY9teq7.htm](equipment/treasure-00-NLcnptEjjiY9teq7.htm)|Intricate silver and gold music box|auto-trad|
|[treasure-00-noLtrW7wFfRFxSUt.htm](equipment/treasure-00-noLtrW7wFfRFxSUt.htm)|Colorful pastoral tapestry|auto-trad|
|[treasure-00-NuX85j3bYLYVcINr.htm](equipment/treasure-00-NuX85j3bYLYVcINr.htm)|Garnet|auto-trad|
|[treasure-00-nzT4kWzAdLMcESWl.htm](equipment/treasure-00-nzT4kWzAdLMcESWl.htm)|Virtuoso silver flute|auto-trad|
|[treasure-00-oH7dJGkgAB0Y2Mad.htm](equipment/treasure-00-oH7dJGkgAB0Y2Mad.htm)|Obsidian|auto-trad|
|[treasure-00-ony7u60cfBMtLG8F.htm](equipment/treasure-00-ony7u60cfBMtLG8F.htm)|Gold chalice with black pearls|auto-trad|
|[treasure-00-Oru7cHMGULLXRLV3.htm](equipment/treasure-00-Oru7cHMGULLXRLV3.htm)|Original manuscript from a world‑famous author|auto-trad|
|[treasure-00-oyGXxUmS6QcthfRc.htm](equipment/treasure-00-oyGXxUmS6QcthfRc.htm)|Topaz|auto-trad|
|[treasure-00-OzxNhgRFEgdTybFl.htm](equipment/treasure-00-OzxNhgRFEgdTybFl.htm)|Major Painting by a Legend|auto-trad|
|[treasure-00-oZYu6GMIIoOYW2Nh.htm](equipment/treasure-00-oZYu6GMIIoOYW2Nh.htm)|Peridot|auto-trad|
|[treasure-00-P2MIJxEQiIWJmV8n.htm](equipment/treasure-00-P2MIJxEQiIWJmV8n.htm)|Carnelian|auto-trad|
|[treasure-00-p3mg6gK5KhpS7plo.htm](equipment/treasure-00-p3mg6gK5KhpS7plo.htm)|Zircon|auto-trad|
|[treasure-00-P7jMpOYmusKse1Yu.htm](equipment/treasure-00-P7jMpOYmusKse1Yu.htm)|Gold and garnet ring|auto-trad|
|[treasure-00-PGlNIOpo8yedgTFR.htm](equipment/treasure-00-PGlNIOpo8yedgTFR.htm)|Quartz, milky, rose, or smoky|auto-trad|
|[treasure-00-PNnmcBhEwZmAKsq9.htm](equipment/treasure-00-PNnmcBhEwZmAKsq9.htm)|Copper and spinel puzzle box|auto-trad|
|[treasure-00-PTkTPYtxrPwgCzeq.htm](equipment/treasure-00-PTkTPYtxrPwgCzeq.htm)|Darkwood violin by a legend|auto-trad|
|[treasure-00-PzWETSKeVKisKCyl.htm](equipment/treasure-00-PzWETSKeVKisKCyl.htm)|Living flame shaped into a phoenix|auto-trad|
|[treasure-00-q72jL8PRcjw3DVoF.htm](equipment/treasure-00-q72jL8PRcjw3DVoF.htm)|Phasing ether silk tapestry|auto-trad|
|[treasure-00-qFkhYAeCxYci8jgk.htm](equipment/treasure-00-qFkhYAeCxYci8jgk.htm)|Amber|auto-trad|
|[treasure-00-qgKOCdtdybxwmiEv.htm](equipment/treasure-00-qgKOCdtdybxwmiEv.htm)|Chrysoberyl symbol of an evil eye|auto-trad|
|[treasure-00-qnK4cIHZ2irCV4BL.htm](equipment/treasure-00-qnK4cIHZ2irCV4BL.htm)|Quartz, rock crystal|auto-trad|
|[treasure-00-r7QGx0YFZCR0W5iu.htm](equipment/treasure-00-r7QGx0YFZCR0W5iu.htm)|Alabaster idol|auto-trad|
|[treasure-00-rgPjAtKt6J1AZ1Pj.htm](equipment/treasure-00-rgPjAtKt6J1AZ1Pj.htm)|Enormous chryselephantine sculpture by a legend|auto-trad|
|[treasure-00-rIhvmrRVr51VgfKa.htm](equipment/treasure-00-rIhvmrRVr51VgfKa.htm)|Solidified Moment of Time|auto-trad|
|[treasure-00-rjHqUOFqUeyTQyin.htm](equipment/treasure-00-rjHqUOFqUeyTQyin.htm)|Lapis lazuli pendant|auto-trad|
|[treasure-00-RKX4vLWyczPwfsKS.htm](equipment/treasure-00-RKX4vLWyczPwfsKS.htm)|Set of decorated ceramic plates|auto-trad|
|[treasure-00-RLFxTy0TjMkN6rM2.htm](equipment/treasure-00-RLFxTy0TjMkN6rM2.htm)|Pearl, irregular freshwater|auto-trad|
|[treasure-00-RNnVb83lRRUXg8hG.htm](equipment/treasure-00-RNnVb83lRRUXg8hG.htm)|Azurite|auto-trad|
|[treasure-00-rOMPvaCeLqNvT6rJ.htm](equipment/treasure-00-rOMPvaCeLqNvT6rJ.htm)|Brass statuette of a bull|auto-trad|
|[treasure-00-rVfFUBb0r423IPg8.htm](equipment/treasure-00-rVfFUBb0r423IPg8.htm)|Set of decorated porcelain plates|auto-trad|
|[treasure-00-s6wRFNG5lVEkGGdJ.htm](equipment/treasure-00-s6wRFNG5lVEkGGdJ.htm)|Wide landscape by an expert|auto-trad|
|[treasure-00-SmN0vRjkPaSaAJZt.htm](equipment/treasure-00-SmN0vRjkPaSaAJZt.htm)|Divine art piece created by Shelyn|auto-trad|
|[treasure-00-svhZ61CaSgatn5by.htm](equipment/treasure-00-svhZ61CaSgatn5by.htm)|Crystallized dragon heart|auto-trad|
|[treasure-00-tcP4ZKGZr5i4586v.htm](equipment/treasure-00-tcP4ZKGZr5i4586v.htm)|Gold mask of a high priest|auto-trad|
|[treasure-00-tMMYXdHps9QakCVn.htm](equipment/treasure-00-tMMYXdHps9QakCVn.htm)|Silver and jade censer|auto-trad|
|[treasure-00-TTuRgoLxVR2c6NEx.htm](equipment/treasure-00-TTuRgoLxVR2c6NEx.htm)|Marble altar|auto-trad|
|[treasure-00-u7YrZzA2yem780cP.htm](equipment/treasure-00-u7YrZzA2yem780cP.htm)|Coral|auto-trad|
|[treasure-00-uAQKNkgub7KNOHoR.htm](equipment/treasure-00-uAQKNkgub7KNOHoR.htm)|Brass anklet|auto-trad|
|[treasure-00-ugOCvoAZnVyEuIiN.htm](equipment/treasure-00-ugOCvoAZnVyEuIiN.htm)|Crystal dinner set, fine silverware|auto-trad|
|[treasure-00-Ujaoyu3WE6V7Y8Vg.htm](equipment/treasure-00-Ujaoyu3WE6V7Y8Vg.htm)|Rhodochrosite|auto-trad|
|[treasure-00-uMxXncL2bQI04GZ9.htm](equipment/treasure-00-uMxXncL2bQI04GZ9.htm)|Chrysoberyl|auto-trad|
|[treasure-00-VHJM3jYtpzMqZ1wc.htm](equipment/treasure-00-VHJM3jYtpzMqZ1wc.htm)|Ceremonial dagger with onyx hilt|auto-trad|
|[treasure-00-vJLb8wwipdwFQDH3.htm](equipment/treasure-00-vJLb8wwipdwFQDH3.htm)|Silk fan decorated with turquoise|auto-trad|
|[treasure-00-vKdjLDfL75SfFGuL.htm](equipment/treasure-00-vKdjLDfL75SfFGuL.htm)|Moonstone|auto-trad|
|[treasure-00-wBoGy4DfKHAvqAko.htm](equipment/treasure-00-wBoGy4DfKHAvqAko.htm)|Spinel, red or green|auto-trad|
|[treasure-00-WEnrPya5A8CKi91A.htm](equipment/treasure-00-WEnrPya5A8CKi91A.htm)|Citrine|auto-trad|
|[treasure-00-WuW89GD0TG7RYTLF.htm](equipment/treasure-00-WuW89GD0TG7RYTLF.htm)|Ruby, small|auto-trad|
|[treasure-00-x46wlAbSDKFEumvg.htm](equipment/treasure-00-x46wlAbSDKFEumvg.htm)|Fine gold spyglass|auto-trad|
|[treasure-00-x7rEpjFkUNTqmt1t.htm](equipment/treasure-00-x7rEpjFkUNTqmt1t.htm)|Gold and opal bracelet|auto-trad|
|[treasure-00-xXo7KXnhp1TfvxRc.htm](equipment/treasure-00-xXo7KXnhp1TfvxRc.htm)|Copper statuette of a salamander|auto-trad|
|[treasure-00-xzmYwmvj61br2bRp.htm](equipment/treasure-00-xzmYwmvj61br2bRp.htm)|Carved wooden game set|auto-trad|
|[treasure-00-YeMi3otmwQ34W2we.htm](equipment/treasure-00-YeMi3otmwQ34W2we.htm)|Iron cauldron with gargoyle faces|auto-trad|
|[treasure-00-YFokzLQ0HK74Vf6n.htm](equipment/treasure-00-YFokzLQ0HK74Vf6n.htm)|Pyrite|auto-trad|
|[treasure-00-yrVJKmpBS0mqFuO1.htm](equipment/treasure-00-yrVJKmpBS0mqFuO1.htm)|Jeweled gold puzzle box|auto-trad|
|[treasure-00-yTYPPB7WrhanBPbu.htm](equipment/treasure-00-yTYPPB7WrhanBPbu.htm)|Sapphire|auto-trad|
|[treasure-00-yv7OfuEaXTm6YCAc.htm](equipment/treasure-00-yv7OfuEaXTm6YCAc.htm)|Small cold iron cauldron with onyx|auto-trad|
|[treasure-00-YZsZ6stG0i9GBCZw.htm](equipment/treasure-00-YZsZ6stG0i9GBCZw.htm)|Iron and rock crystal brazier|auto-trad|
|[treasure-00-z3PT7OgX34TQKAwH.htm](equipment/treasure-00-z3PT7OgX34TQKAwH.htm)|Diamond, small|auto-trad|
|[treasure-00-z84EQt8kcPf0RXwx.htm](equipment/treasure-00-z84EQt8kcPf0RXwx.htm)|Porcelain doll with amber eyes|auto-trad|
|[treasure-00-zdiHlKtd4Fy1aIPC.htm](equipment/treasure-00-zdiHlKtd4Fy1aIPC.htm)|Life‑size sculpture by an expert|auto-trad|
|[treasure-00-ZQXCEq8IyGf3WnEj.htm](equipment/treasure-00-ZQXCEq8IyGf3WnEj.htm)|Diamond ring with platinum band|auto-trad|
|[treasure-00-zRsMLEqh2t2hqSw4.htm](equipment/treasure-00-zRsMLEqh2t2hqSw4.htm)|Jewel‑encrusted gold altar|auto-trad|
|[weapon-00-0E9ADJkQUVsz7A4G.htm](equipment/weapon-00-0E9ADJkQUVsz7A4G.htm)|Gakgung|auto-trad|
|[weapon-00-0lCXehyFlXdYxDfA.htm](equipment/weapon-00-0lCXehyFlXdYxDfA.htm)|Bladed Diabolo|auto-trad|
|[weapon-00-17MQKaMIHQAJNtuu.htm](equipment/weapon-00-17MQKaMIHQAJNtuu.htm)|Fighting Stick|auto-trad|
|[weapon-00-1U7Laa7Yt7i3G77L.htm](equipment/weapon-00-1U7Laa7Yt7i3G77L.htm)|Gnome Hooked Hammer|auto-trad|
|[weapon-00-1Uvz5xJpE5UvOFjJ.htm](equipment/weapon-00-1Uvz5xJpE5UvOFjJ.htm)|Wish Knife|auto-trad|
|[weapon-00-27CO2NIr5yMgA6sa.htm](equipment/weapon-00-27CO2NIr5yMgA6sa.htm)|Leiomano|auto-trad|
|[weapon-00-2P9jItR1sV20OqmD.htm](equipment/weapon-00-2P9jItR1sV20OqmD.htm)|Thorn Whip|auto-trad|
|[weapon-00-3NQj5gtHIYFYlAch.htm](equipment/weapon-00-3NQj5gtHIYFYlAch.htm)|Thundermace|auto-trad|
|[weapon-00-3Zv5hSXXtlaDatUv.htm](equipment/weapon-00-3Zv5hSXXtlaDatUv.htm)|Greatpick|auto-trad|
|[weapon-00-4gWvfeDGTSAf9LAQ.htm](equipment/weapon-00-4gWvfeDGTSAf9LAQ.htm)|Dragon Mouth Pistol|auto-trad|
|[weapon-00-4LJEpZ2HkCu9BvHI.htm](equipment/weapon-00-4LJEpZ2HkCu9BvHI.htm)|Hand Cannon|auto-trad|
|[weapon-00-4lO2uNA70HqTuSAD.htm](equipment/weapon-00-4lO2uNA70HqTuSAD.htm)|Piercing Wind|auto-trad|
|[weapon-00-5fu6dCtqhdBnHNqh.htm](equipment/weapon-00-5fu6dCtqhdBnHNqh.htm)|Morningstar|auto-trad|
|[weapon-00-5qW06ylMkJ0wToDd.htm](equipment/weapon-00-5qW06ylMkJ0wToDd.htm)|Combat Lure|auto-trad|
|[weapon-00-5v2mhiBbYQDhlsw5.htm](equipment/weapon-00-5v2mhiBbYQDhlsw5.htm)|Dandpatta|auto-trad|
|[weapon-00-6I4YJAQUbTAqbpsI.htm](equipment/weapon-00-6I4YJAQUbTAqbpsI.htm)|Pick|auto-trad|
|[weapon-00-6KWYmeRMxsQfWhhJ.htm](equipment/weapon-00-6KWYmeRMxsQfWhhJ.htm)|Bastard Sword|auto-trad|
|[weapon-00-6qJkA6fnHHiF4CEV.htm](equipment/weapon-00-6qJkA6fnHHiF4CEV.htm)|Tengu Gale Blade|auto-trad|
|[weapon-00-6v4aedCiIAoSJfiL.htm](equipment/weapon-00-6v4aedCiIAoSJfiL.htm)|Kalis|auto-trad|
|[weapon-00-76AqNZB5xuFWxgJI.htm](equipment/weapon-00-76AqNZB5xuFWxgJI.htm)|Fighting Fan|auto-trad|
|[weapon-00-79krBL334Kp1RMNB.htm](equipment/weapon-00-79krBL334Kp1RMNB.htm)|Battle Lute|auto-trad|
|[weapon-00-7mXZyzL5v3K6Buu2.htm](equipment/weapon-00-7mXZyzL5v3K6Buu2.htm)|Battle Saddle|auto-trad|
|[weapon-00-7tKkkF8eZ4iCLJtp.htm](equipment/weapon-00-7tKkkF8eZ4iCLJtp.htm)|Shortsword|auto-trad|
|[weapon-00-7WRzbBTOCsJx5f7L.htm](equipment/weapon-00-7WRzbBTOCsJx5f7L.htm)|Buugeng|auto-trad|
|[weapon-00-80G0z7iFUCjHeYGf.htm](equipment/weapon-00-80G0z7iFUCjHeYGf.htm)|Nightstick|auto-trad|
|[weapon-00-8COlYvHe6hKCXY8x.htm](equipment/weapon-00-8COlYvHe6hKCXY8x.htm)|Greataxe|auto-trad|
|[weapon-00-8eGV6fXbmt2v74uB.htm](equipment/weapon-00-8eGV6fXbmt2v74uB.htm)|Black Powder Knuckle Dusters|auto-trad|
|[weapon-00-8V4mgecGASsQ7fjl.htm](equipment/weapon-00-8V4mgecGASsQ7fjl.htm)|Adze|auto-trad|
|[weapon-00-8XwE8hsWBFoIdoDC.htm](equipment/weapon-00-8XwE8hsWBFoIdoDC.htm)|Karambit|auto-trad|
|[weapon-00-9iDqOLNFKxiTcFKE.htm](equipment/weapon-00-9iDqOLNFKxiTcFKE.htm)|Mace|auto-trad|
|[weapon-00-a5DlDpB3RuSIvLIC.htm](equipment/weapon-00-a5DlDpB3RuSIvLIC.htm)|Hongali Hornbow|auto-trad|
|[weapon-00-ADvVuMwIWDZZF9cv.htm](equipment/weapon-00-ADvVuMwIWDZZF9cv.htm)|Kris|auto-trad|
|[weapon-00-Ak3oHoPDJoMR14yg.htm](equipment/weapon-00-Ak3oHoPDJoMR14yg.htm)|Lion Scythe|auto-trad|
|[weapon-00-aXuJh4i8HqSu6NYV.htm](equipment/weapon-00-aXuJh4i8HqSu6NYV.htm)|Longspear|auto-trad|
|[weapon-00-aYzWoFt5uYY31zEQ.htm](equipment/weapon-00-aYzWoFt5uYY31zEQ.htm)|Dwarven Dorn-Dergar|auto-trad|
|[weapon-00-bCPxjuqVusM7Qlpk.htm](equipment/weapon-00-bCPxjuqVusM7Qlpk.htm)|Tonfa|auto-trad|
|[weapon-00-BPQRgrJwr5GuT00g.htm](equipment/weapon-00-BPQRgrJwr5GuT00g.htm)|Dagger Pistol|auto-trad|
|[weapon-00-Brs4linGy3GAod5y.htm](equipment/weapon-00-Brs4linGy3GAod5y.htm)|Three-Section Naginata|auto-trad|
|[weapon-00-BtncTx8EfxTsHqQI.htm](equipment/weapon-00-BtncTx8EfxTsHqQI.htm)|Clan Pistol|auto-trad|
|[weapon-00-bzhJNnkIXdVRbjAA.htm](equipment/weapon-00-bzhJNnkIXdVRbjAA.htm)|Broadspear|auto-trad|
|[weapon-00-ChTaE7jhvCjcS6jI.htm](equipment/weapon-00-ChTaE7jhvCjcS6jI.htm)|Arquebus|auto-trad|
|[weapon-00-cKbiv1dUMViikKOS.htm](equipment/weapon-00-cKbiv1dUMViikKOS.htm)|Monkey's Fist|auto-trad|
|[weapon-00-cSGWhw4Wn6Z98tVK.htm](equipment/weapon-00-cSGWhw4Wn6Z98tVK.htm)|Lancer|auto-trad|
|[weapon-00-csXSDzgZASX4RWr4.htm](equipment/weapon-00-csXSDzgZASX4RWr4.htm)|Blunderbuss|auto-trad|
|[weapon-00-CVTgOpNuRE7hsnc1.htm](equipment/weapon-00-CVTgOpNuRE7hsnc1.htm)|Filcher's Fork|auto-trad|
|[weapon-00-d3Xv6QDWkpeg7VZn.htm](equipment/weapon-00-d3Xv6QDWkpeg7VZn.htm)|Dueling Spear|auto-trad|
|[weapon-00-D6E4VaKVG05G26Rm.htm](equipment/weapon-00-D6E4VaKVG05G26Rm.htm)|Kusarigama|auto-trad|
|[weapon-00-dfum7DpOEkwxwTsT.htm](equipment/weapon-00-dfum7DpOEkwxwTsT.htm)|Shield Boss|auto-trad|
|[weapon-00-dgXjUcaoQDAY6ZpO.htm](equipment/weapon-00-dgXjUcaoQDAY6ZpO.htm)|Knuckle Duster|auto-trad|
|[weapon-00-djSCel13i1p8JeXv.htm](equipment/weapon-00-djSCel13i1p8JeXv.htm)|Rope Dart|auto-trad|
|[weapon-00-DP03AwxQxCOAxch0.htm](equipment/weapon-00-DP03AwxQxCOAxch0.htm)|Asp Coil|auto-trad|
|[weapon-00-EPhsyiO4jYl0jWoC.htm](equipment/weapon-00-EPhsyiO4jYl0jWoC.htm)|Flingflenser|auto-trad|
|[weapon-00-EtfVzTsCYULkEzFk.htm](equipment/weapon-00-EtfVzTsCYULkEzFk.htm)|Chakram|auto-trad|
|[weapon-00-f1gwoTkf3Nn0v3PN.htm](equipment/weapon-00-f1gwoTkf3Nn0v3PN.htm)|Whip|auto-trad|
|[weapon-00-FbquJr6FuXL3K373.htm](equipment/weapon-00-FbquJr6FuXL3K373.htm)|Gill Hook|auto-trad|
|[weapon-00-fcWvG7jAZDLMRBWn.htm](equipment/weapon-00-fcWvG7jAZDLMRBWn.htm)|Earthbreaker|auto-trad|
|[weapon-00-Fg3GkCDkszj5WtgQ.htm](equipment/weapon-00-Fg3GkCDkszj5WtgQ.htm)|Katar|auto-trad|
|[weapon-00-FibwLZ12EIEwLGhw.htm](equipment/weapon-00-FibwLZ12EIEwLGhw.htm)|Light Hammer|auto-trad|
|[weapon-00-fjkwYZ0hRmBztwBG.htm](equipment/weapon-00-fjkwYZ0hRmBztwBG.htm)|Shuriken|auto-trad|
|[weapon-00-FJrsDoaIXksVjld9.htm](equipment/weapon-00-FJrsDoaIXksVjld9.htm)|Trident|auto-trad|
|[weapon-00-FuS6F91Rhd4m3T6d.htm](equipment/weapon-00-FuS6F91Rhd4m3T6d.htm)|Fauchard|auto-trad|
|[weapon-00-fvvfZxfGV9i3urkd.htm](equipment/weapon-00-fvvfZxfGV9i3urkd.htm)|Claw Blade|auto-trad|
|[weapon-00-fX63Af9Bf2XVarJu.htm](equipment/weapon-00-fX63Af9Bf2XVarJu.htm)|Dancer's Spear|auto-trad|
|[weapon-00-fz8XDaJ7n0zeOH1f.htm](equipment/weapon-00-fz8XDaJ7n0zeOH1f.htm)|Shield Bow|auto-trad|
|[weapon-00-fZxI5GkBFfAUUf1z.htm](equipment/weapon-00-fZxI5GkBFfAUUf1z.htm)|Wrecker|auto-trad|
|[weapon-00-GntaZfIfKj5MRyA5.htm](equipment/weapon-00-GntaZfIfKj5MRyA5.htm)|Wheel Blades|auto-trad|
|[weapon-00-grmaV4GdoGD7sKbn.htm](equipment/weapon-00-grmaV4GdoGD7sKbn.htm)|Scimitar|auto-trad|
|[weapon-00-gTTJuNgwTcNmkDx2.htm](equipment/weapon-00-gTTJuNgwTcNmkDx2.htm)|Boarding Axe|auto-trad|
|[weapon-00-GuWKXErLL5R43sIy.htm](equipment/weapon-00-GuWKXErLL5R43sIy.htm)|Talwar|auto-trad|
|[weapon-00-H74vYwHJ8XT4qOPI.htm](equipment/weapon-00-H74vYwHJ8XT4qOPI.htm)|Scythe|auto-trad|
|[weapon-00-Hc4qCwyO9i0Avlzt.htm](equipment/weapon-00-Hc4qCwyO9i0Avlzt.htm)|Long Air Repeater|auto-trad|
|[weapon-00-hqMtsTwmOShdAdQW.htm](equipment/weapon-00-hqMtsTwmOShdAdQW.htm)|Flintlock Musket|auto-trad|
|[weapon-00-I5DJUbAFGMwa6qCz.htm](equipment/weapon-00-I5DJUbAFGMwa6qCz.htm)|Scourge|auto-trad|
|[weapon-00-iCdvNfzvsTZ0s5vg.htm](equipment/weapon-00-iCdvNfzvsTZ0s5vg.htm)|Bladed Scarf|auto-trad|
|[weapon-00-IEA7M6GGv4rnGLdW.htm](equipment/weapon-00-IEA7M6GGv4rnGLdW.htm)|Bladed Hoop|auto-trad|
|[weapon-00-IF6qUrR3i030v0dH.htm](equipment/weapon-00-IF6qUrR3i030v0dH.htm)|Shears|auto-trad|
|[weapon-00-IoJ0WpfUPWGuaqBG.htm](equipment/weapon-00-IoJ0WpfUPWGuaqBG.htm)|Gada|auto-trad|
|[weapon-00-iWcw1sFLAzByVLBP.htm](equipment/weapon-00-iWcw1sFLAzByVLBP.htm)|Main-Gauche|auto-trad|
|[weapon-00-Ix2vicchE79d6Cl3.htm](equipment/weapon-00-Ix2vicchE79d6Cl3.htm)|Gauntlet|auto-trad|
|[weapon-00-J6H5UbLVsSXShoTs.htm](equipment/weapon-00-J6H5UbLVsSXShoTs.htm)|Orc Necksplitter|auto-trad|
|[weapon-00-jbSgs5Tq0bkmHUab.htm](equipment/weapon-00-jbSgs5Tq0bkmHUab.htm)|Sansetsukon|auto-trad|
|[weapon-00-jkYn89XREERA3V2e.htm](equipment/weapon-00-jkYn89XREERA3V2e.htm)|Shauth Blade|auto-trad|
|[weapon-00-JlakyeGJjNbryq6v.htm](equipment/weapon-00-JlakyeGJjNbryq6v.htm)|Chain Sword|auto-trad|
|[weapon-00-jm5gQnfvVeUiVfdW.htm](equipment/weapon-00-jm5gQnfvVeUiVfdW.htm)|Bayonet|auto-trad|
|[weapon-00-JNt7GmLCCVz5BiEI.htm](equipment/weapon-00-JNt7GmLCCVz5BiEI.htm)|Javelin|auto-trad|
|[weapon-00-JuR0cug2xq4sWxHR.htm](equipment/weapon-00-JuR0cug2xq4sWxHR.htm)|Butterfly Sword|auto-trad|
|[weapon-00-k8V3wTG1gMU5ksUr.htm](equipment/weapon-00-k8V3wTG1gMU5ksUr.htm)|Rhoka Sword|auto-trad|
|[weapon-00-K9ndlHU5JRHj0ivP.htm](equipment/weapon-00-K9ndlHU5JRHj0ivP.htm)|Bow Staff|auto-trad|
|[weapon-00-Ka49l6JYNi41tgVi.htm](equipment/weapon-00-Ka49l6JYNi41tgVi.htm)|Atlatl|auto-trad|
|[weapon-00-KaYvuSNXZaOammij.htm](equipment/weapon-00-KaYvuSNXZaOammij.htm)|Crescent Cross|auto-trad|
|[weapon-00-kbKAFqsEDWy5mN1y.htm](equipment/weapon-00-kbKAFqsEDWy5mN1y.htm)|Flyssa|auto-trad|
|[weapon-00-kdGqnqbrwPzQfTsm.htm](equipment/weapon-00-kdGqnqbrwPzQfTsm.htm)|Greatclub|auto-trad|
|[weapon-00-kedgBVNDRAdmseRe.htm](equipment/weapon-00-kedgBVNDRAdmseRe.htm)|Tamchal Chakram|auto-trad|
|[weapon-00-kJJvKm80KwWXPukV.htm](equipment/weapon-00-kJJvKm80KwWXPukV.htm)|Clan Dagger|auto-trad|
|[weapon-00-KtarUneAx0hibGtW.htm](equipment/weapon-00-KtarUneAx0hibGtW.htm)|Nodachi|auto-trad|
|[weapon-00-kxrlIT6mcZ4OEAjQ.htm](equipment/weapon-00-kxrlIT6mcZ4OEAjQ.htm)|Elven Branched Spear|auto-trad|
|[weapon-00-LA8wwpQBi6tylE6z.htm](equipment/weapon-00-LA8wwpQBi6tylE6z.htm)|Nunchaku|auto-trad|
|[weapon-00-LBSRvTsvrFofbB2c.htm](equipment/weapon-00-LBSRvTsvrFofbB2c.htm)|Piranha Kiss|auto-trad|
|[weapon-00-LcIfvbMPnOGYXVge.htm](equipment/weapon-00-LcIfvbMPnOGYXVge.htm)|Gauntlet Bow|auto-trad|
|[weapon-00-LDHGjtQHRM8bA8ZZ.htm](equipment/weapon-00-LDHGjtQHRM8bA8ZZ.htm)|Rapier Pistol|auto-trad|
|[weapon-00-LdOXiIVgRWpnOtcd.htm](equipment/weapon-00-LdOXiIVgRWpnOtcd.htm)|Zulfikar|auto-trad|
|[weapon-00-LGgvev6AV0So8tP9.htm](equipment/weapon-00-LGgvev6AV0So8tP9.htm)|Hatchet|auto-trad|
|[weapon-00-LJdbVTOZog39EEbi.htm](equipment/weapon-00-LJdbVTOZog39EEbi.htm)|Longsword|auto-trad|
|[weapon-00-LLYD2GEhzhdxoCAx.htm](equipment/weapon-00-LLYD2GEhzhdxoCAx.htm)|Coat Pistol|auto-trad|
|[weapon-00-loueS11Tfa9WD320.htm](equipment/weapon-00-loueS11Tfa9WD320.htm)|Alchemical Crossbow|auto-trad|
|[weapon-00-LxqA4VC4np2RLkQb.htm](equipment/weapon-00-LxqA4VC4np2RLkQb.htm)|Butchering Axe|auto-trad|
|[weapon-00-m3QMdlNxFyDFrkQX.htm](equipment/weapon-00-m3QMdlNxFyDFrkQX.htm)|Mace Multipistol|auto-trad|
|[weapon-00-MccX4AlmqYDjAN2J.htm](equipment/weapon-00-MccX4AlmqYDjAN2J.htm)|Hand Adze|auto-trad|
|[weapon-00-MeA0g9xvWCbGxf2g.htm](equipment/weapon-00-MeA0g9xvWCbGxf2g.htm)|Mambele|auto-trad|
|[weapon-00-mgrabKkyLmvxgLBK.htm](equipment/weapon-00-mgrabKkyLmvxgLBK.htm)|Elven Curve Blade|auto-trad|
|[weapon-00-mlrmkpOlwpnGkw4I.htm](equipment/weapon-00-mlrmkpOlwpnGkw4I.htm)|Maul|auto-trad|
|[weapon-00-mSOYAQb8iq1N1S1I.htm](equipment/weapon-00-mSOYAQb8iq1N1S1I.htm)|Corset Knife|auto-trad|
|[weapon-00-Mv2I6M70bagbaBPn.htm](equipment/weapon-00-Mv2I6M70bagbaBPn.htm)|Starknife|auto-trad|
|[weapon-00-mVxA9E2ernBL6fM6.htm](equipment/weapon-00-mVxA9E2ernBL6fM6.htm)|War Flail|auto-trad|
|[weapon-00-MvzR9nTnvKTeNjvQ.htm](equipment/weapon-00-MvzR9nTnvKTeNjvQ.htm)|Double-Barreled Pistol|auto-trad|
|[weapon-00-mXBOqphNfUCK7L8c.htm](equipment/weapon-00-mXBOqphNfUCK7L8c.htm)|Long Hammer|auto-trad|
|[weapon-00-MYnh7w7EL3AcQT41.htm](equipment/weapon-00-MYnh7w7EL3AcQT41.htm)|Dwarven War Axe|auto-trad|
|[weapon-00-N3nNqO5Nw2DIFhrv.htm](equipment/weapon-00-N3nNqO5Nw2DIFhrv.htm)|Flintlock Pistol|auto-trad|
|[weapon-00-NApB2Fl7i8wzp7RX.htm](equipment/weapon-00-NApB2Fl7i8wzp7RX.htm)|Gaff|auto-trad|
|[weapon-00-NhKIUGMcxVusTJ6y.htm](equipment/weapon-00-NhKIUGMcxVusTJ6y.htm)|Harmona Gun|auto-trad|
|[weapon-00-NIsxR5zXtVa3PuyU.htm](equipment/weapon-00-NIsxR5zXtVa3PuyU.htm)|Temple Sword|auto-trad|
|[weapon-00-nOFcCidD5AwVZWTv.htm](equipment/weapon-00-nOFcCidD5AwVZWTv.htm)|War Razor|auto-trad|
|[weapon-00-nSO0Z662LkkLfa2u.htm](equipment/weapon-00-nSO0Z662LkkLfa2u.htm)|Shield Spikes|auto-trad|
|[weapon-00-NufKswitJjxDCX8f.htm](equipment/weapon-00-NufKswitJjxDCX8f.htm)|Naginata|auto-trad|
|[weapon-00-O3XAH3VkOx9a0FyD.htm](equipment/weapon-00-O3XAH3VkOx9a0FyD.htm)|Hook Sword|auto-trad|
|[weapon-00-O7IZBvVoe7W2XnBa.htm](equipment/weapon-00-O7IZBvVoe7W2XnBa.htm)|Machete|auto-trad|
|[weapon-00-Oiq3QgLrM4i3W5Hg.htm](equipment/weapon-00-Oiq3QgLrM4i3W5Hg.htm)|Ogre Hook|auto-trad|
|[weapon-00-okEnjp0JgUzbVauY.htm](equipment/weapon-00-okEnjp0JgUzbVauY.htm)|Wheel Spikes|auto-trad|
|[weapon-00-olwngGXM3hpgoLEP.htm](equipment/weapon-00-olwngGXM3hpgoLEP.htm)|Dogslicer|auto-trad|
|[weapon-00-ONHFPXNNw9BkNAKm.htm](equipment/weapon-00-ONHFPXNNw9BkNAKm.htm)|Pantograph Gauntlet|auto-trad|
|[weapon-00-OPMzQdM9rZznMGhW.htm](equipment/weapon-00-OPMzQdM9rZznMGhW.htm)|Lance|auto-trad|
|[weapon-00-oSQET5hKn9q4xlrl.htm](equipment/weapon-00-oSQET5hKn9q4xlrl.htm)|Gnome Flickmace|auto-trad|
|[weapon-00-oTQKyZ8Vd2RPzaWK.htm](equipment/weapon-00-oTQKyZ8Vd2RPzaWK.htm)|Harpoon|auto-trad|
|[weapon-00-oW9neeVcpHjEvjcN.htm](equipment/weapon-00-oW9neeVcpHjEvjcN.htm)|Mikazuki|auto-trad|
|[weapon-00-PIzBQehx975vMsvZ.htm](equipment/weapon-00-PIzBQehx975vMsvZ.htm)|Throwing Knife|auto-trad|
|[weapon-00-PJboj72eI9JXjtbn.htm](equipment/weapon-00-PJboj72eI9JXjtbn.htm)|Bladed Gauntlet|auto-trad|
|[weapon-00-pKiuN3wnAxlJCBuK.htm](equipment/weapon-00-pKiuN3wnAxlJCBuK.htm)|Reinforced Wheels|auto-trad|
|[weapon-00-PlXEjK5nqIxEYXQa.htm](equipment/weapon-00-PlXEjK5nqIxEYXQa.htm)|Forked Bipod|auto-trad|
|[weapon-00-PnFfW5u24xZV6mOH.htm](equipment/weapon-00-PnFfW5u24xZV6mOH.htm)|Poi|auto-trad|
|[weapon-00-Po1oeTDUaSUxeZxG.htm](equipment/weapon-00-Po1oeTDUaSUxeZxG.htm)|Donchak|auto-trad|
|[weapon-00-puQ90OZgQpVhxLRe.htm](equipment/weapon-00-puQ90OZgQpVhxLRe.htm)|Wish Blade|auto-trad|
|[weapon-00-QAEVaSQ3SCT8AuBX.htm](equipment/weapon-00-QAEVaSQ3SCT8AuBX.htm)|Whip Claw|auto-trad|
|[weapon-00-qQJguPUWpYY8z6BD.htm](equipment/weapon-00-qQJguPUWpYY8z6BD.htm)|Umbrella Injector|auto-trad|
|[weapon-00-qRqGIxcadjjxcij3.htm](equipment/weapon-00-qRqGIxcadjjxcij3.htm)|Mithral Tree|auto-trad|
|[weapon-00-R2mDnYVqewmx3dV0.htm](equipment/weapon-00-R2mDnYVqewmx3dV0.htm)|Exquisite Sword Cane Sheath|auto-trad|
|[weapon-00-RbM4HvPyrZ4YJxRc.htm](equipment/weapon-00-RbM4HvPyrZ4YJxRc.htm)|Light Pick|auto-trad|
|[weapon-00-RhfhVSfiH23v4U7k.htm](equipment/weapon-00-RhfhVSfiH23v4U7k.htm)|Juggling Club|auto-trad|
|[weapon-00-RN3m5Hb5LPbdZGoO.htm](equipment/weapon-00-RN3m5Hb5LPbdZGoO.htm)|Daikyu|auto-trad|
|[weapon-00-rQWaJhI5Bko5x14Z.htm](equipment/weapon-00-rQWaJhI5Bko5x14Z.htm)|Dagger|auto-trad|
|[weapon-00-rXt4629QSg7KDTgJ.htm](equipment/weapon-00-rXt4629QSg7KDTgJ.htm)|Warhammer|auto-trad|
|[weapon-00-RytXxlJJ7dib8seN.htm](equipment/weapon-00-RytXxlJJ7dib8seN.htm)|Triggerbrand|auto-trad|
|[weapon-00-sAjzLXgA2LnIwpBM.htm](equipment/weapon-00-sAjzLXgA2LnIwpBM.htm)|Orc Knuckle Dagger|auto-trad|
|[weapon-00-ssDxSpQTYORoeCFA.htm](equipment/weapon-00-ssDxSpQTYORoeCFA.htm)|Frying Pan|auto-trad|
|[weapon-00-sY3UJ34xJ4MA0FMK.htm](equipment/weapon-00-sY3UJ34xJ4MA0FMK.htm)|Visap|auto-trad|
|[weapon-00-SzUynRs4HVtnpnel.htm](equipment/weapon-00-SzUynRs4HVtnpnel.htm)|Air Repeater|auto-trad|
|[weapon-00-t5FbyZtRL4qV0V7k.htm](equipment/weapon-00-t5FbyZtRL4qV0V7k.htm)|Flail|auto-trad|
|[weapon-00-T5ojw0tuXiAajhZx.htm](equipment/weapon-00-T5ojw0tuXiAajhZx.htm)|Rotary Bow|auto-trad|
|[weapon-00-TDrO7Xdyn7juFy3c.htm](equipment/weapon-00-TDrO7Xdyn7juFy3c.htm)|Kukri|auto-trad|
|[weapon-00-tH5GirEy7YB3ZgCk.htm](equipment/weapon-00-tH5GirEy7YB3ZgCk.htm)|Rapier|auto-trad|
|[weapon-00-tHIb3ynOHj7dGiCH.htm](equipment/weapon-00-tHIb3ynOHj7dGiCH.htm)|Reinforced Stock|auto-trad|
|[weapon-00-tk4cfktEnMrp4K6m.htm](equipment/weapon-00-tk4cfktEnMrp4K6m.htm)|Pepperbox|auto-trad|
|[weapon-00-tOhoGvmCMw4JpWcS.htm](equipment/weapon-00-tOhoGvmCMw4JpWcS.htm)|Spear|auto-trad|
|[weapon-00-TQ3kT9pUFQNkH2dG.htm](equipment/weapon-00-TQ3kT9pUFQNkH2dG.htm)|Spiral Rapier|auto-trad|
|[weapon-00-Tt4Qw64fwrxhr5gT.htm](equipment/weapon-00-Tt4Qw64fwrxhr5gT.htm)|Dart|auto-trad|
|[weapon-00-u2u6dkr01AB34tyA.htm](equipment/weapon-00-u2u6dkr01AB34tyA.htm)|Sai|auto-trad|
|[weapon-00-UCK8zXKb0Gi6uyum.htm](equipment/weapon-00-UCK8zXKb0Gi6uyum.htm)|Injection Spear|auto-trad|
|[weapon-00-UcLqR32mNaGdNkUk.htm](equipment/weapon-00-UcLqR32mNaGdNkUk.htm)|Fist|auto-trad|
|[weapon-00-UfurZQK6H6SgOjqe.htm](equipment/weapon-00-UfurZQK6H6SgOjqe.htm)|Ranseur|auto-trad|
|[weapon-00-uM0OlCC1Sh6OLdNn.htm](equipment/weapon-00-uM0OlCC1Sh6OLdNn.htm)|Throwing Knife (Extinction Curse)|auto-trad|
|[weapon-00-UnTUQ9w8yswupr9m.htm](equipment/weapon-00-UnTUQ9w8yswupr9m.htm)|Cane Pistol|auto-trad|
|[weapon-00-UX71GkWBL9g41VwM.htm](equipment/weapon-00-UX71GkWBL9g41VwM.htm)|Greatsword|auto-trad|
|[weapon-00-UXjKXqsfWYiayeMD.htm](equipment/weapon-00-UXjKXqsfWYiayeMD.htm)|Scorpion Whip|auto-trad|
|[weapon-00-v8C4hkqMNeUk60Db.htm](equipment/weapon-00-v8C4hkqMNeUk60Db.htm)|Rungu|auto-trad|
|[weapon-00-VitLIpdIAmKlGb7i.htm](equipment/weapon-00-VitLIpdIAmKlGb7i.htm)|Sword Cane|auto-trad|
|[weapon-00-vlnzTSsRmWeSkt9O.htm](equipment/weapon-00-vlnzTSsRmWeSkt9O.htm)|Glaive|auto-trad|
|[weapon-00-vnT33Z3V101FOUuV.htm](equipment/weapon-00-vnT33Z3V101FOUuV.htm)|Whipstaff|auto-trad|
|[weapon-00-VTFTlP8xmPKKV4S6.htm](equipment/weapon-00-VTFTlP8xmPKKV4S6.htm)|Bec de Corbin|auto-trad|
|[weapon-00-War0uyLBx1jA0Ge7.htm](equipment/weapon-00-War0uyLBx1jA0Ge7.htm)|Battle Axe|auto-trad|
|[weapon-00-wbJPWXKzQBKYZ74s.htm](equipment/weapon-00-wbJPWXKzQBKYZ74s.htm)|Taw Launcher|auto-trad|
|[weapon-00-WiPcevdeD4YoTLRa.htm](equipment/weapon-00-WiPcevdeD4YoTLRa.htm)|Shauth Lash|auto-trad|
|[weapon-00-wkzxLpSe7LN6c5Ld.htm](equipment/weapon-00-wkzxLpSe7LN6c5Ld.htm)|Sawtooth Saber|auto-trad|
|[weapon-00-wpvfykcSSbg27DgU.htm](equipment/weapon-00-wpvfykcSSbg27DgU.htm)|Dart Umbrella|auto-trad|
|[weapon-00-WUA40bb01pSWv88I.htm](equipment/weapon-00-WUA40bb01pSWv88I.htm)|Fire Lance|auto-trad|
|[weapon-00-WuzK2R5ra5SZdbij.htm](equipment/weapon-00-WuzK2R5ra5SZdbij.htm)|Tri-bladed Katar|auto-trad|
|[weapon-00-WxczDmQ5qOaD5IxB.htm](equipment/weapon-00-WxczDmQ5qOaD5IxB.htm)|Jiu Huan Dao|auto-trad|
|[weapon-00-wYruARP1YLfTQBsc.htm](equipment/weapon-00-wYruARP1YLfTQBsc.htm)|Fangwire|auto-trad|
|[weapon-00-x1TOpwH755Ami5bC.htm](equipment/weapon-00-x1TOpwH755Ami5bC.htm)|Light Mace|auto-trad|
|[weapon-00-XguFCTUBh1yqJXd1.htm](equipment/weapon-00-XguFCTUBh1yqJXd1.htm)|Spraysling|auto-trad|
|[weapon-00-xHdbwPOUgLPUtqLj.htm](equipment/weapon-00-xHdbwPOUgLPUtqLj.htm)|Falcata|auto-trad|
|[weapon-00-xmOGKAuCNYO4tJkM.htm](equipment/weapon-00-xmOGKAuCNYO4tJkM.htm)|Griffon Cane|auto-trad|
|[weapon-00-XQ57G0xuxu3nSzcD.htm](equipment/weapon-00-XQ57G0xuxu3nSzcD.htm)|Probing Cane|auto-trad|
|[weapon-00-xu3azdMCIa53Oe1f.htm](equipment/weapon-00-xu3azdMCIa53Oe1f.htm)|Meteor Hammer|auto-trad|
|[weapon-00-Y1dkRsRd1Z7Jf2y6.htm](equipment/weapon-00-Y1dkRsRd1Z7Jf2y6.htm)|Katana|auto-trad|
|[weapon-00-y5bkwzClxfs6gpDn.htm](equipment/weapon-00-y5bkwzClxfs6gpDn.htm)|Boomerang|auto-trad|
|[weapon-00-ye7cYmornhgnnOlu.htm](equipment/weapon-00-ye7cYmornhgnnOlu.htm)|Sickle-saber|auto-trad|
|[weapon-00-YFfG2fMyaIAXkmtr.htm](equipment/weapon-00-YFfG2fMyaIAXkmtr.htm)|Spiked Gauntlet|auto-trad|
|[weapon-00-yIY0voZkwMoff5b3.htm](equipment/weapon-00-yIY0voZkwMoff5b3.htm)|Scizore|auto-trad|
|[weapon-00-YLb6XnT7OxAVGL5m.htm](equipment/weapon-00-YLb6XnT7OxAVGL5m.htm)|Khopesh|auto-trad|
|[weapon-00-ymyPCjfyXCNyDcnn.htm](equipment/weapon-00-ymyPCjfyXCNyDcnn.htm)|Breaching Pike|auto-trad|
|[weapon-00-ynnBwzkzsR6B73iO.htm](equipment/weapon-00-ynnBwzkzsR6B73iO.htm)|Sickle|auto-trad|
|[weapon-00-YnPYSKCQBLIOtm0J.htm](equipment/weapon-00-YnPYSKCQBLIOtm0J.htm)|Aklys|auto-trad|
|[weapon-00-YnwEttFE1bJacLDh.htm](equipment/weapon-00-YnwEttFE1bJacLDh.htm)|Horsechopper|auto-trad|
|[weapon-00-YOpopjoWgU7bkmwh.htm](equipment/weapon-00-YOpopjoWgU7bkmwh.htm)|Wakizashi|auto-trad|
|[weapon-00-YUzPv0i8d8p2J9yx.htm](equipment/weapon-00-YUzPv0i8d8p2J9yx.htm)|Bola|auto-trad|
|[weapon-00-ZhxxqYpVdVx0jSMm.htm](equipment/weapon-00-ZhxxqYpVdVx0jSMm.htm)|Shield Bash|auto-trad|
|[weapon-00-znIsDYwM5rLK2aVD.htm](equipment/weapon-00-znIsDYwM5rLK2aVD.htm)|Nine-Ring Sword|auto-trad|
|[weapon-00-zO6Fvq7ghEXpJgJR.htm](equipment/weapon-00-zO6Fvq7ghEXpJgJR.htm)|Tricky Pick|auto-trad|
|[weapon-00-zSAoyzcf3nSeZCiF.htm](equipment/weapon-00-zSAoyzcf3nSeZCiF.htm)|Kama|auto-trad|
|[weapon-00-zWp7Kcn7mhKyGFxW.htm](equipment/weapon-00-zWp7Kcn7mhKyGFxW.htm)|Sun Sling|auto-trad|
|[weapon-00-zXbZn5HJf81tquPm.htm](equipment/weapon-00-zXbZn5HJf81tquPm.htm)|War Lance|auto-trad|
|[weapon-01-10J0WjnfgRvriLXc.htm](equipment/weapon-01-10J0WjnfgRvriLXc.htm)|Gun Sword|auto-trad|
|[weapon-01-2dHdOVBySillcOGM.htm](equipment/weapon-01-2dHdOVBySillcOGM.htm)|Backpack Ballista|auto-trad|
|[weapon-01-3Sij5RwC6Z1ZVuFp.htm](equipment/weapon-01-3Sij5RwC6Z1ZVuFp.htm)|Skunk Bomb (Lesser)|auto-trad|
|[weapon-01-3T3ayvgw9HDn05Mz.htm](equipment/weapon-01-3T3ayvgw9HDn05Mz.htm)|Combat Grapnel|auto-trad|
|[weapon-01-68bqnb84kst3hFjP.htm](equipment/weapon-01-68bqnb84kst3hFjP.htm)|Backpack Catapult|auto-trad|
|[weapon-01-6mgB6Wv8X65pFMRL.htm](equipment/weapon-01-6mgB6Wv8X65pFMRL.htm)|Three Peaked Tree|auto-trad|
|[weapon-01-8ncDuZSIZAHl7PW8.htm](equipment/weapon-01-8ncDuZSIZAHl7PW8.htm)|Repeating Crossbow|auto-trad|
|[weapon-01-AFR01HVd7DcZvkpP.htm](equipment/weapon-01-AFR01HVd7DcZvkpP.htm)|Bottled Lightning (Lesser)|auto-trad|
|[weapon-01-bP608Wb1l9ZY1zwb.htm](equipment/weapon-01-bP608Wb1l9ZY1zwb.htm)|Repeating Heavy Crossbow|auto-trad|
|[weapon-01-c124j3cpv8rl5MLp.htm](equipment/weapon-01-c124j3cpv8rl5MLp.htm)|Peshpine Grenade (Lesser)|auto-trad|
|[weapon-01-caWhNJ2ahCzZjr55.htm](equipment/weapon-01-caWhNJ2ahCzZjr55.htm)|Gnome Amalgam Musket|auto-trad|
|[weapon-01-cFHUt5tTA7jVPtRh.htm](equipment/weapon-01-cFHUt5tTA7jVPtRh.htm)|Explosive Dogslicer|auto-trad|
|[weapon-01-DFZj7RCrk6rk9fhf.htm](equipment/weapon-01-DFZj7RCrk6rk9fhf.htm)|Granny's Hedge Trimmer|auto-trad|
|[weapon-01-DHKCkxAGveLE3Jnf.htm](equipment/weapon-01-DHKCkxAGveLE3Jnf.htm)|Shobhad Longrifle|auto-trad|
|[weapon-01-ds0OdA989ZZw9km1.htm](equipment/weapon-01-ds0OdA989ZZw9km1.htm)|Dread Ampoule (Lesser)|auto-trad|
|[weapon-01-EGhUorZhB7nV73Ev.htm](equipment/weapon-01-EGhUorZhB7nV73Ev.htm)|Unholy Water|auto-trad|
|[weapon-01-EpxmUtLpCkE8R6KJ.htm](equipment/weapon-01-EpxmUtLpCkE8R6KJ.htm)|Frost Vial (Lesser)|auto-trad|
|[weapon-01-EvBQcbkiYUusdFKY.htm](equipment/weapon-01-EvBQcbkiYUusdFKY.htm)|Phalanx Piercer|auto-trad|
|[weapon-01-GG7XnMngd9MnxhHb.htm](equipment/weapon-01-GG7XnMngd9MnxhHb.htm)|Big Boom Gun|auto-trad|
|[weapon-01-gO5dOlPBk57bg2x5.htm](equipment/weapon-01-gO5dOlPBk57bg2x5.htm)|Slide Pistol|auto-trad|
|[weapon-01-Hh3F4DEP6aVulwQN.htm](equipment/weapon-01-Hh3F4DEP6aVulwQN.htm)|Stiletto Pen|auto-trad|
|[weapon-01-HtcjYkGS8vuC0e7T.htm](equipment/weapon-01-HtcjYkGS8vuC0e7T.htm)|Redpitch Bomb (Lesser)|auto-trad|
|[weapon-01-iSceAVMkVv1uKK7t.htm](equipment/weapon-01-iSceAVMkVv1uKK7t.htm)|Aldori Dueling Sword|auto-trad|
|[weapon-01-j8ajvNqyyQGBpBch.htm](equipment/weapon-01-j8ajvNqyyQGBpBch.htm)|Blight Bomb (Lesser)|auto-trad|
|[weapon-01-JAOzIeqKr5hULumB.htm](equipment/weapon-01-JAOzIeqKr5hULumB.htm)|Alchemical Gauntlet|auto-trad|
|[weapon-01-jcIabnkJgjwzK6Og.htm](equipment/weapon-01-jcIabnkJgjwzK6Og.htm)|Dwarven Scattergun|auto-trad|
|[weapon-01-l3IOo6AiQj7pxPhB.htm](equipment/weapon-01-l3IOo6AiQj7pxPhB.htm)|Dragon's Crest|auto-trad|
|[weapon-01-LNd5u19GqC51ngby.htm](equipment/weapon-01-LNd5u19GqC51ngby.htm)|Ghost Charge (Lesser)|auto-trad|
|[weapon-01-M1k5QQc1qQLxzyCK.htm](equipment/weapon-01-M1k5QQc1qQLxzyCK.htm)|Acid Flask (Lesser)|auto-trad|
|[weapon-01-MMYL2rfbYNWT0Rs1.htm](equipment/weapon-01-MMYL2rfbYNWT0Rs1.htm)|Spoon Gun|auto-trad|
|[weapon-01-NEZfXSasFdLdQFBi.htm](equipment/weapon-01-NEZfXSasFdLdQFBi.htm)|Jezail|auto-trad|
|[weapon-01-ogv5nwwrc3sU8DnP.htm](equipment/weapon-01-ogv5nwwrc3sU8DnP.htm)|Dueling Pistol|auto-trad|
|[weapon-01-OOqno1OwqJQBEtXe.htm](equipment/weapon-01-OOqno1OwqJQBEtXe.htm)|Tallow Bomb (Lesser)|auto-trad|
|[weapon-01-oUaeLZLK7WfolqBb.htm](equipment/weapon-01-oUaeLZLK7WfolqBb.htm)|Axe Musket|auto-trad|
|[weapon-01-OUOSAKU1tSipWtty.htm](equipment/weapon-01-OUOSAKU1tSipWtty.htm)|Bioluminescence Bomb|auto-trad|
|[weapon-01-rePjr9KceepKIDuB.htm](equipment/weapon-01-rePjr9KceepKIDuB.htm)|Double-Barreled Musket|auto-trad|
|[weapon-01-sF9uOLAjQiSmt9i3.htm](equipment/weapon-01-sF9uOLAjQiSmt9i3.htm)|Boarding Pike|auto-trad|
|[weapon-01-t2A5PzInAqmErLzK.htm](equipment/weapon-01-t2A5PzInAqmErLzK.htm)|Goo Grenade (Lesser)|auto-trad|
|[weapon-01-T6Appwwl6nUl56Xj.htm](equipment/weapon-01-T6Appwwl6nUl56Xj.htm)|Tanglefoot Bag (Lesser)|auto-trad|
|[weapon-01-tm5l8uXtQQGMTvgY.htm](equipment/weapon-01-tm5l8uXtQQGMTvgY.htm)|Twigjack Sack (Lesser)|auto-trad|
|[weapon-01-ucz1WdBA0Ma34OsS.htm](equipment/weapon-01-ucz1WdBA0Ma34OsS.htm)|Flying Talon|auto-trad|
|[weapon-01-udm5X6TXm5vdIo4h.htm](equipment/weapon-01-udm5X6TXm5vdIo4h.htm)|Sukgung|auto-trad|
|[weapon-01-UogGzQfXDc32E1p7.htm](equipment/weapon-01-UogGzQfXDc32E1p7.htm)|Barricade Buster|auto-trad|
|[weapon-01-vRCH0cQJMllWvpfU.htm](equipment/weapon-01-vRCH0cQJMllWvpfU.htm)|Polytool|auto-trad|
|[weapon-01-WNWnpAsmFoFUlQLK.htm](equipment/weapon-01-WNWnpAsmFoFUlQLK.htm)|Alignment Ampoule (Lesser)|auto-trad|
|[weapon-01-WS78LUHzlpeONMRo.htm](equipment/weapon-01-WS78LUHzlpeONMRo.htm)|Scrollstaff|auto-trad|
|[weapon-01-Xnqglykl3Cif8rN9.htm](equipment/weapon-01-Xnqglykl3Cif8rN9.htm)|Thunderstone (Lesser)|auto-trad|
|[weapon-01-xZFT5JdEvDi7q467.htm](equipment/weapon-01-xZFT5JdEvDi7q467.htm)|Dwarven Daisy (Lesser)|auto-trad|
|[weapon-01-yd3kEK21YknZLlcT.htm](equipment/weapon-01-yd3kEK21YknZLlcT.htm)|Alchemist's Fire (Lesser)|auto-trad|
|[weapon-01-yyZ3iD8IXMIrrWL1.htm](equipment/weapon-01-yyZ3iD8IXMIrrWL1.htm)|Switchscythe|auto-trad|
|[weapon-01-z9T4c1hXwOotsMCp.htm](equipment/weapon-01-z9T4c1hXwOotsMCp.htm)|Holy Water|auto-trad|
|[weapon-01-zcrIuILnp6oh4dr9.htm](equipment/weapon-01-zcrIuILnp6oh4dr9.htm)|Repeating Hand Crossbow|auto-trad|
|[weapon-01-zHJEzmoybIfX1LaV.htm](equipment/weapon-01-zHJEzmoybIfX1LaV.htm)|Hammer Gun|auto-trad|
|[weapon-02-FNDq4NFSN0g2HKWO.htm](equipment/weapon-02-FNDq4NFSN0g2HKWO.htm)|Handwraps of Mighty Blows|auto-trad|
|[weapon-02-iGT4UeQq3YFcNpEb.htm](equipment/weapon-02-iGT4UeQq3YFcNpEb.htm)|Bottled Sunlight (Lesser)|auto-trad|
|[weapon-03-158vwM1andv8DbRI.htm](equipment/weapon-03-158vwM1andv8DbRI.htm)|Blight Bomb (Moderate)|auto-trad|
|[weapon-03-4pVGwkVi6izoUuLC.htm](equipment/weapon-03-4pVGwkVi6izoUuLC.htm)|Tallow Bomb (Moderate)|auto-trad|
|[weapon-03-8TEundYBdonchDj1.htm](equipment/weapon-03-8TEundYBdonchDj1.htm)|Staff of Air|auto-trad|
|[weapon-03-97QyNEOAyYLdGaYc.htm](equipment/weapon-03-97QyNEOAyYLdGaYc.htm)|Bottled Lightning (Moderate)|auto-trad|
|[weapon-03-AvGZqDfalwwbYFvA.htm](equipment/weapon-03-AvGZqDfalwwbYFvA.htm)|Peshpine Grenade (Moderate)|auto-trad|
|[weapon-03-Do4rJuA1nhr2TTiF.htm](equipment/weapon-03-Do4rJuA1nhr2TTiF.htm)|Dwarven Daisy (Moderate)|auto-trad|
|[weapon-03-EuTZxxwdVeN6Xg3A.htm](equipment/weapon-03-EuTZxxwdVeN6Xg3A.htm)|Storm Hammer|auto-trad|
|[weapon-03-evBPzM1VsuYcoenn.htm](equipment/weapon-03-evBPzM1VsuYcoenn.htm)|Tanglefoot Bag (Moderate)|auto-trad|
|[weapon-03-frCSW1zLFYiYpjdR.htm](equipment/weapon-03-frCSW1zLFYiYpjdR.htm)|Staff of Water|auto-trad|
|[weapon-03-gWr4q4HiyGhETA8H.htm](equipment/weapon-03-gWr4q4HiyGhETA8H.htm)|Alchemist's Fire (Moderate)|auto-trad|
|[weapon-03-IvFEJqp2MUew65nQ.htm](equipment/weapon-03-IvFEJqp2MUew65nQ.htm)|Dread Ampoule (Moderate)|auto-trad|
|[weapon-03-Ix5w4UNElK0ncBm2.htm](equipment/weapon-03-Ix5w4UNElK0ncBm2.htm)|Twigjack Sack (Moderate)|auto-trad|
|[weapon-03-JOaELkzLWTywhn5Z.htm](equipment/weapon-03-JOaELkzLWTywhn5Z.htm)|Thunderstone (Moderate)|auto-trad|
|[weapon-03-K7vNtVcHUKB9wks1.htm](equipment/weapon-03-K7vNtVcHUKB9wks1.htm)|Alignment Ampoule (Moderate)|auto-trad|
|[weapon-03-Kx6FQS5GyVB6jlrW.htm](equipment/weapon-03-Kx6FQS5GyVB6jlrW.htm)|Fighter's Fork|auto-trad|
|[weapon-03-m091zbWD9EoH6kWC.htm](equipment/weapon-03-m091zbWD9EoH6kWC.htm)|Skunk Bomb (Moderate)|auto-trad|
|[weapon-03-nVvH1ZcM7OwIVIs8.htm](equipment/weapon-03-nVvH1ZcM7OwIVIs8.htm)|Frost Vial (Moderate)|auto-trad|
|[weapon-03-onFxlajKyWpHZcXt.htm](equipment/weapon-03-onFxlajKyWpHZcXt.htm)|Smoking Sword|auto-trad|
|[weapon-03-opfpl1JmKgrfds9P.htm](equipment/weapon-03-opfpl1JmKgrfds9P.htm)|Staff of Fire|auto-trad|
|[weapon-03-pX3rpVDBLqClcL9M.htm](equipment/weapon-03-pX3rpVDBLqClcL9M.htm)|Staff of Earth|auto-trad|
|[weapon-03-q6ZvspNDkzJSP6dg.htm](equipment/weapon-03-q6ZvspNDkzJSP6dg.htm)|Retribution Axe|auto-trad|
|[weapon-03-qWRigVOjyLvSW6UA.htm](equipment/weapon-03-qWRigVOjyLvSW6UA.htm)|Redpitch Bomb (Moderate)|auto-trad|
|[weapon-03-RapaDb10oJZOjujo.htm](equipment/weapon-03-RapaDb10oJZOjujo.htm)|Hunter's Bow|auto-trad|
|[weapon-03-RgVOC3rpptrwENbu.htm](equipment/weapon-03-RgVOC3rpptrwENbu.htm)|Little Love|auto-trad|
|[weapon-03-SgtqZxt26BdjUmEB.htm](equipment/weapon-03-SgtqZxt26BdjUmEB.htm)|Acid Flask (Moderate)|auto-trad|
|[weapon-03-VSxLv1JRSo10waL0.htm](equipment/weapon-03-VSxLv1JRSo10waL0.htm)|Ghast Stiletto|auto-trad|
|[weapon-03-Wf94e8YNhZdIvWc9.htm](equipment/weapon-03-Wf94e8YNhZdIvWc9.htm)|Ghost Charge (Moderate)|auto-trad|
|[weapon-03-wHfhyAqJgKP9v3Dv.htm](equipment/weapon-03-wHfhyAqJgKP9v3Dv.htm)|Wordreaper|auto-trad|
|[weapon-03-wi97vRj8IXx3wLmZ.htm](equipment/weapon-03-wi97vRj8IXx3wLmZ.htm)|Goo Grenade (Moderate)|auto-trad|
|[weapon-04-3OkOKxCee9WruGU5.htm](equipment/weapon-04-3OkOKxCee9WruGU5.htm)|Staff of Healing|auto-trad|
|[weapon-04-6TC4FywUTzVAK97v.htm](equipment/weapon-04-6TC4FywUTzVAK97v.htm)|Composer Staff|auto-trad|
|[weapon-04-qLhb5ZvIDOmbwvc9.htm](equipment/weapon-04-qLhb5ZvIDOmbwvc9.htm)|Blast Lance|auto-trad|
|[weapon-04-RN6rEc8eSkruNLPW.htm](equipment/weapon-04-RN6rEc8eSkruNLPW.htm)|Exquisite Sword Cane|auto-trad|
|[weapon-04-SeZfwtBYwmxKrphR.htm](equipment/weapon-04-SeZfwtBYwmxKrphR.htm)|Poisoner's Staff|auto-trad|
|[weapon-04-WcuknnE3xYfSdbhm.htm](equipment/weapon-04-WcuknnE3xYfSdbhm.htm)|Animal Staff|auto-trad|
|[weapon-04-wIQjJGLZOKCY1es9.htm](equipment/weapon-04-wIQjJGLZOKCY1es9.htm)|Bottled Sunlight (Moderate)|auto-trad|
|[weapon-04-xwiZBOjispKVZzGA.htm](equipment/weapon-04-xwiZBOjispKVZzGA.htm)|Mentalist's Staff|auto-trad|
|[weapon-05-1eiaTQo9SKiybP8G.htm](equipment/weapon-05-1eiaTQo9SKiybP8G.htm)|Scizore of the Crab|auto-trad|
|[weapon-05-1S3RgLWa1DBitB9V.htm](equipment/weapon-05-1S3RgLWa1DBitB9V.htm)|Dagger of Eternal Sleep|auto-trad|
|[weapon-05-2KNAip9W6IoBrfIU.htm](equipment/weapon-05-2KNAip9W6IoBrfIU.htm)|Caterwaul Sling|auto-trad|
|[weapon-05-6HOQwVUQNplJsrhq.htm](equipment/weapon-05-6HOQwVUQNplJsrhq.htm)|Reaper's Lancet|auto-trad|
|[weapon-05-6rWcLbV82KYwaHo1.htm](equipment/weapon-05-6rWcLbV82KYwaHo1.htm)|Dog-Bone Knife|auto-trad|
|[weapon-05-AYdpUABAZeZnSA7s.htm](equipment/weapon-05-AYdpUABAZeZnSA7s.htm)|Infiltrator's Accessory|auto-trad|
|[weapon-05-bcZB7xmlQy8UWqe1.htm](equipment/weapon-05-bcZB7xmlQy8UWqe1.htm)|Heartripper Blade|auto-trad|
|[weapon-05-Dti9PZZoty6We8OV.htm](equipment/weapon-05-Dti9PZZoty6We8OV.htm)|Cinderclaw Gauntlet|auto-trad|
|[weapon-05-enBDzoapUkvk4WVu.htm](equipment/weapon-05-enBDzoapUkvk4WVu.htm)|Thundercrasher|auto-trad|
|[weapon-05-FSIDKg5gYiXlr39j.htm](equipment/weapon-05-FSIDKg5gYiXlr39j.htm)|Alicorn Lance|auto-trad|
|[weapon-05-mf5eeIdKjfSZ0K3p.htm](equipment/weapon-05-mf5eeIdKjfSZ0K3p.htm)|Solar Shellflower|auto-trad|
|[weapon-05-NzVp3RKhBkAPbe3g.htm](equipment/weapon-05-NzVp3RKhBkAPbe3g.htm)|Metronomic Hammer|auto-trad|
|[weapon-05-OKAR7GIyJac8dmsi.htm](equipment/weapon-05-OKAR7GIyJac8dmsi.htm)|Singing Sword|auto-trad|
|[weapon-05-wWXnr3xw5TzDL1M7.htm](equipment/weapon-05-wWXnr3xw5TzDL1M7.htm)|Grasp of Droskar|auto-trad|
|[weapon-05-YPo7jLMxob6E2p8b.htm](equipment/weapon-05-YPo7jLMxob6E2p8b.htm)|Auspicious Scepter|auto-trad|
|[weapon-05-ZvIEJCY60fHqzl6r.htm](equipment/weapon-05-ZvIEJCY60fHqzl6r.htm)|Dagger of Venom|auto-trad|
|[weapon-06-0yp4hAgJKt9Al2lz.htm](equipment/weapon-06-0yp4hAgJKt9Al2lz.htm)|Staff of the Desert Winds|auto-trad|
|[weapon-06-17Oza88ts0ASngdw.htm](equipment/weapon-06-17Oza88ts0ASngdw.htm)|Acrobat's Staff|auto-trad|
|[weapon-06-2z4GSaKaVX22kzPU.htm](equipment/weapon-06-2z4GSaKaVX22kzPU.htm)|Gluttonous Spear|auto-trad|
|[weapon-06-3qn56dTO5TYJHjbF.htm](equipment/weapon-06-3qn56dTO5TYJHjbF.htm)|Spike Launcher|auto-trad|
|[weapon-06-4jZqYIyouoK7xe8u.htm](equipment/weapon-06-4jZqYIyouoK7xe8u.htm)|Dragonscale Staff|auto-trad|
|[weapon-06-4kyf3ScoJ4TvhPG3.htm](equipment/weapon-06-4kyf3ScoJ4TvhPG3.htm)|Arboreal's Revenge|auto-trad|
|[weapon-06-6IJZamE4JkERQumf.htm](equipment/weapon-06-6IJZamE4JkERQumf.htm)|Staff of Conjuration|auto-trad|
|[weapon-06-96VBd7CV8NQyv3lP.htm](equipment/weapon-06-96VBd7CV8NQyv3lP.htm)|Staff of Evocation|auto-trad|
|[weapon-06-bh1Jr4YqfXQe7r9I.htm](equipment/weapon-06-bh1Jr4YqfXQe7r9I.htm)|Librarian Staff|auto-trad|
|[weapon-06-c40Zn2TCRr3inIBA.htm](equipment/weapon-06-c40Zn2TCRr3inIBA.htm)|Staff of Necromancy|auto-trad|
|[weapon-06-C7ca35oypTp2xJbj.htm](equipment/weapon-06-C7ca35oypTp2xJbj.htm)|Lady's Knife|auto-trad|
|[weapon-06-dYydONniFSGwAdL1.htm](equipment/weapon-06-dYydONniFSGwAdL1.htm)|Iris of the Sky|auto-trad|
|[weapon-06-eaYNVLnTX9VejnaA.htm](equipment/weapon-06-eaYNVLnTX9VejnaA.htm)|Staff of Enchantment|auto-trad|
|[weapon-06-EYRrABqjUYPrhrZr.htm](equipment/weapon-06-EYRrABqjUYPrhrZr.htm)|Staff of Abjuration|auto-trad|
|[weapon-06-fcRTJBvy1RqXr3ow.htm](equipment/weapon-06-fcRTJBvy1RqXr3ow.htm)|Lyrakien Staff|auto-trad|
|[weapon-06-HMjn8Ln1Wp1Ld2S3.htm](equipment/weapon-06-HMjn8Ln1Wp1Ld2S3.htm)|Staff of Final Rest|auto-trad|
|[weapon-06-hvMIJKY1mKXDmg1V.htm](equipment/weapon-06-hvMIJKY1mKXDmg1V.htm)|Windlass Bola|auto-trad|
|[weapon-06-KXoJIPHbdz746Rec.htm](equipment/weapon-06-KXoJIPHbdz746Rec.htm)|Cooperative Blade|auto-trad|
|[weapon-06-ObmYN6I64Pjj7yEA.htm](equipment/weapon-06-ObmYN6I64Pjj7yEA.htm)|Staff of Divination|auto-trad|
|[weapon-06-p1pysEaGypaNxTEL.htm](equipment/weapon-06-p1pysEaGypaNxTEL.htm)|Staff of Transmutation|auto-trad|
|[weapon-06-PFmBfQ4xENK3jX4M.htm](equipment/weapon-06-PFmBfQ4xENK3jX4M.htm)|Conflagration Club|auto-trad|
|[weapon-06-pjUACbrAXNIy9O7S.htm](equipment/weapon-06-pjUACbrAXNIy9O7S.htm)|Twining Staff|auto-trad|
|[weapon-06-Q2QHZdBaoOLkE1lX.htm](equipment/weapon-06-Q2QHZdBaoOLkE1lX.htm)|Staff of Illusion|auto-trad|
|[weapon-06-rsqPU9IRM6tk2eHc.htm](equipment/weapon-06-rsqPU9IRM6tk2eHc.htm)|Chatterer of Follies|auto-trad|
|[weapon-06-sQFQlglhORQsxBKS.htm](equipment/weapon-06-sQFQlglhORQsxBKS.htm)|Verdant Staff|auto-trad|
|[weapon-06-tpKD2TZEzAToow1O.htm](equipment/weapon-06-tpKD2TZEzAToow1O.htm)|Ringmaster's Staff|auto-trad|
|[weapon-06-tRmoj42jniQOdjYS.htm](equipment/weapon-06-tRmoj42jniQOdjYS.htm)|Accursed Staff|auto-trad|
|[weapon-06-UQ0h9IVtHla8gWT7.htm](equipment/weapon-06-UQ0h9IVtHla8gWT7.htm)|Guardian Staff|auto-trad|
|[weapon-06-uVcePmysXUFhOebP.htm](equipment/weapon-06-uVcePmysXUFhOebP.htm)|Soul Chain|auto-trad|
|[weapon-06-WA5eoFFuAsyx7A2t.htm](equipment/weapon-06-WA5eoFFuAsyx7A2t.htm)|Staff of Impossible Visions|auto-trad|
|[weapon-06-XeTmuhuNnhGf7c4t.htm](equipment/weapon-06-XeTmuhuNnhGf7c4t.htm)|Staff of Providence|auto-trad|
|[weapon-07-174fUKNwSgbKwroU.htm](equipment/weapon-07-174fUKNwSgbKwroU.htm)|Lady's Spiral|auto-trad|
|[weapon-07-1IicJ7ESDNhGUqDE.htm](equipment/weapon-07-1IicJ7ESDNhGUqDE.htm)|Tentacle Cannon|auto-trad|
|[weapon-07-4ADeEQGQJR0ju2Nd.htm](equipment/weapon-07-4ADeEQGQJR0ju2Nd.htm)|Man-Feller|auto-trad|
|[weapon-07-6aubQ21gRc5EMnbT.htm](equipment/weapon-07-6aubQ21gRc5EMnbT.htm)|Undead Scourge|auto-trad|
|[weapon-07-8LEKZdpipKp3jYfx.htm](equipment/weapon-07-8LEKZdpipKp3jYfx.htm)|Liar's Gun|auto-trad|
|[weapon-07-atB2ewbcBMWvhjNT.htm](equipment/weapon-07-atB2ewbcBMWvhjNT.htm)|Guiding Star|auto-trad|
|[weapon-07-eoSl6Rbop1EX5DwP.htm](equipment/weapon-07-eoSl6Rbop1EX5DwP.htm)|Spy Staff|auto-trad|
|[weapon-07-HoxkzrS7jhqmWpla.htm](equipment/weapon-07-HoxkzrS7jhqmWpla.htm)|Beast Staff|auto-trad|
|[weapon-07-JtCO2nielwAg1x9m.htm](equipment/weapon-07-JtCO2nielwAg1x9m.htm)|Zombie Staff|auto-trad|
|[weapon-07-jwa10xel66Wc7U3p.htm](equipment/weapon-07-jwa10xel66Wc7U3p.htm)|Staff of Nature's Cunning|auto-trad|
|[weapon-07-KoyxJDibsKJh24am.htm](equipment/weapon-07-KoyxJDibsKJh24am.htm)|Azarim|auto-trad|
|[weapon-07-KQRrAVdcRqtd0Lq2.htm](equipment/weapon-07-KQRrAVdcRqtd0Lq2.htm)|Fulminating Spear|auto-trad|
|[weapon-07-QyzNxesxaSdPo3Pd.htm](equipment/weapon-07-QyzNxesxaSdPo3Pd.htm)|Alghollthu Lash|auto-trad|
|[weapon-07-UGqhitKOWCsqs9UQ.htm](equipment/weapon-07-UGqhitKOWCsqs9UQ.htm)|Bellicose Dagger|auto-trad|
|[weapon-07-USzeQdvsXz6P5L2k.htm](equipment/weapon-07-USzeQdvsXz6P5L2k.htm)|Slime Whip|auto-trad|
|[weapon-07-v6LpzpIA0BmKvEtK.htm](equipment/weapon-07-v6LpzpIA0BmKvEtK.htm)|Spellguard Blade|auto-trad|
|[weapon-08-0RCC0fOg1Lp7f79I.htm](equipment/weapon-08-0RCC0fOg1Lp7f79I.htm)|Mentalist's Staff (Greater)|auto-trad|
|[weapon-08-2UX3tlC5vKeQjG7t.htm](equipment/weapon-08-2UX3tlC5vKeQjG7t.htm)|Spiritsight Crossbow|auto-trad|
|[weapon-08-8No84rsBOCVCkXJK.htm](equipment/weapon-08-8No84rsBOCVCkXJK.htm)|Staff of Illumination|auto-trad|
|[weapon-08-9GUSQFJZarLs5tQC.htm](equipment/weapon-08-9GUSQFJZarLs5tQC.htm)|Staff of Air (Greater)|auto-trad|
|[weapon-08-9jn87Bnp2kGnDFPQ.htm](equipment/weapon-08-9jn87Bnp2kGnDFPQ.htm)|Breath Blaster|auto-trad|
|[weapon-08-BdNRM9VTxs5BYq0C.htm](equipment/weapon-08-BdNRM9VTxs5BYq0C.htm)|Jax|auto-trad|
|[weapon-08-BJuqdqbflAcuCJkt.htm](equipment/weapon-08-BJuqdqbflAcuCJkt.htm)|Clockwork Macuahuitl|auto-trad|
|[weapon-08-bT9azgpc96DNbitA.htm](equipment/weapon-08-bT9azgpc96DNbitA.htm)|Animal Staff (Greater)|auto-trad|
|[weapon-08-DwNELc8YIKTb2uj1.htm](equipment/weapon-08-DwNELc8YIKTb2uj1.htm)|Staff of Water (Greater)|auto-trad|
|[weapon-08-FCA7CE4mK85SVLz3.htm](equipment/weapon-08-FCA7CE4mK85SVLz3.htm)|Eclipse|auto-trad|
|[weapon-08-GFwfZrBGjCZTDcuw.htm](equipment/weapon-08-GFwfZrBGjCZTDcuw.htm)|Vampire-Fang Morningstar|auto-trad|
|[weapon-08-jaM12dWpIozRiIhk.htm](equipment/weapon-08-jaM12dWpIozRiIhk.htm)|Staff of Earth (Greater)|auto-trad|
|[weapon-08-JLoMkMq727XeMmlR.htm](equipment/weapon-08-JLoMkMq727XeMmlR.htm)|Vine Whip|auto-trad|
|[weapon-08-KcjaeMgrsBGgwUWL.htm](equipment/weapon-08-KcjaeMgrsBGgwUWL.htm)|Staff of Fire (Greater)|auto-trad|
|[weapon-08-KSHZBPFxPD6uCZXM.htm](equipment/weapon-08-KSHZBPFxPD6uCZXM.htm)|Composer Staff (Greater)|auto-trad|
|[weapon-08-O5da0gpQqR9MwGLF.htm](equipment/weapon-08-O5da0gpQqR9MwGLF.htm)|Mindlance|auto-trad|
|[weapon-08-RijrZ8lnaYrGTeMV.htm](equipment/weapon-08-RijrZ8lnaYrGTeMV.htm)|Blast Lance (Greater)|auto-trad|
|[weapon-08-RZkttcpkv4qLs1Lk.htm](equipment/weapon-08-RZkttcpkv4qLs1Lk.htm)|Hundred-Moth Caress|auto-trad|
|[weapon-08-sfKzk4bzplyT4zax.htm](equipment/weapon-08-sfKzk4bzplyT4zax.htm)|Sunken Pistol|auto-trad|
|[weapon-08-tU4PKaoo1XuPkKg1.htm](equipment/weapon-08-tU4PKaoo1XuPkKg1.htm)|Boreal Staff|auto-trad|
|[weapon-08-xr99pbmKLO2eDMk5.htm](equipment/weapon-08-xr99pbmKLO2eDMk5.htm)|Poisoner's Staff (Greater)|auto-trad|
|[weapon-08-XSwEE8wjHr6UXzpw.htm](equipment/weapon-08-XSwEE8wjHr6UXzpw.htm)|Staff of Healing (Greater)|auto-trad|
|[weapon-08-zakdImp4CqahnFfc.htm](equipment/weapon-08-zakdImp4CqahnFfc.htm)|Habu's Cudgel|auto-trad|
|[weapon-09-B0R1MtLnQvBwnOne.htm](equipment/weapon-09-B0R1MtLnQvBwnOne.htm)|Sonic Tuning Mace|auto-trad|
|[weapon-09-CtUPO3qFZ2Y6lozq.htm](equipment/weapon-09-CtUPO3qFZ2Y6lozq.htm)|Skeletal Claw|auto-trad|
|[weapon-09-DXidUrzzkwWIMNMD.htm](equipment/weapon-09-DXidUrzzkwWIMNMD.htm)|Whip of Compliance|auto-trad|
|[weapon-09-ET88qMrIBXBCWI5J.htm](equipment/weapon-09-ET88qMrIBXBCWI5J.htm)|Spider Gun (Greater)|auto-trad|
|[weapon-09-Il75ytwHrdwAAOwe.htm](equipment/weapon-09-Il75ytwHrdwAAOwe.htm)|Reaper's Crescent|auto-trad|
|[weapon-09-k5P9YZO4ARlE4By3.htm](equipment/weapon-09-k5P9YZO4ARlE4By3.htm)|Crimson Brand|auto-trad|
|[weapon-09-PVdl7StKFu6QPTVl.htm](equipment/weapon-09-PVdl7StKFu6QPTVl.htm)|Screech Shooter|auto-trad|
|[weapon-09-ssS1yEAZwZun8yFw.htm](equipment/weapon-09-ssS1yEAZwZun8yFw.htm)|Growth Gun|auto-trad|
|[weapon-09-TO8UvbBy1NfVbrdB.htm](equipment/weapon-09-TO8UvbBy1NfVbrdB.htm)|Devil's Trident|auto-trad|
|[weapon-09-Us5dVXQHJzNjY0X8.htm](equipment/weapon-09-Us5dVXQHJzNjY0X8.htm)|Grisly Scythe|auto-trad|
|[weapon-09-w5ZX1R3dPvuLcuRx.htm](equipment/weapon-09-w5ZX1R3dPvuLcuRx.htm)|Gloom Blade|auto-trad|
|[weapon-09-X95j0ZdqjXJkGmfZ.htm](equipment/weapon-09-X95j0ZdqjXJkGmfZ.htm)|Spellender|auto-trad|
|[weapon-09-XOXUFjBGO6azymyb.htm](equipment/weapon-09-XOXUFjBGO6azymyb.htm)|Erraticannon|auto-trad|
|[weapon-09-ZiaAFSG9zQY0CXdL.htm](equipment/weapon-09-ZiaAFSG9zQY0CXdL.htm)|Vine of Roses|auto-trad|
|[weapon-10-4nte7LbGGPwViu1U.htm](equipment/weapon-10-4nte7LbGGPwViu1U.htm)|North Wind's Night Verse|auto-trad|
|[weapon-10-5SVttGODx1TkyhTU.htm](equipment/weapon-10-5SVttGODx1TkyhTU.htm)|Immolation Clan Pistol|auto-trad|
|[weapon-10-6u14xOkKhnB9lVy0.htm](equipment/weapon-10-6u14xOkKhnB9lVy0.htm)|Staff of Illusion (Greater)|auto-trad|
|[weapon-10-8S81RvynBS09rIOF.htm](equipment/weapon-10-8S81RvynBS09rIOF.htm)|South Wind's Scorch Song|auto-trad|
|[weapon-10-Ab5EjJyoRSec5YrW.htm](equipment/weapon-10-Ab5EjJyoRSec5YrW.htm)|Staff of Necromancy (Greater)|auto-trad|
|[weapon-10-aPbDAb6cJKPvCHk2.htm](equipment/weapon-10-aPbDAb6cJKPvCHk2.htm)|Staff of Nature's Vengeance|auto-trad|
|[weapon-10-E63MaprijBuUK9Om.htm](equipment/weapon-10-E63MaprijBuUK9Om.htm)|Staff of Divination (Greater)|auto-trad|
|[weapon-10-eIGcGgQDBihvJsEo.htm](equipment/weapon-10-eIGcGgQDBihvJsEo.htm)|Accursed Staff (Greater)|auto-trad|
|[weapon-10-ER9gH5A7elGPdHag.htm](equipment/weapon-10-ER9gH5A7elGPdHag.htm)|Grounding Spike|auto-trad|
|[weapon-10-ExExv8dJVcCj4rL0.htm](equipment/weapon-10-ExExv8dJVcCj4rL0.htm)|Staff of Enchantment (Greater)|auto-trad|
|[weapon-10-HRNQ1j2OM54Qakhp.htm](equipment/weapon-10-HRNQ1j2OM54Qakhp.htm)|Staff of Abjuration (Greater)|auto-trad|
|[weapon-10-jIX2T21YvEsRaaqp.htm](equipment/weapon-10-jIX2T21YvEsRaaqp.htm)|Guardian Staff (Greater)|auto-trad|
|[weapon-10-n4p8VFMKYACf200W.htm](equipment/weapon-10-n4p8VFMKYACf200W.htm)|Polarizing Mace|auto-trad|
|[weapon-10-q1mt1F9q1XmJWAKY.htm](equipment/weapon-10-q1mt1F9q1XmJWAKY.htm)|Pact-Bound Pistol|auto-trad|
|[weapon-10-RCQ4DzDh34ZBiTxI.htm](equipment/weapon-10-RCQ4DzDh34ZBiTxI.htm)|Staff of Evocation (Greater)|auto-trad|
|[weapon-10-T1L6XbgMqJLDv2Pi.htm](equipment/weapon-10-T1L6XbgMqJLDv2Pi.htm)|Staff of Providence (Greater)|auto-trad|
|[weapon-10-uepeQsiOEIqTyNsx.htm](equipment/weapon-10-uepeQsiOEIqTyNsx.htm)|Lyrakien Staff (Greater)|auto-trad|
|[weapon-10-xec2kwqFjxHjFwLa.htm](equipment/weapon-10-xec2kwqFjxHjFwLa.htm)|Staff of Final Rest (Greater)|auto-trad|
|[weapon-10-xQf98bdGgE7UiBe4.htm](equipment/weapon-10-xQf98bdGgE7UiBe4.htm)|Staff of Conjuration (Greater)|auto-trad|
|[weapon-10-YxLC2Np7cEBnZzGX.htm](equipment/weapon-10-YxLC2Np7cEBnZzGX.htm)|Staff of Transmutation (Greater)|auto-trad|
|[weapon-11-28osNZINXLWzqzUL.htm](equipment/weapon-11-28osNZINXLWzqzUL.htm)|Ghost Charge (Greater)|auto-trad|
|[weapon-11-4nEdlX9GOlXGyfpl.htm](equipment/weapon-11-4nEdlX9GOlXGyfpl.htm)|Spy Staff (Greater)|auto-trad|
|[weapon-11-5B8sBxQ7IeKVI6TO.htm](equipment/weapon-11-5B8sBxQ7IeKVI6TO.htm)|Zombie Staff (Greater)|auto-trad|
|[weapon-11-5IUyrJItXkMASgmq.htm](equipment/weapon-11-5IUyrJItXkMASgmq.htm)|Tallow Bomb (Greater)|auto-trad|
|[weapon-11-5Qkz4RVJr2Kx3RL6.htm](equipment/weapon-11-5Qkz4RVJr2Kx3RL6.htm)|Tanglefoot Bag (Greater)|auto-trad|
|[weapon-11-73JDNoUQPmUqjz97.htm](equipment/weapon-11-73JDNoUQPmUqjz97.htm)|Shattered Plan|auto-trad|
|[weapon-11-7haFJ3s6G6K1TQFj.htm](equipment/weapon-11-7haFJ3s6G6K1TQFj.htm)|Gloaming Arc|auto-trad|
|[weapon-11-7WwKI53IcXVIHbkh.htm](equipment/weapon-11-7WwKI53IcXVIHbkh.htm)|Skunk Bomb (Greater)|auto-trad|
|[weapon-11-aBfyzL0WBFMBAbuI.htm](equipment/weapon-11-aBfyzL0WBFMBAbuI.htm)|Buzzsaw Axe|auto-trad|
|[weapon-11-bheWnrBqMphBRUmn.htm](equipment/weapon-11-bheWnrBqMphBRUmn.htm)|Oathbow|auto-trad|
|[weapon-11-CS3mKu9koUfRiHJo.htm](equipment/weapon-11-CS3mKu9koUfRiHJo.htm)|Redpitch Bomb (Greater)|auto-trad|
|[weapon-11-DePdZBHfTorg74Ah.htm](equipment/weapon-11-DePdZBHfTorg74Ah.htm)|Reaper's Grasp|auto-trad|
|[weapon-11-dsMkvuLgpOOGLWDy.htm](equipment/weapon-11-dsMkvuLgpOOGLWDy.htm)|Thunderstone (Greater)|auto-trad|
|[weapon-11-GKBCShgNVVhDiNCb.htm](equipment/weapon-11-GKBCShgNVVhDiNCb.htm)|Redeemer's Pistol|auto-trad|
|[weapon-11-HKcfpawD4CesJtFy.htm](equipment/weapon-11-HKcfpawD4CesJtFy.htm)|Hex Blaster|auto-trad|
|[weapon-11-HWVORFebjWAPQ2NI.htm](equipment/weapon-11-HWVORFebjWAPQ2NI.htm)|Gloaming Shard|auto-trad|
|[weapon-11-intVPcEzbE9o4NQd.htm](equipment/weapon-11-intVPcEzbE9o4NQd.htm)|Frost Vial (Greater)|auto-trad|
|[weapon-11-jC8GmH0Un6vDxdMj.htm](equipment/weapon-11-jC8GmH0Un6vDxdMj.htm)|Acid Flask (Greater)|auto-trad|
|[weapon-11-kP51e5Ul3Q1fusSg.htm](equipment/weapon-11-kP51e5Ul3Q1fusSg.htm)|Dezullon Fountain|auto-trad|
|[weapon-11-kpJmjm843vaMZjIu.htm](equipment/weapon-11-kpJmjm843vaMZjIu.htm)|Dread Ampoule (Greater)|auto-trad|
|[weapon-11-LhMXIGelSrFPYild.htm](equipment/weapon-11-LhMXIGelSrFPYild.htm)|Blight Bomb (Greater)|auto-trad|
|[weapon-11-n5yTlTs63o53n3k6.htm](equipment/weapon-11-n5yTlTs63o53n3k6.htm)|Alchemist's Fire (Greater)|auto-trad|
|[weapon-11-py3PPoO2zMUjOzOI.htm](equipment/weapon-11-py3PPoO2zMUjOzOI.htm)|Rime Foil|auto-trad|
|[weapon-11-r2iTRbt1zpkAqHj2.htm](equipment/weapon-11-r2iTRbt1zpkAqHj2.htm)|Bottled Lightning (Greater)|auto-trad|
|[weapon-11-rvxphQJrycwqejMW.htm](equipment/weapon-11-rvxphQJrycwqejMW.htm)|Rat-Catcher Trident|auto-trad|
|[weapon-11-s3wY38FriyervrtC.htm](equipment/weapon-11-s3wY38FriyervrtC.htm)|Alignment Ampoule (Greater)|auto-trad|
|[weapon-11-SAtG9jrqey2y8EGb.htm](equipment/weapon-11-SAtG9jrqey2y8EGb.htm)|Goo Grenade (Greater)|auto-trad|
|[weapon-11-tE2IarA29mYsgrxj.htm](equipment/weapon-11-tE2IarA29mYsgrxj.htm)|Spore Sap|auto-trad|
|[weapon-11-ud3pqCquYA5UecaS.htm](equipment/weapon-11-ud3pqCquYA5UecaS.htm)|Stoneraiser Javelin|auto-trad|
|[weapon-11-xt4sb8vz8VRlJoKm.htm](equipment/weapon-11-xt4sb8vz8VRlJoKm.htm)|Staff of Nature's Cunning (Greater)|auto-trad|
|[weapon-11-yMbMbPW2WfjOIrmJ.htm](equipment/weapon-11-yMbMbPW2WfjOIrmJ.htm)|Nightmare Cudgel|auto-trad|
|[weapon-11-yq5BoByEscKRZsto.htm](equipment/weapon-11-yq5BoByEscKRZsto.htm)|Twigjack Sack (Greater)|auto-trad|
|[weapon-11-ZVhSxudgfnSO4AMd.htm](equipment/weapon-11-ZVhSxudgfnSO4AMd.htm)|Peshpine Grenade (Greater)|auto-trad|
|[weapon-12-2wUR0XVYONWrBVa8.htm](equipment/weapon-12-2wUR0XVYONWrBVa8.htm)|Staff of Air (Major)|auto-trad|
|[weapon-12-3oexArva2aEm69WV.htm](equipment/weapon-12-3oexArva2aEm69WV.htm)|Four-Ways Dogslicer|auto-trad|
|[weapon-12-6qqsyhVHPo8LQH8Z.htm](equipment/weapon-12-6qqsyhVHPo8LQH8Z.htm)|Arboreal Staff|auto-trad|
|[weapon-12-a8YCTjKnXySKpU5C.htm](equipment/weapon-12-a8YCTjKnXySKpU5C.htm)|Staff of Earth (Major)|auto-trad|
|[weapon-12-AH0u9DDvs4napvdh.htm](equipment/weapon-12-AH0u9DDvs4napvdh.htm)|Bottled Sunlight (Greater)|auto-trad|
|[weapon-12-DBQ68Evl8PdLAVmt.htm](equipment/weapon-12-DBQ68Evl8PdLAVmt.htm)|Lodestone Bomb|auto-trad|
|[weapon-12-DTIBj6Yhy73G5P6j.htm](equipment/weapon-12-DTIBj6Yhy73G5P6j.htm)|Mentalist's Staff (Major)|auto-trad|
|[weapon-12-ED3By5mICaJeiQYo.htm](equipment/weapon-12-ED3By5mICaJeiQYo.htm)|Staff of the Black Desert|auto-trad|
|[weapon-12-FTVap8IjoKgCexH7.htm](equipment/weapon-12-FTVap8IjoKgCexH7.htm)|Staff of Healing (Major)|auto-trad|
|[weapon-12-IKuvON8e4o7ARe4p.htm](equipment/weapon-12-IKuvON8e4o7ARe4p.htm)|Gambler's Staff|auto-trad|
|[weapon-12-KxSyomQx7rpwqemP.htm](equipment/weapon-12-KxSyomQx7rpwqemP.htm)|Staff of Impossible Visions (Greater)|auto-trad|
|[weapon-12-MSzFbb4Jswvrqjru.htm](equipment/weapon-12-MSzFbb4Jswvrqjru.htm)|Pirate Staff|auto-trad|
|[weapon-12-Oo7IpJQDSTmzUyzG.htm](equipment/weapon-12-Oo7IpJQDSTmzUyzG.htm)|Animal Staff (Major)|auto-trad|
|[weapon-12-PAZQrWMLfYTpDoal.htm](equipment/weapon-12-PAZQrWMLfYTpDoal.htm)|Boreal Staff (Greater)|auto-trad|
|[weapon-12-pBPVm5JjKfKX8gaX.htm](equipment/weapon-12-pBPVm5JjKfKX8gaX.htm)|Composer Staff (Major)|auto-trad|
|[weapon-12-PNs3SqMhJ3rTxpSI.htm](equipment/weapon-12-PNs3SqMhJ3rTxpSI.htm)|Trueshape Bomb|auto-trad|
|[weapon-12-QFvB2qgpWdeEa4fT.htm](equipment/weapon-12-QFvB2qgpWdeEa4fT.htm)|Librarian Staff (Greater)|auto-trad|
|[weapon-12-QMqPGk7oL4kESGc3.htm](equipment/weapon-12-QMqPGk7oL4kESGc3.htm)|Staff of Water (Major)|auto-trad|
|[weapon-12-rmGfBLRTfOqhwhHW.htm](equipment/weapon-12-rmGfBLRTfOqhwhHW.htm)|Socialite Staff|auto-trad|
|[weapon-12-sBtfUPTLuyRPYI7g.htm](equipment/weapon-12-sBtfUPTLuyRPYI7g.htm)|Shatterstone|auto-trad|
|[weapon-12-sjOkiftTv4KbR2c0.htm](equipment/weapon-12-sjOkiftTv4KbR2c0.htm)|Boulder Seed|auto-trad|
|[weapon-12-tYnFIqjvxT9bWaoR.htm](equipment/weapon-12-tYnFIqjvxT9bWaoR.htm)|Mammoth Bow|auto-trad|
|[weapon-12-uqbvXACABUJG0ifJ.htm](equipment/weapon-12-uqbvXACABUJG0ifJ.htm)|Staff of the Desert Winds (Greater)|auto-trad|
|[weapon-12-wWBi79jkzYGWD6uC.htm](equipment/weapon-12-wWBi79jkzYGWD6uC.htm)|Ringmaster's Staff (Greater)|auto-trad|
|[weapon-12-YCGMVbqlWT8f1F6v.htm](equipment/weapon-12-YCGMVbqlWT8f1F6v.htm)|Tidal Fishhook|auto-trad|
|[weapon-12-ylRk8NvpK2kA8bjw.htm](equipment/weapon-12-ylRk8NvpK2kA8bjw.htm)|Verdant Staff (Greater)|auto-trad|
|[weapon-13-1lZNaqqGOrza9BsG.htm](equipment/weapon-13-1lZNaqqGOrza9BsG.htm)|Screech Shooter (Greater)|auto-trad|
|[weapon-13-297RUMeetOWyciDe.htm](equipment/weapon-13-297RUMeetOWyciDe.htm)|Vampiric Scythe|auto-trad|
|[weapon-13-3SjF539wVL6NK9qq.htm](equipment/weapon-13-3SjF539wVL6NK9qq.htm)|Pistol of Wonder|auto-trad|
|[weapon-13-A6Bavci9ngWPln4l.htm](equipment/weapon-13-A6Bavci9ngWPln4l.htm)|Piston Gauntlets|auto-trad|
|[weapon-13-ak3DfubhIWT3UiCf.htm](equipment/weapon-13-ak3DfubhIWT3UiCf.htm)|Sonic Tuning Mace (Greater)|auto-trad|
|[weapon-13-AUJfvrouapAeC4Mg.htm](equipment/weapon-13-AUJfvrouapAeC4Mg.htm)|Blade of Four Energies|auto-trad|
|[weapon-13-EmTK4nPOORXtb58X.htm](equipment/weapon-13-EmTK4nPOORXtb58X.htm)|Knight Captain's Lance|auto-trad|
|[weapon-13-f1UCVpGYLHa089SE.htm](equipment/weapon-13-f1UCVpGYLHa089SE.htm)|Spark Dancer|auto-trad|
|[weapon-13-G3a60Ug3YNCMyMVR.htm](equipment/weapon-13-G3a60Ug3YNCMyMVR.htm)|Flame Tongue|auto-trad|
|[weapon-13-K5PVPhK5EVM2Fxgg.htm](equipment/weapon-13-K5PVPhK5EVM2Fxgg.htm)|Duchy Defender|auto-trad|
|[weapon-13-kSLHMQjKL77CtvBx.htm](equipment/weapon-13-kSLHMQjKL77CtvBx.htm)|Anchor Spear|auto-trad|
|[weapon-13-SQrkh42q3EYgygx8.htm](equipment/weapon-13-SQrkh42q3EYgygx8.htm)|Dragontooth Club|auto-trad|
|[weapon-13-U8P9aRhjFpptHJCd.htm](equipment/weapon-13-U8P9aRhjFpptHJCd.htm)|Tentacle Cannon (Greater)|auto-trad|
|[weapon-13-XK4DM8wOtcuOsji6.htm](equipment/weapon-13-XK4DM8wOtcuOsji6.htm)|Dwarven Thrower|auto-trad|
|[weapon-13-ZuyzSxmg4QdnZqLy.htm](equipment/weapon-13-ZuyzSxmg4QdnZqLy.htm)|Ankylostar|auto-trad|
|[weapon-14-4UHTZGZosYcP28lD.htm](equipment/weapon-14-4UHTZGZosYcP28lD.htm)|Breath Blaster (Greater)|auto-trad|
|[weapon-14-7aLpTxRVmoNQTqvJ.htm](equipment/weapon-14-7aLpTxRVmoNQTqvJ.htm)|Staff of Conjuration (Major)|auto-trad|
|[weapon-14-gca5Pgt9Pg8G23VA.htm](equipment/weapon-14-gca5Pgt9Pg8G23VA.htm)|Staff of Nature's Vengeance (Greater)|auto-trad|
|[weapon-14-gG4IUEzqD253s7nx.htm](equipment/weapon-14-gG4IUEzqD253s7nx.htm)|Staff of Illusion (Major)|auto-trad|
|[weapon-14-GlyiVrIokFpFFRu2.htm](equipment/weapon-14-GlyiVrIokFpFFRu2.htm)|Staff of Transmutation (Major)|auto-trad|
|[weapon-14-IIV5L4y8zLH17mAK.htm](equipment/weapon-14-IIV5L4y8zLH17mAK.htm)|Staff of Necromancy (Major)|auto-trad|
|[weapon-14-J5It6yq3ZkN059zP.htm](equipment/weapon-14-J5It6yq3ZkN059zP.htm)|Glaive of the Artist|auto-trad|
|[weapon-14-jE5H6jmDJEzxGvSP.htm](equipment/weapon-14-jE5H6jmDJEzxGvSP.htm)|Deepdread Claw|auto-trad|
|[weapon-14-jHCPejseO01ki3lO.htm](equipment/weapon-14-jHCPejseO01ki3lO.htm)|Accursed Staff (Major)|auto-trad|
|[weapon-14-kCN0QxUbJrvidysF.htm](equipment/weapon-14-kCN0QxUbJrvidysF.htm)|Brilliant Rapier|auto-trad|
|[weapon-14-kNvo4FNaiQRnZfO7.htm](equipment/weapon-14-kNvo4FNaiQRnZfO7.htm)|Staff of Evocation (Major)|auto-trad|
|[weapon-14-lOeShVkqV98KlvTI.htm](equipment/weapon-14-lOeShVkqV98KlvTI.htm)|Staff of Divination (Major)|auto-trad|
|[weapon-14-MeUgw1hsSEcWP97G.htm](equipment/weapon-14-MeUgw1hsSEcWP97G.htm)|Staff of Enchantment (Major)|auto-trad|
|[weapon-14-mkFrHOwWJaHF0aGp.htm](equipment/weapon-14-mkFrHOwWJaHF0aGp.htm)|Stargazer|auto-trad|
|[weapon-14-N7cH1SS57NNaOtf5.htm](equipment/weapon-14-N7cH1SS57NNaOtf5.htm)|Ouroboros Flail|auto-trad|
|[weapon-14-OIs6WPuCRh2UJTOe.htm](equipment/weapon-14-OIs6WPuCRh2UJTOe.htm)|Holy Avenger|auto-trad|
|[weapon-14-qgpufYE8gHRWUPiQ.htm](equipment/weapon-14-qgpufYE8gHRWUPiQ.htm)|Songbird's Brush|auto-trad|
|[weapon-14-qtlkftQXfO4wuCVu.htm](equipment/weapon-14-qtlkftQXfO4wuCVu.htm)|Hardened Harrow Deck|auto-trad|
|[weapon-14-REvrniIEqBykkWyI.htm](equipment/weapon-14-REvrniIEqBykkWyI.htm)|Guardian Staff (Major)|auto-trad|
|[weapon-14-SatFUtE2UaPvC394.htm](equipment/weapon-14-SatFUtE2UaPvC394.htm)|Skyrider Sword|auto-trad|
|[weapon-14-tEI1WOo4ZRJ5cI0K.htm](equipment/weapon-14-tEI1WOo4ZRJ5cI0K.htm)|Dragonprism Staff|auto-trad|
|[weapon-14-tVcI2ypPhfhchwMp.htm](equipment/weapon-14-tVcI2ypPhfhchwMp.htm)|Singing Shortbow|auto-trad|
|[weapon-14-U8vVCE2ePjyca666.htm](equipment/weapon-14-U8vVCE2ePjyca666.htm)|Staff of Providence (Major)|auto-trad|
|[weapon-14-YMwlSFUoIEPIyctl.htm](equipment/weapon-14-YMwlSFUoIEPIyctl.htm)|Poisoner's Staff (Major)|auto-trad|
|[weapon-14-Za5Mnae009cDCwuN.htm](equipment/weapon-14-Za5Mnae009cDCwuN.htm)|Storm Flash|auto-trad|
|[weapon-14-ZSt2PxZVik7UHGG3.htm](equipment/weapon-14-ZSt2PxZVik7UHGG3.htm)|Staff of Abjuration (Major)|auto-trad|
|[weapon-14-zXUmfJEBQ7Pfovve.htm](equipment/weapon-14-zXUmfJEBQ7Pfovve.htm)|Deflecting Branch|auto-trad|
|[weapon-15-1yZRZeVAVxrb5HM2.htm](equipment/weapon-15-1yZRZeVAVxrb5HM2.htm)|Staff of Nature's Cunning (Major)|auto-trad|
|[weapon-15-APZJC0A469gBozf1.htm](equipment/weapon-15-APZJC0A469gBozf1.htm)|Blade of the Rabbit Prince|auto-trad|
|[weapon-15-aYFelpLdqrvZMx2Q.htm](equipment/weapon-15-aYFelpLdqrvZMx2Q.htm)|Godsbreath Bow|auto-trad|
|[weapon-15-ChLxhFpo4WSAKgLh.htm](equipment/weapon-15-ChLxhFpo4WSAKgLh.htm)|Petrification Cannon|auto-trad|
|[weapon-15-fLqRH3XpvDZEMxOO.htm](equipment/weapon-15-fLqRH3XpvDZEMxOO.htm)|Radiant Lance|auto-trad|
|[weapon-15-IuL8O6sR8st1mevo.htm](equipment/weapon-15-IuL8O6sR8st1mevo.htm)|Mountebank's Passage|auto-trad|
|[weapon-15-kIExO59KkiXZUmto.htm](equipment/weapon-15-kIExO59KkiXZUmto.htm)|Blightburn Bomb|auto-trad|
|[weapon-15-kJzp8I0rAmcucHuw.htm](equipment/weapon-15-kJzp8I0rAmcucHuw.htm)|Zombie Staff (Major)|auto-trad|
|[weapon-15-LGbyZ4z6SLRC1cuB.htm](equipment/weapon-15-LGbyZ4z6SLRC1cuB.htm)|Spider Gun (Major)|auto-trad|
|[weapon-15-mKEhLq7Ha4lvupMP.htm](equipment/weapon-15-mKEhLq7Ha4lvupMP.htm)|Animate Dreamer|auto-trad|
|[weapon-15-mXlVJIxv6EpzwTJs.htm](equipment/weapon-15-mXlVJIxv6EpzwTJs.htm)|Buzzsaw Axe (Greater)|auto-trad|
|[weapon-15-O7pgJB6M5UqUZ9Qq.htm](equipment/weapon-15-O7pgJB6M5UqUZ9Qq.htm)|Hyldarf's Fang|auto-trad|
|[weapon-15-tD4ZNDutHoCGIFQn.htm](equipment/weapon-15-tD4ZNDutHoCGIFQn.htm)|Blade of the Black Sovereign|auto-trad|
|[weapon-15-tzzvevwmwwKQaHK0.htm](equipment/weapon-15-tzzvevwmwwKQaHK0.htm)|Spy Staff (Major)|auto-trad|
|[weapon-15-U266pQUnWwKYImhD.htm](equipment/weapon-15-U266pQUnWwKYImhD.htm)|Rod of Razors|auto-trad|
|[weapon-15-UmteC2OgKGJBkMDz.htm](equipment/weapon-15-UmteC2OgKGJBkMDz.htm)|Mindrender's Baton|auto-trad|
|[weapon-16-6m9niDjhA6tBfp5x.htm](equipment/weapon-16-6m9niDjhA6tBfp5x.htm)|Staff of Power|auto-trad|
|[weapon-16-87IvbaQnkUOBgdD0.htm](equipment/weapon-16-87IvbaQnkUOBgdD0.htm)|Frost Brand|auto-trad|
|[weapon-16-FkT0UkXdNEmKRhRx.htm](equipment/weapon-16-FkT0UkXdNEmKRhRx.htm)|Staff of the Desert Winds (Major)|auto-trad|
|[weapon-16-nz8Cx3SiRWANdlJE.htm](equipment/weapon-16-nz8Cx3SiRWANdlJE.htm)|Rowan Rifle|auto-trad|
|[weapon-16-oT4pyqLKpJVXDb46.htm](equipment/weapon-16-oT4pyqLKpJVXDb46.htm)|Staff of Impossible Visions (Major)|auto-trad|
|[weapon-16-Ri09KBg9DG0aapLw.htm](equipment/weapon-16-Ri09KBg9DG0aapLw.htm)|Staff of the Black Desert (Greater)|auto-trad|
|[weapon-16-SSZojuAMC9Npvfm3.htm](equipment/weapon-16-SSZojuAMC9Npvfm3.htm)|Staff of Final Rest (Major)|auto-trad|
|[weapon-16-uX8urrzOQM5wdoTD.htm](equipment/weapon-16-uX8urrzOQM5wdoTD.htm)|Lyrakien Staff (Major)|auto-trad|
|[weapon-16-WbcQqXrUytXkCMK3.htm](equipment/weapon-16-WbcQqXrUytXkCMK3.htm)|Staff of Healing (True)|auto-trad|
|[weapon-17-1AHntZrWzp5e31SX.htm](equipment/weapon-17-1AHntZrWzp5e31SX.htm)|Gearblade|auto-trad|
|[weapon-17-4DJQID8GIlxQ7b9C.htm](equipment/weapon-17-4DJQID8GIlxQ7b9C.htm)|Frost Vial (Major)|auto-trad|
|[weapon-17-5MuAq1wFhy5NE0U0.htm](equipment/weapon-17-5MuAq1wFhy5NE0U0.htm)|Zealot Staff|auto-trad|
|[weapon-17-8h5UFFk5EodKZh19.htm](equipment/weapon-17-8h5UFFk5EodKZh19.htm)|Twigjack Sack (Major)|auto-trad|
|[weapon-17-Aulo25tnSdOCOmrI.htm](equipment/weapon-17-Aulo25tnSdOCOmrI.htm)|Alignment Ampoule (Major)|auto-trad|
|[weapon-17-b1gx7ZhuFJAyJOo8.htm](equipment/weapon-17-b1gx7ZhuFJAyJOo8.htm)|Peshpine Grenade (Major)|auto-trad|
|[weapon-17-BxJNWWvpxacDIsdt.htm](equipment/weapon-17-BxJNWWvpxacDIsdt.htm)|Tanglefoot Bag (Major)|auto-trad|
|[weapon-17-cLZrOihPht5M7eTw.htm](equipment/weapon-17-cLZrOihPht5M7eTw.htm)|Alchemist's Fire (Major)|auto-trad|
|[weapon-17-Dv1bozSbR39McpWd.htm](equipment/weapon-17-Dv1bozSbR39McpWd.htm)|Skunk Bomb (Major)|auto-trad|
|[weapon-17-GttLYMT2welSpwCd.htm](equipment/weapon-17-GttLYMT2welSpwCd.htm)|Acid Flask (Major)|auto-trad|
|[weapon-17-gWSmUEKln103LVvB.htm](equipment/weapon-17-gWSmUEKln103LVvB.htm)|Wyrm Drinker|auto-trad|
|[weapon-17-i7bJcib5TUJKOd4Z.htm](equipment/weapon-17-i7bJcib5TUJKOd4Z.htm)|Ghost Charge (Major)|auto-trad|
|[weapon-17-IuGydh0En8LbfnWo.htm](equipment/weapon-17-IuGydh0En8LbfnWo.htm)|Thunderstone (Major)|auto-trad|
|[weapon-17-l29fzEJ6SSIP2wJn.htm](equipment/weapon-17-l29fzEJ6SSIP2wJn.htm)|Tallow Bomb (Major)|auto-trad|
|[weapon-17-LKIs3LJqrKNP2yrx.htm](equipment/weapon-17-LKIs3LJqrKNP2yrx.htm)|Spellcutter|auto-trad|
|[weapon-17-MCHYtxP8E7njLC3s.htm](equipment/weapon-17-MCHYtxP8E7njLC3s.htm)|Dread Ampoule (Major)|auto-trad|
|[weapon-17-pIYlenaADKnxdp11.htm](equipment/weapon-17-pIYlenaADKnxdp11.htm)|Luck Blade|auto-trad|
|[weapon-17-pvxRcuBexbFawjCg.htm](equipment/weapon-17-pvxRcuBexbFawjCg.htm)|Flame Tongue (Greater)|auto-trad|
|[weapon-17-QbEkoFL1EYNGsOom.htm](equipment/weapon-17-QbEkoFL1EYNGsOom.htm)|Hell Staff|auto-trad|
|[weapon-17-riHdB9PBAXZUMsWC.htm](equipment/weapon-17-riHdB9PBAXZUMsWC.htm)|Redpitch Bomb (Major)|auto-trad|
|[weapon-17-rKCiDIoozZTkyE4j.htm](equipment/weapon-17-rKCiDIoozZTkyE4j.htm)|Tentacle Cannon (Major)|auto-trad|
|[weapon-17-rpbbfkexLhtadBDV.htm](equipment/weapon-17-rpbbfkexLhtadBDV.htm)|Bottled Lightning (Major)|auto-trad|
|[weapon-17-rYBX6v9JqpxAiQvn.htm](equipment/weapon-17-rYBX6v9JqpxAiQvn.htm)|Boreal Staff (Major)|auto-trad|
|[weapon-17-S1ALL8osqV1pOtf3.htm](equipment/weapon-17-S1ALL8osqV1pOtf3.htm)|Celestial Staff|auto-trad|
|[weapon-17-sJjuv1991SZ7DWWD.htm](equipment/weapon-17-sJjuv1991SZ7DWWD.htm)|Blight Bomb (Major)|auto-trad|
|[weapon-17-sLuU3V5Hv9bYrl2a.htm](equipment/weapon-17-sLuU3V5Hv9bYrl2a.htm)|Ouroboros Flail (Greater)|auto-trad|
|[weapon-17-TKvAPv3jgv4l143r.htm](equipment/weapon-17-TKvAPv3jgv4l143r.htm)|Celestial Peachwood Sword|auto-trad|
|[weapon-17-v9smWWOsWFwhCZoB.htm](equipment/weapon-17-v9smWWOsWFwhCZoB.htm)|Screech Shooter (Major)|auto-trad|
|[weapon-17-W0RWd2E0jkF2ZXhf.htm](equipment/weapon-17-W0RWd2E0jkF2ZXhf.htm)|Goo Grenade (Major)|auto-trad|
|[weapon-17-XBn3aapPC0x7X13W.htm](equipment/weapon-17-XBn3aapPC0x7X13W.htm)|Chronomancer Staff|auto-trad|
|[weapon-18-1gi4mdZYNgPrzWHc.htm](equipment/weapon-18-1gi4mdZYNgPrzWHc.htm)|Life's Last Breath|auto-trad|
|[weapon-18-APJ0NWJ4DYUXNpNm.htm](equipment/weapon-18-APJ0NWJ4DYUXNpNm.htm)|Lodestone Bomb (Greater)|auto-trad|
|[weapon-18-AWUzApS5PPwt3TCI.htm](equipment/weapon-18-AWUzApS5PPwt3TCI.htm)|Blade of Four Energies (Greater)|auto-trad|
|[weapon-18-CJesyj3lklJVujh0.htm](equipment/weapon-18-CJesyj3lklJVujh0.htm)|Shatterstone (Greater)|auto-trad|
|[weapon-18-hnx3dOQrYLBtsu3V.htm](equipment/weapon-18-hnx3dOQrYLBtsu3V.htm)|Staff of Nature's Vengeance (Major)|auto-trad|
|[weapon-18-i9mxfSIBTTOwsSlx.htm](equipment/weapon-18-i9mxfSIBTTOwsSlx.htm)|Storm Flash (Greater)|auto-trad|
|[weapon-18-Kmrkm1sHKkGXiJlr.htm](equipment/weapon-18-Kmrkm1sHKkGXiJlr.htm)|Torag's Silver Anvil|auto-trad|
|[weapon-18-nz1v5aetUZVLBlvg.htm](equipment/weapon-18-nz1v5aetUZVLBlvg.htm)|Final Rest|auto-trad|
|[weapon-18-ul4LuDff24Kz9n7e.htm](equipment/weapon-18-ul4LuDff24Kz9n7e.htm)|Singing Shortbow (Greater)|auto-trad|
|[weapon-18-VwNmUeRHomup8VoI.htm](equipment/weapon-18-VwNmUeRHomup8VoI.htm)|Shadow's Heart|auto-trad|
|[weapon-18-WnzQy5naKy9Gj4pY.htm](equipment/weapon-18-WnzQy5naKy9Gj4pY.htm)|Breath Blaster (Major)|auto-trad|
|[weapon-18-wtRDteH3D3ME4FU9.htm](equipment/weapon-18-wtRDteH3D3ME4FU9.htm)|Trueshape Bomb (Greater)|auto-trad|
|[weapon-18-xoucExiZTvCbjWMB.htm](equipment/weapon-18-xoucExiZTvCbjWMB.htm)|Bottled Sunlight (Major)|auto-trad|
|[weapon-19-C3vAFRWoeGbMQTAH.htm](equipment/weapon-19-C3vAFRWoeGbMQTAH.htm)|Luck Blade (Wishing)|auto-trad|
|[weapon-19-jBn5TdnxOvzf6a1F.htm](equipment/weapon-19-jBn5TdnxOvzf6a1F.htm)|Sky-Piercing Bow|auto-trad|
|[weapon-19-WCWdJmR5tYO7Aulb.htm](equipment/weapon-19-WCWdJmR5tYO7Aulb.htm)|Mattock of the Titans|auto-trad|
|[weapon-19-y49tnkAWZqJRMkba.htm](equipment/weapon-19-y49tnkAWZqJRMkba.htm)|Skyrider Sword (Greater)|auto-trad|
|[weapon-20-1glQQ63AeQOfQNvc.htm](equipment/weapon-20-1glQQ63AeQOfQNvc.htm)|Staff of Impossible Visions (True)|auto-trad|
|[weapon-20-ANvbi1zKF1So8bON.htm](equipment/weapon-20-ANvbi1zKF1So8bON.htm)|Sky Hammer|auto-trad|
|[weapon-20-BzVwQ0Go1GFp9R0U.htm](equipment/weapon-20-BzVwQ0Go1GFp9R0U.htm)|Dragon Handwraps|auto-trad|
|[weapon-20-D2CnWGQ84bSjh5bz.htm](equipment/weapon-20-D2CnWGQ84bSjh5bz.htm)|Buzzsaw Axe (Major)|auto-trad|
|[weapon-20-dh4FEXlA0FxTfnpY.htm](equipment/weapon-20-dh4FEXlA0FxTfnpY.htm)|Staff of Sieges|auto-trad|
|[weapon-20-IEmYcavVfSy58aYx.htm](equipment/weapon-20-IEmYcavVfSy58aYx.htm)|Staff of the Desert Winds (True)|auto-trad|
|[weapon-20-inyiBwK0zNootR6G.htm](equipment/weapon-20-inyiBwK0zNootR6G.htm)|Dragonprism Staff (Greater)|auto-trad|
|[weapon-20-j9Z8TrUDHhdArJgi.htm](equipment/weapon-20-j9Z8TrUDHhdArJgi.htm)|Ouroboros Flail (Major)|auto-trad|
|[weapon-20-kg6tEjEBFGeilmLO.htm](equipment/weapon-20-kg6tEjEBFGeilmLO.htm)|Spear of the Destroyer's Flame|auto-trad|
|[weapon-20-lSwITRpBSdodhc1c.htm](equipment/weapon-20-lSwITRpBSdodhc1c.htm)|Phoenix Fighting Fan|auto-trad|
|[weapon-20-nI9shR1EG3P09I8r.htm](equipment/weapon-20-nI9shR1EG3P09I8r.htm)|Staff of the Magi|auto-trad|
|[weapon-20-nSAA77N0cnSkljmE.htm](equipment/weapon-20-nSAA77N0cnSkljmE.htm)|Viper Rapier|auto-trad|
|[weapon-20-O6he0J7l1uQgsama.htm](equipment/weapon-20-O6he0J7l1uQgsama.htm)|Piereta|auto-trad|
|[weapon-20-pIagOwW8EFBaKW3k.htm](equipment/weapon-20-pIagOwW8EFBaKW3k.htm)|Staff of Providence (True)|auto-trad|
|[weapon-20-urcBTs4AgSq8u5FJ.htm](equipment/weapon-20-urcBTs4AgSq8u5FJ.htm)|Kaldemash's Lament|auto-trad|
|[weapon-20-VJbZuJFTooFckp26.htm](equipment/weapon-20-VJbZuJFTooFckp26.htm)|Ridill|auto-trad|
|[weapon-20-WrNHmsU8ZaBY3V9S.htm](equipment/weapon-20-WrNHmsU8ZaBY3V9S.htm)|Whispering Staff|auto-trad|
|[weapon-20-YjaXxg9uQ02IbwLi.htm](equipment/weapon-20-YjaXxg9uQ02IbwLi.htm)|Orb Shard|auto-trad|
|[weapon-20-ZrXw7hAH8dQOGAYL.htm](equipment/weapon-20-ZrXw7hAH8dQOGAYL.htm)|Blightburn Bomb (Greater)|auto-trad|
|[weapon-22-2WOpgJyaFE2gNW7H.htm](equipment/weapon-22-2WOpgJyaFE2gNW7H.htm)|Spiral Athame|auto-trad|
|[weapon-22-h2UebueRKfVLwKOa.htm](equipment/weapon-22-h2UebueRKfVLwKOa.htm)|Cane of the Maelstrom|auto-trad|
|[weapon-22-Vz7nITDWjrXmyoJn.htm](equipment/weapon-22-Vz7nITDWjrXmyoJn.htm)|Kortos Diamond|auto-trad|
|[weapon-23-2jtZPnHF1M8vKWry.htm](equipment/weapon-23-2jtZPnHF1M8vKWry.htm)|Hunter's Dawn|auto-trad|
|[weapon-23-emGyagWNmjvtjiGK.htm](equipment/weapon-23-emGyagWNmjvtjiGK.htm)|Serithtial|auto-trad|
|[weapon-23-JYtqvOyF11GUsQTB.htm](equipment/weapon-23-JYtqvOyF11GUsQTB.htm)|Coldstar Pistols|auto-trad|
|[weapon-23-xxSpHT2vnCFxTr0q.htm](equipment/weapon-23-xxSpHT2vnCFxTr0q.htm)|Ghosthand's Comet|auto-trad|
|[weapon-25-LMKw9PCVGeZy7knY.htm](equipment/weapon-25-LMKw9PCVGeZy7knY.htm)|Cayden's Tankard|auto-trad|
|[weapon-25-o9IErbpmItz9NZT3.htm](equipment/weapon-25-o9IErbpmItz9NZT3.htm)|Blackaxe|auto-trad|
|[weapon-26-tPwYYwxNqdGQp2nW.htm](equipment/weapon-26-tPwYYwxNqdGQp2nW.htm)|Axe of the Dwarven Lords|auto-trad|
|[weapon-28-rft8qacTooqfsUIo.htm](equipment/weapon-28-rft8qacTooqfsUIo.htm)|Whisperer of Souls|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[armor-05-MK3Fq0BV2uTspvXn.htm](equipment/armor-05-MK3Fq0BV2uTspvXn.htm)|Burr Shield|Escudo Burr|modificada|
|[armor-09-GPrjYsQiC3jKdQmO.htm](equipment/armor-09-GPrjYsQiC3jKdQmO.htm)|Blast Suit|Blast Suit|modificada|
|[armor-12-QJNyQlmPNzIrnuLf.htm](equipment/armor-12-QJNyQlmPNzIrnuLf.htm)|Jawbreaker Shield|Jawbreaker Escudo|modificada|
|[armor-18-XKsPGGbPKLkrqHwM.htm](equipment/armor-18-XKsPGGbPKLkrqHwM.htm)|Linnorm's Sankeit|Sankeit de Linnorm|modificada|
|[backpack-07-MN8SF2sArQhJg6QG.htm](equipment/backpack-07-MN8SF2sArQhJg6QG.htm)|Bag of Devouring Type I|Bolsa de Devorar Tipo I|modificada|
|[backpack-11-EIBmADRICTyYzxik.htm](equipment/backpack-11-EIBmADRICTyYzxik.htm)|Bag of Devouring Type II|Bolsa de devorar tipo II|modificada|
|[backpack-13-n3kJYoTrzXYwlYaV.htm](equipment/backpack-13-n3kJYoTrzXYwlYaV.htm)|Bag of Devouring Type III|Bolsa de Devorar Tipo III|modificada|
|[consumable-01-Led9ciHzpF8uPN6Y.htm](equipment/consumable-01-Led9ciHzpF8uPN6Y.htm)|Thunderbird Tuft (Lesser)|Mechón de Pájaro Trueno (Menor)|modificada|
|[consumable-01-mbrwudO35tItsldq.htm](equipment/consumable-01-mbrwudO35tItsldq.htm)|Energy Mutagen (Lesser)|Mutágeno de energía (Menor)|modificada|
|[consumable-01-NI7twU2G6UCDmvCO.htm](equipment/consumable-01-NI7twU2G6UCDmvCO.htm)|Shining Ammunition|Munición brillante|modificada|
|[consumable-02-66EBP5Ii9QMlZdDx.htm](equipment/consumable-02-66EBP5Ii9QMlZdDx.htm)|Static Snare|Static Snare|modificada|
|[consumable-02-8fbsKEEbZ0CfBMIr.htm](equipment/consumable-02-8fbsKEEbZ0CfBMIr.htm)|Silversheen|Lustre de plata|modificada|
|[consumable-02-p3RduziOxQxtREg9.htm](equipment/consumable-02-p3RduziOxQxtREg9.htm)|Looter's Lethargy|Letargo de Saqueador|modificada|
|[consumable-03-ANQz5rzp5kKe7ul6.htm](equipment/consumable-03-ANQz5rzp5kKe7ul6.htm)|Toadskin Salve|Toadskin Salve|modificada|
|[consumable-03-cImNgwDCqwa6Dil1.htm](equipment/consumable-03-cImNgwDCqwa6Dil1.htm)|Cold Iron Blanch (Lesser)|Blanqueo de Hierro Frío (Menor)|modificada|
|[consumable-03-lgqyRmQ3GZbbxQsD.htm](equipment/consumable-03-lgqyRmQ3GZbbxQsD.htm)|Vermin Repellent Agent (Lesser)|Agente repelente de alimañas (Menor)|modificada|
|[consumable-03-rfyWXgrVD2lm91CC.htm](equipment/consumable-03-rfyWXgrVD2lm91CC.htm)|Blue Dragonfly Poison|Veneno de libélula azul|modificada|
|[consumable-03-SHQv4yntEoU4uYRJ.htm](equipment/consumable-03-SHQv4yntEoU4uYRJ.htm)|Conrasu Coin (Arbiter)|Moneda Conrasu (Arbiter)|modificada|
|[consumable-03-SLJTJwyIRjuWjfuK.htm](equipment/consumable-03-SLJTJwyIRjuWjfuK.htm)|Energy Mutagen (Moderate)|Mutágeno de energía (moderada)|modificada|
|[consumable-04-77D3QdU33DLcwNrJ.htm](equipment/consumable-04-77D3QdU33DLcwNrJ.htm)|Irritating Thorn Snare|Lazo de espinas irritante|modificada|
|[consumable-04-k6D64EAjcKMf8NZB.htm](equipment/consumable-04-k6D64EAjcKMf8NZB.htm)|Bloodseeker Beak|Pico de buscasangre|modificada|
|[consumable-05-ik15k2r6znOdguXo.htm](equipment/consumable-05-ik15k2r6znOdguXo.htm)|Thunderbird Tuft (Moderate)|Thunderbird Tuft (Moderada)|modificada|
|[consumable-05-uoWWFCt1B0lKtjwZ.htm](equipment/consumable-05-uoWWFCt1B0lKtjwZ.htm)|Blister Ammunition (Lesser)|Munición Ampolla (Menor)|modificada|
|[consumable-05-UUriyvai2k6COWpS.htm](equipment/consumable-05-UUriyvai2k6COWpS.htm)|Necrobinding Serum|Suero Necrobinding|modificada|
|[consumable-05-xlwcMtJTKpS6EkSw.htm](equipment/consumable-05-xlwcMtJTKpS6EkSw.htm)|Wand of Caustic Effluence|Wand of Caustic Effluence|modificada|
|[consumable-05-Xnmx6SJ6OkJOfYxF.htm](equipment/consumable-05-Xnmx6SJ6OkJOfYxF.htm)|Freezing Ammunition|Munición congelante|modificada|
|[consumable-06-iCRoLguNunOjIM7L.htm](equipment/consumable-06-iCRoLguNunOjIM7L.htm)|Forgetful Ink|Forgetful Ink|modificada|
|[consumable-07-2Rnw5hTlMd0YSozb.htm](equipment/consumable-07-2Rnw5hTlMd0YSozb.htm)|Meteor Shot|Meteor Shot|modificada|
|[consumable-07-4TzKhSM0gD9czMdC.htm](equipment/consumable-07-4TzKhSM0gD9czMdC.htm)|Camouflage Dye (Greater)|Tinte de camuflaje (Mayor)|modificada|
|[consumable-07-nn2HAN1XKkKHfVnM.htm](equipment/consumable-07-nn2HAN1XKkKHfVnM.htm)|Stupor Poison|Veneno de estupor|modificada|
|[consumable-07-RuGGAFaTHMCC6chN.htm](equipment/consumable-07-RuGGAFaTHMCC6chN.htm)|Toadskin Salve (Greater)|Toadskin Salve (Mayor)|modificada|
|[consumable-07-YGaO4HyH6jn3P731.htm](equipment/consumable-07-YGaO4HyH6jn3P731.htm)|Murderer's Knot|Nudo Asesino|modificada|
|[consumable-08-WGbE7qAGw8O5yH07.htm](equipment/consumable-08-WGbE7qAGw8O5yH07.htm)|Vermin Repellent Agent (Moderate)|Agente repelente de alimañas (moderado)|modificada|
|[consumable-09-jnCP2zYJamUeZsJC.htm](equipment/consumable-09-jnCP2zYJamUeZsJC.htm)|Cold Iron Blanch (Moderate)|Blanch de hierro frío (moderada)|modificada|
|[consumable-09-q07Ej07uIsKx5zab.htm](equipment/consumable-09-q07Ej07uIsKx5zab.htm)|Mana-Rattler Liniment|Mana-Rattler Linimento|modificada|
|[consumable-09-rKknk8odXDBpON5l.htm](equipment/consumable-09-rKknk8odXDBpON5l.htm)|Explosive Ammunition|Munición Explosión|modificada|
|[consumable-09-sYTvRLIlhfUd1Dm5.htm](equipment/consumable-09-sYTvRLIlhfUd1Dm5.htm)|Thunderbird Tuft (Greater)|Penacho de pájaro trueno (mayor)|modificada|
|[consumable-09-xvJ2jzqipBNrwZ2w.htm](equipment/consumable-09-xvJ2jzqipBNrwZ2w.htm)|Sight-Theft Grit|Sight-Theft Grit|modificada|
|[consumable-09-yaKQS5aG2i7IioyH.htm](equipment/consumable-09-yaKQS5aG2i7IioyH.htm)|Corpsecaller Round|Corpsecaller Round|modificada|
|[consumable-10-EOk6kNsNJOhoSbRZ.htm](equipment/consumable-10-EOk6kNsNJOhoSbRZ.htm)|Effervescent Decoction|Decocción Efervescente|modificada|
|[consumable-10-FgZCLBhOOhhXDdvI.htm](equipment/consumable-10-FgZCLBhOOhhXDdvI.htm)|Conrasu Coin (Bythos)|Moneda Conrasu (Bythos)|modificada|
|[consumable-10-MZ5fWDqpKEMv2d1Q.htm](equipment/consumable-10-MZ5fWDqpKEMv2d1Q.htm)|Mutagenic Renovator|Renovador mutagénico|modificada|
|[consumable-10-UYCQoe8sKZgbKYHg.htm](equipment/consumable-10-UYCQoe8sKZgbKYHg.htm)|Follypops|Follypops|modificada|
|[consumable-11-8GEsRsQAdDljRT1s.htm](equipment/consumable-11-8GEsRsQAdDljRT1s.htm)|Hunger Oil|Aceite del Hambre|modificada|
|[consumable-11-dlaDnZFsy4JCYmEO.htm](equipment/consumable-11-dlaDnZFsy4JCYmEO.htm)|Toadskin Salve (Major)|Bálsamo de piel de sapo (superior)|modificada|
|[consumable-11-xKME7GEjssNkyTgt.htm](equipment/consumable-11-xKME7GEjssNkyTgt.htm)|Blister Ammunition (Moderate)|Munición Ampolla (Moderado)|modificada|
|[consumable-11-xVtyaEnYZ1aJT4ki.htm](equipment/consumable-11-xVtyaEnYZ1aJT4ki.htm)|Energy Mutagen (Greater)|Mutágeno de energía (mayor)|modificada|
|[consumable-12-9Kk6P7idLGRhZJ2q.htm](equipment/consumable-12-9Kk6P7idLGRhZJ2q.htm)|Bleeding Spines Snare|Trampa de lazo de espinos sangrantes|modificada|
|[consumable-12-CReWZjAd3szYQ6Mx.htm](equipment/consumable-12-CReWZjAd3szYQ6Mx.htm)|Vermin Repellent Agent (Greater)|Vermin Repellent Agent (Mayor)|modificada|
|[consumable-12-ENrhapQe2Gw8HdwR.htm](equipment/consumable-12-ENrhapQe2Gw8HdwR.htm)|Meteor Shot (Greater)|Meteor Shot (Mayor)|modificada|
|[consumable-12-tOg1nt08dBTZbkwR.htm](equipment/consumable-12-tOg1nt08dBTZbkwR.htm)|Penetrating Ammunition|Munición perforante|modificada|
|[consumable-13-9nrB5W2evrc2U3op.htm](equipment/consumable-13-9nrB5W2evrc2U3op.htm)|Bower Fruit|Bower Fruit|modificada|
|[consumable-13-g27kK39Nzqphl1Tb.htm](equipment/consumable-13-g27kK39Nzqphl1Tb.htm)|Explosive Ammunition (Greater)|Munición explosiva, mayor.|modificada|
|[consumable-13-QImdOdyolrMWwuxD.htm](equipment/consumable-13-QImdOdyolrMWwuxD.htm)|Gorgon's Breath|Gorgon's Breath|modificada|
|[consumable-13-xYJwgmn2Nmthztb7.htm](equipment/consumable-13-xYJwgmn2Nmthztb7.htm)|Thunderbird Tuft (Major)|Penacho de ave de trueno (superior)|modificada|
|[consumable-14-5koINU0dy5nPL8Cg.htm](equipment/consumable-14-5koINU0dy5nPL8Cg.htm)|Rending Snare|Rasgadura Snare|modificada|
|[consumable-14-8qUja4YdvewN4es0.htm](equipment/consumable-14-8qUja4YdvewN4es0.htm)|Death Knell Powder|Polvo doblar a muerto|modificada|
|[consumable-14-px55EOmlMx8xtEgM.htm](equipment/consumable-14-px55EOmlMx8xtEgM.htm)|Unending Itch|Picazón interminable|modificada|
|[consumable-14-ulGv1u9BAqRYYe4H.htm](equipment/consumable-14-ulGv1u9BAqRYYe4H.htm)|Shrapnel Snare|Shrapnel Snare|modificada|
|[consumable-14-vAEQsYQbOYmqfIc0.htm](equipment/consumable-14-vAEQsYQbOYmqfIc0.htm)|Liar's Demise|Liar's Demise|modificada|
|[consumable-15-R8ZmXOfbfrp54VVB.htm](equipment/consumable-15-R8ZmXOfbfrp54VVB.htm)|Garrote Bolt|Perno Garrote|modificada|
|[consumable-16-C6v5p0ZtIUroYlU2.htm](equipment/consumable-16-C6v5p0ZtIUroYlU2.htm)|Vermin Repellent Agent (Major)|Agente repelente de alimañas (superior)|modificada|
|[consumable-16-O4SbscW0mhRRSbRK.htm](equipment/consumable-16-O4SbscW0mhRRSbRK.htm)|Apricot of Bestial Might|Albaricoque de Poder Bestial|modificada|
|[consumable-16-WXnGIl4d62detRlf.htm](equipment/consumable-16-WXnGIl4d62detRlf.htm)|Cold Iron Blanch (Greater)|Blanqueo de Hierro Frío (Mayor)|modificada|
|[consumable-16-xm8FszDcmRrnlk09.htm](equipment/consumable-16-xm8FszDcmRrnlk09.htm)|Blister Ammunition (Greater)|Munición Ampolla (Mayor)|modificada|
|[consumable-17-34MbgwW1SGlowyk2.htm](equipment/consumable-17-34MbgwW1SGlowyk2.htm)|Energy Mutagen (Major)|Mutágeno de energía (superior)|modificada|
|[consumable-17-mPta1oQq0IWp5uE3.htm](equipment/consumable-17-mPta1oQq0IWp5uE3.htm)|Meteor Shot (Major)|Tiro de meteorito (superior)|modificada|
|[equipment-00-4ftXXUCBHcf4b0MH.htm](equipment/equipment-00-4ftXXUCBHcf4b0MH.htm)|Alchemist's Tools|Alchemist's Tools|modificada|
|[equipment-00-C9VvYnjbWertzgMc.htm](equipment/equipment-00-C9VvYnjbWertzgMc.htm)|Ka Stone|Ka Stone|modificada|
|[equipment-00-s1vB3HdXjMigYAnY.htm](equipment/equipment-00-s1vB3HdXjMigYAnY.htm)|Healer's Tools|Herramientas de curandero|modificada|
|[equipment-00-y34yjumCFakrbtdw.htm](equipment/equipment-00-y34yjumCFakrbtdw.htm)|Artisan's Tools|Herramientas del artesano|modificada|
|[equipment-00-zvLyCVD8g2PdHJAc.htm](equipment/equipment-00-zvLyCVD8g2PdHJAc.htm)|Thieves' Tools|Herramientas de ladrón|modificada|
|[equipment-01-FABVnBeYtGXshlwm.htm](equipment/equipment-01-FABVnBeYtGXshlwm.htm)|Ring of Discretion|Anillo de Discreción|modificada|
|[equipment-01-HuC6uonXt6YXePjb.htm](equipment/equipment-01-HuC6uonXt6YXePjb.htm)|Armor Latches|Armor Latches|modificada|
|[equipment-01-UwGcNJS5jjjUssPb.htm](equipment/equipment-01-UwGcNJS5jjjUssPb.htm)|Candlecap|Candlecap|modificada|
|[equipment-02-599vQVSFAKMzvVEt.htm](equipment/equipment-02-599vQVSFAKMzvVEt.htm)|Skittering Mask|Máscara Zafarse|modificada|
|[equipment-02-GLr0U1yY4hSHBApa.htm](equipment/equipment-02-GLr0U1yY4hSHBApa.htm)|Goz Mask|Máscara de Goz|modificada|
|[equipment-02-n8nmqJdfHrDfuG1J.htm](equipment/equipment-02-n8nmqJdfHrDfuG1J.htm)|Standard Astrolabe|Astrolabio estándar|modificada|
|[equipment-02-sZxPKnLtspXPRDNb.htm](equipment/equipment-02-sZxPKnLtspXPRDNb.htm)|Pathfinder's Coin|Pathfinder's Coin|modificada|
|[equipment-03-0QgniSjpzksm5riV.htm](equipment/equipment-03-0QgniSjpzksm5riV.htm)|Artisan's Tools (Sterling)|Herramientas de artesano excelentes (Sterling)|modificada|
|[equipment-03-6nrCxNQFycUVFOV2.htm](equipment/equipment-03-6nrCxNQFycUVFOV2.htm)|Thieves' Tools (Infiltrator)|Herramientas de ladrón (Infiltrador)|modificada|
|[equipment-03-O1W40RBvlZW069Mw.htm](equipment/equipment-03-O1W40RBvlZW069Mw.htm)|Mariner's Astrolabe|Astrolabio de marino|modificada|
|[equipment-03-P8xfjaEsrcKxl7k0.htm](equipment/equipment-03-P8xfjaEsrcKxl7k0.htm)|Everyneed Pack|Everyneed Pack|modificada|
|[equipment-03-sgH9U2fKqWCedKNO.htm](equipment/equipment-03-sgH9U2fKqWCedKNO.htm)|Skinsaw Mask|Skinsaw Mask|modificada|
|[equipment-03-SGkOHFyBbzWdBk8D.htm](equipment/equipment-03-SGkOHFyBbzWdBk8D.htm)|Healer's Tools (Expanded)|Herramientas de curandero (Ampliado)|modificada|
|[equipment-03-VUxPhU966hQaIq2C.htm](equipment/equipment-03-VUxPhU966hQaIq2C.htm)|Thrower's Bandolier|Bandolera de lanzador|modificada|
|[equipment-04-Dntj7zwUTmj4fboD.htm](equipment/equipment-04-Dntj7zwUTmj4fboD.htm)|Shapespeak Mask|Shapespeak Mask|modificada|
|[equipment-04-JCGsj7iY88o9uTP5.htm](equipment/equipment-04-JCGsj7iY88o9uTP5.htm)|Hunter's Arrowhead|Punta de flecha de cazador|modificada|
|[equipment-04-NUEwFlFb7RJnLD4w.htm](equipment/equipment-04-NUEwFlFb7RJnLD4w.htm)|Mask of Mercy|Máscara de Merced|modificada|
|[equipment-05-0LfgYMONUws8lzOv.htm](equipment/equipment-05-0LfgYMONUws8lzOv.htm)|Tlil Mask|Máscara Tlil|modificada|
|[equipment-05-6cV9Kpwc7aiuhqbH.htm](equipment/equipment-05-6cV9Kpwc7aiuhqbH.htm)|Necklace of Fireballs I|Collar de bolas de fuego tipo VI|modificada|
|[equipment-05-8YnlhfZSQ89FjDZ2.htm](equipment/equipment-05-8YnlhfZSQ89FjDZ2.htm)|Corpseward Pendant|Colgante Corpseward|modificada|
|[equipment-05-P1hk0BO89ihEAw8g.htm](equipment/equipment-05-P1hk0BO89ihEAw8g.htm)|Bloodhound Olfactory Stimulators|Estimuladores olfativos Bloodhound|modificada|
|[equipment-05-R8I13CDRzvpVXOVe.htm](equipment/equipment-05-R8I13CDRzvpVXOVe.htm)|Pacifying|Pacifying|modificada|
|[equipment-06-3jd8wUCHH5WhCTEJ.htm](equipment/equipment-06-3jd8wUCHH5WhCTEJ.htm)|Crown of the Companion|Corona del Compañero|modificada|
|[equipment-06-ifsBwfIhd6UcoUCI.htm](equipment/equipment-06-ifsBwfIhd6UcoUCI.htm)|Bellflower Toolbelt|Cinturón de herramientas Bellflower|modificada|
|[equipment-06-JN2wkB9JG4nklVKP.htm](equipment/equipment-06-JN2wkB9JG4nklVKP.htm)|Wand of Noisome Acid (2nd-Level Spell)|Varita de ácido fétido (conjuro de 2¬∫ nivel).|modificada|
|[equipment-06-k8YWyDliEVj4iA0p.htm](equipment/equipment-06-k8YWyDliEVj4iA0p.htm)|Stag's Helm|Yelmo de Ciervo|modificada|
|[equipment-06-LmveO9fSuEnkMKAH.htm](equipment/equipment-06-LmveO9fSuEnkMKAH.htm)|Vaultbreaker's Harness|Vaultbreaker's Harness|modificada|
|[equipment-06-o7Tr4KbPcmd3zzd1.htm](equipment/equipment-06-o7Tr4KbPcmd3zzd1.htm)|Herd Mask|Máscara de Manada|modificada|
|[equipment-06-Qqh586pudsEqITUk.htm](equipment/equipment-06-Qqh586pudsEqITUk.htm)|Energizing|Energizante|modificada|
|[equipment-06-toest6lPgkXHI3cy.htm](equipment/equipment-06-toest6lPgkXHI3cy.htm)|Sure-Step Crampons|Crampones Sure-Step|modificada|
|[equipment-06-xH51QHrc9073UzH7.htm](equipment/equipment-06-xH51QHrc9073UzH7.htm)|Goz Mask (Greater)|Máscara de Goz (Mayor)|modificada|
|[equipment-06-yWlQQOs0A3ApEc1J.htm](equipment/equipment-06-yWlQQOs0A3ApEc1J.htm)|Wand of Teeming Ghosts (2nd-Level Spell)|Varita de torrente fantasmal (conjuro de 2¬∫ nivel).|modificada|
|[equipment-07-31tSOy1iHCjNNd4N.htm](equipment/equipment-07-31tSOy1iHCjNNd4N.htm)|Everyneed Pack (Greater)|Paquete Everyneed (Mayor)|modificada|
|[equipment-07-fo6Yhq5mbQXsnZs0.htm](equipment/equipment-07-fo6Yhq5mbQXsnZs0.htm)|Wounding|Hiriente|modificada|
|[equipment-07-qzgi4PCcvpH0QPVj.htm](equipment/equipment-07-qzgi4PCcvpH0QPVj.htm)|Ethersight Ring|Ethersight Ring|modificada|
|[equipment-07-Zun8aKbODnBFeut6.htm](equipment/equipment-07-Zun8aKbODnBFeut6.htm)|Necklace of Fireballs II|Collar de bolas de fuego tipo VI|modificada|
|[equipment-08-62oxvKUCReo7Iah7.htm](equipment/equipment-08-62oxvKUCReo7Iah7.htm)|Polished Demon Horn (Greater)|Cuerno de Demonio Pulido (Mayor)|modificada|
|[equipment-08-dPQhe0tAa5LkPFoW.htm](equipment/equipment-08-dPQhe0tAa5LkPFoW.htm)|Swarmeater's Clasp|Swarmeater's Clasp|modificada|
|[equipment-08-SVEPGvNFj95HjlAE.htm](equipment/equipment-08-SVEPGvNFj95HjlAE.htm)|Wand of Teeming Ghosts (3rd-Level Spell)|Varita de torrente fantasmal (conjuro de 3er nivel).|modificada|
|[equipment-08-X5OGyKvpF4wcIj7m.htm](equipment/equipment-08-X5OGyKvpF4wcIj7m.htm)|Mask of the Cursed Eye|Máscara del Ojo Maldito|modificada|
|[equipment-08-y8T7k0kkqxNbMAAD.htm](equipment/equipment-08-y8T7k0kkqxNbMAAD.htm)|Skittering Mask (Greater)|Máscara de Zafarse (Mayor)|modificada|
|[equipment-09-FPDe2DQv8HbkN4tz.htm](equipment/equipment-09-FPDe2DQv8HbkN4tz.htm)|Tlil Mask (Greater)|Máscara de Tlil (Mayor)|modificada|
|[equipment-09-IufO3dNPhC1c2ZcL.htm](equipment/equipment-09-IufO3dNPhC1c2ZcL.htm)|Necklace of Fireballs III|Collar de bolas de fuego tipo VI|modificada|
|[equipment-09-qQpcBE7ngv7TIAW3.htm](equipment/equipment-09-qQpcBE7ngv7TIAW3.htm)|Wand of Overflowing Life (3rd-Level Spell)|Varita de vida desbordante (hechizo de 3er nivel).|modificada|
|[equipment-09-QRtsLdNvTs2C1oI2.htm](equipment/equipment-09-QRtsLdNvTs2C1oI2.htm)|Starless Scope Lens|Lente de Ámbito sin Estrellas|modificada|
|[equipment-09-qUnDHEXteUQGE8yp.htm](equipment/equipment-09-qUnDHEXteUQGE8yp.htm)|Grievous|Grave|modificada|
|[equipment-09-u4mjXLioKytEZMTb.htm](equipment/equipment-09-u4mjXLioKytEZMTb.htm)|Minotaur Chair|Silla Minotauro|modificada|
|[equipment-10-0nVHDnehORCQlqP5.htm](equipment/equipment-10-0nVHDnehORCQlqP5.htm)|Drazmorg's Staff of All-Sight|Drazmorg's Staff of All-Sight|modificada|
|[equipment-10-76FyZmtpHi6OXQ0B.htm](equipment/equipment-10-76FyZmtpHi6OXQ0B.htm)|Wildwood Ink (Greater)|Wildwood Tinta (Mayor)|modificada|
|[equipment-10-Nv4DjyBqIMXQ4KM4.htm](equipment/equipment-10-Nv4DjyBqIMXQ4KM4.htm)|Wand of Noisome Acid (4nd-Level Spell)|Varita de ácido fétido (Hechizo de 4º nivel).|modificada|
|[equipment-10-ofhqc85Z3Rt7dcrQ.htm](equipment/equipment-10-ofhqc85Z3Rt7dcrQ.htm)|Wand of Teeming Ghosts (4th-Level Spell)|Varita de torrente fantasmal (conjuro de 4¬∫ nivel).|modificada|
|[equipment-11-85GqeSGXjSQwLy07.htm](equipment/equipment-11-85GqeSGXjSQwLy07.htm)|Wand of Overflowing Life (4th-Level Spell)|Varita de vida desbordante (conjuro de 4¬∫ nivel)|modificada|
|[equipment-11-9YtYO8u5UOg4q64Y.htm](equipment/equipment-11-9YtYO8u5UOg4q64Y.htm)|Wand of the Spider (4th-Level Spell)|Varita de araña (hechizo de 4¬∫ nivel).|modificada|
|[equipment-11-BagxmaKWmG5SYdFV.htm](equipment/equipment-11-BagxmaKWmG5SYdFV.htm)|Dragon Rune Bracelet|Pulsera runa del dragón|modificada|
|[equipment-11-boQHr2Z0W6TQCJo3.htm](equipment/equipment-11-boQHr2Z0W6TQCJo3.htm)|Oracular Crown|Corona Oracular|modificada|
|[equipment-11-Dr9b08JhDThUenF9.htm](equipment/equipment-11-Dr9b08JhDThUenF9.htm)|Necklace of Fireballs IV|Collar de bolas de fuego tipo VI|modificada|
|[equipment-12-1kWWhTpXf68BdCWe.htm](equipment/equipment-12-1kWWhTpXf68BdCWe.htm)|Wand of Traitorous Thoughts|Varita de Pensamientos Traidores|modificada|
|[equipment-12-eRlOMigJjx5jAEKw.htm](equipment/equipment-12-eRlOMigJjx5jAEKw.htm)|Polished Demon Horn (Major)|Cuerno de Demonio Pulido (Mayor)|modificada|
|[equipment-12-JWacChecDZbdo6sT.htm](equipment/equipment-12-JWacChecDZbdo6sT.htm)|Wand of Teeming Ghosts (5th-Level Spell)|Varita de torrente fantasmal (conjuro de 5¬∫ nivel)|modificada|
|[equipment-12-qixSKcFT1OA4mIkp.htm](equipment/equipment-12-qixSKcFT1OA4mIkp.htm)|Goz Mask (Major)|Máscara de Goz (superior)|modificada|
|[equipment-13-aj2eTtEAgLu4f14b.htm](equipment/equipment-13-aj2eTtEAgLu4f14b.htm)|Wand of Overflowing Life (5th-Level Spell)|Varita de vida desbordante (hechizo de 5¬∫ nivel)|modificada|
|[equipment-13-hDO0vRS8r9K2Zw87.htm](equipment/equipment-13-hDO0vRS8r9K2Zw87.htm)|Necklace of Fireballs V|Collar de bolas de fuego tipo VI|modificada|
|[equipment-13-zEys8FeMMAwTqwgW.htm](equipment/equipment-13-zEys8FeMMAwTqwgW.htm)|Bloodbane (Greater)|Perdición de sangre (mayor)|modificada|
|[equipment-14-4Lhy6pAOIZdOufu3.htm](equipment/equipment-14-4Lhy6pAOIZdOufu3.htm)|Wand of Teeming Ghosts (6th-Level Spell)|Varita de torrente fantasmal (conjuro de 6¬∫ nivel)|modificada|
|[equipment-14-IHybXd0JGwhMrOlr.htm](equipment/equipment-14-IHybXd0JGwhMrOlr.htm)|Pactmaster's Grace|Pactmaster's Grace|modificada|
|[equipment-14-ljZwHU5BMnFafVa3.htm](equipment/equipment-14-ljZwHU5BMnFafVa3.htm)|Wand of Noisome Acid (6th-Level Spell)|Varita de ácido fétido (hechizo de 6º nivel).|modificada|
|[equipment-14-QC4TXwjteVhwRSNO.htm](equipment/equipment-14-QC4TXwjteVhwRSNO.htm)|Wand of Pernicious Poison (Purple Worm Sting)|Varita de veneno pernicioso (Picadura de gusano púrpura) (Copy)|modificada|
|[equipment-14-RBlbAZSGAJOBsmjv.htm](equipment/equipment-14-RBlbAZSGAJOBsmjv.htm)|Storm Chair|Silla Tormenta|modificada|
|[equipment-15-0AxBSojJ2XhcqMvJ.htm](equipment/equipment-15-0AxBSojJ2XhcqMvJ.htm)|Sanguine Fang (Major)|Colmillo Sanguinario (Mayor)|modificada|
|[equipment-15-LfiGXU03Khb1o6fs.htm](equipment/equipment-15-LfiGXU03Khb1o6fs.htm)|Heedless Spurs|Heedless Spurs|modificada|
|[equipment-15-tzoN71erFxQ2HVLl.htm](equipment/equipment-15-tzoN71erFxQ2HVLl.htm)|Toshigami Blossom|Toshigami Blossom|modificada|
|[equipment-15-VtzChvlCG2TQRrgu.htm](equipment/equipment-15-VtzChvlCG2TQRrgu.htm)|Wand of Overflowing Life (6th-Level Spell)|Varita de vida desbordante (hechizo de 6¬∫ nivel).|modificada|
|[equipment-15-ZZcIhgiKuptXRGyK.htm](equipment/equipment-15-ZZcIhgiKuptXRGyK.htm)|Necklace of Fireballs VI|Collar de bolas de fuego tipo VI.|modificada|
|[equipment-16-sLnkcAGiRQkwQrla.htm](equipment/equipment-16-sLnkcAGiRQkwQrla.htm)|Wand of Teeming Ghosts (7th-Level Spell)|Varita de torrente fantasmal (7¬∫ nivel de conjuro)|modificada|
|[equipment-16-son3QT3YfrBYlWPq.htm](equipment/equipment-16-son3QT3YfrBYlWPq.htm)|Headbands of Translocation|Diademas de Translocación|modificada|
|[equipment-17-grRxpX1iE3zOJA1q.htm](equipment/equipment-17-grRxpX1iE3zOJA1q.htm)|Wand of Overflowing Life (7th-Level Spell)|Varita de vida desbordante (hechizo de 7¬∫ nivel).|modificada|
|[equipment-17-Lwvg9GECyIPsG8Xa.htm](equipment/equipment-17-Lwvg9GECyIPsG8Xa.htm)|Wildwood Ink (Major)|Wildwood Ink (Mayor)|modificada|
|[equipment-17-mOY0STwY5hx4UPCN.htm](equipment/equipment-17-mOY0STwY5hx4UPCN.htm)|Necklace of Fireballs VII|Collar de bolas de fuego tipo VI.|modificada|
|[equipment-18-ASroFtAWLwn6HJJH.htm](equipment/equipment-18-ASroFtAWLwn6HJJH.htm)|Wand of Teeming Ghosts (8th-Level Spell)|Varita de torrente fantasmal (conjuro de 8¬∫ nivel).|modificada|
|[equipment-18-T1XSnU0bwrn3m520.htm](equipment/equipment-18-T1XSnU0bwrn3m520.htm)|Wand of Noisome Acid (8th-Level Spell)|Varita de ácido fétido (hechizo de 8º nivel).|modificada|
|[equipment-18-v9tI40skGswMQTVN.htm](equipment/equipment-18-v9tI40skGswMQTVN.htm)|Clockwork Cloak|Capa Mecánica|modificada|
|[equipment-19-xZFbFeJckiQS7smT.htm](equipment/equipment-19-xZFbFeJckiQS7smT.htm)|Wand of Overflowing Life (8th-Level Spell)|Varita de vida desbordante (hechizo de 8¬∫ nivel).|modificada|
|[equipment-20-gufc2pmaYn9jzFVL.htm](equipment/equipment-20-gufc2pmaYn9jzFVL.htm)|Unending Youth|Juventud interminable|modificada|
|[equipment-20-Y7xHJEyX5Mm3gpq3.htm](equipment/equipment-20-Y7xHJEyX5Mm3gpq3.htm)|Wand of Teeming Ghosts (9th-Level Spell)|Varita de torrente fantasmal (conjuro de 9¬∫ nivel).|modificada|
|[weapon-00-0GEXLXh8M5Ce6oYT.htm](equipment/weapon-00-0GEXLXh8M5Ce6oYT.htm)|Chakri (Lost Omens)|Chakri|modificada|
|[weapon-00-DFtbWoL6yBnDbtB3.htm](equipment/weapon-00-DFtbWoL6yBnDbtB3.htm)|Panabas|Panabas|modificada|
|[weapon-00-KgKSX95uVSg8iBBA.htm](equipment/weapon-00-KgKSX95uVSg8iBBA.htm)|Khakkhara|Khakkara|modificada|
|[weapon-00-pnA6uENYDDduWmAn.htm](equipment/weapon-00-pnA6uENYDDduWmAn.htm)|Thunder Sling|Honda Tronante|modificada|
|[weapon-00-rfP9e1fnwjnIQSJK.htm](equipment/weapon-00-rfP9e1fnwjnIQSJK.htm)|Fire Poi|Poi de Fuego|modificada|
|[weapon-00-rhHGULVS5tumszGP.htm](equipment/weapon-00-rhHGULVS5tumszGP.htm)|Chakri|Chakri|modificada|
|[weapon-00-Sarxo8kSrJs2qJEw.htm](equipment/weapon-00-Sarxo8kSrJs2qJEw.htm)|Urumi|Urumi|modificada|
|[weapon-00-vsJ0zvYZzduP7rtD.htm](equipment/weapon-00-vsJ0zvYZzduP7rtD.htm)|Tekko-Kagi|Tekko-kagi|modificada|
|[weapon-00-yUgOLSyb4pGGxfCR.htm](equipment/weapon-00-yUgOLSyb4pGGxfCR.htm)|Feng Huo Lun|Rueda de Viento y Fuego|modificada|
|[weapon-01-841G24r5ITFYk5zQ.htm](equipment/weapon-01-841G24r5ITFYk5zQ.htm)|Pernicious Spore Bomb (Lesser)|Bomba de esporas perniciosas (menor)|modificada|
|[weapon-01-8JnOYyTdatqRnAV4.htm](equipment/weapon-01-8JnOYyTdatqRnAV4.htm)|Necrotic Bomb (Lesser)|Bomba Necrótica (Menor)|modificada|
|[weapon-01-Bb3cvysXG8rCM5FM.htm](equipment/weapon-01-Bb3cvysXG8rCM5FM.htm)|Vexing Vapor (Lesser)|Vapor Excitante (Menor)|modificada|
|[weapon-01-BG04L9GjWerSgWX3.htm](equipment/weapon-01-BG04L9GjWerSgWX3.htm)|Water Bomb (Lesser)|Bomba de Agua (Menor)|modificada|
|[weapon-01-HtBqmBFYuxJp2QiZ.htm](equipment/weapon-01-HtBqmBFYuxJp2QiZ.htm)|Mud Bomb (Lesser)|Bomba de barro (Menor)|modificada|
|[weapon-01-HYxnpGzoDL70FrVs.htm](equipment/weapon-01-HYxnpGzoDL70FrVs.htm)|Pressure Bomb (Lesser)|Bomba de presión (menor)|modificada|
|[weapon-01-ndVATyJ2c8Ka3HKm.htm](equipment/weapon-01-ndVATyJ2c8Ka3HKm.htm)|Sulfur Bomb (Lesser)|Bomba de azufre (menor)|modificada|
|[weapon-01-RsCLCgCgGAn67ry5.htm](equipment/weapon-01-RsCLCgCgGAn67ry5.htm)|Star Grenade (Lesser)|Granada Estelar (Menor)|modificada|
|[weapon-01-WJMHv8xXowLNP2Di.htm](equipment/weapon-01-WJMHv8xXowLNP2Di.htm)|Junk Bomb (Lesser)|Bomba Basura (Menor)|modificada|
|[weapon-03-0U4qXSxfZDMIJDtA.htm](equipment/weapon-03-0U4qXSxfZDMIJDtA.htm)|Mud Bomb (Moderate)|Bomba de barro (moderada)|modificada|
|[weapon-03-1MuIK1TnzdKuqZJO.htm](equipment/weapon-03-1MuIK1TnzdKuqZJO.htm)|Junk Bomb (Moderate)|Bomba Basura (Moderada)|modificada|
|[weapon-03-2pfEtA97lpG93kG2.htm](equipment/weapon-03-2pfEtA97lpG93kG2.htm)|Sulfur Bomb (Moderate)|Bomba de azufre (moderada)|modificada|
|[weapon-03-5BU4hJ1oXMLtSkuA.htm](equipment/weapon-03-5BU4hJ1oXMLtSkuA.htm)|Sparkblade|Sparkblade|modificada|
|[weapon-03-7irlzyZA50KqTwce.htm](equipment/weapon-03-7irlzyZA50KqTwce.htm)|Boastful Hunter|Cazador fanfarrón|modificada|
|[weapon-03-cbgJbCMU330TVmmO.htm](equipment/weapon-03-cbgJbCMU330TVmmO.htm)|Pernicious Spore Bomb (Moderate)|Bomba de esporas perniciosas (moderada)|modificada|
|[weapon-03-da6nSPRUwGicvx3c.htm](equipment/weapon-03-da6nSPRUwGicvx3c.htm)|Pressure Bomb (Moderate)|Bomba de presión (moderada)|modificada|
|[weapon-03-hZ0s1gz0Cr0LGOLG.htm](equipment/weapon-03-hZ0s1gz0Cr0LGOLG.htm)|Water Bomb (Moderate)|Bomba de Agua (Moderada)|modificada|
|[weapon-03-KFTkiLXWENatK9XY.htm](equipment/weapon-03-KFTkiLXWENatK9XY.htm)|Star Grenade (Moderate)|Granada Estelar (Moderada)|modificada|
|[weapon-03-moBSgma1rtJqh7Rp.htm](equipment/weapon-03-moBSgma1rtJqh7Rp.htm)|Necrotic Bomb (Moderate)|Bomba necrótica (moderada)|modificada|
|[weapon-03-Z9TThSU2KtCX9gfD.htm](equipment/weapon-03-Z9TThSU2KtCX9gfD.htm)|Vexing Vapor (Moderate)|Vapor Vexing (Moderada)|modificada|
|[weapon-04-5TOdQaW1XEjDhTP0.htm](equipment/weapon-04-5TOdQaW1XEjDhTP0.htm)|Drake Rifle (Cold)|Rifle Drake (Frío)|modificada|
|[weapon-04-FU9tZ3neEPM8szay.htm](equipment/weapon-04-FU9tZ3neEPM8szay.htm)|Drake Rifle (Acid)|Drake Rifle (Acid)|modificada|
|[weapon-04-Gfu13B6b9eDdHCtV.htm](equipment/weapon-04-Gfu13B6b9eDdHCtV.htm)|Drake Rifle (Fire)|Drake Rifle (Fuego)|modificada|
|[weapon-04-h1G2xCiMrHd7uFko.htm](equipment/weapon-04-h1G2xCiMrHd7uFko.htm)|Drake Rifle (Electricity)|Drake Rifle (Electricidad)|modificada|
|[weapon-04-V4wgOWYmHlbSZsVG.htm](equipment/weapon-04-V4wgOWYmHlbSZsVG.htm)|Crystal Shards (Moderate)|Fragmentos de cristal (moderada)|modificada|
|[weapon-04-ywUMtG1viqbnDFYT.htm](equipment/weapon-04-ywUMtG1viqbnDFYT.htm)|Drake Rifle (Poison)|Drake Rifle (Poison)|modificada|
|[weapon-05-hK5xPjOFd2hb81mi.htm](equipment/weapon-05-hK5xPjOFd2hb81mi.htm)|Poisonous Dagger|Daga venenosa|modificada|
|[weapon-06-214J8KhXn2H19DeC.htm](equipment/weapon-06-214J8KhXn2H19DeC.htm)|Staff of the Dreamlands|Bastón de las Tierras del Sue.|modificada|
|[weapon-06-70H54rIRjO9fJkdt.htm](equipment/weapon-06-70H54rIRjO9fJkdt.htm)|Spider Gun|Spider Gun|modificada|
|[weapon-06-tNue4PqJe85ZEE5v.htm](equipment/weapon-06-tNue4PqJe85ZEE5v.htm)|Bloodletting Kukri|Kukri sangriento|modificada|
|[weapon-07-Cb0h1Y2OSrbYXJQ2.htm](equipment/weapon-07-Cb0h1Y2OSrbYXJQ2.htm)|Spellstriker Staff|Spellstriker Staff|modificada|
|[weapon-08-dfheZ5lK0thQUTVT.htm](equipment/weapon-08-dfheZ5lK0thQUTVT.htm)|Flaying Knife|Cuchillo desollador|modificada|
|[weapon-10-zB0Q86QD0A10CeFH.htm](equipment/weapon-10-zB0Q86QD0A10CeFH.htm)|Batsbreath Cane|Batsbreath Cane|modificada|
|[weapon-11-0MPBjp2lOYVCsgeg.htm](equipment/weapon-11-0MPBjp2lOYVCsgeg.htm)|Vexing Vapor (Greater)|Vapor Vexing (Mayor)|modificada|
|[weapon-11-2GngLv6m3uHObFGZ.htm](equipment/weapon-11-2GngLv6m3uHObFGZ.htm)|Sulfur Bomb (Greater)|Bomba de azufre (mayor)|modificada|
|[weapon-11-9j3sqH9sG5mUe6rI.htm](equipment/weapon-11-9j3sqH9sG5mUe6rI.htm)|Mud Bomb (Greater)|Bomba de barro (Mayor)|modificada|
|[weapon-11-DheIz1WCPXwUhxQD.htm](equipment/weapon-11-DheIz1WCPXwUhxQD.htm)|Pressure Bomb (Greater)|Bomba de presión (Mayor)|modificada|
|[weapon-11-EtIXWXnfYdV5AGzy.htm](equipment/weapon-11-EtIXWXnfYdV5AGzy.htm)|Tiger's Claw|Garra de Tigre|modificada|
|[weapon-11-itn6l0yJGRw7EFwu.htm](equipment/weapon-11-itn6l0yJGRw7EFwu.htm)|Water Bomb (Greater)|Bomba de Agua (Mayor)|modificada|
|[weapon-11-QhUTsTnbehv1ft9u.htm](equipment/weapon-11-QhUTsTnbehv1ft9u.htm)|Junk Bomb (Greater)|Bomba Basura (Mayor)|modificada|
|[weapon-11-TvEnLYsYjijBe1Cg.htm](equipment/weapon-11-TvEnLYsYjijBe1Cg.htm)|Pernicious Spore Bomb (Greater)|Bomba de Esporas Perniciosas (Mayor)|modificada|
|[weapon-11-u41wQz3bgu6J1XKo.htm](equipment/weapon-11-u41wQz3bgu6J1XKo.htm)|Necrotic Bomb (Greater)|Bomba Necrótica (Mayor)|modificada|
|[weapon-11-W1XG0qStVh8fmeJ6.htm](equipment/weapon-11-W1XG0qStVh8fmeJ6.htm)|Star Grenade (Greater)|Granada Estelar (Mayor)|modificada|
|[weapon-11-Y2L8NM50BvRfUaat.htm](equipment/weapon-11-Y2L8NM50BvRfUaat.htm)|Spellstriker Staff (Greater)|Spellstriker Staff (Greater)|modificada|
|[weapon-12-eHs1vJMXGLIPues8.htm](equipment/weapon-12-eHs1vJMXGLIPues8.htm)|Staff of the Dreamlands (Greater)|Bastón de las Tierras del Sue (mayor).|modificada|
|[weapon-12-ivaL0xt33k6QNwQK.htm](equipment/weapon-12-ivaL0xt33k6QNwQK.htm)|Crystal Shards (Greater)|Fragmentos de cristal (mayor)|modificada|
|[weapon-12-qx4Cq99vng6GhzEh.htm](equipment/weapon-12-qx4Cq99vng6GhzEh.htm)|Staff of Fire (Major)|Bastón de fuego (superior).|modificada|
|[weapon-12-zzLGE9zmSu3yMEOq.htm](equipment/weapon-12-zzLGE9zmSu3yMEOq.htm)|Beast Staff (Greater)|Báculo de Bestia (Mayor)|modificada|
|[weapon-14-UFAIquTmQcgqSl7s.htm](equipment/weapon-14-UFAIquTmQcgqSl7s.htm)|Blessed Reformer|bendito Reformador|modificada|
|[weapon-15-dIUmjoLjlcrKgXbH.htm](equipment/weapon-15-dIUmjoLjlcrKgXbH.htm)|Golden Blade of Mzali|Golden Blade of Mzali|modificada|
|[weapon-15-PeRLqNTbzaiITNuO.htm](equipment/weapon-15-PeRLqNTbzaiITNuO.htm)|Beast Staff (Major)|Beast Staff (Major)|modificada|
|[weapon-16-AFkybwwQzAArjrF4.htm](equipment/weapon-16-AFkybwwQzAArjrF4.htm)|Spellstriker Staff (Major)|Spellstriker Staff (Major)|modificada|
|[weapon-17-4Thv3O2smZ8AHNjP.htm](equipment/weapon-17-4Thv3O2smZ8AHNjP.htm)|Pernicious Spore Bomb (Major)|Bomba de esporas perniciosas (superior)|modificada|
|[weapon-17-Djp91D21I6QW6dlw.htm](equipment/weapon-17-Djp91D21I6QW6dlw.htm)|Mud Bomb (Major)|Bomba de barro (superior)|modificada|
|[weapon-17-EjJUCXG4Yr3T6DTM.htm](equipment/weapon-17-EjJUCXG4Yr3T6DTM.htm)|Water Bomb (Major)|Bomba de Agua (Superior)|modificada|
|[weapon-17-K291ECjrRssmH93Q.htm](equipment/weapon-17-K291ECjrRssmH93Q.htm)|Junk Bomb (Major)|Bomba basura (superior)|modificada|
|[weapon-17-Otd9pV58s2LFK1gW.htm](equipment/weapon-17-Otd9pV58s2LFK1gW.htm)|Pressure Bomb (Major)|Bomba de presión (superior)|modificada|
|[weapon-17-PkqzU1Ap3O3IicUv.htm](equipment/weapon-17-PkqzU1Ap3O3IicUv.htm)|Star Grenade (Major)|Granada estelar (superior)|modificada|
|[weapon-17-RC8EmoFiMITFUopr.htm](equipment/weapon-17-RC8EmoFiMITFUopr.htm)|Necrotic Bomb (Major)|Bomba Necrótica (superior)|modificada|
|[weapon-17-RXpM5Kbuf3BYfK6f.htm](equipment/weapon-17-RXpM5Kbuf3BYfK6f.htm)|Vexing Vapor (Major)|Vapor Vexing (superior)|modificada|
|[weapon-17-yPndyLkx3Nj2GMiz.htm](equipment/weapon-17-yPndyLkx3Nj2GMiz.htm)|Sulfur Bomb (Major)|Bomba de azufre (superior)|modificada|
|[weapon-18-NWMHPsVBIaNZ5X2W.htm](equipment/weapon-18-NWMHPsVBIaNZ5X2W.htm)|Staff of the Dreamlands (Major)|Bastón de las Tierras del Sue (superior).|modificada|
|[weapon-18-OHwsumgfO1oyH7KL.htm](equipment/weapon-18-OHwsumgfO1oyH7KL.htm)|Boulder Seed (Greater)|Semilla de Roca (Mayor)|modificada|
|[weapon-18-V2knI4GpdJYLupjg.htm](equipment/weapon-18-V2knI4GpdJYLupjg.htm)|Crystal Shards (Major)|Fragmentos de cristal (superior)|modificada|
|[weapon-20-FXY5YezrQ13dubnd.htm](equipment/weapon-20-FXY5YezrQ13dubnd.htm)|Briar|Zarzas|modificada|
|[weapon-20-tbc3dqTyibuGoCA6.htm](equipment/weapon-20-tbc3dqTyibuGoCA6.htm)|Ovinrbaane|Ovinrbaane|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[armor-00-5yL2D3GPAggadZQN.htm](equipment/armor-00-5yL2D3GPAggadZQN.htm)|Light Barding|vacía|
|[armor-02-juQGJI9deskAVfm7.htm](equipment/armor-02-juQGJI9deskAVfm7.htm)|Heavy Barding (Small or Medium)|vacía|
|[armor-03-MUtAHJXf22SiVBeH.htm](equipment/armor-03-MUtAHJXf22SiVBeH.htm)|Heavy Barding (Large)|vacía|
|[consumable-00-xwHTZGzD8HQrVSal.htm](equipment/consumable-00-xwHTZGzD8HQrVSal.htm)|Shield Pistol Rounds|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[consumable-00-MKSeXwUm56c15MZa.htm](equipment/consumable-00-MKSeXwUm56c15MZa.htm)|Sling Bullets|Bala de honda|oficial|
|[consumable-00-sqhr1crb184s3Vnd.htm](equipment/consumable-00-sqhr1crb184s3Vnd.htm)|Blowgun Darts|Dardo de cerbatana|oficial|
|[weapon-00-62nnVQvGhoVLLl2K.htm](equipment/weapon-00-62nnVQvGhoVLLl2K.htm)|Crossbow|Ballesta|oficial|
|[weapon-00-c58wczIzH2gzeXQL.htm](equipment/weapon-00-c58wczIzH2gzeXQL.htm)|Club|Clava|oficial|
|[weapon-00-dgWxsYm0DWHb27h6.htm](equipment/weapon-00-dgWxsYm0DWHb27h6.htm)|Halberd|Alabarda|oficial|
|[weapon-00-dUC8Fsa6FZtVikS3.htm](equipment/weapon-00-dUC8Fsa6FZtVikS3.htm)|Composite Longbow|Arco Largo Compuesto|oficial|
|[weapon-00-DV4qelKHrviM0O5i.htm](equipment/weapon-00-DV4qelKHrviM0O5i.htm)|Halfling Sling Staff|Bastón honda mediano|oficial|
|[weapon-00-e4NwsnPnpQKbDZ9F.htm](equipment/weapon-00-e4NwsnPnpQKbDZ9F.htm)|Composite Shortbow|Arco corto compuesto|oficial|
|[weapon-00-FPwsiGqMCNPLHmjX.htm](equipment/weapon-00-FPwsiGqMCNPLHmjX.htm)|Blowgun|Cerbatana|oficial|
|[weapon-00-FVjTuBCIefAgloUU.htm](equipment/weapon-00-FVjTuBCIefAgloUU.htm)|Staff|Bastón|oficial|
|[weapon-00-giO4LwIKGzJZoaxa.htm](equipment/weapon-00-giO4LwIKGzJZoaxa.htm)|Heavy Crossbow|Ballesta pesada|oficial|
|[weapon-00-hIgqLgH3YcLZBeoT.htm](equipment/weapon-00-hIgqLgH3YcLZBeoT.htm)|Shortbow|Arco corto|oficial|
|[weapon-00-hMYdSFmMWzidzHih.htm](equipment/weapon-00-hMYdSFmMWzidzHih.htm)|Bo Staff|Bastón Bo|oficial|
|[weapon-00-MVAWttmT0QDa7LsV.htm](equipment/weapon-00-MVAWttmT0QDa7LsV.htm)|Longbow|Arco largo|oficial|
|[weapon-00-TLQErnOpM9Luy7rL.htm](equipment/weapon-00-TLQErnOpM9Luy7rL.htm)|Sap|Cachiporra|oficial|
|[weapon-00-TT0pJz9odKApfq6D.htm](equipment/weapon-00-TT0pJz9odKApfq6D.htm)|Guisarme|Bisarma|oficial|
|[weapon-00-UCH4myuFnokGv0vF.htm](equipment/weapon-00-UCH4myuFnokGv0vF.htm)|Sling|Honda|oficial|
|[weapon-00-XGtIUZ4ZNKuFx1uL.htm](equipment/weapon-00-XGtIUZ4ZNKuFx1uL.htm)|Falchion|Alfanje|oficial|
|[weapon-00-XyA6PKV46aNlLXOd.htm](equipment/weapon-00-XyA6PKV46aNlLXOd.htm)|Hand Crossbow|Ballesta de mano|oficial|
|[weapon-00-zi9ovfoRp2fMhfpO.htm](equipment/weapon-00-zi9ovfoRp2fMhfpO.htm)|Spiked Chain|Cadena armada|oficial|
