# Estado de la traducción (hazards-items)

 * **auto-trad**: 79


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0fjRyLkdj1a2kfGM.htm](hazards-items/0fjRyLkdj1a2kfGM.htm)|Falling Scythes|auto-trad|
|[0FpzSygcs7G688GT.htm](hazards-items/0FpzSygcs7G688GT.htm)|Quietus|auto-trad|
|[0KrOaCLFSEcIdcej.htm](hazards-items/0KrOaCLFSEcIdcej.htm)|Spear|auto-trad|
|[17gJOHIRtWXfYMKa.htm](hazards-items/17gJOHIRtWXfYMKa.htm)|Sap Vitality|auto-trad|
|[1bvXbELRBoJTBqsR.htm](hazards-items/1bvXbELRBoJTBqsR.htm)|Spring|auto-trad|
|[1M9OasbKWAPME9IC.htm](hazards-items/1M9OasbKWAPME9IC.htm)|Summon Monster|auto-trad|
|[34y07VryqAVeJhfG.htm](hazards-items/34y07VryqAVeJhfG.htm)|Slam Shut|auto-trad|
|[3oSIIlS5PZwciZ3k.htm](hazards-items/3oSIIlS5PZwciZ3k.htm)|Poisoned Dart|auto-trad|
|[4QF1yNgj2FX7rO4C.htm](hazards-items/4QF1yNgj2FX7rO4C.htm)|Total Decapitation|auto-trad|
|[6wV9i41Jd4HThaAi.htm](hazards-items/6wV9i41Jd4HThaAi.htm)|Speed|auto-trad|
|[8yNeU3TLczTIlFjP.htm](hazards-items/8yNeU3TLczTIlFjP.htm)|Lunging Dead|auto-trad|
|[aJouqXgwCGFTWYHD.htm](hazards-items/aJouqXgwCGFTWYHD.htm)|Objects|auto-trad|
|[aNYeaG5N3o5PQ9IB.htm](hazards-items/aNYeaG5N3o5PQ9IB.htm)|Dissolving Ambush|auto-trad|
|[B486xwl3ZYYCqNyw.htm](hazards-items/B486xwl3ZYYCqNyw.htm)|Spear|auto-trad|
|[Bov9feGVYpn5nmUl.htm](hazards-items/Bov9feGVYpn5nmUl.htm)|Prelude|auto-trad|
|[caUgV11OydLsLw1j.htm](hazards-items/caUgV11OydLsLw1j.htm)|Forbid Entry|auto-trad|
|[Cc30YjKZ7qSxVjSC.htm](hazards-items/Cc30YjKZ7qSxVjSC.htm)|Curse the Intruders|auto-trad|
|[CHZ0RRKgpq6eI2AS.htm](hazards-items/CHZ0RRKgpq6eI2AS.htm)|Ensnare|auto-trad|
|[ddB24NYRgK5YbbcR.htm](hazards-items/ddB24NYRgK5YbbcR.htm)|In the Beginning|auto-trad|
|[DE7tTstJaGz9oAi4.htm](hazards-items/DE7tTstJaGz9oAi4.htm)|Rockslide|auto-trad|
|[E8X8ETVNwXuUF08C.htm](hazards-items/E8X8ETVNwXuUF08C.htm)|Searing Agony|auto-trad|
|[e90G0Brsf6uhlGp6.htm](hazards-items/e90G0Brsf6uhlGp6.htm)|Rend Magic|auto-trad|
|[eauYGISAGprcOwH4.htm](hazards-items/eauYGISAGprcOwH4.htm)|Devour|auto-trad|
|[eLyrxG4My2vDsmHg.htm](hazards-items/eLyrxG4My2vDsmHg.htm)|No MAP|auto-trad|
|[eTpo3PPNbe4sFYhD.htm](hazards-items/eTpo3PPNbe4sFYhD.htm)|Spore Explosion|auto-trad|
|[eY731rvsYJZ2o5Sh.htm](hazards-items/eY731rvsYJZ2o5Sh.htm)|Flesset Poison|auto-trad|
|[Ey8CknllSG5csxdG.htm](hazards-items/Ey8CknllSG5csxdG.htm)|Submerge|auto-trad|
|[F0I1pLJQm1AQtXPH.htm](hazards-items/F0I1pLJQm1AQtXPH.htm)|Rising Pillar|auto-trad|
|[f9aVsy30iGDJTtb4.htm](hazards-items/f9aVsy30iGDJTtb4.htm)|Snap Shut|auto-trad|
|[FzfgLXTt6R8bYmTP.htm](hazards-items/FzfgLXTt6R8bYmTP.htm)|Electrocution|auto-trad|
|[gxjS3APksgflCuua.htm](hazards-items/gxjS3APksgflCuua.htm)|Burst Free|auto-trad|
|[h1kUAmChVAo2xByF.htm](hazards-items/h1kUAmChVAo2xByF.htm)|Clockwork Fist|auto-trad|
|[hEtVGo9EpKcXlhPI.htm](hazards-items/hEtVGo9EpKcXlhPI.htm)|Spectral Impale|auto-trad|
|[HhzDfHd26GkdqhME.htm](hazards-items/HhzDfHd26GkdqhME.htm)|Shock|auto-trad|
|[hK2a0NJwqQgjs15q.htm](hazards-items/hK2a0NJwqQgjs15q.htm)|Mark for Damnation|auto-trad|
|[hkSWLZaKG41zef8w.htm](hazards-items/hkSWLZaKG41zef8w.htm)|Continuous Barrage|auto-trad|
|[hoSnbd89PgLy85mP.htm](hazards-items/hoSnbd89PgLy85mP.htm)|Snowdrop|auto-trad|
|[HoWRGPkBlJjsL9zl.htm](hazards-items/HoWRGPkBlJjsL9zl.htm)|Cladis Poison|auto-trad|
|[i9qsvHuq9IdeRohO.htm](hazards-items/i9qsvHuq9IdeRohO.htm)|Call of the Ground|auto-trad|
|[iji2yCS44gpqc5p3.htm](hazards-items/iji2yCS44gpqc5p3.htm)|Pitfall|auto-trad|
|[j3QvHDCU1gvOInG4.htm](hazards-items/j3QvHDCU1gvOInG4.htm)|Deadfall|auto-trad|
|[JEXj6WkaqB0iscnL.htm](hazards-items/JEXj6WkaqB0iscnL.htm)|Reflection of Evil|auto-trad|
|[jSBfyDGUXAPbIJTT.htm](hazards-items/jSBfyDGUXAPbIJTT.htm)|Whirling Blades|auto-trad|
|[K7Ya0U5jerfvH0vw.htm](hazards-items/K7Ya0U5jerfvH0vw.htm)|Into the Great Beyond|auto-trad|
|[KaYeYfefttO5X9OF.htm](hazards-items/KaYeYfefttO5X9OF.htm)|Burn It All|auto-trad|
|[KFCwJINs8Uu4riRs.htm](hazards-items/KFCwJINs8Uu4riRs.htm)|Dart Volley|auto-trad|
|[lY83oUjx0DLxDByK.htm](hazards-items/lY83oUjx0DLxDByK.htm)|Pitfall|auto-trad|
|[LZTjCUaLv54zwB8W.htm](hazards-items/LZTjCUaLv54zwB8W.htm)|Web Noose|auto-trad|
|[mPVesRXUMN4Ws9IR.htm](hazards-items/mPVesRXUMN4Ws9IR.htm)|Decapitation|auto-trad|
|[MQ3CKYIdF6zrMg0x.htm](hazards-items/MQ3CKYIdF6zrMg0x.htm)|Leech Warmth|auto-trad|
|[nCcx1LK7XrbDnVXI.htm](hazards-items/nCcx1LK7XrbDnVXI.htm)|Sportlebore Infestation|auto-trad|
|[NcyiM0WihYrncbSF.htm](hazards-items/NcyiM0WihYrncbSF.htm)|Emit Cold|auto-trad|
|[NjmHPcx6IEA14Yul.htm](hazards-items/NjmHPcx6IEA14Yul.htm)|Powder Burst|auto-trad|
|[nqcgUfOiRWy6Kti7.htm](hazards-items/nqcgUfOiRWy6Kti7.htm)|Scream|auto-trad|
|[oaNkRH35bkjdSm6P.htm](hazards-items/oaNkRH35bkjdSm6P.htm)|Scythe|auto-trad|
|[oZGdTS4jvoAj1myH.htm](hazards-items/oZGdTS4jvoAj1myH.htm)|Profane Chant|auto-trad|
|[pLuxXYKq1Wi28Czn.htm](hazards-items/pLuxXYKq1Wi28Czn.htm)|Hammer|auto-trad|
|[Pz2JfI4HEqH5iEwA.htm](hazards-items/Pz2JfI4HEqH5iEwA.htm)|Filth Fever|auto-trad|
|[Q2074Gj6y8XUUgAk.htm](hazards-items/Q2074Gj6y8XUUgAk.htm)|Awaken|auto-trad|
|[qBQpsAk64L9c0sJ2.htm](hazards-items/qBQpsAk64L9c0sJ2.htm)|Titanic Flytrap Toxin|auto-trad|
|[QrgnumULVnz8pWfK.htm](hazards-items/QrgnumULVnz8pWfK.htm)|Unmask|auto-trad|
|[sBSa8YzXeDYM0ODl.htm](hazards-items/sBSa8YzXeDYM0ODl.htm)|Shriek|auto-trad|
|[slQb54KbvJ8vJpRb.htm](hazards-items/slQb54KbvJ8vJpRb.htm)|Flume Activation|auto-trad|
|[SoObyOgEreU0KLDs.htm](hazards-items/SoObyOgEreU0KLDs.htm)|Adrift in Time|auto-trad|
|[SvqJNCRH2eI1jZk1.htm](hazards-items/SvqJNCRH2eI1jZk1.htm)|Steam Blast|auto-trad|
|[SvVSBExFgbjDrBvW.htm](hazards-items/SvVSBExFgbjDrBvW.htm)|Agitate|auto-trad|
|[T0k2Rnyz49qG1Gkv.htm](hazards-items/T0k2Rnyz49qG1Gkv.htm)|Saw Blade|auto-trad|
|[TxhvkFafggYWv5D7.htm](hazards-items/TxhvkFafggYWv5D7.htm)|Baleful Polymorph|auto-trad|
|[tyAeSBnHmP9Nuh2k.htm](hazards-items/tyAeSBnHmP9Nuh2k.htm)|Yellow Mold Spores|auto-trad|
|[uJpg7vUJ1qag7R3t.htm](hazards-items/uJpg7vUJ1qag7R3t.htm)|Fireball|auto-trad|
|[V7tWWuB9NSEsMEae.htm](hazards-items/V7tWWuB9NSEsMEae.htm)|Special|auto-trad|
|[vFBoAHwlrVTu9wKm.htm](hazards-items/vFBoAHwlrVTu9wKm.htm)|Spine|auto-trad|
|[VNaaW87S24zzxXZh.htm](hazards-items/VNaaW87S24zzxXZh.htm)|Noose|auto-trad|
|[vS5xxWVCLY1BNE4O.htm](hazards-items/vS5xxWVCLY1BNE4O.htm)|Spinning Blade|auto-trad|
|[wdA0nAhiSTY7znrs.htm](hazards-items/wdA0nAhiSTY7znrs.htm)|Shadow Barbs|auto-trad|
|[WJtDPJzNrJO19H6k.htm](hazards-items/WJtDPJzNrJO19H6k.htm)|Infinite Pitfall|auto-trad|
|[WleycA2DC7Pz8RYR.htm](hazards-items/WleycA2DC7Pz8RYR.htm)|Mimic Food|auto-trad|
|[Yw05wWhZB9fLdatc.htm](hazards-items/Yw05wWhZB9fLdatc.htm)|Jaws|auto-trad|
|[ZS4b3rYv9jOgbMzm.htm](hazards-items/ZS4b3rYv9jOgbMzm.htm)|Wheel Spin|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
