# Estado de la traducción (ac-features)

 * **auto-trad**: 110
 * **ninguna**: 1


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[fdrq5fvlMiYoNKvx.htm](ac-features/fdrq5fvlMiYoNKvx.htm)|Gulp Blood|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0i2ZSrRpjKG0KV4U.htm](ac-features/0i2ZSrRpjKG0KV4U.htm)|Eidolon Ability Boosts (20th)|auto-trad|
|[1maDsT21my4REUEX.htm](ac-features/1maDsT21my4REUEX.htm)|Creeping Plant|auto-trad|
|[3mUx8vXEQkiIuBcR.htm](ac-features/3mUx8vXEQkiIuBcR.htm)|Negative Healing|auto-trad|
|[3Ns5BmFpAcoGQIY5.htm](ac-features/3Ns5BmFpAcoGQIY5.htm)|Drain Life|auto-trad|
|[4NuAF8jxWDWzia9Z.htm](ac-features/4NuAF8jxWDWzia9Z.htm)|Dutiful Retaliation|auto-trad|
|[4ZCaD8Y817JjbrX2.htm](ac-features/4ZCaD8Y817JjbrX2.htm)|Field of Roots|auto-trad|
|[5E71mdImeRE9Fh1D.htm](ac-features/5E71mdImeRE9Fh1D.htm)|Eidolon Ability|auto-trad|
|[5gnUVvjqXD99OpUX.htm](ac-features/5gnUVvjqXD99OpUX.htm)|Strength Boost|auto-trad|
|[5I9rbPGBER1gzqXT.htm](ac-features/5I9rbPGBER1gzqXT.htm)|Guardian Plant|auto-trad|
|[6uCSkTAltByUK2Pb.htm](ac-features/6uCSkTAltByUK2Pb.htm)|Traveler's Aura|auto-trad|
|[7K6UJ3D3owmOQS03.htm](ac-features/7K6UJ3D3owmOQS03.htm)|Seething Frenzy|auto-trad|
|[8dyatrsCMoIPuOlE.htm](ac-features/8dyatrsCMoIPuOlE.htm)|Primary (1d6, deadly d8, finesse)|auto-trad|
|[8EJNylotPDelRtZK.htm](ac-features/8EJNylotPDelRtZK.htm)|Primary (1d8, nonlethal)|auto-trad|
|[8IodRUpyU4nXGyAv.htm](ac-features/8IodRUpyU4nXGyAv.htm)|Primary (1d6, fatal d10)|auto-trad|
|[8PDmjqUZ1qfVhnFx.htm](ac-features/8PDmjqUZ1qfVhnFx.htm)|Eidolon Defensive Expertise|auto-trad|
|[a6supDv34hhqsktB.htm](ac-features/a6supDv34hhqsktB.htm)|Eidolon Weapon Specialization|auto-trad|
|[AaVyAec0r17ocz42.htm](ac-features/AaVyAec0r17ocz42.htm)|Reconfigured Evolution|auto-trad|
|[ADnKqMNT8kKfPl9o.htm](ac-features/ADnKqMNT8kKfPl9o.htm)|Trickster Fey|auto-trad|
|[ajYVsdmTxkHEXWVO.htm](ac-features/ajYVsdmTxkHEXWVO.htm)|Wrecker Demon|auto-trad|
|[At61D9oWr39Ss6Ea.htm](ac-features/At61D9oWr39Ss6Ea.htm)|Wisdom Boost|auto-trad|
|[bFHz5ZHKMGTDkggN.htm](ac-features/bFHz5ZHKMGTDkggN.htm)|Eidolon Defensive Mastery|auto-trad|
|[BjE6osOFlAishRbh.htm](ac-features/BjE6osOFlAishRbh.htm)|Anchored Incorporeality|auto-trad|
|[bjSHfY92UwLjdWZC.htm](ac-features/bjSHfY92UwLjdWZC.htm)|Scout Construct|auto-trad|
|[C0zzz9wVwfmGtfIQ.htm](ac-features/C0zzz9wVwfmGtfIQ.htm)|Blasphemous Decree|auto-trad|
|[c2c6dqt5rMakwb4V.htm](ac-features/c2c6dqt5rMakwb4V.htm)|Warrior Construct|auto-trad|
|[CBJrHjIETQsh3OG6.htm](ac-features/CBJrHjIETQsh3OG6.htm)|Furious Strike|auto-trad|
|[CdQUkt9tPINNqSok.htm](ac-features/CdQUkt9tPINNqSok.htm)|Negative Essence|auto-trad|
|[cIAB28BfwNA2uTQv.htm](ac-features/cIAB28BfwNA2uTQv.htm)|Primary (1d6, forceful, sweep)|auto-trad|
|[cnYMvDxCRY4NvUmJ.htm](ac-features/cnYMvDxCRY4NvUmJ.htm)|Visions of Sin|auto-trad|
|[coBen1GA1czeBTOs.htm](ac-features/coBen1GA1czeBTOs.htm)|Whirlwind Maul|auto-trad|
|[CSBy1IgHTSkVAFMM.htm](ac-features/CSBy1IgHTSkVAFMM.htm)|Mindless Companion|auto-trad|
|[doQ6pf2mVdysqgfB.htm](ac-features/doQ6pf2mVdysqgfB.htm)|Fleet Beast|auto-trad|
|[drVBJuBlzQDb9sS0.htm](ac-features/drVBJuBlzQDb9sS0.htm)|Undead Brute|auto-trad|
|[dTwtFZU9OLpbWLno.htm](ac-features/dTwtFZU9OLpbWLno.htm)|Wyrm's Breath|auto-trad|
|[E7DFHsVNS1xph8DT.htm](ac-features/E7DFHsVNS1xph8DT.htm)|Eidolon Transcendence|auto-trad|
|[ExCuxQ4A31rmtr8Q.htm](ac-features/ExCuxQ4A31rmtr8Q.htm)|Breath Weapon (Negative)|auto-trad|
|[f5t6k3hwRSXqGto1.htm](ac-features/f5t6k3hwRSXqGto1.htm)|Ultimate Reconfiguration|auto-trad|
|[F8wk9ybBPYBeoszZ.htm](ac-features/F8wk9ybBPYBeoszZ.htm)|Skirmisher Fey|auto-trad|
|[FM1HQ1kqC3N9nWr0.htm](ac-features/FM1HQ1kqC3N9nWr0.htm)|Cunning Dragon|auto-trad|
|[fNTyFkFxNCi5O5SE.htm](ac-features/fNTyFkFxNCi5O5SE.htm)|Tendril Strike|auto-trad|
|[GEVoWzCBjidkO6DG.htm](ac-features/GEVoWzCBjidkO6DG.htm)|Primary (1d8, disarm)|auto-trad|
|[gLvVuJKxvL85qv4n.htm](ac-features/gLvVuJKxvL85qv4n.htm)|Brutal Beast|auto-trad|
|[Gn750G9nNM3GiI5k.htm](ac-features/Gn750G9nNM3GiI5k.htm)|Primal Roar|auto-trad|
|[GpeinpTPNrfAijp2.htm](ac-features/GpeinpTPNrfAijp2.htm)|Eidolon Ability Boosts (15th)|auto-trad|
|[gyI9aHjkO84BQ3tR.htm](ac-features/gyI9aHjkO84BQ3tR.htm)|Spirit Touch|auto-trad|
|[hcQsGAZQgqdCckCm.htm](ac-features/hcQsGAZQgqdCckCm.htm)|Primal Roar|auto-trad|
|[HiVt4B5bE2hi0FWc.htm](ac-features/HiVt4B5bE2hi0FWc.htm)|Draconic Frenzy|auto-trad|
|[hzuu7aoFjo0Hwrub.htm](ac-features/hzuu7aoFjo0Hwrub.htm)|Breath Weapon (Poison)|auto-trad|
|[I8fG26JJS29ZYwfZ.htm](ac-features/I8fG26JJS29ZYwfZ.htm)|Breath Weapon|auto-trad|
|[iRlcl9MOhoEL943l.htm](ac-features/iRlcl9MOhoEL943l.htm)|Eidolon Ability Boosts (10th)|auto-trad|
|[ISKjzLSNysEr7Kit.htm](ac-features/ISKjzLSNysEr7Kit.htm)|Rejuvenation|auto-trad|
|[ITQghUQFhbYpD2QM.htm](ac-features/ITQghUQFhbYpD2QM.htm)|Beast's Charge|auto-trad|
|[iunzITJGz0QUudUk.htm](ac-features/iunzITJGz0QUudUk.htm)|Tendril Strike|auto-trad|
|[JcYmSwl3FCjqFEMm.htm](ac-features/JcYmSwl3FCjqFEMm.htm)|Marauding Dragon|auto-trad|
|[jQBe4yXTAcMt2kKQ.htm](ac-features/jQBe4yXTAcMt2kKQ.htm)|Eidolon Ability Boosts (5th)|auto-trad|
|[Kgt1NyBesS3rohzb.htm](ac-features/Kgt1NyBesS3rohzb.htm)|Badger Rage|auto-trad|
|[KKOgokqHa7RjVDN6.htm](ac-features/KKOgokqHa7RjVDN6.htm)|Animal Companion|auto-trad|
|[kpcL3Iz1KJnmEKwC.htm](ac-features/kpcL3Iz1KJnmEKwC.htm)|Field of Roots|auto-trad|
|[KrHyF5BSvYM6tFbB.htm](ac-features/KrHyF5BSvYM6tFbB.htm)|Advanced Maneuver|auto-trad|
|[le0f9D0ZOnkdGaQO.htm](ac-features/le0f9D0ZOnkdGaQO.htm)|Enraged Assassin|auto-trad|
|[lonj7zEZdJD2pbai.htm](ac-features/lonj7zEZdJD2pbai.htm)|Beast's Charge|auto-trad|
|[LoOw9anYYf7LDNZ7.htm](ac-features/LoOw9anYYf7LDNZ7.htm)|Breath Weapon (Cold)|auto-trad|
|[MET0PzSNfpxi0D3E.htm](ac-features/MET0PzSNfpxi0D3E.htm)|Anger Aura|auto-trad|
|[mL6vuNanUJoubOjL.htm](ac-features/mL6vuNanUJoubOjL.htm)|Breath Weapon (Fire)|auto-trad|
|[MmMaKe7G1PQULBjL.htm](ac-features/MmMaKe7G1PQULBjL.htm)|Breath Weapon (Electricity)|auto-trad|
|[mzeQIa11p6ASSxIm.htm](ac-features/mzeQIa11p6ASSxIm.htm)|Breath Weapon (Acid)|auto-trad|
|[MzqIc3XmAUQVdSFO.htm](ac-features/MzqIc3XmAUQVdSFO.htm)|Seething Frenzy|auto-trad|
|[N47g9LDkOdZZdgup.htm](ac-features/N47g9LDkOdZZdgup.htm)|Steadfast Devotion|auto-trad|
|[nhFkOYRdlT5iLzdQ.htm](ac-features/nhFkOYRdlT5iLzdQ.htm)|Dexterity Boost|auto-trad|
|[o3P8heIfNr1z53Ay.htm](ac-features/o3P8heIfNr1z53Ay.htm)|Demonic Strikes|auto-trad|
|[OJePkZgnguu5Z8cA.htm](ac-features/OJePkZgnguu5Z8cA.htm)|Construct Companion|auto-trad|
|[oOBWRXPtYIXWp5Sm.htm](ac-features/oOBWRXPtYIXWp5Sm.htm)|Devotion Aura|auto-trad|
|[oQ4aTLTv0qU7sRGG.htm](ac-features/oQ4aTLTv0qU7sRGG.htm)|Angelic Avenger|auto-trad|
|[oqRBCMQDSicPRkVA.htm](ac-features/oqRBCMQDSicPRkVA.htm)|Wrathful Berserker|auto-trad|
|[oSjJ726kv8wvk1Rb.htm](ac-features/oSjJ726kv8wvk1Rb.htm)|Draconic Frenzy|auto-trad|
|[oxpHUtYre2CTaE4t.htm](ac-features/oxpHUtYre2CTaE4t.htm)|Drain Life|auto-trad|
|[P7qw5V2ylPwCWMPB.htm](ac-features/P7qw5V2ylPwCWMPB.htm)|Wyrm's Breath|auto-trad|
|[pn0rELSmLV6G5FUu.htm](ac-features/pn0rELSmLV6G5FUu.htm)|Fey Mischief|auto-trad|
|[Q8yXShOx8PGjL0LV.htm](ac-features/Q8yXShOx8PGjL0LV.htm)|Furious Strike|auto-trad|
|[QDtXuIshSTUBcxff.htm](ac-features/QDtXuIshSTUBcxff.htm)|Angelic Mercy|auto-trad|
|[qFYR3I9XcjBZckUf.htm](ac-features/qFYR3I9XcjBZckUf.htm)|Hidden Watcher|auto-trad|
|[qhUxX4BhaZc0griR.htm](ac-features/qhUxX4BhaZc0griR.htm)|Charisma Boost|auto-trad|
|[rbOOv8FqBFQlTDeu.htm](ac-features/rbOOv8FqBFQlTDeu.htm)|Eidolon Unarmed Mastery|auto-trad|
|[rFuzSio2a1VOd2Ch.htm](ac-features/rFuzSio2a1VOd2Ch.htm)|Spirit Taker|auto-trad|
|[RQd8FPcVP3hrVXCr.htm](ac-features/RQd8FPcVP3hrVXCr.htm)|Constitution Boost|auto-trad|
|[RsDBv9TwwNMnofud.htm](ac-features/RsDBv9TwwNMnofud.htm)|Eidolon Symbiosis|auto-trad|
|[RvCQCutoKANEqw5C.htm](ac-features/RvCQCutoKANEqw5C.htm)|Intelligence Boost|auto-trad|
|[srfAyOXpUYxEz0gx.htm](ac-features/srfAyOXpUYxEz0gx.htm)|Visions of Sin|auto-trad|
|[sSfOct37bkinwW5E.htm](ac-features/sSfOct37bkinwW5E.htm)|Growing Vines|auto-trad|
|[tBhCZN3SwlxQirL1.htm](ac-features/tBhCZN3SwlxQirL1.htm)|Construct Heart|auto-trad|
|[TDWe85aZadixqmRU.htm](ac-features/TDWe85aZadixqmRU.htm)|Swift Protector|auto-trad|
|[uqwHH20BKk3WcjbW.htm](ac-features/uqwHH20BKk3WcjbW.htm)|Fey Gift Spells|auto-trad|
|[V5bcYgKbJTHdOBHI.htm](ac-features/V5bcYgKbJTHdOBHI.htm)|Dutiful Retaliation|auto-trad|
|[v5dfcKYsaD0EvvtH.htm](ac-features/v5dfcKYsaD0EvvtH.htm)|Secondary Attack|auto-trad|
|[VXlVUNQjQls2ubwX.htm](ac-features/VXlVUNQjQls2ubwX.htm)|Scribe of the Dead|auto-trad|
|[VzJuYopRKC81GteD.htm](ac-features/VzJuYopRKC81GteD.htm)|Greater Eidolon Specialization|auto-trad|
|[VZQ3CcjOZFG57R0O.htm](ac-features/VZQ3CcjOZFG57R0O.htm)|Fey Chicanery|auto-trad|
|[W4VCL85CO77MYkQH.htm](ac-features/W4VCL85CO77MYkQH.htm)|Whirlwind Maul|auto-trad|
|[WeZLk8HXhoa20J7t.htm](ac-features/WeZLk8HXhoa20J7t.htm)|Angelic Emissary|auto-trad|
|[WfNq9WV1ERdAz7xG.htm](ac-features/WfNq9WV1ERdAz7xG.htm)|Tempter Demon|auto-trad|
|[wHIXVGxc57t8Va4G.htm](ac-features/wHIXVGxc57t8Va4G.htm)|Breath Weapon (Piercing)|auto-trad|
|[WRWGcHidHQz4ef2W.htm](ac-features/WRWGcHidHQz4ef2W.htm)|Stalwart Guardian|auto-trad|
|[WyN2qQ2AP4cOMNxF.htm](ac-features/WyN2qQ2AP4cOMNxF.htm)|Hallowed Strikes|auto-trad|
|[xE3lRxl1t4ulQyLa.htm](ac-features/xE3lRxl1t4ulQyLa.htm)|Undead Stalker|auto-trad|
|[XNgPUKsnRQO2OS2O.htm](ac-features/XNgPUKsnRQO2OS2O.htm)|Primary (1d8, shove)|auto-trad|
|[xPn27nNxcLOByTXJ.htm](ac-features/xPn27nNxcLOByTXJ.htm)|Eidolon|auto-trad|
|[yE9n6h53psgbYThV.htm](ac-features/yE9n6h53psgbYThV.htm)|Effect: Seething Frenzy|auto-trad|
|[YkevbtGldcm34DlT.htm](ac-features/YkevbtGldcm34DlT.htm)|Soul Guardian|auto-trad|
|[zF6bLjVwj7Njkqan.htm](ac-features/zF6bLjVwj7Njkqan.htm)|Eidolon Unarmed Expertise|auto-trad|
|[ZXrAWVLwKxz5kovL.htm](ac-features/ZXrAWVLwKxz5kovL.htm)|Primary (1d8, trip)|auto-trad|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
