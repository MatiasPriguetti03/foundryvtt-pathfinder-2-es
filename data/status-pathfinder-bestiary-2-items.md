# Estado de la traducción (pathfinder-bestiary-2-items)

 * **auto-trad**: 2958
 * **ninguna**: 300
 * **vacía**: 107
 * **modificada**: 58


DÚltima actualización: 2023-05-18 18:49 *(hora de Canadá/Montreal)*

Este archivo se genera automáticamente. ¡NO MODIFICAR!
## Lista de traducciones pendientes

| Fichero   | Nombre (EN)    |
|-----------|----------------|
|[0BT4rsp6NVFOXHYe.htm](pathfinder-bestiary-2-items/0BT4rsp6NVFOXHYe.htm)|Dispel Magic (At will)|
|[0mexpNYfz6rpTLXa.htm](pathfinder-bestiary-2-items/0mexpNYfz6rpTLXa.htm)|Kukri|+1,striking|
|[0ODNsZ6th57SXfiA.htm](pathfinder-bestiary-2-items/0ODNsZ6th57SXfiA.htm)|Color Spray (At Will)|
|[1Hex7UaPya8easf7.htm](pathfinder-bestiary-2-items/1Hex7UaPya8easf7.htm)|Jump (At will)|
|[1JQ0EDkjIezvUDEb.htm](pathfinder-bestiary-2-items/1JQ0EDkjIezvUDEb.htm)|Tree Shape (At will) (Appears as a burnt, dead tree)|
|[1RcuxgvvscX2wpNJ.htm](pathfinder-bestiary-2-items/1RcuxgvvscX2wpNJ.htm)|Paranoia (At Will)|
|[2k1TYJ74qItzuakS.htm](pathfinder-bestiary-2-items/2k1TYJ74qItzuakS.htm)|Detect Alignment (Lawful only)|
|[2mXUYLr8NYh6u9sO.htm](pathfinder-bestiary-2-items/2mXUYLr8NYh6u9sO.htm)|Tongues (Constant)|
|[2q5Hz0vUEER1aViF.htm](pathfinder-bestiary-2-items/2q5Hz0vUEER1aViF.htm)|Invisibility (At Will) (Self Only)|
|[2s2y3NKDRdpdH6gx.htm](pathfinder-bestiary-2-items/2s2y3NKDRdpdH6gx.htm)|Fire Shield (Constant)|
|[2tizdcbdj6rD6U6d.htm](pathfinder-bestiary-2-items/2tizdcbdj6rD6U6d.htm)|True Seeing (Constant)|
|[2VooFbfGeZCEGVLM.htm](pathfinder-bestiary-2-items/2VooFbfGeZCEGVLM.htm)|Dimension Door (At will)|
|[30vWGptXw7CTKs0b.htm](pathfinder-bestiary-2-items/30vWGptXw7CTKs0b.htm)|Sling|+1|
|[36CmhzOplKDvSnh4.htm](pathfinder-bestiary-2-items/36CmhzOplKDvSnh4.htm)|Illusory Disguise (At will)|
|[38kb57Hhd2E3vn6A.htm](pathfinder-bestiary-2-items/38kb57Hhd2E3vn6A.htm)|Detect Alignment (At will) (Good only)|
|[3cOLTqxhMIIVbLaC.htm](pathfinder-bestiary-2-items/3cOLTqxhMIIVbLaC.htm)|Circle of Protection (Against evil only)|
|[3de6fgPOs2joOR89.htm](pathfinder-bestiary-2-items/3de6fgPOs2joOR89.htm)|Air Walk (Constant)|
|[3KU5Y0WaS2TkXwb0.htm](pathfinder-bestiary-2-items/3KU5Y0WaS2TkXwb0.htm)|+2 Greater Resilient Full Plate|
|[3M2SYWM5VaJw4Y1F.htm](pathfinder-bestiary-2-items/3M2SYWM5VaJw4Y1F.htm)|Ventriloquism (At will)|
|[3n0E4BJ7jYbNm4ux.htm](pathfinder-bestiary-2-items/3n0E4BJ7jYbNm4ux.htm)|True Seeing (Constant)|
|[3NM1dClgOcqEF0sP.htm](pathfinder-bestiary-2-items/3NM1dClgOcqEF0sP.htm)|Meld into Stone (At will)|
|[4dJ4DHIq0hvHbkYj.htm](pathfinder-bestiary-2-items/4dJ4DHIq0hvHbkYj.htm)|Dimension Door (Self Only)|
|[4irhhdrSS6R8cngA.htm](pathfinder-bestiary-2-items/4irhhdrSS6R8cngA.htm)|Glitterdust (At Will)|
|[4O37hYlL21BWJ3M8.htm](pathfinder-bestiary-2-items/4O37hYlL21BWJ3M8.htm)|Detect Alignment (At will) (Evil only)|
|[4Pg0aWLpvKg5oEEp.htm](pathfinder-bestiary-2-items/4Pg0aWLpvKg5oEEp.htm)|Air Walk (Constant)|
|[4rEcF7higflYgkOC.htm](pathfinder-bestiary-2-items/4rEcF7higflYgkOC.htm)|Dimension Door (At will)|
|[4SMYBQGP5HVl75M7.htm](pathfinder-bestiary-2-items/4SMYBQGP5HVl75M7.htm)|+1 Resilient Breastplate|
|[4TTBHqodJtZulkHb.htm](pathfinder-bestiary-2-items/4TTBHqodJtZulkHb.htm)|Divine Wrath (Chaotic)|
|[4wGOi1Y9QjvOIUWd.htm](pathfinder-bestiary-2-items/4wGOi1Y9QjvOIUWd.htm)|Speak with Animals (Constant)|
|[56O2v35BcXiX1Ksi.htm](pathfinder-bestiary-2-items/56O2v35BcXiX1Ksi.htm)|Talking Corpse (At will)|
|[5AQMHCwBWHE6c3p1.htm](pathfinder-bestiary-2-items/5AQMHCwBWHE6c3p1.htm)|Endure Elements (Constant)|
|[5b4HNF0LotCdB3Bv.htm](pathfinder-bestiary-2-items/5b4HNF0LotCdB3Bv.htm)|+1 Composite Longbow|+1|
|[5FQFNi5wNLjJfVVi.htm](pathfinder-bestiary-2-items/5FQFNi5wNLjJfVVi.htm)|Divine Wrath (At will)|
|[5kLTjDQN7XI3SO30.htm](pathfinder-bestiary-2-items/5kLTjDQN7XI3SO30.htm)|Freedom of Movement (Constant)|
|[5kY50OvZXGwzw1RS.htm](pathfinder-bestiary-2-items/5kY50OvZXGwzw1RS.htm)|Divine Decree (Chaotic only)|
|[5oQstrPyIsACaBES.htm](pathfinder-bestiary-2-items/5oQstrPyIsACaBES.htm)|See Invisibility (Constant)|
|[5pXnmJAJ3WP1fA9Z.htm](pathfinder-bestiary-2-items/5pXnmJAJ3WP1fA9Z.htm)|Speak with Plants (Constant)|
|[5QK8En6tcwzzpyVx.htm](pathfinder-bestiary-2-items/5QK8En6tcwzzpyVx.htm)|Produce Flame (6th)|
|[5u6wc1mpLV2mconA.htm](pathfinder-bestiary-2-items/5u6wc1mpLV2mconA.htm)|See Invisibility (Constant)|
|[6JJdqYYhIoqVcIMD.htm](pathfinder-bestiary-2-items/6JJdqYYhIoqVcIMD.htm)|Primal Call (doesn't require secondary casters)|
|[6MFcZa19cUunLJRu.htm](pathfinder-bestiary-2-items/6MFcZa19cUunLJRu.htm)|Blink (Constant)|
|[6vHpvXfI8ykMKIr4.htm](pathfinder-bestiary-2-items/6vHpvXfI8ykMKIr4.htm)|Air Walk (Constant)|
|[71Z5UP7uzcvxSIy9.htm](pathfinder-bestiary-2-items/71Z5UP7uzcvxSIy9.htm)|Plane Shift (To the Material Plane, Plane of Fire, or Plane of Earth only)|
|[7FgPojTX7HEIApDW.htm](pathfinder-bestiary-2-items/7FgPojTX7HEIApDW.htm)|Blank Formula Book|
|[7p38bExu8L9jgQ5N.htm](pathfinder-bestiary-2-items/7p38bExu8L9jgQ5N.htm)|See Invisibility (Constant)|
|[7rYfyAQXss9duKBu.htm](pathfinder-bestiary-2-items/7rYfyAQXss9duKBu.htm)|Suggestion (At Will)|
|[7UB9CyvhkK4ZhIuW.htm](pathfinder-bestiary-2-items/7UB9CyvhkK4ZhIuW.htm)|Detect Alignment (At will) (Lawful only)|
|[8gcJAk8CnEreKNfO.htm](pathfinder-bestiary-2-items/8gcJAk8CnEreKNfO.htm)|Wall of Fire (At Will)|
|[8lQzvlhmZk1A0Cgw.htm](pathfinder-bestiary-2-items/8lQzvlhmZk1A0Cgw.htm)|Magic Missile (At will)|
|[8vvlKfwG2KuafsIJ.htm](pathfinder-bestiary-2-items/8vvlKfwG2KuafsIJ.htm)|Calm Emotions (At will)|
|[92vCOzCAnkOL6uGy.htm](pathfinder-bestiary-2-items/92vCOzCAnkOL6uGy.htm)|Gust of Wind (At will)|
|[9ejFXBhPxifD2amI.htm](pathfinder-bestiary-2-items/9ejFXBhPxifD2amI.htm)|True Seeing (Constant)|
|[A0HFdfwZ7P5RQBJw.htm](pathfinder-bestiary-2-items/A0HFdfwZ7P5RQBJw.htm)|Charm (At will)|
|[A1Jg6isNRXtE1HSR.htm](pathfinder-bestiary-2-items/A1Jg6isNRXtE1HSR.htm)|Water Walk (Constant)|
|[AaUkJlN6zMw3tT9b.htm](pathfinder-bestiary-2-items/AaUkJlN6zMw3tT9b.htm)|Obscuring Mist (At will)|
|[Aeu3hLK3x9HeXzhD.htm](pathfinder-bestiary-2-items/Aeu3hLK3x9HeXzhD.htm)|Darkness (At Will)|
|[aF866brRh1uTW7EA.htm](pathfinder-bestiary-2-items/aF866brRh1uTW7EA.htm)|+1 Striking Falchion|+1,striking|
|[ajiWSW5GFwz1c2fZ.htm](pathfinder-bestiary-2-items/ajiWSW5GFwz1c2fZ.htm)|Polar Ray (At will)|
|[anKjfgYsttUshhxM.htm](pathfinder-bestiary-2-items/anKjfgYsttUshhxM.htm)|True Seeing (Constant)|
|[anSe09sUoNwtRyCS.htm](pathfinder-bestiary-2-items/anSe09sUoNwtRyCS.htm)|Mind Probe (At will)|
|[aO9yC0TQ3hBNeO78.htm](pathfinder-bestiary-2-items/aO9yC0TQ3hBNeO78.htm)|Obscuring Mist (At Will)|
|[ARgSXbi7YTyqxDpw.htm](pathfinder-bestiary-2-items/ARgSXbi7YTyqxDpw.htm)|Dimension Door (At will)|
|[ARiXN5DX2BVzOvCn.htm](pathfinder-bestiary-2-items/ARiXN5DX2BVzOvCn.htm)|Dimension Door (At will)|
|[aTbUZmqX3wxWV8hR.htm](pathfinder-bestiary-2-items/aTbUZmqX3wxWV8hR.htm)|Dimension Door (At will)|
|[aTZDvupAiIZ4j3su.htm](pathfinder-bestiary-2-items/aTZDvupAiIZ4j3su.htm)|+3 Greater Striking Warhammer|+3,greaterStriking|
|[aV7tH63nZZOcXqzb.htm](pathfinder-bestiary-2-items/aV7tH63nZZOcXqzb.htm)|Tongues (Constant)|
|[AVCJ2SYe5pptCOsw.htm](pathfinder-bestiary-2-items/AVCJ2SYe5pptCOsw.htm)|Fear (At will)|
|[B2XsgB3WjM0q9j54.htm](pathfinder-bestiary-2-items/B2XsgB3WjM0q9j54.htm)|Create Undead (Shadows Only)|
|[b88vNXkalAte3tvf.htm](pathfinder-bestiary-2-items/b88vNXkalAte3tvf.htm)|Restoration (At will)|
|[BDA6jFei5doNakee.htm](pathfinder-bestiary-2-items/BDA6jFei5doNakee.htm)|Dimension Door (At will)|
|[BkBEhqLM3mXNI13M.htm](pathfinder-bestiary-2-items/BkBEhqLM3mXNI13M.htm)|Dimension Door (At Will)|
|[bkk440fw7A5zFFVh.htm](pathfinder-bestiary-2-items/bkk440fw7A5zFFVh.htm)|Illusory Object (At will)|
|[BNtTt4rbMDmj6cYA.htm](pathfinder-bestiary-2-items/BNtTt4rbMDmj6cYA.htm)|Dispel Magic (At will)|
|[boE5n1kGWU3PGXxn.htm](pathfinder-bestiary-2-items/boE5n1kGWU3PGXxn.htm)|Burning Hands (At Will)|
|[BrL7yO7TVS8Vo0MQ.htm](pathfinder-bestiary-2-items/BrL7yO7TVS8Vo0MQ.htm)|+1 Longbow|+1|
|[buj6GwDfNq7DUnUI.htm](pathfinder-bestiary-2-items/buj6GwDfNq7DUnUI.htm)|Hypnotic Pattern (At Will)|
|[BuPE1MFIbw6ZThvx.htm](pathfinder-bestiary-2-items/BuPE1MFIbw6ZThvx.htm)|Detect Alignment (Good Only) (At Will)|
|[bXd6K0aIwu8oNq40.htm](pathfinder-bestiary-2-items/bXd6K0aIwu8oNq40.htm)|Freedom of Movement (Constant)|
|[c19tAQpDeptWNyHM.htm](pathfinder-bestiary-2-items/c19tAQpDeptWNyHM.htm)|Hydraulic Push (At Will)|
|[C3660uLElTZLdhwb.htm](pathfinder-bestiary-2-items/C3660uLElTZLdhwb.htm)|Freedom of Movement (Constant)|
|[c8DVpWzqdTXj4S64.htm](pathfinder-bestiary-2-items/c8DVpWzqdTXj4S64.htm)|True Seeing (Constant)|
|[CbjDPYYSPr0LQHyI.htm](pathfinder-bestiary-2-items/CbjDPYYSPr0LQHyI.htm)|Invisibility (At will) (Self only)|
|[cfgD8ZaLXL5H6PyY.htm](pathfinder-bestiary-2-items/cfgD8ZaLXL5H6PyY.htm)|Dispel Magic (At will)|
|[Cj6s7OuIDPePwy5R.htm](pathfinder-bestiary-2-items/Cj6s7OuIDPePwy5R.htm)|Gust of Wind (At will)|
|[cMrDWgOf9PHIIIZT.htm](pathfinder-bestiary-2-items/cMrDWgOf9PHIIIZT.htm)|True Seeing (Constant)|
|[cMsllioMOMLPtc53.htm](pathfinder-bestiary-2-items/cMsllioMOMLPtc53.htm)|True Seeing (Constant)|
|[cOqmT9M74QEp6QXX.htm](pathfinder-bestiary-2-items/cOqmT9M74QEp6QXX.htm)|Control Water (At will)|
|[cPd8aBLUXVC2uVQc.htm](pathfinder-bestiary-2-items/cPd8aBLUXVC2uVQc.htm)|Burning Hands (At Will)|
|[CTBtapRqTyHVFJWP.htm](pathfinder-bestiary-2-items/CTBtapRqTyHVFJWP.htm)|Dimension Door (At will)|
|[CTeOUny3G8ZgKMct.htm](pathfinder-bestiary-2-items/CTeOUny3G8ZgKMct.htm)|Plane Shift (At Will; Self Plus Skiff and Passengers Only; Astral, Ethereal, and Evil Planes Only)|
|[Cvcdr8l0DxG0Fszv.htm](pathfinder-bestiary-2-items/Cvcdr8l0DxG0Fszv.htm)|Detect Alignment (At will) (Lawful only)|
|[cXiAbItR9q9FYi5R.htm](pathfinder-bestiary-2-items/cXiAbItR9q9FYi5R.htm)|Feather Fall (Self only)|
|[CZh0sJkqcSdR6tyg.htm](pathfinder-bestiary-2-items/CZh0sJkqcSdR6tyg.htm)|Illusory Disguise (At will)|
|[D1Dv5T7CBXZtJx8F.htm](pathfinder-bestiary-2-items/D1Dv5T7CBXZtJx8F.htm)|Darkness (At Will)|
|[d3GX5vViofz5foXq.htm](pathfinder-bestiary-2-items/d3GX5vViofz5foXq.htm)|Dimension Door (At will)|
|[d5mwrX7WIXXz8R97.htm](pathfinder-bestiary-2-items/d5mwrX7WIXXz8R97.htm)|Plane Shift (Self Only)|
|[DbvMliTitDLEnD8H.htm](pathfinder-bestiary-2-items/DbvMliTitDLEnD8H.htm)|Dimension Door (At will)|
|[Dc4o0DNBB2mCvOPt.htm](pathfinder-bestiary-2-items/Dc4o0DNBB2mCvOPt.htm)|Freedom of Movement (Constant)|
|[DfffftC2BwZMqeyZ.htm](pathfinder-bestiary-2-items/DfffftC2BwZMqeyZ.htm)|Obscuring Mist (At will)|
|[E2cmnuR9GJnpvSmN.htm](pathfinder-bestiary-2-items/E2cmnuR9GJnpvSmN.htm)|+1 Resilient Full Plate|
|[eaAeSDb88zeKI7ii.htm](pathfinder-bestiary-2-items/eaAeSDb88zeKI7ii.htm)|Teleport (Self and rider only)|
|[eF4sqCqXrBkfqjib.htm](pathfinder-bestiary-2-items/eF4sqCqXrBkfqjib.htm)|Lesser Smokestick|
|[ehkSYzB2HyKwd9xA.htm](pathfinder-bestiary-2-items/ehkSYzB2HyKwd9xA.htm)|Hide|
|[eiif4YWDshtMsiDY.htm](pathfinder-bestiary-2-items/eiif4YWDshtMsiDY.htm)|Mind Blank (Constant)|
|[eRnSGx9tKMwZStVB.htm](pathfinder-bestiary-2-items/eRnSGx9tKMwZStVB.htm)|Lesser Alchemist's Fire|
|[eXhIeUv9hE9Bnejj.htm](pathfinder-bestiary-2-items/eXhIeUv9hE9Bnejj.htm)|Invisibility (At will) (Self only)|
|[ExTn8RH5Xp1BVvJk.htm](pathfinder-bestiary-2-items/ExTn8RH5Xp1BVvJk.htm)|Illusory Disguise (At Will)|
|[Faz79fumsbTV2o3W.htm](pathfinder-bestiary-2-items/Faz79fumsbTV2o3W.htm)|Mirror Image (At will)|
|[fh90HLxqhj0ijf1r.htm](pathfinder-bestiary-2-items/fh90HLxqhj0ijf1r.htm)|True Seeing (Constant)|
|[fjNBgwnXVFm9Yc36.htm](pathfinder-bestiary-2-items/fjNBgwnXVFm9Yc36.htm)|Augury (At will)|
|[fJsYqRu8WagF6tZb.htm](pathfinder-bestiary-2-items/fJsYqRu8WagF6tZb.htm)|Invisibility (At will) (Self only)|
|[FkEMFgT2F1YJ8CJN.htm](pathfinder-bestiary-2-items/FkEMFgT2F1YJ8CJN.htm)|Invisibility (At will) (Self only)|
|[FtmumZg3l3SZaIex.htm](pathfinder-bestiary-2-items/FtmumZg3l3SZaIex.htm)|Mirror Image (At will)|
|[ftyJjjKcXegKOZO6.htm](pathfinder-bestiary-2-items/ftyJjjKcXegKOZO6.htm)|Elemental Form (Fire Elemental only)|
|[fUotsbKaORew1RjV.htm](pathfinder-bestiary-2-items/fUotsbKaORew1RjV.htm)|Invisibility (Self only)|
|[fVJKjuH7SYz6QQHp.htm](pathfinder-bestiary-2-items/fVJKjuH7SYz6QQHp.htm)|Dimension Door (At will)|
|[Fzzx1tAOIDLb7AC0.htm](pathfinder-bestiary-2-items/Fzzx1tAOIDLb7AC0.htm)|Tongues (Constant)|
|[g0gdzzzvux4ApSD5.htm](pathfinder-bestiary-2-items/g0gdzzzvux4ApSD5.htm)|Glyph of Warding (At will)|
|[g2euZN5HL0HEXaye.htm](pathfinder-bestiary-2-items/g2euZN5HL0HEXaye.htm)|True Seeing (Constant)|
|[GS4ThRRm7cy5BuYJ.htm](pathfinder-bestiary-2-items/GS4ThRRm7cy5BuYJ.htm)|Oar (Functions as Mace)|
|[GXXWozDHxwpERohz.htm](pathfinder-bestiary-2-items/GXXWozDHxwpERohz.htm)|Imprisonment (Temporal stasis only)|
|[h0EZFRSOjyHLlf5b.htm](pathfinder-bestiary-2-items/h0EZFRSOjyHLlf5b.htm)|Detect Alignment (Constant) (Evil only)|
|[H1AVYB6nbAL4PkmI.htm](pathfinder-bestiary-2-items/H1AVYB6nbAL4PkmI.htm)|+3 Greater Striking Sickle|+3,greaterStriking|
|[h67Ean1BYGruHE1T.htm](pathfinder-bestiary-2-items/h67Ean1BYGruHE1T.htm)|Entangle (At will)|
|[hljYIMEGFNQ7w61Z.htm](pathfinder-bestiary-2-items/hljYIMEGFNQ7w61Z.htm)|True Seeing (Constant)|
|[HSgvSQ1CqyZNesYm.htm](pathfinder-bestiary-2-items/HSgvSQ1CqyZNesYm.htm)|Water Walk (Constant)|
|[HsTNpm0qjSAwEMqC.htm](pathfinder-bestiary-2-items/HsTNpm0qjSAwEMqC.htm)|Tongues (Constant)|
|[hTa7eD0P303Y4ykY.htm](pathfinder-bestiary-2-items/hTa7eD0P303Y4ykY.htm)|Plane Shift (Self only)|
|[hYDyNv5iN58yeeg1.htm](pathfinder-bestiary-2-items/hYDyNv5iN58yeeg1.htm)|Suggestion (At will)|
|[HyVnK39Qbmr02ZZS.htm](pathfinder-bestiary-2-items/HyVnK39Qbmr02ZZS.htm)|Religious Symbol of Ydersius|
|[hzBBpY4XdhIZXued.htm](pathfinder-bestiary-2-items/hzBBpY4XdhIZXued.htm)|True Seeing (Constant)|
|[I1dP96sPZylIIzoF.htm](pathfinder-bestiary-2-items/I1dP96sPZylIIzoF.htm)|Detect Alignment (At will) (Good only)|
|[iDFRh2jZ2ogL63IM.htm](pathfinder-bestiary-2-items/iDFRh2jZ2ogL63IM.htm)|Dimension Door (At Will)|
|[IGYasXoP7pT26KvN.htm](pathfinder-bestiary-2-items/IGYasXoP7pT26KvN.htm)|Shape Stone (At will)|
|[IHpzMqwVP4Qwisa6.htm](pathfinder-bestiary-2-items/IHpzMqwVP4Qwisa6.htm)|Sunburst (At will)|
|[iHz93nPS055VlpTQ.htm](pathfinder-bestiary-2-items/iHz93nPS055VlpTQ.htm)|+1 Crossbow|+1|
|[IlW1FymNgiZZvNAV.htm](pathfinder-bestiary-2-items/IlW1FymNgiZZvNAV.htm)|Illusory Disguise (At will)|
|[ippiazWAmHg5l3F6.htm](pathfinder-bestiary-2-items/ippiazWAmHg5l3F6.htm)|+1 Striking Bastard Sword|+1,striking|
|[iQ847whFi4kDiEDu.htm](pathfinder-bestiary-2-items/iQ847whFi4kDiEDu.htm)|Detect Alignment (Constant) (Evil only)|
|[iQNp3D21Ql8PiDZA.htm](pathfinder-bestiary-2-items/iQNp3D21Ql8PiDZA.htm)|+3 Major Striking Greatsword|+3,majorStriking|
|[ISGa39OKNaOBS5Rb.htm](pathfinder-bestiary-2-items/ISGa39OKNaOBS5Rb.htm)|Tangling Creepers (At will)|
|[IV7eERHkT9RjZVgM.htm](pathfinder-bestiary-2-items/IV7eERHkT9RjZVgM.htm)|Speak with Animals (Constant)|
|[ivWNCekJdUWL913h.htm](pathfinder-bestiary-2-items/ivWNCekJdUWL913h.htm)|Invisibility (At Will) (Self Only)|
|[iznQySJVx9S3aKAQ.htm](pathfinder-bestiary-2-items/iznQySJVx9S3aKAQ.htm)|+2 Striking Katana|+2,striking|
|[J5vFJlvjFoW7YiFc.htm](pathfinder-bestiary-2-items/J5vFJlvjFoW7YiFc.htm)|+1 Striking Spiked Chain|+1,striking|
|[J8gjFlA8Gb7zM2ZT.htm](pathfinder-bestiary-2-items/J8gjFlA8Gb7zM2ZT.htm)|Possession (range touch)|
|[jF73rxKIbYkxXiUW.htm](pathfinder-bestiary-2-items/jF73rxKIbYkxXiUW.htm)|+1 Striking Bo Staff|+1,striking|
|[JLbHt0LM9WGhFhGj.htm](pathfinder-bestiary-2-items/JLbHt0LM9WGhFhGj.htm)|Summon Animal (Swarm creatures only)|
|[jRw5SkXrC7GWOpXb.htm](pathfinder-bestiary-2-items/jRw5SkXrC7GWOpXb.htm)|Remove Fear (At will)|
|[jw75q6Yh45aJWTbt.htm](pathfinder-bestiary-2-items/jw75q6Yh45aJWTbt.htm)|Tongues (Constant)|
|[jZget5HVHnwqjGr2.htm](pathfinder-bestiary-2-items/jZget5HVHnwqjGr2.htm)|Ventriloquism (At will)|
|[jZN7KNxo55eAkwom.htm](pathfinder-bestiary-2-items/jZN7KNxo55eAkwom.htm)|Mirror Image (At will)|
|[k4Emo2tqftGwEdIo.htm](pathfinder-bestiary-2-items/k4Emo2tqftGwEdIo.htm)|+1 Resilient Breastplate|
|[K5z1UHEznk6ZV8GU.htm](pathfinder-bestiary-2-items/K5z1UHEznk6ZV8GU.htm)|Dimension Door (At will)|
|[k8jXY7QubcoSOAYR.htm](pathfinder-bestiary-2-items/k8jXY7QubcoSOAYR.htm)|Freedom of Movement (Constant)|
|[KceVAfrBOyOgCGUj.htm](pathfinder-bestiary-2-items/KceVAfrBOyOgCGUj.htm)|Dimension Door (At will)|
|[kfWXgpb3mBY8x5bX.htm](pathfinder-bestiary-2-items/kfWXgpb3mBY8x5bX.htm)|Ethereal Jaunt (At will)|
|[KJp32QDhePr24crV.htm](pathfinder-bestiary-2-items/KJp32QDhePr24crV.htm)|+2 Striking Bastard Sword|+2,striking|
|[KRlaXOK9VXp9mlCh.htm](pathfinder-bestiary-2-items/KRlaXOK9VXp9mlCh.htm)|+2 Greater Striking Starknife|+2,greaterStriking|
|[kxdTx8XfluDNDeWc.htm](pathfinder-bestiary-2-items/kxdTx8XfluDNDeWc.htm)|Dimension Door (At will)|
|[LdMlE7uRJEkPdKUA.htm](pathfinder-bestiary-2-items/LdMlE7uRJEkPdKUA.htm)|Shadow Walk (At will)|
|[LFMYOAZVijC6GmzA.htm](pathfinder-bestiary-2-items/LFMYOAZVijC6GmzA.htm)|Dimension Door (At will)|
|[LHtq8h9QsdHppn66.htm](pathfinder-bestiary-2-items/LHtq8h9QsdHppn66.htm)|Dimension Door (At will)|
|[lIhRNM5msYgcZJCL.htm](pathfinder-bestiary-2-items/lIhRNM5msYgcZJCL.htm)|True Seeing (Constant)|
|[lPHvBGYu9z42HqQj.htm](pathfinder-bestiary-2-items/lPHvBGYu9z42HqQj.htm)|Air Walk (Constant)|
|[lQzCQf7gDhv2S88W.htm](pathfinder-bestiary-2-items/lQzCQf7gDhv2S88W.htm)|Invisibility (At Will) (Self Only)|
|[lSIa7PfqOouOg8nP.htm](pathfinder-bestiary-2-items/lSIa7PfqOouOg8nP.htm)|Dispel Magic (At will)|
|[lsRbdvha9BWdhNaL.htm](pathfinder-bestiary-2-items/lsRbdvha9BWdhNaL.htm)|Spiked Chain|+2,greaterStriking,unholy|
|[LvgsOjqlWG5jltps.htm](pathfinder-bestiary-2-items/LvgsOjqlWG5jltps.htm)|Gust of Wind (At Will)|
|[lWOARoBijDLtXC58.htm](pathfinder-bestiary-2-items/lWOARoBijDLtXC58.htm)|Dimension Door (At will)|
|[lzENfKBITeCmLlWo.htm](pathfinder-bestiary-2-items/lzENfKBITeCmLlWo.htm)|Invisibility (At will) (Self only)|
|[ManCGXxwaYWyKQzU.htm](pathfinder-bestiary-2-items/ManCGXxwaYWyKQzU.htm)|Gaseous Form (At will)|
|[mANOW81qmnDzn8ZY.htm](pathfinder-bestiary-2-items/mANOW81qmnDzn8ZY.htm)|+3 Major Striking Longbow|+3,majorStriking|
|[MBviE2DY2Z7xnOS6.htm](pathfinder-bestiary-2-items/MBviE2DY2Z7xnOS6.htm)|Mace|+1,striking|
|[MgMNZTUTynKhqwV7.htm](pathfinder-bestiary-2-items/MgMNZTUTynKhqwV7.htm)|Detect Magic (Constant) (10th)|
|[mjBHvn4bdXCaG65R.htm](pathfinder-bestiary-2-items/mjBHvn4bdXCaG65R.htm)|Dimension Door (At will)|
|[mkhUcjzL4QL7a659.htm](pathfinder-bestiary-2-items/mkhUcjzL4QL7a659.htm)|Shatter (At will)|
|[MKjUJB31R5kiPnhA.htm](pathfinder-bestiary-2-items/MKjUJB31R5kiPnhA.htm)|Tongues (Constant)|
|[mLsKErBN02PEpFEb.htm](pathfinder-bestiary-2-items/mLsKErBN02PEpFEb.htm)|Augury (At will)|
|[moM05QmLZeKsreVi.htm](pathfinder-bestiary-2-items/moM05QmLZeKsreVi.htm)|Detect Alignment (At will) (Good Only)|
|[MPie4KH85kEeFZ7y.htm](pathfinder-bestiary-2-items/MPie4KH85kEeFZ7y.htm)|Feather Fall (Self only)|
|[mRiJSf4IJmyEQ9Rp.htm](pathfinder-bestiary-2-items/mRiJSf4IJmyEQ9Rp.htm)|Fish Hook|
|[mxpkzQTw0tzSwaUL.htm](pathfinder-bestiary-2-items/mxpkzQTw0tzSwaUL.htm)|Dimension Door (At Will)|
|[mzVH7SLTrdxDaZ3j.htm](pathfinder-bestiary-2-items/mzVH7SLTrdxDaZ3j.htm)|True Seeing (Constant)|
|[NjjDQDdWwJEhS8MF.htm](pathfinder-bestiary-2-items/NjjDQDdWwJEhS8MF.htm)|Dimensional Anchor (At will)|
|[NPRORdlBK4J6mXJz.htm](pathfinder-bestiary-2-items/NPRORdlBK4J6mXJz.htm)|Plane Shift (Self only) (To the Material Plane or Shadow Plane only)|
|[NS4Sroct6rpeddeR.htm](pathfinder-bestiary-2-items/NS4Sroct6rpeddeR.htm)|Acid Arrow (At will)|
|[nv8kWBvEcv3diNEd.htm](pathfinder-bestiary-2-items/nv8kWBvEcv3diNEd.htm)|Detect Alignment (At will) (Lawful only)|
|[nzHApOLkuTxQ7ZED.htm](pathfinder-bestiary-2-items/nzHApOLkuTxQ7ZED.htm)|Air Walk (Constant)|
|[O09US7v1AHXJRsXA.htm](pathfinder-bestiary-2-items/O09US7v1AHXJRsXA.htm)|Dimension Door (At will)|
|[OFeWf6HdNT5mBfJj.htm](pathfinder-bestiary-2-items/OFeWf6HdNT5mBfJj.htm)|Freedom of Movement (Constant)|
|[ofun5QHmnppWzdgV.htm](pathfinder-bestiary-2-items/ofun5QHmnppWzdgV.htm)|Remove Fear (At will)|
|[oIJY4ycXpaF4IjIM.htm](pathfinder-bestiary-2-items/oIJY4ycXpaF4IjIM.htm)|See Invisibility (Constant)|
|[OlEMHzm2vPkpFx3l.htm](pathfinder-bestiary-2-items/OlEMHzm2vPkpFx3l.htm)|Burning Hands (At Will)|
|[olvPYkQEIgtlun6u.htm](pathfinder-bestiary-2-items/olvPYkQEIgtlun6u.htm)|Plane Shift (Self only) (To Shadow Plane only)|
|[OMt6ba5mjMd0d6WH.htm](pathfinder-bestiary-2-items/OMt6ba5mjMd0d6WH.htm)|Detect Alignment (At will) (Evil only)|
|[ot1UCPxTvpQLgwz3.htm](pathfinder-bestiary-2-items/ot1UCPxTvpQLgwz3.htm)|Detect Alignment (At will) (Good only)|
|[OthOWZLzsugOgJdj.htm](pathfinder-bestiary-2-items/OthOWZLzsugOgJdj.htm)|Darkness (At will)|
|[pIbaFSljYvozaZdG.htm](pathfinder-bestiary-2-items/pIbaFSljYvozaZdG.htm)|Tambourine (Handheld Musical Instrument)|
|[pLU70j6ysOj7eVT8.htm](pathfinder-bestiary-2-items/pLU70j6ysOj7eVT8.htm)|Air Walk (Constant)|
|[PvAhmTacSiuheD52.htm](pathfinder-bestiary-2-items/PvAhmTacSiuheD52.htm)|Dimension Door (At will)|
|[pX8XlXGbLapNVOQT.htm](pathfinder-bestiary-2-items/pX8XlXGbLapNVOQT.htm)|Freedom of Movement (Constant)|
|[pyDtidglBpduEzvf.htm](pathfinder-bestiary-2-items/pyDtidglBpduEzvf.htm)|Read Omens (At will)|
|[pZNjrGx8hawqDqpa.htm](pathfinder-bestiary-2-items/pZNjrGx8hawqDqpa.htm)|True Seeing (Constant)|
|[Q3rKZnaS5mmSsg1J.htm](pathfinder-bestiary-2-items/Q3rKZnaS5mmSsg1J.htm)|Detect Poison (At will)|
|[Q61EcGc9xdoDN1Kb.htm](pathfinder-bestiary-2-items/Q61EcGc9xdoDN1Kb.htm)|Speak with Plants (Constant)|
|[QFVCFmJDRYL0iTIf.htm](pathfinder-bestiary-2-items/QFVCFmJDRYL0iTIf.htm)|Air Walk (Constant)|
|[qgSdrZvWfACQ129g.htm](pathfinder-bestiary-2-items/qgSdrZvWfACQ129g.htm)|True Seeing (Constant)|
|[QRGAhXT66Xuc32WM.htm](pathfinder-bestiary-2-items/QRGAhXT66Xuc32WM.htm)|Invisibility (Self only)|
|[qT91ezbxbLydVEdW.htm](pathfinder-bestiary-2-items/qT91ezbxbLydVEdW.htm)|Tongues (Constant)|
|[QTK9hewzSwMZUHHY.htm](pathfinder-bestiary-2-items/QTK9hewzSwMZUHHY.htm)|Invisibility (Self only)|
|[qUW0IwnKj9NAkPZu.htm](pathfinder-bestiary-2-items/qUW0IwnKj9NAkPZu.htm)|Control Weather (Doesn't require secondary casters)|
|[QX2KJ9zKwYbtaPba.htm](pathfinder-bestiary-2-items/QX2KJ9zKwYbtaPba.htm)|Invisibility (At will) (Self only)|
|[qxGCsUCI43v3qoRC.htm](pathfinder-bestiary-2-items/qxGCsUCI43v3qoRC.htm)|Primal Phenomenon (Once per year)|
|[qZDyd8KGBqXEYFHU.htm](pathfinder-bestiary-2-items/qZDyd8KGBqXEYFHU.htm)|Lesser Elixir of Life|
|[qZpXsMwmncbpw5Dw.htm](pathfinder-bestiary-2-items/qZpXsMwmncbpw5Dw.htm)|Invisibility (At will)|
|[R1yZI6IRZc1d6a6H.htm](pathfinder-bestiary-2-items/R1yZI6IRZc1d6a6H.htm)|Invisibility (At will) (Self only)|
|[r9Vti0pP2In1fPgO.htm](pathfinder-bestiary-2-items/r9Vti0pP2In1fPgO.htm)|Tongues (Constant)|
|[rChIZqOgM2sc4XDf.htm](pathfinder-bestiary-2-items/rChIZqOgM2sc4XDf.htm)|Dimensional Anchor (At will)|
|[rcWb1X7tL04A2SNN.htm](pathfinder-bestiary-2-items/rcWb1X7tL04A2SNN.htm)|Darkness (At Will)|
|[rEHDvZqglITeLKYB.htm](pathfinder-bestiary-2-items/rEHDvZqglITeLKYB.htm)|Pass Without Trace (Constant)|
|[rijZTMFR5Ee6MsQm.htm](pathfinder-bestiary-2-items/rijZTMFR5Ee6MsQm.htm)|Plane Shift (To Ethereal Plane or Material Plane only) (Self only)|
|[rJ7AdRxufg0jAASh.htm](pathfinder-bestiary-2-items/rJ7AdRxufg0jAASh.htm)|Gust of Wind (At Will)|
|[RM8PNh1GNliHv6hz.htm](pathfinder-bestiary-2-items/RM8PNh1GNliHv6hz.htm)|Tongues (Constant)|
|[RmX8OdrO08JM0AmG.htm](pathfinder-bestiary-2-items/RmX8OdrO08JM0AmG.htm)|Wall of Fire (At Will)|
|[RojqXvZ5IxWWuBv7.htm](pathfinder-bestiary-2-items/RojqXvZ5IxWWuBv7.htm)|Dimension Door (At Will)|
|[RTunxYN9r4ztOeQC.htm](pathfinder-bestiary-2-items/RTunxYN9r4ztOeQC.htm)|Plane Shift (To Material Plane or Shadow Plane only)|
|[sCQvzLM4bDnpiOMd.htm](pathfinder-bestiary-2-items/sCQvzLM4bDnpiOMd.htm)|True Seeing (Constant)|
|[SFLxOb7tFxjJz3mj.htm](pathfinder-bestiary-2-items/SFLxOb7tFxjJz3mj.htm)|Mind Reading (At will)|
|[sgAI2bN5p9QHXare.htm](pathfinder-bestiary-2-items/sgAI2bN5p9QHXare.htm)|Invisibility (At will) (Self only)|
|[sinEXpm7xMjuylIj.htm](pathfinder-bestiary-2-items/sinEXpm7xMjuylIj.htm)|Detect Magic (Constant)|
|[SOdblsm3svF3C9dl.htm](pathfinder-bestiary-2-items/SOdblsm3svF3C9dl.htm)|Dimension Door (At Will)|
|[SwC3pb24zjBolDvy.htm](pathfinder-bestiary-2-items/SwC3pb24zjBolDvy.htm)|Gust of Wind (At Will)|
|[SZFg9OyW2ROGepvG.htm](pathfinder-bestiary-2-items/SZFg9OyW2ROGepvG.htm)|Dimension Door (At will)|
|[T1jTn0jRuZqV93CM.htm](pathfinder-bestiary-2-items/T1jTn0jRuZqV93CM.htm)|Baleful Polymorph (At will)|
|[T4ROoWQsVb4LvmqB.htm](pathfinder-bestiary-2-items/T4ROoWQsVb4LvmqB.htm)|Mind Probe (At will)|
|[TanTXAIbWmvfNYpc.htm](pathfinder-bestiary-2-items/TanTXAIbWmvfNYpc.htm)|Tongues (Constant)|
|[TgdLSsUbxoCtvEgV.htm](pathfinder-bestiary-2-items/TgdLSsUbxoCtvEgV.htm)|+1 Scythe|+1|
|[tLyD1GZ9Oz7JtF5e.htm](pathfinder-bestiary-2-items/tLyD1GZ9Oz7JtF5e.htm)|Dimension Door (Only when in bright light, and only to an area in bright light)|
|[tN60VOoNQEIM0XvA.htm](pathfinder-bestiary-2-items/tN60VOoNQEIM0XvA.htm)|Longspear|+1,striking|
|[tOHJkoP70gzyQa0q.htm](pathfinder-bestiary-2-items/tOHJkoP70gzyQa0q.htm)|Bastard Sword|+1,striking|
|[toXHeAIIzSZkh0pV.htm](pathfinder-bestiary-2-items/toXHeAIIzSZkh0pV.htm)|Detect Alignment (At will) (Good only)|
|[TSzAkKreLHo2kCCD.htm](pathfinder-bestiary-2-items/TSzAkKreLHo2kCCD.htm)|Virtuoso Trumpet|
|[TYl58bz88u92DVdX.htm](pathfinder-bestiary-2-items/TYl58bz88u92DVdX.htm)|Longspear|+1|
|[TzbvlBarJ1dDLVDB.htm](pathfinder-bestiary-2-items/TzbvlBarJ1dDLVDB.htm)|+1 Striking Staff|+1,striking|
|[u3M9UJPCkvUJAvTB.htm](pathfinder-bestiary-2-items/u3M9UJPCkvUJAvTB.htm)|Dispel Magic (At will)|
|[uBJYf08dJnWbAhyX.htm](pathfinder-bestiary-2-items/uBJYf08dJnWbAhyX.htm)|True Seeing (Constant)|
|[UEPKPYWJrNSP0Jqk.htm](pathfinder-bestiary-2-items/UEPKPYWJrNSP0Jqk.htm)|Shell Armor (Hide)|
|[Um9w32ZfDOZ1WfA3.htm](pathfinder-bestiary-2-items/Um9w32ZfDOZ1WfA3.htm)|Tree Stride (At will)|
|[Ump4s3SQmPTp8c0K.htm](pathfinder-bestiary-2-items/Ump4s3SQmPTp8c0K.htm)|Wall of Fire (At Will)|
|[UpvwE0gzoIhEpwom.htm](pathfinder-bestiary-2-items/UpvwE0gzoIhEpwom.htm)|+2 Greater Striking Glaive|+2,greaterStriking|
|[V2Qq9JcpjIeBP5L4.htm](pathfinder-bestiary-2-items/V2Qq9JcpjIeBP5L4.htm)|Composite Longbow|+1|
|[v3MNBWrVRN9SQv9V.htm](pathfinder-bestiary-2-items/v3MNBWrVRN9SQv9V.htm)|Water Walk (Constant)|
|[v4FHOiOqxoo52YTS.htm](pathfinder-bestiary-2-items/v4FHOiOqxoo52YTS.htm)|True Seeing (Constant)|
|[VFViUEQww0BRVE03.htm](pathfinder-bestiary-2-items/VFViUEQww0BRVE03.htm)|Shape Stone (At will)|
|[VgQyT5O8X59xTXWs.htm](pathfinder-bestiary-2-items/VgQyT5O8X59xTXWs.htm)|Summon Elemental (Water Elementals only)|
|[VjrbFMGP6O3PLlyp.htm](pathfinder-bestiary-2-items/VjrbFMGP6O3PLlyp.htm)|Tongues (Constant)|
|[VpnCsS6gaS1m1Imz.htm](pathfinder-bestiary-2-items/VpnCsS6gaS1m1Imz.htm)|Tongues (Constant)|
|[vQBJ6UOCVxfLLwKL.htm](pathfinder-bestiary-2-items/vQBJ6UOCVxfLLwKL.htm)|Control Water (At will)|
|[Vz2xdhsoi4Vg6Gej.htm](pathfinder-bestiary-2-items/Vz2xdhsoi4Vg6Gej.htm)|True Seeing (Constant)|
|[wbilAnBHlo9UFygH.htm](pathfinder-bestiary-2-items/wbilAnBHlo9UFygH.htm)|True Seeing (Constant)|
|[wFwYkevdZGRjwibC.htm](pathfinder-bestiary-2-items/wFwYkevdZGRjwibC.htm)|Charm (plant creatures only)|
|[Wj4PwJFZfcVXdLml.htm](pathfinder-bestiary-2-items/Wj4PwJFZfcVXdLml.htm)|Mind Reading (At will)|
|[wJAzn02M6Mv8HeZj.htm](pathfinder-bestiary-2-items/wJAzn02M6Mv8HeZj.htm)|Levitate (At will) (Self only)|
|[wJkawABLUtrnlz3Y.htm](pathfinder-bestiary-2-items/wJkawABLUtrnlz3Y.htm)|+1 Ranseur|+1|
|[X8HPyH2dWuDAiYT3.htm](pathfinder-bestiary-2-items/X8HPyH2dWuDAiYT3.htm)|Speak with Plants (Constant)|
|[x9WaNwW8LODf3mt3.htm](pathfinder-bestiary-2-items/x9WaNwW8LODf3mt3.htm)|Tongues (Constant)|
|[xCOiURbOFmMct0Ad.htm](pathfinder-bestiary-2-items/xCOiURbOFmMct0Ad.htm)|Detect Alignment (Constant) (All Alignments Simultaneously)|
|[xm7v6OJXM7xTHseI.htm](pathfinder-bestiary-2-items/xm7v6OJXM7xTHseI.htm)|Invisibility (At will) (Self only)|
|[xrKfhlv0heNZjJQl.htm](pathfinder-bestiary-2-items/xrKfhlv0heNZjJQl.htm)|+1 Morningstar|+1|
|[XSu8EpdK6CVQXvZe.htm](pathfinder-bestiary-2-items/XSu8EpdK6CVQXvZe.htm)|True Seeing (Constant)|
|[xuzZonbgefgsArAz.htm](pathfinder-bestiary-2-items/xuzZonbgefgsArAz.htm)|Summon Entity (Will-o'-Wisp Only)|
|[xWKBg0GE28Tpu1bC.htm](pathfinder-bestiary-2-items/xWKBg0GE28Tpu1bC.htm)|Plane Shift (At will)|
|[y12tfuxEBFbR9qgU.htm](pathfinder-bestiary-2-items/y12tfuxEBFbR9qgU.htm)|Tongues (Constant)|
|[y1vZf9cXWJAE7Ig5.htm](pathfinder-bestiary-2-items/y1vZf9cXWJAE7Ig5.htm)|Ventriloquism (At will)|
|[y7xDGYy2BAsv28Gu.htm](pathfinder-bestiary-2-items/y7xDGYy2BAsv28Gu.htm)|Freedom of Movement (Constant)|
|[y8TePj5x2rTRtyMK.htm](pathfinder-bestiary-2-items/y8TePj5x2rTRtyMK.htm)|Dimension Door (At will)|
|[YBf7cE1gXS2MxYx2.htm](pathfinder-bestiary-2-items/YBf7cE1gXS2MxYx2.htm)|Greatsword|+2,greaterStriking|
|[ybPZgyruroPBZ9gc.htm](pathfinder-bestiary-2-items/ybPZgyruroPBZ9gc.htm)|Tongues (Constant)|
|[ybZoLDhRr9Dy4RFI.htm](pathfinder-bestiary-2-items/ybZoLDhRr9Dy4RFI.htm)|Unseen Servant (At will)|
|[yfqMmmrCpEGsye6G.htm](pathfinder-bestiary-2-items/yfqMmmrCpEGsye6G.htm)|Dimension Door (At will)|
|[ygKRMmKOt3k4RrRs.htm](pathfinder-bestiary-2-items/ygKRMmKOt3k4RrRs.htm)|Spellwrack (At will)|
|[yhlcSZF88RwErXcb.htm](pathfinder-bestiary-2-items/yhlcSZF88RwErXcb.htm)|Divine Aura (Chaotic only)|
|[ysmXZKOimV2eOJdk.htm](pathfinder-bestiary-2-items/ysmXZKOimV2eOJdk.htm)|See Invisibility (Constant)|
|[YtRTdI3UAqH6vZVL.htm](pathfinder-bestiary-2-items/YtRTdI3UAqH6vZVL.htm)|Elemental Form (Water only)|
|[YVBJxmLTHes9asum.htm](pathfinder-bestiary-2-items/YVBJxmLTHes9asum.htm)|Invisibility (At will) (Self only)|
|[z0tSvLqh0IK6Y4TT.htm](pathfinder-bestiary-2-items/z0tSvLqh0IK6Y4TT.htm)|Floating Disk (At will)|
|[Z9Qm5DGAbFw04kMI.htm](pathfinder-bestiary-2-items/Z9Qm5DGAbFw04kMI.htm)|Dimension Door (At will)|
|[ZbNsXG0791f7GcKW.htm](pathfinder-bestiary-2-items/ZbNsXG0791f7GcKW.htm)|+2 Greater Striking Longspear|+2,greaterStriking|
|[zD1p5wo8W7il4Xw4.htm](pathfinder-bestiary-2-items/zD1p5wo8W7il4Xw4.htm)|Tongues (Constant)|
|[zkvNDt3HOWSmCOid.htm](pathfinder-bestiary-2-items/zkvNDt3HOWSmCOid.htm)|+1 Striking Gaff|
|[zmdWFf8TYR11NSvB.htm](pathfinder-bestiary-2-items/zmdWFf8TYR11NSvB.htm)|Illusory Object (At will)|
|[zmGi1QNStSaYC6ai.htm](pathfinder-bestiary-2-items/zmGi1QNStSaYC6ai.htm)|True Seeing (Constant)|
|[zpCGLXqUH07afHBV.htm](pathfinder-bestiary-2-items/zpCGLXqUH07afHBV.htm)|Wall of Wind (At Will)|
|[zpkYVcLlyvOVqOdx.htm](pathfinder-bestiary-2-items/zpkYVcLlyvOVqOdx.htm)|Telekinetic Haul (At will)|
|[zSdkC5bHVECvhK79.htm](pathfinder-bestiary-2-items/zSdkC5bHVECvhK79.htm)|Dispel Magic (At will)|
|[zSJNdELBxJk00I4X.htm](pathfinder-bestiary-2-items/zSJNdELBxJk00I4X.htm)|Darkness (At Will)|
|[ZwTQYhVwvbqvzaHX.htm](pathfinder-bestiary-2-items/ZwTQYhVwvbqvzaHX.htm)|Air Walk (Constant)|
|[ZXR3Dqh3vpkb4MJB.htm](pathfinder-bestiary-2-items/ZXR3Dqh3vpkb4MJB.htm)|Enthrall (At will)|
|[ZYBGyu05L6Ve0V1r.htm](pathfinder-bestiary-2-items/ZYBGyu05L6Ve0V1r.htm)|Shadow Siphon (At will)|

## Lista de traducciones automáticas que deben corregirse/retraducirse

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[00kCy3dtoRU8EnP9.htm](pathfinder-bestiary-2-items/00kCy3dtoRU8EnP9.htm)|Longspear|auto-trad|
|[00zSyT0a2tdDQnmZ.htm](pathfinder-bestiary-2-items/00zSyT0a2tdDQnmZ.htm)|Low-Light Vision|auto-trad|
|[03MMQCPuK2UJR1Z6.htm](pathfinder-bestiary-2-items/03MMQCPuK2UJR1Z6.htm)|Graft Flesh|auto-trad|
|[05N8sEltz4b9htCX.htm](pathfinder-bestiary-2-items/05N8sEltz4b9htCX.htm)|Dagger|auto-trad|
|[072UUA8WXrWV6tPc.htm](pathfinder-bestiary-2-items/072UUA8WXrWV6tPc.htm)|Horn|auto-trad|
|[090u49BTBz9TdQi7.htm](pathfinder-bestiary-2-items/090u49BTBz9TdQi7.htm)|Regeneration 15 (deactivated by cold iron)|auto-trad|
|[0a9bxpI79rMpqyXh.htm](pathfinder-bestiary-2-items/0a9bxpI79rMpqyXh.htm)|Greater Darkvision|auto-trad|
|[0BVpj3wkSrFlhgdK.htm](pathfinder-bestiary-2-items/0BVpj3wkSrFlhgdK.htm)|Agile Swimmer|auto-trad|
|[0D7DkzCzV4iv9Q0r.htm](pathfinder-bestiary-2-items/0D7DkzCzV4iv9Q0r.htm)|Grab|auto-trad|
|[0DDWtKSDSWdXnkgj.htm](pathfinder-bestiary-2-items/0DDWtKSDSWdXnkgj.htm)|Bloodfire Fever|auto-trad|
|[0dNqhk5KI3nAaTlu.htm](pathfinder-bestiary-2-items/0dNqhk5KI3nAaTlu.htm)|Demon Hunter|auto-trad|
|[0DOPtcRBzrEKku1I.htm](pathfinder-bestiary-2-items/0DOPtcRBzrEKku1I.htm)|Cloud Walk|auto-trad|
|[0DR36G32rpPd9PIN.htm](pathfinder-bestiary-2-items/0DR36G32rpPd9PIN.htm)|Whispered Despair|auto-trad|
|[0dTEwPV9TXqDzFno.htm](pathfinder-bestiary-2-items/0dTEwPV9TXqDzFno.htm)|Soulsense|auto-trad|
|[0eRwevoxA5zdvqb9.htm](pathfinder-bestiary-2-items/0eRwevoxA5zdvqb9.htm)|Breath Weapon|auto-trad|
|[0F96cEU48FezVgKB.htm](pathfinder-bestiary-2-items/0F96cEU48FezVgKB.htm)|Bite|auto-trad|
|[0FwbXedAmkMCm8OR.htm](pathfinder-bestiary-2-items/0FwbXedAmkMCm8OR.htm)|Tremorsense 30 feet|auto-trad|
|[0G6ZKuqCpYAAIPki.htm](pathfinder-bestiary-2-items/0G6ZKuqCpYAAIPki.htm)|Constant Spells|auto-trad|
|[0gNZbOZYyi010Pow.htm](pathfinder-bestiary-2-items/0gNZbOZYyi010Pow.htm)|Curse of Stolen Breath|auto-trad|
|[0hkOF2WHkLhHAEjQ.htm](pathfinder-bestiary-2-items/0hkOF2WHkLhHAEjQ.htm)|Descend on a Web|auto-trad|
|[0IEitR672ZSYf8Jn.htm](pathfinder-bestiary-2-items/0IEitR672ZSYf8Jn.htm)|Primal Prepared Spells|auto-trad|
|[0IfzoP16zRxLRIJ3.htm](pathfinder-bestiary-2-items/0IfzoP16zRxLRIJ3.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[0IwQxSLxEqwPcB3q.htm](pathfinder-bestiary-2-items/0IwQxSLxEqwPcB3q.htm)|Steam Vision|auto-trad|
|[0jpjkgun2zt97YLL.htm](pathfinder-bestiary-2-items/0jpjkgun2zt97YLL.htm)|Deceptive Reposition|auto-trad|
|[0llDJD2BXJrr95lm.htm](pathfinder-bestiary-2-items/0llDJD2BXJrr95lm.htm)|Smoke Form|auto-trad|
|[0MEzU0e7MJtwV7WA.htm](pathfinder-bestiary-2-items/0MEzU0e7MJtwV7WA.htm)|Truespeech|auto-trad|
|[0mzc60K0kDFbeLL4.htm](pathfinder-bestiary-2-items/0mzc60K0kDFbeLL4.htm)|Fast Swallow|auto-trad|
|[0NKzpP896UuTepW0.htm](pathfinder-bestiary-2-items/0NKzpP896UuTepW0.htm)|Jaws|auto-trad|
|[0Nlywj3cWzzzNsFd.htm](pathfinder-bestiary-2-items/0Nlywj3cWzzzNsFd.htm)|Bastard Sword|auto-trad|
|[0Q0S3Hd1xOjt3Ked.htm](pathfinder-bestiary-2-items/0Q0S3Hd1xOjt3Ked.htm)|Low-Light Vision|auto-trad|
|[0q3s792vZI0wANuc.htm](pathfinder-bestiary-2-items/0q3s792vZI0wANuc.htm)|Tentacle|auto-trad|
|[0QCPtkVtKzuuVVaK.htm](pathfinder-bestiary-2-items/0QCPtkVtKzuuVVaK.htm)|Bladed Limb|auto-trad|
|[0s0zoTqBFyl1ZTjC.htm](pathfinder-bestiary-2-items/0s0zoTqBFyl1ZTjC.htm)|Frightful Presence|auto-trad|
|[0s7Elyqvdpp0ck2s.htm](pathfinder-bestiary-2-items/0s7Elyqvdpp0ck2s.htm)|Darkvision|auto-trad|
|[0SmjbTrLS51rwGcc.htm](pathfinder-bestiary-2-items/0SmjbTrLS51rwGcc.htm)|Draconic Frenzy|auto-trad|
|[0TCobRYKQCosWffG.htm](pathfinder-bestiary-2-items/0TCobRYKQCosWffG.htm)|Claw|auto-trad|
|[0ToPGrofmnFDeqOe.htm](pathfinder-bestiary-2-items/0ToPGrofmnFDeqOe.htm)|Slithering Attack|auto-trad|
|[0UKsJtM3fEth21iR.htm](pathfinder-bestiary-2-items/0UKsJtM3fEth21iR.htm)|Lifesense|auto-trad|
|[0USgUg4kKiE4rcKV.htm](pathfinder-bestiary-2-items/0USgUg4kKiE4rcKV.htm)|Club|auto-trad|
|[0uU20iNCnRv4spmg.htm](pathfinder-bestiary-2-items/0uU20iNCnRv4spmg.htm)|Darkvision|auto-trad|
|[0uYLHfv1lvXE4qG4.htm](pathfinder-bestiary-2-items/0uYLHfv1lvXE4qG4.htm)|Constant Spells|auto-trad|
|[0v7H9KCVbUUdkdU7.htm](pathfinder-bestiary-2-items/0v7H9KCVbUUdkdU7.htm)|Cloud Form|auto-trad|
|[0vByzvft4IIwjZh2.htm](pathfinder-bestiary-2-items/0vByzvft4IIwjZh2.htm)|At-Will Spells|auto-trad|
|[0VNX7MbrCqRSKYkE.htm](pathfinder-bestiary-2-items/0VNX7MbrCqRSKYkE.htm)|Shadow Evade|auto-trad|
|[0vrX1z0w3GFCktsF.htm](pathfinder-bestiary-2-items/0vrX1z0w3GFCktsF.htm)|Darkvision|auto-trad|
|[0wGgpxEStAnmfi0k.htm](pathfinder-bestiary-2-items/0wGgpxEStAnmfi0k.htm)|Foot|auto-trad|
|[0xGnS3VP3WvvpnSI.htm](pathfinder-bestiary-2-items/0xGnS3VP3WvvpnSI.htm)|Darkvision|auto-trad|
|[0XjG2HkUAja2e7pE.htm](pathfinder-bestiary-2-items/0XjG2HkUAja2e7pE.htm)|Jaws|auto-trad|
|[0xLF0LRnkscCWi8p.htm](pathfinder-bestiary-2-items/0xLF0LRnkscCWi8p.htm)|Primal Spontaneous Spells|auto-trad|
|[0Y47YuHl29LzryME.htm](pathfinder-bestiary-2-items/0Y47YuHl29LzryME.htm)|Trunk|auto-trad|
|[0zl5InxQA7HuiemZ.htm](pathfinder-bestiary-2-items/0zl5InxQA7HuiemZ.htm)|Occult Spontaneous Spells|auto-trad|
|[109W7aPnwhNHkrHE.htm](pathfinder-bestiary-2-items/109W7aPnwhNHkrHE.htm)|Claw|auto-trad|
|[12e8csvZLJdg2OS8.htm](pathfinder-bestiary-2-items/12e8csvZLJdg2OS8.htm)|Darkvision|auto-trad|
|[19Ayra6rX2M49iPG.htm](pathfinder-bestiary-2-items/19Ayra6rX2M49iPG.htm)|Feed on Blood|auto-trad|
|[19bztZWgNzgUNmTs.htm](pathfinder-bestiary-2-items/19bztZWgNzgUNmTs.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[1A17VJOGWG0BFypA.htm](pathfinder-bestiary-2-items/1A17VJOGWG0BFypA.htm)|Club|auto-trad|
|[1abnSJGdHEvG6Lyl.htm](pathfinder-bestiary-2-items/1abnSJGdHEvG6Lyl.htm)|Bramble Jump|auto-trad|
|[1BBbgSS3coktFqQA.htm](pathfinder-bestiary-2-items/1BBbgSS3coktFqQA.htm)|Constant Spells|auto-trad|
|[1BD5jRohvhbGdDwq.htm](pathfinder-bestiary-2-items/1BD5jRohvhbGdDwq.htm)|Fangs|auto-trad|
|[1BrHXN7JmOnmyeOk.htm](pathfinder-bestiary-2-items/1BrHXN7JmOnmyeOk.htm)|Grab|auto-trad|
|[1ChXT4DL7N5JhHEq.htm](pathfinder-bestiary-2-items/1ChXT4DL7N5JhHEq.htm)|Tail|auto-trad|
|[1deHNHPL9laFtZgR.htm](pathfinder-bestiary-2-items/1deHNHPL9laFtZgR.htm)|Tremorsense|auto-trad|
|[1DfgmZUiWL8m4tiP.htm](pathfinder-bestiary-2-items/1DfgmZUiWL8m4tiP.htm)|Low-Light Vision|auto-trad|
|[1eb07HNvWyOOELI3.htm](pathfinder-bestiary-2-items/1eb07HNvWyOOELI3.htm)|Darkvision|auto-trad|
|[1ENGsx3BESdgdHnk.htm](pathfinder-bestiary-2-items/1ENGsx3BESdgdHnk.htm)|Lightning Drinker|auto-trad|
|[1FR8UtVfDAC3RoWh.htm](pathfinder-bestiary-2-items/1FR8UtVfDAC3RoWh.htm)|Mortasheen|auto-trad|
|[1fTRsJAGDuFgPeXF.htm](pathfinder-bestiary-2-items/1fTRsJAGDuFgPeXF.htm)|Earth Glide|auto-trad|
|[1gwUpyZY3w0LloLq.htm](pathfinder-bestiary-2-items/1gwUpyZY3w0LloLq.htm)|Devil's Howl|auto-trad|
|[1IvCoMHs0qGTlw8J.htm](pathfinder-bestiary-2-items/1IvCoMHs0qGTlw8J.htm)|Ripping Gaze|auto-trad|
|[1jqzGUINJJfVCKfu.htm](pathfinder-bestiary-2-items/1jqzGUINJJfVCKfu.htm)|Smoke Vision|auto-trad|
|[1Kmvtn64DZibqUls.htm](pathfinder-bestiary-2-items/1Kmvtn64DZibqUls.htm)|Elemental Assault|auto-trad|
|[1LmOy2NMwi2RqSKT.htm](pathfinder-bestiary-2-items/1LmOy2NMwi2RqSKT.htm)|Golem Antimagic|auto-trad|
|[1lSOq5grKxoZhTWw.htm](pathfinder-bestiary-2-items/1lSOq5grKxoZhTWw.htm)|Constant Spells|auto-trad|
|[1lYZ7v9sRWUarCls.htm](pathfinder-bestiary-2-items/1lYZ7v9sRWUarCls.htm)|Wing Deflection|auto-trad|
|[1M2I2905EVmXay9H.htm](pathfinder-bestiary-2-items/1M2I2905EVmXay9H.htm)|Draconic Frenzy|auto-trad|
|[1MWjpvCpwBy0BFwq.htm](pathfinder-bestiary-2-items/1MWjpvCpwBy0BFwq.htm)|Emperor Cobra Venom|auto-trad|
|[1qBTVbPZxVREvpnT.htm](pathfinder-bestiary-2-items/1qBTVbPZxVREvpnT.htm)|Telepathy (Touch only)|auto-trad|
|[1RA0WbiyBjX7Iegz.htm](pathfinder-bestiary-2-items/1RA0WbiyBjX7Iegz.htm)|Darkvision|auto-trad|
|[1RcwWRNvn63MNFhB.htm](pathfinder-bestiary-2-items/1RcwWRNvn63MNFhB.htm)|Darkvision|auto-trad|
|[1Re8gJ0YTH6TxfLa.htm](pathfinder-bestiary-2-items/1Re8gJ0YTH6TxfLa.htm)|Darkvision|auto-trad|
|[1RpmI2FY6QNDsOVh.htm](pathfinder-bestiary-2-items/1RpmI2FY6QNDsOVh.htm)|Scent|auto-trad|
|[1UzzAnKlRhYDkzDO.htm](pathfinder-bestiary-2-items/1UzzAnKlRhYDkzDO.htm)|Telepathy|auto-trad|
|[1vC7HEUnMXCc5wme.htm](pathfinder-bestiary-2-items/1vC7HEUnMXCc5wme.htm)|Positive Energy Affinity|auto-trad|
|[1w9sOvRdSaIEpLNe.htm](pathfinder-bestiary-2-items/1w9sOvRdSaIEpLNe.htm)|Tail|auto-trad|
|[1ynGuwCvWvX0EOiE.htm](pathfinder-bestiary-2-items/1ynGuwCvWvX0EOiE.htm)|Frightful Presence|auto-trad|
|[1Z3FZ39brRJH2cIs.htm](pathfinder-bestiary-2-items/1Z3FZ39brRJH2cIs.htm)|Recall Weapon|auto-trad|
|[1zSROTRkSluSvmzR.htm](pathfinder-bestiary-2-items/1zSROTRkSluSvmzR.htm)|Swallow Whole|auto-trad|
|[1zvM86hRPPnGLOEW.htm](pathfinder-bestiary-2-items/1zvM86hRPPnGLOEW.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[20d4bCP4x4uHaNkd.htm](pathfinder-bestiary-2-items/20d4bCP4x4uHaNkd.htm)|Claw|auto-trad|
|[223kLr85XBsNkrd0.htm](pathfinder-bestiary-2-items/223kLr85XBsNkrd0.htm)|Primal Prepared Spells|auto-trad|
|[27ggnHd24eNbhg9e.htm](pathfinder-bestiary-2-items/27ggnHd24eNbhg9e.htm)|Darkvision|auto-trad|
|[28TZFvjgwHVKS489.htm](pathfinder-bestiary-2-items/28TZFvjgwHVKS489.htm)|Filament|auto-trad|
|[28xyMG8bDPJrcRsP.htm](pathfinder-bestiary-2-items/28xyMG8bDPJrcRsP.htm)|Death-Stealing Gaze|auto-trad|
|[2a2eONlkTBanM6W0.htm](pathfinder-bestiary-2-items/2a2eONlkTBanM6W0.htm)|Drain Life|auto-trad|
|[2a4l18G9fqQ03iFA.htm](pathfinder-bestiary-2-items/2a4l18G9fqQ03iFA.htm)|Tail|auto-trad|
|[2ABnFaydFGtobnZC.htm](pathfinder-bestiary-2-items/2ABnFaydFGtobnZC.htm)|At-Will Spells|auto-trad|
|[2aU81mgG4GTqpajQ.htm](pathfinder-bestiary-2-items/2aU81mgG4GTqpajQ.htm)|Foot|auto-trad|
|[2avn7E9oKYvrvNGl.htm](pathfinder-bestiary-2-items/2avn7E9oKYvrvNGl.htm)|Greater Darkvision|auto-trad|
|[2B1o26m73tHgV6UJ.htm](pathfinder-bestiary-2-items/2B1o26m73tHgV6UJ.htm)|Low-Light Vision|auto-trad|
|[2bevQZIcuccKMxmq.htm](pathfinder-bestiary-2-items/2bevQZIcuccKMxmq.htm)|Jaws|auto-trad|
|[2BHwzsnFNAsPoULp.htm](pathfinder-bestiary-2-items/2BHwzsnFNAsPoULp.htm)|Fangs|auto-trad|
|[2bthXOLfHVbIarRo.htm](pathfinder-bestiary-2-items/2bthXOLfHVbIarRo.htm)|At-Will Spells|auto-trad|
|[2D3ZN6sbGBpo3oKC.htm](pathfinder-bestiary-2-items/2D3ZN6sbGBpo3oKC.htm)|Scent|auto-trad|
|[2dHPDZu3BYN1GauB.htm](pathfinder-bestiary-2-items/2dHPDZu3BYN1GauB.htm)|No Breath|auto-trad|
|[2doPyPIBLoDzjzk9.htm](pathfinder-bestiary-2-items/2doPyPIBLoDzjzk9.htm)|Paralysis|auto-trad|
|[2FR8eBARbXfskcJR.htm](pathfinder-bestiary-2-items/2FR8eBARbXfskcJR.htm)|Low-Light Vision|auto-trad|
|[2GNdGYBXQOD8VlC7.htm](pathfinder-bestiary-2-items/2GNdGYBXQOD8VlC7.htm)|Darkvision|auto-trad|
|[2hG90mAiBnKHWoxc.htm](pathfinder-bestiary-2-items/2hG90mAiBnKHWoxc.htm)|Fast Healing 10|auto-trad|
|[2hIYkkGgRCsVdWhY.htm](pathfinder-bestiary-2-items/2hIYkkGgRCsVdWhY.htm)|Constant Spells|auto-trad|
|[2HOhVTSCzGdbcfjr.htm](pathfinder-bestiary-2-items/2HOhVTSCzGdbcfjr.htm)|Jaws|auto-trad|
|[2hyOsL9cmbYO1koQ.htm](pathfinder-bestiary-2-items/2hyOsL9cmbYO1koQ.htm)|Frightful Presence|auto-trad|
|[2IZWaM2O4amLWbye.htm](pathfinder-bestiary-2-items/2IZWaM2O4amLWbye.htm)|Draconic Frenzy|auto-trad|
|[2J8nRu7ryhdxrn6Q.htm](pathfinder-bestiary-2-items/2J8nRu7ryhdxrn6Q.htm)|Jaws|auto-trad|
|[2JNLdz4nnbNkOkbO.htm](pathfinder-bestiary-2-items/2JNLdz4nnbNkOkbO.htm)|Claw|auto-trad|
|[2KNgUCdMbbr8kaE7.htm](pathfinder-bestiary-2-items/2KNgUCdMbbr8kaE7.htm)|Barbed Tongue|auto-trad|
|[2KSiu60tktdQLiFE.htm](pathfinder-bestiary-2-items/2KSiu60tktdQLiFE.htm)|Ultimate Sacrifice|auto-trad|
|[2M8fHq1jWvJybtt9.htm](pathfinder-bestiary-2-items/2M8fHq1jWvJybtt9.htm)|Frightful Presence|auto-trad|
|[2MrVVGinZvjfZN22.htm](pathfinder-bestiary-2-items/2MrVVGinZvjfZN22.htm)|Tremorsense|auto-trad|
|[2NXWhRmjG27cSo87.htm](pathfinder-bestiary-2-items/2NXWhRmjG27cSo87.htm)|Starknife|auto-trad|
|[2O7YGmWG9wjbFcRM.htm](pathfinder-bestiary-2-items/2O7YGmWG9wjbFcRM.htm)|Scent|auto-trad|
|[2oF9dlmEoHZ6o4U9.htm](pathfinder-bestiary-2-items/2oF9dlmEoHZ6o4U9.htm)|Bastard Sword|auto-trad|
|[2OIZKsg9vZrNrBBw.htm](pathfinder-bestiary-2-items/2OIZKsg9vZrNrBBw.htm)|Basidirond Spores|auto-trad|
|[2ooN4XI8c3aPZmLa.htm](pathfinder-bestiary-2-items/2ooN4XI8c3aPZmLa.htm)|Grab|auto-trad|
|[2oTY3Ohbh0C59vOv.htm](pathfinder-bestiary-2-items/2oTY3Ohbh0C59vOv.htm)|Curse of Drowning|auto-trad|
|[2PLVvoyanKPQQdYK.htm](pathfinder-bestiary-2-items/2PLVvoyanKPQQdYK.htm)|Mist Cloud|auto-trad|
|[2rJdPiiHOrVnLUyK.htm](pathfinder-bestiary-2-items/2rJdPiiHOrVnLUyK.htm)|Fish Hook|auto-trad|
|[2SEpmpzrfI3iTOKV.htm](pathfinder-bestiary-2-items/2SEpmpzrfI3iTOKV.htm)|Jaws|auto-trad|
|[2ShdE5g6oMqrL464.htm](pathfinder-bestiary-2-items/2ShdE5g6oMqrL464.htm)|Scent|auto-trad|
|[2SKe9OcSUJ5AXTPf.htm](pathfinder-bestiary-2-items/2SKe9OcSUJ5AXTPf.htm)|Archon's Door|auto-trad|
|[2svyNegNoV205Fbp.htm](pathfinder-bestiary-2-items/2svyNegNoV205Fbp.htm)|Icy Demise|auto-trad|
|[2szfgkG5m4pZQ6z8.htm](pathfinder-bestiary-2-items/2szfgkG5m4pZQ6z8.htm)|At-Will Spells|auto-trad|
|[2tplJgSAMF7CmPNN.htm](pathfinder-bestiary-2-items/2tplJgSAMF7CmPNN.htm)|Attack of Opportunity|auto-trad|
|[2uyZOjTUnURaOWQp.htm](pathfinder-bestiary-2-items/2uyZOjTUnURaOWQp.htm)|Tail|auto-trad|
|[2VxyCqJZcASXlsyq.htm](pathfinder-bestiary-2-items/2VxyCqJZcASXlsyq.htm)|Calcification|auto-trad|
|[2waLtCSTbFL4Q6ta.htm](pathfinder-bestiary-2-items/2waLtCSTbFL4Q6ta.htm)|Regeneration 15 (Deactivated by Acid or Fire)|auto-trad|
|[2wgeHmf6T4ScZpAk.htm](pathfinder-bestiary-2-items/2wgeHmf6T4ScZpAk.htm)|Gray Ooze Acid|auto-trad|
|[2wJro1SjxNwEf7TQ.htm](pathfinder-bestiary-2-items/2wJro1SjxNwEf7TQ.htm)|Hatchet|auto-trad|
|[2wZs6OKCKOsMAU1a.htm](pathfinder-bestiary-2-items/2wZs6OKCKOsMAU1a.htm)|Throw Rock|auto-trad|
|[2X26aYIXJXcffHKZ.htm](pathfinder-bestiary-2-items/2X26aYIXJXcffHKZ.htm)|Throw Rock|auto-trad|
|[2xM4lSOsw2nfs2tu.htm](pathfinder-bestiary-2-items/2xM4lSOsw2nfs2tu.htm)|Mauler|auto-trad|
|[2YDmS6mB4lt2kYvF.htm](pathfinder-bestiary-2-items/2YDmS6mB4lt2kYvF.htm)|Jaws|auto-trad|
|[2yuX2a5ufNBYmWHK.htm](pathfinder-bestiary-2-items/2yuX2a5ufNBYmWHK.htm)|Fangs|auto-trad|
|[3141UeEi3hFz0moy.htm](pathfinder-bestiary-2-items/3141UeEi3hFz0moy.htm)|Darkvision|auto-trad|
|[32RMKlAce7FfxW8e.htm](pathfinder-bestiary-2-items/32RMKlAce7FfxW8e.htm)|Darkvision|auto-trad|
|[34NmTbXjw24GQVju.htm](pathfinder-bestiary-2-items/34NmTbXjw24GQVju.htm)|Mind-Rending Sting|auto-trad|
|[37D2wZ5XwqQ5zjsE.htm](pathfinder-bestiary-2-items/37D2wZ5XwqQ5zjsE.htm)|Negative Healing|auto-trad|
|[38cGo2E7vYZ2PNb5.htm](pathfinder-bestiary-2-items/38cGo2E7vYZ2PNb5.htm)|+2 Status Bonus to Saves vs. Fear|auto-trad|
|[38rv8OhglPWCKDoM.htm](pathfinder-bestiary-2-items/38rv8OhglPWCKDoM.htm)|Low-Light Vision|auto-trad|
|[3AK8xWwQNAfn8GZR.htm](pathfinder-bestiary-2-items/3AK8xWwQNAfn8GZR.htm)|Bite|auto-trad|
|[3AqKPdexSndZxd5X.htm](pathfinder-bestiary-2-items/3AqKPdexSndZxd5X.htm)|Rend|auto-trad|
|[3bHndJ6sRVOw7gwC.htm](pathfinder-bestiary-2-items/3bHndJ6sRVOw7gwC.htm)|Improved Grab|auto-trad|
|[3BSxL95mz6QjoWi1.htm](pathfinder-bestiary-2-items/3BSxL95mz6QjoWi1.htm)|Sneak Attack|auto-trad|
|[3c01H24sjgfJHyvI.htm](pathfinder-bestiary-2-items/3c01H24sjgfJHyvI.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[3cCdl7Qgc7kTT6at.htm](pathfinder-bestiary-2-items/3cCdl7Qgc7kTT6at.htm)|Darkvision|auto-trad|
|[3cCzIUJ7cXhefnQ1.htm](pathfinder-bestiary-2-items/3cCzIUJ7cXhefnQ1.htm)|Whirling Death|auto-trad|
|[3CKGWY5tOmbNJznT.htm](pathfinder-bestiary-2-items/3CKGWY5tOmbNJznT.htm)|Fist|auto-trad|
|[3DQQAVyagZHfe2VD.htm](pathfinder-bestiary-2-items/3DQQAVyagZHfe2VD.htm)|Improved Grab|auto-trad|
|[3EzYscL7vFw5pVbn.htm](pathfinder-bestiary-2-items/3EzYscL7vFw5pVbn.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[3f1Y7j76I98KPIWK.htm](pathfinder-bestiary-2-items/3f1Y7j76I98KPIWK.htm)|Ice Missile|auto-trad|
|[3F8tYJDQeeHlPY6p.htm](pathfinder-bestiary-2-items/3F8tYJDQeeHlPY6p.htm)|Darkvision|auto-trad|
|[3gCf4mj9ynEcSXVi.htm](pathfinder-bestiary-2-items/3gCf4mj9ynEcSXVi.htm)|Constrict|auto-trad|
|[3gEkJauWCduRIDhx.htm](pathfinder-bestiary-2-items/3gEkJauWCduRIDhx.htm)|Arcane Innate Spells|auto-trad|
|[3GOGRqIhDzmef7he.htm](pathfinder-bestiary-2-items/3GOGRqIhDzmef7he.htm)|Hallucinogenic Cloud|auto-trad|
|[3IExZ0tOTpB3Neuc.htm](pathfinder-bestiary-2-items/3IExZ0tOTpB3Neuc.htm)|Whirling Frenzy|auto-trad|
|[3IQSdTZPneapXwVf.htm](pathfinder-bestiary-2-items/3IQSdTZPneapXwVf.htm)|Change Shape|auto-trad|
|[3jVmH3McNsX8B0Od.htm](pathfinder-bestiary-2-items/3jVmH3McNsX8B0Od.htm)|Jaws|auto-trad|
|[3KyuJoHTxD577UTr.htm](pathfinder-bestiary-2-items/3KyuJoHTxD577UTr.htm)|Primal Innate Spells|auto-trad|
|[3LE8g6DKV4fZYrQq.htm](pathfinder-bestiary-2-items/3LE8g6DKV4fZYrQq.htm)|Regeneration 20 (deactivated by cold iron)|auto-trad|
|[3MKqU2vGsb11kD4j.htm](pathfinder-bestiary-2-items/3MKqU2vGsb11kD4j.htm)|Negative Healing|auto-trad|
|[3mOfCebY1QT6qHAG.htm](pathfinder-bestiary-2-items/3mOfCebY1QT6qHAG.htm)|Diligent Assault|auto-trad|
|[3PMo2s4e4A2PWtKx.htm](pathfinder-bestiary-2-items/3PMo2s4e4A2PWtKx.htm)|Arcane Innate Spells|auto-trad|
|[3R5GHgg08mxLJg23.htm](pathfinder-bestiary-2-items/3R5GHgg08mxLJg23.htm)|Divine Innate Spells|auto-trad|
|[3ryftdIzkfXriKLV.htm](pathfinder-bestiary-2-items/3ryftdIzkfXriKLV.htm)|Scent|auto-trad|
|[3s66FP7raqgSCnP1.htm](pathfinder-bestiary-2-items/3s66FP7raqgSCnP1.htm)|Mucus Trail|auto-trad|
|[3t3ulXs6r1EL76F0.htm](pathfinder-bestiary-2-items/3t3ulXs6r1EL76F0.htm)|Breath Weapon|auto-trad|
|[3t8p1OnCR5iUrhxt.htm](pathfinder-bestiary-2-items/3t8p1OnCR5iUrhxt.htm)|Pounce|auto-trad|
|[3tFe6InN38WdT6ps.htm](pathfinder-bestiary-2-items/3tFe6InN38WdT6ps.htm)|Darkvision|auto-trad|
|[3Ut3Piu4ATI3yyUU.htm](pathfinder-bestiary-2-items/3Ut3Piu4ATI3yyUU.htm)|Regeneration 25 (Deactivated by Good or Silver)|auto-trad|
|[3vycv9ThXOQbgNBf.htm](pathfinder-bestiary-2-items/3vycv9ThXOQbgNBf.htm)|Instinctual Tinker|auto-trad|
|[3WqEPpUcLFd5pJv6.htm](pathfinder-bestiary-2-items/3WqEPpUcLFd5pJv6.htm)|Protean Anatomy 6|auto-trad|
|[3YdQIsd5sKYMGTIc.htm](pathfinder-bestiary-2-items/3YdQIsd5sKYMGTIc.htm)|Regurgitation|auto-trad|
|[3z9Sgd2sbQUaFIUK.htm](pathfinder-bestiary-2-items/3z9Sgd2sbQUaFIUK.htm)|Dagger|auto-trad|
|[3zjA9MZy3tcHkLRD.htm](pathfinder-bestiary-2-items/3zjA9MZy3tcHkLRD.htm)|At-Will Spells|auto-trad|
|[40BuHxitDl0hJDTK.htm](pathfinder-bestiary-2-items/40BuHxitDl0hJDTK.htm)|Toxic Skin|auto-trad|
|[42LsnRw0ara1e2xW.htm](pathfinder-bestiary-2-items/42LsnRw0ara1e2xW.htm)|Darkvision|auto-trad|
|[43BphUu4vcCAeieN.htm](pathfinder-bestiary-2-items/43BphUu4vcCAeieN.htm)|Carbuncle Empathy|auto-trad|
|[43gvQaRJhhjmfG3Z.htm](pathfinder-bestiary-2-items/43gvQaRJhhjmfG3Z.htm)|Blood Drain|auto-trad|
|[43K5JpJ2j1VBI70k.htm](pathfinder-bestiary-2-items/43K5JpJ2j1VBI70k.htm)|Darkvision|auto-trad|
|[44O3eEgI5o0mZpbU.htm](pathfinder-bestiary-2-items/44O3eEgI5o0mZpbU.htm)|Telepathy|auto-trad|
|[44tsCgB5uAVisAQ3.htm](pathfinder-bestiary-2-items/44tsCgB5uAVisAQ3.htm)|Darkvision|auto-trad|
|[49xv61tdfV6J6P29.htm](pathfinder-bestiary-2-items/49xv61tdfV6J6P29.htm)|Discorporate|auto-trad|
|[49zcmiZjqimM6a7b.htm](pathfinder-bestiary-2-items/49zcmiZjqimM6a7b.htm)|Wrap in Coils|auto-trad|
|[4AlhbRnSYawCodcB.htm](pathfinder-bestiary-2-items/4AlhbRnSYawCodcB.htm)|Darkvision|auto-trad|
|[4au76ROiAjb1dHdY.htm](pathfinder-bestiary-2-items/4au76ROiAjb1dHdY.htm)|Draconic Resistance|auto-trad|
|[4b9FtRKJfWmjYbQR.htm](pathfinder-bestiary-2-items/4b9FtRKJfWmjYbQR.htm)|Focus Gaze|auto-trad|
|[4BH9uwuYigIuBuzB.htm](pathfinder-bestiary-2-items/4BH9uwuYigIuBuzB.htm)|Lightning Blast|auto-trad|
|[4CALESo8Bdrqe3vE.htm](pathfinder-bestiary-2-items/4CALESo8Bdrqe3vE.htm)|Spill Venom|auto-trad|
|[4cuSJxJLUg7icXmj.htm](pathfinder-bestiary-2-items/4cuSJxJLUg7icXmj.htm)|Vulnerable to Shatter|auto-trad|
|[4CyhH5ZkHNlxJtIP.htm](pathfinder-bestiary-2-items/4CyhH5ZkHNlxJtIP.htm)|Regeneration 10 (deactivated by cold iron)|auto-trad|
|[4d37i0818MYTgBAF.htm](pathfinder-bestiary-2-items/4d37i0818MYTgBAF.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[4DpvbfaL4gw6x5Iy.htm](pathfinder-bestiary-2-items/4DpvbfaL4gw6x5Iy.htm)|Scent|auto-trad|
|[4eg3EowtqBauiDtw.htm](pathfinder-bestiary-2-items/4eg3EowtqBauiDtw.htm)|Constant Spells|auto-trad|
|[4eJg9N0UmfD2Yt6C.htm](pathfinder-bestiary-2-items/4eJg9N0UmfD2Yt6C.htm)|Fist|auto-trad|
|[4f8Xn8AVpubItk3h.htm](pathfinder-bestiary-2-items/4f8Xn8AVpubItk3h.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[4fUKQrOX75rYdFV8.htm](pathfinder-bestiary-2-items/4fUKQrOX75rYdFV8.htm)|Entropy Sense|auto-trad|
|[4J4DJgMerRgq1mfr.htm](pathfinder-bestiary-2-items/4J4DJgMerRgq1mfr.htm)|Claw|auto-trad|
|[4jUkeNIE92J0asLl.htm](pathfinder-bestiary-2-items/4jUkeNIE92J0asLl.htm)|Negative Healing|auto-trad|
|[4KxhR1H4vfF5teNe.htm](pathfinder-bestiary-2-items/4KxhR1H4vfF5teNe.htm)|Breath Weapon|auto-trad|
|[4LgyNabFsMIjCO9s.htm](pathfinder-bestiary-2-items/4LgyNabFsMIjCO9s.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[4MEkxras2sA4aWfg.htm](pathfinder-bestiary-2-items/4MEkxras2sA4aWfg.htm)|Spiked Chain|auto-trad|
|[4mqhPMJ1Yzc6mTFk.htm](pathfinder-bestiary-2-items/4mqhPMJ1Yzc6mTFk.htm)|Draconic Momentum|auto-trad|
|[4NE1llNXfbsRiHmP.htm](pathfinder-bestiary-2-items/4NE1llNXfbsRiHmP.htm)|Savage Jaws|auto-trad|
|[4Ok3T9qk3DEMSviw.htm](pathfinder-bestiary-2-items/4Ok3T9qk3DEMSviw.htm)|Darkvision|auto-trad|
|[4oL2L4CT1FG6IAEu.htm](pathfinder-bestiary-2-items/4oL2L4CT1FG6IAEu.htm)|Grab|auto-trad|
|[4OphdUKxBpxPyBKN.htm](pathfinder-bestiary-2-items/4OphdUKxBpxPyBKN.htm)|Tail Drag|auto-trad|
|[4Ot5qt44391NJqRn.htm](pathfinder-bestiary-2-items/4Ot5qt44391NJqRn.htm)|Primal Innate Spells|auto-trad|
|[4PAeJHUBsggCTLYw.htm](pathfinder-bestiary-2-items/4PAeJHUBsggCTLYw.htm)|Tail|auto-trad|
|[4PafOhYr090tdIQE.htm](pathfinder-bestiary-2-items/4PafOhYr090tdIQE.htm)|Aging Strikes|auto-trad|
|[4PmFH52nJw8E0Xwl.htm](pathfinder-bestiary-2-items/4PmFH52nJw8E0Xwl.htm)|Grab|auto-trad|
|[4R3SAD3faLDXH2p1.htm](pathfinder-bestiary-2-items/4R3SAD3faLDXH2p1.htm)|Divine Innate Spells|auto-trad|
|[4SqlNn1QJhVGrfJb.htm](pathfinder-bestiary-2-items/4SqlNn1QJhVGrfJb.htm)|Darkvision|auto-trad|
|[4tddCK0S1FejzHcG.htm](pathfinder-bestiary-2-items/4tddCK0S1FejzHcG.htm)|Wild Empathy|auto-trad|
|[4tJ8KAPk36CVF7E2.htm](pathfinder-bestiary-2-items/4tJ8KAPk36CVF7E2.htm)|Savage Jaws|auto-trad|
|[4ujIS89zTfcMbbaX.htm](pathfinder-bestiary-2-items/4ujIS89zTfcMbbaX.htm)|Jaws|auto-trad|
|[4UMDwIzgdxumhvIf.htm](pathfinder-bestiary-2-items/4UMDwIzgdxumhvIf.htm)|Swarm Mind|auto-trad|
|[4Uoy7JHv0Ea9SrCr.htm](pathfinder-bestiary-2-items/4Uoy7JHv0Ea9SrCr.htm)|Baffling Bluff|auto-trad|
|[4w3vvxZvUVrs2iWm.htm](pathfinder-bestiary-2-items/4w3vvxZvUVrs2iWm.htm)|Darkvision|auto-trad|
|[4W4LiMhJykn0tAnC.htm](pathfinder-bestiary-2-items/4W4LiMhJykn0tAnC.htm)|Darkvision|auto-trad|
|[4z5ULy6UhFHErn4I.htm](pathfinder-bestiary-2-items/4z5ULy6UhFHErn4I.htm)|Darkvision|auto-trad|
|[4zBBMGUTIvxbdVBf.htm](pathfinder-bestiary-2-items/4zBBMGUTIvxbdVBf.htm)|Fist|auto-trad|
|[4ZdMoPLM5L40W4TK.htm](pathfinder-bestiary-2-items/4ZdMoPLM5L40W4TK.htm)|Claw|auto-trad|
|[4zEyOGGm0vwVrtmi.htm](pathfinder-bestiary-2-items/4zEyOGGm0vwVrtmi.htm)|Cunning|auto-trad|
|[52OH24WzpzdT3ptJ.htm](pathfinder-bestiary-2-items/52OH24WzpzdT3ptJ.htm)|Envisioning|auto-trad|
|[54mSt8tnFXfSUjhU.htm](pathfinder-bestiary-2-items/54mSt8tnFXfSUjhU.htm)|Cling|auto-trad|
|[54xjK61C3DBrqHe7.htm](pathfinder-bestiary-2-items/54xjK61C3DBrqHe7.htm)|Constant Spells|auto-trad|
|[58HvfTTw56mTEDsj.htm](pathfinder-bestiary-2-items/58HvfTTw56mTEDsj.htm)|Breath Weapon|auto-trad|
|[5aqMBttykiS0SKAb.htm](pathfinder-bestiary-2-items/5aqMBttykiS0SKAb.htm)|Petrifying Glance|auto-trad|
|[5ASzxj6oyeWu6gWM.htm](pathfinder-bestiary-2-items/5ASzxj6oyeWu6gWM.htm)|Darkvision|auto-trad|
|[5cC1uj7hBSGN8tix.htm](pathfinder-bestiary-2-items/5cC1uj7hBSGN8tix.htm)|Lifesense|auto-trad|
|[5CgVbeYygEXhZf6y.htm](pathfinder-bestiary-2-items/5CgVbeYygEXhZf6y.htm)|Bully's Bludgeon|auto-trad|
|[5d1k1jwifXVc23o0.htm](pathfinder-bestiary-2-items/5d1k1jwifXVc23o0.htm)|Claw|auto-trad|
|[5djM8sO4U2PY23bG.htm](pathfinder-bestiary-2-items/5djM8sO4U2PY23bG.htm)|Unstoppable|auto-trad|
|[5Ds8GMXoIjWg9j7Q.htm](pathfinder-bestiary-2-items/5Ds8GMXoIjWg9j7Q.htm)|Tentacle|auto-trad|
|[5e5FND78feSKXP99.htm](pathfinder-bestiary-2-items/5e5FND78feSKXP99.htm)|Impaling Chain|auto-trad|
|[5E9Da5IYp7uvbZKT.htm](pathfinder-bestiary-2-items/5E9Da5IYp7uvbZKT.htm)|Divine Innate Spells|auto-trad|
|[5EcCWK4rV7lqrPqY.htm](pathfinder-bestiary-2-items/5EcCWK4rV7lqrPqY.htm)|+2 Status to All Saves vs. Mental|auto-trad|
|[5f6mJgvaVADXCaAz.htm](pathfinder-bestiary-2-items/5f6mJgvaVADXCaAz.htm)|Scent|auto-trad|
|[5fPhzfjVfDH0Ssgz.htm](pathfinder-bestiary-2-items/5fPhzfjVfDH0Ssgz.htm)|Terrifying Gaze|auto-trad|
|[5g1Keo0L3GpJZyQp.htm](pathfinder-bestiary-2-items/5g1Keo0L3GpJZyQp.htm)|Curse of the Wereboar|auto-trad|
|[5gLIoGOoPN1ReZMD.htm](pathfinder-bestiary-2-items/5gLIoGOoPN1ReZMD.htm)|Wing|auto-trad|
|[5hqdjh4Jn5wtzqGs.htm](pathfinder-bestiary-2-items/5hqdjh4Jn5wtzqGs.htm)|Tendril|auto-trad|
|[5IixHzakyftZgS0g.htm](pathfinder-bestiary-2-items/5IixHzakyftZgS0g.htm)|Speed Surge|auto-trad|
|[5IQ8tiyhCdSfyPEo.htm](pathfinder-bestiary-2-items/5IQ8tiyhCdSfyPEo.htm)|Grab|auto-trad|
|[5kEI04PwOkPj2l0j.htm](pathfinder-bestiary-2-items/5kEI04PwOkPj2l0j.htm)|At-Will Spells|auto-trad|
|[5lCoU2D6djePIJZT.htm](pathfinder-bestiary-2-items/5lCoU2D6djePIJZT.htm)|Web Sense|auto-trad|
|[5lPaA6GKal2YHAjj.htm](pathfinder-bestiary-2-items/5lPaA6GKal2YHAjj.htm)|Jaws|auto-trad|
|[5nazHNTE9SHWxEH5.htm](pathfinder-bestiary-2-items/5nazHNTE9SHWxEH5.htm)|Claw|auto-trad|
|[5OLDndk87U2qEJ5p.htm](pathfinder-bestiary-2-items/5OLDndk87U2qEJ5p.htm)|Pincer|auto-trad|
|[5p5UO2vVsNmY2INe.htm](pathfinder-bestiary-2-items/5p5UO2vVsNmY2INe.htm)|Primal Innate Spells|auto-trad|
|[5PvfqR4GcUsfw2mR.htm](pathfinder-bestiary-2-items/5PvfqR4GcUsfw2mR.htm)|Rend|auto-trad|
|[5qArY8oNtsZLv3Lg.htm](pathfinder-bestiary-2-items/5qArY8oNtsZLv3Lg.htm)|Jaws|auto-trad|
|[5qgx9qhXAceS6uLh.htm](pathfinder-bestiary-2-items/5qgx9qhXAceS6uLh.htm)|Enraged Cunning|auto-trad|
|[5QKXjFlhvGGdNcxf.htm](pathfinder-bestiary-2-items/5QKXjFlhvGGdNcxf.htm)|Claw|auto-trad|
|[5QMBo1Vy2JZTTsNV.htm](pathfinder-bestiary-2-items/5QMBo1Vy2JZTTsNV.htm)|Tremorsense|auto-trad|
|[5rN2CIypbGA4N34M.htm](pathfinder-bestiary-2-items/5rN2CIypbGA4N34M.htm)|Fangs|auto-trad|
|[5RsDoTAVa9W8gKnx.htm](pathfinder-bestiary-2-items/5RsDoTAVa9W8gKnx.htm)|Draconic Resistance|auto-trad|
|[5rxGka9M6pYlIoVb.htm](pathfinder-bestiary-2-items/5rxGka9M6pYlIoVb.htm)|Occult Innate Spells|auto-trad|
|[5SaRw4TGk2D0VBTZ.htm](pathfinder-bestiary-2-items/5SaRw4TGk2D0VBTZ.htm)|Draconic Momentum|auto-trad|
|[5t9D4gKM7JVpBSf6.htm](pathfinder-bestiary-2-items/5t9D4gKM7JVpBSf6.htm)|Wild Empathy|auto-trad|
|[5tiASMinDzyHUAEv.htm](pathfinder-bestiary-2-items/5tiASMinDzyHUAEv.htm)|Jaws|auto-trad|
|[5tqNBlspN6txKrz9.htm](pathfinder-bestiary-2-items/5tqNBlspN6txKrz9.htm)|Jaws|auto-trad|
|[5UAZUiojo21FxrFI.htm](pathfinder-bestiary-2-items/5UAZUiojo21FxrFI.htm)|Primal Innate Spells|auto-trad|
|[5UFt8Dv6NCEOpZlE.htm](pathfinder-bestiary-2-items/5UFt8Dv6NCEOpZlE.htm)|Constant Spells|auto-trad|
|[5Vw9VTB3I1sie1g1.htm](pathfinder-bestiary-2-items/5Vw9VTB3I1sie1g1.htm)|Sarglagon Venom|auto-trad|
|[5WLytdwKYQa3JLh7.htm](pathfinder-bestiary-2-items/5WLytdwKYQa3JLh7.htm)|Malleable|auto-trad|
|[5XOy3zqts39I3bzo.htm](pathfinder-bestiary-2-items/5XOy3zqts39I3bzo.htm)|Low-Light Vision|auto-trad|
|[5xuP57kLx1Xu16ZO.htm](pathfinder-bestiary-2-items/5xuP57kLx1Xu16ZO.htm)|Fist|auto-trad|
|[5YDssRfPpgId4gTE.htm](pathfinder-bestiary-2-items/5YDssRfPpgId4gTE.htm)|Low-Light Vision|auto-trad|
|[5Ygh7NnWAZBamFaS.htm](pathfinder-bestiary-2-items/5Ygh7NnWAZBamFaS.htm)|Jaws|auto-trad|
|[5yWmBrLhwYvStUFi.htm](pathfinder-bestiary-2-items/5yWmBrLhwYvStUFi.htm)|At-Will Spells|auto-trad|
|[60d6TRr07HgWYE5B.htm](pathfinder-bestiary-2-items/60d6TRr07HgWYE5B.htm)|Current|auto-trad|
|[61EkmT4Ba451hMDC.htm](pathfinder-bestiary-2-items/61EkmT4Ba451hMDC.htm)|Jaws|auto-trad|
|[61GyOh9BdFBPZCUD.htm](pathfinder-bestiary-2-items/61GyOh9BdFBPZCUD.htm)|Low-Light Vision|auto-trad|
|[61nmK3yvcoeX2PbG.htm](pathfinder-bestiary-2-items/61nmK3yvcoeX2PbG.htm)|Low-Light Vision|auto-trad|
|[68hstcNYAfYsk65j.htm](pathfinder-bestiary-2-items/68hstcNYAfYsk65j.htm)|Claw|auto-trad|
|[68IxixgMddYHirAR.htm](pathfinder-bestiary-2-items/68IxixgMddYHirAR.htm)|Constrict|auto-trad|
|[68RVtURJJY5N5dTK.htm](pathfinder-bestiary-2-items/68RVtURJJY5N5dTK.htm)|Darkvision|auto-trad|
|[6a1pfDWvzEsw0JWE.htm](pathfinder-bestiary-2-items/6a1pfDWvzEsw0JWE.htm)|Darkvision|auto-trad|
|[6A6lheQTnhAryrxG.htm](pathfinder-bestiary-2-items/6A6lheQTnhAryrxG.htm)|Constant Spells|auto-trad|
|[6B5ZXZm5DwAILPJ2.htm](pathfinder-bestiary-2-items/6B5ZXZm5DwAILPJ2.htm)|Dance of Death|auto-trad|
|[6BxCWHRHwlGUEbIO.htm](pathfinder-bestiary-2-items/6BxCWHRHwlGUEbIO.htm)|Primal Innate Spells|auto-trad|
|[6CGO8ENM5nFAurjN.htm](pathfinder-bestiary-2-items/6CGO8ENM5nFAurjN.htm)|Ranseur|auto-trad|
|[6dcoGxoSd8A6QFv2.htm](pathfinder-bestiary-2-items/6dcoGxoSd8A6QFv2.htm)|Twisting Tail|auto-trad|
|[6gE7wfS2bLW0PuDW.htm](pathfinder-bestiary-2-items/6gE7wfS2bLW0PuDW.htm)|Kiss of Death|auto-trad|
|[6gwNFiImkw2H0QWA.htm](pathfinder-bestiary-2-items/6gwNFiImkw2H0QWA.htm)|Grab|auto-trad|
|[6i7GkvP5XmSn8g6G.htm](pathfinder-bestiary-2-items/6i7GkvP5XmSn8g6G.htm)|Breath Weapon|auto-trad|
|[6ITLmA9FU1LuwZNY.htm](pathfinder-bestiary-2-items/6ITLmA9FU1LuwZNY.htm)|Divine Innate Spells|auto-trad|
|[6jVS5MIWVjrSMBki.htm](pathfinder-bestiary-2-items/6jVS5MIWVjrSMBki.htm)|Telepathy|auto-trad|
|[6leNSciOB3vrO7El.htm](pathfinder-bestiary-2-items/6leNSciOB3vrO7El.htm)|Divine Innate Spells|auto-trad|
|[6lnr0C4QK4LAkOa1.htm](pathfinder-bestiary-2-items/6lnr0C4QK4LAkOa1.htm)|Low-Light Vision|auto-trad|
|[6LuECgP1a3GGskbB.htm](pathfinder-bestiary-2-items/6LuECgP1a3GGskbB.htm)|Jaws|auto-trad|
|[6mCb1o0EW6yQ0NfB.htm](pathfinder-bestiary-2-items/6mCb1o0EW6yQ0NfB.htm)|Divine Innate Spells|auto-trad|
|[6Mfkl1CejuYcVcXf.htm](pathfinder-bestiary-2-items/6Mfkl1CejuYcVcXf.htm)|Seed|auto-trad|
|[6mmZWINTcnrrNQcU.htm](pathfinder-bestiary-2-items/6mmZWINTcnrrNQcU.htm)|Constant Spells|auto-trad|
|[6N8zQ0TKFxKlkOTG.htm](pathfinder-bestiary-2-items/6N8zQ0TKFxKlkOTG.htm)|Claim Wealth|auto-trad|
|[6NgDd3kvRPxleBuE.htm](pathfinder-bestiary-2-items/6NgDd3kvRPxleBuE.htm)|Blinding Sand|auto-trad|
|[6no2LBNfSHP8w9DB.htm](pathfinder-bestiary-2-items/6no2LBNfSHP8w9DB.htm)|Negative Healing|auto-trad|
|[6oTpSA3IXew7MulO.htm](pathfinder-bestiary-2-items/6oTpSA3IXew7MulO.htm)|Hand Of Fate|auto-trad|
|[6Psn5RSYm8UGDpwM.htm](pathfinder-bestiary-2-items/6Psn5RSYm8UGDpwM.htm)|Pod Prison|auto-trad|
|[6qaFJjq9JjlhZbaw.htm](pathfinder-bestiary-2-items/6qaFJjq9JjlhZbaw.htm)|Ogre Spider Venom|auto-trad|
|[6qDqRA2Nq93rsvx9.htm](pathfinder-bestiary-2-items/6qDqRA2Nq93rsvx9.htm)|Greater Electrolocation|auto-trad|
|[6rslY3YCHH1EYxYL.htm](pathfinder-bestiary-2-items/6rslY3YCHH1EYxYL.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[6Sms9YOa60rjevVP.htm](pathfinder-bestiary-2-items/6Sms9YOa60rjevVP.htm)|Jaws|auto-trad|
|[6t0iHm0ru1bcVGTw.htm](pathfinder-bestiary-2-items/6t0iHm0ru1bcVGTw.htm)|Entangling Tendrils|auto-trad|
|[6U0xCdudnhZPawQv.htm](pathfinder-bestiary-2-items/6U0xCdudnhZPawQv.htm)|Web War Flail|auto-trad|
|[6UhOo5jcpQz76GcZ.htm](pathfinder-bestiary-2-items/6UhOo5jcpQz76GcZ.htm)|Darkvision|auto-trad|
|[6VEsckJo4laa2uls.htm](pathfinder-bestiary-2-items/6VEsckJo4laa2uls.htm)|Claw|auto-trad|
|[6vnexEfGJBfKrtHp.htm](pathfinder-bestiary-2-items/6vnexEfGJBfKrtHp.htm)|Tusk Sweep|auto-trad|
|[6xKgrVRYAjGEO0p7.htm](pathfinder-bestiary-2-items/6xKgrVRYAjGEO0p7.htm)|Flame Jet|auto-trad|
|[6XMcWttuyRBueX73.htm](pathfinder-bestiary-2-items/6XMcWttuyRBueX73.htm)|Knockdown|auto-trad|
|[6Yl0sNRGgIqfvRj8.htm](pathfinder-bestiary-2-items/6Yl0sNRGgIqfvRj8.htm)|Darkvision|auto-trad|
|[6ZtjeB9jdEW4o2QJ.htm](pathfinder-bestiary-2-items/6ZtjeB9jdEW4o2QJ.htm)|Bite|auto-trad|
|[71Xez8c9broSGKbf.htm](pathfinder-bestiary-2-items/71Xez8c9broSGKbf.htm)|Quill Barrage|auto-trad|
|[72YvqHEAOHV34zLK.htm](pathfinder-bestiary-2-items/72YvqHEAOHV34zLK.htm)|Mucus|auto-trad|
|[76jTiA45mzSvkuz1.htm](pathfinder-bestiary-2-items/76jTiA45mzSvkuz1.htm)|Antler|auto-trad|
|[7AMBKbBXmPFp0RlP.htm](pathfinder-bestiary-2-items/7AMBKbBXmPFp0RlP.htm)|Darkvision|auto-trad|
|[7B1Q0p1BKuDwOEoo.htm](pathfinder-bestiary-2-items/7B1Q0p1BKuDwOEoo.htm)|Smoke Vision|auto-trad|
|[7cdnWouJZ6KDhfxj.htm](pathfinder-bestiary-2-items/7cdnWouJZ6KDhfxj.htm)|Club|auto-trad|
|[7D3xnmgdDZGumRA4.htm](pathfinder-bestiary-2-items/7D3xnmgdDZGumRA4.htm)|Breath Weapon|auto-trad|
|[7dOMKFcaiBcHkgt1.htm](pathfinder-bestiary-2-items/7dOMKFcaiBcHkgt1.htm)|Divine Innate Spells|auto-trad|
|[7dxhRfITiiUW00kr.htm](pathfinder-bestiary-2-items/7dxhRfITiiUW00kr.htm)|Darkvision|auto-trad|
|[7EYorPSwB5MGbm8A.htm](pathfinder-bestiary-2-items/7EYorPSwB5MGbm8A.htm)|Constant Spells|auto-trad|
|[7FksKj3ieU9yJEu0.htm](pathfinder-bestiary-2-items/7FksKj3ieU9yJEu0.htm)|Darkvision|auto-trad|
|[7GHhkRMg6SxILo1W.htm](pathfinder-bestiary-2-items/7GHhkRMg6SxILo1W.htm)|Lifesense|auto-trad|
|[7Hht50p2jc8ZG7Xq.htm](pathfinder-bestiary-2-items/7Hht50p2jc8ZG7Xq.htm)|Primal Innate Spells|auto-trad|
|[7HuZlfRX4ZQI1GwE.htm](pathfinder-bestiary-2-items/7HuZlfRX4ZQI1GwE.htm)|Blade|auto-trad|
|[7i3BZvYdZOVTki7v.htm](pathfinder-bestiary-2-items/7i3BZvYdZOVTki7v.htm)|Throw Rock|auto-trad|
|[7ibCw6e1BSbQ94NW.htm](pathfinder-bestiary-2-items/7ibCw6e1BSbQ94NW.htm)|Lurker's Glow|auto-trad|
|[7j5B40DMWHBTShLl.htm](pathfinder-bestiary-2-items/7j5B40DMWHBTShLl.htm)|Darkvision|auto-trad|
|[7j5ta80hcHB60jDE.htm](pathfinder-bestiary-2-items/7j5ta80hcHB60jDE.htm)|Primal Innate Spells|auto-trad|
|[7kGu1ORgE4Nay79B.htm](pathfinder-bestiary-2-items/7kGu1ORgE4Nay79B.htm)|Sneak Attack|auto-trad|
|[7MiqMH6uob5zPB4e.htm](pathfinder-bestiary-2-items/7MiqMH6uob5zPB4e.htm)|Spotlight|auto-trad|
|[7mVYoMlm7knbjqzo.htm](pathfinder-bestiary-2-items/7mVYoMlm7knbjqzo.htm)|Tail|auto-trad|
|[7NjsgQKMg0FvxDmJ.htm](pathfinder-bestiary-2-items/7NjsgQKMg0FvxDmJ.htm)|Telepathy 100 feet|auto-trad|
|[7PG4MVtAvT1kH55R.htm](pathfinder-bestiary-2-items/7PG4MVtAvT1kH55R.htm)|Telepathy (Touch)|auto-trad|
|[7pxsxbazh1PpK8A4.htm](pathfinder-bestiary-2-items/7pxsxbazh1PpK8A4.htm)|Fast Healing 10|auto-trad|
|[7Qq2wYdCdaBf6zs4.htm](pathfinder-bestiary-2-items/7Qq2wYdCdaBf6zs4.htm)|Darkvision|auto-trad|
|[7QTkBSoDwgvNiBiC.htm](pathfinder-bestiary-2-items/7QTkBSoDwgvNiBiC.htm)|Chain|auto-trad|
|[7RlCpEdZ5Lw3Omd2.htm](pathfinder-bestiary-2-items/7RlCpEdZ5Lw3Omd2.htm)|Gust|auto-trad|
|[7rYYffte0B1eDFDt.htm](pathfinder-bestiary-2-items/7rYYffte0B1eDFDt.htm)|Telepathy|auto-trad|
|[7RZaK7BB8AYtaXJb.htm](pathfinder-bestiary-2-items/7RZaK7BB8AYtaXJb.htm)|At-Will Spells|auto-trad|
|[7TyKfwJXlkKJmSp1.htm](pathfinder-bestiary-2-items/7TyKfwJXlkKJmSp1.htm)|Darkvision|auto-trad|
|[7wi6tn6G6vLyc1tN.htm](pathfinder-bestiary-2-items/7wi6tn6G6vLyc1tN.htm)|Wrap in Coils|auto-trad|
|[7x3Iu1Xd8r44ZR48.htm](pathfinder-bestiary-2-items/7x3Iu1Xd8r44ZR48.htm)|Attack of Opportunity|auto-trad|
|[7XnqXwEoetjKQZg4.htm](pathfinder-bestiary-2-items/7XnqXwEoetjKQZg4.htm)|Divine Innate Spells|auto-trad|
|[8BGsf6kEIrCtKGgo.htm](pathfinder-bestiary-2-items/8BGsf6kEIrCtKGgo.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[8CRzC395t7TeEnQO.htm](pathfinder-bestiary-2-items/8CRzC395t7TeEnQO.htm)|Darkvision|auto-trad|
|[8F42ZUECGwbl9nmU.htm](pathfinder-bestiary-2-items/8F42ZUECGwbl9nmU.htm)|Darkvision|auto-trad|
|[8g9m2H7zav2ekCFO.htm](pathfinder-bestiary-2-items/8g9m2H7zav2ekCFO.htm)|Darkvision|auto-trad|
|[8GCV9NHdglsydwmB.htm](pathfinder-bestiary-2-items/8GCV9NHdglsydwmB.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[8GQKAYkj3NIJHurF.htm](pathfinder-bestiary-2-items/8GQKAYkj3NIJHurF.htm)|Children of the Night|auto-trad|
|[8H0a3j4x2wevxTU0.htm](pathfinder-bestiary-2-items/8H0a3j4x2wevxTU0.htm)|Rock|auto-trad|
|[8J1VrbhC0fQxs0Xu.htm](pathfinder-bestiary-2-items/8J1VrbhC0fQxs0Xu.htm)|Muddy Field|auto-trad|
|[8JhtIGFYB5PcTSIt.htm](pathfinder-bestiary-2-items/8JhtIGFYB5PcTSIt.htm)|Ice Stride|auto-trad|
|[8JPlEU0ps7X2hfP6.htm](pathfinder-bestiary-2-items/8JPlEU0ps7X2hfP6.htm)|Flower|auto-trad|
|[8Mkj5lFXbki11UgV.htm](pathfinder-bestiary-2-items/8Mkj5lFXbki11UgV.htm)|Infernal Mindlink|auto-trad|
|[8nLUnVkKYxkXvAZs.htm](pathfinder-bestiary-2-items/8nLUnVkKYxkXvAZs.htm)|Constrict|auto-trad|
|[8og5NF6ejbQcG1bF.htm](pathfinder-bestiary-2-items/8og5NF6ejbQcG1bF.htm)|Rusting Grasp|auto-trad|
|[8PnlI5NzEir5W7Il.htm](pathfinder-bestiary-2-items/8PnlI5NzEir5W7Il.htm)|Derghodaemon's Stare|auto-trad|
|[8PpUoIkvn5HDKBBp.htm](pathfinder-bestiary-2-items/8PpUoIkvn5HDKBBp.htm)|Darkvision|auto-trad|
|[8prRtWLrblxto9pO.htm](pathfinder-bestiary-2-items/8prRtWLrblxto9pO.htm)|Poison Gasp|auto-trad|
|[8qlQbmcwGPfRmLYu.htm](pathfinder-bestiary-2-items/8qlQbmcwGPfRmLYu.htm)|Low-Light Vision|auto-trad|
|[8qNsivFnCww0jFZo.htm](pathfinder-bestiary-2-items/8qNsivFnCww0jFZo.htm)|Electric Missile|auto-trad|
|[8qYHl9oKTV79IZ6r.htm](pathfinder-bestiary-2-items/8qYHl9oKTV79IZ6r.htm)|Constrict|auto-trad|
|[8r06lqp1MccJLeUV.htm](pathfinder-bestiary-2-items/8r06lqp1MccJLeUV.htm)|Drain Blood|auto-trad|
|[8r5XXqPZOhI0JSfB.htm](pathfinder-bestiary-2-items/8r5XXqPZOhI0JSfB.htm)|Darkvision|auto-trad|
|[8RH2spsc79xmzAXn.htm](pathfinder-bestiary-2-items/8RH2spsc79xmzAXn.htm)|Allergen Aura|auto-trad|
|[8RpmS3nHFfxBiGaY.htm](pathfinder-bestiary-2-items/8RpmS3nHFfxBiGaY.htm)|Swarm Mind|auto-trad|
|[8SCH000Zv6X9BFPf.htm](pathfinder-bestiary-2-items/8SCH000Zv6X9BFPf.htm)|Fist|auto-trad|
|[8SI23rDwK2QYJaxP.htm](pathfinder-bestiary-2-items/8SI23rDwK2QYJaxP.htm)|Catch Rock|auto-trad|
|[8sphOZAnVIS4eVZ6.htm](pathfinder-bestiary-2-items/8sphOZAnVIS4eVZ6.htm)|Wing|auto-trad|
|[8tPI0ksCv4zvd6WV.htm](pathfinder-bestiary-2-items/8tPI0ksCv4zvd6WV.htm)|Corkscrew|auto-trad|
|[8UGnwgNXjFG8VCZU.htm](pathfinder-bestiary-2-items/8UGnwgNXjFG8VCZU.htm)|Hoof|auto-trad|
|[8UmPplmBw6ibPa9v.htm](pathfinder-bestiary-2-items/8UmPplmBw6ibPa9v.htm)|Explosion|auto-trad|
|[8UOucEsn2oF9O2uX.htm](pathfinder-bestiary-2-items/8UOucEsn2oF9O2uX.htm)|Frightful Presence|auto-trad|
|[8wrcvKK8xk5AIIva.htm](pathfinder-bestiary-2-items/8wrcvKK8xk5AIIva.htm)|Muddy Field|auto-trad|
|[8WsmugIanltHKUg1.htm](pathfinder-bestiary-2-items/8WsmugIanltHKUg1.htm)|Low-Light Vision|auto-trad|
|[8Xjni4fPUr5P0Dl9.htm](pathfinder-bestiary-2-items/8Xjni4fPUr5P0Dl9.htm)|Jellyfish Venom|auto-trad|
|[8y1uIv1RL7FsHZzu.htm](pathfinder-bestiary-2-items/8y1uIv1RL7FsHZzu.htm)|Swallow Whole|auto-trad|
|[8yvIS9Nt5mat9qnA.htm](pathfinder-bestiary-2-items/8yvIS9Nt5mat9qnA.htm)|Occult Innate Spells|auto-trad|
|[8yVRt0M6F9WbgdWV.htm](pathfinder-bestiary-2-items/8yVRt0M6F9WbgdWV.htm)|Cairn Linnorm Venom|auto-trad|
|[8Zo1SkheQdiIdsNs.htm](pathfinder-bestiary-2-items/8Zo1SkheQdiIdsNs.htm)|Darkvision|auto-trad|
|[90V4TwzjxTH7bK7r.htm](pathfinder-bestiary-2-items/90V4TwzjxTH7bK7r.htm)|Spear|auto-trad|
|[92YIjzpVzdYw3ZQC.htm](pathfinder-bestiary-2-items/92YIjzpVzdYw3ZQC.htm)|Change Shape|auto-trad|
|[967GKOeV0EqG2hzn.htm](pathfinder-bestiary-2-items/967GKOeV0EqG2hzn.htm)|Grab|auto-trad|
|[981IVfc2IS4Qnd3U.htm](pathfinder-bestiary-2-items/981IVfc2IS4Qnd3U.htm)|Aura of Sobs|auto-trad|
|[98N9e0IqCp7WoRHr.htm](pathfinder-bestiary-2-items/98N9e0IqCp7WoRHr.htm)|Darkvision|auto-trad|
|[98pkTquG5m1vBxbt.htm](pathfinder-bestiary-2-items/98pkTquG5m1vBxbt.htm)|Tail|auto-trad|
|[9AHoo6l3g2enrpEz.htm](pathfinder-bestiary-2-items/9AHoo6l3g2enrpEz.htm)|Occult Innate Spells|auto-trad|
|[9atqlz9j100vC4uo.htm](pathfinder-bestiary-2-items/9atqlz9j100vC4uo.htm)|Jaws|auto-trad|
|[9bkFg1e33lRT8urN.htm](pathfinder-bestiary-2-items/9bkFg1e33lRT8urN.htm)|Sneak Attack|auto-trad|
|[9bMQUXIt8V0ivYCg.htm](pathfinder-bestiary-2-items/9bMQUXIt8V0ivYCg.htm)|Darkvision|auto-trad|
|[9bSMDZ283g6Zq6IU.htm](pathfinder-bestiary-2-items/9bSMDZ283g6Zq6IU.htm)|Aquatic Ambush|auto-trad|
|[9cHX93V0XpVb7b7b.htm](pathfinder-bestiary-2-items/9cHX93V0XpVb7b7b.htm)|Lashing Tongues|auto-trad|
|[9EJRdPBHkHMyY0yk.htm](pathfinder-bestiary-2-items/9EJRdPBHkHMyY0yk.htm)|Trample|auto-trad|
|[9eONvr5by0xfPFcf.htm](pathfinder-bestiary-2-items/9eONvr5by0xfPFcf.htm)|Witchflame Caress|auto-trad|
|[9FcMZnkENPj4z3q0.htm](pathfinder-bestiary-2-items/9FcMZnkENPj4z3q0.htm)|Warpwave Strike|auto-trad|
|[9G77nycfkHc93HGi.htm](pathfinder-bestiary-2-items/9G77nycfkHc93HGi.htm)|Dagger|auto-trad|
|[9H6kMOzk61FQZoC9.htm](pathfinder-bestiary-2-items/9H6kMOzk61FQZoC9.htm)|Tail|auto-trad|
|[9hRQJlQwiVSfsdc2.htm](pathfinder-bestiary-2-items/9hRQJlQwiVSfsdc2.htm)|Undulate|auto-trad|
|[9k7o784yI4Bka8ys.htm](pathfinder-bestiary-2-items/9k7o784yI4Bka8ys.htm)|Crossbow|auto-trad|
|[9KZUjyVAnGC6gEbC.htm](pathfinder-bestiary-2-items/9KZUjyVAnGC6gEbC.htm)|Darkvision|auto-trad|
|[9oJwSlJ6tyt3KuRl.htm](pathfinder-bestiary-2-items/9oJwSlJ6tyt3KuRl.htm)|Spear|auto-trad|
|[9ONTjzUo2qngSIHO.htm](pathfinder-bestiary-2-items/9ONTjzUo2qngSIHO.htm)|Grab|auto-trad|
|[9Q05lkXgY0PrqSQz.htm](pathfinder-bestiary-2-items/9Q05lkXgY0PrqSQz.htm)|At-Will Spells|auto-trad|
|[9rLnLLOFCIgXRAK8.htm](pathfinder-bestiary-2-items/9rLnLLOFCIgXRAK8.htm)|Jaws|auto-trad|
|[9tmJIJ6vy9JEPaL3.htm](pathfinder-bestiary-2-items/9tmJIJ6vy9JEPaL3.htm)|Tail|auto-trad|
|[9tpWx4QY08q4Nv5G.htm](pathfinder-bestiary-2-items/9tpWx4QY08q4Nv5G.htm)|Telepathy|auto-trad|
|[9tukJ8Am3f00bHUu.htm](pathfinder-bestiary-2-items/9tukJ8Am3f00bHUu.htm)|Savage Jaws|auto-trad|
|[9UD8fQB0IDkl7w1X.htm](pathfinder-bestiary-2-items/9UD8fQB0IDkl7w1X.htm)|Staggering Servitude|auto-trad|
|[9UqGqGPYi6N8yk6X.htm](pathfinder-bestiary-2-items/9UqGqGPYi6N8yk6X.htm)|Storm of Vines|auto-trad|
|[9wolaUuOroFk0cOg.htm](pathfinder-bestiary-2-items/9wolaUuOroFk0cOg.htm)|Frightful Presence|auto-trad|
|[9WzRmRmcle5Fmp7N.htm](pathfinder-bestiary-2-items/9WzRmRmcle5Fmp7N.htm)|Alter Weather|auto-trad|
|[9Xg9EXT1XEoi38zM.htm](pathfinder-bestiary-2-items/9Xg9EXT1XEoi38zM.htm)|Spear|auto-trad|
|[9XxEDCpiqnYKds7V.htm](pathfinder-bestiary-2-items/9XxEDCpiqnYKds7V.htm)|Draconic Resistance|auto-trad|
|[9yu44cM7W0Xg2Q36.htm](pathfinder-bestiary-2-items/9yu44cM7W0Xg2Q36.htm)|Planar Incarnation - Astral Plane|auto-trad|
|[9zIzERdEdvnnEjwe.htm](pathfinder-bestiary-2-items/9zIzERdEdvnnEjwe.htm)|Claw|auto-trad|
|[A18yDTtE84tJ1jhJ.htm](pathfinder-bestiary-2-items/A18yDTtE84tJ1jhJ.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[a29GFo7lbKhSTAYm.htm](pathfinder-bestiary-2-items/a29GFo7lbKhSTAYm.htm)|Fast Healing 2 (when touching ice or snow)|auto-trad|
|[A2KzHDp53CpDVtv5.htm](pathfinder-bestiary-2-items/A2KzHDp53CpDVtv5.htm)|At-Will Spells|auto-trad|
|[A2lqtFMyanvkN6wM.htm](pathfinder-bestiary-2-items/A2lqtFMyanvkN6wM.htm)|Low-Light Vision|auto-trad|
|[a3cYrmsIGVchwgtR.htm](pathfinder-bestiary-2-items/a3cYrmsIGVchwgtR.htm)|Pseudopod|auto-trad|
|[a3nL0MNgY6YXvvP9.htm](pathfinder-bestiary-2-items/a3nL0MNgY6YXvvP9.htm)|Jaws|auto-trad|
|[A3RVgZkJQrtRoYi2.htm](pathfinder-bestiary-2-items/A3RVgZkJQrtRoYi2.htm)|Tusk|auto-trad|
|[A5NdjSB3IdJS5DAb.htm](pathfinder-bestiary-2-items/A5NdjSB3IdJS5DAb.htm)|Draconic Frenzy|auto-trad|
|[a8kHbScnPzkgg3j3.htm](pathfinder-bestiary-2-items/a8kHbScnPzkgg3j3.htm)|Darkvision|auto-trad|
|[a9NvSrt50HRCakM8.htm](pathfinder-bestiary-2-items/a9NvSrt50HRCakM8.htm)|Telepathy|auto-trad|
|[A9SIfdrB8Hk28o23.htm](pathfinder-bestiary-2-items/A9SIfdrB8Hk28o23.htm)|Jaws|auto-trad|
|[AasR87d76iiM7CAd.htm](pathfinder-bestiary-2-items/AasR87d76iiM7CAd.htm)|Mandibles|auto-trad|
|[AAyJQ8CViWWMXWPU.htm](pathfinder-bestiary-2-items/AAyJQ8CViWWMXWPU.htm)|Jaws|auto-trad|
|[Ab1sWqDegn71eEve.htm](pathfinder-bestiary-2-items/Ab1sWqDegn71eEve.htm)|Magma Tomb|auto-trad|
|[Ab4r9QqPgjwOMjaW.htm](pathfinder-bestiary-2-items/Ab4r9QqPgjwOMjaW.htm)|Tail|auto-trad|
|[AbCU1QPQN43QSP6d.htm](pathfinder-bestiary-2-items/AbCU1QPQN43QSP6d.htm)|Speed Surge|auto-trad|
|[abGmJgduMKED2VeX.htm](pathfinder-bestiary-2-items/abGmJgduMKED2VeX.htm)|Chupar|auto-trad|
|[aBIEnRecRBiBmpU4.htm](pathfinder-bestiary-2-items/aBIEnRecRBiBmpU4.htm)|Attack of Opportunity|auto-trad|
|[AbJYjoA74dUGnY4h.htm](pathfinder-bestiary-2-items/AbJYjoA74dUGnY4h.htm)|Grab|auto-trad|
|[ABqHj13JWyqTUohA.htm](pathfinder-bestiary-2-items/ABqHj13JWyqTUohA.htm)|Misty Tendril|auto-trad|
|[acasfdr7NXNxUTjJ.htm](pathfinder-bestiary-2-items/acasfdr7NXNxUTjJ.htm)|At-Will Spells|auto-trad|
|[adAFWbGgq7jnZcih.htm](pathfinder-bestiary-2-items/adAFWbGgq7jnZcih.htm)|Tail|auto-trad|
|[aDF1WE7DJM17CR29.htm](pathfinder-bestiary-2-items/aDF1WE7DJM17CR29.htm)|Darkvision|auto-trad|
|[ADUDwILKarUV2YPp.htm](pathfinder-bestiary-2-items/ADUDwILKarUV2YPp.htm)|Slow|auto-trad|
|[aeyCi4yAR1t8VtMz.htm](pathfinder-bestiary-2-items/aeyCi4yAR1t8VtMz.htm)|Divine Innate Spells|auto-trad|
|[aFiPts0pbbuzRaLf.htm](pathfinder-bestiary-2-items/aFiPts0pbbuzRaLf.htm)|Sudden Shove|auto-trad|
|[AfkzOHwQLaNCyPZt.htm](pathfinder-bestiary-2-items/AfkzOHwQLaNCyPZt.htm)|Jaws|auto-trad|
|[AFlgcQtl6dJr1rtT.htm](pathfinder-bestiary-2-items/AFlgcQtl6dJr1rtT.htm)|Wolverine Rage|auto-trad|
|[AFv5FWlpT2zyryiG.htm](pathfinder-bestiary-2-items/AFv5FWlpT2zyryiG.htm)|Swarm Mind|auto-trad|
|[AfwRVW7rPHMx2CVt.htm](pathfinder-bestiary-2-items/AfwRVW7rPHMx2CVt.htm)|Darkvision|auto-trad|
|[AGDYcCgG4mkmvihe.htm](pathfinder-bestiary-2-items/AGDYcCgG4mkmvihe.htm)|Breath Weapon|auto-trad|
|[AGm4GoMUxx5xAmpo.htm](pathfinder-bestiary-2-items/AGm4GoMUxx5xAmpo.htm)|Swarm Mind|auto-trad|
|[agTNdZe3k9VHuHav.htm](pathfinder-bestiary-2-items/agTNdZe3k9VHuHav.htm)|Constant Spells|auto-trad|
|[AGZBKBnJMkp5NrlX.htm](pathfinder-bestiary-2-items/AGZBKBnJMkp5NrlX.htm)|Fangs|auto-trad|
|[aHamTcS3vkrZLHGN.htm](pathfinder-bestiary-2-items/aHamTcS3vkrZLHGN.htm)|Instant Suggestion|auto-trad|
|[AhfsEZ9fBUS6e02P.htm](pathfinder-bestiary-2-items/AhfsEZ9fBUS6e02P.htm)|Poisonous Warts|auto-trad|
|[AHglfvlj1P2w1uM8.htm](pathfinder-bestiary-2-items/AHglfvlj1P2w1uM8.htm)|Nightmare Tendril|auto-trad|
|[aHkhx52xZZTtPI25.htm](pathfinder-bestiary-2-items/aHkhx52xZZTtPI25.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[AhkuOBFHV5aW2vlI.htm](pathfinder-bestiary-2-items/AhkuOBFHV5aW2vlI.htm)|Grab|auto-trad|
|[Ai8J0A4OuhkDr7Cx.htm](pathfinder-bestiary-2-items/Ai8J0A4OuhkDr7Cx.htm)|Confusing Gaze|auto-trad|
|[AiGQomtSZwNKsTr9.htm](pathfinder-bestiary-2-items/AiGQomtSZwNKsTr9.htm)|Speedy Sabotage|auto-trad|
|[aisovzPKcYvcneL7.htm](pathfinder-bestiary-2-items/aisovzPKcYvcneL7.htm)|Throw Rock|auto-trad|
|[AJ6r1HXEEIIyvlnh.htm](pathfinder-bestiary-2-items/AJ6r1HXEEIIyvlnh.htm)|Regeneration 20 (Deactivated by Cold)|auto-trad|
|[AJ75IwgP2pUKWDa2.htm](pathfinder-bestiary-2-items/AJ75IwgP2pUKWDa2.htm)|Greater Darkvision|auto-trad|
|[aJhXMbTAQPFT4ySw.htm](pathfinder-bestiary-2-items/aJhXMbTAQPFT4ySw.htm)|Scurry|auto-trad|
|[aJhxWGibV6WOF98e.htm](pathfinder-bestiary-2-items/aJhxWGibV6WOF98e.htm)|Beckoning Call|auto-trad|
|[AjxbFK9De8rRPATh.htm](pathfinder-bestiary-2-items/AjxbFK9De8rRPATh.htm)|Darkvision|auto-trad|
|[AjxhxqAsiBgHNq9P.htm](pathfinder-bestiary-2-items/AjxhxqAsiBgHNq9P.htm)|Frightful Presence|auto-trad|
|[aKGDMqsWvw2cOIdE.htm](pathfinder-bestiary-2-items/aKGDMqsWvw2cOIdE.htm)|Power Attack|auto-trad|
|[AKHyZQZIqFBrYLLC.htm](pathfinder-bestiary-2-items/AKHyZQZIqFBrYLLC.htm)|Tilting Strike|auto-trad|
|[AKOKPl5zfFti3EZh.htm](pathfinder-bestiary-2-items/AKOKPl5zfFti3EZh.htm)|Moon Frenzy|auto-trad|
|[ALbbJNuSFbicqOfo.htm](pathfinder-bestiary-2-items/ALbbJNuSFbicqOfo.htm)|Ripping Disarm|auto-trad|
|[AldxbbHYC1u6qB4A.htm](pathfinder-bestiary-2-items/AldxbbHYC1u6qB4A.htm)|Vulnerable to Gentle Repose|auto-trad|
|[Am3HvCv3WfgNu9sm.htm](pathfinder-bestiary-2-items/Am3HvCv3WfgNu9sm.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[AmgNn4ZAhXxVpdtv.htm](pathfinder-bestiary-2-items/AmgNn4ZAhXxVpdtv.htm)|Jaws|auto-trad|
|[AMnTAvqDDSvhK4rN.htm](pathfinder-bestiary-2-items/AMnTAvqDDSvhK4rN.htm)|Salt Water Vulnerability|auto-trad|
|[aNHpJbgvbOZz2Kpc.htm](pathfinder-bestiary-2-items/aNHpJbgvbOZz2Kpc.htm)|Fist|auto-trad|
|[AoE6IxAfyxcUD4Jo.htm](pathfinder-bestiary-2-items/AoE6IxAfyxcUD4Jo.htm)|Fist|auto-trad|
|[aOFgYZJ7qXDOoWDH.htm](pathfinder-bestiary-2-items/aOFgYZJ7qXDOoWDH.htm)|Ghoul Fever|auto-trad|
|[AoPor6Ll5rYacPzT.htm](pathfinder-bestiary-2-items/AoPor6Ll5rYacPzT.htm)|Club|auto-trad|
|[APChG7D4rcYDlQ8m.htm](pathfinder-bestiary-2-items/APChG7D4rcYDlQ8m.htm)|Skrik Nettle Venom|auto-trad|
|[APd8zemC3kgQClDQ.htm](pathfinder-bestiary-2-items/APd8zemC3kgQClDQ.htm)|Darkvision|auto-trad|
|[aPrPuIOSdVOf8LwX.htm](pathfinder-bestiary-2-items/aPrPuIOSdVOf8LwX.htm)|Scent|auto-trad|
|[Aq9Qe7xGRm2JMoFA.htm](pathfinder-bestiary-2-items/Aq9Qe7xGRm2JMoFA.htm)|Low-Light Vision|auto-trad|
|[aQDe7We9BV7qqiIm.htm](pathfinder-bestiary-2-items/aQDe7We9BV7qqiIm.htm)|Envelop|auto-trad|
|[AQeKaKMvD6zIOMpq.htm](pathfinder-bestiary-2-items/AQeKaKMvD6zIOMpq.htm)|Attack of Opportunity (Tail Only)|auto-trad|
|[aQUfPBPzpqLlkRM0.htm](pathfinder-bestiary-2-items/aQUfPBPzpqLlkRM0.htm)|Stygian Inquisitor|auto-trad|
|[Ar1bSS3EJOZsaTMx.htm](pathfinder-bestiary-2-items/Ar1bSS3EJOZsaTMx.htm)|Jaws|auto-trad|
|[Ar58HUSjCtqUCBWd.htm](pathfinder-bestiary-2-items/Ar58HUSjCtqUCBWd.htm)|Jaws|auto-trad|
|[arLLxzC2PfddWyB8.htm](pathfinder-bestiary-2-items/arLLxzC2PfddWyB8.htm)|Whirlwind Blast|auto-trad|
|[ARmbgc05YhfVz4Vz.htm](pathfinder-bestiary-2-items/ARmbgc05YhfVz4Vz.htm)|Scent|auto-trad|
|[aRmGycM99xyVkKnJ.htm](pathfinder-bestiary-2-items/aRmGycM99xyVkKnJ.htm)|Caustic Blood|auto-trad|
|[aRTvrHip6K9YL4qq.htm](pathfinder-bestiary-2-items/aRTvrHip6K9YL4qq.htm)|Darkvision|auto-trad|
|[aSfa8TllpzF0cq1p.htm](pathfinder-bestiary-2-items/aSfa8TllpzF0cq1p.htm)|Jaws|auto-trad|
|[aSFQMPlhrSWqprUV.htm](pathfinder-bestiary-2-items/aSFQMPlhrSWqprUV.htm)|Tremorsense|auto-trad|
|[aT4z6HhNe6Rlop3y.htm](pathfinder-bestiary-2-items/aT4z6HhNe6Rlop3y.htm)|Tail|auto-trad|
|[atrOWDXVF87vS3KI.htm](pathfinder-bestiary-2-items/atrOWDXVF87vS3KI.htm)|Curse of the Crooked Cane|auto-trad|
|[aTTUtcIk2CfsuqEX.htm](pathfinder-bestiary-2-items/aTTUtcIk2CfsuqEX.htm)|Purity Vulnerability|auto-trad|
|[AtW9S7uW8rnbEEtY.htm](pathfinder-bestiary-2-items/AtW9S7uW8rnbEEtY.htm)|Darkvision|auto-trad|
|[AU14pBO1YZPWAEH5.htm](pathfinder-bestiary-2-items/AU14pBO1YZPWAEH5.htm)|Telepathy|auto-trad|
|[aUo4Cxd0YTiFEabx.htm](pathfinder-bestiary-2-items/aUo4Cxd0YTiFEabx.htm)|Fist|auto-trad|
|[AUp2Wnd4EQNf6jKb.htm](pathfinder-bestiary-2-items/AUp2Wnd4EQNf6jKb.htm)|Attack of Opportunity|auto-trad|
|[auQwzWmfz2NSN0Rk.htm](pathfinder-bestiary-2-items/auQwzWmfz2NSN0Rk.htm)|Swarm Mind|auto-trad|
|[AuRaykn2A42pUIob.htm](pathfinder-bestiary-2-items/AuRaykn2A42pUIob.htm)|Jaws|auto-trad|
|[avWzQnKIoQnTzxDr.htm](pathfinder-bestiary-2-items/avWzQnKIoQnTzxDr.htm)|Greater Darkvision|auto-trad|
|[AWGPMKkIerwV9Q8R.htm](pathfinder-bestiary-2-items/AWGPMKkIerwV9Q8R.htm)|Freezing Breath|auto-trad|
|[AWNveQn0EQ1aYORU.htm](pathfinder-bestiary-2-items/AWNveQn0EQ1aYORU.htm)|Rise Up|auto-trad|
|[aWo3aVa2XXkGsrLY.htm](pathfinder-bestiary-2-items/aWo3aVa2XXkGsrLY.htm)|Final Spite|auto-trad|
|[awrAfsHw9vT0Y612.htm](pathfinder-bestiary-2-items/awrAfsHw9vT0Y612.htm)|Grab|auto-trad|
|[Ax2F85HRXfPDtO6x.htm](pathfinder-bestiary-2-items/Ax2F85HRXfPDtO6x.htm)|At-Will Spells|auto-trad|
|[ax8IJyIaesSgfUVD.htm](pathfinder-bestiary-2-items/ax8IJyIaesSgfUVD.htm)|Draconic Frenzy|auto-trad|
|[aYcG8JHXY8UJvVcF.htm](pathfinder-bestiary-2-items/aYcG8JHXY8UJvVcF.htm)|Sticky Feet|auto-trad|
|[ayFG0wkvOvyHre59.htm](pathfinder-bestiary-2-items/ayFG0wkvOvyHre59.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[AySAWmXmOwHrjkuG.htm](pathfinder-bestiary-2-items/AySAWmXmOwHrjkuG.htm)|Vine|auto-trad|
|[AytjsrleVTlWADIi.htm](pathfinder-bestiary-2-items/AytjsrleVTlWADIi.htm)|Draining Presence|auto-trad|
|[AYVzOquWiUdDuNN3.htm](pathfinder-bestiary-2-items/AYVzOquWiUdDuNN3.htm)|Improved Grab|auto-trad|
|[azVkPps7BUkHzups.htm](pathfinder-bestiary-2-items/azVkPps7BUkHzups.htm)|Baleful Shriek|auto-trad|
|[B0ACGOBzm6iH5fdI.htm](pathfinder-bestiary-2-items/B0ACGOBzm6iH5fdI.htm)|Push|auto-trad|
|[B0vz4xw1pLJ9FeyD.htm](pathfinder-bestiary-2-items/B0vz4xw1pLJ9FeyD.htm)|Pounce|auto-trad|
|[B26U4dSQygOAKMgv.htm](pathfinder-bestiary-2-items/B26U4dSQygOAKMgv.htm)|Archon's Door|auto-trad|
|[B2rF8mguredueRbP.htm](pathfinder-bestiary-2-items/B2rF8mguredueRbP.htm)|Hoof|auto-trad|
|[b342tA8Gkde6f6zo.htm](pathfinder-bestiary-2-items/b342tA8Gkde6f6zo.htm)|Jaws|auto-trad|
|[B3HRE3VYFmesNO4i.htm](pathfinder-bestiary-2-items/B3HRE3VYFmesNO4i.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[B4lZzVAkZocFD4OC.htm](pathfinder-bestiary-2-items/B4lZzVAkZocFD4OC.htm)|Hoof|auto-trad|
|[b67N2gXUAWetXEVH.htm](pathfinder-bestiary-2-items/b67N2gXUAWetXEVH.htm)|Grab|auto-trad|
|[B6PM6Ak7BCncLxYT.htm](pathfinder-bestiary-2-items/B6PM6Ak7BCncLxYT.htm)|Trample|auto-trad|
|[B73QNqTjGdZQ9uba.htm](pathfinder-bestiary-2-items/B73QNqTjGdZQ9uba.htm)|Petrifying Glance|auto-trad|
|[B7bDAaNEMatWBkDD.htm](pathfinder-bestiary-2-items/B7bDAaNEMatWBkDD.htm)|Camouflage|auto-trad|
|[b7ci3cZP3HOyvnjf.htm](pathfinder-bestiary-2-items/b7ci3cZP3HOyvnjf.htm)|Constant Spells|auto-trad|
|[b9pe5dbiY9560pAA.htm](pathfinder-bestiary-2-items/b9pe5dbiY9560pAA.htm)|At-Will Spells|auto-trad|
|[b9sqjjGGkWII8xxu.htm](pathfinder-bestiary-2-items/b9sqjjGGkWII8xxu.htm)|Mimic Form|auto-trad|
|[BA1HIu1Y9Xs74B9s.htm](pathfinder-bestiary-2-items/BA1HIu1Y9Xs74B9s.htm)|Darkvision|auto-trad|
|[ba5N4O9vroJScmAq.htm](pathfinder-bestiary-2-items/ba5N4O9vroJScmAq.htm)|Misty Form|auto-trad|
|[BAJs3gJ34CxRZX5W.htm](pathfinder-bestiary-2-items/BAJs3gJ34CxRZX5W.htm)|Fair Competition|auto-trad|
|[BbeKEw6uMpYbSxue.htm](pathfinder-bestiary-2-items/BbeKEw6uMpYbSxue.htm)|Mandibles|auto-trad|
|[bBQMy1lr5xW6rBgj.htm](pathfinder-bestiary-2-items/bBQMy1lr5xW6rBgj.htm)|Temporal Strike|auto-trad|
|[BbzYprMJVagq4zOn.htm](pathfinder-bestiary-2-items/BbzYprMJVagq4zOn.htm)|Gust|auto-trad|
|[bC1xvgHpQUnix9Bt.htm](pathfinder-bestiary-2-items/bC1xvgHpQUnix9Bt.htm)|Deep Breath|auto-trad|
|[bcYwNYN6KGrhljcb.htm](pathfinder-bestiary-2-items/bcYwNYN6KGrhljcb.htm)|Darkvision|auto-trad|
|[BczEX9fZ2q5qXxKI.htm](pathfinder-bestiary-2-items/BczEX9fZ2q5qXxKI.htm)|Rock Tunneler|auto-trad|
|[BdJy1HqrIlkJp8rF.htm](pathfinder-bestiary-2-items/BdJy1HqrIlkJp8rF.htm)|Animated Hair|auto-trad|
|[BdObP5U0rM3c59uD.htm](pathfinder-bestiary-2-items/BdObP5U0rM3c59uD.htm)|Scent|auto-trad|
|[BdPJLN1AK4im8nIu.htm](pathfinder-bestiary-2-items/BdPJLN1AK4im8nIu.htm)|Breath Weapon|auto-trad|
|[bEAhPwJXnrqxPt5i.htm](pathfinder-bestiary-2-items/bEAhPwJXnrqxPt5i.htm)|Blend with Light|auto-trad|
|[bEnEcBaXcq0xrckT.htm](pathfinder-bestiary-2-items/bEnEcBaXcq0xrckT.htm)|Flawless Hearing|auto-trad|
|[bezkLxOASRaFHd9O.htm](pathfinder-bestiary-2-items/bezkLxOASRaFHd9O.htm)|Composite Longbow|auto-trad|
|[BfQDGpd8cSNGgENd.htm](pathfinder-bestiary-2-items/BfQDGpd8cSNGgENd.htm)|Retract Tongue|auto-trad|
|[bIfz2rvBHXjIjmyK.htm](pathfinder-bestiary-2-items/bIfz2rvBHXjIjmyK.htm)|Darkvision|auto-trad|
|[BIIDNJTjlW3lJ1b2.htm](pathfinder-bestiary-2-items/BIIDNJTjlW3lJ1b2.htm)|Volcanic Veins|auto-trad|
|[BIjOxLctW7PgHsOt.htm](pathfinder-bestiary-2-items/BIjOxLctW7PgHsOt.htm)|Compsognathus Venom|auto-trad|
|[bj3LBfUDYYqFHskT.htm](pathfinder-bestiary-2-items/bj3LBfUDYYqFHskT.htm)|Greater Darkvision|auto-trad|
|[BJelgf4pdEHXMd9u.htm](pathfinder-bestiary-2-items/BJelgf4pdEHXMd9u.htm)|Change Shape|auto-trad|
|[bJUTz62klSlUEuTo.htm](pathfinder-bestiary-2-items/bJUTz62klSlUEuTo.htm)|Drink Blood|auto-trad|
|[bJVD9JQb7YrlkdAs.htm](pathfinder-bestiary-2-items/bJVD9JQb7YrlkdAs.htm)|Void Child|auto-trad|
|[bJzQGcgwU0vilj1V.htm](pathfinder-bestiary-2-items/bJzQGcgwU0vilj1V.htm)|Culdewen's Curse|auto-trad|
|[bk1rrI0FiHN6P4an.htm](pathfinder-bestiary-2-items/bk1rrI0FiHN6P4an.htm)|Rapid Stinging|auto-trad|
|[Bk63QeRIxXqBysrQ.htm](pathfinder-bestiary-2-items/Bk63QeRIxXqBysrQ.htm)|At-Will Spells|auto-trad|
|[bKdGH4QNZyOm2rhg.htm](pathfinder-bestiary-2-items/bKdGH4QNZyOm2rhg.htm)|Land the Fish|auto-trad|
|[bKkmonWLmkkxYnh2.htm](pathfinder-bestiary-2-items/bKkmonWLmkkxYnh2.htm)|Camouflage|auto-trad|
|[BkLQtw75VewmqYSQ.htm](pathfinder-bestiary-2-items/BkLQtw75VewmqYSQ.htm)|Jaws|auto-trad|
|[BkmdLBAIea36UOWC.htm](pathfinder-bestiary-2-items/BkmdLBAIea36UOWC.htm)|Darkvision|auto-trad|
|[BKpd3Xf4hwQIvXYT.htm](pathfinder-bestiary-2-items/BKpd3Xf4hwQIvXYT.htm)|Darkvision|auto-trad|
|[bl1xW54BlfiFnU8h.htm](pathfinder-bestiary-2-items/bl1xW54BlfiFnU8h.htm)|Catch Rock|auto-trad|
|[bLGN7Biu5pB1bqtL.htm](pathfinder-bestiary-2-items/bLGN7Biu5pB1bqtL.htm)|Tongue|auto-trad|
|[BmhdDSnmoRYLlefv.htm](pathfinder-bestiary-2-items/BmhdDSnmoRYLlefv.htm)|Consumptive Aura|auto-trad|
|[bMLV80N79GMyKDyo.htm](pathfinder-bestiary-2-items/bMLV80N79GMyKDyo.htm)|Hand Crossbow (Serpentfolk Venom)|auto-trad|
|[BmSUiM1ndU9nWVml.htm](pathfinder-bestiary-2-items/BmSUiM1ndU9nWVml.htm)|Darkvision|auto-trad|
|[BMv1jkvT4dSbEL8X.htm](pathfinder-bestiary-2-items/BMv1jkvT4dSbEL8X.htm)|Greater Darkvision|auto-trad|
|[bMW69DQKTMLqLasp.htm](pathfinder-bestiary-2-items/bMW69DQKTMLqLasp.htm)|Rider's Bond|auto-trad|
|[bMX8ZvBxeu0Ty5cl.htm](pathfinder-bestiary-2-items/bMX8ZvBxeu0Ty5cl.htm)|Devour Soul|auto-trad|
|[bMypsTuAesTEDTll.htm](pathfinder-bestiary-2-items/bMypsTuAesTEDTll.htm)|Constant Spells|auto-trad|
|[bN7RCHuZpCHYpb2L.htm](pathfinder-bestiary-2-items/bN7RCHuZpCHYpb2L.htm)|Enraged Growth|auto-trad|
|[bNbPPpEgnT6EWV5B.htm](pathfinder-bestiary-2-items/bNbPPpEgnT6EWV5B.htm)|Claw|auto-trad|
|[BNs4e17E8TlmQdRh.htm](pathfinder-bestiary-2-items/BNs4e17E8TlmQdRh.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[Bo7Y34vNrq537uhA.htm](pathfinder-bestiary-2-items/Bo7Y34vNrq537uhA.htm)|Tail|auto-trad|
|[boC55Edo1J7Heynq.htm](pathfinder-bestiary-2-items/boC55Edo1J7Heynq.htm)|Darkvision|auto-trad|
|[bOe2DBNsKt2ZttfX.htm](pathfinder-bestiary-2-items/bOe2DBNsKt2ZttfX.htm)|Spectral Hand|auto-trad|
|[BOGJAY8I1abQRm7S.htm](pathfinder-bestiary-2-items/BOGJAY8I1abQRm7S.htm)|At-Will Spells|auto-trad|
|[boOcmJoGp5w8sRkS.htm](pathfinder-bestiary-2-items/boOcmJoGp5w8sRkS.htm)|Primal Innate Spells|auto-trad|
|[Bpm9xvzcp3dBpCvp.htm](pathfinder-bestiary-2-items/Bpm9xvzcp3dBpCvp.htm)|Sneak Attack|auto-trad|
|[bQ7xqBShE3D6pEMf.htm](pathfinder-bestiary-2-items/bQ7xqBShE3D6pEMf.htm)|Greater Darkvision|auto-trad|
|[BQDQsCBmg1gB3VSr.htm](pathfinder-bestiary-2-items/BQDQsCBmg1gB3VSr.htm)|Petrifying Gaze|auto-trad|
|[bR35AECKKuKfjL3N.htm](pathfinder-bestiary-2-items/bR35AECKKuKfjL3N.htm)|Constant Spells|auto-trad|
|[bR7DFykvg3BJRRmV.htm](pathfinder-bestiary-2-items/bR7DFykvg3BJRRmV.htm)|Dream Spider Venom|auto-trad|
|[BRAhQrI56OQkBhNy.htm](pathfinder-bestiary-2-items/BRAhQrI56OQkBhNy.htm)|Draconic Momentum|auto-trad|
|[bRIQtOJZdj1Z9gcD.htm](pathfinder-bestiary-2-items/bRIQtOJZdj1Z9gcD.htm)|Low-Light Vision|auto-trad|
|[brun3T7XF65PG2xu.htm](pathfinder-bestiary-2-items/brun3T7XF65PG2xu.htm)|Woodland Stride|auto-trad|
|[bS27ihuZ0Xs3mU7a.htm](pathfinder-bestiary-2-items/bS27ihuZ0Xs3mU7a.htm)|Swipe|auto-trad|
|[BsKlvqGTEEIQo1cx.htm](pathfinder-bestiary-2-items/BsKlvqGTEEIQo1cx.htm)|+4 Bonus on Perception to Detect Illusions|auto-trad|
|[BsxGfhkuyAGxetGI.htm](pathfinder-bestiary-2-items/BsxGfhkuyAGxetGI.htm)|Frightful Presence|auto-trad|
|[BtLt92JdieUMFygu.htm](pathfinder-bestiary-2-items/BtLt92JdieUMFygu.htm)|Regeneration 20 (Deactivated by Acid or Sonic)|auto-trad|
|[bu97rur2OngJhlHv.htm](pathfinder-bestiary-2-items/bu97rur2OngJhlHv.htm)|Titan Centipede Venom|auto-trad|
|[bW6X64ql7gjX9zqQ.htm](pathfinder-bestiary-2-items/bW6X64ql7gjX9zqQ.htm)|Entangling Tendrils|auto-trad|
|[bwZ7BYouujVO39kJ.htm](pathfinder-bestiary-2-items/bwZ7BYouujVO39kJ.htm)|Draconic Resistance|auto-trad|
|[bxjYZ892TLi9AizW.htm](pathfinder-bestiary-2-items/bxjYZ892TLi9AizW.htm)|Attack of Opportunity|auto-trad|
|[BXp3Iqh8AykJCF9i.htm](pathfinder-bestiary-2-items/BXp3Iqh8AykJCF9i.htm)|Talon|auto-trad|
|[ByGE43xowJglhHxP.htm](pathfinder-bestiary-2-items/ByGE43xowJglhHxP.htm)|Reactive Strikes|auto-trad|
|[bYqHLnt7oqthGfZi.htm](pathfinder-bestiary-2-items/bYqHLnt7oqthGfZi.htm)|Primal Innate Spells|auto-trad|
|[bysIJ8d5SoGqSABO.htm](pathfinder-bestiary-2-items/bysIJ8d5SoGqSABO.htm)|Implant|auto-trad|
|[bYuZrlYxVWLd0lD3.htm](pathfinder-bestiary-2-items/bYuZrlYxVWLd0lD3.htm)|Low-Light Vision|auto-trad|
|[BZfKFoVLNm5bmKHc.htm](pathfinder-bestiary-2-items/BZfKFoVLNm5bmKHc.htm)|Mimic Shadow|auto-trad|
|[c1COFBWgk0baYBNq.htm](pathfinder-bestiary-2-items/c1COFBWgk0baYBNq.htm)|Constant Spells|auto-trad|
|[c2nQF7qTMD3mwnrM.htm](pathfinder-bestiary-2-items/c2nQF7qTMD3mwnrM.htm)|Stingray Venom|auto-trad|
|[c4mDZBMrkh9kwIHn.htm](pathfinder-bestiary-2-items/c4mDZBMrkh9kwIHn.htm)|Negative Healing|auto-trad|
|[C4MfMpmGULlKKhXy.htm](pathfinder-bestiary-2-items/C4MfMpmGULlKKhXy.htm)|Greater Darkvision|auto-trad|
|[C6IiDSyc4X1neQwS.htm](pathfinder-bestiary-2-items/C6IiDSyc4X1neQwS.htm)|Darkvision|auto-trad|
|[C6LpIB1Gy5mjBk2G.htm](pathfinder-bestiary-2-items/C6LpIB1Gy5mjBk2G.htm)|Jaws|auto-trad|
|[c6VHX9kLiVuag6R0.htm](pathfinder-bestiary-2-items/c6VHX9kLiVuag6R0.htm)|Foot|auto-trad|
|[c74H6Oxpa5sZzsN5.htm](pathfinder-bestiary-2-items/c74H6Oxpa5sZzsN5.htm)|+4 Status to All Saves vs. Mental|auto-trad|
|[C7C9dLMFwNtAccbA.htm](pathfinder-bestiary-2-items/C7C9dLMFwNtAccbA.htm)|At-Will Spells|auto-trad|
|[c7V9HgCpZq3Unkja.htm](pathfinder-bestiary-2-items/c7V9HgCpZq3Unkja.htm)|Web|auto-trad|
|[C91ggah5Ye65LrMH.htm](pathfinder-bestiary-2-items/C91ggah5Ye65LrMH.htm)|Leng Ruby|auto-trad|
|[c9Aq0MMPKh2wKXXy.htm](pathfinder-bestiary-2-items/c9Aq0MMPKh2wKXXy.htm)|At-Will Spells|auto-trad|
|[CAKfIK6WRhkgpCW2.htm](pathfinder-bestiary-2-items/CAKfIK6WRhkgpCW2.htm)|Living Footsteps|auto-trad|
|[CApABKQSTABj5rMd.htm](pathfinder-bestiary-2-items/CApABKQSTABj5rMd.htm)|Occult Innate Spells|auto-trad|
|[cb6s0BQEA2EVpswg.htm](pathfinder-bestiary-2-items/cb6s0BQEA2EVpswg.htm)|Attack of Opportunity|auto-trad|
|[cCv1bOaozCwd4fB5.htm](pathfinder-bestiary-2-items/cCv1bOaozCwd4fB5.htm)|Claw|auto-trad|
|[CDAurAI8XPZBeHsL.htm](pathfinder-bestiary-2-items/CDAurAI8XPZBeHsL.htm)|Low-Light Vision|auto-trad|
|[CDI8zDOfYxM1bdM6.htm](pathfinder-bestiary-2-items/CDI8zDOfYxM1bdM6.htm)|Aura of Order's Ruin|auto-trad|
|[cDS4azJaDaRZITrQ.htm](pathfinder-bestiary-2-items/cDS4azJaDaRZITrQ.htm)|Cold Vulnerability|auto-trad|
|[cE9GHCJxhOHcgxgi.htm](pathfinder-bestiary-2-items/cE9GHCJxhOHcgxgi.htm)|Shattering Ice|auto-trad|
|[CeExWr00cFSdrZ4J.htm](pathfinder-bestiary-2-items/CeExWr00cFSdrZ4J.htm)|Buck|auto-trad|
|[CEREyXabBiMwUJlQ.htm](pathfinder-bestiary-2-items/CEREyXabBiMwUJlQ.htm)|Push|auto-trad|
|[CFncnCOk0MBxjpim.htm](pathfinder-bestiary-2-items/CFncnCOk0MBxjpim.htm)|Divine Innate Spells|auto-trad|
|[CGd3B4KwGynCutU4.htm](pathfinder-bestiary-2-items/CGd3B4KwGynCutU4.htm)|Vulnerable to Shape Wood|auto-trad|
|[cGXZfGlgZYjzMBP8.htm](pathfinder-bestiary-2-items/cGXZfGlgZYjzMBP8.htm)|Tremorsense|auto-trad|
|[cHiKqfceiHjpXqNg.htm](pathfinder-bestiary-2-items/cHiKqfceiHjpXqNg.htm)|Constant Spells|auto-trad|
|[chnUQOapjPm4tXAS.htm](pathfinder-bestiary-2-items/chnUQOapjPm4tXAS.htm)|Calming Presence|auto-trad|
|[ChobeFNcesw2DX01.htm](pathfinder-bestiary-2-items/ChobeFNcesw2DX01.htm)|Fangs|auto-trad|
|[CHpqAmXq4vOr2TMj.htm](pathfinder-bestiary-2-items/CHpqAmXq4vOr2TMj.htm)|Occult Innate Spells|auto-trad|
|[chTQW4y1K5vGGjVn.htm](pathfinder-bestiary-2-items/chTQW4y1K5vGGjVn.htm)|Foot|auto-trad|
|[CiQKBNNHPATwPDUX.htm](pathfinder-bestiary-2-items/CiQKBNNHPATwPDUX.htm)|Shortsword|auto-trad|
|[cJ2jYiJRZR43AJCJ.htm](pathfinder-bestiary-2-items/cJ2jYiJRZR43AJCJ.htm)|Double Slash|auto-trad|
|[Cj8UIWLOhEQ5yNgA.htm](pathfinder-bestiary-2-items/Cj8UIWLOhEQ5yNgA.htm)|Body Strike|auto-trad|
|[cJf7dfR7gJ442K2d.htm](pathfinder-bestiary-2-items/cJf7dfR7gJ442K2d.htm)|Occult Innate Spells|auto-trad|
|[cJFgwvR7wtsldJBw.htm](pathfinder-bestiary-2-items/cJFgwvR7wtsldJBw.htm)|Darkvision|auto-trad|
|[cjFyZppcDxVUaRJD.htm](pathfinder-bestiary-2-items/cjFyZppcDxVUaRJD.htm)|Drink Blood|auto-trad|
|[cjXnGrEMkDDki8sC.htm](pathfinder-bestiary-2-items/cjXnGrEMkDDki8sC.htm)|Occult Innate Spells|auto-trad|
|[cjYLiElA3muRcJuo.htm](pathfinder-bestiary-2-items/cjYLiElA3muRcJuo.htm)|Grotesque Gift|auto-trad|
|[CkFeQyjFwtsMRJY8.htm](pathfinder-bestiary-2-items/CkFeQyjFwtsMRJY8.htm)|Darkvision|auto-trad|
|[ckpzKVXKEEwbtEV4.htm](pathfinder-bestiary-2-items/ckpzKVXKEEwbtEV4.htm)|Greater Darkvision|auto-trad|
|[Cl2tanuHvlyyksIz.htm](pathfinder-bestiary-2-items/Cl2tanuHvlyyksIz.htm)|Frightful Presence|auto-trad|
|[cLUNjff4ATgKuIj8.htm](pathfinder-bestiary-2-items/cLUNjff4ATgKuIj8.htm)|Wicked Bite|auto-trad|
|[cLWUZTt9knGddxzH.htm](pathfinder-bestiary-2-items/cLWUZTt9knGddxzH.htm)|Constant Spells|auto-trad|
|[clxrPF7TPJ4sS2A6.htm](pathfinder-bestiary-2-items/clxrPF7TPJ4sS2A6.htm)|Extend Mandibles|auto-trad|
|[CmBRVSLQzMzJ4WUR.htm](pathfinder-bestiary-2-items/CmBRVSLQzMzJ4WUR.htm)|Lifesense 120 feet|auto-trad|
|[cMrE7oz3K11aJZtC.htm](pathfinder-bestiary-2-items/cMrE7oz3K11aJZtC.htm)|Glimpse of Stolen Flesh|auto-trad|
|[CNilhCQNrj60lElM.htm](pathfinder-bestiary-2-items/CNilhCQNrj60lElM.htm)|Beak|auto-trad|
|[cNnj1mdRc6n47fUX.htm](pathfinder-bestiary-2-items/cNnj1mdRc6n47fUX.htm)|Darkvision|auto-trad|
|[coCCw9uVbyTHhKfl.htm](pathfinder-bestiary-2-items/coCCw9uVbyTHhKfl.htm)|Jaws|auto-trad|
|[cOypeEDeaznFsSdO.htm](pathfinder-bestiary-2-items/cOypeEDeaznFsSdO.htm)|Ravenous Attack|auto-trad|
|[Cp1loMnqFXw0uGPS.htm](pathfinder-bestiary-2-items/Cp1loMnqFXw0uGPS.htm)|Jaws|auto-trad|
|[cPD60wC9FBtIdzPL.htm](pathfinder-bestiary-2-items/cPD60wC9FBtIdzPL.htm)|Head Regrowth|auto-trad|
|[CpDQG0xz9auLn9gr.htm](pathfinder-bestiary-2-items/CpDQG0xz9auLn9gr.htm)|Double Punch|auto-trad|
|[cpSC9RcShIFK28EU.htm](pathfinder-bestiary-2-items/cpSC9RcShIFK28EU.htm)|Snout|auto-trad|
|[CpTRDv42ZCA5jHZL.htm](pathfinder-bestiary-2-items/CpTRDv42ZCA5jHZL.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[CpVMSme8RWwiT0UY.htm](pathfinder-bestiary-2-items/CpVMSme8RWwiT0UY.htm)|Fast Healing 2 (in boiling water or steam)|auto-trad|
|[CQ9x9wMLcGSsM5wK.htm](pathfinder-bestiary-2-items/CQ9x9wMLcGSsM5wK.htm)|Web|auto-trad|
|[cQH5uYYBW0hseijL.htm](pathfinder-bestiary-2-items/cQH5uYYBW0hseijL.htm)|Darkvision|auto-trad|
|[cqI7kqooduGDEqz6.htm](pathfinder-bestiary-2-items/cqI7kqooduGDEqz6.htm)|Warhammer|auto-trad|
|[cQTIij6l7HjJwqQW.htm](pathfinder-bestiary-2-items/cQTIij6l7HjJwqQW.htm)|Telepathy|auto-trad|
|[CquGR1meZf8Szzmo.htm](pathfinder-bestiary-2-items/CquGR1meZf8Szzmo.htm)|Splinter|auto-trad|
|[CR1qaOcpKevyIuQj.htm](pathfinder-bestiary-2-items/CR1qaOcpKevyIuQj.htm)|Darkvision|auto-trad|
|[cRTNLnVuzisUG7kl.htm](pathfinder-bestiary-2-items/cRTNLnVuzisUG7kl.htm)|Improved Knockdown|auto-trad|
|[CRuPtzSYQoeDAWbH.htm](pathfinder-bestiary-2-items/CRuPtzSYQoeDAWbH.htm)|Swarm Mind|auto-trad|
|[cS4RQnXm1y1xugCt.htm](pathfinder-bestiary-2-items/cS4RQnXm1y1xugCt.htm)|Claw|auto-trad|
|[CSHQvYrfiR0GmczO.htm](pathfinder-bestiary-2-items/CSHQvYrfiR0GmczO.htm)|Brine Spit|auto-trad|
|[CsNzwFbWatN4ivRP.htm](pathfinder-bestiary-2-items/CsNzwFbWatN4ivRP.htm)|Hurl Weapon|auto-trad|
|[CsrgxIJmjkumjsBJ.htm](pathfinder-bestiary-2-items/CsrgxIJmjkumjsBJ.htm)|Trample|auto-trad|
|[CSSsFGyexhD5YUX3.htm](pathfinder-bestiary-2-items/CSSsFGyexhD5YUX3.htm)|Darkvision|auto-trad|
|[CswqTh16aa0KDlDF.htm](pathfinder-bestiary-2-items/CswqTh16aa0KDlDF.htm)|Necrophidic Paralysis|auto-trad|
|[CsYqT7LTtX34B3Yz.htm](pathfinder-bestiary-2-items/CsYqT7LTtX34B3Yz.htm)|Breath Weapon|auto-trad|
|[CTeYc8gFkSPfWH7F.htm](pathfinder-bestiary-2-items/CTeYc8gFkSPfWH7F.htm)|Jaws|auto-trad|
|[CThMS3QhSA3Obh7Z.htm](pathfinder-bestiary-2-items/CThMS3QhSA3Obh7Z.htm)|Breath Weapon|auto-trad|
|[Ctng3iJDIMkFh490.htm](pathfinder-bestiary-2-items/Ctng3iJDIMkFh490.htm)|Tail|auto-trad|
|[ctRDT8yPATnd3gxe.htm](pathfinder-bestiary-2-items/ctRDT8yPATnd3gxe.htm)|Primal Innate Spells|auto-trad|
|[cttVijuQGlQJk5Ux.htm](pathfinder-bestiary-2-items/cttVijuQGlQJk5Ux.htm)|Grab|auto-trad|
|[CUcADH9CrR1t2kkL.htm](pathfinder-bestiary-2-items/CUcADH9CrR1t2kkL.htm)|Darkvision|auto-trad|
|[cuHB5zR7MVWma2MD.htm](pathfinder-bestiary-2-items/cuHB5zR7MVWma2MD.htm)|Cloud Form|auto-trad|
|[CUl4eRbkbnzMJ2kZ.htm](pathfinder-bestiary-2-items/CUl4eRbkbnzMJ2kZ.htm)|Vulnerable to Endure Elements|auto-trad|
|[CuMZTwnGZC3P7sm5.htm](pathfinder-bestiary-2-items/CuMZTwnGZC3P7sm5.htm)|Spiked Tail|auto-trad|
|[cUtNYZ935fPsgj8x.htm](pathfinder-bestiary-2-items/cUtNYZ935fPsgj8x.htm)|Brine Spit|auto-trad|
|[cVEH8toDyk2cQnEC.htm](pathfinder-bestiary-2-items/cVEH8toDyk2cQnEC.htm)|Breath Weapon|auto-trad|
|[cvtGewKiksGkDNKz.htm](pathfinder-bestiary-2-items/cvtGewKiksGkDNKz.htm)|Draconic Frenzy|auto-trad|
|[CwEVrNGC8oHJQrv0.htm](pathfinder-bestiary-2-items/CwEVrNGC8oHJQrv0.htm)|Isqulugia|auto-trad|
|[cWj2CNZYq3EWCKZF.htm](pathfinder-bestiary-2-items/cWj2CNZYq3EWCKZF.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[CwjWGtOVvGZH0bP8.htm](pathfinder-bestiary-2-items/CwjWGtOVvGZH0bP8.htm)|Darkvision|auto-trad|
|[CwNuoWOgc2U8qCLd.htm](pathfinder-bestiary-2-items/CwNuoWOgc2U8qCLd.htm)|Knockdown|auto-trad|
|[CwSvR6GqNWvMMxwR.htm](pathfinder-bestiary-2-items/CwSvR6GqNWvMMxwR.htm)|Water Stride|auto-trad|
|[CwxYoKcoC0dSFuhX.htm](pathfinder-bestiary-2-items/CwxYoKcoC0dSFuhX.htm)|Mind-Numbing Touch|auto-trad|
|[CXc8oBEPlsp8HDTd.htm](pathfinder-bestiary-2-items/CXc8oBEPlsp8HDTd.htm)|Staff|auto-trad|
|[CxhKccyCVc1NDZFs.htm](pathfinder-bestiary-2-items/CxhKccyCVc1NDZFs.htm)|Greater Darkvision|auto-trad|
|[Cxm87AR1TJom2drt.htm](pathfinder-bestiary-2-items/Cxm87AR1TJom2drt.htm)|Double Claw|auto-trad|
|[CYtByJ7KaQiXTAWv.htm](pathfinder-bestiary-2-items/CYtByJ7KaQiXTAWv.htm)|Retributive Strike|auto-trad|
|[czBySBdDeH7WSORn.htm](pathfinder-bestiary-2-items/czBySBdDeH7WSORn.htm)|Spell Pilfer|auto-trad|
|[D0L9Y3ZIku07uMOk.htm](pathfinder-bestiary-2-items/D0L9Y3ZIku07uMOk.htm)|Fated|auto-trad|
|[D13sjvjuSrxFEe35.htm](pathfinder-bestiary-2-items/D13sjvjuSrxFEe35.htm)|No Breath|auto-trad|
|[D1QeVZDcMpO7hdmk.htm](pathfinder-bestiary-2-items/D1QeVZDcMpO7hdmk.htm)|Darkvision|auto-trad|
|[D1R03gCQbWQLLgtL.htm](pathfinder-bestiary-2-items/D1R03gCQbWQLLgtL.htm)|Splinter Volley|auto-trad|
|[d2h0zilfslOI1NGH.htm](pathfinder-bestiary-2-items/d2h0zilfslOI1NGH.htm)|Divine Innate Spells|auto-trad|
|[D3ePCbVgWGtm6c2e.htm](pathfinder-bestiary-2-items/D3ePCbVgWGtm6c2e.htm)|At-Will Spells|auto-trad|
|[D3z26xV27YGuSjtp.htm](pathfinder-bestiary-2-items/D3z26xV27YGuSjtp.htm)|Glaive|auto-trad|
|[d5CWT1AwKpYtsajX.htm](pathfinder-bestiary-2-items/d5CWT1AwKpYtsajX.htm)|Darkvision|auto-trad|
|[D5p47qiv8jIAcjoL.htm](pathfinder-bestiary-2-items/D5p47qiv8jIAcjoL.htm)|Claw|auto-trad|
|[d68HrWYDDAvuRFIR.htm](pathfinder-bestiary-2-items/d68HrWYDDAvuRFIR.htm)|Redirect Fire|auto-trad|
|[d7HHzvbGGdWbPEMo.htm](pathfinder-bestiary-2-items/d7HHzvbGGdWbPEMo.htm)|Claw|auto-trad|
|[d7lY0T9fiGJzH2O1.htm](pathfinder-bestiary-2-items/d7lY0T9fiGJzH2O1.htm)|Arcane Innate Spells|auto-trad|
|[D7nU92x03rn3cnoU.htm](pathfinder-bestiary-2-items/D7nU92x03rn3cnoU.htm)|Jaws|auto-trad|
|[D89GcjloR9CLNbq8.htm](pathfinder-bestiary-2-items/D89GcjloR9CLNbq8.htm)|Elemental Bulwark|auto-trad|
|[D8xygwrc0WQBOje8.htm](pathfinder-bestiary-2-items/D8xygwrc0WQBOje8.htm)|Negative Healing|auto-trad|
|[Da6b96ZUkpdT5y8i.htm](pathfinder-bestiary-2-items/Da6b96ZUkpdT5y8i.htm)|Reel In|auto-trad|
|[DA9qmvZn2NX8sUkj.htm](pathfinder-bestiary-2-items/DA9qmvZn2NX8sUkj.htm)|Jet|auto-trad|
|[Dax6qfY1KhJqryzV.htm](pathfinder-bestiary-2-items/Dax6qfY1KhJqryzV.htm)|Deep Breath|auto-trad|
|[dbDXIpmXMwGGSF0O.htm](pathfinder-bestiary-2-items/dbDXIpmXMwGGSF0O.htm)|Breath Weapon|auto-trad|
|[Dbrk210Q9t44P8vP.htm](pathfinder-bestiary-2-items/Dbrk210Q9t44P8vP.htm)|Claw|auto-trad|
|[DBzHso7FlJtYCeiU.htm](pathfinder-bestiary-2-items/DBzHso7FlJtYCeiU.htm)|At-Will Spells|auto-trad|
|[DC3068Ne4eQKeeW1.htm](pathfinder-bestiary-2-items/DC3068Ne4eQKeeW1.htm)|Create Spawn|auto-trad|
|[dCgNsIZTnOJTOkca.htm](pathfinder-bestiary-2-items/dCgNsIZTnOJTOkca.htm)|Telepathy|auto-trad|
|[DcQizfyqoLcJASk7.htm](pathfinder-bestiary-2-items/DcQizfyqoLcJASk7.htm)|Surprise Attacker|auto-trad|
|[DDI0cQeKx8SLhTUR.htm](pathfinder-bestiary-2-items/DDI0cQeKx8SLhTUR.htm)|Swift Swimmer|auto-trad|
|[de7GnRXapzBCdDoL.htm](pathfinder-bestiary-2-items/de7GnRXapzBCdDoL.htm)|Infuse Weapon|auto-trad|
|[de8ASlT2fs4snv5M.htm](pathfinder-bestiary-2-items/de8ASlT2fs4snv5M.htm)|Knockdown|auto-trad|
|[DEac5zIyNqdQYdnc.htm](pathfinder-bestiary-2-items/DEac5zIyNqdQYdnc.htm)|Grab|auto-trad|
|[dEH0bojR7ga9l8vX.htm](pathfinder-bestiary-2-items/dEH0bojR7ga9l8vX.htm)|Primal Prepared Spells|auto-trad|
|[dEiJsJoECS4hvDnB.htm](pathfinder-bestiary-2-items/dEiJsJoECS4hvDnB.htm)|Sneak Attack|auto-trad|
|[deytOmtiZjHFzGRU.htm](pathfinder-bestiary-2-items/deytOmtiZjHFzGRU.htm)|Athach Venom|auto-trad|
|[DEz7gcf0AumMmDO8.htm](pathfinder-bestiary-2-items/DEz7gcf0AumMmDO8.htm)|Primal Innate Spells|auto-trad|
|[dF7LLSCwf5DeGcNU.htm](pathfinder-bestiary-2-items/dF7LLSCwf5DeGcNU.htm)|Rend|auto-trad|
|[DfbcYHdJzDXVkgEs.htm](pathfinder-bestiary-2-items/DfbcYHdJzDXVkgEs.htm)|Swallow Whole|auto-trad|
|[DfefDz8ZxlBybpoa.htm](pathfinder-bestiary-2-items/DfefDz8ZxlBybpoa.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[dFKXMQ8FbHDHNhTC.htm](pathfinder-bestiary-2-items/dFKXMQ8FbHDHNhTC.htm)|Quick Bomber|auto-trad|
|[dfO2vkmoVzinZBdb.htm](pathfinder-bestiary-2-items/dfO2vkmoVzinZBdb.htm)|Constant Spells|auto-trad|
|[dFTh6F51ONxZqLHO.htm](pathfinder-bestiary-2-items/dFTh6F51ONxZqLHO.htm)|Draconic Frenzy|auto-trad|
|[DfyikbxEMRuzW77m.htm](pathfinder-bestiary-2-items/DfyikbxEMRuzW77m.htm)|Swarm Shape|auto-trad|
|[DFZPLEk0MQcqSi9v.htm](pathfinder-bestiary-2-items/DFZPLEk0MQcqSi9v.htm)|Primal Innate Spells|auto-trad|
|[dG6wdlMkywKsurgA.htm](pathfinder-bestiary-2-items/dG6wdlMkywKsurgA.htm)|Fist|auto-trad|
|[DI1RbgDDypEQJCMq.htm](pathfinder-bestiary-2-items/DI1RbgDDypEQJCMq.htm)|Steal Breath|auto-trad|
|[DJ4DzpxUnMokZPId.htm](pathfinder-bestiary-2-items/DJ4DzpxUnMokZPId.htm)|Attack of Opportunity (Tentacle Only)|auto-trad|
|[dj9YzVTLAtPASMHB.htm](pathfinder-bestiary-2-items/dj9YzVTLAtPASMHB.htm)|Creeping Cold|auto-trad|
|[djpxVgfWCno4NZwq.htm](pathfinder-bestiary-2-items/djpxVgfWCno4NZwq.htm)|Breath Weapon|auto-trad|
|[dkeTBVoAjjtnQkuW.htm](pathfinder-bestiary-2-items/dkeTBVoAjjtnQkuW.htm)|Scent|auto-trad|
|[dkgK1et0DFUYLpwT.htm](pathfinder-bestiary-2-items/dkgK1et0DFUYLpwT.htm)|Greater Darkvision|auto-trad|
|[dl10m47AKrgF3eVj.htm](pathfinder-bestiary-2-items/dl10m47AKrgF3eVj.htm)|Pour Down Throat|auto-trad|
|[DL16zUuUzuYySm31.htm](pathfinder-bestiary-2-items/DL16zUuUzuYySm31.htm)|Wing|auto-trad|
|[DLcmdOFICEUjArKs.htm](pathfinder-bestiary-2-items/DLcmdOFICEUjArKs.htm)|Jaws|auto-trad|
|[DlQoJTMEjgmsWgCa.htm](pathfinder-bestiary-2-items/DlQoJTMEjgmsWgCa.htm)|Mandibles|auto-trad|
|[DmYndlktCCjOahA2.htm](pathfinder-bestiary-2-items/DmYndlktCCjOahA2.htm)|Spiritual Warden|auto-trad|
|[dn95vUYkWbZHUAEc.htm](pathfinder-bestiary-2-items/dn95vUYkWbZHUAEc.htm)|Swallow Whole|auto-trad|
|[dNIO5C35HeSGtxDD.htm](pathfinder-bestiary-2-items/dNIO5C35HeSGtxDD.htm)|See Invisibility|auto-trad|
|[dO0urnF4xfwl5Yp8.htm](pathfinder-bestiary-2-items/dO0urnF4xfwl5Yp8.htm)|Ravenous Embrace|auto-trad|
|[dOcDYCNGkTD6GGcZ.htm](pathfinder-bestiary-2-items/dOcDYCNGkTD6GGcZ.htm)|Splinter Spray|auto-trad|
|[DOihMsQlvxGBJLxK.htm](pathfinder-bestiary-2-items/DOihMsQlvxGBJLxK.htm)|Coven Spells|auto-trad|
|[dOkhtnPU3mvJbRar.htm](pathfinder-bestiary-2-items/dOkhtnPU3mvJbRar.htm)|Tail|auto-trad|
|[DPcRdZIL38qcDj2I.htm](pathfinder-bestiary-2-items/DPcRdZIL38qcDj2I.htm)|Snatch|auto-trad|
|[DpIIIQSKrhsFKToW.htm](pathfinder-bestiary-2-items/DpIIIQSKrhsFKToW.htm)|Holy Beam|auto-trad|
|[dplDPboVsTPEngpW.htm](pathfinder-bestiary-2-items/dplDPboVsTPEngpW.htm)|Beak|auto-trad|
|[DQRNNHu5GqCVAno4.htm](pathfinder-bestiary-2-items/DQRNNHu5GqCVAno4.htm)|Fangs|auto-trad|
|[DQT6UjypWovf9CmL.htm](pathfinder-bestiary-2-items/DQT6UjypWovf9CmL.htm)|Darkvision|auto-trad|
|[drwHha4i2p4cqBa7.htm](pathfinder-bestiary-2-items/drwHha4i2p4cqBa7.htm)|Tail|auto-trad|
|[DryXaAfUqz1HN8lZ.htm](pathfinder-bestiary-2-items/DryXaAfUqz1HN8lZ.htm)|Fist|auto-trad|
|[dSkIJ0gsh7XlH2OA.htm](pathfinder-bestiary-2-items/dSkIJ0gsh7XlH2OA.htm)|Claw|auto-trad|
|[DsM9HKBiut2efUZZ.htm](pathfinder-bestiary-2-items/DsM9HKBiut2efUZZ.htm)|Swarming Slither|auto-trad|
|[DSxcWM211sMMf5q3.htm](pathfinder-bestiary-2-items/DSxcWM211sMMf5q3.htm)|Regurgitate Gastrolith|auto-trad|
|[Dsz0LBZEAi6Jkmmo.htm](pathfinder-bestiary-2-items/Dsz0LBZEAi6Jkmmo.htm)|Pincer|auto-trad|
|[DteZBAInF0ur5LIY.htm](pathfinder-bestiary-2-items/DteZBAInF0ur5LIY.htm)|Shape Flesh|auto-trad|
|[DtSPqabFqLrQDfGC.htm](pathfinder-bestiary-2-items/DtSPqabFqLrQDfGC.htm)|Low-Light Vision|auto-trad|
|[dTZURdZLg8n5enSz.htm](pathfinder-bestiary-2-items/dTZURdZLg8n5enSz.htm)|Lifesense|auto-trad|
|[dUfRUEqARe3yHUZM.htm](pathfinder-bestiary-2-items/dUfRUEqARe3yHUZM.htm)|Venom Spray|auto-trad|
|[DUJLW3KPZ4HNw8je.htm](pathfinder-bestiary-2-items/DUJLW3KPZ4HNw8je.htm)|+4 Status to Will Saves vs. Mental|auto-trad|
|[DuUso0h1BlAS0Ch3.htm](pathfinder-bestiary-2-items/DuUso0h1BlAS0Ch3.htm)|Darkvision|auto-trad|
|[DvGdYVJB2tQSEu99.htm](pathfinder-bestiary-2-items/DvGdYVJB2tQSEu99.htm)|Divine Innate Spells|auto-trad|
|[DVIJX5RWBUIXJUk5.htm](pathfinder-bestiary-2-items/DVIJX5RWBUIXJUk5.htm)|Scent|auto-trad|
|[DVQrLVPCYEJyvk5s.htm](pathfinder-bestiary-2-items/DVQrLVPCYEJyvk5s.htm)|Destructive Harmonics|auto-trad|
|[dW28Q4hMeEWpDtay.htm](pathfinder-bestiary-2-items/dW28Q4hMeEWpDtay.htm)|At-Will Spells|auto-trad|
|[dx1pnJnp1Gvp5XLN.htm](pathfinder-bestiary-2-items/dx1pnJnp1Gvp5XLN.htm)|Darkvision|auto-trad|
|[DxgiMoyx7wyltYqp.htm](pathfinder-bestiary-2-items/DxgiMoyx7wyltYqp.htm)|Drink Blood|auto-trad|
|[dXKhtDCdlXRIWq2O.htm](pathfinder-bestiary-2-items/dXKhtDCdlXRIWq2O.htm)|Fangs|auto-trad|
|[dydhZgaxaoJL5dXx.htm](pathfinder-bestiary-2-items/dydhZgaxaoJL5dXx.htm)|Aquatic Ambush|auto-trad|
|[dYdv3Ex4iHyirwUo.htm](pathfinder-bestiary-2-items/dYdv3Ex4iHyirwUo.htm)|Darkvision|auto-trad|
|[Dye4kplXn2HWokpZ.htm](pathfinder-bestiary-2-items/Dye4kplXn2HWokpZ.htm)|Constant Spells|auto-trad|
|[DyfwddYc7CkBo0HK.htm](pathfinder-bestiary-2-items/DyfwddYc7CkBo0HK.htm)|At-Will Spells|auto-trad|
|[dyis1hyzjJyWz9Sg.htm](pathfinder-bestiary-2-items/dyis1hyzjJyWz9Sg.htm)|Rock|auto-trad|
|[dynvtBdr79KGnqBo.htm](pathfinder-bestiary-2-items/dynvtBdr79KGnqBo.htm)|Can't Catch Me|auto-trad|
|[dYvQOwzgoNuS9cG8.htm](pathfinder-bestiary-2-items/dYvQOwzgoNuS9cG8.htm)|Body Thief|auto-trad|
|[dZoSOjCNPr4EIzGG.htm](pathfinder-bestiary-2-items/dZoSOjCNPr4EIzGG.htm)|Petrifying Glance|auto-trad|
|[E1FAcXClkBLXXYC9.htm](pathfinder-bestiary-2-items/E1FAcXClkBLXXYC9.htm)|Claw|auto-trad|
|[E283z5pYuHL8XdzA.htm](pathfinder-bestiary-2-items/E283z5pYuHL8XdzA.htm)|Primal Innate Spells|auto-trad|
|[e45FiBhbCxHP7Had.htm](pathfinder-bestiary-2-items/e45FiBhbCxHP7Had.htm)|Divine Innate Spells|auto-trad|
|[E4bINR3oniilC0ba.htm](pathfinder-bestiary-2-items/E4bINR3oniilC0ba.htm)|Shell Defense|auto-trad|
|[e7rgKN11dqX1fsSq.htm](pathfinder-bestiary-2-items/e7rgKN11dqX1fsSq.htm)|Focus Gaze|auto-trad|
|[E7WX10Amh46NUWCC.htm](pathfinder-bestiary-2-items/E7WX10Amh46NUWCC.htm)|Low-Light Vision|auto-trad|
|[E89TNZAj6eZb2Z9E.htm](pathfinder-bestiary-2-items/E89TNZAj6eZb2Z9E.htm)|Tentacle|auto-trad|
|[E8ICm8MwDDWyjgKR.htm](pathfinder-bestiary-2-items/E8ICm8MwDDWyjgKR.htm)|Rock|auto-trad|
|[e8r1Ct8F3Oee48fo.htm](pathfinder-bestiary-2-items/e8r1Ct8F3Oee48fo.htm)|Tremorsense 60 feet|auto-trad|
|[E9aF47rwjTOQ1OTC.htm](pathfinder-bestiary-2-items/E9aF47rwjTOQ1OTC.htm)|Scent|auto-trad|
|[E9d31Pl4TeWAIjWG.htm](pathfinder-bestiary-2-items/E9d31Pl4TeWAIjWG.htm)|At-Will Spells|auto-trad|
|[E9FVtTu2qyp5HgnY.htm](pathfinder-bestiary-2-items/E9FVtTu2qyp5HgnY.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[e9VfAq0loJF4o49U.htm](pathfinder-bestiary-2-items/e9VfAq0loJF4o49U.htm)|Throw Rock|auto-trad|
|[EaiT42eaTUXQMPIu.htm](pathfinder-bestiary-2-items/EaiT42eaTUXQMPIu.htm)|Fist|auto-trad|
|[EakALyMQUSW9bDgO.htm](pathfinder-bestiary-2-items/EakALyMQUSW9bDgO.htm)|Painsight|auto-trad|
|[earmbFXXQH89tgAG.htm](pathfinder-bestiary-2-items/earmbFXXQH89tgAG.htm)|Warhammer|auto-trad|
|[eaXk793zko55RRmG.htm](pathfinder-bestiary-2-items/eaXk793zko55RRmG.htm)|Icy Deflection|auto-trad|
|[eb6RXroslhq0fMd9.htm](pathfinder-bestiary-2-items/eb6RXroslhq0fMd9.htm)|Extra Reaction|auto-trad|
|[eBkXc5S4Spo7zZ3w.htm](pathfinder-bestiary-2-items/eBkXc5S4Spo7zZ3w.htm)|Darkvision|auto-trad|
|[EBnYEijsgTINPs3Y.htm](pathfinder-bestiary-2-items/EBnYEijsgTINPs3Y.htm)|Sickle|auto-trad|
|[eBR0R2mRoSAc5xwp.htm](pathfinder-bestiary-2-items/eBR0R2mRoSAc5xwp.htm)|Claw|auto-trad|
|[ed300vUjktB7yhD7.htm](pathfinder-bestiary-2-items/ed300vUjktB7yhD7.htm)|Fast Healing 5|auto-trad|
|[ED7Sb3O9SLxFnTRr.htm](pathfinder-bestiary-2-items/ED7Sb3O9SLxFnTRr.htm)|Holy Greatsword|auto-trad|
|[eDciSvI59Etta4Vh.htm](pathfinder-bestiary-2-items/eDciSvI59Etta4Vh.htm)|All-Around Vision|auto-trad|
|[edczs6WrO42Ttglo.htm](pathfinder-bestiary-2-items/edczs6WrO42Ttglo.htm)|Constrict|auto-trad|
|[edEJeTPVOOPPaCiE.htm](pathfinder-bestiary-2-items/edEJeTPVOOPPaCiE.htm)|Improved Grab|auto-trad|
|[EDGqAUdAUwkbCzQK.htm](pathfinder-bestiary-2-items/EDGqAUdAUwkbCzQK.htm)|Spew Mud|auto-trad|
|[eE4KRcdYFo35NqFR.htm](pathfinder-bestiary-2-items/eE4KRcdYFo35NqFR.htm)|Capsize|auto-trad|
|[EelpjpvvyRcCSTCg.htm](pathfinder-bestiary-2-items/EelpjpvvyRcCSTCg.htm)|Darkvision|auto-trad|
|[EEQTDYHn0GwNCqRL.htm](pathfinder-bestiary-2-items/EEQTDYHn0GwNCqRL.htm)|Retributive Strike|auto-trad|
|[eESVKEI54htLp14a.htm](pathfinder-bestiary-2-items/eESVKEI54htLp14a.htm)|Low-Light Vision|auto-trad|
|[EEVNlijtDn2A6Y9J.htm](pathfinder-bestiary-2-items/EEVNlijtDn2A6Y9J.htm)|At-Will Spells|auto-trad|
|[EF6jFpXwBUM3i2gf.htm](pathfinder-bestiary-2-items/EF6jFpXwBUM3i2gf.htm)|Fist|auto-trad|
|[eF8APed2EwYOr5dF.htm](pathfinder-bestiary-2-items/eF8APed2EwYOr5dF.htm)|Rupturing Venom|auto-trad|
|[eFiZ1wgGbxx9AVu1.htm](pathfinder-bestiary-2-items/eFiZ1wgGbxx9AVu1.htm)|Septic Malaria|auto-trad|
|[efL2PrxRnB9GLHmZ.htm](pathfinder-bestiary-2-items/efL2PrxRnB9GLHmZ.htm)|At-Will Spells|auto-trad|
|[eFL7EhhQB0m79mqu.htm](pathfinder-bestiary-2-items/eFL7EhhQB0m79mqu.htm)|Claw|auto-trad|
|[eFQH0YR5OMYZyD7n.htm](pathfinder-bestiary-2-items/eFQH0YR5OMYZyD7n.htm)|At-Will Spells|auto-trad|
|[EG42MTaDcKnzDi6h.htm](pathfinder-bestiary-2-items/EG42MTaDcKnzDi6h.htm)|Jaws|auto-trad|
|[eG5iE2SpiXdvquWh.htm](pathfinder-bestiary-2-items/eG5iE2SpiXdvquWh.htm)|Funereal Dirge|auto-trad|
|[EguLo1LUzA4CQ1Qi.htm](pathfinder-bestiary-2-items/EguLo1LUzA4CQ1Qi.htm)|Deep Breath|auto-trad|
|[EHjIsHVK3BSAzKZW.htm](pathfinder-bestiary-2-items/EHjIsHVK3BSAzKZW.htm)|Focus Gaze|auto-trad|
|[EhmJLz0h6EUzbywV.htm](pathfinder-bestiary-2-items/EhmJLz0h6EUzbywV.htm)|Haul Away|auto-trad|
|[ehOgYBBQ86ktUisv.htm](pathfinder-bestiary-2-items/ehOgYBBQ86ktUisv.htm)|Grab|auto-trad|
|[ehUknW9WZvN9eNjb.htm](pathfinder-bestiary-2-items/ehUknW9WZvN9eNjb.htm)|Darkvision|auto-trad|
|[Ehx0cPu5CtggKCGE.htm](pathfinder-bestiary-2-items/Ehx0cPu5CtggKCGE.htm)|Low-Light Vision|auto-trad|
|[eiefYuDeu60ebYAN.htm](pathfinder-bestiary-2-items/eiefYuDeu60ebYAN.htm)|+1 Circumstance Bonus on all Saving Throws|auto-trad|
|[EIhaPhUMiN88cQT6.htm](pathfinder-bestiary-2-items/EIhaPhUMiN88cQT6.htm)|Aura of Righteousness|auto-trad|
|[EILiYrpIKrSD8lNU.htm](pathfinder-bestiary-2-items/EILiYrpIKrSD8lNU.htm)|Bite|auto-trad|
|[eIV0ZcgzUZI0e8VM.htm](pathfinder-bestiary-2-items/eIV0ZcgzUZI0e8VM.htm)|Earth Glide|auto-trad|
|[ej0lyLnZuPKMNMnI.htm](pathfinder-bestiary-2-items/ej0lyLnZuPKMNMnI.htm)|Focused Flames|auto-trad|
|[ej30s3gZXMNhiGRr.htm](pathfinder-bestiary-2-items/ej30s3gZXMNhiGRr.htm)|Primal Innate Spells|auto-trad|
|[EjSQcYiJF0lhuXqk.htm](pathfinder-bestiary-2-items/EjSQcYiJF0lhuXqk.htm)|Occult Innate Spells|auto-trad|
|[EK9tHuUdNF8lzwwK.htm](pathfinder-bestiary-2-items/EK9tHuUdNF8lzwwK.htm)|Club|auto-trad|
|[eKrrD4BYO7xl9vPo.htm](pathfinder-bestiary-2-items/eKrrD4BYO7xl9vPo.htm)|Fearful Attack|auto-trad|
|[eKSzSgT3W8A9oNzs.htm](pathfinder-bestiary-2-items/eKSzSgT3W8A9oNzs.htm)|Multiple Opportunities|auto-trad|
|[El38bC5IzKKtBdcW.htm](pathfinder-bestiary-2-items/El38bC5IzKKtBdcW.htm)|Steal Voice|auto-trad|
|[ElDo5C9tGNaFEyUo.htm](pathfinder-bestiary-2-items/ElDo5C9tGNaFEyUo.htm)|Tail|auto-trad|
|[ELI8vuFBdrN99fE3.htm](pathfinder-bestiary-2-items/ELI8vuFBdrN99fE3.htm)|Tendril|auto-trad|
|[elLeiMASiyy7jPxz.htm](pathfinder-bestiary-2-items/elLeiMASiyy7jPxz.htm)|Spirit Touch|auto-trad|
|[eLovXAlbDKmZARNK.htm](pathfinder-bestiary-2-items/eLovXAlbDKmZARNK.htm)|Constant Spells|auto-trad|
|[eN9AmHu7Wb5ZVY5F.htm](pathfinder-bestiary-2-items/eN9AmHu7Wb5ZVY5F.htm)|Whiffling|auto-trad|
|[ENtN1odPFGnOKcBk.htm](pathfinder-bestiary-2-items/ENtN1odPFGnOKcBk.htm)|Telepathy|auto-trad|
|[Eo3e73BdyU4oMYEq.htm](pathfinder-bestiary-2-items/Eo3e73BdyU4oMYEq.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[eO5aWydhyXcR1nGE.htm](pathfinder-bestiary-2-items/eO5aWydhyXcR1nGE.htm)|Primal Innate Spells|auto-trad|
|[EO9iiH6RJCSv55DM.htm](pathfinder-bestiary-2-items/EO9iiH6RJCSv55DM.htm)|Root|auto-trad|
|[eohTEKJnqW3cF8U8.htm](pathfinder-bestiary-2-items/eohTEKJnqW3cF8U8.htm)|Force Bolt|auto-trad|
|[eOpB0SxDMEU65Oyy.htm](pathfinder-bestiary-2-items/eOpB0SxDMEU65Oyy.htm)|Blood Drain|auto-trad|
|[ephUPtGMB5sA3OWQ.htm](pathfinder-bestiary-2-items/ephUPtGMB5sA3OWQ.htm)|Tremorsense|auto-trad|
|[EPlZiO8molN6azkO.htm](pathfinder-bestiary-2-items/EPlZiO8molN6azkO.htm)|Spittle|auto-trad|
|[EPXehWRhcO5caYov.htm](pathfinder-bestiary-2-items/EPXehWRhcO5caYov.htm)|Shift Fate|auto-trad|
|[eQAeRO9fMIqYp05Q.htm](pathfinder-bestiary-2-items/eQAeRO9fMIqYp05Q.htm)|Sense Fate|auto-trad|
|[eQGUATVlHv8iZ97x.htm](pathfinder-bestiary-2-items/eQGUATVlHv8iZ97x.htm)|Weak Acid|auto-trad|
|[eqGZhn9lsZI6JDYE.htm](pathfinder-bestiary-2-items/eqGZhn9lsZI6JDYE.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Eqk7JKBf2mGkZbCf.htm](pathfinder-bestiary-2-items/Eqk7JKBf2mGkZbCf.htm)|Darkvision|auto-trad|
|[eQwYRUkwKCcTxqhO.htm](pathfinder-bestiary-2-items/eQwYRUkwKCcTxqhO.htm)|Divine Innate Spells|auto-trad|
|[Er19GkrGFluxc3FN.htm](pathfinder-bestiary-2-items/Er19GkrGFluxc3FN.htm)|At-Will Spells|auto-trad|
|[eR2U38j5LSvSk5t6.htm](pathfinder-bestiary-2-items/eR2U38j5LSvSk5t6.htm)|Attack of Opportunity|auto-trad|
|[eR8AFynWU8vfsTA9.htm](pathfinder-bestiary-2-items/eR8AFynWU8vfsTA9.htm)|Draconic Frenzy|auto-trad|
|[ErIhDSVTBd7aGHeC.htm](pathfinder-bestiary-2-items/ErIhDSVTBd7aGHeC.htm)|Stinger|auto-trad|
|[ERMHc3Xc7e0s0qGD.htm](pathfinder-bestiary-2-items/ERMHc3Xc7e0s0qGD.htm)|Withering Touch|auto-trad|
|[ERoazbmYgnuBSYsk.htm](pathfinder-bestiary-2-items/ERoazbmYgnuBSYsk.htm)|Xill Paralysis|auto-trad|
|[ERoYflv6bErPQNP2.htm](pathfinder-bestiary-2-items/ERoYflv6bErPQNP2.htm)|Tail|auto-trad|
|[ESiP5kSLjg2exhAh.htm](pathfinder-bestiary-2-items/ESiP5kSLjg2exhAh.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[esTHEOJBat1zXzXY.htm](pathfinder-bestiary-2-items/esTHEOJBat1zXzXY.htm)|Primal Innate Spells|auto-trad|
|[eTAHPb22Vfb1ZIIA.htm](pathfinder-bestiary-2-items/eTAHPb22Vfb1ZIIA.htm)|Grab|auto-trad|
|[eTTwVRhxdeVx0PRD.htm](pathfinder-bestiary-2-items/eTTwVRhxdeVx0PRD.htm)|Low-Light Vision|auto-trad|
|[eu0SQkCWm2vp3Yy6.htm](pathfinder-bestiary-2-items/eu0SQkCWm2vp3Yy6.htm)|Claw|auto-trad|
|[eUBZ8xXpWH2Gvg39.htm](pathfinder-bestiary-2-items/eUBZ8xXpWH2Gvg39.htm)|Kukri|auto-trad|
|[eugKdHe7dmAkjXP0.htm](pathfinder-bestiary-2-items/eugKdHe7dmAkjXP0.htm)|Witchflame Bolt|auto-trad|
|[EungEyymZcbKAl61.htm](pathfinder-bestiary-2-items/EungEyymZcbKAl61.htm)|Primal Innate Spells|auto-trad|
|[EuS6s6eoD5RfY7jI.htm](pathfinder-bestiary-2-items/EuS6s6eoD5RfY7jI.htm)|Cloud Walk|auto-trad|
|[Ev5luBuqFKW5s9MN.htm](pathfinder-bestiary-2-items/Ev5luBuqFKW5s9MN.htm)|Water Glide|auto-trad|
|[eVlxUKYnPRCGVP58.htm](pathfinder-bestiary-2-items/eVlxUKYnPRCGVP58.htm)|Wavesense|auto-trad|
|[EVtI5ReFhXPbGsa8.htm](pathfinder-bestiary-2-items/EVtI5ReFhXPbGsa8.htm)|Primal Innate Spells|auto-trad|
|[EW5LH6Dw5jEKEVOU.htm](pathfinder-bestiary-2-items/EW5LH6Dw5jEKEVOU.htm)|Draconic Momentum|auto-trad|
|[Ew7CBGn58KzAoHsQ.htm](pathfinder-bestiary-2-items/Ew7CBGn58KzAoHsQ.htm)|Bite|auto-trad|
|[eWih1h98kRb5uo3E.htm](pathfinder-bestiary-2-items/eWih1h98kRb5uo3E.htm)|Telepathy|auto-trad|
|[ewIhUHZqcxVqsG7x.htm](pathfinder-bestiary-2-items/ewIhUHZqcxVqsG7x.htm)|Dagger|auto-trad|
|[EWOsMDPiDiwsUVSL.htm](pathfinder-bestiary-2-items/EWOsMDPiDiwsUVSL.htm)|At-Will Spells|auto-trad|
|[ewPaxkwRS28GoetB.htm](pathfinder-bestiary-2-items/ewPaxkwRS28GoetB.htm)|Jaws|auto-trad|
|[EXLaDRArP4bsh7XH.htm](pathfinder-bestiary-2-items/EXLaDRArP4bsh7XH.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[ExsxFy6Bmcgsy0GX.htm](pathfinder-bestiary-2-items/ExsxFy6Bmcgsy0GX.htm)|Low-Light Vision|auto-trad|
|[eyEiaf2KwHBA5miS.htm](pathfinder-bestiary-2-items/eyEiaf2KwHBA5miS.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[EYtHIPWgwGTdjCBp.htm](pathfinder-bestiary-2-items/EYtHIPWgwGTdjCBp.htm)|Fascination of Flame|auto-trad|
|[ez4TlFP9ir8diYv7.htm](pathfinder-bestiary-2-items/ez4TlFP9ir8diYv7.htm)|Scuttle|auto-trad|
|[ezme3D83xTKiLkhR.htm](pathfinder-bestiary-2-items/ezme3D83xTKiLkhR.htm)|Final End|auto-trad|
|[eZpQaoruvmngTW6T.htm](pathfinder-bestiary-2-items/eZpQaoruvmngTW6T.htm)|Greater Darkvision|auto-trad|
|[eZt77JcDJLXdqusp.htm](pathfinder-bestiary-2-items/eZt77JcDJLXdqusp.htm)|Telepathy 100 feet|auto-trad|
|[f1adZO1FX8WkPN1h.htm](pathfinder-bestiary-2-items/f1adZO1FX8WkPN1h.htm)|Violet Rot|auto-trad|
|[F1orhEfNkIuGNlEh.htm](pathfinder-bestiary-2-items/F1orhEfNkIuGNlEh.htm)|Buck|auto-trad|
|[F2cUCIHpa06Qkqy2.htm](pathfinder-bestiary-2-items/F2cUCIHpa06Qkqy2.htm)|Squeeze|auto-trad|
|[F2mPoxLl7PTJpc3I.htm](pathfinder-bestiary-2-items/F2mPoxLl7PTJpc3I.htm)|All-Around Vision|auto-trad|
|[f3X4SLxmMSok3p65.htm](pathfinder-bestiary-2-items/f3X4SLxmMSok3p65.htm)|Fate Drain|auto-trad|
|[f4JPwXqfZM8fy4Zp.htm](pathfinder-bestiary-2-items/f4JPwXqfZM8fy4Zp.htm)|Absorb Force|auto-trad|
|[F4z6xO5BYtoag9hK.htm](pathfinder-bestiary-2-items/F4z6xO5BYtoag9hK.htm)|Jaws|auto-trad|
|[f61dlQmeP2vZs1Go.htm](pathfinder-bestiary-2-items/f61dlQmeP2vZs1Go.htm)|Darkvision|auto-trad|
|[F7GzmZlJ6PXy8Cm5.htm](pathfinder-bestiary-2-items/F7GzmZlJ6PXy8Cm5.htm)|Smoke Vision|auto-trad|
|[f7niepbS8VyknoBK.htm](pathfinder-bestiary-2-items/f7niepbS8VyknoBK.htm)|Scintillating Aura|auto-trad|
|[fakAfk3puTMpw1vT.htm](pathfinder-bestiary-2-items/fakAfk3puTMpw1vT.htm)|Slow Metabolism|auto-trad|
|[FapwAUPhHeXc1NKj.htm](pathfinder-bestiary-2-items/FapwAUPhHeXc1NKj.htm)|Warlord's Training|auto-trad|
|[FBiCgn8XESVcHSPx.htm](pathfinder-bestiary-2-items/FBiCgn8XESVcHSPx.htm)|Jaws|auto-trad|
|[FBO25NSysfQagiQl.htm](pathfinder-bestiary-2-items/FBO25NSysfQagiQl.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[fBpvD0xb43MGDESG.htm](pathfinder-bestiary-2-items/fBpvD0xb43MGDESG.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[FBYVc1nHfCqd2hLL.htm](pathfinder-bestiary-2-items/FBYVc1nHfCqd2hLL.htm)|Morningstar|auto-trad|
|[Fc0hTwlWHz8gcaaN.htm](pathfinder-bestiary-2-items/Fc0hTwlWHz8gcaaN.htm)|Tail|auto-trad|
|[FcafMwq1Bll5o21K.htm](pathfinder-bestiary-2-items/FcafMwq1Bll5o21K.htm)|Deep Breath|auto-trad|
|[fcCuiTHr7ed8LhqK.htm](pathfinder-bestiary-2-items/fcCuiTHr7ed8LhqK.htm)|Fangs|auto-trad|
|[FcLtb9IKlH2X8wBl.htm](pathfinder-bestiary-2-items/FcLtb9IKlH2X8wBl.htm)|Darkvision|auto-trad|
|[FcnwHTEpIdO7oCXf.htm](pathfinder-bestiary-2-items/FcnwHTEpIdO7oCXf.htm)|Darkvision|auto-trad|
|[Fd4JoDpXliyIpcvN.htm](pathfinder-bestiary-2-items/Fd4JoDpXliyIpcvN.htm)|Horn|auto-trad|
|[fDEMKsDuC7SNDbx6.htm](pathfinder-bestiary-2-items/fDEMKsDuC7SNDbx6.htm)|Unbodied Possession|auto-trad|
|[fDOQq8mfa2PhlHcF.htm](pathfinder-bestiary-2-items/fDOQq8mfa2PhlHcF.htm)|Bore into Brain|auto-trad|
|[FdZZPAWhuFULerJd.htm](pathfinder-bestiary-2-items/FdZZPAWhuFULerJd.htm)|Trumpet Blast|auto-trad|
|[fEKfgiJvpDVUGTI9.htm](pathfinder-bestiary-2-items/fEKfgiJvpDVUGTI9.htm)|Occult Innate Spells|auto-trad|
|[fFFM3BNDIa0yq0lg.htm](pathfinder-bestiary-2-items/fFFM3BNDIa0yq0lg.htm)|Regeneration 20 (Deactivated by Good or Silver)|auto-trad|
|[FFRvTDGH1FEnAKjH.htm](pathfinder-bestiary-2-items/FFRvTDGH1FEnAKjH.htm)|Splintering Death|auto-trad|
|[ffxbLuQioRQZCSba.htm](pathfinder-bestiary-2-items/ffxbLuQioRQZCSba.htm)|Darkvision|auto-trad|
|[fg8mmyOMmhcaaO4s.htm](pathfinder-bestiary-2-items/fg8mmyOMmhcaaO4s.htm)|Fast Healing 2 (when touching mud or slime)|auto-trad|
|[FH5MdxzW5lzQE9Hf.htm](pathfinder-bestiary-2-items/FH5MdxzW5lzQE9Hf.htm)|Regeneration 7 (deactivated by acid or fire)|auto-trad|
|[fHcej8vlhHnCFoab.htm](pathfinder-bestiary-2-items/fHcej8vlhHnCFoab.htm)|Tremorsense|auto-trad|
|[FHQiK6c7jZUggMXg.htm](pathfinder-bestiary-2-items/FHQiK6c7jZUggMXg.htm)|Darkvision|auto-trad|
|[FhV61jMMGaGgOnmc.htm](pathfinder-bestiary-2-items/FhV61jMMGaGgOnmc.htm)|Divine Innate Spells|auto-trad|
|[FhwufRLfw0dtrKFI.htm](pathfinder-bestiary-2-items/FhwufRLfw0dtrKFI.htm)|Kneecapper|auto-trad|
|[FHyqgRwAZouh37Gw.htm](pathfinder-bestiary-2-items/FHyqgRwAZouh37Gw.htm)|Fangs|auto-trad|
|[FIBgwJl6ghhZJB6Y.htm](pathfinder-bestiary-2-items/FIBgwJl6ghhZJB6Y.htm)|Quetz Couatl Venom|auto-trad|
|[fiJwhvqSCYbkgaJg.htm](pathfinder-bestiary-2-items/fiJwhvqSCYbkgaJg.htm)|Swear|auto-trad|
|[fiKhFiynAYN8X5if.htm](pathfinder-bestiary-2-items/fiKhFiynAYN8X5if.htm)|Gemsight|auto-trad|
|[filzxUgupqvL1ZQq.htm](pathfinder-bestiary-2-items/filzxUgupqvL1ZQq.htm)|Gouging Lunge|auto-trad|
|[FjdhY5Rvi0x7AdX0.htm](pathfinder-bestiary-2-items/FjdhY5Rvi0x7AdX0.htm)|Darkvision|auto-trad|
|[fJJc56GDg0EB3oit.htm](pathfinder-bestiary-2-items/fJJc56GDg0EB3oit.htm)|Greater Darkvision|auto-trad|
|[fJwNiBKQ1mE1NNbz.htm](pathfinder-bestiary-2-items/fJwNiBKQ1mE1NNbz.htm)|Dual Tusks|auto-trad|
|[FKd8OWhPrCEcBKFC.htm](pathfinder-bestiary-2-items/FKd8OWhPrCEcBKFC.htm)|Darkvision|auto-trad|
|[fKeX8OGCBc8j0eJu.htm](pathfinder-bestiary-2-items/fKeX8OGCBc8j0eJu.htm)|Low-Light Vision|auto-trad|
|[FKgN8Ya6AS8vCOry.htm](pathfinder-bestiary-2-items/FKgN8Ya6AS8vCOry.htm)|Darkvision|auto-trad|
|[FKpHGfQVea81uMo2.htm](pathfinder-bestiary-2-items/FKpHGfQVea81uMo2.htm)|Motionsense|auto-trad|
|[fKqimVsLOPsE2xN0.htm](pathfinder-bestiary-2-items/fKqimVsLOPsE2xN0.htm)|Menace to Magic|auto-trad|
|[FlbbYL5EQllXYKjc.htm](pathfinder-bestiary-2-items/FlbbYL5EQllXYKjc.htm)|Susceptible to Desiccation|auto-trad|
|[flCrHBNNesdvv5af.htm](pathfinder-bestiary-2-items/flCrHBNNesdvv5af.htm)|Attack of Opportunity|auto-trad|
|[fm5cOYL11qhzHV3G.htm](pathfinder-bestiary-2-items/fm5cOYL11qhzHV3G.htm)|Low-Light Vision|auto-trad|
|[fM7GchX13PhXHMdp.htm](pathfinder-bestiary-2-items/fM7GchX13PhXHMdp.htm)|Divine Innate Spells|auto-trad|
|[fmHvsMYoy6LDxLkK.htm](pathfinder-bestiary-2-items/fmHvsMYoy6LDxLkK.htm)|Tentacle Mouth|auto-trad|
|[FN5QndPNQPuzc0V3.htm](pathfinder-bestiary-2-items/FN5QndPNQPuzc0V3.htm)|Coven Spells|auto-trad|
|[FNBiiooVEkzVDOTE.htm](pathfinder-bestiary-2-items/FNBiiooVEkzVDOTE.htm)|Regeneration 10 (Deactivated by Good or Silver)|auto-trad|
|[FnRuOLk9xVqNWtrD.htm](pathfinder-bestiary-2-items/FnRuOLk9xVqNWtrD.htm)|Constant Spells|auto-trad|
|[FNSLMfCQHDGPvjsv.htm](pathfinder-bestiary-2-items/FNSLMfCQHDGPvjsv.htm)|Sudden Retreat|auto-trad|
|[FnwUcB0l68viXYtQ.htm](pathfinder-bestiary-2-items/FnwUcB0l68viXYtQ.htm)|Fish Hook|auto-trad|
|[fO8E1sNI1DHDU5Dq.htm](pathfinder-bestiary-2-items/fO8E1sNI1DHDU5Dq.htm)|Motion Sense 60 feet|auto-trad|
|[foIpp0GPCbsHL0dl.htm](pathfinder-bestiary-2-items/foIpp0GPCbsHL0dl.htm)|Cave Scorpion Venom|auto-trad|
|[fon9lS38svdiFHZJ.htm](pathfinder-bestiary-2-items/fon9lS38svdiFHZJ.htm)|Greedy Grab|auto-trad|
|[Fp4obYTKAlnrpeFi.htm](pathfinder-bestiary-2-items/Fp4obYTKAlnrpeFi.htm)|Change Shape|auto-trad|
|[FpcxdJ4oqnyX8sBs.htm](pathfinder-bestiary-2-items/FpcxdJ4oqnyX8sBs.htm)|Long Neck|auto-trad|
|[fPGmCfWuF8oqK8hX.htm](pathfinder-bestiary-2-items/fPGmCfWuF8oqK8hX.htm)|Otherworldly Laugh|auto-trad|
|[FqRKnuudm93xTutr.htm](pathfinder-bestiary-2-items/FqRKnuudm93xTutr.htm)|At-Will Spells|auto-trad|
|[fqXSW2GPcFXzcz2P.htm](pathfinder-bestiary-2-items/fqXSW2GPcFXzcz2P.htm)|Constant Spells|auto-trad|
|[FQZGypLzxjxM8v3a.htm](pathfinder-bestiary-2-items/FQZGypLzxjxM8v3a.htm)|Black Flame Knives|auto-trad|
|[FrDnLG8ehjXFC6Ey.htm](pathfinder-bestiary-2-items/FrDnLG8ehjXFC6Ey.htm)|Attack of Opportunity|auto-trad|
|[frIks1GEvtVQAPPw.htm](pathfinder-bestiary-2-items/frIks1GEvtVQAPPw.htm)|Vine|auto-trad|
|[FrTdGO6BqbRvBLu9.htm](pathfinder-bestiary-2-items/FrTdGO6BqbRvBLu9.htm)|Fangs|auto-trad|
|[fRtY0qdhDDIRBRWj.htm](pathfinder-bestiary-2-items/fRtY0qdhDDIRBRWj.htm)|Fist|auto-trad|
|[fRuyuldjZkDBFASw.htm](pathfinder-bestiary-2-items/fRuyuldjZkDBFASw.htm)|Arcane Innate Spells|auto-trad|
|[FsgtuZo9KF7fxlp5.htm](pathfinder-bestiary-2-items/FsgtuZo9KF7fxlp5.htm)|Jaws|auto-trad|
|[FTeRIF51hvaZAOzR.htm](pathfinder-bestiary-2-items/FTeRIF51hvaZAOzR.htm)|Smoke Vision|auto-trad|
|[FTil5NFptMbuNK7y.htm](pathfinder-bestiary-2-items/FTil5NFptMbuNK7y.htm)|Dagger|auto-trad|
|[ftJSskhqjZaMBNyZ.htm](pathfinder-bestiary-2-items/ftJSskhqjZaMBNyZ.htm)|Jet|auto-trad|
|[Fu1B7hcvnk9S79DA.htm](pathfinder-bestiary-2-items/Fu1B7hcvnk9S79DA.htm)|Commander's Aura|auto-trad|
|[FUbGjkLswPM3bxUL.htm](pathfinder-bestiary-2-items/FUbGjkLswPM3bxUL.htm)|Jaws|auto-trad|
|[FUdyjRGkgKWx4So9.htm](pathfinder-bestiary-2-items/FUdyjRGkgKWx4So9.htm)|Primal Spontaneous Spells|auto-trad|
|[FuTchIYowgou1HIa.htm](pathfinder-bestiary-2-items/FuTchIYowgou1HIa.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[fuxGM2FMqoHqXPJ4.htm](pathfinder-bestiary-2-items/fuxGM2FMqoHqXPJ4.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[fUyM5YcNhV5dlwTV.htm](pathfinder-bestiary-2-items/fUyM5YcNhV5dlwTV.htm)|Ice Burrow|auto-trad|
|[fVaZtygxTBUfppLk.htm](pathfinder-bestiary-2-items/fVaZtygxTBUfppLk.htm)|Swarming Bites|auto-trad|
|[FVDPg3HIsGffnOjd.htm](pathfinder-bestiary-2-items/FVDPg3HIsGffnOjd.htm)|Shameful Touch|auto-trad|
|[FWHFEirBNdz22BqV.htm](pathfinder-bestiary-2-items/FWHFEirBNdz22BqV.htm)|Occult Innate Spells|auto-trad|
|[fx1cIpK2KgNbm7Rz.htm](pathfinder-bestiary-2-items/fx1cIpK2KgNbm7Rz.htm)|Magma Swim|auto-trad|
|[FXPT5cs0LSYkZOku.htm](pathfinder-bestiary-2-items/FXPT5cs0LSYkZOku.htm)|Claw|auto-trad|
|[FXSgcv7xK9yqmz6Q.htm](pathfinder-bestiary-2-items/FXSgcv7xK9yqmz6Q.htm)|Jaws|auto-trad|
|[fxuqMIJcFuZQworq.htm](pathfinder-bestiary-2-items/fxuqMIJcFuZQworq.htm)|Fangs|auto-trad|
|[fxZztiCJybeYYeOI.htm](pathfinder-bestiary-2-items/fxZztiCJybeYYeOI.htm)|Captivating Dance|auto-trad|
|[FYiiFhvRvIKkTu5v.htm](pathfinder-bestiary-2-items/FYiiFhvRvIKkTu5v.htm)|Fast Healing 5|auto-trad|
|[fysusPtWaa25MvUW.htm](pathfinder-bestiary-2-items/fysusPtWaa25MvUW.htm)|Claw|auto-trad|
|[FZaWBsctyw8LDdfD.htm](pathfinder-bestiary-2-items/FZaWBsctyw8LDdfD.htm)|Draconic Momentum|auto-trad|
|[fZOolgYWKm1a27op.htm](pathfinder-bestiary-2-items/fZOolgYWKm1a27op.htm)|Purity Vulnerability|auto-trad|
|[G0D3kQHUd2mouA90.htm](pathfinder-bestiary-2-items/G0D3kQHUd2mouA90.htm)|Dominate Animal|auto-trad|
|[G0pgg8gIhgw18USv.htm](pathfinder-bestiary-2-items/G0pgg8gIhgw18USv.htm)|Trample|auto-trad|
|[g2gB3rKzB7uL7Kxn.htm](pathfinder-bestiary-2-items/g2gB3rKzB7uL7Kxn.htm)|Claw|auto-trad|
|[G2PIvLQqH98WKNaK.htm](pathfinder-bestiary-2-items/G2PIvLQqH98WKNaK.htm)|Planar Acclimation|auto-trad|
|[g3Gb5EYfu1jFqndC.htm](pathfinder-bestiary-2-items/g3Gb5EYfu1jFqndC.htm)|Ink Cloud|auto-trad|
|[G3Qoqz5IMBvs9EYb.htm](pathfinder-bestiary-2-items/G3Qoqz5IMBvs9EYb.htm)|Verdant Burst|auto-trad|
|[G412UtVloe7PIpa4.htm](pathfinder-bestiary-2-items/G412UtVloe7PIpa4.htm)|+2 Status to All Saves vs. Disease and Poison|auto-trad|
|[g41jvLpQ6exKQBgd.htm](pathfinder-bestiary-2-items/g41jvLpQ6exKQBgd.htm)|Spines|auto-trad|
|[g56aPVqelhyIjRuv.htm](pathfinder-bestiary-2-items/g56aPVqelhyIjRuv.htm)|+4 Status to All Saves vs. Sonic|auto-trad|
|[G5e1Cf5jNqFiqF6y.htm](pathfinder-bestiary-2-items/G5e1Cf5jNqFiqF6y.htm)|Grab|auto-trad|
|[G5NJxlLVYCrXX8Jv.htm](pathfinder-bestiary-2-items/G5NJxlLVYCrXX8Jv.htm)|Twisting Tail|auto-trad|
|[g5TUPRPR9gHfXrfU.htm](pathfinder-bestiary-2-items/g5TUPRPR9gHfXrfU.htm)|Necrotic Decay|auto-trad|
|[G6O55FgFD95SRjPP.htm](pathfinder-bestiary-2-items/G6O55FgFD95SRjPP.htm)|Claw|auto-trad|
|[G7P9My2HYy017m1O.htm](pathfinder-bestiary-2-items/G7P9My2HYy017m1O.htm)|36 AC vs. Non-Magical|auto-trad|
|[g9enKXrvCUkwck8W.htm](pathfinder-bestiary-2-items/g9enKXrvCUkwck8W.htm)|Attack of Opportunity (Special)|auto-trad|
|[gADgaaM8ZZlYZcXg.htm](pathfinder-bestiary-2-items/gADgaaM8ZZlYZcXg.htm)|Jaws|auto-trad|
|[gAdwnZ4OP8sPpuOg.htm](pathfinder-bestiary-2-items/gAdwnZ4OP8sPpuOg.htm)|Painsight|auto-trad|
|[GBBCSikxVuTyL14m.htm](pathfinder-bestiary-2-items/GBBCSikxVuTyL14m.htm)|Foot|auto-trad|
|[Gbcpa6lIhj2KUyH4.htm](pathfinder-bestiary-2-items/Gbcpa6lIhj2KUyH4.htm)|Rampant Growth|auto-trad|
|[gBFFibeJFk4MB72C.htm](pathfinder-bestiary-2-items/gBFFibeJFk4MB72C.htm)|Draconic Frenzy|auto-trad|
|[GbfpFzRZunqiRxI5.htm](pathfinder-bestiary-2-items/GbfpFzRZunqiRxI5.htm)|Catch Rock|auto-trad|
|[GBMDJeuQSv0Q6T5o.htm](pathfinder-bestiary-2-items/GBMDJeuQSv0Q6T5o.htm)|Breath Weapon|auto-trad|
|[gbsLwd2FBQal3xdZ.htm](pathfinder-bestiary-2-items/gbsLwd2FBQal3xdZ.htm)|Sneak Attack|auto-trad|
|[GCC6Zf1NaPihfEJ0.htm](pathfinder-bestiary-2-items/GCC6Zf1NaPihfEJ0.htm)|Tangle Spores|auto-trad|
|[gcdD70PvUw3sfPCK.htm](pathfinder-bestiary-2-items/gcdD70PvUw3sfPCK.htm)|Painful Harmonics|auto-trad|
|[GCGNJYiHP5GMdDAQ.htm](pathfinder-bestiary-2-items/GCGNJYiHP5GMdDAQ.htm)|Exit Body|auto-trad|
|[GcML4HwZyqVy7XVw.htm](pathfinder-bestiary-2-items/GcML4HwZyqVy7XVw.htm)|Talon|auto-trad|
|[GCu6OFgRjem6F0sI.htm](pathfinder-bestiary-2-items/GCu6OFgRjem6F0sI.htm)|Witchflame|auto-trad|
|[gCwKF48GST0z9EEa.htm](pathfinder-bestiary-2-items/gCwKF48GST0z9EEa.htm)|Grab|auto-trad|
|[GcxVApKgeD5WO9Wh.htm](pathfinder-bestiary-2-items/GcxVApKgeD5WO9Wh.htm)|Desiccating Bite|auto-trad|
|[gd4pTa9mCV6qrbcN.htm](pathfinder-bestiary-2-items/gd4pTa9mCV6qrbcN.htm)|Undulate|auto-trad|
|[gEADE2ZlQ0vRnps7.htm](pathfinder-bestiary-2-items/gEADE2ZlQ0vRnps7.htm)|Stench|auto-trad|
|[GEb3KusxBDvL8CkC.htm](pathfinder-bestiary-2-items/GEb3KusxBDvL8CkC.htm)|Stench|auto-trad|
|[GeIadA4vgUJvJ926.htm](pathfinder-bestiary-2-items/GeIadA4vgUJvJ926.htm)|Composite Shortbow|auto-trad|
|[GFW3IASGrcBSyETb.htm](pathfinder-bestiary-2-items/GFW3IASGrcBSyETb.htm)|Worm Chill|auto-trad|
|[gGIkwe9BtDFc6qdx.htm](pathfinder-bestiary-2-items/gGIkwe9BtDFc6qdx.htm)|Tremorsense|auto-trad|
|[ggo5fsSPiaSiT5ot.htm](pathfinder-bestiary-2-items/ggo5fsSPiaSiT5ot.htm)|At-Will Spells|auto-trad|
|[gGqDItdgQIf2RRPw.htm](pathfinder-bestiary-2-items/gGqDItdgQIf2RRPw.htm)|Falchion|auto-trad|
|[gH06P6rp2bswuGya.htm](pathfinder-bestiary-2-items/gH06P6rp2bswuGya.htm)|Jaws|auto-trad|
|[GHbCLo2MkEFt1qai.htm](pathfinder-bestiary-2-items/GHbCLo2MkEFt1qai.htm)|Scimitar|auto-trad|
|[ghbtjQahExs1AvpE.htm](pathfinder-bestiary-2-items/ghbtjQahExs1AvpE.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[GHcUVfhfs6mcm7mS.htm](pathfinder-bestiary-2-items/GHcUVfhfs6mcm7mS.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[gHUajYINDeCp7360.htm](pathfinder-bestiary-2-items/gHUajYINDeCp7360.htm)|Echolocation 120 feet|auto-trad|
|[gI0Pt3FCseRtdPpl.htm](pathfinder-bestiary-2-items/gI0Pt3FCseRtdPpl.htm)|Swarming Bites|auto-trad|
|[GIj7axAUfLMpl5DN.htm](pathfinder-bestiary-2-items/GIj7axAUfLMpl5DN.htm)|Greater Darkvision|auto-trad|
|[GimuDrJ7GoLAne8i.htm](pathfinder-bestiary-2-items/GimuDrJ7GoLAne8i.htm)|Avoid the Swat|auto-trad|
|[gINTMtPPDP3qyAhd.htm](pathfinder-bestiary-2-items/gINTMtPPDP3qyAhd.htm)|At-Will Spells|auto-trad|
|[GIop4yLHoN44KXN5.htm](pathfinder-bestiary-2-items/GIop4yLHoN44KXN5.htm)|Trample|auto-trad|
|[giVESjdTpw33NygU.htm](pathfinder-bestiary-2-items/giVESjdTpw33NygU.htm)|Norn Shears|auto-trad|
|[gixaFzYqsyV9bsLi.htm](pathfinder-bestiary-2-items/gixaFzYqsyV9bsLi.htm)|Cairn Wight Spawn|auto-trad|
|[GJECjnP6onECplMu.htm](pathfinder-bestiary-2-items/GJECjnP6onECplMu.htm)|Fangs|auto-trad|
|[gJMFRPJuO2fffH1c.htm](pathfinder-bestiary-2-items/gJMFRPJuO2fffH1c.htm)|Deflecting Cloud|auto-trad|
|[GjmjXrm0BYOCXs5E.htm](pathfinder-bestiary-2-items/GjmjXrm0BYOCXs5E.htm)|Telepathy|auto-trad|
|[gkiQGmXDkxDHGY7j.htm](pathfinder-bestiary-2-items/gkiQGmXDkxDHGY7j.htm)|Mask of Power|auto-trad|
|[GkMCAxjpf2Y5nfdv.htm](pathfinder-bestiary-2-items/GkMCAxjpf2Y5nfdv.htm)|Draconic Momentum|auto-trad|
|[gKqI8aZDucHlpC9C.htm](pathfinder-bestiary-2-items/gKqI8aZDucHlpC9C.htm)|Cowering Fear|auto-trad|
|[gKujbrE9tdbREcZi.htm](pathfinder-bestiary-2-items/gKujbrE9tdbREcZi.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[GL5oMe9C6pcXxqcU.htm](pathfinder-bestiary-2-items/GL5oMe9C6pcXxqcU.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[gL7E79l3yS72fXPo.htm](pathfinder-bestiary-2-items/gL7E79l3yS72fXPo.htm)|Petrifying Gaze|auto-trad|
|[gm4ckzxne8UYVaZg.htm](pathfinder-bestiary-2-items/gm4ckzxne8UYVaZg.htm)|Feral Possession|auto-trad|
|[GmbRFl9ktqpdH1WE.htm](pathfinder-bestiary-2-items/GmbRFl9ktqpdH1WE.htm)|At-Will Spells|auto-trad|
|[gmjig3Ov1aG6tBOx.htm](pathfinder-bestiary-2-items/gmjig3Ov1aG6tBOx.htm)|Mandibles|auto-trad|
|[gmtlsmLh9eNdpNrk.htm](pathfinder-bestiary-2-items/gmtlsmLh9eNdpNrk.htm)|Mud Puddle|auto-trad|
|[GnAoSJeodjtFoanm.htm](pathfinder-bestiary-2-items/GnAoSJeodjtFoanm.htm)|Feral Possession|auto-trad|
|[gNhak3Nf3ve1ouhK.htm](pathfinder-bestiary-2-items/gNhak3Nf3ve1ouhK.htm)|Claw|auto-trad|
|[gnL5MFzMfrbfAapT.htm](pathfinder-bestiary-2-items/gnL5MFzMfrbfAapT.htm)|Holy Armaments|auto-trad|
|[gOjlMNwWXWNHohGo.htm](pathfinder-bestiary-2-items/gOjlMNwWXWNHohGo.htm)|Mark Quarry|auto-trad|
|[goYkQxQdewpmD26j.htm](pathfinder-bestiary-2-items/goYkQxQdewpmD26j.htm)|Jaws|auto-trad|
|[GoYV1mj4XKxDiBOM.htm](pathfinder-bestiary-2-items/GoYV1mj4XKxDiBOM.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[gozNFecLMRX00Wzz.htm](pathfinder-bestiary-2-items/gozNFecLMRX00Wzz.htm)|Frightful Presence|auto-trad|
|[gPcAYqbYyOADoHwc.htm](pathfinder-bestiary-2-items/gPcAYqbYyOADoHwc.htm)|Top-Heavy|auto-trad|
|[gPCpNSHZ6SvXUhij.htm](pathfinder-bestiary-2-items/gPCpNSHZ6SvXUhij.htm)|Painful Strikes|auto-trad|
|[GpD6rwvIzYFSvtUC.htm](pathfinder-bestiary-2-items/GpD6rwvIzYFSvtUC.htm)|Draconic Momentum|auto-trad|
|[GPRRfLJKZALqfKud.htm](pathfinder-bestiary-2-items/GPRRfLJKZALqfKud.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[GPSk2dGRxJdcBtW7.htm](pathfinder-bestiary-2-items/GPSk2dGRxJdcBtW7.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[gpZkw015Ns4Dbplk.htm](pathfinder-bestiary-2-items/gpZkw015Ns4Dbplk.htm)|Drowning Touch|auto-trad|
|[GQ4af1lP9hWhVhkx.htm](pathfinder-bestiary-2-items/GQ4af1lP9hWhVhkx.htm)|Frightful Presence|auto-trad|
|[gQnxWI3DPgkoWjmj.htm](pathfinder-bestiary-2-items/gQnxWI3DPgkoWjmj.htm)|Darkvision|auto-trad|
|[GRiHxWvHx9rf0zcF.htm](pathfinder-bestiary-2-items/GRiHxWvHx9rf0zcF.htm)|Expel Infestation|auto-trad|
|[GryMTmkSRqTAW3ol.htm](pathfinder-bestiary-2-items/GryMTmkSRqTAW3ol.htm)|Wing|auto-trad|
|[gS1EUP1J8sgiDyR1.htm](pathfinder-bestiary-2-items/gS1EUP1J8sgiDyR1.htm)|Consume Death|auto-trad|
|[GsaoA7w1bfpGuWvA.htm](pathfinder-bestiary-2-items/GsaoA7w1bfpGuWvA.htm)|Tentacle|auto-trad|
|[gsfeTAQtmgDaEQI7.htm](pathfinder-bestiary-2-items/gsfeTAQtmgDaEQI7.htm)|Darkvision|auto-trad|
|[GSglY8uncoV8v0jg.htm](pathfinder-bestiary-2-items/GSglY8uncoV8v0jg.htm)|Jet|auto-trad|
|[gSm7LVM6KgueNn8S.htm](pathfinder-bestiary-2-items/gSm7LVM6KgueNn8S.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Gsx5q76S7ZPL3iXd.htm](pathfinder-bestiary-2-items/Gsx5q76S7ZPL3iXd.htm)|Greater Darkvision|auto-trad|
|[GtJ8PtIPDvGSAIHU.htm](pathfinder-bestiary-2-items/GtJ8PtIPDvGSAIHU.htm)|Sneak Attack|auto-trad|
|[GTW3qSprQnTyrwNU.htm](pathfinder-bestiary-2-items/GTW3qSprQnTyrwNU.htm)|Claw|auto-trad|
|[gu96K11Wqu5OcYew.htm](pathfinder-bestiary-2-items/gu96K11Wqu5OcYew.htm)|Jaws|auto-trad|
|[guUOsE03Ck5JEoBu.htm](pathfinder-bestiary-2-items/guUOsE03Ck5JEoBu.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[GVFq98KDosC4rgkt.htm](pathfinder-bestiary-2-items/GVFq98KDosC4rgkt.htm)|Jaws|auto-trad|
|[GVP9BghiDShSeHpL.htm](pathfinder-bestiary-2-items/GVP9BghiDShSeHpL.htm)|Alter Dweomer|auto-trad|
|[gVRJ2KRIYk9slxtI.htm](pathfinder-bestiary-2-items/gVRJ2KRIYk9slxtI.htm)|Longbow|auto-trad|
|[gvU3ZMNnU7PcbF4v.htm](pathfinder-bestiary-2-items/gvU3ZMNnU7PcbF4v.htm)|Shield Block|auto-trad|
|[GWbkONUjqsTF8hP7.htm](pathfinder-bestiary-2-items/GWbkONUjqsTF8hP7.htm)|Claw|auto-trad|
|[GwBXsOeV4FFdxDzQ.htm](pathfinder-bestiary-2-items/GwBXsOeV4FFdxDzQ.htm)|Composite Longbow|auto-trad|
|[gWsyeIZTyzTRH7z1.htm](pathfinder-bestiary-2-items/gWsyeIZTyzTRH7z1.htm)|Telepathy|auto-trad|
|[GY6jeRcNdp5Fd9OB.htm](pathfinder-bestiary-2-items/GY6jeRcNdp5Fd9OB.htm)|Darkvision|auto-trad|
|[GYapjknILGOMiV84.htm](pathfinder-bestiary-2-items/GYapjknILGOMiV84.htm)|-2 Status Bonus on Saves vs. Curses|auto-trad|
|[gYBpkJDYJYKODQRK.htm](pathfinder-bestiary-2-items/gYBpkJDYJYKODQRK.htm)|Gem Gaze|auto-trad|
|[GyMOxyS6WyN6PNVC.htm](pathfinder-bestiary-2-items/GyMOxyS6WyN6PNVC.htm)|Greater Darkvision|auto-trad|
|[GYPODdG0fWWAPk5y.htm](pathfinder-bestiary-2-items/GYPODdG0fWWAPk5y.htm)|Frond|auto-trad|
|[GYQMiGEbQopeX3bK.htm](pathfinder-bestiary-2-items/GYQMiGEbQopeX3bK.htm)|Flowing Hair|auto-trad|
|[gyVj6MqUbxY54dB9.htm](pathfinder-bestiary-2-items/gyVj6MqUbxY54dB9.htm)|Light of Avarice|auto-trad|
|[gYXpVKd4NC2QDwe3.htm](pathfinder-bestiary-2-items/gYXpVKd4NC2QDwe3.htm)|At-Will Spells|auto-trad|
|[Gz3flOxBg3X75q6s.htm](pathfinder-bestiary-2-items/Gz3flOxBg3X75q6s.htm)|Constant Spells|auto-trad|
|[GZeUcgFbUA7B68BW.htm](pathfinder-bestiary-2-items/GZeUcgFbUA7B68BW.htm)|Telepathy|auto-trad|
|[gZfWtX8zkOoe49o7.htm](pathfinder-bestiary-2-items/gZfWtX8zkOoe49o7.htm)|Regeneration 25 (Deactivated by Vorpal Weapons)|auto-trad|
|[gzQlpS0D81W4iCub.htm](pathfinder-bestiary-2-items/gzQlpS0D81W4iCub.htm)|Primal Innate Spells|auto-trad|
|[h0VU0sBf5jz6KCu1.htm](pathfinder-bestiary-2-items/h0VU0sBf5jz6KCu1.htm)|The Sea's Revenge|auto-trad|
|[h10snW2DCZ2hS7fP.htm](pathfinder-bestiary-2-items/h10snW2DCZ2hS7fP.htm)|Claw|auto-trad|
|[h155XUVgiUJ5onse.htm](pathfinder-bestiary-2-items/h155XUVgiUJ5onse.htm)|Blink Resistances|auto-trad|
|[h1bIOksYIZLff4Jb.htm](pathfinder-bestiary-2-items/h1bIOksYIZLff4Jb.htm)|Jaws|auto-trad|
|[H1CrtJTYHt1zROG7.htm](pathfinder-bestiary-2-items/H1CrtJTYHt1zROG7.htm)|Rend|auto-trad|
|[h20KMmp1wuDSm1Oa.htm](pathfinder-bestiary-2-items/h20KMmp1wuDSm1Oa.htm)|Seed Spray|auto-trad|
|[h282lP6zRpC6R83o.htm](pathfinder-bestiary-2-items/h282lP6zRpC6R83o.htm)|Swarming Infestation|auto-trad|
|[H2rT9wdiejx3F1b0.htm](pathfinder-bestiary-2-items/H2rT9wdiejx3F1b0.htm)|Light Blindness|auto-trad|
|[h35hRTaiSXOfVZFA.htm](pathfinder-bestiary-2-items/h35hRTaiSXOfVZFA.htm)|Echolocation|auto-trad|
|[h3krrxTbAiZjVs8m.htm](pathfinder-bestiary-2-items/h3krrxTbAiZjVs8m.htm)|Greater Darkvision|auto-trad|
|[H3y3IbgMpQWevqRk.htm](pathfinder-bestiary-2-items/H3y3IbgMpQWevqRk.htm)|Drain Life|auto-trad|
|[h4MdAekw9ZoGZc2Q.htm](pathfinder-bestiary-2-items/h4MdAekw9ZoGZc2Q.htm)|Attack of Opportunity (Tail Only)|auto-trad|
|[H5H2qTd7RH9FnyiA.htm](pathfinder-bestiary-2-items/H5H2qTd7RH9FnyiA.htm)|Enliven Foliage|auto-trad|
|[H5oOhIYYYiEvkna6.htm](pathfinder-bestiary-2-items/H5oOhIYYYiEvkna6.htm)|At-Will Spells|auto-trad|
|[h5vOq3kLWQKQrs6h.htm](pathfinder-bestiary-2-items/h5vOq3kLWQKQrs6h.htm)|Swallow Whole|auto-trad|
|[h61KXB1CbfC2hTh2.htm](pathfinder-bestiary-2-items/h61KXB1CbfC2hTh2.htm)|Swallow Whole|auto-trad|
|[h68Rp5GCPIpGGeyq.htm](pathfinder-bestiary-2-items/h68Rp5GCPIpGGeyq.htm)|Breath Weapon|auto-trad|
|[h69jYWXNKYbuTyzE.htm](pathfinder-bestiary-2-items/h69jYWXNKYbuTyzE.htm)|Breath Weapon|auto-trad|
|[H6d3brisQR3MDHO6.htm](pathfinder-bestiary-2-items/H6d3brisQR3MDHO6.htm)|Low-Light Vision|auto-trad|
|[H6WOpA1s4iRwT9Ik.htm](pathfinder-bestiary-2-items/H6WOpA1s4iRwT9Ik.htm)|Scent|auto-trad|
|[H7is94BX3CDB78hl.htm](pathfinder-bestiary-2-items/H7is94BX3CDB78hl.htm)|Fist|auto-trad|
|[H7lzbseyLDhs5Tp3.htm](pathfinder-bestiary-2-items/H7lzbseyLDhs5Tp3.htm)|Sound Mimicry|auto-trad|
|[H7ovqSFCx0c885Da.htm](pathfinder-bestiary-2-items/H7ovqSFCx0c885Da.htm)|Tentacles|auto-trad|
|[h7vIylds9BVXZM8F.htm](pathfinder-bestiary-2-items/h7vIylds9BVXZM8F.htm)|Primal Innate Spells|auto-trad|
|[H7ZdigvTaI99Ab76.htm](pathfinder-bestiary-2-items/H7ZdigvTaI99Ab76.htm)|Darkvision|auto-trad|
|[H8b5ywUVrI7pjhB8.htm](pathfinder-bestiary-2-items/H8b5ywUVrI7pjhB8.htm)|Shortsword|auto-trad|
|[H97yITJBuctoMctx.htm](pathfinder-bestiary-2-items/H97yITJBuctoMctx.htm)|Change Shape|auto-trad|
|[H9cKHc9OAHiXKDzf.htm](pathfinder-bestiary-2-items/H9cKHc9OAHiXKDzf.htm)|Swarming Bites|auto-trad|
|[H9Hu5uk0AogKrXF4.htm](pathfinder-bestiary-2-items/H9Hu5uk0AogKrXF4.htm)|Regeneration 15 (deactivated by evil)|auto-trad|
|[h9Ugyirm1B1g6pt6.htm](pathfinder-bestiary-2-items/h9Ugyirm1B1g6pt6.htm)|Jaws|auto-trad|
|[hA360VLZ8uHZCZxF.htm](pathfinder-bestiary-2-items/hA360VLZ8uHZCZxF.htm)|Primal Innate Spells|auto-trad|
|[hA4DPWIY7hQbSa0i.htm](pathfinder-bestiary-2-items/hA4DPWIY7hQbSa0i.htm)|Dart|auto-trad|
|[HAh9hAsU8XCNWPOU.htm](pathfinder-bestiary-2-items/HAh9hAsU8XCNWPOU.htm)|Organ of Endless Water|auto-trad|
|[HAxp8tP3tYXocejD.htm](pathfinder-bestiary-2-items/HAxp8tP3tYXocejD.htm)|Claw|auto-trad|
|[HaXqTHQH97u8nuJK.htm](pathfinder-bestiary-2-items/HaXqTHQH97u8nuJK.htm)|Fist|auto-trad|
|[HaYQ8Cd4C0hYSFdc.htm](pathfinder-bestiary-2-items/HaYQ8Cd4C0hYSFdc.htm)|Leap Attack|auto-trad|
|[hbf277SQgtMCjjWF.htm](pathfinder-bestiary-2-items/hbf277SQgtMCjjWF.htm)|Attack of Opportunity|auto-trad|
|[hBvEzmMtwWP1eG7b.htm](pathfinder-bestiary-2-items/hBvEzmMtwWP1eG7b.htm)|Divine Innate Spells|auto-trad|
|[HcJNEFmJC7L4Ku0M.htm](pathfinder-bestiary-2-items/HcJNEFmJC7L4Ku0M.htm)|Rock|auto-trad|
|[Hd5SG091X71rJV1D.htm](pathfinder-bestiary-2-items/Hd5SG091X71rJV1D.htm)|Confusing Display|auto-trad|
|[hDdpyAhdnjpO1Bp9.htm](pathfinder-bestiary-2-items/hDdpyAhdnjpO1Bp9.htm)|Low-Light Vision|auto-trad|
|[HDyQbtYQNxLarefS.htm](pathfinder-bestiary-2-items/HDyQbtYQNxLarefS.htm)|Grab|auto-trad|
|[he0x5StuDg4yge3c.htm](pathfinder-bestiary-2-items/he0x5StuDg4yge3c.htm)|Claw|auto-trad|
|[hegoW9J1eMFWn0I6.htm](pathfinder-bestiary-2-items/hegoW9J1eMFWn0I6.htm)|Grab|auto-trad|
|[hEsUevgpSNSIcNzB.htm](pathfinder-bestiary-2-items/hEsUevgpSNSIcNzB.htm)|Pounce|auto-trad|
|[HFAfo9jbjahhbw5J.htm](pathfinder-bestiary-2-items/HFAfo9jbjahhbw5J.htm)|Greater Darkvision|auto-trad|
|[hFDR91zB1cjh1UHv.htm](pathfinder-bestiary-2-items/hFDR91zB1cjh1UHv.htm)|Dead Tree|auto-trad|
|[hg3lMJ6qrkw8WpZ3.htm](pathfinder-bestiary-2-items/hg3lMJ6qrkw8WpZ3.htm)|Light Blindness|auto-trad|
|[HHnlE5IJhxgzBkin.htm](pathfinder-bestiary-2-items/HHnlE5IJhxgzBkin.htm)|Darkvision|auto-trad|
|[hHqx5bavcH4BYz5s.htm](pathfinder-bestiary-2-items/hHqx5bavcH4BYz5s.htm)|Darkvision|auto-trad|
|[HHRUhEQ6DJPe6kyX.htm](pathfinder-bestiary-2-items/HHRUhEQ6DJPe6kyX.htm)|Claw|auto-trad|
|[HHscPOBzNRjg9IJ5.htm](pathfinder-bestiary-2-items/HHscPOBzNRjg9IJ5.htm)|Smoke Vision|auto-trad|
|[HhUiMTB8Qa8ySlVM.htm](pathfinder-bestiary-2-items/HhUiMTB8Qa8ySlVM.htm)|Sneak Attack|auto-trad|
|[HilJh22elkeshhLj.htm](pathfinder-bestiary-2-items/HilJh22elkeshhLj.htm)|Wing|auto-trad|
|[HIn5PSEUsrEdOYWT.htm](pathfinder-bestiary-2-items/HIn5PSEUsrEdOYWT.htm)|Trunk|auto-trad|
|[HIRmU6WblxuDmWc6.htm](pathfinder-bestiary-2-items/HIRmU6WblxuDmWc6.htm)|Fleshgout|auto-trad|
|[hkgYTbnzCzYBfP7k.htm](pathfinder-bestiary-2-items/hkgYTbnzCzYBfP7k.htm)|Voice Imitation|auto-trad|
|[Hkm57fRjKQG62Gkr.htm](pathfinder-bestiary-2-items/Hkm57fRjKQG62Gkr.htm)|Occult Innate Spells|auto-trad|
|[HL1pjayMZJpK1JID.htm](pathfinder-bestiary-2-items/HL1pjayMZJpK1JID.htm)|Lifesense 60 feet|auto-trad|
|[HL22Gfjw3DN4v0Sx.htm](pathfinder-bestiary-2-items/HL22Gfjw3DN4v0Sx.htm)|Divine Innate Spells|auto-trad|
|[HLcMADTgT1nixxce.htm](pathfinder-bestiary-2-items/HLcMADTgT1nixxce.htm)|Breath of the Bog|auto-trad|
|[HLOEvF4GMvQEPHct.htm](pathfinder-bestiary-2-items/HLOEvF4GMvQEPHct.htm)|Camouflaged Step|auto-trad|
|[hm7ls5Yo0n7QOWMl.htm](pathfinder-bestiary-2-items/hm7ls5Yo0n7QOWMl.htm)|Low-Light Vision|auto-trad|
|[Hmb2WHTATQHONj7o.htm](pathfinder-bestiary-2-items/Hmb2WHTATQHONj7o.htm)|Tail|auto-trad|
|[HMmnA0DwFNibve0c.htm](pathfinder-bestiary-2-items/HMmnA0DwFNibve0c.htm)|At-Will Spells|auto-trad|
|[HmU4KBbkb30WqDpU.htm](pathfinder-bestiary-2-items/HmU4KBbkb30WqDpU.htm)|Darkvision|auto-trad|
|[Hn5PgfCFMwbKdN20.htm](pathfinder-bestiary-2-items/Hn5PgfCFMwbKdN20.htm)|Self-Loathing|auto-trad|
|[hNAE5swsHtvsyity.htm](pathfinder-bestiary-2-items/hNAE5swsHtvsyity.htm)|Darkvision|auto-trad|
|[HNfXE0yusiYA2juN.htm](pathfinder-bestiary-2-items/HNfXE0yusiYA2juN.htm)|Darkvision|auto-trad|
|[HnkrKvCxJTmladAk.htm](pathfinder-bestiary-2-items/HnkrKvCxJTmladAk.htm)|Primal Prepared Spells|auto-trad|
|[HOVV49IZbhN2hIwN.htm](pathfinder-bestiary-2-items/HOVV49IZbhN2hIwN.htm)|Scent|auto-trad|
|[HphLKNRNlCVczAuz.htm](pathfinder-bestiary-2-items/HphLKNRNlCVczAuz.htm)|Claw|auto-trad|
|[hPjYRtGf041nnmLw.htm](pathfinder-bestiary-2-items/hPjYRtGf041nnmLw.htm)|Stinger|auto-trad|
|[HpLHoXbsdK98PiDh.htm](pathfinder-bestiary-2-items/HpLHoXbsdK98PiDh.htm)|Solid Refrain|auto-trad|
|[HPPj6Nf4GtdwCAEx.htm](pathfinder-bestiary-2-items/HPPj6Nf4GtdwCAEx.htm)|No Hearing|auto-trad|
|[hPXSQxaDWNJRZg36.htm](pathfinder-bestiary-2-items/hPXSQxaDWNJRZg36.htm)|Constrict|auto-trad|
|[HQ205X1b6CWTHXfU.htm](pathfinder-bestiary-2-items/HQ205X1b6CWTHXfU.htm)|Slow|auto-trad|
|[hQdMdV8u5o7eWZEV.htm](pathfinder-bestiary-2-items/hQdMdV8u5o7eWZEV.htm)|Jaws|auto-trad|
|[HQfx53oV60UYo0lg.htm](pathfinder-bestiary-2-items/HQfx53oV60UYo0lg.htm)|Gremlin Snare|auto-trad|
|[HQTX3r7nEv9YINQh.htm](pathfinder-bestiary-2-items/HQTX3r7nEv9YINQh.htm)|Ferocity|auto-trad|
|[HqvJxFfKrYlRxHWC.htm](pathfinder-bestiary-2-items/HqvJxFfKrYlRxHWC.htm)|Rend|auto-trad|
|[HqwhBLhYDbCBbv8f.htm](pathfinder-bestiary-2-items/HqwhBLhYDbCBbv8f.htm)|Constant Spells|auto-trad|
|[hr8UQcvJzWfTNGoH.htm](pathfinder-bestiary-2-items/hr8UQcvJzWfTNGoH.htm)|Planar Fast Healing 5|auto-trad|
|[HR9jZiX9oYUmGloZ.htm](pathfinder-bestiary-2-items/HR9jZiX9oYUmGloZ.htm)|Darkvision|auto-trad|
|[hREXXxR78zjxAtPW.htm](pathfinder-bestiary-2-items/hREXXxR78zjxAtPW.htm)|Shoal Linnorm Venom|auto-trad|
|[HRJpFIoBwbJ1rwnG.htm](pathfinder-bestiary-2-items/HRJpFIoBwbJ1rwnG.htm)|Telepathy|auto-trad|
|[HrUGB7et2xcTkWSB.htm](pathfinder-bestiary-2-items/HrUGB7et2xcTkWSB.htm)|Mandibles|auto-trad|
|[hS50gj87pD1XnQk9.htm](pathfinder-bestiary-2-items/hS50gj87pD1XnQk9.htm)|Claw Storm|auto-trad|
|[Hsh9yw0IW6kN2n8t.htm](pathfinder-bestiary-2-items/Hsh9yw0IW6kN2n8t.htm)|Electrified Blood|auto-trad|
|[hSqvFZoc1oaDpRVt.htm](pathfinder-bestiary-2-items/hSqvFZoc1oaDpRVt.htm)|Darkvision|auto-trad|
|[hSvaFFK9ERUSfUry.htm](pathfinder-bestiary-2-items/hSvaFFK9ERUSfUry.htm)|Moon Frenzy|auto-trad|
|[hswGSODfM3HdK8zC.htm](pathfinder-bestiary-2-items/hswGSODfM3HdK8zC.htm)|Stench|auto-trad|
|[HT78d5cKcguY6IcL.htm](pathfinder-bestiary-2-items/HT78d5cKcguY6IcL.htm)|Pounce|auto-trad|
|[HTGkYCrWWOoIvAja.htm](pathfinder-bestiary-2-items/HTGkYCrWWOoIvAja.htm)|Serpentfolk Venom|auto-trad|
|[hTqMUAAnd9iTtvQW.htm](pathfinder-bestiary-2-items/hTqMUAAnd9iTtvQW.htm)|Primal Innate Spells|auto-trad|
|[hTuKXeGWvUSUobx7.htm](pathfinder-bestiary-2-items/hTuKXeGWvUSUobx7.htm)|Ball Lightning Breath|auto-trad|
|[Hu1SdyNVG4WJmNcx.htm](pathfinder-bestiary-2-items/Hu1SdyNVG4WJmNcx.htm)|Soul Spells|auto-trad|
|[Hu5L9AYzwJsXxIDY.htm](pathfinder-bestiary-2-items/Hu5L9AYzwJsXxIDY.htm)|Tremorsense 30 feet|auto-trad|
|[hV3hgI3sPR9ZrQeK.htm](pathfinder-bestiary-2-items/hV3hgI3sPR9ZrQeK.htm)|Improved Grab|auto-trad|
|[HViM44dkUyHEWK31.htm](pathfinder-bestiary-2-items/HViM44dkUyHEWK31.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[HvL4pznzaaZwsFkA.htm](pathfinder-bestiary-2-items/HvL4pznzaaZwsFkA.htm)|Claw|auto-trad|
|[hVmOEk3NrvvmFkKg.htm](pathfinder-bestiary-2-items/hVmOEk3NrvvmFkKg.htm)|Painsight|auto-trad|
|[hvQmC6GTFeQoVTYG.htm](pathfinder-bestiary-2-items/hvQmC6GTFeQoVTYG.htm)|Breath Weapon|auto-trad|
|[hw7F7aaLP2U1MYZy.htm](pathfinder-bestiary-2-items/hw7F7aaLP2U1MYZy.htm)|Occult Innate Spells|auto-trad|
|[hWE70sgsZXn7ZEa7.htm](pathfinder-bestiary-2-items/hWE70sgsZXn7ZEa7.htm)|Greatsword|auto-trad|
|[hWi2Ff37mvsMC81Y.htm](pathfinder-bestiary-2-items/hWi2Ff37mvsMC81Y.htm)|Aquatic Ambush|auto-trad|
|[HWKLKpUPvbnvqEcp.htm](pathfinder-bestiary-2-items/HWKLKpUPvbnvqEcp.htm)|Horn|auto-trad|
|[hwNl8SFYu6kOmwly.htm](pathfinder-bestiary-2-items/hwNl8SFYu6kOmwly.htm)|Greater Darkvision|auto-trad|
|[hwnMtKzru4aBoOu3.htm](pathfinder-bestiary-2-items/hwnMtKzru4aBoOu3.htm)|Change Shape|auto-trad|
|[hwQlpkEec2QqnSxR.htm](pathfinder-bestiary-2-items/hwQlpkEec2QqnSxR.htm)|Regeneration 25 (deactivated by acid or cold)|auto-trad|
|[HWUmjgOASJXWhmgn.htm](pathfinder-bestiary-2-items/HWUmjgOASJXWhmgn.htm)|Aquatic Ambush|auto-trad|
|[hwX8mlw73yOCgwYw.htm](pathfinder-bestiary-2-items/hwX8mlw73yOCgwYw.htm)|Braincloud|auto-trad|
|[hwXxwT85s7KyKoWq.htm](pathfinder-bestiary-2-items/hwXxwT85s7KyKoWq.htm)|Lifesense 30 feet|auto-trad|
|[hwZzmzZjVpUb64cH.htm](pathfinder-bestiary-2-items/hwZzmzZjVpUb64cH.htm)|Earth Glide|auto-trad|
|[hxGtO1RrJyi0gVvQ.htm](pathfinder-bestiary-2-items/hxGtO1RrJyi0gVvQ.htm)|Scent|auto-trad|
|[HXNPKNUC1fSgSuTM.htm](pathfinder-bestiary-2-items/HXNPKNUC1fSgSuTM.htm)|Mohrg Spawn|auto-trad|
|[HXybLZsE68ejwZbJ.htm](pathfinder-bestiary-2-items/HXybLZsE68ejwZbJ.htm)|Limb Extension|auto-trad|
|[hy3Lxy9hFGa73bQP.htm](pathfinder-bestiary-2-items/hy3Lxy9hFGa73bQP.htm)|Sunlight Powerlessness|auto-trad|
|[hychIP30Jt5HOYED.htm](pathfinder-bestiary-2-items/hychIP30Jt5HOYED.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[hykxILVZRpdseaRk.htm](pathfinder-bestiary-2-items/hykxILVZRpdseaRk.htm)|Regeneration 25 (Deactivated by Acid or Fire)|auto-trad|
|[hyT0NpcWHdpgo5lt.htm](pathfinder-bestiary-2-items/hyT0NpcWHdpgo5lt.htm)|Dagger|auto-trad|
|[hyuJvmXFcS4R6bfc.htm](pathfinder-bestiary-2-items/hyuJvmXFcS4R6bfc.htm)|Divine Innate Spells|auto-trad|
|[hzr2Jl7pTcCFNWlF.htm](pathfinder-bestiary-2-items/hzr2Jl7pTcCFNWlF.htm)|Greataxe|auto-trad|
|[I06QLdoJMbnJD3Pu.htm](pathfinder-bestiary-2-items/I06QLdoJMbnJD3Pu.htm)|Create Spawn|auto-trad|
|[i0ue3QRUsYmGAJDm.htm](pathfinder-bestiary-2-items/i0ue3QRUsYmGAJDm.htm)|Improved Grab|auto-trad|
|[I1cXiDVfmHepyrXf.htm](pathfinder-bestiary-2-items/I1cXiDVfmHepyrXf.htm)|Low-Light Vision|auto-trad|
|[i1qGIf7bGKRu3RUq.htm](pathfinder-bestiary-2-items/i1qGIf7bGKRu3RUq.htm)|Tail|auto-trad|
|[I1XiJaJSXxln1l7V.htm](pathfinder-bestiary-2-items/I1XiJaJSXxln1l7V.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[i2d0NrqSfbRm2l1Y.htm](pathfinder-bestiary-2-items/i2d0NrqSfbRm2l1Y.htm)|Shocking Douse|auto-trad|
|[I2ICHtXckQoNKHcm.htm](pathfinder-bestiary-2-items/I2ICHtXckQoNKHcm.htm)|Knockdown|auto-trad|
|[I2sDX6PHhRSYYUZN.htm](pathfinder-bestiary-2-items/I2sDX6PHhRSYYUZN.htm)|Fist|auto-trad|
|[i3JP3msktn7Ld1pK.htm](pathfinder-bestiary-2-items/i3JP3msktn7Ld1pK.htm)|Divine Innate Spells|auto-trad|
|[i47viSV57dRQC5ZG.htm](pathfinder-bestiary-2-items/i47viSV57dRQC5ZG.htm)|Tremorsense|auto-trad|
|[I4ZjVogUXmOSRkNn.htm](pathfinder-bestiary-2-items/I4ZjVogUXmOSRkNn.htm)|Low-Light Vision|auto-trad|
|[I6QwINd18HEwJGWW.htm](pathfinder-bestiary-2-items/I6QwINd18HEwJGWW.htm)|Frightful Presence|auto-trad|
|[i8KkA2JGZTsQSxta.htm](pathfinder-bestiary-2-items/i8KkA2JGZTsQSxta.htm)|Engulf|auto-trad|
|[i8TVY4kXRvQIaLLG.htm](pathfinder-bestiary-2-items/i8TVY4kXRvQIaLLG.htm)|Pollen Touch|auto-trad|
|[i9n1VIjQaHqI1V9X.htm](pathfinder-bestiary-2-items/i9n1VIjQaHqI1V9X.htm)|Paralyzing Display|auto-trad|
|[i9N4Fg8nJJdpMQUf.htm](pathfinder-bestiary-2-items/i9N4Fg8nJJdpMQUf.htm)|Tongue|auto-trad|
|[i9Rco0paxz9ohGlm.htm](pathfinder-bestiary-2-items/i9Rco0paxz9ohGlm.htm)|Scent|auto-trad|
|[iaO10SxubynwqoL4.htm](pathfinder-bestiary-2-items/iaO10SxubynwqoL4.htm)|Prepared Primal Spells|auto-trad|
|[icFkORI0Y4bNqBbP.htm](pathfinder-bestiary-2-items/icFkORI0Y4bNqBbP.htm)|Tail|auto-trad|
|[iCMR05ML275h0Gek.htm](pathfinder-bestiary-2-items/iCMR05ML275h0Gek.htm)|Rebirth|auto-trad|
|[IcWvzNL316a1QTak.htm](pathfinder-bestiary-2-items/IcWvzNL316a1QTak.htm)|Claw|auto-trad|
|[IeAmIUQMEYZdSbaq.htm](pathfinder-bestiary-2-items/IeAmIUQMEYZdSbaq.htm)|Grab|auto-trad|
|[ieuwqdM4zXO2i22Z.htm](pathfinder-bestiary-2-items/ieuwqdM4zXO2i22Z.htm)|Low-Light Vision|auto-trad|
|[IFp4VK6X80nXiTJu.htm](pathfinder-bestiary-2-items/IFp4VK6X80nXiTJu.htm)|Darkvision|auto-trad|
|[IHM0ZFUpH2qKKM7E.htm](pathfinder-bestiary-2-items/IHM0ZFUpH2qKKM7E.htm)|Regeneration 10 (deactivated by good or silver)|auto-trad|
|[IHn483eD21ShK5j4.htm](pathfinder-bestiary-2-items/IHn483eD21ShK5j4.htm)|Infuse Weapons|auto-trad|
|[iHR7yRqYeZST3uB5.htm](pathfinder-bestiary-2-items/iHR7yRqYeZST3uB5.htm)|Constrict|auto-trad|
|[IieLHPPUSUSDsN8d.htm](pathfinder-bestiary-2-items/IieLHPPUSUSDsN8d.htm)|Motion Sense 60 feet|auto-trad|
|[iINgbpwr9zD0DJ0d.htm](pathfinder-bestiary-2-items/iINgbpwr9zD0DJ0d.htm)|Fast Healing 10|auto-trad|
|[Ij85zXU5FdthHI5o.htm](pathfinder-bestiary-2-items/Ij85zXU5FdthHI5o.htm)|Stolen Death|auto-trad|
|[IJc6CxwwAtsS27ry.htm](pathfinder-bestiary-2-items/IJc6CxwwAtsS27ry.htm)|Tremorsense|auto-trad|
|[iJjnT8l7EIEaChZu.htm](pathfinder-bestiary-2-items/iJjnT8l7EIEaChZu.htm)|Camouflage|auto-trad|
|[iKbhKQt4aD8JDvDd.htm](pathfinder-bestiary-2-items/iKbhKQt4aD8JDvDd.htm)|Absorb Flame|auto-trad|
|[iKjpMK3lSakpUkFY.htm](pathfinder-bestiary-2-items/iKjpMK3lSakpUkFY.htm)|Jaws|auto-trad|
|[iKpGYrRKqAjGDO9B.htm](pathfinder-bestiary-2-items/iKpGYrRKqAjGDO9B.htm)|Constrict|auto-trad|
|[IKRXYrbZOj22lXn8.htm](pathfinder-bestiary-2-items/IKRXYrbZOj22lXn8.htm)|Ferocity|auto-trad|
|[IM4bochbTpbrXxfY.htm](pathfinder-bestiary-2-items/IM4bochbTpbrXxfY.htm)|Sneak Attack|auto-trad|
|[imFNV40cUextfevX.htm](pathfinder-bestiary-2-items/imFNV40cUextfevX.htm)|Grasping Foliage|auto-trad|
|[ImVU4G37cmGJMDoA.htm](pathfinder-bestiary-2-items/ImVU4G37cmGJMDoA.htm)|Sickle|auto-trad|
|[InHy3ZKl3MHOyTX4.htm](pathfinder-bestiary-2-items/InHy3ZKl3MHOyTX4.htm)|At-Will Spells|auto-trad|
|[injH2iWqg5QjQiqs.htm](pathfinder-bestiary-2-items/injH2iWqg5QjQiqs.htm)|Blade|auto-trad|
|[InntbXg8SWCGpsZF.htm](pathfinder-bestiary-2-items/InntbXg8SWCGpsZF.htm)|Telepathy|auto-trad|
|[IoE3s3hLaVewsjSQ.htm](pathfinder-bestiary-2-items/IoE3s3hLaVewsjSQ.htm)|Storm Aura|auto-trad|
|[IoKSTbFElqXPiGiX.htm](pathfinder-bestiary-2-items/IoKSTbFElqXPiGiX.htm)|Primal Innate Spells|auto-trad|
|[Ipe3znrfNfiAvRCl.htm](pathfinder-bestiary-2-items/Ipe3znrfNfiAvRCl.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Ipf389K0Mqyu4zcP.htm](pathfinder-bestiary-2-items/Ipf389K0Mqyu4zcP.htm)|Scent|auto-trad|
|[ipGw6L7E6GUS565c.htm](pathfinder-bestiary-2-items/ipGw6L7E6GUS565c.htm)|Hypnosis|auto-trad|
|[iPIBtZVZ5TaPEFp6.htm](pathfinder-bestiary-2-items/iPIBtZVZ5TaPEFp6.htm)|Darkvision|auto-trad|
|[IPLCR3K5wWzQiSHK.htm](pathfinder-bestiary-2-items/IPLCR3K5wWzQiSHK.htm)|Discharge|auto-trad|
|[IpYr4SLkhnzKvpdC.htm](pathfinder-bestiary-2-items/IpYr4SLkhnzKvpdC.htm)|At-Will Spells|auto-trad|
|[iqDrSwSZaUvRJKuG.htm](pathfinder-bestiary-2-items/iqDrSwSZaUvRJKuG.htm)|Grab|auto-trad|
|[IqEYrNctOMJsWe6k.htm](pathfinder-bestiary-2-items/IqEYrNctOMJsWe6k.htm)|Primal Prepared Spells|auto-trad|
|[IQsStyO8N3SIqIw2.htm](pathfinder-bestiary-2-items/IQsStyO8N3SIqIw2.htm)|Scent|auto-trad|
|[Ir6SQ9XsXTFTSPKi.htm](pathfinder-bestiary-2-items/Ir6SQ9XsXTFTSPKi.htm)|Protected by the Ancestors|auto-trad|
|[iRcU8dloUGz1q7VC.htm](pathfinder-bestiary-2-items/iRcU8dloUGz1q7VC.htm)|Claw|auto-trad|
|[iRmxw9CTYdvk3WOX.htm](pathfinder-bestiary-2-items/iRmxw9CTYdvk3WOX.htm)|Gatekeeper Aura|auto-trad|
|[irQzM9Rv3zR8bh6X.htm](pathfinder-bestiary-2-items/irQzM9Rv3zR8bh6X.htm)|Whirling Slice|auto-trad|
|[IRXDgrccWGdvA4YK.htm](pathfinder-bestiary-2-items/IRXDgrccWGdvA4YK.htm)|Ferocity|auto-trad|
|[iRZgPtkf6Lw3MzYm.htm](pathfinder-bestiary-2-items/iRZgPtkf6Lw3MzYm.htm)|Constant Spells|auto-trad|
|[IS4BmsREr7eTZUPO.htm](pathfinder-bestiary-2-items/IS4BmsREr7eTZUPO.htm)|Claw|auto-trad|
|[is74odjgQwqelbqi.htm](pathfinder-bestiary-2-items/is74odjgQwqelbqi.htm)|Volcanic Purge|auto-trad|
|[ISiKvYdK9qbVBsG0.htm](pathfinder-bestiary-2-items/ISiKvYdK9qbVBsG0.htm)|Fangs|auto-trad|
|[ISU9cvmrd38iqIo6.htm](pathfinder-bestiary-2-items/ISU9cvmrd38iqIo6.htm)|Kimono|auto-trad|
|[isUs3BDvVUggXstu.htm](pathfinder-bestiary-2-items/isUs3BDvVUggXstu.htm)|Shadow Traveler|auto-trad|
|[iSVPM2m77YXV78mJ.htm](pathfinder-bestiary-2-items/iSVPM2m77YXV78mJ.htm)|Badger Rage|auto-trad|
|[iSvQy8CUUD5u4nsG.htm](pathfinder-bestiary-2-items/iSvQy8CUUD5u4nsG.htm)|Mandibles|auto-trad|
|[iT766eWfAydCiJYK.htm](pathfinder-bestiary-2-items/iT766eWfAydCiJYK.htm)|Mandibles|auto-trad|
|[iThujq9Fhag6Jad2.htm](pathfinder-bestiary-2-items/iThujq9Fhag6Jad2.htm)|Grip Throat|auto-trad|
|[itoyZJLBh98BavI1.htm](pathfinder-bestiary-2-items/itoyZJLBh98BavI1.htm)|Entrench|auto-trad|
|[iTVvlmvMKiOcRmTQ.htm](pathfinder-bestiary-2-items/iTVvlmvMKiOcRmTQ.htm)|Ferocity|auto-trad|
|[iuqPDrdlLQjQi1NU.htm](pathfinder-bestiary-2-items/iuqPDrdlLQjQi1NU.htm)|Bastard Sword|auto-trad|
|[IvFL4oT7wjrxQZqw.htm](pathfinder-bestiary-2-items/IvFL4oT7wjrxQZqw.htm)|Claw|auto-trad|
|[iVrKx2VXmKTgxWvi.htm](pathfinder-bestiary-2-items/iVrKx2VXmKTgxWvi.htm)|Claw|auto-trad|
|[IvU2EF3nyrIZUdjp.htm](pathfinder-bestiary-2-items/IvU2EF3nyrIZUdjp.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[IVUIIQ9KDB8DHfvL.htm](pathfinder-bestiary-2-items/IVUIIQ9KDB8DHfvL.htm)|Jaws|auto-trad|
|[ivy8kuAr2Ml8VdAF.htm](pathfinder-bestiary-2-items/ivy8kuAr2Ml8VdAF.htm)|Cold Adaptation|auto-trad|
|[IW1NCko3S3NUTExU.htm](pathfinder-bestiary-2-items/IW1NCko3S3NUTExU.htm)|Knockdown|auto-trad|
|[iw7xcu7OkpYHYyQ2.htm](pathfinder-bestiary-2-items/iw7xcu7OkpYHYyQ2.htm)|Greater Web Sense|auto-trad|
|[IWFZQHALE4qFDWJv.htm](pathfinder-bestiary-2-items/IWFZQHALE4qFDWJv.htm)|Snow Vision|auto-trad|
|[IWIgUhii4TYY31l7.htm](pathfinder-bestiary-2-items/IWIgUhii4TYY31l7.htm)|At-Will Spells|auto-trad|
|[iWjW5DElqzkofyBS.htm](pathfinder-bestiary-2-items/iWjW5DElqzkofyBS.htm)|Fist|auto-trad|
|[IwPCTJvgvDQC90Yj.htm](pathfinder-bestiary-2-items/IwPCTJvgvDQC90Yj.htm)|Hurl Net|auto-trad|
|[IWSVjUBcLTretOIM.htm](pathfinder-bestiary-2-items/IWSVjUBcLTretOIM.htm)|Claw|auto-trad|
|[IWyA3McLAMxHc0jq.htm](pathfinder-bestiary-2-items/IWyA3McLAMxHc0jq.htm)|Capsize|auto-trad|
|[iWYCpNJjdadjk8JX.htm](pathfinder-bestiary-2-items/iWYCpNJjdadjk8JX.htm)|Briny Wound|auto-trad|
|[Ixt0Bpsg2FgwTGiy.htm](pathfinder-bestiary-2-items/Ixt0Bpsg2FgwTGiy.htm)|Dispelling Roar|auto-trad|
|[IxVJURBO9NY3Og9k.htm](pathfinder-bestiary-2-items/IxVJURBO9NY3Og9k.htm)|Primal Innate Spells|auto-trad|
|[ixXRVHF3jg0IMuKw.htm](pathfinder-bestiary-2-items/ixXRVHF3jg0IMuKw.htm)|Leprechaun Magic|auto-trad|
|[iYT6OEbdoPMem0XE.htm](pathfinder-bestiary-2-items/iYT6OEbdoPMem0XE.htm)|Children of the Night|auto-trad|
|[IZ5Q9oQE8jylYahf.htm](pathfinder-bestiary-2-items/IZ5Q9oQE8jylYahf.htm)|Ultrasonic Blast|auto-trad|
|[iZA7ek0jbfpt5FDo.htm](pathfinder-bestiary-2-items/iZA7ek0jbfpt5FDo.htm)|Attack of Opportunity|auto-trad|
|[izA7GPk45mKcDG02.htm](pathfinder-bestiary-2-items/izA7GPk45mKcDG02.htm)|Giant Ant Venom|auto-trad|
|[IzbfeuZKm6hz1cWS.htm](pathfinder-bestiary-2-items/IzbfeuZKm6hz1cWS.htm)|Darkvision|auto-trad|
|[IZKppOuI7oVJKs5K.htm](pathfinder-bestiary-2-items/IZKppOuI7oVJKs5K.htm)|Scent|auto-trad|
|[IZVxmKtbXiti9BZx.htm](pathfinder-bestiary-2-items/IZVxmKtbXiti9BZx.htm)|Cacophonous Roar|auto-trad|
|[iZxDrmolCkhIxWO6.htm](pathfinder-bestiary-2-items/iZxDrmolCkhIxWO6.htm)|Ranged Trip|auto-trad|
|[J0PFKoQWakUEDyIJ.htm](pathfinder-bestiary-2-items/J0PFKoQWakUEDyIJ.htm)|Shortsword|auto-trad|
|[J0ri0JZTtI3g3kxN.htm](pathfinder-bestiary-2-items/J0ri0JZTtI3g3kxN.htm)|Change Shape|auto-trad|
|[J1OKxBmPeSehcjj9.htm](pathfinder-bestiary-2-items/J1OKxBmPeSehcjj9.htm)|Breath Weapon|auto-trad|
|[J2LUtQlsnbpMEQ1r.htm](pathfinder-bestiary-2-items/J2LUtQlsnbpMEQ1r.htm)|Tail|auto-trad|
|[j2XC7qI2EMqGvGSs.htm](pathfinder-bestiary-2-items/j2XC7qI2EMqGvGSs.htm)|Low-Light Vision|auto-trad|
|[j45FbeObppNsRIhN.htm](pathfinder-bestiary-2-items/j45FbeObppNsRIhN.htm)|Darkvision|auto-trad|
|[j4OHhvOuFnXfbDjB.htm](pathfinder-bestiary-2-items/j4OHhvOuFnXfbDjB.htm)|Spell Feedback|auto-trad|
|[J4SHx8p5wCH9xlMo.htm](pathfinder-bestiary-2-items/J4SHx8p5wCH9xlMo.htm)|Clench Jaws|auto-trad|
|[J7vnMM93liaoRzjV.htm](pathfinder-bestiary-2-items/J7vnMM93liaoRzjV.htm)|Primal Innate Spells|auto-trad|
|[J83izDOsrGU2y7y6.htm](pathfinder-bestiary-2-items/J83izDOsrGU2y7y6.htm)|Lance Arm|auto-trad|
|[j8BUnBP4Ahs3QmLx.htm](pathfinder-bestiary-2-items/j8BUnBP4Ahs3QmLx.htm)|Swarm Mind|auto-trad|
|[j8GeF2Jp36lFRgyF.htm](pathfinder-bestiary-2-items/j8GeF2Jp36lFRgyF.htm)|Draconic Frenzy|auto-trad|
|[j8i5xeCEDDiWbh3z.htm](pathfinder-bestiary-2-items/j8i5xeCEDDiWbh3z.htm)|Fist|auto-trad|
|[j8Wq9vNUrxbBHNGK.htm](pathfinder-bestiary-2-items/j8Wq9vNUrxbBHNGK.htm)|Throw Rock|auto-trad|
|[J9iEljd9P9EPB4wm.htm](pathfinder-bestiary-2-items/J9iEljd9P9EPB4wm.htm)|Grab|auto-trad|
|[j9L9BPR6t7eHpHAt.htm](pathfinder-bestiary-2-items/j9L9BPR6t7eHpHAt.htm)|Breath Weapon|auto-trad|
|[JAh7SWOTsIXvrckz.htm](pathfinder-bestiary-2-items/JAh7SWOTsIXvrckz.htm)|Water Travel|auto-trad|
|[JakacQ9SCpgoCpCp.htm](pathfinder-bestiary-2-items/JakacQ9SCpgoCpCp.htm)|Shocking Burst|auto-trad|
|[jBybuNj39jNUTdkz.htm](pathfinder-bestiary-2-items/jBybuNj39jNUTdkz.htm)|Scent Demons 60 feet|auto-trad|
|[jC3hvioFdJ495oDv.htm](pathfinder-bestiary-2-items/jC3hvioFdJ495oDv.htm)|Crystallize Flesh|auto-trad|
|[JcDvmYqI4o86358N.htm](pathfinder-bestiary-2-items/JcDvmYqI4o86358N.htm)|Grievous Strike|auto-trad|
|[jCQO2RfqPLKqDeuF.htm](pathfinder-bestiary-2-items/jCQO2RfqPLKqDeuF.htm)|Slow Susceptibility|auto-trad|
|[Jd5I0ORcv8JckftL.htm](pathfinder-bestiary-2-items/Jd5I0ORcv8JckftL.htm)|Stormsight|auto-trad|
|[jdfDA1e2B3iIQzhU.htm](pathfinder-bestiary-2-items/jdfDA1e2B3iIQzhU.htm)|Divine Innate Spells|auto-trad|
|[JdsDXPVL1v4JEQt5.htm](pathfinder-bestiary-2-items/JdsDXPVL1v4JEQt5.htm)|Wing|auto-trad|
|[jDvNUVTVAmMni2Va.htm](pathfinder-bestiary-2-items/jDvNUVTVAmMni2Va.htm)|Aura of Vitality|auto-trad|
|[JdyDKp2bgIArfQmi.htm](pathfinder-bestiary-2-items/JdyDKp2bgIArfQmi.htm)|Improved Grab|auto-trad|
|[jEaI3soXVIxnAcPe.htm](pathfinder-bestiary-2-items/jEaI3soXVIxnAcPe.htm)|Breath Weapon|auto-trad|
|[jeV0r6dAZqnh6sD8.htm](pathfinder-bestiary-2-items/jeV0r6dAZqnh6sD8.htm)|Claw|auto-trad|
|[jfL2tU9w9oSiQlmg.htm](pathfinder-bestiary-2-items/jfL2tU9w9oSiQlmg.htm)|Enhance Venom|auto-trad|
|[JfR3IkhT8g6aU9ry.htm](pathfinder-bestiary-2-items/JfR3IkhT8g6aU9ry.htm)|Scythe Branch|auto-trad|
|[jG3O965IQDZgxg9B.htm](pathfinder-bestiary-2-items/jG3O965IQDZgxg9B.htm)|Attack of Opportunity|auto-trad|
|[JGCkaVnAvnwAxWOp.htm](pathfinder-bestiary-2-items/JGCkaVnAvnwAxWOp.htm)|Mauler|auto-trad|
|[JGEy5BcX7MQ17sdA.htm](pathfinder-bestiary-2-items/JGEy5BcX7MQ17sdA.htm)|Vrykolakas Vulnerabilities|auto-trad|
|[jGhebYMT6hWNMixj.htm](pathfinder-bestiary-2-items/jGhebYMT6hWNMixj.htm)|Constant Spells|auto-trad|
|[jgUPBrXeWbjnI6f1.htm](pathfinder-bestiary-2-items/jgUPBrXeWbjnI6f1.htm)|Attack of Opportunity|auto-trad|
|[JgV2QrmvVWjF51JT.htm](pathfinder-bestiary-2-items/JgV2QrmvVWjF51JT.htm)|At-Will Spells|auto-trad|
|[JGXYh66auv2dCHDv.htm](pathfinder-bestiary-2-items/JGXYh66auv2dCHDv.htm)|Volcanic Eruption|auto-trad|
|[JHlhwEGUybavtbTq.htm](pathfinder-bestiary-2-items/JHlhwEGUybavtbTq.htm)|Living Form|auto-trad|
|[JIAc2IH8Ot3uIXfD.htm](pathfinder-bestiary-2-items/JIAc2IH8Ot3uIXfD.htm)|Trample|auto-trad|
|[jICQ6Y5JgzCUZXC3.htm](pathfinder-bestiary-2-items/jICQ6Y5JgzCUZXC3.htm)|Hurled Barb|auto-trad|
|[JIKJPPU6aBax4pjD.htm](pathfinder-bestiary-2-items/JIKJPPU6aBax4pjD.htm)|Mist Vision|auto-trad|
|[JImKJVdKy1YbBpwf.htm](pathfinder-bestiary-2-items/JImKJVdKy1YbBpwf.htm)|Mandibles|auto-trad|
|[jISdvlh9f3wIBxZ8.htm](pathfinder-bestiary-2-items/jISdvlh9f3wIBxZ8.htm)|Freezing Blood|auto-trad|
|[jJ8meK8Q70uy8CHT.htm](pathfinder-bestiary-2-items/jJ8meK8Q70uy8CHT.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[jJAZZAqxbrSxGLXv.htm](pathfinder-bestiary-2-items/jJAZZAqxbrSxGLXv.htm)|Wing|auto-trad|
|[jJMWnNtJ3dst8bcj.htm](pathfinder-bestiary-2-items/jJMWnNtJ3dst8bcj.htm)|Drain Vigor|auto-trad|
|[JJTobEFnvJPV524S.htm](pathfinder-bestiary-2-items/JJTobEFnvJPV524S.htm)|Vorpal Fear|auto-trad|
|[JK7loVXQ7UJJt3F9.htm](pathfinder-bestiary-2-items/JK7loVXQ7UJJt3F9.htm)|Fists of Thunder and Lightning|auto-trad|
|[jK8jhXaoNnEEDRLV.htm](pathfinder-bestiary-2-items/jK8jhXaoNnEEDRLV.htm)|Darkvision|auto-trad|
|[jKCuP9xlPQoqlnGq.htm](pathfinder-bestiary-2-items/jKCuP9xlPQoqlnGq.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[JkVCc433MJHRl0qG.htm](pathfinder-bestiary-2-items/JkVCc433MJHRl0qG.htm)|Darkvision|auto-trad|
|[JLgWzofKmiq0ajpJ.htm](pathfinder-bestiary-2-items/JLgWzofKmiq0ajpJ.htm)|Draconic Frenzy|auto-trad|
|[JLM9pHvLtJxtkDH5.htm](pathfinder-bestiary-2-items/JLM9pHvLtJxtkDH5.htm)|Breath Weapon|auto-trad|
|[JlQUb1sbsYfmz5Vi.htm](pathfinder-bestiary-2-items/JlQUb1sbsYfmz5Vi.htm)|Cloak in Embers|auto-trad|
|[JLqzN2RUPFKa8beH.htm](pathfinder-bestiary-2-items/JLqzN2RUPFKa8beH.htm)|Reflect Spell|auto-trad|
|[jM9qG2FB1AcJMMiL.htm](pathfinder-bestiary-2-items/jM9qG2FB1AcJMMiL.htm)|Darkvision|auto-trad|
|[JMoMmAd0UH6b2Ckg.htm](pathfinder-bestiary-2-items/JMoMmAd0UH6b2Ckg.htm)|Telepathy|auto-trad|
|[jN9qLXiQfaOBlFfC.htm](pathfinder-bestiary-2-items/jN9qLXiQfaOBlFfC.htm)|Ramming Speed|auto-trad|
|[jNQXKdaz3dr7h6s3.htm](pathfinder-bestiary-2-items/jNQXKdaz3dr7h6s3.htm)|Jaws|auto-trad|
|[JoBMwImgYPvlh4zo.htm](pathfinder-bestiary-2-items/JoBMwImgYPvlh4zo.htm)|Darkvision|auto-trad|
|[joBz08Vy0TnC5ELN.htm](pathfinder-bestiary-2-items/joBz08Vy0TnC5ELN.htm)|Jaws|auto-trad|
|[JOeW89FGrajDYQsW.htm](pathfinder-bestiary-2-items/JOeW89FGrajDYQsW.htm)|Vrykolakas Vulnerabilities|auto-trad|
|[jOuIuz89du1DGTFh.htm](pathfinder-bestiary-2-items/jOuIuz89du1DGTFh.htm)|Tongue|auto-trad|
|[Joyek0dGuGD8dX9d.htm](pathfinder-bestiary-2-items/Joyek0dGuGD8dX9d.htm)|Divine Innate Spells|auto-trad|
|[JpDIWcdXAS2ZFRY1.htm](pathfinder-bestiary-2-items/JpDIWcdXAS2ZFRY1.htm)|Primal Prepared Spells|auto-trad|
|[jpeqjLFSGfQaHqwb.htm](pathfinder-bestiary-2-items/jpeqjLFSGfQaHqwb.htm)|Bite|auto-trad|
|[jppcxZjbxQ0e8W30.htm](pathfinder-bestiary-2-items/jppcxZjbxQ0e8W30.htm)|At-Will Spells|auto-trad|
|[jQ0BoxOUmGdOkGIf.htm](pathfinder-bestiary-2-items/jQ0BoxOUmGdOkGIf.htm)|Constrict|auto-trad|
|[Jqh53awrV2X4sb81.htm](pathfinder-bestiary-2-items/Jqh53awrV2X4sb81.htm)|Occult Innate Spells|auto-trad|
|[JqoRYjfcXOg9zHSr.htm](pathfinder-bestiary-2-items/JqoRYjfcXOg9zHSr.htm)|Darkvision|auto-trad|
|[JqWD8UssH8Z7OeZz.htm](pathfinder-bestiary-2-items/JqWD8UssH8Z7OeZz.htm)|Limb Extension|auto-trad|
|[jr6lRjG0ZeZgW116.htm](pathfinder-bestiary-2-items/jr6lRjG0ZeZgW116.htm)|Soul Scream|auto-trad|
|[JRi9pHjxAIEGIlv1.htm](pathfinder-bestiary-2-items/JRi9pHjxAIEGIlv1.htm)|Tentacle|auto-trad|
|[JS74Gdm0nVhPpG4k.htm](pathfinder-bestiary-2-items/JS74Gdm0nVhPpG4k.htm)|Mindwarping|auto-trad|
|[jskRIHujbQ0xJ8oy.htm](pathfinder-bestiary-2-items/jskRIHujbQ0xJ8oy.htm)|Frightful Presence|auto-trad|
|[JSpF7t2LpyYp70eW.htm](pathfinder-bestiary-2-items/JSpF7t2LpyYp70eW.htm)|Primal Innate Spells|auto-trad|
|[Jspfa7SNI6gNhPK4.htm](pathfinder-bestiary-2-items/Jspfa7SNI6gNhPK4.htm)|Coven|auto-trad|
|[jtcA1Dklz2pAKWhI.htm](pathfinder-bestiary-2-items/jtcA1Dklz2pAKWhI.htm)|Ghost Bane|auto-trad|
|[jtcRrpxqpR8quMII.htm](pathfinder-bestiary-2-items/jtcRrpxqpR8quMII.htm)|Quick Invisibility|auto-trad|
|[JtEaK5YV5AgypBAF.htm](pathfinder-bestiary-2-items/JtEaK5YV5AgypBAF.htm)|Change Shape|auto-trad|
|[jTkhIBakBrDpEBuu.htm](pathfinder-bestiary-2-items/jTkhIBakBrDpEBuu.htm)|At-Will Spells|auto-trad|
|[JTKyH2bv7vq73eF0.htm](pathfinder-bestiary-2-items/JTKyH2bv7vq73eF0.htm)|Whiptail Centipede Venom|auto-trad|
|[jToSRHKjIQK8vQjE.htm](pathfinder-bestiary-2-items/jToSRHKjIQK8vQjE.htm)|Claw|auto-trad|
|[jTPTmoj95k6FZSf8.htm](pathfinder-bestiary-2-items/jTPTmoj95k6FZSf8.htm)|Darkvision|auto-trad|
|[jURll9yGhDX2eIFe.htm](pathfinder-bestiary-2-items/jURll9yGhDX2eIFe.htm)|Occult Innate Spells|auto-trad|
|[JVezHGtBp5knZ21Z.htm](pathfinder-bestiary-2-items/JVezHGtBp5knZ21Z.htm)|Jaws|auto-trad|
|[jVgZ19s2KzgHUo1a.htm](pathfinder-bestiary-2-items/jVgZ19s2KzgHUo1a.htm)|Tentacle|auto-trad|
|[jVnAGrnHMY0ZZTAt.htm](pathfinder-bestiary-2-items/jVnAGrnHMY0ZZTAt.htm)|Negative Healing|auto-trad|
|[JVxGmdUEcMuZmGlg.htm](pathfinder-bestiary-2-items/JVxGmdUEcMuZmGlg.htm)|Light Pulse|auto-trad|
|[Jw5v3XqSesAoiBsp.htm](pathfinder-bestiary-2-items/Jw5v3XqSesAoiBsp.htm)|Reactive Shock|auto-trad|
|[Jw8Rmo5ZFh9ouq86.htm](pathfinder-bestiary-2-items/Jw8Rmo5ZFh9ouq86.htm)|Ice Burrow|auto-trad|
|[JWaVcBKV9F51VWja.htm](pathfinder-bestiary-2-items/JWaVcBKV9F51VWja.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[jWc6dwkDJvfVXXtk.htm](pathfinder-bestiary-2-items/jWc6dwkDJvfVXXtk.htm)|Trample|auto-trad|
|[jwprc3z4QABxyA1y.htm](pathfinder-bestiary-2-items/jwprc3z4QABxyA1y.htm)|Pyrexic Malaria|auto-trad|
|[JwstH3hUkDgNqT21.htm](pathfinder-bestiary-2-items/JwstH3hUkDgNqT21.htm)|Stinger|auto-trad|
|[jWt4W1OUjOcKKBNI.htm](pathfinder-bestiary-2-items/jWt4W1OUjOcKKBNI.htm)|Primal Prepared Spells|auto-trad|
|[jwZ2PMloPddIckyK.htm](pathfinder-bestiary-2-items/jwZ2PMloPddIckyK.htm)|Spirit Touch|auto-trad|
|[jXcwI039VI8MCBIE.htm](pathfinder-bestiary-2-items/jXcwI039VI8MCBIE.htm)|Rock|auto-trad|
|[JxE1ccJO8ctrpivc.htm](pathfinder-bestiary-2-items/JxE1ccJO8ctrpivc.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[jXl0E4vOdMPzQ3Hi.htm](pathfinder-bestiary-2-items/jXl0E4vOdMPzQ3Hi.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[jzbdXMuXge2oT2ol.htm](pathfinder-bestiary-2-items/jzbdXMuXge2oT2ol.htm)|Wolverine Rage|auto-trad|
|[JzdZZiLkLG0WmOid.htm](pathfinder-bestiary-2-items/JzdZZiLkLG0WmOid.htm)|Low-Light Vision|auto-trad|
|[jzI4ObWhN1kXyFjY.htm](pathfinder-bestiary-2-items/jzI4ObWhN1kXyFjY.htm)|Attack of Opportunity|auto-trad|
|[jZkG8pIe4dAne06Q.htm](pathfinder-bestiary-2-items/jZkG8pIe4dAne06Q.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[jzVssayBq4mzMU9u.htm](pathfinder-bestiary-2-items/jzVssayBq4mzMU9u.htm)|Darkvision|auto-trad|
|[JzWnn3KoGale3jgX.htm](pathfinder-bestiary-2-items/JzWnn3KoGale3jgX.htm)|Claw|auto-trad|
|[k0yAlAVkZbGrXfX3.htm](pathfinder-bestiary-2-items/k0yAlAVkZbGrXfX3.htm)|Jaws|auto-trad|
|[K1woMBH8Ut9J50As.htm](pathfinder-bestiary-2-items/K1woMBH8Ut9J50As.htm)|Foot|auto-trad|
|[k2d7PB0MeYOIfjcj.htm](pathfinder-bestiary-2-items/k2d7PB0MeYOIfjcj.htm)|Filth Fever|auto-trad|
|[k2HAX2N3qaYcbpb8.htm](pathfinder-bestiary-2-items/k2HAX2N3qaYcbpb8.htm)|Greater Darkvision|auto-trad|
|[K32GntqM2YIDwGgh.htm](pathfinder-bestiary-2-items/K32GntqM2YIDwGgh.htm)|Scent|auto-trad|
|[k3o7ZpWc6J6flWCQ.htm](pathfinder-bestiary-2-items/k3o7ZpWc6J6flWCQ.htm)|Breach|auto-trad|
|[K45PxGQvJlwB2Y0j.htm](pathfinder-bestiary-2-items/K45PxGQvJlwB2Y0j.htm)|Shield Block|auto-trad|
|[k4tbzCH8jMb0d1Qk.htm](pathfinder-bestiary-2-items/k4tbzCH8jMb0d1Qk.htm)|Leng Spider Venom|auto-trad|
|[K5BMaVGszyeB9sm8.htm](pathfinder-bestiary-2-items/K5BMaVGszyeB9sm8.htm)|Constant Spells|auto-trad|
|[k6tK5Cya0h5SEzNQ.htm](pathfinder-bestiary-2-items/k6tK5Cya0h5SEzNQ.htm)|Rend|auto-trad|
|[k7IjtCkUuIzOv0dz.htm](pathfinder-bestiary-2-items/k7IjtCkUuIzOv0dz.htm)|Frightful Presence|auto-trad|
|[K7yXQ4PZPINLQXnj.htm](pathfinder-bestiary-2-items/K7yXQ4PZPINLQXnj.htm)|Darkvision|auto-trad|
|[K80GtqxqKN7Og68w.htm](pathfinder-bestiary-2-items/K80GtqxqKN7Og68w.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[K8JEIxdXOOyv0Mm6.htm](pathfinder-bestiary-2-items/K8JEIxdXOOyv0Mm6.htm)|Darkvision|auto-trad|
|[k8MjR75C6SWEa6gd.htm](pathfinder-bestiary-2-items/k8MjR75C6SWEa6gd.htm)|Flytrap Mouth|auto-trad|
|[k8sBT0SGwUD3ZUuE.htm](pathfinder-bestiary-2-items/k8sBT0SGwUD3ZUuE.htm)|+2 Status to All Saves vs. Divine Magic|auto-trad|
|[k8uRKWJkW8KHtabq.htm](pathfinder-bestiary-2-items/k8uRKWJkW8KHtabq.htm)|Grab|auto-trad|
|[kAgFhMe8TiBlKO3j.htm](pathfinder-bestiary-2-items/kAgFhMe8TiBlKO3j.htm)|At-Will Spells|auto-trad|
|[KBUEqfW6m7bI5a0g.htm](pathfinder-bestiary-2-items/KBUEqfW6m7bI5a0g.htm)|Thunderstrike|auto-trad|
|[kbVyC3rbr7rfXsxu.htm](pathfinder-bestiary-2-items/kbVyC3rbr7rfXsxu.htm)|Divine Innate Spells|auto-trad|
|[kBxRAEuGBWNPM636.htm](pathfinder-bestiary-2-items/kBxRAEuGBWNPM636.htm)|Language Adaptation|auto-trad|
|[kbzaYm7nhy7Ip1Tj.htm](pathfinder-bestiary-2-items/kbzaYm7nhy7Ip1Tj.htm)|Grab|auto-trad|
|[kbzgtxi6mitCaLGP.htm](pathfinder-bestiary-2-items/kbzgtxi6mitCaLGP.htm)|Grant Desire|auto-trad|
|[kCNQjJs87QDFBoGR.htm](pathfinder-bestiary-2-items/kCNQjJs87QDFBoGR.htm)|Attack of Opportunity|auto-trad|
|[kcRvfKLlgq9sdmsj.htm](pathfinder-bestiary-2-items/kcRvfKLlgq9sdmsj.htm)|Golem Antimagic|auto-trad|
|[KdGuOd6jczb7aCb5.htm](pathfinder-bestiary-2-items/KdGuOd6jczb7aCb5.htm)|Scent|auto-trad|
|[KDUeQyj1J71Vb3Q1.htm](pathfinder-bestiary-2-items/KDUeQyj1J71Vb3Q1.htm)|Absorb Wraith|auto-trad|
|[KdVfLfgHwYXdIYXG.htm](pathfinder-bestiary-2-items/KdVfLfgHwYXdIYXG.htm)|Jaws|auto-trad|
|[kDZcpk5GUe5jUU73.htm](pathfinder-bestiary-2-items/kDZcpk5GUe5jUU73.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[keXbMIiyh6Q6tjbr.htm](pathfinder-bestiary-2-items/keXbMIiyh6Q6tjbr.htm)|Darkvision|auto-trad|
|[KF7pVWIzuG4o2wMP.htm](pathfinder-bestiary-2-items/KF7pVWIzuG4o2wMP.htm)|Holy Mace|auto-trad|
|[KF8TJluc2GTgVJhl.htm](pathfinder-bestiary-2-items/KF8TJluc2GTgVJhl.htm)|Paddle|auto-trad|
|[kFLwkDqwtyueOpyG.htm](pathfinder-bestiary-2-items/kFLwkDqwtyueOpyG.htm)|Breath Weapon|auto-trad|
|[kFNxTYgL056it5oI.htm](pathfinder-bestiary-2-items/kFNxTYgL056it5oI.htm)|Occult Innate Spells|auto-trad|
|[kFT6c7OlCSLY8QF1.htm](pathfinder-bestiary-2-items/kFT6c7OlCSLY8QF1.htm)|Regeneration 15 (deactivated by acid or fire)|auto-trad|
|[kGJwFA2RdVWHs6ty.htm](pathfinder-bestiary-2-items/kGJwFA2RdVWHs6ty.htm)|Fly Pox|auto-trad|
|[kGnrd8Q7x3x8myE3.htm](pathfinder-bestiary-2-items/kGnrd8Q7x3x8myE3.htm)|Tremorsense|auto-trad|
|[KgYaEP7QLp7DpYYS.htm](pathfinder-bestiary-2-items/KgYaEP7QLp7DpYYS.htm)|Constant Spells|auto-trad|
|[kGZni5n9AHCuIa7c.htm](pathfinder-bestiary-2-items/kGZni5n9AHCuIa7c.htm)|Vicious Criticals|auto-trad|
|[khgGPSxgTQcgev3o.htm](pathfinder-bestiary-2-items/khgGPSxgTQcgev3o.htm)|Deep Breath|auto-trad|
|[kHid4k2E9vYIzPDP.htm](pathfinder-bestiary-2-items/kHid4k2E9vYIzPDP.htm)|Claw|auto-trad|
|[khwh43CDMROkIfgf.htm](pathfinder-bestiary-2-items/khwh43CDMROkIfgf.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[ki1JmOjsmc5eRI42.htm](pathfinder-bestiary-2-items/ki1JmOjsmc5eRI42.htm)|Jaws|auto-trad|
|[KIhN4LqmxLYnSLJj.htm](pathfinder-bestiary-2-items/KIhN4LqmxLYnSLJj.htm)|Change Shape|auto-trad|
|[kIvuH6nOKhB0zVQI.htm](pathfinder-bestiary-2-items/kIvuH6nOKhB0zVQI.htm)|Jaws|auto-trad|
|[kj7lFKS7xdj7h83n.htm](pathfinder-bestiary-2-items/kj7lFKS7xdj7h83n.htm)|Change Shape|auto-trad|
|[kjaeBRLTBCSOl9cS.htm](pathfinder-bestiary-2-items/kjaeBRLTBCSOl9cS.htm)|Swallow Whole|auto-trad|
|[kJAxB9HM1zsPkPDC.htm](pathfinder-bestiary-2-items/kJAxB9HM1zsPkPDC.htm)|Tentacle|auto-trad|
|[KJKzoE0ci2sMwyFX.htm](pathfinder-bestiary-2-items/KJKzoE0ci2sMwyFX.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[KJVejaONCwwGt9ka.htm](pathfinder-bestiary-2-items/KJVejaONCwwGt9ka.htm)|Buck|auto-trad|
|[Kk860My0b7DPBnAo.htm](pathfinder-bestiary-2-items/Kk860My0b7DPBnAo.htm)|Darkvision|auto-trad|
|[KkNlymsNYCYGKgtS.htm](pathfinder-bestiary-2-items/KkNlymsNYCYGKgtS.htm)|Rend|auto-trad|
|[kKubgrbJBPv5ke6D.htm](pathfinder-bestiary-2-items/kKubgrbJBPv5ke6D.htm)|Tail|auto-trad|
|[kkVojFishI8AxnVn.htm](pathfinder-bestiary-2-items/kkVojFishI8AxnVn.htm)|Slime Rot|auto-trad|
|[kli2QsKUO2VSz2oy.htm](pathfinder-bestiary-2-items/kli2QsKUO2VSz2oy.htm)|Breath Weapon|auto-trad|
|[kM1AoSQF8Q3phCJt.htm](pathfinder-bestiary-2-items/kM1AoSQF8Q3phCJt.htm)|Primal Innate Spells|auto-trad|
|[KmdxRqIdBe3pc6XL.htm](pathfinder-bestiary-2-items/KmdxRqIdBe3pc6XL.htm)|Trample|auto-trad|
|[Kmh2fOosCcnUFq1X.htm](pathfinder-bestiary-2-items/Kmh2fOosCcnUFq1X.htm)|Darkvision|auto-trad|
|[kN2uT5XwOWMjkzA5.htm](pathfinder-bestiary-2-items/kN2uT5XwOWMjkzA5.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[KO7ETr6PiZ0HJeHK.htm](pathfinder-bestiary-2-items/KO7ETr6PiZ0HJeHK.htm)|Holy Greatsword|auto-trad|
|[KovaVbwNiKoGj4DJ.htm](pathfinder-bestiary-2-items/KovaVbwNiKoGj4DJ.htm)|Greater Constrict|auto-trad|
|[KpGCXx2t9NN3a85q.htm](pathfinder-bestiary-2-items/KpGCXx2t9NN3a85q.htm)|Fascinating Display|auto-trad|
|[KQ5qTss6bA4fQLVK.htm](pathfinder-bestiary-2-items/KQ5qTss6bA4fQLVK.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[KQAihkBcgo2L9lGv.htm](pathfinder-bestiary-2-items/KQAihkBcgo2L9lGv.htm)|Breath Weapon|auto-trad|
|[KQBI90QlI48BEPP3.htm](pathfinder-bestiary-2-items/KQBI90QlI48BEPP3.htm)|Giant Toad Poison|auto-trad|
|[kqeAoYGDfSoyJUyC.htm](pathfinder-bestiary-2-items/kqeAoYGDfSoyJUyC.htm)|Web|auto-trad|
|[KqYkMZZ4hM5T9rej.htm](pathfinder-bestiary-2-items/KqYkMZZ4hM5T9rej.htm)|Darkvision|auto-trad|
|[kryZwH2P5QUcvtyI.htm](pathfinder-bestiary-2-items/kryZwH2P5QUcvtyI.htm)|Split|auto-trad|
|[ksfJahOG38JYy8bt.htm](pathfinder-bestiary-2-items/ksfJahOG38JYy8bt.htm)|Scent|auto-trad|
|[KswFDiMoMctr0zFY.htm](pathfinder-bestiary-2-items/KswFDiMoMctr0zFY.htm)|Draining Glance|auto-trad|
|[kSySgkElvNOAzAXn.htm](pathfinder-bestiary-2-items/kSySgkElvNOAzAXn.htm)|Enveloping Kimono|auto-trad|
|[KtaZ7mxQIQwm19ZM.htm](pathfinder-bestiary-2-items/KtaZ7mxQIQwm19ZM.htm)|Dimensional Tether|auto-trad|
|[KTc5frqwQnvcjRY9.htm](pathfinder-bestiary-2-items/KTc5frqwQnvcjRY9.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[ktdazJyPHLCoWXur.htm](pathfinder-bestiary-2-items/ktdazJyPHLCoWXur.htm)|Sneak Attack|auto-trad|
|[KtIqgI2Oe8cbrguF.htm](pathfinder-bestiary-2-items/KtIqgI2Oe8cbrguF.htm)|Worm Trill|auto-trad|
|[KtjrcbTPGagKVhSc.htm](pathfinder-bestiary-2-items/KtjrcbTPGagKVhSc.htm)|Desiccating Bite|auto-trad|
|[KTxjm0shb9zyrh0z.htm](pathfinder-bestiary-2-items/KTxjm0shb9zyrh0z.htm)|Drain Life|auto-trad|
|[kUtQvCE4ePTqjluS.htm](pathfinder-bestiary-2-items/kUtQvCE4ePTqjluS.htm)|At-Will Spells|auto-trad|
|[kuV1kCq4ZEiH9DzH.htm](pathfinder-bestiary-2-items/kuV1kCq4ZEiH9DzH.htm)|Vulnerable to Curved Space|auto-trad|
|[kuW4IJohw5LunECQ.htm](pathfinder-bestiary-2-items/kuW4IJohw5LunECQ.htm)|Inhale Vitality|auto-trad|
|[Kv1pkf5Ma30A8n58.htm](pathfinder-bestiary-2-items/Kv1pkf5Ma30A8n58.htm)|Sickening Display|auto-trad|
|[kV8qUZKG2gXQGCKq.htm](pathfinder-bestiary-2-items/kV8qUZKG2gXQGCKq.htm)|Tremorsense|auto-trad|
|[kVhpKM2gcWYaxWvb.htm](pathfinder-bestiary-2-items/kVhpKM2gcWYaxWvb.htm)|Rock|auto-trad|
|[kVPXtEyRgZTAdwiv.htm](pathfinder-bestiary-2-items/kVPXtEyRgZTAdwiv.htm)|Wing|auto-trad|
|[KvQg0sUQsjltpY39.htm](pathfinder-bestiary-2-items/KvQg0sUQsjltpY39.htm)|Thunderbolt|auto-trad|
|[kvsmf0ENkLJSdBP2.htm](pathfinder-bestiary-2-items/kvsmf0ENkLJSdBP2.htm)|Dominate Animal|auto-trad|
|[kxiMCVyD0e1fsT96.htm](pathfinder-bestiary-2-items/kxiMCVyD0e1fsT96.htm)|Soul Ward|auto-trad|
|[kxo11YQf1psAw6Ig.htm](pathfinder-bestiary-2-items/kxo11YQf1psAw6Ig.htm)|At-Will Spells|auto-trad|
|[KXvcedAJNDy7eaCz.htm](pathfinder-bestiary-2-items/KXvcedAJNDy7eaCz.htm)|Foot|auto-trad|
|[Ky9wR2NCUma8mVz1.htm](pathfinder-bestiary-2-items/Ky9wR2NCUma8mVz1.htm)|Occult Innate Spells|auto-trad|
|[KYPHjX89IxHU4Sz3.htm](pathfinder-bestiary-2-items/KYPHjX89IxHU4Sz3.htm)|Claw|auto-trad|
|[kzg3qoq55PGMljc3.htm](pathfinder-bestiary-2-items/kzg3qoq55PGMljc3.htm)|Spear Frog Venom|auto-trad|
|[kziZybKPrc34cUoX.htm](pathfinder-bestiary-2-items/kziZybKPrc34cUoX.htm)|Jungle Stride|auto-trad|
|[Kzxltf72FUB0IXJp.htm](pathfinder-bestiary-2-items/Kzxltf72FUB0IXJp.htm)|Darkvision|auto-trad|
|[l0EHPtcSyp31Bf57.htm](pathfinder-bestiary-2-items/l0EHPtcSyp31Bf57.htm)|At-Will Spells|auto-trad|
|[l0ivHsOCu0wN1yYF.htm](pathfinder-bestiary-2-items/l0ivHsOCu0wN1yYF.htm)|Darkvision|auto-trad|
|[l0J79vRF4TUP2DXP.htm](pathfinder-bestiary-2-items/l0J79vRF4TUP2DXP.htm)|Tail|auto-trad|
|[L17R2FDChGWE5pgD.htm](pathfinder-bestiary-2-items/L17R2FDChGWE5pgD.htm)|Void Death|auto-trad|
|[L19PpXXqJh02tp7Q.htm](pathfinder-bestiary-2-items/L19PpXXqJh02tp7Q.htm)|Greater Darkvision|auto-trad|
|[l25YrJUMqQDtE3NT.htm](pathfinder-bestiary-2-items/l25YrJUMqQDtE3NT.htm)|Jaws|auto-trad|
|[L35cRaqjhItrc6Po.htm](pathfinder-bestiary-2-items/L35cRaqjhItrc6Po.htm)|Claw|auto-trad|
|[l3FDPjYAvQMi4tfJ.htm](pathfinder-bestiary-2-items/l3FDPjYAvQMi4tfJ.htm)|Darkvision|auto-trad|
|[l4eGICy0QK1EEERs.htm](pathfinder-bestiary-2-items/l4eGICy0QK1EEERs.htm)|Jaws|auto-trad|
|[l5R6X6sffDy41u9i.htm](pathfinder-bestiary-2-items/l5R6X6sffDy41u9i.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[l6mRpAGFCZ7gTIb1.htm](pathfinder-bestiary-2-items/l6mRpAGFCZ7gTIb1.htm)|Rasping Tongue|auto-trad|
|[l6ptuF1kw5FfVHfl.htm](pathfinder-bestiary-2-items/l6ptuF1kw5FfVHfl.htm)|Pus Burst|auto-trad|
|[l8iqEFTf53TY4e9n.htm](pathfinder-bestiary-2-items/l8iqEFTf53TY4e9n.htm)|Fist|auto-trad|
|[L8tZphaG7qnpPLpQ.htm](pathfinder-bestiary-2-items/L8tZphaG7qnpPLpQ.htm)|Golem Antimagic|auto-trad|
|[L8z6SDNKm57PB639.htm](pathfinder-bestiary-2-items/L8z6SDNKm57PB639.htm)|Claw|auto-trad|
|[L97RozqSBXjCukbA.htm](pathfinder-bestiary-2-items/L97RozqSBXjCukbA.htm)|Darkvision|auto-trad|
|[L9XO3TUsAurnPduF.htm](pathfinder-bestiary-2-items/L9XO3TUsAurnPduF.htm)|Jaws|auto-trad|
|[lA1KEaw1II1i4twA.htm](pathfinder-bestiary-2-items/lA1KEaw1II1i4twA.htm)|Boar Charge|auto-trad|
|[LA4f99Yw7Dk1e16d.htm](pathfinder-bestiary-2-items/LA4f99Yw7Dk1e16d.htm)|Fast Healing 2 (in dust or sand)|auto-trad|
|[LaDtFmUzWtxdlYPv.htm](pathfinder-bestiary-2-items/LaDtFmUzWtxdlYPv.htm)|Scent|auto-trad|
|[laXpsxFAPFs8EXkA.htm](pathfinder-bestiary-2-items/laXpsxFAPFs8EXkA.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[lB6z7CfDLMd5lPpG.htm](pathfinder-bestiary-2-items/lB6z7CfDLMd5lPpG.htm)|Starknife|auto-trad|
|[lbG9FKqt6rVqqKrM.htm](pathfinder-bestiary-2-items/lbG9FKqt6rVqqKrM.htm)|Low-Light Vision|auto-trad|
|[LbkW4zjvlyreuGX1.htm](pathfinder-bestiary-2-items/LbkW4zjvlyreuGX1.htm)|Fangs|auto-trad|
|[lbNjzpti2ihhuipU.htm](pathfinder-bestiary-2-items/lbNjzpti2ihhuipU.htm)|Pounce|auto-trad|
|[LbTesuVUtAbIzLRY.htm](pathfinder-bestiary-2-items/LbTesuVUtAbIzLRY.htm)|Darkvision|auto-trad|
|[LBtPHwiQcsiBIHG7.htm](pathfinder-bestiary-2-items/LBtPHwiQcsiBIHG7.htm)|Draconic Frenzy|auto-trad|
|[Lc0XTAOFODosmzys.htm](pathfinder-bestiary-2-items/Lc0XTAOFODosmzys.htm)|Arcane Innate Spells|auto-trad|
|[lcdESp8kjoyZyuYE.htm](pathfinder-bestiary-2-items/lcdESp8kjoyZyuYE.htm)|Jaws|auto-trad|
|[lCfKYASIe4pKTiK3.htm](pathfinder-bestiary-2-items/lCfKYASIe4pKTiK3.htm)|Occult Innate Spells|auto-trad|
|[LcG0o3PUyemEv5oK.htm](pathfinder-bestiary-2-items/LcG0o3PUyemEv5oK.htm)|Darkvision|auto-trad|
|[LcHF4msXaAScrZFk.htm](pathfinder-bestiary-2-items/LcHF4msXaAScrZFk.htm)|Attack of Opportunity|auto-trad|
|[LcmY1jYUhcSTLbed.htm](pathfinder-bestiary-2-items/LcmY1jYUhcSTLbed.htm)|Stormflight|auto-trad|
|[LDLwyqZVioLuytc9.htm](pathfinder-bestiary-2-items/LDLwyqZVioLuytc9.htm)|Low-Light Vision|auto-trad|
|[le0p8FsNfX4jkVzn.htm](pathfinder-bestiary-2-items/le0p8FsNfX4jkVzn.htm)|Amplify Voltage|auto-trad|
|[lflIfQcsvaJJEVL7.htm](pathfinder-bestiary-2-items/lflIfQcsvaJJEVL7.htm)|Proboscis|auto-trad|
|[LfTrwLmDBA7f2MCm.htm](pathfinder-bestiary-2-items/LfTrwLmDBA7f2MCm.htm)|Divine Innate Spells|auto-trad|
|[LFvM7oAwEJD1soNx.htm](pathfinder-bestiary-2-items/LFvM7oAwEJD1soNx.htm)|Low-Light Vision|auto-trad|
|[lgHFmVhsmZOzCgFm.htm](pathfinder-bestiary-2-items/lgHFmVhsmZOzCgFm.htm)|Returning Starknife|auto-trad|
|[lgkWnI9fHF9WMU2Q.htm](pathfinder-bestiary-2-items/lgkWnI9fHF9WMU2Q.htm)|Scent|auto-trad|
|[LGmgWdigRZKEMwNq.htm](pathfinder-bestiary-2-items/LGmgWdigRZKEMwNq.htm)|Swallow Whole|auto-trad|
|[LGq3APZstgY9LM9o.htm](pathfinder-bestiary-2-items/LGq3APZstgY9LM9o.htm)|Claw|auto-trad|
|[Lh6yYbwltdDOrlaP.htm](pathfinder-bestiary-2-items/Lh6yYbwltdDOrlaP.htm)|Pincers|auto-trad|
|[lhaBLbyRbWQceU9t.htm](pathfinder-bestiary-2-items/lhaBLbyRbWQceU9t.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[lIhEV7aWkpaAEZbm.htm](pathfinder-bestiary-2-items/lIhEV7aWkpaAEZbm.htm)|Primal Innate Spells|auto-trad|
|[lIiFggTlCNveUaJM.htm](pathfinder-bestiary-2-items/lIiFggTlCNveUaJM.htm)|Vrykolakas Vulnerabilities|auto-trad|
|[lIutGZGuGlSN1uvq.htm](pathfinder-bestiary-2-items/lIutGZGuGlSN1uvq.htm)|Fist|auto-trad|
|[lJ35tuNxwPc194AM.htm](pathfinder-bestiary-2-items/lJ35tuNxwPc194AM.htm)|Jaws|auto-trad|
|[lJ7R0wPVIxspGA72.htm](pathfinder-bestiary-2-items/lJ7R0wPVIxspGA72.htm)|Smoke Form|auto-trad|
|[LJ98Uh0Kt9H0AdSx.htm](pathfinder-bestiary-2-items/LJ98Uh0Kt9H0AdSx.htm)|Grab|auto-trad|
|[ljlBgsFIBKMNCZPZ.htm](pathfinder-bestiary-2-items/ljlBgsFIBKMNCZPZ.htm)|Camouflage|auto-trad|
|[LkKOdtSgJydv1rSu.htm](pathfinder-bestiary-2-items/LkKOdtSgJydv1rSu.htm)|Flytrap Hand|auto-trad|
|[LKQ6v7y7Jr6HEaPh.htm](pathfinder-bestiary-2-items/LKQ6v7y7Jr6HEaPh.htm)|Scent|auto-trad|
|[lL32oAteqyTNi0nT.htm](pathfinder-bestiary-2-items/lL32oAteqyTNi0nT.htm)|Hallucinatory Brine|auto-trad|
|[LmNbsiKPocWzw1cu.htm](pathfinder-bestiary-2-items/LmNbsiKPocWzw1cu.htm)|Blood Berries|auto-trad|
|[LMv9Jl8QyuoKhMpw.htm](pathfinder-bestiary-2-items/LMv9Jl8QyuoKhMpw.htm)|Spirit Touch|auto-trad|
|[Lmym2mXsN2UYnCLA.htm](pathfinder-bestiary-2-items/Lmym2mXsN2UYnCLA.htm)|Regeneration 5 (deactivated by good or silver)|auto-trad|
|[LN7JypAnmAunS0lg.htm](pathfinder-bestiary-2-items/LN7JypAnmAunS0lg.htm)|Twist the Blade|auto-trad|
|[LNbjPm5a6CTl4lko.htm](pathfinder-bestiary-2-items/LNbjPm5a6CTl4lko.htm)|Infernal Eye|auto-trad|
|[lNktyL9mwqYhh5mh.htm](pathfinder-bestiary-2-items/lNktyL9mwqYhh5mh.htm)|Scent|auto-trad|
|[loahwrlFxvmp2CXy.htm](pathfinder-bestiary-2-items/loahwrlFxvmp2CXy.htm)|Jet|auto-trad|
|[LOsTy4ZSFM2RSN4k.htm](pathfinder-bestiary-2-items/LOsTy4ZSFM2RSN4k.htm)|Rev Up|auto-trad|
|[loVBGCMa9ni2Xgvw.htm](pathfinder-bestiary-2-items/loVBGCMa9ni2Xgvw.htm)|Scent|auto-trad|
|[lOVrcn3AJGm4KWma.htm](pathfinder-bestiary-2-items/lOVrcn3AJGm4KWma.htm)|Eyes Of Flame|auto-trad|
|[lP5ul9ZfSW3FEFpa.htm](pathfinder-bestiary-2-items/lP5ul9ZfSW3FEFpa.htm)|Swarm Mind|auto-trad|
|[LPf9DIArTBdgTKrW.htm](pathfinder-bestiary-2-items/LPf9DIArTBdgTKrW.htm)|Push|auto-trad|
|[lPGPRMZYecowSzGA.htm](pathfinder-bestiary-2-items/lPGPRMZYecowSzGA.htm)|Regeneration 10 (Deactivated by Cold Iron)|auto-trad|
|[lPlAVVeLD4qqvZ8q.htm](pathfinder-bestiary-2-items/lPlAVVeLD4qqvZ8q.htm)|Trample|auto-trad|
|[LPLiXKoxMuJu4ifi.htm](pathfinder-bestiary-2-items/LPLiXKoxMuJu4ifi.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[LpnkFiQmJ2nxDYm5.htm](pathfinder-bestiary-2-items/LpnkFiQmJ2nxDYm5.htm)|Blinding Beams|auto-trad|
|[lPnyzUu12dzRoAGk.htm](pathfinder-bestiary-2-items/lPnyzUu12dzRoAGk.htm)|At-Will Spells|auto-trad|
|[lPV8I6T6I09MjTy3.htm](pathfinder-bestiary-2-items/lPV8I6T6I09MjTy3.htm)|Jaws|auto-trad|
|[LpxpKBeM4lLjGR0o.htm](pathfinder-bestiary-2-items/LpxpKBeM4lLjGR0o.htm)|Pincer|auto-trad|
|[LQBlltvEb0rVnJYA.htm](pathfinder-bestiary-2-items/LQBlltvEb0rVnJYA.htm)|Heliotrope|auto-trad|
|[lqG3xn6Di5qPS55E.htm](pathfinder-bestiary-2-items/lqG3xn6Di5qPS55E.htm)|Drain Soul|auto-trad|
|[LqIsXOTw0kTELToc.htm](pathfinder-bestiary-2-items/LqIsXOTw0kTELToc.htm)|Antler|auto-trad|
|[LqnyBEmxdZr7oIfJ.htm](pathfinder-bestiary-2-items/LqnyBEmxdZr7oIfJ.htm)|Light Sickness|auto-trad|
|[LqorFNvTu3dYKgMS.htm](pathfinder-bestiary-2-items/LqorFNvTu3dYKgMS.htm)|Divine Spontaneous Spells|auto-trad|
|[LqV3ag2bSeXmlk7D.htm](pathfinder-bestiary-2-items/LqV3ag2bSeXmlk7D.htm)|Boar Empathy|auto-trad|
|[lQZGx8hWnzcG2R6l.htm](pathfinder-bestiary-2-items/lQZGx8hWnzcG2R6l.htm)|Dazzling Brilliance|auto-trad|
|[LRAuzrNMjhe4ZYR9.htm](pathfinder-bestiary-2-items/LRAuzrNMjhe4ZYR9.htm)|Jaws|auto-trad|
|[LRu9rZ2D08Nr5Wa4.htm](pathfinder-bestiary-2-items/LRu9rZ2D08Nr5Wa4.htm)|Occult Innate Spells|auto-trad|
|[lSdAwlGX28qPPhdE.htm](pathfinder-bestiary-2-items/lSdAwlGX28qPPhdE.htm)|Scent|auto-trad|
|[lSfQ3gfSsjhNS3HW.htm](pathfinder-bestiary-2-items/lSfQ3gfSsjhNS3HW.htm)|Tremorsense|auto-trad|
|[lssPHJEGxZtK2T3A.htm](pathfinder-bestiary-2-items/lssPHJEGxZtK2T3A.htm)|Jaws|auto-trad|
|[lt0GXB5vawxWhRLk.htm](pathfinder-bestiary-2-items/lt0GXB5vawxWhRLk.htm)|Fist|auto-trad|
|[Lt44trsI3w4Nptsb.htm](pathfinder-bestiary-2-items/Lt44trsI3w4Nptsb.htm)|Jaws|auto-trad|
|[Lt5U1QzZ5Wx4rMiJ.htm](pathfinder-bestiary-2-items/Lt5U1QzZ5Wx4rMiJ.htm)|Caster Link|auto-trad|
|[LTcf2lgijfe0Cv96.htm](pathfinder-bestiary-2-items/LTcf2lgijfe0Cv96.htm)|Darkvision|auto-trad|
|[LUG7zInRwnS0c3vJ.htm](pathfinder-bestiary-2-items/LUG7zInRwnS0c3vJ.htm)|Cloud Walk|auto-trad|
|[lugrDlIHRd8kGDKs.htm](pathfinder-bestiary-2-items/lugrDlIHRd8kGDKs.htm)|Burning Swarm|auto-trad|
|[lUhvPsYzVAYDYIIZ.htm](pathfinder-bestiary-2-items/lUhvPsYzVAYDYIIZ.htm)|Primal Innate Spells|auto-trad|
|[LUKxBKPHrzVKWWNz.htm](pathfinder-bestiary-2-items/LUKxBKPHrzVKWWNz.htm)|Tongue|auto-trad|
|[Lv1GZd4xpvCGfjkz.htm](pathfinder-bestiary-2-items/Lv1GZd4xpvCGfjkz.htm)|Pollen Touch|auto-trad|
|[LV58uBPdmRP5wve2.htm](pathfinder-bestiary-2-items/LV58uBPdmRP5wve2.htm)|Negative Healing|auto-trad|
|[LvD2kKe3HyLRK3Gr.htm](pathfinder-bestiary-2-items/LvD2kKe3HyLRK3Gr.htm)|Flare Hood|auto-trad|
|[lVmzxYHnjQBBZTN4.htm](pathfinder-bestiary-2-items/lVmzxYHnjQBBZTN4.htm)|Claw|auto-trad|
|[lvSGUnxmrOKOF2tT.htm](pathfinder-bestiary-2-items/lvSGUnxmrOKOF2tT.htm)|Grab|auto-trad|
|[LwAD4kqFgkuoaJ0b.htm](pathfinder-bestiary-2-items/LwAD4kqFgkuoaJ0b.htm)|Change Shape|auto-trad|
|[LwBTFYgYLRf8gXzt.htm](pathfinder-bestiary-2-items/LwBTFYgYLRf8gXzt.htm)|Spell Reflection|auto-trad|
|[lWr6fzaJ1ya4uUVM.htm](pathfinder-bestiary-2-items/lWr6fzaJ1ya4uUVM.htm)|Deflect Arrow|auto-trad|
|[lWwRydUGrcy5WmZf.htm](pathfinder-bestiary-2-items/lWwRydUGrcy5WmZf.htm)|Change Shape|auto-trad|
|[lXe9Yv9DRWtrP5OH.htm](pathfinder-bestiary-2-items/lXe9Yv9DRWtrP5OH.htm)|Shattering Harmonics|auto-trad|
|[LXMgaHhqx5CpVHAu.htm](pathfinder-bestiary-2-items/LXMgaHhqx5CpVHAu.htm)|Thorn Volley|auto-trad|
|[lxsWZN09WFvanWVx.htm](pathfinder-bestiary-2-items/lxsWZN09WFvanWVx.htm)|Primal Innate Spells|auto-trad|
|[LXXg3Endpf7Vtn3r.htm](pathfinder-bestiary-2-items/LXXg3Endpf7Vtn3r.htm)|Constrict|auto-trad|
|[lxYAhoN0aTLMmh5g.htm](pathfinder-bestiary-2-items/lxYAhoN0aTLMmh5g.htm)|Divine Innate Spells|auto-trad|
|[LxYfnrKWPcZkzYgL.htm](pathfinder-bestiary-2-items/LxYfnrKWPcZkzYgL.htm)|Primal Innate Spells|auto-trad|
|[lY7Ggtouj0iOizqY.htm](pathfinder-bestiary-2-items/lY7Ggtouj0iOizqY.htm)|Chain|auto-trad|
|[Ly9IqnJW6Zy3JQ4y.htm](pathfinder-bestiary-2-items/Ly9IqnJW6Zy3JQ4y.htm)|Pseudopod|auto-trad|
|[LYeMRFsmLZRCk3uE.htm](pathfinder-bestiary-2-items/LYeMRFsmLZRCk3uE.htm)|Sudden Retreat|auto-trad|
|[LyhMcP0ruqAc5kCP.htm](pathfinder-bestiary-2-items/LyhMcP0ruqAc5kCP.htm)|Fist|auto-trad|
|[LYl70MYcQUy11TXM.htm](pathfinder-bestiary-2-items/LYl70MYcQUy11TXM.htm)|Improved Knockdown|auto-trad|
|[lZ2HxJLCCy5TITMC.htm](pathfinder-bestiary-2-items/lZ2HxJLCCy5TITMC.htm)|At-Will Spells|auto-trad|
|[lZ9B89A9j1PjYcdf.htm](pathfinder-bestiary-2-items/lZ9B89A9j1PjYcdf.htm)|Volcanic Purge|auto-trad|
|[lzbAKcAmDlWaRGOF.htm](pathfinder-bestiary-2-items/lzbAKcAmDlWaRGOF.htm)|Telepathy|auto-trad|
|[LzoVt6l20KHm15wv.htm](pathfinder-bestiary-2-items/LzoVt6l20KHm15wv.htm)|Radiant Beam|auto-trad|
|[LZTLNHZnYh3Eg4aU.htm](pathfinder-bestiary-2-items/LZTLNHZnYh3Eg4aU.htm)|Indispensable Savvy|auto-trad|
|[m079lrhvpcahyNqy.htm](pathfinder-bestiary-2-items/m079lrhvpcahyNqy.htm)|Primal Innate Spells|auto-trad|
|[m0MCsCQ2HU7X7Rr2.htm](pathfinder-bestiary-2-items/m0MCsCQ2HU7X7Rr2.htm)|Claw|auto-trad|
|[M14sGTx3h9uOKf65.htm](pathfinder-bestiary-2-items/M14sGTx3h9uOKf65.htm)|Darkvision|auto-trad|
|[m1Ku7DrcoF8JnIil.htm](pathfinder-bestiary-2-items/m1Ku7DrcoF8JnIil.htm)|Beak|auto-trad|
|[m1YzMXBeebVN4Cp1.htm](pathfinder-bestiary-2-items/m1YzMXBeebVN4Cp1.htm)|Telepathy 100 feet|auto-trad|
|[m39B0WvlTUpZ3tLS.htm](pathfinder-bestiary-2-items/m39B0WvlTUpZ3tLS.htm)|Darkvision|auto-trad|
|[M3YBIgGHTHFIcGRF.htm](pathfinder-bestiary-2-items/M3YBIgGHTHFIcGRF.htm)|Icicle|auto-trad|
|[M5bYj5m7ymoWlabG.htm](pathfinder-bestiary-2-items/M5bYj5m7ymoWlabG.htm)|Necrotic Decay|auto-trad|
|[M6lP0K1hMi81OdrJ.htm](pathfinder-bestiary-2-items/M6lP0K1hMi81OdrJ.htm)|Beak|auto-trad|
|[M7sZToYvFNrig8TH.htm](pathfinder-bestiary-2-items/M7sZToYvFNrig8TH.htm)|Constant Spells|auto-trad|
|[m9kHAZ6q8lhsA9dC.htm](pathfinder-bestiary-2-items/m9kHAZ6q8lhsA9dC.htm)|Deep Breath|auto-trad|
|[ma50pIyMGEpOtjhi.htm](pathfinder-bestiary-2-items/ma50pIyMGEpOtjhi.htm)|Darkvision|auto-trad|
|[mBjJ5gUJvowlXzIH.htm](pathfinder-bestiary-2-items/mBjJ5gUJvowlXzIH.htm)|Breath Weapon|auto-trad|
|[mcIvTT80sYixnl9W.htm](pathfinder-bestiary-2-items/mcIvTT80sYixnl9W.htm)|Nauseating Display|auto-trad|
|[mcOn5wJPK3EDRCeU.htm](pathfinder-bestiary-2-items/mcOn5wJPK3EDRCeU.htm)|Sneak Attack|auto-trad|
|[mcp20ANc2q921IjQ.htm](pathfinder-bestiary-2-items/mcp20ANc2q921IjQ.htm)|Claw|auto-trad|
|[mcsK2cK4pMrDIfZJ.htm](pathfinder-bestiary-2-items/mcsK2cK4pMrDIfZJ.htm)|Curse of Endless Storms|auto-trad|
|[md179yuEkGpC91XD.htm](pathfinder-bestiary-2-items/md179yuEkGpC91XD.htm)|Claw|auto-trad|
|[MdeM9TcCMZeHBGEs.htm](pathfinder-bestiary-2-items/MdeM9TcCMZeHBGEs.htm)|Constant Spells|auto-trad|
|[mdEZdShzE63nlTQK.htm](pathfinder-bestiary-2-items/mdEZdShzE63nlTQK.htm)|Salt Water Vulnerability|auto-trad|
|[MDX9dw99EkjeyPM0.htm](pathfinder-bestiary-2-items/MDX9dw99EkjeyPM0.htm)|Grab|auto-trad|
|[MeT5lDeI05fBDoW3.htm](pathfinder-bestiary-2-items/MeT5lDeI05fBDoW3.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[MEtJibVB3AjSqu2W.htm](pathfinder-bestiary-2-items/MEtJibVB3AjSqu2W.htm)|Improved Knockdown|auto-trad|
|[meXB4fiPOKy6eWwH.htm](pathfinder-bestiary-2-items/meXB4fiPOKy6eWwH.htm)|Longspear|auto-trad|
|[mey0HoXRCPNAYCqa.htm](pathfinder-bestiary-2-items/mey0HoXRCPNAYCqa.htm)|Low-Light Vision|auto-trad|
|[Mg0QNEtZXzZdfn88.htm](pathfinder-bestiary-2-items/Mg0QNEtZXzZdfn88.htm)|Low-Light Vision|auto-trad|
|[mgA0es28Vb0TAr40.htm](pathfinder-bestiary-2-items/mgA0es28Vb0TAr40.htm)|Menacing Growl|auto-trad|
|[mgF7v4Mg6J6e9WVv.htm](pathfinder-bestiary-2-items/mgF7v4Mg6J6e9WVv.htm)|Composite Longbow|auto-trad|
|[mGkbLuXtisEvFNNs.htm](pathfinder-bestiary-2-items/mGkbLuXtisEvFNNs.htm)|Fastening Leap|auto-trad|
|[MgWi09fDy6GgzHJM.htm](pathfinder-bestiary-2-items/MgWi09fDy6GgzHJM.htm)|Sinister Bite|auto-trad|
|[MH7wNM2dBDOtmzVO.htm](pathfinder-bestiary-2-items/MH7wNM2dBDOtmzVO.htm)|Sneak Attack|auto-trad|
|[mHMefxDyPXjW4mKt.htm](pathfinder-bestiary-2-items/mHMefxDyPXjW4mKt.htm)|Grab|auto-trad|
|[MhPq4EOlBLppwQeu.htm](pathfinder-bestiary-2-items/MhPq4EOlBLppwQeu.htm)|Cocytan Filth|auto-trad|
|[MiX1rLReSocMJdta.htm](pathfinder-bestiary-2-items/MiX1rLReSocMJdta.htm)|Cold Lethargy|auto-trad|
|[Mj4CtMHXmN85o3tJ.htm](pathfinder-bestiary-2-items/Mj4CtMHXmN85o3tJ.htm)|Focus Gaze|auto-trad|
|[MJIrkULpmp4soA0F.htm](pathfinder-bestiary-2-items/MJIrkULpmp4soA0F.htm)|Rolling Charge|auto-trad|
|[MJktJ33e2ejK5I1o.htm](pathfinder-bestiary-2-items/MJktJ33e2ejK5I1o.htm)|Debilitating Bite|auto-trad|
|[mjWf3inPCmf7NzIh.htm](pathfinder-bestiary-2-items/mjWf3inPCmf7NzIh.htm)|Bite|auto-trad|
|[mKh47XLxTvXG71TF.htm](pathfinder-bestiary-2-items/mKh47XLxTvXG71TF.htm)|Soul Crush|auto-trad|
|[MKjlk8JpKxpWjpPW.htm](pathfinder-bestiary-2-items/MKjlk8JpKxpWjpPW.htm)|Darkvision|auto-trad|
|[mKNSE9q3LtnCb49y.htm](pathfinder-bestiary-2-items/mKNSE9q3LtnCb49y.htm)|Scythe|auto-trad|
|[MkwxsOzEbMfQNQfG.htm](pathfinder-bestiary-2-items/MkwxsOzEbMfQNQfG.htm)|At-Will Spells|auto-trad|
|[ml69ZMPwkVsbCTC7.htm](pathfinder-bestiary-2-items/ml69ZMPwkVsbCTC7.htm)|Storm of Tentacles|auto-trad|
|[mLaikYbAsRTru36r.htm](pathfinder-bestiary-2-items/mLaikYbAsRTru36r.htm)|Arms|auto-trad|
|[MLD9Fi3IkozFjlXZ.htm](pathfinder-bestiary-2-items/MLD9Fi3IkozFjlXZ.htm)|Penetrating Strike|auto-trad|
|[mlnT94tqOlTGgqXM.htm](pathfinder-bestiary-2-items/mlnT94tqOlTGgqXM.htm)|Darkvision|auto-trad|
|[mLqIKRgLPV3i0SWY.htm](pathfinder-bestiary-2-items/mLqIKRgLPV3i0SWY.htm)|Claw|auto-trad|
|[mlQrjH8yonwvq4Td.htm](pathfinder-bestiary-2-items/mlQrjH8yonwvq4Td.htm)|Primal Innate Spells|auto-trad|
|[MLvT2QA997KCHesX.htm](pathfinder-bestiary-2-items/MLvT2QA997KCHesX.htm)|Low-Light Vision|auto-trad|
|[mmb4NGB4HueXi8Ty.htm](pathfinder-bestiary-2-items/mmb4NGB4HueXi8Ty.htm)|Bite|auto-trad|
|[mMCpgVRLUjGayWTB.htm](pathfinder-bestiary-2-items/mMCpgVRLUjGayWTB.htm)|Darkvision|auto-trad|
|[mMsy3KDfbrGBdYgB.htm](pathfinder-bestiary-2-items/mMsy3KDfbrGBdYgB.htm)|Low-Light Vision|auto-trad|
|[mnNaEVfu4nEmEwem.htm](pathfinder-bestiary-2-items/mnNaEVfu4nEmEwem.htm)|Attack of Opportunity|auto-trad|
|[mNQAWCGl6DU8cQUP.htm](pathfinder-bestiary-2-items/mNQAWCGl6DU8cQUP.htm)|Jagged Jaws|auto-trad|
|[mObSHLoIirMcASES.htm](pathfinder-bestiary-2-items/mObSHLoIirMcASES.htm)|Jaws|auto-trad|
|[mOfIGDW3g48Ft7w8.htm](pathfinder-bestiary-2-items/mOfIGDW3g48Ft7w8.htm)|Catch Rock|auto-trad|
|[MopioBBWXr3np4t0.htm](pathfinder-bestiary-2-items/MopioBBWXr3np4t0.htm)|Darkvision|auto-trad|
|[MPbG99HGshwvN1vm.htm](pathfinder-bestiary-2-items/MPbG99HGshwvN1vm.htm)|Grab|auto-trad|
|[MPJFQqOrPfQloQLr.htm](pathfinder-bestiary-2-items/MPJFQqOrPfQloQLr.htm)|Death Throes|auto-trad|
|[mPQXusMKyetxlwjx.htm](pathfinder-bestiary-2-items/mPQXusMKyetxlwjx.htm)|Scent|auto-trad|
|[mqgxK9jf9zGXE672.htm](pathfinder-bestiary-2-items/mqgxK9jf9zGXE672.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[MqN8V1YL2EczTFIG.htm](pathfinder-bestiary-2-items/MqN8V1YL2EczTFIG.htm)|Trident|auto-trad|
|[mqqrT74LZMyFyuic.htm](pathfinder-bestiary-2-items/mqqrT74LZMyFyuic.htm)|Jaws|auto-trad|
|[mqsn67bFzywwGQ10.htm](pathfinder-bestiary-2-items/mqsn67bFzywwGQ10.htm)|Toxic Bite|auto-trad|
|[mQUpigRG6jKJbeLx.htm](pathfinder-bestiary-2-items/mQUpigRG6jKJbeLx.htm)|Primal Innate Spells|auto-trad|
|[MRVG5RH41oEJ41N3.htm](pathfinder-bestiary-2-items/MRVG5RH41oEJ41N3.htm)|Fist|auto-trad|
|[mS3DC54HONPgp9Pg.htm](pathfinder-bestiary-2-items/mS3DC54HONPgp9Pg.htm)|Sunlight Vulnerability|auto-trad|
|[MSbm9DhCPOKdhf7m.htm](pathfinder-bestiary-2-items/MSbm9DhCPOKdhf7m.htm)|Drink Flesh|auto-trad|
|[msIyAn6AsHKdGxMu.htm](pathfinder-bestiary-2-items/msIyAn6AsHKdGxMu.htm)|Thundering Bite|auto-trad|
|[mSpMpLaDOLKbXWns.htm](pathfinder-bestiary-2-items/mSpMpLaDOLKbXWns.htm)|Piscovenom|auto-trad|
|[MtM4AVZZ5pbVijPC.htm](pathfinder-bestiary-2-items/MtM4AVZZ5pbVijPC.htm)|+2 Status to All Saves vs. Magic|auto-trad|
|[mTsKSxGS9jWZPCar.htm](pathfinder-bestiary-2-items/mTsKSxGS9jWZPCar.htm)|Greater Darkvision|auto-trad|
|[Mu6RmJmoPp5iM9ld.htm](pathfinder-bestiary-2-items/Mu6RmJmoPp5iM9ld.htm)|Primal Innate Spells|auto-trad|
|[MUg2W7zTfRWFuMZY.htm](pathfinder-bestiary-2-items/MUg2W7zTfRWFuMZY.htm)|Low-Light Vision|auto-trad|
|[mUjeve67uKUXgiEz.htm](pathfinder-bestiary-2-items/mUjeve67uKUXgiEz.htm)|Regeneration 15 (Deactivated by Cold Iron)|auto-trad|
|[mUpNn76jylw99mXr.htm](pathfinder-bestiary-2-items/mUpNn76jylw99mXr.htm)|Frightful Presence|auto-trad|
|[MuzXu4bDzodvaZ7w.htm](pathfinder-bestiary-2-items/MuzXu4bDzodvaZ7w.htm)|Watery Transparency|auto-trad|
|[MvqmGM4ha7aQt1CR.htm](pathfinder-bestiary-2-items/MvqmGM4ha7aQt1CR.htm)|Change Shape|auto-trad|
|[MVRIjPsJ2e4QYHM9.htm](pathfinder-bestiary-2-items/MVRIjPsJ2e4QYHM9.htm)|Wing Deflection|auto-trad|
|[MvwGFLtsnf8ZFqhG.htm](pathfinder-bestiary-2-items/MvwGFLtsnf8ZFqhG.htm)|Root|auto-trad|
|[MvxbkaVb0xvmWEgv.htm](pathfinder-bestiary-2-items/MvxbkaVb0xvmWEgv.htm)|Constant Spells|auto-trad|
|[MWff0Ro0CLotg930.htm](pathfinder-bestiary-2-items/MWff0Ro0CLotg930.htm)|Constrict|auto-trad|
|[mwrBvwI1EzNpPqAp.htm](pathfinder-bestiary-2-items/mwrBvwI1EzNpPqAp.htm)|Fist|auto-trad|
|[MwuE7Ua1AaKVpFBo.htm](pathfinder-bestiary-2-items/MwuE7Ua1AaKVpFBo.htm)|Peluda Venom|auto-trad|
|[mXh2p5FT7dkLC4KJ.htm](pathfinder-bestiary-2-items/mXh2p5FT7dkLC4KJ.htm)|Attack of Opportunity|auto-trad|
|[mXvv89TdLwyRKCuM.htm](pathfinder-bestiary-2-items/mXvv89TdLwyRKCuM.htm)|Tentacle Mouth|auto-trad|
|[mY1TZIEvZh0l0OBD.htm](pathfinder-bestiary-2-items/mY1TZIEvZh0l0OBD.htm)|Throw Rock|auto-trad|
|[MY7iImbmJzLQjtsK.htm](pathfinder-bestiary-2-items/MY7iImbmJzLQjtsK.htm)|Lifesense 60 feet|auto-trad|
|[MYTvAqtXzKikO4tg.htm](pathfinder-bestiary-2-items/MYTvAqtXzKikO4tg.htm)|Stinger|auto-trad|
|[myXt3SwrT2L8VMW4.htm](pathfinder-bestiary-2-items/myXt3SwrT2L8VMW4.htm)|Stygian Guardian|auto-trad|
|[mZb7eFQ1yel73da5.htm](pathfinder-bestiary-2-items/mZb7eFQ1yel73da5.htm)|Soulsense|auto-trad|
|[mzcBU4P7MDB5Kav4.htm](pathfinder-bestiary-2-items/mzcBU4P7MDB5Kav4.htm)|Frightful Presence|auto-trad|
|[mzdRGmCygfrC7SXm.htm](pathfinder-bestiary-2-items/mzdRGmCygfrC7SXm.htm)|Tick Fever|auto-trad|
|[MZE5l2QagOF7ICxV.htm](pathfinder-bestiary-2-items/MZE5l2QagOF7ICxV.htm)|Scent|auto-trad|
|[MzmPJ00qwdaxKPhE.htm](pathfinder-bestiary-2-items/MzmPJ00qwdaxKPhE.htm)|Grab|auto-trad|
|[MZYIlatBEX5yoG4G.htm](pathfinder-bestiary-2-items/MZYIlatBEX5yoG4G.htm)|Bite|auto-trad|
|[N1bssKPKGqtiKwNk.htm](pathfinder-bestiary-2-items/N1bssKPKGqtiKwNk.htm)|Magma Swim|auto-trad|
|[N1nMBoBunIANgCSx.htm](pathfinder-bestiary-2-items/N1nMBoBunIANgCSx.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[n4WaiTMOMHY2C2Rc.htm](pathfinder-bestiary-2-items/n4WaiTMOMHY2C2Rc.htm)|Darkvision|auto-trad|
|[N4XGBKf3QWAL89KZ.htm](pathfinder-bestiary-2-items/N4XGBKf3QWAL89KZ.htm)|Change Shape|auto-trad|
|[n5nOO8opkHkd9ZDW.htm](pathfinder-bestiary-2-items/n5nOO8opkHkd9ZDW.htm)|Darkvision|auto-trad|
|[N5T2rvrlqZmn4pNy.htm](pathfinder-bestiary-2-items/N5T2rvrlqZmn4pNy.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[N5U9pyyTitN5UDgN.htm](pathfinder-bestiary-2-items/N5U9pyyTitN5UDgN.htm)|Jaws|auto-trad|
|[n5uTPxg5zF7PKMZF.htm](pathfinder-bestiary-2-items/n5uTPxg5zF7PKMZF.htm)|Yank|auto-trad|
|[n61gabHGuCegRfKD.htm](pathfinder-bestiary-2-items/n61gabHGuCegRfKD.htm)|Primal Innate Spells|auto-trad|
|[n6zbDHFEoD3NyVNv.htm](pathfinder-bestiary-2-items/n6zbDHFEoD3NyVNv.htm)|Hypostome|auto-trad|
|[N8OPQrQXn7YLXcis.htm](pathfinder-bestiary-2-items/N8OPQrQXn7YLXcis.htm)|Constant Spells|auto-trad|
|[n94Dl501br4UFyL1.htm](pathfinder-bestiary-2-items/n94Dl501br4UFyL1.htm)|Throw Rock|auto-trad|
|[N9in8b8yuAVDLoyD.htm](pathfinder-bestiary-2-items/N9in8b8yuAVDLoyD.htm)|Motion Sense|auto-trad|
|[N9O6ClFh2arDGR4l.htm](pathfinder-bestiary-2-items/N9O6ClFh2arDGR4l.htm)|Pseudopod|auto-trad|
|[N9qNd1NMgEeyNwcF.htm](pathfinder-bestiary-2-items/N9qNd1NMgEeyNwcF.htm)|Spell Deflection|auto-trad|
|[NA3aEVVR6sNkB8KH.htm](pathfinder-bestiary-2-items/NA3aEVVR6sNkB8KH.htm)|Ice Shard|auto-trad|
|[nBbFutvNlcdf1cgZ.htm](pathfinder-bestiary-2-items/nBbFutvNlcdf1cgZ.htm)|Drown|auto-trad|
|[Nbea4sMv3ZuYKikA.htm](pathfinder-bestiary-2-items/Nbea4sMv3ZuYKikA.htm)|Divine Innate Spells|auto-trad|
|[nceXlZk44iBehzvl.htm](pathfinder-bestiary-2-items/nceXlZk44iBehzvl.htm)|Tail|auto-trad|
|[NCoI7vXSD3LDfPTM.htm](pathfinder-bestiary-2-items/NCoI7vXSD3LDfPTM.htm)|Rapid Rake|auto-trad|
|[NCrNtLcBDV1QPiXn.htm](pathfinder-bestiary-2-items/NCrNtLcBDV1QPiXn.htm)|Rend|auto-trad|
|[nd0M4055DPNvjNvf.htm](pathfinder-bestiary-2-items/nd0M4055DPNvjNvf.htm)|Manifest Shawl|auto-trad|
|[nD0QzJqbAecWv9ls.htm](pathfinder-bestiary-2-items/nD0QzJqbAecWv9ls.htm)|Low-Light Vision|auto-trad|
|[ND8R6Z5H3eqFpdDz.htm](pathfinder-bestiary-2-items/ND8R6Z5H3eqFpdDz.htm)|Regeneration 40 (deactivated by acid or fire)|auto-trad|
|[NDhBBYbkfa48lqzj.htm](pathfinder-bestiary-2-items/NDhBBYbkfa48lqzj.htm)|Mist Vision|auto-trad|
|[NDQPoKh0SP7tR1Ss.htm](pathfinder-bestiary-2-items/NDQPoKh0SP7tR1Ss.htm)|Tentacle Stab|auto-trad|
|[Ne1X80xmcoz2KP6a.htm](pathfinder-bestiary-2-items/Ne1X80xmcoz2KP6a.htm)|Darkvision|auto-trad|
|[NEE6cuy1OUxkBe2Q.htm](pathfinder-bestiary-2-items/NEE6cuy1OUxkBe2Q.htm)|Darkvision|auto-trad|
|[neGWW2ZDslwlxS5Y.htm](pathfinder-bestiary-2-items/neGWW2ZDslwlxS5Y.htm)|Tremorsense|auto-trad|
|[newbZPBnDfEIJX5y.htm](pathfinder-bestiary-2-items/newbZPBnDfEIJX5y.htm)|Feeding Tendril|auto-trad|
|[NEWDCp6ZApgJEtMx.htm](pathfinder-bestiary-2-items/NEWDCp6ZApgJEtMx.htm)|Dispelling Field|auto-trad|
|[nEZNMODnNpwrnUPC.htm](pathfinder-bestiary-2-items/nEZNMODnNpwrnUPC.htm)|Final Judgment|auto-trad|
|[nflrLT1POlEJZu3H.htm](pathfinder-bestiary-2-items/nflrLT1POlEJZu3H.htm)|Claw|auto-trad|
|[ngIT9cdBAAOK2wzV.htm](pathfinder-bestiary-2-items/ngIT9cdBAAOK2wzV.htm)|Independent Brains|auto-trad|
|[nhcCpe4q2AuE5rBl.htm](pathfinder-bestiary-2-items/nhcCpe4q2AuE5rBl.htm)|Fire Jelly Venom|auto-trad|
|[NHDhjNbpULppyjkP.htm](pathfinder-bestiary-2-items/NHDhjNbpULppyjkP.htm)|Darkvision|auto-trad|
|[nhn4UFoQny0X750B.htm](pathfinder-bestiary-2-items/nhn4UFoQny0X750B.htm)|Breath Weapon|auto-trad|
|[nHTCIVLK6QSJqHd3.htm](pathfinder-bestiary-2-items/nHTCIVLK6QSJqHd3.htm)|Poisonous Pustules|auto-trad|
|[nIDe1OaPgWDPyBej.htm](pathfinder-bestiary-2-items/nIDe1OaPgWDPyBej.htm)|Text Immersion|auto-trad|
|[nj1sfrFbZHDVVn3y.htm](pathfinder-bestiary-2-items/nj1sfrFbZHDVVn3y.htm)|Darkvision|auto-trad|
|[njNLaG6BmpA7LInp.htm](pathfinder-bestiary-2-items/njNLaG6BmpA7LInp.htm)|Jaws|auto-trad|
|[NJS22AHrPTckdEPS.htm](pathfinder-bestiary-2-items/NJS22AHrPTckdEPS.htm)|Sense Murderer|auto-trad|
|[nKHp4Fk7EvpSS3N9.htm](pathfinder-bestiary-2-items/nKHp4Fk7EvpSS3N9.htm)|Jaws|auto-trad|
|[nKqb3ksLlia2sFBZ.htm](pathfinder-bestiary-2-items/nKqb3ksLlia2sFBZ.htm)|Infused Items|auto-trad|
|[Nl0sqXtr54YzMawN.htm](pathfinder-bestiary-2-items/Nl0sqXtr54YzMawN.htm)|Consume Death|auto-trad|
|[nLocPM8hKYMstnGr.htm](pathfinder-bestiary-2-items/nLocPM8hKYMstnGr.htm)|Change Shape|auto-trad|
|[nM4ThlQUyejxc6bH.htm](pathfinder-bestiary-2-items/nM4ThlQUyejxc6bH.htm)|Stone Step|auto-trad|
|[nneVdsY6tUBGsScQ.htm](pathfinder-bestiary-2-items/nneVdsY6tUBGsScQ.htm)|Head Regrowth|auto-trad|
|[nnl33KWF4lCqql97.htm](pathfinder-bestiary-2-items/nnl33KWF4lCqql97.htm)|Easy to Influence|auto-trad|
|[NnMlHBLwjpkBjx4v.htm](pathfinder-bestiary-2-items/NnMlHBLwjpkBjx4v.htm)|Claw|auto-trad|
|[NNPWYZUGqcGbUeZI.htm](pathfinder-bestiary-2-items/NNPWYZUGqcGbUeZI.htm)|Summon Aquatic Ally|auto-trad|
|[NOvlBahz080PaM4R.htm](pathfinder-bestiary-2-items/NOvlBahz080PaM4R.htm)|Tail|auto-trad|
|[NP8RWxt3VPo7LZwb.htm](pathfinder-bestiary-2-items/NP8RWxt3VPo7LZwb.htm)|Beak|auto-trad|
|[npIgzch2HZkhFifI.htm](pathfinder-bestiary-2-items/npIgzch2HZkhFifI.htm)|Claw|auto-trad|
|[nplfqbEUtcOBlV0b.htm](pathfinder-bestiary-2-items/nplfqbEUtcOBlV0b.htm)|Attack of Opportunity (Special)|auto-trad|
|[nPZlZs1rfnERketf.htm](pathfinder-bestiary-2-items/nPZlZs1rfnERketf.htm)|Undying Vendetta|auto-trad|
|[NqdW9zuvmBwHZScr.htm](pathfinder-bestiary-2-items/NqdW9zuvmBwHZScr.htm)|Archon's Door|auto-trad|
|[nQJ4bCNSLy7MFyXP.htm](pathfinder-bestiary-2-items/nQJ4bCNSLy7MFyXP.htm)|Occult Innate Spells|auto-trad|
|[nQuUyJS1AvKyFrA0.htm](pathfinder-bestiary-2-items/nQuUyJS1AvKyFrA0.htm)|Attack of Opportunity (Special)|auto-trad|
|[nQV7pNdpGD2GDrgl.htm](pathfinder-bestiary-2-items/nQV7pNdpGD2GDrgl.htm)|Sneak Attack|auto-trad|
|[NrafRXoflRlcVm6H.htm](pathfinder-bestiary-2-items/NrafRXoflRlcVm6H.htm)|At-Will Spells|auto-trad|
|[nrHgXy7XizFWsPFe.htm](pathfinder-bestiary-2-items/nrHgXy7XizFWsPFe.htm)|Dweomer Leap|auto-trad|
|[nrXh0T6f3LTaRSGo.htm](pathfinder-bestiary-2-items/nrXh0T6f3LTaRSGo.htm)|At-Will Spells|auto-trad|
|[NSdZME2VBfcmBDQA.htm](pathfinder-bestiary-2-items/NSdZME2VBfcmBDQA.htm)|Claw|auto-trad|
|[nSgOSBLM92y28WsS.htm](pathfinder-bestiary-2-items/nSgOSBLM92y28WsS.htm)|Darkvision|auto-trad|
|[NshCFgqhon7NfWbq.htm](pathfinder-bestiary-2-items/NshCFgqhon7NfWbq.htm)|Claw|auto-trad|
|[NSLdgH5UqohAav11.htm](pathfinder-bestiary-2-items/NSLdgH5UqohAav11.htm)|Tiger Empathy|auto-trad|
|[NsRn4K2Tli8P3WKm.htm](pathfinder-bestiary-2-items/NsRn4K2Tli8P3WKm.htm)|Ignore Pain|auto-trad|
|[nszzMLENLPQ4zPoa.htm](pathfinder-bestiary-2-items/nszzMLENLPQ4zPoa.htm)|Tenacious Flames|auto-trad|
|[NTWIBx9lvUwV7F1W.htm](pathfinder-bestiary-2-items/NTWIBx9lvUwV7F1W.htm)|Mundane Appearance|auto-trad|
|[numgrwQUdlF3fy51.htm](pathfinder-bestiary-2-items/numgrwQUdlF3fy51.htm)|Tail|auto-trad|
|[nuNIIa81vZ9PT2RL.htm](pathfinder-bestiary-2-items/nuNIIa81vZ9PT2RL.htm)|Dazzling Burst|auto-trad|
|[NUrrmWCh8zDwdKFw.htm](pathfinder-bestiary-2-items/NUrrmWCh8zDwdKFw.htm)|Grab|auto-trad|
|[nuV1GkHwu5pqVXnI.htm](pathfinder-bestiary-2-items/nuV1GkHwu5pqVXnI.htm)|Barb|auto-trad|
|[nvEMR0N4HLFwMTMQ.htm](pathfinder-bestiary-2-items/nvEMR0N4HLFwMTMQ.htm)|Tremorsense|auto-trad|
|[nVMaCED96f0puvS1.htm](pathfinder-bestiary-2-items/nVMaCED96f0puvS1.htm)|Apocalypse Breath|auto-trad|
|[NWD8KA0NNOrEyrvq.htm](pathfinder-bestiary-2-items/NWD8KA0NNOrEyrvq.htm)|Flying Strafe|auto-trad|
|[nxagDqd1qxC8w8dl.htm](pathfinder-bestiary-2-items/nxagDqd1qxC8w8dl.htm)|Low-Light Vision|auto-trad|
|[nxblqBzJUsncnJ9t.htm](pathfinder-bestiary-2-items/nxblqBzJUsncnJ9t.htm)|Noxious Burst|auto-trad|
|[NXk6QjCfO8eW6Txp.htm](pathfinder-bestiary-2-items/NXk6QjCfO8eW6Txp.htm)|Sneak Attack|auto-trad|
|[nyh1lXyTGoUctOzo.htm](pathfinder-bestiary-2-items/nyh1lXyTGoUctOzo.htm)|Double Chomp|auto-trad|
|[nYQgnHNVs60rcvUn.htm](pathfinder-bestiary-2-items/nYQgnHNVs60rcvUn.htm)|Mechanical Vulnerability|auto-trad|
|[NYqhdSbPLiwcvxjs.htm](pathfinder-bestiary-2-items/NYqhdSbPLiwcvxjs.htm)|Darkvision|auto-trad|
|[Nz33adJeSgdyaAw6.htm](pathfinder-bestiary-2-items/Nz33adJeSgdyaAw6.htm)|Hoof|auto-trad|
|[Nz9xljPpg09kcxBl.htm](pathfinder-bestiary-2-items/Nz9xljPpg09kcxBl.htm)|Bite|auto-trad|
|[NZhUsB0mdsYLr6UX.htm](pathfinder-bestiary-2-items/NZhUsB0mdsYLr6UX.htm)|Telepathy 100 feet|auto-trad|
|[NzMuO5EzlGKw4nGq.htm](pathfinder-bestiary-2-items/NzMuO5EzlGKw4nGq.htm)|Mouth|auto-trad|
|[NZRABrBe5xEzlWlP.htm](pathfinder-bestiary-2-items/NZRABrBe5xEzlWlP.htm)|Arrow of Mortality|auto-trad|
|[O0uewbc9915uqqlD.htm](pathfinder-bestiary-2-items/O0uewbc9915uqqlD.htm)|Tail|auto-trad|
|[O0YSsVnEBCg6oJnT.htm](pathfinder-bestiary-2-items/O0YSsVnEBCg6oJnT.htm)|Horns|auto-trad|
|[O10CpdkrcpxqCHHH.htm](pathfinder-bestiary-2-items/O10CpdkrcpxqCHHH.htm)|Circle of Protection|auto-trad|
|[o19lnL3xo6DpdVgZ.htm](pathfinder-bestiary-2-items/o19lnL3xo6DpdVgZ.htm)|Twisting Tail|auto-trad|
|[o38MzuC03KOCw8wS.htm](pathfinder-bestiary-2-items/o38MzuC03KOCw8wS.htm)|Jaws That Bite|auto-trad|
|[o3nbXHISVkXC32B1.htm](pathfinder-bestiary-2-items/o3nbXHISVkXC32B1.htm)|Grab|auto-trad|
|[o40ddeHAtx8cJ0uY.htm](pathfinder-bestiary-2-items/o40ddeHAtx8cJ0uY.htm)|Mind Lash|auto-trad|
|[O4cx0ZHztve08mHY.htm](pathfinder-bestiary-2-items/O4cx0ZHztve08mHY.htm)|Darkvision|auto-trad|
|[O4epuzV9B2OOYJXy.htm](pathfinder-bestiary-2-items/O4epuzV9B2OOYJXy.htm)|Primal Prepared Spells|auto-trad|
|[o4iEKaEBIwAr18XF.htm](pathfinder-bestiary-2-items/o4iEKaEBIwAr18XF.htm)|+4 Status to Will Saves vs. Mental|auto-trad|
|[o4uNCohL2RA2ShrG.htm](pathfinder-bestiary-2-items/o4uNCohL2RA2ShrG.htm)|Negative Healing|auto-trad|
|[O5P2324cJfYLLpmm.htm](pathfinder-bestiary-2-items/O5P2324cJfYLLpmm.htm)|Glass Armor|auto-trad|
|[o6CQEcEj0w3RIaZB.htm](pathfinder-bestiary-2-items/o6CQEcEj0w3RIaZB.htm)|At-Will Spells|auto-trad|
|[o6dcBgCrq9s6bVuO.htm](pathfinder-bestiary-2-items/o6dcBgCrq9s6bVuO.htm)|Swallow Whole|auto-trad|
|[oa0MYylUzdTPYei2.htm](pathfinder-bestiary-2-items/oa0MYylUzdTPYei2.htm)|Claw|auto-trad|
|[OAaAcan3IQyoJJ4X.htm](pathfinder-bestiary-2-items/OAaAcan3IQyoJJ4X.htm)|Divine Innate Spells|auto-trad|
|[oabwo3FEBY2CjPRM.htm](pathfinder-bestiary-2-items/oabwo3FEBY2CjPRM.htm)|Darkvision|auto-trad|
|[OACmutVuYdHoFbvw.htm](pathfinder-bestiary-2-items/OACmutVuYdHoFbvw.htm)|Divine Innate Spells|auto-trad|
|[oApMO7ZwPJkW1mn1.htm](pathfinder-bestiary-2-items/oApMO7ZwPJkW1mn1.htm)|Improved Push|auto-trad|
|[oAxtReNr1iwTRmKX.htm](pathfinder-bestiary-2-items/oAxtReNr1iwTRmKX.htm)|Negate Levitation|auto-trad|
|[OB8l4HRZ5Zc4G42p.htm](pathfinder-bestiary-2-items/OB8l4HRZ5Zc4G42p.htm)|Scent|auto-trad|
|[OBbctD8lNz3bU0yf.htm](pathfinder-bestiary-2-items/OBbctD8lNz3bU0yf.htm)|At-Will Spells|auto-trad|
|[obCe64sZedFgSWGQ.htm](pathfinder-bestiary-2-items/obCe64sZedFgSWGQ.htm)|Luring Cry|auto-trad|
|[oBMQTX6kkJaK35ed.htm](pathfinder-bestiary-2-items/oBMQTX6kkJaK35ed.htm)|Claw|auto-trad|
|[OBn7syfr2t30QJaa.htm](pathfinder-bestiary-2-items/OBn7syfr2t30QJaa.htm)|Bite|auto-trad|
|[ObPZcbNErqRJbu6p.htm](pathfinder-bestiary-2-items/ObPZcbNErqRJbu6p.htm)|Holy Longbow|auto-trad|
|[oBq0zVqb67SoY8qy.htm](pathfinder-bestiary-2-items/oBq0zVqb67SoY8qy.htm)|Darkvision|auto-trad|
|[oBV1Ybwqj4dpXeff.htm](pathfinder-bestiary-2-items/oBV1Ybwqj4dpXeff.htm)|Spirit Touch|auto-trad|
|[OBXLT5RlRI6AauZ1.htm](pathfinder-bestiary-2-items/OBXLT5RlRI6AauZ1.htm)|Breath Weapon|auto-trad|
|[oc2KzcT2MuHsRZ1k.htm](pathfinder-bestiary-2-items/oc2KzcT2MuHsRZ1k.htm)|Frozen Strike|auto-trad|
|[oC4rXv11i7sVgw4k.htm](pathfinder-bestiary-2-items/oC4rXv11i7sVgw4k.htm)|Radula|auto-trad|
|[OC4UGWpbAZyLbFb6.htm](pathfinder-bestiary-2-items/OC4UGWpbAZyLbFb6.htm)|Breath Weapon|auto-trad|
|[oC5vZAEFIISSmofG.htm](pathfinder-bestiary-2-items/oC5vZAEFIISSmofG.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Od1YX31hXRHCzHS3.htm](pathfinder-bestiary-2-items/Od1YX31hXRHCzHS3.htm)|Rip and Tear|auto-trad|
|[odZw9mK4mxnlp6qz.htm](pathfinder-bestiary-2-items/odZw9mK4mxnlp6qz.htm)|Darkvision|auto-trad|
|[oE4IMiBnWcfVyg7X.htm](pathfinder-bestiary-2-items/oE4IMiBnWcfVyg7X.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[Oe7KNrqiGTrNfkV9.htm](pathfinder-bestiary-2-items/Oe7KNrqiGTrNfkV9.htm)|Jaws|auto-trad|
|[OeHuAJLeUtdJFHfo.htm](pathfinder-bestiary-2-items/OeHuAJLeUtdJFHfo.htm)|Rhinoceros Charge|auto-trad|
|[Oem6sNhw2eizgyTW.htm](pathfinder-bestiary-2-items/Oem6sNhw2eizgyTW.htm)|Constant Spells|auto-trad|
|[Oevi96zftiaBjAOH.htm](pathfinder-bestiary-2-items/Oevi96zftiaBjAOH.htm)|Occult Innate Spells|auto-trad|
|[OEYNgSplNvqswLPK.htm](pathfinder-bestiary-2-items/OEYNgSplNvqswLPK.htm)|Golem Antimagic|auto-trad|
|[Of0URrwSMG72XvUL.htm](pathfinder-bestiary-2-items/Of0URrwSMG72XvUL.htm)|Lifesense|auto-trad|
|[ofz49oiBkargWzh6.htm](pathfinder-bestiary-2-items/ofz49oiBkargWzh6.htm)|Jaws|auto-trad|
|[oG07EBWnvzNPv4hk.htm](pathfinder-bestiary-2-items/oG07EBWnvzNPv4hk.htm)|Darkvision|auto-trad|
|[OhlQui33tv2Wq8Ur.htm](pathfinder-bestiary-2-items/OhlQui33tv2Wq8Ur.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[oifiHjSHVsJGyQiE.htm](pathfinder-bestiary-2-items/oifiHjSHVsJGyQiE.htm)|Darkvision|auto-trad|
|[oIucCyy3YvC5FoCA.htm](pathfinder-bestiary-2-items/oIucCyy3YvC5FoCA.htm)|Stone Stride|auto-trad|
|[OJA9CljFVtmCuzlZ.htm](pathfinder-bestiary-2-items/OJA9CljFVtmCuzlZ.htm)|Prudent Asterism|auto-trad|
|[OJg4VMV8aMk40kBm.htm](pathfinder-bestiary-2-items/OJg4VMV8aMk40kBm.htm)|Shadow Blend|auto-trad|
|[oJLAT2NTlht6zFtA.htm](pathfinder-bestiary-2-items/oJLAT2NTlht6zFtA.htm)|Divine Innate Spells|auto-trad|
|[ojlZFCKNrXd0MjTG.htm](pathfinder-bestiary-2-items/ojlZFCKNrXd0MjTG.htm)|Blood Siphon|auto-trad|
|[oKEblFTjM5UV6Lwa.htm](pathfinder-bestiary-2-items/oKEblFTjM5UV6Lwa.htm)|Steep Weapon|auto-trad|
|[OKH1R1FoBSEm3oKP.htm](pathfinder-bestiary-2-items/OKH1R1FoBSEm3oKP.htm)|Enraged Growth|auto-trad|
|[OKh9LnvuAtiEOz1f.htm](pathfinder-bestiary-2-items/OKh9LnvuAtiEOz1f.htm)|Primal Innate Spells|auto-trad|
|[okXnipjpQrYq6vGT.htm](pathfinder-bestiary-2-items/okXnipjpQrYq6vGT.htm)|Constrict|auto-trad|
|[OL5iKgpKK0q9NrD1.htm](pathfinder-bestiary-2-items/OL5iKgpKK0q9NrD1.htm)|Low-Light Vision|auto-trad|
|[OlCiQCdanvjrjD3s.htm](pathfinder-bestiary-2-items/OlCiQCdanvjrjD3s.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[OlnWMyIX3qedGUTu.htm](pathfinder-bestiary-2-items/OlnWMyIX3qedGUTu.htm)|Darkvision|auto-trad|
|[OLwbn1vkUwxatzY5.htm](pathfinder-bestiary-2-items/OLwbn1vkUwxatzY5.htm)|Lifesense|auto-trad|
|[OlwLivQfLQIh0ut7.htm](pathfinder-bestiary-2-items/OlwLivQfLQIh0ut7.htm)|Magic Sense|auto-trad|
|[oLxdFwYmbkBdHCgB.htm](pathfinder-bestiary-2-items/oLxdFwYmbkBdHCgB.htm)|At-Will Spells|auto-trad|
|[oMj0SiABPgORm6t3.htm](pathfinder-bestiary-2-items/oMj0SiABPgORm6t3.htm)|Breath Weapon|auto-trad|
|[OMTKXoF2bm81nkS5.htm](pathfinder-bestiary-2-items/OMTKXoF2bm81nkS5.htm)|Longsword|auto-trad|
|[onjhPnLTGiThJjTI.htm](pathfinder-bestiary-2-items/onjhPnLTGiThJjTI.htm)|Tremorsense (Imprecise) 60 feet|auto-trad|
|[oNmHvve4Vcwj6IiO.htm](pathfinder-bestiary-2-items/oNmHvve4Vcwj6IiO.htm)|Occult Innate Spells|auto-trad|
|[oNpExBZGMP135Gh8.htm](pathfinder-bestiary-2-items/oNpExBZGMP135Gh8.htm)|Capsize|auto-trad|
|[ope3wpbKSAJOWCJD.htm](pathfinder-bestiary-2-items/ope3wpbKSAJOWCJD.htm)|Bony Hand|auto-trad|
|[opNwYrwABCLTV4vZ.htm](pathfinder-bestiary-2-items/opNwYrwABCLTV4vZ.htm)|Occult Innate Spells|auto-trad|
|[OpUEqqVcx7trG4mu.htm](pathfinder-bestiary-2-items/OpUEqqVcx7trG4mu.htm)|Kukri|auto-trad|
|[oPWhso0y4ezQpGaJ.htm](pathfinder-bestiary-2-items/oPWhso0y4ezQpGaJ.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[oQcnbbUjvuJ6yIaK.htm](pathfinder-bestiary-2-items/oQcnbbUjvuJ6yIaK.htm)|Deep Breath|auto-trad|
|[oqcxPwDIMJ2bu4Ul.htm](pathfinder-bestiary-2-items/oqcxPwDIMJ2bu4Ul.htm)|Primal Innate Spells|auto-trad|
|[OqJWwPCeEzKsYH8X.htm](pathfinder-bestiary-2-items/OqJWwPCeEzKsYH8X.htm)|Darkvision|auto-trad|
|[OQMwQiE7vGLPMasL.htm](pathfinder-bestiary-2-items/OQMwQiE7vGLPMasL.htm)|Blood Drain|auto-trad|
|[oQNL2zFwRBLQE7vn.htm](pathfinder-bestiary-2-items/oQNL2zFwRBLQE7vn.htm)|Horns|auto-trad|
|[oqYW7IkKIsygycAY.htm](pathfinder-bestiary-2-items/oqYW7IkKIsygycAY.htm)|Reflexive Grab|auto-trad|
|[OR27Y1Vkv8bMFjHO.htm](pathfinder-bestiary-2-items/OR27Y1Vkv8bMFjHO.htm)|Dagger|auto-trad|
|[orQ7bvmsL70g6qk9.htm](pathfinder-bestiary-2-items/orQ7bvmsL70g6qk9.htm)|Jaws|auto-trad|
|[osi4ctAQErnOa2BZ.htm](pathfinder-bestiary-2-items/osi4ctAQErnOa2BZ.htm)|Heart Ripper|auto-trad|
|[OSiq1CXChzTT7udW.htm](pathfinder-bestiary-2-items/OSiq1CXChzTT7udW.htm)|Spectral Corruption|auto-trad|
|[osogPaHrRMKqt4Fw.htm](pathfinder-bestiary-2-items/osogPaHrRMKqt4Fw.htm)|Trample|auto-trad|
|[ot1RNObQMdnpWSrb.htm](pathfinder-bestiary-2-items/ot1RNObQMdnpWSrb.htm)|Catch Rock|auto-trad|
|[Ot3tRiGqqRheUAKs.htm](pathfinder-bestiary-2-items/Ot3tRiGqqRheUAKs.htm)|Crystallize Flesh|auto-trad|
|[otBr1P6MKia09fCt.htm](pathfinder-bestiary-2-items/otBr1P6MKia09fCt.htm)|Catch Rock|auto-trad|
|[OUEVYrICDhOs5NML.htm](pathfinder-bestiary-2-items/OUEVYrICDhOs5NML.htm)|Savage Jaws|auto-trad|
|[oUmLwF6HrGb0Y9yj.htm](pathfinder-bestiary-2-items/oUmLwF6HrGb0Y9yj.htm)|Regeneration 20 (deactivated by evil)|auto-trad|
|[Ox2rTRtNAQouxryx.htm](pathfinder-bestiary-2-items/Ox2rTRtNAQouxryx.htm)|Motion Sense 60 feet|auto-trad|
|[OxEJ1NOQewlXZjZh.htm](pathfinder-bestiary-2-items/OxEJ1NOQewlXZjZh.htm)|Claw|auto-trad|
|[OXfHyEExXIhlnUp7.htm](pathfinder-bestiary-2-items/OXfHyEExXIhlnUp7.htm)|Attack of Opportunity|auto-trad|
|[OxPw5pr9S1edjy8S.htm](pathfinder-bestiary-2-items/OxPw5pr9S1edjy8S.htm)|Lunar Naga Venom|auto-trad|
|[OxZGoHvhVubVXMm4.htm](pathfinder-bestiary-2-items/OxZGoHvhVubVXMm4.htm)|Petrifying Gaze|auto-trad|
|[OYFgzf5Q5j6UeJhf.htm](pathfinder-bestiary-2-items/OYFgzf5Q5j6UeJhf.htm)|Jaws|auto-trad|
|[OygWVWtANgUBztcL.htm](pathfinder-bestiary-2-items/OygWVWtANgUBztcL.htm)|At-Will Spells|auto-trad|
|[OymgeOQy6WJ5jvF1.htm](pathfinder-bestiary-2-items/OymgeOQy6WJ5jvF1.htm)|Scent|auto-trad|
|[OYTKATaPDdautdNt.htm](pathfinder-bestiary-2-items/OYTKATaPDdautdNt.htm)|Wing|auto-trad|
|[oYtncLyCKuBAQThk.htm](pathfinder-bestiary-2-items/oYtncLyCKuBAQThk.htm)|Clawing Fear|auto-trad|
|[Oz0PpydQ8KCagat3.htm](pathfinder-bestiary-2-items/Oz0PpydQ8KCagat3.htm)|Tail|auto-trad|
|[OZmNooOK98jFnTUU.htm](pathfinder-bestiary-2-items/OZmNooOK98jFnTUU.htm)|Barbed Tentacles|auto-trad|
|[OzuFfQ0SSo89GdPZ.htm](pathfinder-bestiary-2-items/OzuFfQ0SSo89GdPZ.htm)|Hoof|auto-trad|
|[P0EyFCnT4og9udgU.htm](pathfinder-bestiary-2-items/P0EyFCnT4og9udgU.htm)|Deep Breath|auto-trad|
|[P0VFneuZMPCfZ35e.htm](pathfinder-bestiary-2-items/P0VFneuZMPCfZ35e.htm)|Constant Spells|auto-trad|
|[p18BSmeidbDDfJ2D.htm](pathfinder-bestiary-2-items/p18BSmeidbDDfJ2D.htm)|Kind Word|auto-trad|
|[p1rxtsKx33vjytn0.htm](pathfinder-bestiary-2-items/p1rxtsKx33vjytn0.htm)|Thulgant Venom|auto-trad|
|[p23AKjDuFSYrPgZz.htm](pathfinder-bestiary-2-items/p23AKjDuFSYrPgZz.htm)|Primal Innate Spells|auto-trad|
|[P48WRIeVuuAB8JWK.htm](pathfinder-bestiary-2-items/P48WRIeVuuAB8JWK.htm)|Death Flood|auto-trad|
|[P4BV5OJUyj3rfr1E.htm](pathfinder-bestiary-2-items/P4BV5OJUyj3rfr1E.htm)|Telepathy|auto-trad|
|[p5OQLHvMklzKmrbR.htm](pathfinder-bestiary-2-items/p5OQLHvMklzKmrbR.htm)|Scimitar|auto-trad|
|[P5TjcVC1dN5jLmFb.htm](pathfinder-bestiary-2-items/P5TjcVC1dN5jLmFb.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[P5tslJuQziddpFpV.htm](pathfinder-bestiary-2-items/P5tslJuQziddpFpV.htm)|Tooth Grind|auto-trad|
|[P6oJVM0MDCy82QgU.htm](pathfinder-bestiary-2-items/P6oJVM0MDCy82QgU.htm)|Shadow Blending|auto-trad|
|[p8Evyqv8pdGxIXIQ.htm](pathfinder-bestiary-2-items/p8Evyqv8pdGxIXIQ.htm)|Rend|auto-trad|
|[P8P9UmFHbSmMnTnd.htm](pathfinder-bestiary-2-items/P8P9UmFHbSmMnTnd.htm)|Low-Light Vision|auto-trad|
|[P92CxO5qbGv9LPbu.htm](pathfinder-bestiary-2-items/P92CxO5qbGv9LPbu.htm)|Snip Thread|auto-trad|
|[p9eUNDT8mPVTSS1D.htm](pathfinder-bestiary-2-items/p9eUNDT8mPVTSS1D.htm)|Dimensional Wormhole|auto-trad|
|[p9ZGDxOXXXlq8PuH.htm](pathfinder-bestiary-2-items/p9ZGDxOXXXlq8PuH.htm)|Jaws|auto-trad|
|[Pb3h2ToW8cyiBXN7.htm](pathfinder-bestiary-2-items/Pb3h2ToW8cyiBXN7.htm)|Tentacle|auto-trad|
|[PBcYVDE3HZByWRI7.htm](pathfinder-bestiary-2-items/PBcYVDE3HZByWRI7.htm)|Black Scorpion Venom|auto-trad|
|[PbnYiMI8IGyrAIej.htm](pathfinder-bestiary-2-items/PbnYiMI8IGyrAIej.htm)|Chain|auto-trad|
|[PbQYJStaqe1nhRUn.htm](pathfinder-bestiary-2-items/PbQYJStaqe1nhRUn.htm)|Frightful Presence|auto-trad|
|[Pd7VzQqoVyctHCLg.htm](pathfinder-bestiary-2-items/Pd7VzQqoVyctHCLg.htm)|Magma Swim|auto-trad|
|[PDKjx7hYxc0mQaVe.htm](pathfinder-bestiary-2-items/PDKjx7hYxc0mQaVe.htm)|At-Will Spells|auto-trad|
|[PDPYyeFFpfksUoXK.htm](pathfinder-bestiary-2-items/PDPYyeFFpfksUoXK.htm)|Shadowcloak|auto-trad|
|[PDq2G4IiMdipukDg.htm](pathfinder-bestiary-2-items/PDq2G4IiMdipukDg.htm)|Beak|auto-trad|
|[pE48e3bWFRDs19oA.htm](pathfinder-bestiary-2-items/pE48e3bWFRDs19oA.htm)|Arcane Innate Spells|auto-trad|
|[pepIdjNqJtUTl9Wn.htm](pathfinder-bestiary-2-items/pepIdjNqJtUTl9Wn.htm)|Endless Nightmare|auto-trad|
|[PF6tfwJNh5sVOavs.htm](pathfinder-bestiary-2-items/PF6tfwJNh5sVOavs.htm)|Thrashing Retreat|auto-trad|
|[PFAC6dSt4RN7aRDz.htm](pathfinder-bestiary-2-items/PFAC6dSt4RN7aRDz.htm)|Stinger|auto-trad|
|[pfYqWHlnkQUBpJIo.htm](pathfinder-bestiary-2-items/pfYqWHlnkQUBpJIo.htm)|Electric Torpor|auto-trad|
|[pfzWyL0gCgqD1xRz.htm](pathfinder-bestiary-2-items/pfzWyL0gCgqD1xRz.htm)|Telepathy|auto-trad|
|[pGaSnoNG83zKVZkX.htm](pathfinder-bestiary-2-items/pGaSnoNG83zKVZkX.htm)|Deflecting Cloud|auto-trad|
|[PGeuRIZhMLp88mYc.htm](pathfinder-bestiary-2-items/PGeuRIZhMLp88mYc.htm)|Fist|auto-trad|
|[PgZtqgPtRR9DRaGz.htm](pathfinder-bestiary-2-items/PgZtqgPtRR9DRaGz.htm)|Vine|auto-trad|
|[pHckdhGspQesS1nl.htm](pathfinder-bestiary-2-items/pHckdhGspQesS1nl.htm)|Ravage|auto-trad|
|[pheseepJ6Q1YF17G.htm](pathfinder-bestiary-2-items/pheseepJ6Q1YF17G.htm)|Darkvision|auto-trad|
|[pJ6eBDUVUixzX9UL.htm](pathfinder-bestiary-2-items/pJ6eBDUVUixzX9UL.htm)|Animate Chains|auto-trad|
|[PjBdBCA31o7FcQvI.htm](pathfinder-bestiary-2-items/PjBdBCA31o7FcQvI.htm)|Telepathy|auto-trad|
|[pke6bvuCCSuidMVb.htm](pathfinder-bestiary-2-items/pke6bvuCCSuidMVb.htm)|Poisonous Touch|auto-trad|
|[pKF3w8Hj8QFO6ZMv.htm](pathfinder-bestiary-2-items/pKF3w8Hj8QFO6ZMv.htm)|Draconic Momentum|auto-trad|
|[PKHHueFUrRyFJPxL.htm](pathfinder-bestiary-2-items/PKHHueFUrRyFJPxL.htm)|Rend|auto-trad|
|[pkqZFkDwe6SFvUXX.htm](pathfinder-bestiary-2-items/pkqZFkDwe6SFvUXX.htm)|Serpentfolk Venom|auto-trad|
|[pL08UYEZoKBSYUzv.htm](pathfinder-bestiary-2-items/pL08UYEZoKBSYUzv.htm)|Darkvision|auto-trad|
|[pLNcYnrUhpwxDo9b.htm](pathfinder-bestiary-2-items/pLNcYnrUhpwxDo9b.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[PLo6RE4lIKmw9DQL.htm](pathfinder-bestiary-2-items/PLo6RE4lIKmw9DQL.htm)|Blurred Form|auto-trad|
|[PM9U1Ca1DQ2ZA7ZJ.htm](pathfinder-bestiary-2-items/PM9U1Ca1DQ2ZA7ZJ.htm)|Negative Healing|auto-trad|
|[PMbE9CQyp5uM7Whu.htm](pathfinder-bestiary-2-items/PMbE9CQyp5uM7Whu.htm)|Improved Grab|auto-trad|
|[pmfr4MB1abUN5RbW.htm](pathfinder-bestiary-2-items/pmfr4MB1abUN5RbW.htm)|Feed|auto-trad|
|[pMGUO0z7GhIrgGlF.htm](pathfinder-bestiary-2-items/pMGUO0z7GhIrgGlF.htm)|Reactive Slime|auto-trad|
|[pmHXQt1EgMIVwD1i.htm](pathfinder-bestiary-2-items/pmHXQt1EgMIVwD1i.htm)|Exquisite Pain|auto-trad|
|[PMVviZt2FjDHt7QV.htm](pathfinder-bestiary-2-items/PMVviZt2FjDHt7QV.htm)|Telepathy 100 feet|auto-trad|
|[pntbmMiiU6IArfJX.htm](pathfinder-bestiary-2-items/pntbmMiiU6IArfJX.htm)|Grab|auto-trad|
|[pnvcZVssnfksWBzv.htm](pathfinder-bestiary-2-items/pnvcZVssnfksWBzv.htm)|Quill Thrust|auto-trad|
|[POIZ15lxh5FqbzaK.htm](pathfinder-bestiary-2-items/POIZ15lxh5FqbzaK.htm)|Fist|auto-trad|
|[POLjXIhCSeQBrvcV.htm](pathfinder-bestiary-2-items/POLjXIhCSeQBrvcV.htm)|Greater Darkvision|auto-trad|
|[POVANlwhTG8kc8av.htm](pathfinder-bestiary-2-items/POVANlwhTG8kc8av.htm)|Claw|auto-trad|
|[pP0EwdXTDTjgDpZA.htm](pathfinder-bestiary-2-items/pP0EwdXTDTjgDpZA.htm)|Darkvision|auto-trad|
|[PpVuZdKsS2ms3K3h.htm](pathfinder-bestiary-2-items/PpVuZdKsS2ms3K3h.htm)|Shadow Stride|auto-trad|
|[Pq25te8zoxBzUMRN.htm](pathfinder-bestiary-2-items/Pq25te8zoxBzUMRN.htm)|Jaws|auto-trad|
|[PQ87JPdxzlCptXn6.htm](pathfinder-bestiary-2-items/PQ87JPdxzlCptXn6.htm)|Tick Fever|auto-trad|
|[pqUWKgysApGeGzWo.htm](pathfinder-bestiary-2-items/pqUWKgysApGeGzWo.htm)|Divine Innate Spells|auto-trad|
|[pRC5IW45o7eWNos1.htm](pathfinder-bestiary-2-items/pRC5IW45o7eWNos1.htm)|Jaws|auto-trad|
|[PrFbn0CsipqWtp4z.htm](pathfinder-bestiary-2-items/PrFbn0CsipqWtp4z.htm)|Flytrap Toxin|auto-trad|
|[pRxhn354ZvirgsU6.htm](pathfinder-bestiary-2-items/pRxhn354ZvirgsU6.htm)|Recall Venom|auto-trad|
|[PskJTiEThXOJo4hH.htm](pathfinder-bestiary-2-items/PskJTiEThXOJo4hH.htm)|Claw|auto-trad|
|[pSl7SmKRkTYkQI1N.htm](pathfinder-bestiary-2-items/pSl7SmKRkTYkQI1N.htm)|Stone Curse|auto-trad|
|[PSqd8jopT7V35lJt.htm](pathfinder-bestiary-2-items/PSqd8jopT7V35lJt.htm)|Hands of the Murderer|auto-trad|
|[psRycYQDP439XLGm.htm](pathfinder-bestiary-2-items/psRycYQDP439XLGm.htm)|Wavesense (Imprecise) 30 feet|auto-trad|
|[pT1xpkQICFklLesg.htm](pathfinder-bestiary-2-items/pT1xpkQICFklLesg.htm)|Tendril|auto-trad|
|[pTjXZs6ZwSF31KLb.htm](pathfinder-bestiary-2-items/pTjXZs6ZwSF31KLb.htm)|Drink Blood|auto-trad|
|[pTowxMM6c9JCvVGF.htm](pathfinder-bestiary-2-items/pTowxMM6c9JCvVGF.htm)|Mind Bolt|auto-trad|
|[pu67MUMU2zgOU5zE.htm](pathfinder-bestiary-2-items/pu67MUMU2zgOU5zE.htm)|Scent|auto-trad|
|[puOX1L9eYq450jRo.htm](pathfinder-bestiary-2-items/puOX1L9eYq450jRo.htm)|Scent|auto-trad|
|[pV2UvWTozyuKQHGx.htm](pathfinder-bestiary-2-items/pV2UvWTozyuKQHGx.htm)|Attack of Opportunity|auto-trad|
|[pVbWqNxbj50FvZ38.htm](pathfinder-bestiary-2-items/pVbWqNxbj50FvZ38.htm)|Low-Light Vision|auto-trad|
|[pvidfrziJK8Olr4p.htm](pathfinder-bestiary-2-items/pvidfrziJK8Olr4p.htm)|Piercing Shot|auto-trad|
|[pViUeTjTLhTubzOw.htm](pathfinder-bestiary-2-items/pViUeTjTLhTubzOw.htm)|Coven|auto-trad|
|[Pvp0MGWk24EJhPud.htm](pathfinder-bestiary-2-items/Pvp0MGWk24EJhPud.htm)|Tentacle|auto-trad|
|[pvqYJHUkKVxUx3Q2.htm](pathfinder-bestiary-2-items/pvqYJHUkKVxUx3Q2.htm)|Claw|auto-trad|
|[pvTRbzLubEXLA5D5.htm](pathfinder-bestiary-2-items/pvTRbzLubEXLA5D5.htm)|Claw|auto-trad|
|[PWBjM1JecEFdjzo0.htm](pathfinder-bestiary-2-items/PWBjM1JecEFdjzo0.htm)|Savage Jaws|auto-trad|
|[pwHxPg0sBxXkBROU.htm](pathfinder-bestiary-2-items/pwHxPg0sBxXkBROU.htm)|Greater Darkvision|auto-trad|
|[PwSEiS1mqX6rqjJa.htm](pathfinder-bestiary-2-items/PwSEiS1mqX6rqjJa.htm)|Bo Staff|auto-trad|
|[PxJ09KlgYVpdaklS.htm](pathfinder-bestiary-2-items/PxJ09KlgYVpdaklS.htm)|Tail|auto-trad|
|[pXklOYevcG58P7sJ.htm](pathfinder-bestiary-2-items/pXklOYevcG58P7sJ.htm)|Beak|auto-trad|
|[pXVSNEwIRE2uBaw6.htm](pathfinder-bestiary-2-items/pXVSNEwIRE2uBaw6.htm)|Wing Flash|auto-trad|
|[Pxxc4fNbFR6zlG0y.htm](pathfinder-bestiary-2-items/Pxxc4fNbFR6zlG0y.htm)|Web Trap|auto-trad|
|[Pxxr235CKBdgLZ6y.htm](pathfinder-bestiary-2-items/Pxxr235CKBdgLZ6y.htm)|Jaws|auto-trad|
|[Py8ycO501wOs8ujU.htm](pathfinder-bestiary-2-items/Py8ycO501wOs8ujU.htm)|Legs|auto-trad|
|[pYBvVxfTXqJhnpWd.htm](pathfinder-bestiary-2-items/pYBvVxfTXqJhnpWd.htm)|Claw|auto-trad|
|[pYiWNLrYh7MbvSRl.htm](pathfinder-bestiary-2-items/pYiWNLrYh7MbvSRl.htm)|Improved Grab|auto-trad|
|[PySanquTNKBp5Zkm.htm](pathfinder-bestiary-2-items/PySanquTNKBp5Zkm.htm)|Breath Weapon|auto-trad|
|[pzMdTIEh6A57hKG7.htm](pathfinder-bestiary-2-items/pzMdTIEh6A57hKG7.htm)|At-Will Spells|auto-trad|
|[pztBMGaCTnjDqJCY.htm](pathfinder-bestiary-2-items/pztBMGaCTnjDqJCY.htm)|At-Will Spells|auto-trad|
|[q0kSVZllWCY1CwD2.htm](pathfinder-bestiary-2-items/q0kSVZllWCY1CwD2.htm)|Darkvision|auto-trad|
|[Q0VRtsqm6etoZxCa.htm](pathfinder-bestiary-2-items/Q0VRtsqm6etoZxCa.htm)|Shadow Doubles|auto-trad|
|[Q0zvzLaylr0AYBGR.htm](pathfinder-bestiary-2-items/Q0zvzLaylr0AYBGR.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[Q1JyYPsUlKes2ELl.htm](pathfinder-bestiary-2-items/Q1JyYPsUlKes2ELl.htm)|Split|auto-trad|
|[q2KJwlgmVEKQMJPb.htm](pathfinder-bestiary-2-items/q2KJwlgmVEKQMJPb.htm)|Maintain Disguise|auto-trad|
|[Q3683RHwk5skFzWi.htm](pathfinder-bestiary-2-items/Q3683RHwk5skFzWi.htm)|Dispelling Strike|auto-trad|
|[Q57wXsK3JdmLYoDR.htm](pathfinder-bestiary-2-items/Q57wXsK3JdmLYoDR.htm)|Blade of Justice|auto-trad|
|[q5XLED0zxjExmJp2.htm](pathfinder-bestiary-2-items/q5XLED0zxjExmJp2.htm)|Tail|auto-trad|
|[Q6cQBmJGmIcB0lWr.htm](pathfinder-bestiary-2-items/Q6cQBmJGmIcB0lWr.htm)|Divine Innate Spells|auto-trad|
|[Q6Z6sPX8M68AUOXI.htm](pathfinder-bestiary-2-items/Q6Z6sPX8M68AUOXI.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Q7cHvTaIWyRwhMYl.htm](pathfinder-bestiary-2-items/Q7cHvTaIWyRwhMYl.htm)|Web Trap|auto-trad|
|[Q7E6X7VR38S4QJfL.htm](pathfinder-bestiary-2-items/Q7E6X7VR38S4QJfL.htm)|Primal Prepared Spells|auto-trad|
|[Q7nRRCF8dXONTqKt.htm](pathfinder-bestiary-2-items/Q7nRRCF8dXONTqKt.htm)|Shock|auto-trad|
|[Q8lUTVhcaklSOhJn.htm](pathfinder-bestiary-2-items/Q8lUTVhcaklSOhJn.htm)|Scent|auto-trad|
|[QACVidUBGQoHHpqW.htm](pathfinder-bestiary-2-items/QACVidUBGQoHHpqW.htm)|Mark Quarry|auto-trad|
|[QAiOrfL7q6tF2GUV.htm](pathfinder-bestiary-2-items/QAiOrfL7q6tF2GUV.htm)|Draconic Momentum|auto-trad|
|[QaqXp54jr3aKQjWN.htm](pathfinder-bestiary-2-items/QaqXp54jr3aKQjWN.htm)|Negative Healing|auto-trad|
|[qARQ5Sntem4thRiy.htm](pathfinder-bestiary-2-items/qARQ5Sntem4thRiy.htm)|Improved Grab|auto-trad|
|[QaWyk1afljYBga0c.htm](pathfinder-bestiary-2-items/QaWyk1afljYBga0c.htm)|Wind Gust|auto-trad|
|[QB6MLb6aO9rvCPbS.htm](pathfinder-bestiary-2-items/QB6MLb6aO9rvCPbS.htm)|Constant Spells|auto-trad|
|[qBIJw1s6aIrNQQaJ.htm](pathfinder-bestiary-2-items/qBIJw1s6aIrNQQaJ.htm)|Divine Innate Spells|auto-trad|
|[QbjjKmqSkCf93Xw5.htm](pathfinder-bestiary-2-items/QbjjKmqSkCf93Xw5.htm)|Primal Innate Spells|auto-trad|
|[qbm10J9qDUiFCQDn.htm](pathfinder-bestiary-2-items/qbm10J9qDUiFCQDn.htm)|Arc Lightning|auto-trad|
|[qbn93YsujSFI6TMZ.htm](pathfinder-bestiary-2-items/qbn93YsujSFI6TMZ.htm)|Camouflage|auto-trad|
|[QBq3x8HUuGt0wVZm.htm](pathfinder-bestiary-2-items/QBq3x8HUuGt0wVZm.htm)|Sense Blood (Imprecise) 60 feet|auto-trad|
|[qcCotjUp5yYyWkjT.htm](pathfinder-bestiary-2-items/qcCotjUp5yYyWkjT.htm)|Claw|auto-trad|
|[qCZyXHHj5OMACPYY.htm](pathfinder-bestiary-2-items/qCZyXHHj5OMACPYY.htm)|Taiga Linnorm Venom|auto-trad|
|[QD2iwjozawGYYqOf.htm](pathfinder-bestiary-2-items/QD2iwjozawGYYqOf.htm)|Attack of Opportunity|auto-trad|
|[qdKGH0iZztLLCCPz.htm](pathfinder-bestiary-2-items/qdKGH0iZztLLCCPz.htm)|Low-Light Vision|auto-trad|
|[qebLFwBJf98hFcSv.htm](pathfinder-bestiary-2-items/qebLFwBJf98hFcSv.htm)|Darkvision|auto-trad|
|[qEh64WzRoljm53JB.htm](pathfinder-bestiary-2-items/qEh64WzRoljm53JB.htm)|Darkvision|auto-trad|
|[qeIQpw65yitumRkB.htm](pathfinder-bestiary-2-items/qeIQpw65yitumRkB.htm)|Jaws|auto-trad|
|[qfAGiyqMgrNFUoSw.htm](pathfinder-bestiary-2-items/qfAGiyqMgrNFUoSw.htm)|Fast Healing 20|auto-trad|
|[qfIg2ulL65DhV6X2.htm](pathfinder-bestiary-2-items/qfIg2ulL65DhV6X2.htm)|Sprint|auto-trad|
|[qfkx0yR9a3Qh4LsT.htm](pathfinder-bestiary-2-items/qfkx0yR9a3Qh4LsT.htm)|Double Attack|auto-trad|
|[qfYFz5QrJfsbmJng.htm](pathfinder-bestiary-2-items/qfYFz5QrJfsbmJng.htm)|Darkvision|auto-trad|
|[Qgjku7yO52q21NCG.htm](pathfinder-bestiary-2-items/Qgjku7yO52q21NCG.htm)|Temporal Flurry|auto-trad|
|[qgyml1ToPqFdJIKa.htm](pathfinder-bestiary-2-items/qgyml1ToPqFdJIKa.htm)|Drag Below|auto-trad|
|[qh8LFXVLWjILfZZx.htm](pathfinder-bestiary-2-items/qh8LFXVLWjILfZZx.htm)|Sunlight Powerlessness|auto-trad|
|[Qhdl7tHPD3bhAihm.htm](pathfinder-bestiary-2-items/Qhdl7tHPD3bhAihm.htm)|Lash Out|auto-trad|
|[QHdmWY86UyD9ECHR.htm](pathfinder-bestiary-2-items/QHdmWY86UyD9ECHR.htm)|Darkvision|auto-trad|
|[qHEzK4MngkQB6vhD.htm](pathfinder-bestiary-2-items/qHEzK4MngkQB6vhD.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[qhgcqwFp1hE2jFbu.htm](pathfinder-bestiary-2-items/qhgcqwFp1hE2jFbu.htm)|Claw|auto-trad|
|[QHvw3fI9gFNac33v.htm](pathfinder-bestiary-2-items/QHvw3fI9gFNac33v.htm)|Fangs|auto-trad|
|[QItaAvzvKUUHn205.htm](pathfinder-bestiary-2-items/QItaAvzvKUUHn205.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[QIzXNCE3GPZBj5sy.htm](pathfinder-bestiary-2-items/QIzXNCE3GPZBj5sy.htm)|Shortsword|auto-trad|
|[Qj1AizQTHLeRQxPV.htm](pathfinder-bestiary-2-items/Qj1AizQTHLeRQxPV.htm)|Darkvision|auto-trad|
|[QjBqDZJ1zOlHWSEG.htm](pathfinder-bestiary-2-items/QjBqDZJ1zOlHWSEG.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[qJQGpBBEACoUjdFY.htm](pathfinder-bestiary-2-items/qJQGpBBEACoUjdFY.htm)|Claw|auto-trad|
|[QK7BnQXUv5A7jm84.htm](pathfinder-bestiary-2-items/QK7BnQXUv5A7jm84.htm)|Warden of Erebus|auto-trad|
|[ql5PVdfgunDKXI5N.htm](pathfinder-bestiary-2-items/ql5PVdfgunDKXI5N.htm)|Darkvision|auto-trad|
|[QleXAKxC6oYMhWIK.htm](pathfinder-bestiary-2-items/QleXAKxC6oYMhWIK.htm)|Roll|auto-trad|
|[qlRNyTqzf6kMvBEV.htm](pathfinder-bestiary-2-items/qlRNyTqzf6kMvBEV.htm)|Quill|auto-trad|
|[QNLWcv6w045fE1oE.htm](pathfinder-bestiary-2-items/QNLWcv6w045fE1oE.htm)|Primal Innate Spells|auto-trad|
|[QNMEwPCmUmz3bSOW.htm](pathfinder-bestiary-2-items/QNMEwPCmUmz3bSOW.htm)|Knockdown|auto-trad|
|[QNvBVG4wk0atHVo9.htm](pathfinder-bestiary-2-items/QNvBVG4wk0atHVo9.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[QNy9Hrx2ib5tkQKt.htm](pathfinder-bestiary-2-items/QNy9Hrx2ib5tkQKt.htm)|Shadow Blend|auto-trad|
|[QoOV8oVWiooAilxA.htm](pathfinder-bestiary-2-items/QoOV8oVWiooAilxA.htm)|Sea Snake Venom|auto-trad|
|[qoqJdxMXA4DPeMal.htm](pathfinder-bestiary-2-items/qoqJdxMXA4DPeMal.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[qoSUaPSmr9BrJU8V.htm](pathfinder-bestiary-2-items/qoSUaPSmr9BrJU8V.htm)|Breath Weapon|auto-trad|
|[QoW8rIq8ikH4Ebpr.htm](pathfinder-bestiary-2-items/QoW8rIq8ikH4Ebpr.htm)|Occult Innate Spells|auto-trad|
|[qpAN2omg7HvCwOzk.htm](pathfinder-bestiary-2-items/qpAN2omg7HvCwOzk.htm)|Undetectable|auto-trad|
|[QPrrVmViA5QTr6tE.htm](pathfinder-bestiary-2-items/QPrrVmViA5QTr6tE.htm)|Bastion Aura|auto-trad|
|[QQgEGkDjynHqQ53g.htm](pathfinder-bestiary-2-items/QQgEGkDjynHqQ53g.htm)|Constant Spells|auto-trad|
|[QqggW4FlHDKMwZT4.htm](pathfinder-bestiary-2-items/QqggW4FlHDKMwZT4.htm)|Push|auto-trad|
|[QQSJHx4JIqEThZoC.htm](pathfinder-bestiary-2-items/QQSJHx4JIqEThZoC.htm)|Mandibles|auto-trad|
|[QrWMNrtokpcMTBMi.htm](pathfinder-bestiary-2-items/QrWMNrtokpcMTBMi.htm)|Primal Innate Spells|auto-trad|
|[qs95uKqjbpoxk48D.htm](pathfinder-bestiary-2-items/qs95uKqjbpoxk48D.htm)|Jaws|auto-trad|
|[QSAMSi294JHAumur.htm](pathfinder-bestiary-2-items/QSAMSi294JHAumur.htm)|Rock|auto-trad|
|[QSxWcw9SiRdGtKM7.htm](pathfinder-bestiary-2-items/QSxWcw9SiRdGtKM7.htm)|Attack of Opportunity|auto-trad|
|[QTFJOKoZto5yRgDd.htm](pathfinder-bestiary-2-items/QTFJOKoZto5yRgDd.htm)|Darkvision|auto-trad|
|[QTnEqxIt2MaL0WlZ.htm](pathfinder-bestiary-2-items/QTnEqxIt2MaL0WlZ.htm)|Spirit Naga Venom|auto-trad|
|[QTxOTyLCST6Zaqb3.htm](pathfinder-bestiary-2-items/QTxOTyLCST6Zaqb3.htm)|At-Will Spells|auto-trad|
|[qu9kOE72tZH2eGQ1.htm](pathfinder-bestiary-2-items/qu9kOE72tZH2eGQ1.htm)|Constant Spells|auto-trad|
|[quht5jPh2LiFjJft.htm](pathfinder-bestiary-2-items/quht5jPh2LiFjJft.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[QuK27vpZ1PXmKcRW.htm](pathfinder-bestiary-2-items/QuK27vpZ1PXmKcRW.htm)|Fist|auto-trad|
|[qUtjLPzfgoNg1aAP.htm](pathfinder-bestiary-2-items/qUtjLPzfgoNg1aAP.htm)|Mandibles|auto-trad|
|[QUwEfng9Ey9oqGwE.htm](pathfinder-bestiary-2-items/QUwEfng9Ey9oqGwE.htm)|Darkvision|auto-trad|
|[QUWT5qx8jyGSWpIM.htm](pathfinder-bestiary-2-items/QUWT5qx8jyGSWpIM.htm)|Occult Innate Spells|auto-trad|
|[QuXauTCFwPyxjFZ6.htm](pathfinder-bestiary-2-items/QuXauTCFwPyxjFZ6.htm)|Accursed Breath|auto-trad|
|[qV74gNViCaiEVUy2.htm](pathfinder-bestiary-2-items/qV74gNViCaiEVUy2.htm)|Capsize|auto-trad|
|[QVlwvsf0cmEupKV2.htm](pathfinder-bestiary-2-items/QVlwvsf0cmEupKV2.htm)|Tremorsense|auto-trad|
|[qWb7LRbSfICoEdcJ.htm](pathfinder-bestiary-2-items/qWb7LRbSfICoEdcJ.htm)|Constant Spells|auto-trad|
|[qWcmE7mc3EOqNR3L.htm](pathfinder-bestiary-2-items/qWcmE7mc3EOqNR3L.htm)|Tentacle|auto-trad|
|[qwrA1CcDM9QaJlc5.htm](pathfinder-bestiary-2-items/qwrA1CcDM9QaJlc5.htm)|Pounce|auto-trad|
|[QwsbmnGAaEHYU0ut.htm](pathfinder-bestiary-2-items/QwsbmnGAaEHYU0ut.htm)|Lightning Bolt|auto-trad|
|[Qx0Uh3ktQIh1dVKT.htm](pathfinder-bestiary-2-items/Qx0Uh3ktQIh1dVKT.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[qxh5yKErE5FpGPnf.htm](pathfinder-bestiary-2-items/qxh5yKErE5FpGPnf.htm)|Knockdown|auto-trad|
|[qxkuhugvyoxDpt7N.htm](pathfinder-bestiary-2-items/qxkuhugvyoxDpt7N.htm)|Draconic Frenzy|auto-trad|
|[qXrvqRXGNuAyfORa.htm](pathfinder-bestiary-2-items/qXrvqRXGNuAyfORa.htm)|Wrench Spirit|auto-trad|
|[qXSjj6mmQyNoxHDN.htm](pathfinder-bestiary-2-items/qXSjj6mmQyNoxHDN.htm)|Occult Spontaneous Spells|auto-trad|
|[qxsT5YmSCH2dqJAT.htm](pathfinder-bestiary-2-items/qxsT5YmSCH2dqJAT.htm)|Amalgam|auto-trad|
|[QxUxl1vPtZ72uSOi.htm](pathfinder-bestiary-2-items/QxUxl1vPtZ72uSOi.htm)|Mist Vision|auto-trad|
|[QXYKDwpj06Pd7sEo.htm](pathfinder-bestiary-2-items/QXYKDwpj06Pd7sEo.htm)|Scent|auto-trad|
|[QY55zAJAyvaoflJY.htm](pathfinder-bestiary-2-items/QY55zAJAyvaoflJY.htm)|Nature Empathy|auto-trad|
|[qZaBb2z6mE2zLL2u.htm](pathfinder-bestiary-2-items/qZaBb2z6mE2zLL2u.htm)|Create Object|auto-trad|
|[Qzr8MBWWm4DE5P8H.htm](pathfinder-bestiary-2-items/Qzr8MBWWm4DE5P8H.htm)|Discorporate|auto-trad|
|[QZup3O3zcKH6TFZd.htm](pathfinder-bestiary-2-items/QZup3O3zcKH6TFZd.htm)|Sneak Attack|auto-trad|
|[R1voQq09UIwdTVP5.htm](pathfinder-bestiary-2-items/R1voQq09UIwdTVP5.htm)|Mercy Vulnerability|auto-trad|
|[R2a5AYr0RbUoKiK3.htm](pathfinder-bestiary-2-items/R2a5AYr0RbUoKiK3.htm)|Sard Venom|auto-trad|
|[R2ziTTF38PwKFQr7.htm](pathfinder-bestiary-2-items/R2ziTTF38PwKFQr7.htm)|Frightful Presence|auto-trad|
|[R30Gee8hs5TPUNYY.htm](pathfinder-bestiary-2-items/R30Gee8hs5TPUNYY.htm)|Darkvision|auto-trad|
|[r3hGo7TAQ3TtGwLY.htm](pathfinder-bestiary-2-items/r3hGo7TAQ3TtGwLY.htm)|Electrolocation|auto-trad|
|[R3HSbrKclMuhH014.htm](pathfinder-bestiary-2-items/R3HSbrKclMuhH014.htm)|Planar Acclimation|auto-trad|
|[r421CNwt0qBGoUVK.htm](pathfinder-bestiary-2-items/r421CNwt0qBGoUVK.htm)|Retributive Strike|auto-trad|
|[R4jXbSxWyqNtYJyp.htm](pathfinder-bestiary-2-items/R4jXbSxWyqNtYJyp.htm)|Thumb Spike|auto-trad|
|[r5Mw7uM8br1L49y5.htm](pathfinder-bestiary-2-items/r5Mw7uM8br1L49y5.htm)|Wing|auto-trad|
|[r62684lc4BkUMn7p.htm](pathfinder-bestiary-2-items/r62684lc4BkUMn7p.htm)|Solidify Mist|auto-trad|
|[r7aL4DHJpi1R2n6Z.htm](pathfinder-bestiary-2-items/r7aL4DHJpi1R2n6Z.htm)|Improved Grab|auto-trad|
|[R8bzIOrsR2PFpnSQ.htm](pathfinder-bestiary-2-items/R8bzIOrsR2PFpnSQ.htm)|Noxious Fumes|auto-trad|
|[R8vIKnaOA34FiWMY.htm](pathfinder-bestiary-2-items/R8vIKnaOA34FiWMY.htm)|Water Jet|auto-trad|
|[r8zcJu9p3TzgNHoU.htm](pathfinder-bestiary-2-items/r8zcJu9p3TzgNHoU.htm)|At-Will Spells|auto-trad|
|[R9YnxMf9012f1ajN.htm](pathfinder-bestiary-2-items/R9YnxMf9012f1ajN.htm)|Divine Innate Spells|auto-trad|
|[rA2rDDvXjf2Cfr4w.htm](pathfinder-bestiary-2-items/rA2rDDvXjf2Cfr4w.htm)|Grab|auto-trad|
|[ra2VPYkOtDIX5nt5.htm](pathfinder-bestiary-2-items/ra2VPYkOtDIX5nt5.htm)|Darkvision|auto-trad|
|[rajOpAe3FLG1J8uc.htm](pathfinder-bestiary-2-items/rajOpAe3FLG1J8uc.htm)|Starknife|auto-trad|
|[rASGf5YkW2lQXMFQ.htm](pathfinder-bestiary-2-items/rASGf5YkW2lQXMFQ.htm)|Trample|auto-trad|
|[rasNjBJHTsaP5LFC.htm](pathfinder-bestiary-2-items/rasNjBJHTsaP5LFC.htm)|Tail|auto-trad|
|[rB0kJuC158tvKsmj.htm](pathfinder-bestiary-2-items/rB0kJuC158tvKsmj.htm)|Garbled Thoughts|auto-trad|
|[rBN7VRWeDcgG0c1C.htm](pathfinder-bestiary-2-items/rBN7VRWeDcgG0c1C.htm)|Darkvision|auto-trad|
|[rCDCEl5KfuYha1bo.htm](pathfinder-bestiary-2-items/rCDCEl5KfuYha1bo.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[rCDEvA0MWLJZe3ba.htm](pathfinder-bestiary-2-items/rCDEvA0MWLJZe3ba.htm)|At-Will Spells|auto-trad|
|[rCFV63sVu1lfxToK.htm](pathfinder-bestiary-2-items/rCFV63sVu1lfxToK.htm)|Negative Healing|auto-trad|
|[RCjZk0ilv6kHRuT5.htm](pathfinder-bestiary-2-items/RCjZk0ilv6kHRuT5.htm)|Curse of the Weretiger|auto-trad|
|[rDl9ZNp0ry5Tek3B.htm](pathfinder-bestiary-2-items/rDl9ZNp0ry5Tek3B.htm)|Dagger|auto-trad|
|[rdri6pl6hY1glOfy.htm](pathfinder-bestiary-2-items/rdri6pl6hY1glOfy.htm)|Cold Healing|auto-trad|
|[reBkTZBqwYPgsiFb.htm](pathfinder-bestiary-2-items/reBkTZBqwYPgsiFb.htm)|Tail|auto-trad|
|[ReQtBIuf6WBTNwEH.htm](pathfinder-bestiary-2-items/ReQtBIuf6WBTNwEH.htm)|Thorn|auto-trad|
|[reVIS3aes83WAuF2.htm](pathfinder-bestiary-2-items/reVIS3aes83WAuF2.htm)|Deep Breath|auto-trad|
|[RF4DrP2dezs9NRn8.htm](pathfinder-bestiary-2-items/RF4DrP2dezs9NRn8.htm)|At-Will Spells|auto-trad|
|[RFcZ8BNKEh3WWl6j.htm](pathfinder-bestiary-2-items/RFcZ8BNKEh3WWl6j.htm)|Whispering Wounds|auto-trad|
|[RFGL24YZ4ufhBCRM.htm](pathfinder-bestiary-2-items/RFGL24YZ4ufhBCRM.htm)|At-Will Spells|auto-trad|
|[RFPvW7U5w3NRyp3q.htm](pathfinder-bestiary-2-items/RFPvW7U5w3NRyp3q.htm)|Envisioning|auto-trad|
|[RGaUdIjwMpgnNYGg.htm](pathfinder-bestiary-2-items/RGaUdIjwMpgnNYGg.htm)|Pain Starvation|auto-trad|
|[rGG9CbH3CnQNbztb.htm](pathfinder-bestiary-2-items/rGG9CbH3CnQNbztb.htm)|Darkvision|auto-trad|
|[rGvREWDBZrWzdUhd.htm](pathfinder-bestiary-2-items/rGvREWDBZrWzdUhd.htm)|Beak|auto-trad|
|[RHRtFPGTQDqJ0cER.htm](pathfinder-bestiary-2-items/RHRtFPGTQDqJ0cER.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Rid4PCaJM2edDiKh.htm](pathfinder-bestiary-2-items/Rid4PCaJM2edDiKh.htm)|Forest Step|auto-trad|
|[RjfMaoY3Fp7Ouab8.htm](pathfinder-bestiary-2-items/RjfMaoY3Fp7Ouab8.htm)|Tremorsense|auto-trad|
|[RJUajmwcmdSx0tQb.htm](pathfinder-bestiary-2-items/RJUajmwcmdSx0tQb.htm)|Mesmerizing Melody|auto-trad|
|[rkayE4XQbhVQPYCS.htm](pathfinder-bestiary-2-items/rkayE4XQbhVQPYCS.htm)|Crossbow|auto-trad|
|[rkxRbGfC97VpayNy.htm](pathfinder-bestiary-2-items/rkxRbGfC97VpayNy.htm)|Triumvirate|auto-trad|
|[RkzJDhpWzxqQXiAq.htm](pathfinder-bestiary-2-items/RkzJDhpWzxqQXiAq.htm)|Strafing Rush|auto-trad|
|[rl4i6PCiSzEqRVyf.htm](pathfinder-bestiary-2-items/rl4i6PCiSzEqRVyf.htm)|Claw|auto-trad|
|[RLJbJxbajEsFLapw.htm](pathfinder-bestiary-2-items/RLJbJxbajEsFLapw.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[rLT3Kld8E7p9hG1y.htm](pathfinder-bestiary-2-items/rLT3Kld8E7p9hG1y.htm)|Low-Light Vision|auto-trad|
|[RMCsutKfNLtikzWK.htm](pathfinder-bestiary-2-items/RMCsutKfNLtikzWK.htm)|Constant Spells|auto-trad|
|[rMTD8eLYVnpfvpCd.htm](pathfinder-bestiary-2-items/rMTD8eLYVnpfvpCd.htm)|Wing|auto-trad|
|[rN5DBFuJma8wPkT1.htm](pathfinder-bestiary-2-items/rN5DBFuJma8wPkT1.htm)|Grab|auto-trad|
|[RNDT57aD8UzBIRVk.htm](pathfinder-bestiary-2-items/RNDT57aD8UzBIRVk.htm)|Pack Attack|auto-trad|
|[RniBjxUV8FfeP05P.htm](pathfinder-bestiary-2-items/RniBjxUV8FfeP05P.htm)|Blue-Ringed Octopus Venom|auto-trad|
|[rnvfOLxvrsqRTH2s.htm](pathfinder-bestiary-2-items/rnvfOLxvrsqRTH2s.htm)|Darkvision|auto-trad|
|[roCv7KuzhfzP7hQD.htm](pathfinder-bestiary-2-items/roCv7KuzhfzP7hQD.htm)|Claw|auto-trad|
|[ROfUBzcM8hE66OL5.htm](pathfinder-bestiary-2-items/ROfUBzcM8hE66OL5.htm)|At-Will Spells|auto-trad|
|[ROXOsK91xVXF9I3t.htm](pathfinder-bestiary-2-items/ROXOsK91xVXF9I3t.htm)|Claw|auto-trad|
|[rOyG05jMYSlAcSS5.htm](pathfinder-bestiary-2-items/rOyG05jMYSlAcSS5.htm)|Swiftness|auto-trad|
|[Rp0zPvS2PAgfYdfn.htm](pathfinder-bestiary-2-items/Rp0zPvS2PAgfYdfn.htm)|Fist|auto-trad|
|[rp6F9XDfGE9qOef0.htm](pathfinder-bestiary-2-items/rp6F9XDfGE9qOef0.htm)|Darkvision|auto-trad|
|[rPjj6ZCE5pwwVZg1.htm](pathfinder-bestiary-2-items/rPjj6ZCE5pwwVZg1.htm)|Jaws|auto-trad|
|[rpp6hEDYHYJ2EJ15.htm](pathfinder-bestiary-2-items/rpp6hEDYHYJ2EJ15.htm)|Claw|auto-trad|
|[rQn75QJUJH2hQKpE.htm](pathfinder-bestiary-2-items/rQn75QJUJH2hQKpE.htm)|Chain of Malebolge|auto-trad|
|[RrDmY99gWZJtFtjo.htm](pathfinder-bestiary-2-items/RrDmY99gWZJtFtjo.htm)|Greater Darkvision|auto-trad|
|[rRJmbyj1idfcUw82.htm](pathfinder-bestiary-2-items/rRJmbyj1idfcUw82.htm)|Grab|auto-trad|
|[RRuGVF2WBzHvPAD4.htm](pathfinder-bestiary-2-items/RRuGVF2WBzHvPAD4.htm)|Vine|auto-trad|
|[rsf1q6ooMNJFjdfh.htm](pathfinder-bestiary-2-items/rsf1q6ooMNJFjdfh.htm)|No Hearing|auto-trad|
|[RSov7mEf6h3jgcfL.htm](pathfinder-bestiary-2-items/RSov7mEf6h3jgcfL.htm)|Draconic Momentum|auto-trad|
|[rSpxgtewFq1FNp4L.htm](pathfinder-bestiary-2-items/rSpxgtewFq1FNp4L.htm)|Retune|auto-trad|
|[RSXc4NTQGKjlqt3X.htm](pathfinder-bestiary-2-items/RSXc4NTQGKjlqt3X.htm)|Darkvision|auto-trad|
|[RTbPNwtAI2pRCjBX.htm](pathfinder-bestiary-2-items/RTbPNwtAI2pRCjBX.htm)|Sling|auto-trad|
|[RTUQPDiCd9wKvFow.htm](pathfinder-bestiary-2-items/RTUQPDiCd9wKvFow.htm)|Tentacle Encage|auto-trad|
|[RUcTjfOyyFNcxgIK.htm](pathfinder-bestiary-2-items/RUcTjfOyyFNcxgIK.htm)|Reactive Chomp|auto-trad|
|[RUPrm8gIOS2f852a.htm](pathfinder-bestiary-2-items/RUPrm8gIOS2f852a.htm)|Ferocity|auto-trad|
|[rUvfMcCzgvir0ElO.htm](pathfinder-bestiary-2-items/rUvfMcCzgvir0ElO.htm)|Jaws|auto-trad|
|[RvyDbBs5muX2yHdI.htm](pathfinder-bestiary-2-items/RvyDbBs5muX2yHdI.htm)|Stolen Death|auto-trad|
|[RwPr1Ik2tKeFrL0r.htm](pathfinder-bestiary-2-items/RwPr1Ik2tKeFrL0r.htm)|Swiftness|auto-trad|
|[RwVJwyaaTesWbfgR.htm](pathfinder-bestiary-2-items/RwVJwyaaTesWbfgR.htm)|Manipulate Flames|auto-trad|
|[rwYZxmONVe2JmBPn.htm](pathfinder-bestiary-2-items/rwYZxmONVe2JmBPn.htm)|Darkvision|auto-trad|
|[rx9aGcoGrLLVPzhV.htm](pathfinder-bestiary-2-items/rx9aGcoGrLLVPzhV.htm)|Withering Opportunity|auto-trad|
|[RXUhJpHPbpDMUlbJ.htm](pathfinder-bestiary-2-items/RXUhJpHPbpDMUlbJ.htm)|Change Shape|auto-trad|
|[s0ekhcyN4jpM6jLQ.htm](pathfinder-bestiary-2-items/s0ekhcyN4jpM6jLQ.htm)|Rock|auto-trad|
|[S0ETALXebRGPPFPv.htm](pathfinder-bestiary-2-items/S0ETALXebRGPPFPv.htm)|Low-Light Vision|auto-trad|
|[s1CEAQqJM8T6nqR4.htm](pathfinder-bestiary-2-items/s1CEAQqJM8T6nqR4.htm)|Claw|auto-trad|
|[s23ZMm1sJc7XLyKW.htm](pathfinder-bestiary-2-items/s23ZMm1sJc7XLyKW.htm)|Seed Spray|auto-trad|
|[S2jsW7P3mTUPKBHZ.htm](pathfinder-bestiary-2-items/S2jsW7P3mTUPKBHZ.htm)|Darkvision|auto-trad|
|[s2VxShdmP0XyPcUl.htm](pathfinder-bestiary-2-items/s2VxShdmP0XyPcUl.htm)|Tremorsense|auto-trad|
|[S31hZB9EKGtsEZ7B.htm](pathfinder-bestiary-2-items/S31hZB9EKGtsEZ7B.htm)|Orrery|auto-trad|
|[S3MoMWkoEovX2zha.htm](pathfinder-bestiary-2-items/S3MoMWkoEovX2zha.htm)|Fist|auto-trad|
|[s43Ks62ECjo1B5kX.htm](pathfinder-bestiary-2-items/s43Ks62ECjo1B5kX.htm)|Claw|auto-trad|
|[s4eMY3meCcZpGDrL.htm](pathfinder-bestiary-2-items/s4eMY3meCcZpGDrL.htm)|Improved Grab|auto-trad|
|[S6Avv1gbTIidozSY.htm](pathfinder-bestiary-2-items/S6Avv1gbTIidozSY.htm)|Low-Light Vision|auto-trad|
|[s7GGq81IzCx73D2B.htm](pathfinder-bestiary-2-items/s7GGq81IzCx73D2B.htm)|Call to Blood|auto-trad|
|[S7QInhdroOaf7k84.htm](pathfinder-bestiary-2-items/S7QInhdroOaf7k84.htm)|Tremorsense|auto-trad|
|[s8TihgQDAypdPOee.htm](pathfinder-bestiary-2-items/s8TihgQDAypdPOee.htm)|Darkvision|auto-trad|
|[S8XYYEnqxZeY5C7K.htm](pathfinder-bestiary-2-items/S8XYYEnqxZeY5C7K.htm)|Daemonic Famine|auto-trad|
|[s9Def6M7z5ceOIoj.htm](pathfinder-bestiary-2-items/s9Def6M7z5ceOIoj.htm)|Pounce|auto-trad|
|[s9PF3C48raLmLaNG.htm](pathfinder-bestiary-2-items/s9PF3C48raLmLaNG.htm)|Scent|auto-trad|
|[sA3oeWc14avc7CPO.htm](pathfinder-bestiary-2-items/sA3oeWc14avc7CPO.htm)|Wrath of Fate|auto-trad|
|[sabgo6OVGbFhpOOA.htm](pathfinder-bestiary-2-items/sabgo6OVGbFhpOOA.htm)|Compel Condemned|auto-trad|
|[SaS7qHeGtLYWa1rX.htm](pathfinder-bestiary-2-items/SaS7qHeGtLYWa1rX.htm)|Tail Claw|auto-trad|
|[SaUKY4rTuI0N39ad.htm](pathfinder-bestiary-2-items/SaUKY4rTuI0N39ad.htm)|Scent|auto-trad|
|[SaVJfclmAsjFCBke.htm](pathfinder-bestiary-2-items/SaVJfclmAsjFCBke.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[SB54Z5VO15GfJd0X.htm](pathfinder-bestiary-2-items/SB54Z5VO15GfJd0X.htm)|Rend|auto-trad|
|[sBfOfFAVZjz8e2Sk.htm](pathfinder-bestiary-2-items/sBfOfFAVZjz8e2Sk.htm)|Grab|auto-trad|
|[sbPAeP8uZ3oiRj7q.htm](pathfinder-bestiary-2-items/sbPAeP8uZ3oiRj7q.htm)|Tangling Chains|auto-trad|
|[sc9sTDDxNEMsTklK.htm](pathfinder-bestiary-2-items/sc9sTDDxNEMsTklK.htm)|Mist Vision|auto-trad|
|[scNTrSPuw9K5tRtq.htm](pathfinder-bestiary-2-items/scNTrSPuw9K5tRtq.htm)|Wind Form|auto-trad|
|[SDF2zWv6zGkmcQH9.htm](pathfinder-bestiary-2-items/SDF2zWv6zGkmcQH9.htm)|Bite|auto-trad|
|[sDghWo7WoJwJ3FPJ.htm](pathfinder-bestiary-2-items/sDghWo7WoJwJ3FPJ.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[sdQC9PYloWzpJd9P.htm](pathfinder-bestiary-2-items/sdQC9PYloWzpJd9P.htm)|Grab|auto-trad|
|[Sei3FrQPmhMHMkTo.htm](pathfinder-bestiary-2-items/Sei3FrQPmhMHMkTo.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[Sep8lU0AriLntZmy.htm](pathfinder-bestiary-2-items/Sep8lU0AriLntZmy.htm)|Vanth's Curse|auto-trad|
|[sfq8i9rLngOZhIeU.htm](pathfinder-bestiary-2-items/sfq8i9rLngOZhIeU.htm)|Low-Light Vision|auto-trad|
|[sfWdhP3dRBdtmiol.htm](pathfinder-bestiary-2-items/sfWdhP3dRBdtmiol.htm)|Blood Drain|auto-trad|
|[Sg3CFomZepZdIWcH.htm](pathfinder-bestiary-2-items/Sg3CFomZepZdIWcH.htm)|Death Gaze|auto-trad|
|[sGGppAnKyKB4kipq.htm](pathfinder-bestiary-2-items/sGGppAnKyKB4kipq.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[sguXTwhSIa1swJ1p.htm](pathfinder-bestiary-2-items/sguXTwhSIa1swJ1p.htm)|Tongue Grab|auto-trad|
|[SHBwJIixLi2Gr8Lx.htm](pathfinder-bestiary-2-items/SHBwJIixLi2Gr8Lx.htm)|Savage Assault|auto-trad|
|[SIJWWzmQ2ZLmATdy.htm](pathfinder-bestiary-2-items/SIJWWzmQ2ZLmATdy.htm)|Claw|auto-trad|
|[sIL9AEoQV1qcrskK.htm](pathfinder-bestiary-2-items/sIL9AEoQV1qcrskK.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[SITpZWO5sj03l95G.htm](pathfinder-bestiary-2-items/SITpZWO5sj03l95G.htm)|Occult Innate Spells|auto-trad|
|[sjk7gsZB7YAEVqjS.htm](pathfinder-bestiary-2-items/sjk7gsZB7YAEVqjS.htm)|Focus Gaze|auto-trad|
|[sJmlL0pxIhzEUZxO.htm](pathfinder-bestiary-2-items/sJmlL0pxIhzEUZxO.htm)|Trample|auto-trad|
|[SLi7KeKZZiNBPdi5.htm](pathfinder-bestiary-2-items/SLi7KeKZZiNBPdi5.htm)|Primal Prepared Spells|auto-trad|
|[sLqxJoKDLd1T2p2i.htm](pathfinder-bestiary-2-items/sLqxJoKDLd1T2p2i.htm)|Solid Blow|auto-trad|
|[SLu0NlKYzhM3DVPZ.htm](pathfinder-bestiary-2-items/SLu0NlKYzhM3DVPZ.htm)|Aura of Doom|auto-trad|
|[SLw6k2GamdDXVK6d.htm](pathfinder-bestiary-2-items/SLw6k2GamdDXVK6d.htm)|Scent|auto-trad|
|[sMafmLEOWxv5RH1h.htm](pathfinder-bestiary-2-items/sMafmLEOWxv5RH1h.htm)|Jaws|auto-trad|
|[sMNeAt05PFEaDVIK.htm](pathfinder-bestiary-2-items/sMNeAt05PFEaDVIK.htm)|Entropy Sense|auto-trad|
|[sMQewzNATahHc65F.htm](pathfinder-bestiary-2-items/sMQewzNATahHc65F.htm)|Death Implosion|auto-trad|
|[Smr9bqHYQl1EI48C.htm](pathfinder-bestiary-2-items/Smr9bqHYQl1EI48C.htm)|Dragon Heat|auto-trad|
|[sN3JKaIDG5kFRAvC.htm](pathfinder-bestiary-2-items/sN3JKaIDG5kFRAvC.htm)|Natural Camouflage|auto-trad|
|[snHGvNo3Y3eluuUN.htm](pathfinder-bestiary-2-items/snHGvNo3Y3eluuUN.htm)|Compel Courage|auto-trad|
|[snsO2nPGYHe6pZx0.htm](pathfinder-bestiary-2-items/snsO2nPGYHe6pZx0.htm)|Create Web Weaponry|auto-trad|
|[SNtv0J2CRa2RFhi9.htm](pathfinder-bestiary-2-items/SNtv0J2CRa2RFhi9.htm)|Entropic Touch|auto-trad|
|[sNUoqbThXZbfj5S7.htm](pathfinder-bestiary-2-items/sNUoqbThXZbfj5S7.htm)|Fade from View|auto-trad|
|[so21bQRtejWVxF5S.htm](pathfinder-bestiary-2-items/so21bQRtejWVxF5S.htm)|Twisting Tail|auto-trad|
|[So7Gr127G3zbRJAt.htm](pathfinder-bestiary-2-items/So7Gr127G3zbRJAt.htm)|Fist|auto-trad|
|[SOapd5GiFsybvQAC.htm](pathfinder-bestiary-2-items/SOapd5GiFsybvQAC.htm)|Occult Innate Spells|auto-trad|
|[SOgonEKuJHWkgeNc.htm](pathfinder-bestiary-2-items/SOgonEKuJHWkgeNc.htm)|Darkvision|auto-trad|
|[sP3rroIKBiOI5Xug.htm](pathfinder-bestiary-2-items/sP3rroIKBiOI5Xug.htm)|Divine Innate Spells|auto-trad|
|[SPkIUm3665kXInNU.htm](pathfinder-bestiary-2-items/SPkIUm3665kXInNU.htm)|Primal Innate Spells|auto-trad|
|[SpQLyhSHuzpd81GZ.htm](pathfinder-bestiary-2-items/SpQLyhSHuzpd81GZ.htm)|Jaws|auto-trad|
|[SQM0OqkayL2Oywmm.htm](pathfinder-bestiary-2-items/SQM0OqkayL2Oywmm.htm)|Tail|auto-trad|
|[SqmhCTwOKtI5c7BT.htm](pathfinder-bestiary-2-items/SqmhCTwOKtI5c7BT.htm)|Mote of Light|auto-trad|
|[sqrdrxJ0d19wPysW.htm](pathfinder-bestiary-2-items/sqrdrxJ0d19wPysW.htm)|Catch Rock|auto-trad|
|[sR5alX5hZopxwTLw.htm](pathfinder-bestiary-2-items/sR5alX5hZopxwTLw.htm)|Web Bola|auto-trad|
|[sRJgd3DpiEABxebP.htm](pathfinder-bestiary-2-items/sRJgd3DpiEABxebP.htm)|Deflecting Gale|auto-trad|
|[SRNjrRDQg8BEgVDx.htm](pathfinder-bestiary-2-items/SRNjrRDQg8BEgVDx.htm)|Ferocity|auto-trad|
|[ssitSO6O1esw1YHS.htm](pathfinder-bestiary-2-items/ssitSO6O1esw1YHS.htm)|Frightful Presence|auto-trad|
|[sSkLpLlTDlkKIKAw.htm](pathfinder-bestiary-2-items/sSkLpLlTDlkKIKAw.htm)|Planar Acclimation|auto-trad|
|[st5T6nXfjzQXbUOB.htm](pathfinder-bestiary-2-items/st5T6nXfjzQXbUOB.htm)|Lancing Charge|auto-trad|
|[stgGnScdknvWx6D2.htm](pathfinder-bestiary-2-items/stgGnScdknvWx6D2.htm)|Regeneration 15 (Deactivated by Acid or Fire)|auto-trad|
|[stKoZGNXuFFgUzxE.htm](pathfinder-bestiary-2-items/stKoZGNXuFFgUzxE.htm)|Fist|auto-trad|
|[storTBRDZBrko2mc.htm](pathfinder-bestiary-2-items/storTBRDZBrko2mc.htm)|Swarm Mind|auto-trad|
|[stSp4FJbtoFzokE9.htm](pathfinder-bestiary-2-items/stSp4FJbtoFzokE9.htm)|Greater Darkvision|auto-trad|
|[Stt8YZwfNCRzNgkQ.htm](pathfinder-bestiary-2-items/Stt8YZwfNCRzNgkQ.htm)|Powerful Charge|auto-trad|
|[Su8xRpdikP4lRabN.htm](pathfinder-bestiary-2-items/Su8xRpdikP4lRabN.htm)|Fist|auto-trad|
|[SU9JTCOQpNZUjegs.htm](pathfinder-bestiary-2-items/SU9JTCOQpNZUjegs.htm)|Darkvision|auto-trad|
|[suaF5gRPb1TibfpQ.htm](pathfinder-bestiary-2-items/suaF5gRPb1TibfpQ.htm)|Hooked|auto-trad|
|[sUall0U5UbL2KkEE.htm](pathfinder-bestiary-2-items/sUall0U5UbL2KkEE.htm)|Reef Octopus Venom|auto-trad|
|[SuO1iTdYnYyeXOGq.htm](pathfinder-bestiary-2-items/SuO1iTdYnYyeXOGq.htm)|Fast Healing 5|auto-trad|
|[sUR4J0lzNGl0KfsK.htm](pathfinder-bestiary-2-items/sUR4J0lzNGl0KfsK.htm)|Tusk|auto-trad|
|[SvghWJwmAo36V4Sx.htm](pathfinder-bestiary-2-items/SvghWJwmAo36V4Sx.htm)|Grab|auto-trad|
|[sw8oj6ADIgUM0I7C.htm](pathfinder-bestiary-2-items/sw8oj6ADIgUM0I7C.htm)|Arm|auto-trad|
|[sW9lqWGbyXr7xeXh.htm](pathfinder-bestiary-2-items/sW9lqWGbyXr7xeXh.htm)|Swallow Whole|auto-trad|
|[swAJMaqujVUFqFKT.htm](pathfinder-bestiary-2-items/swAJMaqujVUFqFKT.htm)|Host Scent|auto-trad|
|[swAWlgrj8JoJq6xB.htm](pathfinder-bestiary-2-items/swAWlgrj8JoJq6xB.htm)|Darkvision|auto-trad|
|[sWbaqjXgO4EigI5j.htm](pathfinder-bestiary-2-items/sWbaqjXgO4EigI5j.htm)|Lay Web Trap|auto-trad|
|[swPjSgGgOFitRVM8.htm](pathfinder-bestiary-2-items/swPjSgGgOFitRVM8.htm)|Low-Light Vision|auto-trad|
|[swUZOpPbIdXVMQ1X.htm](pathfinder-bestiary-2-items/swUZOpPbIdXVMQ1X.htm)|Horn|auto-trad|
|[SXISaBxf4jsnV512.htm](pathfinder-bestiary-2-items/SXISaBxf4jsnV512.htm)|Breath Weapon|auto-trad|
|[sy1d3enT3AxVwNJ9.htm](pathfinder-bestiary-2-items/sy1d3enT3AxVwNJ9.htm)|Telepathy|auto-trad|
|[sy7gZgu0yKjVGQB8.htm](pathfinder-bestiary-2-items/sy7gZgu0yKjVGQB8.htm)|Ferocity|auto-trad|
|[sYzEwjtXD4GsP03b.htm](pathfinder-bestiary-2-items/sYzEwjtXD4GsP03b.htm)|Hibernation|auto-trad|
|[sZ3V72jPCOJ8oK7p.htm](pathfinder-bestiary-2-items/sZ3V72jPCOJ8oK7p.htm)|Divine Innate Spells|auto-trad|
|[SZrUoVGK2e5HE1dm.htm](pathfinder-bestiary-2-items/SZrUoVGK2e5HE1dm.htm)|Brood Leech Swarm Venom|auto-trad|
|[T0KOWsF7QwJ1oDQq.htm](pathfinder-bestiary-2-items/T0KOWsF7QwJ1oDQq.htm)|Low-Light Vision|auto-trad|
|[t0On0v7gyVoScNyY.htm](pathfinder-bestiary-2-items/t0On0v7gyVoScNyY.htm)|Staggering Sail|auto-trad|
|[T0vnmUhrqmtMKLQD.htm](pathfinder-bestiary-2-items/T0vnmUhrqmtMKLQD.htm)|Hostile Duet|auto-trad|
|[T30LeVWTmp9xhLg7.htm](pathfinder-bestiary-2-items/T30LeVWTmp9xhLg7.htm)|Darkvision|auto-trad|
|[t3cnDaUX26zIroa8.htm](pathfinder-bestiary-2-items/t3cnDaUX26zIroa8.htm)|Focus Gaze|auto-trad|
|[T4cC9H9vxNTUqnco.htm](pathfinder-bestiary-2-items/T4cC9H9vxNTUqnco.htm)|Deflecting Cloud|auto-trad|
|[T4TOIiPyHfghQy04.htm](pathfinder-bestiary-2-items/T4TOIiPyHfghQy04.htm)|Attack of Opportunity|auto-trad|
|[T5aqHU7uB2IvWwrg.htm](pathfinder-bestiary-2-items/T5aqHU7uB2IvWwrg.htm)|Surgical Rend|auto-trad|
|[T7L2FMHmpDoURw0s.htm](pathfinder-bestiary-2-items/T7L2FMHmpDoURw0s.htm)|Clinging Suckers|auto-trad|
|[T7rl8xICznuEuEmm.htm](pathfinder-bestiary-2-items/T7rl8xICznuEuEmm.htm)|Primal Innate Spells|auto-trad|
|[T7rTM38nfnCsn85I.htm](pathfinder-bestiary-2-items/T7rTM38nfnCsn85I.htm)|Jaws|auto-trad|
|[T7tMkEUVA0Pq90Yo.htm](pathfinder-bestiary-2-items/T7tMkEUVA0Pq90Yo.htm)|At-Will Spells|auto-trad|
|[t8m638wy4P8pa4PZ.htm](pathfinder-bestiary-2-items/t8m638wy4P8pa4PZ.htm)|Jaws|auto-trad|
|[t8ToZMzymYUk47az.htm](pathfinder-bestiary-2-items/t8ToZMzymYUk47az.htm)|Greater Darkvision|auto-trad|
|[t9eUo7KSFGGelyru.htm](pathfinder-bestiary-2-items/t9eUo7KSFGGelyru.htm)|Trample|auto-trad|
|[t9ReCb126y5KXuru.htm](pathfinder-bestiary-2-items/t9ReCb126y5KXuru.htm)|Opportune Witchflame|auto-trad|
|[TA0G6wsaR62bthsu.htm](pathfinder-bestiary-2-items/TA0G6wsaR62bthsu.htm)|Constant Spells|auto-trad|
|[tAdSWqK8N7bxwjab.htm](pathfinder-bestiary-2-items/tAdSWqK8N7bxwjab.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[taEoZD9fGbhrr6G9.htm](pathfinder-bestiary-2-items/taEoZD9fGbhrr6G9.htm)|Darkvision|auto-trad|
|[TAIcUgt3JKg5c7fV.htm](pathfinder-bestiary-2-items/TAIcUgt3JKg5c7fV.htm)|Scarecrow's Leer|auto-trad|
|[TBp1tYUPtYoOEonO.htm](pathfinder-bestiary-2-items/TBp1tYUPtYoOEonO.htm)|Aquatic Echolocation|auto-trad|
|[TBsuxObKIaeVagO5.htm](pathfinder-bestiary-2-items/TBsuxObKIaeVagO5.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[tByLKFHCnMANfCJV.htm](pathfinder-bestiary-2-items/tByLKFHCnMANfCJV.htm)|Fist|auto-trad|
|[tcE3BBuNT8JtsDJQ.htm](pathfinder-bestiary-2-items/tcE3BBuNT8JtsDJQ.htm)|Low-Light Vision|auto-trad|
|[TcRZ95uAXroBAwRv.htm](pathfinder-bestiary-2-items/TcRZ95uAXroBAwRv.htm)|Verdurous Ooze Acid|auto-trad|
|[tCt2b8QlMXslGLNM.htm](pathfinder-bestiary-2-items/tCt2b8QlMXslGLNM.htm)|Feel the Blades|auto-trad|
|[tCTtMSu5Ius1bg6U.htm](pathfinder-bestiary-2-items/tCTtMSu5Ius1bg6U.htm)|Primal Innate Spells|auto-trad|
|[tdnc6MMJXidby82v.htm](pathfinder-bestiary-2-items/tdnc6MMJXidby82v.htm)|Darkvision|auto-trad|
|[TdOAA9HgzD5psJwW.htm](pathfinder-bestiary-2-items/TdOAA9HgzD5psJwW.htm)|Pull Filament|auto-trad|
|[teonM0iI5aOvey0N.htm](pathfinder-bestiary-2-items/teonM0iI5aOvey0N.htm)|Fast Healing 1|auto-trad|
|[TeoUKrxXSlxbQGds.htm](pathfinder-bestiary-2-items/TeoUKrxXSlxbQGds.htm)|Divine Innate Spells|auto-trad|
|[tF3KNEXj3uSK3cil.htm](pathfinder-bestiary-2-items/tF3KNEXj3uSK3cil.htm)|Throw Rock|auto-trad|
|[TF4ZUPn2jeFMb9Fz.htm](pathfinder-bestiary-2-items/TF4ZUPn2jeFMb9Fz.htm)|Fast Healing 5|auto-trad|
|[TfHuj8LvtfwsXfXQ.htm](pathfinder-bestiary-2-items/TfHuj8LvtfwsXfXQ.htm)|Tentacle Arm|auto-trad|
|[TfK9D77zOoU9jjxj.htm](pathfinder-bestiary-2-items/TfK9D77zOoU9jjxj.htm)|Grab|auto-trad|
|[tFkVmhokDW1zCny4.htm](pathfinder-bestiary-2-items/tFkVmhokDW1zCny4.htm)|Wing|auto-trad|
|[TFsDNtIHhwteQUz9.htm](pathfinder-bestiary-2-items/TFsDNtIHhwteQUz9.htm)|Web Sense|auto-trad|
|[tG4nT3YD8KPOsT5n.htm](pathfinder-bestiary-2-items/tG4nT3YD8KPOsT5n.htm)|Grab|auto-trad|
|[tH3f4GsDerHRNPik.htm](pathfinder-bestiary-2-items/tH3f4GsDerHRNPik.htm)|Change Shape|auto-trad|
|[THkJV9WgvlzdR1Ll.htm](pathfinder-bestiary-2-items/THkJV9WgvlzdR1Ll.htm)|Arcane Innate Spells|auto-trad|
|[THUDJyCpb4at07bz.htm](pathfinder-bestiary-2-items/THUDJyCpb4at07bz.htm)|Arcane Innate Spells|auto-trad|
|[tIiXnBCscCuQi4WY.htm](pathfinder-bestiary-2-items/tIiXnBCscCuQi4WY.htm)|Favored Host|auto-trad|
|[tikGFRXVhGNo3ahM.htm](pathfinder-bestiary-2-items/tikGFRXVhGNo3ahM.htm)|Hammer|auto-trad|
|[tIQTZDimC6ewCWZ7.htm](pathfinder-bestiary-2-items/tIQTZDimC6ewCWZ7.htm)|Jaws|auto-trad|
|[tisbaQ4bvix6pdXR.htm](pathfinder-bestiary-2-items/tisbaQ4bvix6pdXR.htm)|Tail|auto-trad|
|[tIvtO7TbZgSNlr3F.htm](pathfinder-bestiary-2-items/tIvtO7TbZgSNlr3F.htm)|Constant Spells|auto-trad|
|[tjcUq4Pdk5hq6KqX.htm](pathfinder-bestiary-2-items/tjcUq4Pdk5hq6KqX.htm)|Boneshatter|auto-trad|
|[tjNFJu8XOANHpzYB.htm](pathfinder-bestiary-2-items/tjNFJu8XOANHpzYB.htm)|At-Will Spells|auto-trad|
|[tjoCXlFjIMoeCEAO.htm](pathfinder-bestiary-2-items/tjoCXlFjIMoeCEAO.htm)|Improved Grab|auto-trad|
|[tjQCRx8UN4LQPnwJ.htm](pathfinder-bestiary-2-items/tjQCRx8UN4LQPnwJ.htm)|Witchflame Kindling|auto-trad|
|[tjv4Luss5zUtzkrn.htm](pathfinder-bestiary-2-items/tjv4Luss5zUtzkrn.htm)|Darkvision|auto-trad|
|[Tjvf8RD2s6FEQelo.htm](pathfinder-bestiary-2-items/Tjvf8RD2s6FEQelo.htm)|Rock|auto-trad|
|[tKbplFnGrHjbOkKF.htm](pathfinder-bestiary-2-items/tKbplFnGrHjbOkKF.htm)|Darkvision|auto-trad|
|[TKjiZlqXKYAz5MTX.htm](pathfinder-bestiary-2-items/TKjiZlqXKYAz5MTX.htm)|All-Around Vision|auto-trad|
|[tkqzuUsreLrNt0sh.htm](pathfinder-bestiary-2-items/tkqzuUsreLrNt0sh.htm)|Aquatic Echolocation|auto-trad|
|[tlcUEtC6qZlc69zv.htm](pathfinder-bestiary-2-items/tlcUEtC6qZlc69zv.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[TlRvAbAXlF8M2LXl.htm](pathfinder-bestiary-2-items/TlRvAbAXlF8M2LXl.htm)|Jaws|auto-trad|
|[TM2S9CZ5bTyxKkXp.htm](pathfinder-bestiary-2-items/TM2S9CZ5bTyxKkXp.htm)|Unnerving Gaze|auto-trad|
|[tmi63gM84pc6s5qK.htm](pathfinder-bestiary-2-items/tmi63gM84pc6s5qK.htm)|Tongue Grab|auto-trad|
|[TNAO4qk6h9SrlrOP.htm](pathfinder-bestiary-2-items/TNAO4qk6h9SrlrOP.htm)|Sever Fate|auto-trad|
|[TnT0MH1u6Ixly1UZ.htm](pathfinder-bestiary-2-items/TnT0MH1u6Ixly1UZ.htm)|Pseudopod|auto-trad|
|[tnyUum6cGLUvry3c.htm](pathfinder-bestiary-2-items/tnyUum6cGLUvry3c.htm)|Jaws|auto-trad|
|[tO053EeFkQRNiOBR.htm](pathfinder-bestiary-2-items/tO053EeFkQRNiOBR.htm)|Draconic Momentum|auto-trad|
|[tO16N6Get5HonzDM.htm](pathfinder-bestiary-2-items/tO16N6Get5HonzDM.htm)|Primal Innate Spells|auto-trad|
|[tO90cMW3Jx22uW0F.htm](pathfinder-bestiary-2-items/tO90cMW3Jx22uW0F.htm)|Arm|auto-trad|
|[tOMw33x9d2qXTKwi.htm](pathfinder-bestiary-2-items/tOMw33x9d2qXTKwi.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[tONGWAraS1nSA49Y.htm](pathfinder-bestiary-2-items/tONGWAraS1nSA49Y.htm)|Wicked Bite|auto-trad|
|[TOzSzGQn4MrKg3wy.htm](pathfinder-bestiary-2-items/TOzSzGQn4MrKg3wy.htm)|Igniting Assault|auto-trad|
|[tPa52hjkpaRccFn3.htm](pathfinder-bestiary-2-items/tPa52hjkpaRccFn3.htm)|Tentacle|auto-trad|
|[TpfBgK9H4iQNfMgS.htm](pathfinder-bestiary-2-items/TpfBgK9H4iQNfMgS.htm)|Sting|auto-trad|
|[TPKGkvgCAQz8TKeC.htm](pathfinder-bestiary-2-items/TPKGkvgCAQz8TKeC.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[tpQDVH3NwAokh2NC.htm](pathfinder-bestiary-2-items/tpQDVH3NwAokh2NC.htm)|Breath Weapon|auto-trad|
|[tqSmdxFVDmY2CLns.htm](pathfinder-bestiary-2-items/tqSmdxFVDmY2CLns.htm)|Heavy Aura|auto-trad|
|[TQzV6HYAwG8ru5Jc.htm](pathfinder-bestiary-2-items/TQzV6HYAwG8ru5Jc.htm)|Serpentfolk Venom|auto-trad|
|[tRLWCHj0DJ8Jb78B.htm](pathfinder-bestiary-2-items/tRLWCHj0DJ8Jb78B.htm)|Focus Gaze|auto-trad|
|[tssacopT88cz2z8v.htm](pathfinder-bestiary-2-items/tssacopT88cz2z8v.htm)|Tail|auto-trad|
|[tTPvWYYvp1PgybyK.htm](pathfinder-bestiary-2-items/tTPvWYYvp1PgybyK.htm)|Swift Tracker|auto-trad|
|[TtqhvUO0vvhjbIFp.htm](pathfinder-bestiary-2-items/TtqhvUO0vvhjbIFp.htm)|Exorcism Vulnerability|auto-trad|
|[TUaSPYJP2yuCkKh9.htm](pathfinder-bestiary-2-items/TUaSPYJP2yuCkKh9.htm)|Wing Deflection|auto-trad|
|[tuQ8FrXL4iChAQA1.htm](pathfinder-bestiary-2-items/tuQ8FrXL4iChAQA1.htm)|Sunlight Petrification|auto-trad|
|[tV582k6fcdjhRPvI.htm](pathfinder-bestiary-2-items/tV582k6fcdjhRPvI.htm)|Divine Innate Spells|auto-trad|
|[tVBujCEfyKPKjA2Q.htm](pathfinder-bestiary-2-items/tVBujCEfyKPKjA2Q.htm)|Flailing Tentacles|auto-trad|
|[TvDY3FqPJsHE5Bz7.htm](pathfinder-bestiary-2-items/TvDY3FqPJsHE5Bz7.htm)|Stinger|auto-trad|
|[TVOxysBVf9C1u5wF.htm](pathfinder-bestiary-2-items/TVOxysBVf9C1u5wF.htm)|Draconic Frenzy|auto-trad|
|[TW4FUgjb7YgD89jt.htm](pathfinder-bestiary-2-items/TW4FUgjb7YgD89jt.htm)|Ancestral Guardian|auto-trad|
|[twuwqjXSZmaqi54S.htm](pathfinder-bestiary-2-items/twuwqjXSZmaqi54S.htm)|Lifesense 30 feet|auto-trad|
|[TxgMLxy2ap1ZIMWz.htm](pathfinder-bestiary-2-items/TxgMLxy2ap1ZIMWz.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[tXoIGwvSGdRcv6jU.htm](pathfinder-bestiary-2-items/tXoIGwvSGdRcv6jU.htm)|Divine Innate Spells|auto-trad|
|[TxRwH6kafpwqJtmQ.htm](pathfinder-bestiary-2-items/TxRwH6kafpwqJtmQ.htm)|Truespeech|auto-trad|
|[tygj3hMG88FeH6W1.htm](pathfinder-bestiary-2-items/tygj3hMG88FeH6W1.htm)|Draconic Resistance|auto-trad|
|[TYoj6whlYb3mxSVJ.htm](pathfinder-bestiary-2-items/TYoj6whlYb3mxSVJ.htm)|Constant Spells|auto-trad|
|[tZ28zS8D4Ibrkmjl.htm](pathfinder-bestiary-2-items/tZ28zS8D4Ibrkmjl.htm)|Thunderbolt|auto-trad|
|[TZC0DNY97EGRDFbD.htm](pathfinder-bestiary-2-items/TZC0DNY97EGRDFbD.htm)|Horns|auto-trad|
|[tZhV19HIQBeAjdki.htm](pathfinder-bestiary-2-items/tZhV19HIQBeAjdki.htm)|Jaws|auto-trad|
|[TzpdMinu4WoEyAY9.htm](pathfinder-bestiary-2-items/TzpdMinu4WoEyAY9.htm)|Occult Innate Spells|auto-trad|
|[TzQpE2J6oD8ijpuC.htm](pathfinder-bestiary-2-items/TzQpE2J6oD8ijpuC.htm)|Splinter|auto-trad|
|[u12odfdEyxk5GPY5.htm](pathfinder-bestiary-2-items/u12odfdEyxk5GPY5.htm)|Lifesense 60 feet|auto-trad|
|[u1o4MqU6HFWR8rVh.htm](pathfinder-bestiary-2-items/u1o4MqU6HFWR8rVh.htm)|Electrolocation|auto-trad|
|[U1rCcNDpkbvZuuCb.htm](pathfinder-bestiary-2-items/U1rCcNDpkbvZuuCb.htm)|Darkvision|auto-trad|
|[U291oOsn8aI5u9Za.htm](pathfinder-bestiary-2-items/U291oOsn8aI5u9Za.htm)|Darkvision|auto-trad|
|[u3s5y4pE6L6HfEAq.htm](pathfinder-bestiary-2-items/u3s5y4pE6L6HfEAq.htm)|Malleability|auto-trad|
|[u4BcgBmwi4zY27Ei.htm](pathfinder-bestiary-2-items/u4BcgBmwi4zY27Ei.htm)|Clacking Exoskeleton|auto-trad|
|[U4uu4gkB12yKgjWl.htm](pathfinder-bestiary-2-items/U4uu4gkB12yKgjWl.htm)|Jaws|auto-trad|
|[u58XzFLTCWAEQCJN.htm](pathfinder-bestiary-2-items/u58XzFLTCWAEQCJN.htm)|Writhing Arms|auto-trad|
|[u5ajL2rEGMJeJmYt.htm](pathfinder-bestiary-2-items/u5ajL2rEGMJeJmYt.htm)|Telepathy|auto-trad|
|[U5OMtZB6VwyAnL04.htm](pathfinder-bestiary-2-items/U5OMtZB6VwyAnL04.htm)|Constant Spells|auto-trad|
|[U64zKLjgQG2MzhAS.htm](pathfinder-bestiary-2-items/U64zKLjgQG2MzhAS.htm)|Wind's Guidance|auto-trad|
|[U69lb7BfCPLfTsfG.htm](pathfinder-bestiary-2-items/U69lb7BfCPLfTsfG.htm)|Attack of Opportunity (Tail Only)|auto-trad|
|[u6jbIfe9u8XJ6Pe0.htm](pathfinder-bestiary-2-items/u6jbIfe9u8XJ6Pe0.htm)|Claw|auto-trad|
|[u6kTO6HI4KtrGtds.htm](pathfinder-bestiary-2-items/u6kTO6HI4KtrGtds.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[U6lHO1793L8PN4Hc.htm](pathfinder-bestiary-2-items/U6lHO1793L8PN4Hc.htm)|Grab|auto-trad|
|[U6mlSBSkzcgtivnv.htm](pathfinder-bestiary-2-items/U6mlSBSkzcgtivnv.htm)|Scent|auto-trad|
|[u6T8tsZkOcFjvrzG.htm](pathfinder-bestiary-2-items/u6T8tsZkOcFjvrzG.htm)|Negative Healing|auto-trad|
|[u6tuxFGclP3Cf3u2.htm](pathfinder-bestiary-2-items/u6tuxFGclP3Cf3u2.htm)|Otherworldly Mind|auto-trad|
|[u7R9PDTQDTEJeQAX.htm](pathfinder-bestiary-2-items/u7R9PDTQDTEJeQAX.htm)|Keen Hearing|auto-trad|
|[U7WoIw4znbFwFj7t.htm](pathfinder-bestiary-2-items/U7WoIw4znbFwFj7t.htm)|Scent|auto-trad|
|[u9uoDYlFsBGEdg0u.htm](pathfinder-bestiary-2-items/u9uoDYlFsBGEdg0u.htm)|Sleep Gas|auto-trad|
|[U9ZtZBoz8oQqwrWa.htm](pathfinder-bestiary-2-items/U9ZtZBoz8oQqwrWa.htm)|Regeneration 15 (Deactivated by Acid or Cold)|auto-trad|
|[uAKDE9WbARcraQJx.htm](pathfinder-bestiary-2-items/uAKDE9WbARcraQJx.htm)|Clobbering Charge|auto-trad|
|[uAlQecVurdHtCKds.htm](pathfinder-bestiary-2-items/uAlQecVurdHtCKds.htm)|Primal Innate Spells|auto-trad|
|[UatC9BxVJopPgwJd.htm](pathfinder-bestiary-2-items/UatC9BxVJopPgwJd.htm)|Rotting Curse|auto-trad|
|[UbbiLTnNP4WGrar5.htm](pathfinder-bestiary-2-items/UbbiLTnNP4WGrar5.htm)|Mold Mulch|auto-trad|
|[ubHtu9kCddCNuEjt.htm](pathfinder-bestiary-2-items/ubHtu9kCddCNuEjt.htm)|Low-Light Vision|auto-trad|
|[ubj0ypG0da0nRirb.htm](pathfinder-bestiary-2-items/ubj0ypG0da0nRirb.htm)|Fade into the Light|auto-trad|
|[ubttzSAG6qyejH25.htm](pathfinder-bestiary-2-items/ubttzSAG6qyejH25.htm)|Grab|auto-trad|
|[UcQu3REAJGDNFfE0.htm](pathfinder-bestiary-2-items/UcQu3REAJGDNFfE0.htm)|Captivating Pollen|auto-trad|
|[UcRrxW2Ay0vhDBrn.htm](pathfinder-bestiary-2-items/UcRrxW2Ay0vhDBrn.htm)|Darkvision|auto-trad|
|[uD7Sv3Sf3fTDECXG.htm](pathfinder-bestiary-2-items/uD7Sv3Sf3fTDECXG.htm)|Darkvision|auto-trad|
|[uDrkrfwjoPByW5Zs.htm](pathfinder-bestiary-2-items/uDrkrfwjoPByW5Zs.htm)|Warpwaves|auto-trad|
|[uDxnASCZGKnXfoQw.htm](pathfinder-bestiary-2-items/uDxnASCZGKnXfoQw.htm)|Trample|auto-trad|
|[UE4WlNSLrotRauZG.htm](pathfinder-bestiary-2-items/UE4WlNSLrotRauZG.htm)|Lightning-Struck Curse|auto-trad|
|[UEAEQewstiRA9wbc.htm](pathfinder-bestiary-2-items/UEAEQewstiRA9wbc.htm)|Paralytic Perfection|auto-trad|
|[uGdTa0kjSsx6O3yO.htm](pathfinder-bestiary-2-items/uGdTa0kjSsx6O3yO.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[ugIVlLPaqyvekXgN.htm](pathfinder-bestiary-2-items/ugIVlLPaqyvekXgN.htm)|Darkvision|auto-trad|
|[UGoT9Ax8vT4G0yrI.htm](pathfinder-bestiary-2-items/UGoT9Ax8vT4G0yrI.htm)|Dagger|auto-trad|
|[UgqU2m5UQKRKewoU.htm](pathfinder-bestiary-2-items/UgqU2m5UQKRKewoU.htm)|Osyluth Venom|auto-trad|
|[uh97rOTxqm9aQTwC.htm](pathfinder-bestiary-2-items/uh97rOTxqm9aQTwC.htm)|Consume Berries|auto-trad|
|[UHFNjGmcb7jCzvBE.htm](pathfinder-bestiary-2-items/UHFNjGmcb7jCzvBE.htm)|Attach|auto-trad|
|[UhnqMxX22JlPHncP.htm](pathfinder-bestiary-2-items/UhnqMxX22JlPHncP.htm)|Tentacle|auto-trad|
|[uhW4OwaKUkyMz6W3.htm](pathfinder-bestiary-2-items/uhW4OwaKUkyMz6W3.htm)|Capsize|auto-trad|
|[UIgdlwmkkFjjq6Kl.htm](pathfinder-bestiary-2-items/UIgdlwmkkFjjq6Kl.htm)|Unsettled Aura|auto-trad|
|[uIPRSgEue1olkirr.htm](pathfinder-bestiary-2-items/uIPRSgEue1olkirr.htm)|Constant Spells|auto-trad|
|[Ujc2l5skhi8bH1Ue.htm](pathfinder-bestiary-2-items/Ujc2l5skhi8bH1Ue.htm)|Holy Armaments|auto-trad|
|[UjEJBgE9VWnbTMRQ.htm](pathfinder-bestiary-2-items/UjEJBgE9VWnbTMRQ.htm)|Negative Healing|auto-trad|
|[ujOn2cOoDTqdrh70.htm](pathfinder-bestiary-2-items/ujOn2cOoDTqdrh70.htm)|Claw|auto-trad|
|[ujQKL0Ru853GDYUK.htm](pathfinder-bestiary-2-items/ujQKL0Ru853GDYUK.htm)|Grab|auto-trad|
|[UjQN53iNiipJJkHh.htm](pathfinder-bestiary-2-items/UjQN53iNiipJJkHh.htm)|Branch|auto-trad|
|[uJuFK46gaxSPryGB.htm](pathfinder-bestiary-2-items/uJuFK46gaxSPryGB.htm)|Brine Spit|auto-trad|
|[UK2ayZO6tmbwgHBL.htm](pathfinder-bestiary-2-items/UK2ayZO6tmbwgHBL.htm)|Jaws|auto-trad|
|[uKp0N4KDIGVFsJ1T.htm](pathfinder-bestiary-2-items/uKp0N4KDIGVFsJ1T.htm)|Darkvision|auto-trad|
|[ULCc7i4GlRFlcgmM.htm](pathfinder-bestiary-2-items/ULCc7i4GlRFlcgmM.htm)|Primal Innate Spells|auto-trad|
|[ulrrbkt4l6SBi1z8.htm](pathfinder-bestiary-2-items/ulrrbkt4l6SBi1z8.htm)|Painsight|auto-trad|
|[uLuSAexGwbZSdlkF.htm](pathfinder-bestiary-2-items/uLuSAexGwbZSdlkF.htm)|Black Flame Knife|auto-trad|
|[uLVKT1uotgIuFDA2.htm](pathfinder-bestiary-2-items/uLVKT1uotgIuFDA2.htm)|At-Will Spells|auto-trad|
|[um06dCu0E8W9LRDd.htm](pathfinder-bestiary-2-items/um06dCu0E8W9LRDd.htm)|Entropy Sense (Imprecise) 30 feet|auto-trad|
|[umE4aR6ccDxLqz3l.htm](pathfinder-bestiary-2-items/umE4aR6ccDxLqz3l.htm)|Hand Crossbow (Hunting Spider Venom)|auto-trad|
|[UmF9ATrgifYyt9He.htm](pathfinder-bestiary-2-items/UmF9ATrgifYyt9He.htm)|Pounce|auto-trad|
|[uMPmwIYK4s395qvB.htm](pathfinder-bestiary-2-items/uMPmwIYK4s395qvB.htm)|Darkvision|auto-trad|
|[uMr0g5ycTJTsxsHh.htm](pathfinder-bestiary-2-items/uMr0g5ycTJTsxsHh.htm)|Darkvision|auto-trad|
|[uMVIaXXt18rDrOPn.htm](pathfinder-bestiary-2-items/uMVIaXXt18rDrOPn.htm)|Ravenous Breath Weapon|auto-trad|
|[UN8cAG3SmdTtuDWe.htm](pathfinder-bestiary-2-items/UN8cAG3SmdTtuDWe.htm)|Smoke Slash|auto-trad|
|[uNH8ZKqCgsh6qYis.htm](pathfinder-bestiary-2-items/uNH8ZKqCgsh6qYis.htm)|Bay|auto-trad|
|[uot20BQOy52S73qO.htm](pathfinder-bestiary-2-items/uot20BQOy52S73qO.htm)|Steal Breath|auto-trad|
|[Uoxf8sSuS6pG7QQF.htm](pathfinder-bestiary-2-items/Uoxf8sSuS6pG7QQF.htm)|Pod Spawn|auto-trad|
|[up0HVVMUk86V0DdT.htm](pathfinder-bestiary-2-items/up0HVVMUk86V0DdT.htm)|Burning Rush|auto-trad|
|[Up9zgawdSde0SLPP.htm](pathfinder-bestiary-2-items/Up9zgawdSde0SLPP.htm)|All-Around Vision|auto-trad|
|[UpD8OqCE3oJ41eCz.htm](pathfinder-bestiary-2-items/UpD8OqCE3oJ41eCz.htm)|Constant Spells|auto-trad|
|[UPUXBFdc12OlpoBV.htm](pathfinder-bestiary-2-items/UPUXBFdc12OlpoBV.htm)|Gaff|auto-trad|
|[uQ1bRoDBDEldN87d.htm](pathfinder-bestiary-2-items/uQ1bRoDBDEldN87d.htm)|Darkvision|auto-trad|
|[uQDUAoudAP9pgxja.htm](pathfinder-bestiary-2-items/uQDUAoudAP9pgxja.htm)|Soul Crush|auto-trad|
|[uQFiXxjHXOCxLHe8.htm](pathfinder-bestiary-2-items/uQFiXxjHXOCxLHe8.htm)|Vulnerability to Supernatural Darkness|auto-trad|
|[UQft7JhW6zhtWCy2.htm](pathfinder-bestiary-2-items/UQft7JhW6zhtWCy2.htm)|Draconic Frenzy|auto-trad|
|[Ur31aU2yqO6lWSV2.htm](pathfinder-bestiary-2-items/Ur31aU2yqO6lWSV2.htm)|At-Will Spells|auto-trad|
|[uR5sStuJbBbR9XK6.htm](pathfinder-bestiary-2-items/uR5sStuJbBbR9XK6.htm)|Hatchet|auto-trad|
|[urEbuoDUEyOj6f0A.htm](pathfinder-bestiary-2-items/urEbuoDUEyOj6f0A.htm)|Greater Darkvision|auto-trad|
|[URm0Bj51PHUjgnXS.htm](pathfinder-bestiary-2-items/URm0Bj51PHUjgnXS.htm)|Breath Weapon|auto-trad|
|[Us7BcRckPUy5WuaZ.htm](pathfinder-bestiary-2-items/Us7BcRckPUy5WuaZ.htm)|Swallow Whole|auto-trad|
|[UScYAVgddobTyqOw.htm](pathfinder-bestiary-2-items/UScYAVgddobTyqOw.htm)|Fist|auto-trad|
|[usIKpszaYrCWfN8r.htm](pathfinder-bestiary-2-items/usIKpszaYrCWfN8r.htm)|Shadow Breath|auto-trad|
|[UTaJ0rJXzssLeINs.htm](pathfinder-bestiary-2-items/UTaJ0rJXzssLeINs.htm)|Motion Sense|auto-trad|
|[uTYt777rmxzqgumS.htm](pathfinder-bestiary-2-items/uTYt777rmxzqgumS.htm)|Sorcerer Bloodline Spells|auto-trad|
|[UuDDHSI5hH9lHpZv.htm](pathfinder-bestiary-2-items/UuDDHSI5hH9lHpZv.htm)|Attack of Opportunity (Tail Only)|auto-trad|
|[uuI2Msz0ObGGd1kj.htm](pathfinder-bestiary-2-items/uuI2Msz0ObGGd1kj.htm)|Scent|auto-trad|
|[Uus1jecX7kww8spI.htm](pathfinder-bestiary-2-items/Uus1jecX7kww8spI.htm)|Fast Healing 2|auto-trad|
|[UuUlz5A5IwoGhe6n.htm](pathfinder-bestiary-2-items/UuUlz5A5IwoGhe6n.htm)|Frightful Strike|auto-trad|
|[UUVUKTE7xQsPxDNy.htm](pathfinder-bestiary-2-items/UUVUKTE7xQsPxDNy.htm)|Bone Shard|auto-trad|
|[UuxtWMTDYGzdEzIA.htm](pathfinder-bestiary-2-items/UuxtWMTDYGzdEzIA.htm)|Glowing Touch|auto-trad|
|[uvi6KWWlZsgw6kYw.htm](pathfinder-bestiary-2-items/uvi6KWWlZsgw6kYw.htm)|Sting|auto-trad|
|[UvIa41hvZRkj3zM0.htm](pathfinder-bestiary-2-items/UvIa41hvZRkj3zM0.htm)|Tremorsense|auto-trad|
|[UVvsMOpzGvjOBeG1.htm](pathfinder-bestiary-2-items/UVvsMOpzGvjOBeG1.htm)|Protean Anatomy 15|auto-trad|
|[uW30KxL4NUlDPAi5.htm](pathfinder-bestiary-2-items/uW30KxL4NUlDPAi5.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[uXmhD5XQjEMFuZSJ.htm](pathfinder-bestiary-2-items/uXmhD5XQjEMFuZSJ.htm)|Darkvision|auto-trad|
|[uY2J1JrUYcVbFp2j.htm](pathfinder-bestiary-2-items/uY2J1JrUYcVbFp2j.htm)|Stunning Display|auto-trad|
|[UYeaGbpn427tN1hJ.htm](pathfinder-bestiary-2-items/UYeaGbpn427tN1hJ.htm)|Oar|auto-trad|
|[uYHwkATnIk2mUvIu.htm](pathfinder-bestiary-2-items/uYHwkATnIk2mUvIu.htm)|Tentacle|auto-trad|
|[uYqNB5URDvfKjzHe.htm](pathfinder-bestiary-2-items/uYqNB5URDvfKjzHe.htm)|Morningstar|auto-trad|
|[uyXWXmerHoFxX1jU.htm](pathfinder-bestiary-2-items/uyXWXmerHoFxX1jU.htm)|Boiling Rain|auto-trad|
|[uzav7YxjDugksaRl.htm](pathfinder-bestiary-2-items/uzav7YxjDugksaRl.htm)|Low-Light Vision|auto-trad|
|[UZc2Evb7hxJkfGtx.htm](pathfinder-bestiary-2-items/UZc2Evb7hxJkfGtx.htm)|Breath Weapon|auto-trad|
|[uze31xboBPgWCYgP.htm](pathfinder-bestiary-2-items/uze31xboBPgWCYgP.htm)|Shortsword|auto-trad|
|[uzTaJDrIHuQpGZCH.htm](pathfinder-bestiary-2-items/uzTaJDrIHuQpGZCH.htm)|Greater Darkvision|auto-trad|
|[V01q458qlTH6dA7i.htm](pathfinder-bestiary-2-items/V01q458qlTH6dA7i.htm)|Darkvision|auto-trad|
|[V0hJjrywdDfeOIZf.htm](pathfinder-bestiary-2-items/V0hJjrywdDfeOIZf.htm)|Scent (Imprecise) 30 feet|auto-trad|
|[v0kg2s0AeP7Pv7zn.htm](pathfinder-bestiary-2-items/v0kg2s0AeP7Pv7zn.htm)|Ravenous Jaws|auto-trad|
|[v1Nae4WDwZunhsHt.htm](pathfinder-bestiary-2-items/v1Nae4WDwZunhsHt.htm)|Regeneration 15 (Deactivated by Chaotic)|auto-trad|
|[v2by5D3cMGyVu78a.htm](pathfinder-bestiary-2-items/v2by5D3cMGyVu78a.htm)|Ritual Gate|auto-trad|
|[V4aoUtsug8TREjXX.htm](pathfinder-bestiary-2-items/V4aoUtsug8TREjXX.htm)|Swarm Spit|auto-trad|
|[V4PELj4i5rSeImC7.htm](pathfinder-bestiary-2-items/V4PELj4i5rSeImC7.htm)|Darkvision|auto-trad|
|[V4TPpwFX1g237Ekq.htm](pathfinder-bestiary-2-items/V4TPpwFX1g237Ekq.htm)|Claw|auto-trad|
|[V4WvWjukxtroI8Xs.htm](pathfinder-bestiary-2-items/V4WvWjukxtroI8Xs.htm)|Swallow Whole|auto-trad|
|[V5AYyUYH7UjuXk8T.htm](pathfinder-bestiary-2-items/V5AYyUYH7UjuXk8T.htm)|Darkvision|auto-trad|
|[V5c6DTpIF11FCsWw.htm](pathfinder-bestiary-2-items/V5c6DTpIF11FCsWw.htm)|Drain Life|auto-trad|
|[V5mIlHkiRCxhndVN.htm](pathfinder-bestiary-2-items/V5mIlHkiRCxhndVN.htm)|Feeding Frenzy|auto-trad|
|[V5Py4m6Gm8m9Z8I2.htm](pathfinder-bestiary-2-items/V5Py4m6Gm8m9Z8I2.htm)|Draconic Momentum|auto-trad|
|[V5YgaBwWcZPdAj1X.htm](pathfinder-bestiary-2-items/V5YgaBwWcZPdAj1X.htm)|Darkvision|auto-trad|
|[v65HAbd7O3gZv2VP.htm](pathfinder-bestiary-2-items/v65HAbd7O3gZv2VP.htm)|Eerie Flexibility|auto-trad|
|[v6dHZF6ZJriIUH9V.htm](pathfinder-bestiary-2-items/v6dHZF6ZJriIUH9V.htm)|Low-Light Vision|auto-trad|
|[v6WETOqHL0U4nwur.htm](pathfinder-bestiary-2-items/v6WETOqHL0U4nwur.htm)|Telepathy|auto-trad|
|[V7yL5WF6xEMuHMIf.htm](pathfinder-bestiary-2-items/V7yL5WF6xEMuHMIf.htm)|Grab|auto-trad|
|[v8A5DhitrEJIAAL1.htm](pathfinder-bestiary-2-items/v8A5DhitrEJIAAL1.htm)|Consume Soul|auto-trad|
|[V8J7D9aK5N4MpaBI.htm](pathfinder-bestiary-2-items/V8J7D9aK5N4MpaBI.htm)|Lifesense|auto-trad|
|[V90pHqyo14mQzVuc.htm](pathfinder-bestiary-2-items/V90pHqyo14mQzVuc.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[V9cC85qOc8a3vbBc.htm](pathfinder-bestiary-2-items/V9cC85qOc8a3vbBc.htm)|Abyssal Rot|auto-trad|
|[V9Ei1dwq0RUilrjT.htm](pathfinder-bestiary-2-items/V9Ei1dwq0RUilrjT.htm)|Telepathy 100 feet (With Spectral Thralls Only)|auto-trad|
|[V9VYMbFdFCqt2IqA.htm](pathfinder-bestiary-2-items/V9VYMbFdFCqt2IqA.htm)|Attack of Opportunity|auto-trad|
|[vApfqAgqE1h1XDQe.htm](pathfinder-bestiary-2-items/vApfqAgqE1h1XDQe.htm)|Motion Sense|auto-trad|
|[vAqYhN2FTor4CPFU.htm](pathfinder-bestiary-2-items/vAqYhN2FTor4CPFU.htm)|Tendril|auto-trad|
|[VaxEKl3gTn1iAnvf.htm](pathfinder-bestiary-2-items/VaxEKl3gTn1iAnvf.htm)|Claw|auto-trad|
|[vb0fFWLf39zgAI4N.htm](pathfinder-bestiary-2-items/vb0fFWLf39zgAI4N.htm)|Spear|auto-trad|
|[vBhPWXgs8q8icc85.htm](pathfinder-bestiary-2-items/vBhPWXgs8q8icc85.htm)|Draconic Frenzy|auto-trad|
|[vbL3yHO5ysgCJOcD.htm](pathfinder-bestiary-2-items/vbL3yHO5ysgCJOcD.htm)|Darkvision|auto-trad|
|[vC8tzGqfa79am1Xm.htm](pathfinder-bestiary-2-items/vC8tzGqfa79am1Xm.htm)|Tusk|auto-trad|
|[vCEb36Sv06OtmLaz.htm](pathfinder-bestiary-2-items/vCEb36Sv06OtmLaz.htm)|Rhinoceros Charge|auto-trad|
|[vCp6u7IXfs7VJRFF.htm](pathfinder-bestiary-2-items/vCp6u7IXfs7VJRFF.htm)|Scimitar|auto-trad|
|[VD3WAa6HAnP9FYx6.htm](pathfinder-bestiary-2-items/VD3WAa6HAnP9FYx6.htm)|Ferocity|auto-trad|
|[vd8w1UZaLid8WPrA.htm](pathfinder-bestiary-2-items/vd8w1UZaLid8WPrA.htm)|Claw|auto-trad|
|[vda7kHsnuwSQWtXd.htm](pathfinder-bestiary-2-items/vda7kHsnuwSQWtXd.htm)|Grab|auto-trad|
|[VDa8nmlZXUFf1Y7z.htm](pathfinder-bestiary-2-items/VDa8nmlZXUFf1Y7z.htm)|Jaws|auto-trad|
|[VdCpMDU3TyIElL0X.htm](pathfinder-bestiary-2-items/VdCpMDU3TyIElL0X.htm)|Darkvision|auto-trad|
|[VdCXQ44TAdADVOEd.htm](pathfinder-bestiary-2-items/VdCXQ44TAdADVOEd.htm)|Low-Light Vision|auto-trad|
|[VDFewubyHTGzI0bL.htm](pathfinder-bestiary-2-items/VDFewubyHTGzI0bL.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[VDW4W2k1N88INhBp.htm](pathfinder-bestiary-2-items/VDW4W2k1N88INhBp.htm)|Breath Weapon|auto-trad|
|[VDwn97NFRX24iTjv.htm](pathfinder-bestiary-2-items/VDwn97NFRX24iTjv.htm)|Claw|auto-trad|
|[vEMwAfv76TagQTy9.htm](pathfinder-bestiary-2-items/vEMwAfv76TagQTy9.htm)|Hurl Net|auto-trad|
|[Vf0GMSeZbKtccykV.htm](pathfinder-bestiary-2-items/Vf0GMSeZbKtccykV.htm)|Draconic Momentum|auto-trad|
|[VFprYdFvs654DtJ6.htm](pathfinder-bestiary-2-items/VFprYdFvs654DtJ6.htm)|Grab|auto-trad|
|[VGBhMtipbBzkkHbO.htm](pathfinder-bestiary-2-items/VGBhMtipbBzkkHbO.htm)|Lifesense|auto-trad|
|[VHBwi6X1gEpQ8YsT.htm](pathfinder-bestiary-2-items/VHBwi6X1gEpQ8YsT.htm)|Flaming Bastard Sword|auto-trad|
|[VhRB4rDZ9bnYht7Z.htm](pathfinder-bestiary-2-items/VhRB4rDZ9bnYht7Z.htm)|Constrict|auto-trad|
|[vHtf6k3iRMSkYTZd.htm](pathfinder-bestiary-2-items/vHtf6k3iRMSkYTZd.htm)|Darkvision|auto-trad|
|[vID81IzA60CV6yD2.htm](pathfinder-bestiary-2-items/vID81IzA60CV6yD2.htm)|Frightful Presence|auto-trad|
|[vIzThDo5KtpM1fnu.htm](pathfinder-bestiary-2-items/vIzThDo5KtpM1fnu.htm)|Fangs|auto-trad|
|[vKcQxEXLQx45iyeq.htm](pathfinder-bestiary-2-items/vKcQxEXLQx45iyeq.htm)|Grimstalker Sap|auto-trad|
|[VKqHnTDh4E88cAGN.htm](pathfinder-bestiary-2-items/VKqHnTDh4E88cAGN.htm)|Primal Innate Spells|auto-trad|
|[VKysBYdqHBzZ3aOg.htm](pathfinder-bestiary-2-items/VKysBYdqHBzZ3aOg.htm)|Breath Weapon|auto-trad|
|[VL2af2GD7VsPiI1u.htm](pathfinder-bestiary-2-items/VL2af2GD7VsPiI1u.htm)|Jaws|auto-trad|
|[vl5TAyZBV7YfjwyO.htm](pathfinder-bestiary-2-items/vl5TAyZBV7YfjwyO.htm)|Scent|auto-trad|
|[VLQ2Vi92N8KogGpN.htm](pathfinder-bestiary-2-items/VLQ2Vi92N8KogGpN.htm)|Light Blindness|auto-trad|
|[vLuAkberNsYeDSD0.htm](pathfinder-bestiary-2-items/vLuAkberNsYeDSD0.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[vLZK0xL9O8b27GxA.htm](pathfinder-bestiary-2-items/vLZK0xL9O8b27GxA.htm)|Wavesense (Imprecise) 30 feet|auto-trad|
|[Vm3TYdn6BMYCQ0MR.htm](pathfinder-bestiary-2-items/Vm3TYdn6BMYCQ0MR.htm)|Claw|auto-trad|
|[vm77jtXXqIjODOmX.htm](pathfinder-bestiary-2-items/vm77jtXXqIjODOmX.htm)|Rebuke Soul|auto-trad|
|[vMozJKWMEqEZvBOl.htm](pathfinder-bestiary-2-items/vMozJKWMEqEZvBOl.htm)|Jaws|auto-trad|
|[vMUHP7DevP54tmz4.htm](pathfinder-bestiary-2-items/vMUHP7DevP54tmz4.htm)|Chameleon Skin|auto-trad|
|[vmyBkhvE34HW6vpl.htm](pathfinder-bestiary-2-items/vmyBkhvE34HW6vpl.htm)|Glowing Mucus|auto-trad|
|[vMYnNlSJnYJepYe8.htm](pathfinder-bestiary-2-items/vMYnNlSJnYJepYe8.htm)|Impaling Barb|auto-trad|
|[VN24JoB39HXPdS3r.htm](pathfinder-bestiary-2-items/VN24JoB39HXPdS3r.htm)|Ravenous Repast|auto-trad|
|[VnUmVnOCMIOvu7ja.htm](pathfinder-bestiary-2-items/VnUmVnOCMIOvu7ja.htm)|Flaming Armaments|auto-trad|
|[vOhew2ogdaZmv4TY.htm](pathfinder-bestiary-2-items/vOhew2ogdaZmv4TY.htm)|Claw|auto-trad|
|[voMMMHUogUu7LSeF.htm](pathfinder-bestiary-2-items/voMMMHUogUu7LSeF.htm)|Mocking Touch|auto-trad|
|[vorcYiRkw5YIs0Qy.htm](pathfinder-bestiary-2-items/vorcYiRkw5YIs0Qy.htm)|Trample|auto-trad|
|[VOzKuytxsOHWyPEJ.htm](pathfinder-bestiary-2-items/VOzKuytxsOHWyPEJ.htm)|Darkvision|auto-trad|
|[vP6KGkCy0KpMQarQ.htm](pathfinder-bestiary-2-items/vP6KGkCy0KpMQarQ.htm)|Sense Portal|auto-trad|
|[VPVWe9JZVLpTixZC.htm](pathfinder-bestiary-2-items/VPVWe9JZVLpTixZC.htm)|Change Shape|auto-trad|
|[VpzTcikdwnBsqtN8.htm](pathfinder-bestiary-2-items/VpzTcikdwnBsqtN8.htm)|Regeneration 2 (Deactivated by Good or Silver)|auto-trad|
|[vPzVhI5rLf9ZTI5N.htm](pathfinder-bestiary-2-items/vPzVhI5rLf9ZTI5N.htm)|Constant Spells|auto-trad|
|[VQZuQhcA0A15y8kQ.htm](pathfinder-bestiary-2-items/VQZuQhcA0A15y8kQ.htm)|Trample|auto-trad|
|[VRDZl6y3aAge5VyP.htm](pathfinder-bestiary-2-items/VRDZl6y3aAge5VyP.htm)|Raise Serpent|auto-trad|
|[vrswuJTbGgycV77r.htm](pathfinder-bestiary-2-items/vrswuJTbGgycV77r.htm)|Vomit|auto-trad|
|[VrxOb3MaxvXl1OR3.htm](pathfinder-bestiary-2-items/VrxOb3MaxvXl1OR3.htm)|Claw|auto-trad|
|[vtC1BWVjxbgjcJ2H.htm](pathfinder-bestiary-2-items/vtC1BWVjxbgjcJ2H.htm)|Darkvision|auto-trad|
|[VTh3x19pApL0tjtU.htm](pathfinder-bestiary-2-items/VTh3x19pApL0tjtU.htm)|Divine Innate Spells|auto-trad|
|[VTmtaRNVlzTpmhif.htm](pathfinder-bestiary-2-items/VTmtaRNVlzTpmhif.htm)|Scimitar|auto-trad|
|[VtQnDFC3P9Efec5Z.htm](pathfinder-bestiary-2-items/VtQnDFC3P9Efec5Z.htm)|Darkvision|auto-trad|
|[vu2E8xDlE75Jm0Mt.htm](pathfinder-bestiary-2-items/vu2E8xDlE75Jm0Mt.htm)|Low-Light Vision|auto-trad|
|[vu4nBmfeMF19jeah.htm](pathfinder-bestiary-2-items/vu4nBmfeMF19jeah.htm)|Radiant Blow|auto-trad|
|[vu53NeOG28PzBXPL.htm](pathfinder-bestiary-2-items/vu53NeOG28PzBXPL.htm)|Darkvision|auto-trad|
|[vuAvJlvQLWub8gyW.htm](pathfinder-bestiary-2-items/vuAvJlvQLWub8gyW.htm)|Strangling Fingers|auto-trad|
|[vvGfXGSxCsVkyVlu.htm](pathfinder-bestiary-2-items/vvGfXGSxCsVkyVlu.htm)|Scent|auto-trad|
|[vVt7KeVPbqT0jHZQ.htm](pathfinder-bestiary-2-items/vVt7KeVPbqT0jHZQ.htm)|Clutch|auto-trad|
|[vVucSfThIT1iEEjO.htm](pathfinder-bestiary-2-items/vVucSfThIT1iEEjO.htm)|Hook Shake|auto-trad|
|[VWC3NNUYpN6qbf09.htm](pathfinder-bestiary-2-items/VWC3NNUYpN6qbf09.htm)|Breath Weapon|auto-trad|
|[VwEDItuYmaW2BAkI.htm](pathfinder-bestiary-2-items/VwEDItuYmaW2BAkI.htm)|Draconic Momentum|auto-trad|
|[VwEyLSgIQwBeoPyV.htm](pathfinder-bestiary-2-items/VwEyLSgIQwBeoPyV.htm)|Nature's Infusion|auto-trad|
|[VxDq9bJ1tuymcMTt.htm](pathfinder-bestiary-2-items/VxDq9bJ1tuymcMTt.htm)|At-Will Spells|auto-trad|
|[VyJyNz5V3NR5Gylr.htm](pathfinder-bestiary-2-items/VyJyNz5V3NR5Gylr.htm)|Axe Vulnerability 5|auto-trad|
|[vYo1emHFZe2lLLBo.htm](pathfinder-bestiary-2-items/vYo1emHFZe2lLLBo.htm)|Constant Spells|auto-trad|
|[vZ2zVHToKsyYJOTI.htm](pathfinder-bestiary-2-items/vZ2zVHToKsyYJOTI.htm)|Low-Light Vision|auto-trad|
|[VzK5CD8g2ofWRIkk.htm](pathfinder-bestiary-2-items/VzK5CD8g2ofWRIkk.htm)|Improved Grab|auto-trad|
|[VzMnFSYbgzk4CXes.htm](pathfinder-bestiary-2-items/VzMnFSYbgzk4CXes.htm)|Trackless|auto-trad|
|[W0kFuPmuWmicQuUc.htm](pathfinder-bestiary-2-items/W0kFuPmuWmicQuUc.htm)|Darkvision|auto-trad|
|[W0OH5fsPmoPURYsD.htm](pathfinder-bestiary-2-items/W0OH5fsPmoPURYsD.htm)|Claw|auto-trad|
|[w0XMZ6GmkzJcCRyO.htm](pathfinder-bestiary-2-items/w0XMZ6GmkzJcCRyO.htm)|Piercing Shriek|auto-trad|
|[W1abGwpcG9yJOB9j.htm](pathfinder-bestiary-2-items/W1abGwpcG9yJOB9j.htm)|Constant Spells|auto-trad|
|[W1bCVUm0sI8Wl7Bs.htm](pathfinder-bestiary-2-items/W1bCVUm0sI8Wl7Bs.htm)|Ghost Bane|auto-trad|
|[W1PoKy0P1n1NYE7q.htm](pathfinder-bestiary-2-items/W1PoKy0P1n1NYE7q.htm)|Low-Light Vision|auto-trad|
|[w1UuelNH4N0BPFBe.htm](pathfinder-bestiary-2-items/w1UuelNH4N0BPFBe.htm)|Attack of Opportunity|auto-trad|
|[W3rITonxbyLEARlP.htm](pathfinder-bestiary-2-items/W3rITonxbyLEARlP.htm)|Breath Weapon|auto-trad|
|[W5c599K6o0Z10LCq.htm](pathfinder-bestiary-2-items/W5c599K6o0Z10LCq.htm)|Jaws|auto-trad|
|[W6gUfzomGXvlnDjI.htm](pathfinder-bestiary-2-items/W6gUfzomGXvlnDjI.htm)|Telepathy|auto-trad|
|[W7bXRQuNuOabEMJg.htm](pathfinder-bestiary-2-items/W7bXRQuNuOabEMJg.htm)|Negative Healing|auto-trad|
|[w7j7MOc866dBtx82.htm](pathfinder-bestiary-2-items/w7j7MOc866dBtx82.htm)|Scent|auto-trad|
|[w7tx24OC6vP2T7De.htm](pathfinder-bestiary-2-items/w7tx24OC6vP2T7De.htm)|Spray Pollen|auto-trad|
|[W7Wjl7sNOZeiCQDu.htm](pathfinder-bestiary-2-items/W7Wjl7sNOZeiCQDu.htm)|Splinter|auto-trad|
|[W86zITZjQ9sKIM3N.htm](pathfinder-bestiary-2-items/W86zITZjQ9sKIM3N.htm)|Petrifying Gaze|auto-trad|
|[W8H9WsxFN6hIlghm.htm](pathfinder-bestiary-2-items/W8H9WsxFN6hIlghm.htm)|Darkvision|auto-trad|
|[W8NWW39lZmTUYk0e.htm](pathfinder-bestiary-2-items/W8NWW39lZmTUYk0e.htm)|Water Sprint|auto-trad|
|[W9urt04ZY9TmtKEz.htm](pathfinder-bestiary-2-items/W9urt04ZY9TmtKEz.htm)|Jaws|auto-trad|
|[W9vOqV3RGCnKCbHu.htm](pathfinder-bestiary-2-items/W9vOqV3RGCnKCbHu.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[wA3oz36wHS6e53Sa.htm](pathfinder-bestiary-2-items/wA3oz36wHS6e53Sa.htm)|Destructive Smash|auto-trad|
|[WbbgLFwKjXAiRHsC.htm](pathfinder-bestiary-2-items/WbbgLFwKjXAiRHsC.htm)|Whipping Tentacles|auto-trad|
|[Wbgjzf3goWRWkItA.htm](pathfinder-bestiary-2-items/Wbgjzf3goWRWkItA.htm)|Claw|auto-trad|
|[wcSlJpWbxhokd7Vc.htm](pathfinder-bestiary-2-items/wcSlJpWbxhokd7Vc.htm)|Pseudopod|auto-trad|
|[WdzuTa2nPDcswvjz.htm](pathfinder-bestiary-2-items/WdzuTa2nPDcswvjz.htm)|Disgorged Mucus|auto-trad|
|[wea1MnyUIhvtedhE.htm](pathfinder-bestiary-2-items/wea1MnyUIhvtedhE.htm)|Primal Innate Spells|auto-trad|
|[weN7yMZk4CWGAypb.htm](pathfinder-bestiary-2-items/weN7yMZk4CWGAypb.htm)|Beak|auto-trad|
|[Wez6v2oJoatkzzVp.htm](pathfinder-bestiary-2-items/Wez6v2oJoatkzzVp.htm)|Throw Rock|auto-trad|
|[wEzrLhyzfEwcfMlc.htm](pathfinder-bestiary-2-items/wEzrLhyzfEwcfMlc.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[wFvcOmqLVFa5RuAY.htm](pathfinder-bestiary-2-items/wFvcOmqLVFa5RuAY.htm)|Longspear|auto-trad|
|[whb2OZIXgagZrUoX.htm](pathfinder-bestiary-2-items/whb2OZIXgagZrUoX.htm)|Corrosive Surface|auto-trad|
|[WHISh35la4f2ULAj.htm](pathfinder-bestiary-2-items/WHISh35la4f2ULAj.htm)|Stunning Chain|auto-trad|
|[wHsOiFn1P7QcaaJ2.htm](pathfinder-bestiary-2-items/wHsOiFn1P7QcaaJ2.htm)|Stolen Identity|auto-trad|
|[WIlbTCI5PaEEizfq.htm](pathfinder-bestiary-2-items/WIlbTCI5PaEEizfq.htm)|Hidden Movement|auto-trad|
|[WIPFxMzyJ87R3t7k.htm](pathfinder-bestiary-2-items/WIPFxMzyJ87R3t7k.htm)|Supernatural Speed|auto-trad|
|[WjdPeEIa3jf7j8U2.htm](pathfinder-bestiary-2-items/WjdPeEIa3jf7j8U2.htm)|Constrict|auto-trad|
|[WJHihy42QBNSUonW.htm](pathfinder-bestiary-2-items/WJHihy42QBNSUonW.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[WKRlqiMNA7n19s8U.htm](pathfinder-bestiary-2-items/WKRlqiMNA7n19s8U.htm)|Hoof|auto-trad|
|[wKTyf3x01s0VjUOw.htm](pathfinder-bestiary-2-items/wKTyf3x01s0VjUOw.htm)|Darkvision|auto-trad|
|[WKWzTrWyiPJNihmZ.htm](pathfinder-bestiary-2-items/WKWzTrWyiPJNihmZ.htm)|Thoughtsense|auto-trad|
|[WLcykyc9hp2QiTO4.htm](pathfinder-bestiary-2-items/WLcykyc9hp2QiTO4.htm)|Serpentfolk Venom|auto-trad|
|[wleIqE8aRKNBcgOU.htm](pathfinder-bestiary-2-items/wleIqE8aRKNBcgOU.htm)|Darkvision|auto-trad|
|[WleXuHzXfn7II29b.htm](pathfinder-bestiary-2-items/WleXuHzXfn7II29b.htm)|Yamaraj Venom|auto-trad|
|[wlmBL32OvWdlwr0B.htm](pathfinder-bestiary-2-items/wlmBL32OvWdlwr0B.htm)|Primal Prepared Spells|auto-trad|
|[WmPbJSiwT9lndOlq.htm](pathfinder-bestiary-2-items/WmPbJSiwT9lndOlq.htm)|Grab|auto-trad|
|[wnZi8QCOMG86Db4l.htm](pathfinder-bestiary-2-items/wnZi8QCOMG86Db4l.htm)|Draconic Frenzy|auto-trad|
|[WocBezkZj15D3pxg.htm](pathfinder-bestiary-2-items/WocBezkZj15D3pxg.htm)|Tremorsense 30 feet|auto-trad|
|[wOmBOkeQmHwUzF7p.htm](pathfinder-bestiary-2-items/wOmBOkeQmHwUzF7p.htm)|Darkvision|auto-trad|
|[WopfIMXZtm7V3YUE.htm](pathfinder-bestiary-2-items/WopfIMXZtm7V3YUE.htm)|Greater Darkvision|auto-trad|
|[WOqo2UC99RfWFqA0.htm](pathfinder-bestiary-2-items/WOqo2UC99RfWFqA0.htm)|Darkvision|auto-trad|
|[wOW7iWJBYxXBP4Dm.htm](pathfinder-bestiary-2-items/wOW7iWJBYxXBP4Dm.htm)|Darkvision|auto-trad|
|[WPlqW6SIzbL8jscs.htm](pathfinder-bestiary-2-items/WPlqW6SIzbL8jscs.htm)|Focused Gaze|auto-trad|
|[wPLUpFiWxDX00EQJ.htm](pathfinder-bestiary-2-items/wPLUpFiWxDX00EQJ.htm)|Horn|auto-trad|
|[WpPjPwdGV3sTxsSN.htm](pathfinder-bestiary-2-items/WpPjPwdGV3sTxsSN.htm)|Claw|auto-trad|
|[WpPQsA9BHFxmr54k.htm](pathfinder-bestiary-2-items/WpPQsA9BHFxmr54k.htm)|Skull|auto-trad|
|[WPVTRIdQSTbDZImk.htm](pathfinder-bestiary-2-items/WPVTRIdQSTbDZImk.htm)|Feral Possession|auto-trad|
|[Wq62yRjG1oubi4Cf.htm](pathfinder-bestiary-2-items/Wq62yRjG1oubi4Cf.htm)|Grab|auto-trad|
|[wQeIEyoNfxwL2mn1.htm](pathfinder-bestiary-2-items/wQeIEyoNfxwL2mn1.htm)|Grab|auto-trad|
|[wqGETtOyNtSqKkGC.htm](pathfinder-bestiary-2-items/wqGETtOyNtSqKkGC.htm)|Grab|auto-trad|
|[wrisTXltKN2ggF1B.htm](pathfinder-bestiary-2-items/wrisTXltKN2ggF1B.htm)|Mandragora Venom|auto-trad|
|[Wrwp9diMkabKorJn.htm](pathfinder-bestiary-2-items/Wrwp9diMkabKorJn.htm)|Beak|auto-trad|
|[ws9YoHlMJ3zrjnM1.htm](pathfinder-bestiary-2-items/ws9YoHlMJ3zrjnM1.htm)|Tail Sweep|auto-trad|
|[WsGuoYeAA0yWHOXE.htm](pathfinder-bestiary-2-items/WsGuoYeAA0yWHOXE.htm)|Pall of Shadow|auto-trad|
|[wSYxanQKAbTVaazk.htm](pathfinder-bestiary-2-items/wSYxanQKAbTVaazk.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[wTAXkMHuxjJpD8OH.htm](pathfinder-bestiary-2-items/wTAXkMHuxjJpD8OH.htm)|Darkvision|auto-trad|
|[WtMCu7aRag8Z4ewh.htm](pathfinder-bestiary-2-items/WtMCu7aRag8Z4ewh.htm)|Swamp Stride|auto-trad|
|[WtPKkX18bEjTQBFy.htm](pathfinder-bestiary-2-items/WtPKkX18bEjTQBFy.htm)|Soul Harvest|auto-trad|
|[WTUxYAzq1Nr3FFwN.htm](pathfinder-bestiary-2-items/WTUxYAzq1Nr3FFwN.htm)|Jaws|auto-trad|
|[wtvDPCazUVSsqJ2O.htm](pathfinder-bestiary-2-items/wtvDPCazUVSsqJ2O.htm)|Negative Healing|auto-trad|
|[WU1jExSnDYhncwTy.htm](pathfinder-bestiary-2-items/WU1jExSnDYhncwTy.htm)|Darkvision|auto-trad|
|[wugLYHXdCUg05j8X.htm](pathfinder-bestiary-2-items/wugLYHXdCUg05j8X.htm)|Sportlebore Infestation|auto-trad|
|[WuwpqYb5cVctrCUQ.htm](pathfinder-bestiary-2-items/WuwpqYb5cVctrCUQ.htm)|Claw|auto-trad|
|[WVChw6DFgtcfsrAf.htm](pathfinder-bestiary-2-items/WVChw6DFgtcfsrAf.htm)|Swarming Beaks|auto-trad|
|[wveW1LFrPr2tdNpt.htm](pathfinder-bestiary-2-items/wveW1LFrPr2tdNpt.htm)|Tentacle|auto-trad|
|[Wvss2Toe2q47z9GS.htm](pathfinder-bestiary-2-items/Wvss2Toe2q47z9GS.htm)|Leg|auto-trad|
|[wwbxcuqrl4U2FZiP.htm](pathfinder-bestiary-2-items/wwbxcuqrl4U2FZiP.htm)|Claw|auto-trad|
|[wWilHEuB4DGYHQdO.htm](pathfinder-bestiary-2-items/wWilHEuB4DGYHQdO.htm)|Jaws|auto-trad|
|[Wx80gWdCaIlQR8Yn.htm](pathfinder-bestiary-2-items/Wx80gWdCaIlQR8Yn.htm)|Darkvision|auto-trad|
|[WXEuGWzgAk5JMfEE.htm](pathfinder-bestiary-2-items/WXEuGWzgAk5JMfEE.htm)|Vulnerable to Prone|auto-trad|
|[WxKg83hAxh4B0VJo.htm](pathfinder-bestiary-2-items/WxKg83hAxh4B0VJo.htm)|Flaming Ghost Touch Longspear|auto-trad|
|[wy1mkkY0rhH0B5oL.htm](pathfinder-bestiary-2-items/wy1mkkY0rhH0B5oL.htm)|Defender of the Seas|auto-trad|
|[wY6wIEuFcKjcTbGS.htm](pathfinder-bestiary-2-items/wY6wIEuFcKjcTbGS.htm)|Improved Grab|auto-trad|
|[wY7gOSBgpUJ4wcUI.htm](pathfinder-bestiary-2-items/wY7gOSBgpUJ4wcUI.htm)|Shortbow|auto-trad|
|[Wyf2nzKbV2LhVCMO.htm](pathfinder-bestiary-2-items/Wyf2nzKbV2LhVCMO.htm)|Scent (Imprecise) 60 feet|auto-trad|
|[wyti8UM7xKPOJOv6.htm](pathfinder-bestiary-2-items/wyti8UM7xKPOJOv6.htm)|Divine Innate Spells|auto-trad|
|[wYU3mjAP9KQSofkn.htm](pathfinder-bestiary-2-items/wYU3mjAP9KQSofkn.htm)|Ghost Bane|auto-trad|
|[wyy8wN5QiLr4fV2L.htm](pathfinder-bestiary-2-items/wyy8wN5QiLr4fV2L.htm)|Fjord Linnorm Venom|auto-trad|
|[WyzkB1NHDFF0YsAm.htm](pathfinder-bestiary-2-items/WyzkB1NHDFF0YsAm.htm)|Improved Grab|auto-trad|
|[WzWOnae18jvHd2C0.htm](pathfinder-bestiary-2-items/WzWOnae18jvHd2C0.htm)|Sadistic Strike|auto-trad|
|[x0HpOyXvVG8ecvWu.htm](pathfinder-bestiary-2-items/x0HpOyXvVG8ecvWu.htm)|Constrict|auto-trad|
|[x0tIw8t8mhFgH7ab.htm](pathfinder-bestiary-2-items/x0tIw8t8mhFgH7ab.htm)|At-Will Spells|auto-trad|
|[x183YuDiYXsrOlKc.htm](pathfinder-bestiary-2-items/x183YuDiYXsrOlKc.htm)|Bone-Chilling Screech|auto-trad|
|[x1avaIq02Cr7LmNs.htm](pathfinder-bestiary-2-items/x1avaIq02Cr7LmNs.htm)|+2 Status Bonus on Saves vs. Enchantment and Illusion Effects|auto-trad|
|[X1Hh7DXiCGoPXh2j.htm](pathfinder-bestiary-2-items/X1Hh7DXiCGoPXh2j.htm)|Jaws|auto-trad|
|[x1kqxURcucjG3BaP.htm](pathfinder-bestiary-2-items/x1kqxURcucjG3BaP.htm)|Change Shape|auto-trad|
|[x2IF1MX8EodRyzeW.htm](pathfinder-bestiary-2-items/x2IF1MX8EodRyzeW.htm)|Rend|auto-trad|
|[x3aksJFYweZk98OP.htm](pathfinder-bestiary-2-items/x3aksJFYweZk98OP.htm)|Retributive Strike|auto-trad|
|[x3sRV0Fub8ixWfBV.htm](pathfinder-bestiary-2-items/x3sRV0Fub8ixWfBV.htm)|Toss|auto-trad|
|[X3x7z0KsDNoqFpey.htm](pathfinder-bestiary-2-items/X3x7z0KsDNoqFpey.htm)|At-Will Spells|auto-trad|
|[X4D3vNjnicVrqgiG.htm](pathfinder-bestiary-2-items/X4D3vNjnicVrqgiG.htm)|Divine Innate Spells|auto-trad|
|[X4O8mzTQLp0lBKqY.htm](pathfinder-bestiary-2-items/X4O8mzTQLp0lBKqY.htm)|Primal Innate Spells|auto-trad|
|[x4vlJSFL6WnsbK82.htm](pathfinder-bestiary-2-items/x4vlJSFL6WnsbK82.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[x5lmNBtvBd1z0Azh.htm](pathfinder-bestiary-2-items/x5lmNBtvBd1z0Azh.htm)|Temporal Reversion|auto-trad|
|[X5uycRcoYkauIGYQ.htm](pathfinder-bestiary-2-items/X5uycRcoYkauIGYQ.htm)|Darkvision|auto-trad|
|[x6cH9h8wAkR78uX9.htm](pathfinder-bestiary-2-items/x6cH9h8wAkR78uX9.htm)|Fist|auto-trad|
|[X6lteRmVzKPr3P02.htm](pathfinder-bestiary-2-items/X6lteRmVzKPr3P02.htm)|Jaws|auto-trad|
|[x7o1u8JU3bkG8XjR.htm](pathfinder-bestiary-2-items/x7o1u8JU3bkG8XjR.htm)|Claw|auto-trad|
|[x7QZQO50H5BxEvkk.htm](pathfinder-bestiary-2-items/x7QZQO50H5BxEvkk.htm)|Starvation Vulnerability|auto-trad|
|[x8IEFlb3LI5XDtkm.htm](pathfinder-bestiary-2-items/x8IEFlb3LI5XDtkm.htm)|Darkvision|auto-trad|
|[X8NPEY2f1Z9LKe53.htm](pathfinder-bestiary-2-items/X8NPEY2f1Z9LKe53.htm)|Scent|auto-trad|
|[x8pzzu8VRuRa5sdg.htm](pathfinder-bestiary-2-items/x8pzzu8VRuRa5sdg.htm)|Stench|auto-trad|
|[x97vWevUTEMqeOCJ.htm](pathfinder-bestiary-2-items/x97vWevUTEMqeOCJ.htm)|Painsight|auto-trad|
|[X9B0ICfO4bmtJ9pv.htm](pathfinder-bestiary-2-items/X9B0ICfO4bmtJ9pv.htm)|Death-Stealing Gaze|auto-trad|
|[X9SDgPuuF9AwEVsK.htm](pathfinder-bestiary-2-items/X9SDgPuuF9AwEVsK.htm)|Darkvision|auto-trad|
|[xa0P6lS6jAgU3iPX.htm](pathfinder-bestiary-2-items/xa0P6lS6jAgU3iPX.htm)|Archon's Door|auto-trad|
|[xA6uZnqBzxOvjJYs.htm](pathfinder-bestiary-2-items/xA6uZnqBzxOvjJYs.htm)|Tongue|auto-trad|
|[XaAIKVkn76K7fFFS.htm](pathfinder-bestiary-2-items/XaAIKVkn76K7fFFS.htm)|Change Shape|auto-trad|
|[XaaNqqz2ZFyt0c4o.htm](pathfinder-bestiary-2-items/XaaNqqz2ZFyt0c4o.htm)|Specious Suggestion|auto-trad|
|[xAlvzCWOjUqsTXiK.htm](pathfinder-bestiary-2-items/xAlvzCWOjUqsTXiK.htm)|Scent|auto-trad|
|[xAtGnv1FFFsyE06f.htm](pathfinder-bestiary-2-items/xAtGnv1FFFsyE06f.htm)|At-Will Spells|auto-trad|
|[xAxdPG2ftegyTaUj.htm](pathfinder-bestiary-2-items/xAxdPG2ftegyTaUj.htm)|Water Jet|auto-trad|
|[Xb5fg9Wf3Iiz5qeu.htm](pathfinder-bestiary-2-items/Xb5fg9Wf3Iiz5qeu.htm)|Wing|auto-trad|
|[xBrIiF2AXw27mMmW.htm](pathfinder-bestiary-2-items/xBrIiF2AXw27mMmW.htm)|Darkvision|auto-trad|
|[Xc15ip4V31NZTNdq.htm](pathfinder-bestiary-2-items/Xc15ip4V31NZTNdq.htm)|Frightful Presence|auto-trad|
|[XCa5wvlPu21MNSQl.htm](pathfinder-bestiary-2-items/XCa5wvlPu21MNSQl.htm)|Horn|auto-trad|
|[XcF1XgnCOdQvrvbz.htm](pathfinder-bestiary-2-items/XcF1XgnCOdQvrvbz.htm)|Talon|auto-trad|
|[XCFOQ29kYuqzushV.htm](pathfinder-bestiary-2-items/XCFOQ29kYuqzushV.htm)|Claw|auto-trad|
|[XcpCwjEjd7OKbKnx.htm](pathfinder-bestiary-2-items/XcpCwjEjd7OKbKnx.htm)|Low-Light Vision|auto-trad|
|[XCr9jxxE59Egb2R1.htm](pathfinder-bestiary-2-items/XCr9jxxE59Egb2R1.htm)|Woodland Ambush|auto-trad|
|[xcs2mYc8h9YshZle.htm](pathfinder-bestiary-2-items/xcs2mYc8h9YshZle.htm)|Attack of Opportunity (Hoof Only)|auto-trad|
|[xdv7JO5TXVfEGxJr.htm](pathfinder-bestiary-2-items/xdv7JO5TXVfEGxJr.htm)|Tenacious Stance|auto-trad|
|[XErVBE2Mmnzz099t.htm](pathfinder-bestiary-2-items/XErVBE2Mmnzz099t.htm)|Darkvision|auto-trad|
|[xgEzGbuJSMIIkD79.htm](pathfinder-bestiary-2-items/xgEzGbuJSMIIkD79.htm)|Frightful Presence|auto-trad|
|[xgpvgBd8BjAbDY4N.htm](pathfinder-bestiary-2-items/xgpvgBd8BjAbDY4N.htm)|Puddled Ambush|auto-trad|
|[XGTj8sfNa1YvUTdZ.htm](pathfinder-bestiary-2-items/XGTj8sfNa1YvUTdZ.htm)|Captivating Lure|auto-trad|
|[xgZ1NJFG41NW7CFG.htm](pathfinder-bestiary-2-items/xgZ1NJFG41NW7CFG.htm)|Telepathy|auto-trad|
|[XHAPOvUbe2eKOW7E.htm](pathfinder-bestiary-2-items/XHAPOvUbe2eKOW7E.htm)|Whirlwind Throw|auto-trad|
|[XhhOGnxq5cUHkcEd.htm](pathfinder-bestiary-2-items/XhhOGnxq5cUHkcEd.htm)|Burble|auto-trad|
|[XHjjW9wEW3pVVm5K.htm](pathfinder-bestiary-2-items/XHjjW9wEW3pVVm5K.htm)|Tendriculos Venom|auto-trad|
|[XHjNDrJBXaCEsi2i.htm](pathfinder-bestiary-2-items/XHjNDrJBXaCEsi2i.htm)|Earth Glide|auto-trad|
|[XHSGBobf5LZDw5R5.htm](pathfinder-bestiary-2-items/XHSGBobf5LZDw5R5.htm)|Jaws|auto-trad|
|[XhyjVYavWAJIwq7b.htm](pathfinder-bestiary-2-items/XhyjVYavWAJIwq7b.htm)|Jaws|auto-trad|
|[xijGvHanHygeHGCM.htm](pathfinder-bestiary-2-items/xijGvHanHygeHGCM.htm)|Hurl Blade|auto-trad|
|[xiOTeaVvejTPBrhk.htm](pathfinder-bestiary-2-items/xiOTeaVvejTPBrhk.htm)|At-Will Spells|auto-trad|
|[XJ7A5MPGGNFZpxXP.htm](pathfinder-bestiary-2-items/XJ7A5MPGGNFZpxXP.htm)|Negative Healing|auto-trad|
|[xJOBW00PxizZ4M8X.htm](pathfinder-bestiary-2-items/xJOBW00PxizZ4M8X.htm)|Draconic Momentum|auto-trad|
|[XjvtH7IilCnzJpIN.htm](pathfinder-bestiary-2-items/XjvtH7IilCnzJpIN.htm)|Darkvision|auto-trad|
|[xKGmyTmllHIjiYO0.htm](pathfinder-bestiary-2-items/xKGmyTmllHIjiYO0.htm)|Earth Shaker|auto-trad|
|[XMbOWL8A6tcWyW18.htm](pathfinder-bestiary-2-items/XMbOWL8A6tcWyW18.htm)|Radiant Wings|auto-trad|
|[xmBUGoRIvL3UEoqs.htm](pathfinder-bestiary-2-items/xmBUGoRIvL3UEoqs.htm)|Breath Weapon|auto-trad|
|[XmKUjz23s0dNFOYW.htm](pathfinder-bestiary-2-items/XmKUjz23s0dNFOYW.htm)|Bog Rot|auto-trad|
|[Xmq08HzwUTnliDuF.htm](pathfinder-bestiary-2-items/Xmq08HzwUTnliDuF.htm)|Fist|auto-trad|
|[XMTEsGgReo4d8QMB.htm](pathfinder-bestiary-2-items/XMTEsGgReo4d8QMB.htm)|Katana|auto-trad|
|[xmYe9vSGZokROsE8.htm](pathfinder-bestiary-2-items/xmYe9vSGZokROsE8.htm)|Blinding Soul|auto-trad|
|[xnaTB4eWK8MokhN6.htm](pathfinder-bestiary-2-items/xnaTB4eWK8MokhN6.htm)|Claw|auto-trad|
|[xNfSi1bsS4hwfcXU.htm](pathfinder-bestiary-2-items/xNfSi1bsS4hwfcXU.htm)|Carnivorous Blob Acid|auto-trad|
|[Xnn4xjhGBTSE9KtK.htm](pathfinder-bestiary-2-items/Xnn4xjhGBTSE9KtK.htm)|Tail|auto-trad|
|[xOfT1xQ4weGRsYHu.htm](pathfinder-bestiary-2-items/xOfT1xQ4weGRsYHu.htm)|Radiant Blast|auto-trad|
|[xomzu6MJJ9pOCVdY.htm](pathfinder-bestiary-2-items/xomzu6MJJ9pOCVdY.htm)|Darkvision|auto-trad|
|[xoV74Ad4ZTLkT5re.htm](pathfinder-bestiary-2-items/xoV74Ad4ZTLkT5re.htm)|Divine Innate Spells|auto-trad|
|[XPEavSJTuusA8TlU.htm](pathfinder-bestiary-2-items/XPEavSJTuusA8TlU.htm)|All-Around Vision|auto-trad|
|[xpfZ2YkQuu4I8kW0.htm](pathfinder-bestiary-2-items/xpfZ2YkQuu4I8kW0.htm)|At-Will Spells|auto-trad|
|[xPid5CwljZFADBGF.htm](pathfinder-bestiary-2-items/xPid5CwljZFADBGF.htm)|Trample|auto-trad|
|[XplLVQ3DKSYJCuYL.htm](pathfinder-bestiary-2-items/XplLVQ3DKSYJCuYL.htm)|At-Will Spells|auto-trad|
|[xPTfLI21EUbrSp4Z.htm](pathfinder-bestiary-2-items/xPTfLI21EUbrSp4Z.htm)|Low-Light Vision|auto-trad|
|[XpTtuP0So5dN4MVl.htm](pathfinder-bestiary-2-items/XpTtuP0So5dN4MVl.htm)|Improved Grab|auto-trad|
|[xq3V1P1OXiEGcqZs.htm](pathfinder-bestiary-2-items/xq3V1P1OXiEGcqZs.htm)|Angled Entry|auto-trad|
|[xqB9FvxJUsDUDs15.htm](pathfinder-bestiary-2-items/xqB9FvxJUsDUDs15.htm)|Thorny Vine|auto-trad|
|[XqbsPpZkRYLwrQDg.htm](pathfinder-bestiary-2-items/XqbsPpZkRYLwrQDg.htm)|Tresses|auto-trad|
|[XqpyTWP6g2DVmNqf.htm](pathfinder-bestiary-2-items/XqpyTWP6g2DVmNqf.htm)|Jaws|auto-trad|
|[XRAQMcQEssAaMi02.htm](pathfinder-bestiary-2-items/XRAQMcQEssAaMi02.htm)|Arcane Innate Spells|auto-trad|
|[xRcOZkgwBwJLmsbr.htm](pathfinder-bestiary-2-items/xRcOZkgwBwJLmsbr.htm)|Constrict|auto-trad|
|[XrJk8sW9W9FE8rtL.htm](pathfinder-bestiary-2-items/XrJk8sW9W9FE8rtL.htm)|Guardian Spirit|auto-trad|
|[xRPyNMWHvevzaNHI.htm](pathfinder-bestiary-2-items/xRPyNMWHvevzaNHI.htm)|Darkvision|auto-trad|
|[xS1M3nE7TiguYe63.htm](pathfinder-bestiary-2-items/xS1M3nE7TiguYe63.htm)|Arm|auto-trad|
|[XSN5GyOi1NAbLqve.htm](pathfinder-bestiary-2-items/XSN5GyOi1NAbLqve.htm)|Holy Armaments|auto-trad|
|[XSrKkFryn35jFMnz.htm](pathfinder-bestiary-2-items/XSrKkFryn35jFMnz.htm)|Darkvision|auto-trad|
|[XucZtbvC1QhYV0HW.htm](pathfinder-bestiary-2-items/XucZtbvC1QhYV0HW.htm)|Ferocity|auto-trad|
|[XUErBTnVDmhG16lW.htm](pathfinder-bestiary-2-items/XUErBTnVDmhG16lW.htm)|Divine Innate Spells|auto-trad|
|[xumH4auLNLHoCwRg.htm](pathfinder-bestiary-2-items/xumH4auLNLHoCwRg.htm)|Telepathy 100 feet|auto-trad|
|[xURZboPgZokTtHAT.htm](pathfinder-bestiary-2-items/xURZboPgZokTtHAT.htm)|Grab|auto-trad|
|[XuX1YKa36ctJmZJH.htm](pathfinder-bestiary-2-items/XuX1YKa36ctJmZJH.htm)|Explosive Rebirth|auto-trad|
|[XWdqUKdwgQ9Cn7vC.htm](pathfinder-bestiary-2-items/XWdqUKdwgQ9Cn7vC.htm)|Club|auto-trad|
|[xwvyEIcVrXBhC3JM.htm](pathfinder-bestiary-2-items/xwvyEIcVrXBhC3JM.htm)|Low-Light Vision|auto-trad|
|[xx6zXcdGLkuW5RmE.htm](pathfinder-bestiary-2-items/xx6zXcdGLkuW5RmE.htm)|Flooding Thrust|auto-trad|
|[xxEXe18G17Aqb8EC.htm](pathfinder-bestiary-2-items/xxEXe18G17Aqb8EC.htm)|Tail|auto-trad|
|[xxK8rXRDWK5Rwj9i.htm](pathfinder-bestiary-2-items/xxK8rXRDWK5Rwj9i.htm)|Divine Innate Spells|auto-trad|
|[xXv6eUeKYXjO80rc.htm](pathfinder-bestiary-2-items/xXv6eUeKYXjO80rc.htm)|Swoop|auto-trad|
|[XyAMzK7AwDLSUd1X.htm](pathfinder-bestiary-2-items/XyAMzK7AwDLSUd1X.htm)|Water Blast|auto-trad|
|[xzuxIMk5QK7iPQ9j.htm](pathfinder-bestiary-2-items/xzuxIMk5QK7iPQ9j.htm)|Divine Innate Spells|auto-trad|
|[Y24jpiGNojJwV4RQ.htm](pathfinder-bestiary-2-items/Y24jpiGNojJwV4RQ.htm)|Improved Grab|auto-trad|
|[Y2i3TiK6GDsS9UIv.htm](pathfinder-bestiary-2-items/Y2i3TiK6GDsS9UIv.htm)|Darkvision|auto-trad|
|[Y3O7AOvH1wkpSV2Z.htm](pathfinder-bestiary-2-items/Y3O7AOvH1wkpSV2Z.htm)|Bite|auto-trad|
|[y5b7lQEAyBanNTYQ.htm](pathfinder-bestiary-2-items/y5b7lQEAyBanNTYQ.htm)|Constrict|auto-trad|
|[y5Va61hQuxXJ9dhb.htm](pathfinder-bestiary-2-items/y5Va61hQuxXJ9dhb.htm)|At-Will Spells|auto-trad|
|[Y6Rqq2ahFM07WKRm.htm](pathfinder-bestiary-2-items/Y6Rqq2ahFM07WKRm.htm)|Swarming Stance|auto-trad|
|[y7LC1NdSY22WtjWg.htm](pathfinder-bestiary-2-items/y7LC1NdSY22WtjWg.htm)|Claw|auto-trad|
|[Y7LW92BfHBvUX6RP.htm](pathfinder-bestiary-2-items/Y7LW92BfHBvUX6RP.htm)|Forfeiture Aversion|auto-trad|
|[y7wcFxhYodNrBPwL.htm](pathfinder-bestiary-2-items/y7wcFxhYodNrBPwL.htm)|Wraith Spawn|auto-trad|
|[Y7ZMEhAtrNgNLJAW.htm](pathfinder-bestiary-2-items/Y7ZMEhAtrNgNLJAW.htm)|Claw|auto-trad|
|[Y810sLO1OzrK8e4z.htm](pathfinder-bestiary-2-items/Y810sLO1OzrK8e4z.htm)|Claw|auto-trad|
|[y9b4KGpxjtDbtBFO.htm](pathfinder-bestiary-2-items/y9b4KGpxjtDbtBFO.htm)|Grab|auto-trad|
|[Y9QK094iVscecdmW.htm](pathfinder-bestiary-2-items/Y9QK094iVscecdmW.htm)|Animate Weapon|auto-trad|
|[yAPG78TDrcEUnBIP.htm](pathfinder-bestiary-2-items/yAPG78TDrcEUnBIP.htm)|Protean Anatomy 8|auto-trad|
|[yb7yQZeKyk9xJFnV.htm](pathfinder-bestiary-2-items/yb7yQZeKyk9xJFnV.htm)|Lifesense 30 feet|auto-trad|
|[YBhZi6V0xuvRTBkT.htm](pathfinder-bestiary-2-items/YBhZi6V0xuvRTBkT.htm)|Maul|auto-trad|
|[YBRIxpMXUrxJfeV1.htm](pathfinder-bestiary-2-items/YBRIxpMXUrxJfeV1.htm)|Grab|auto-trad|
|[YBrJf0B8ODS54eSM.htm](pathfinder-bestiary-2-items/YBrJf0B8ODS54eSM.htm)|Constant Spells|auto-trad|
|[YcPEa5pOVPtksEtZ.htm](pathfinder-bestiary-2-items/YcPEa5pOVPtksEtZ.htm)|Frightful Presence|auto-trad|
|[YdcYSwBDfJFctOhL.htm](pathfinder-bestiary-2-items/YdcYSwBDfJFctOhL.htm)|Pounce|auto-trad|
|[YEQydLBRNZJVJBAN.htm](pathfinder-bestiary-2-items/YEQydLBRNZJVJBAN.htm)|Dagger|auto-trad|
|[YerPyajhgUANJwRq.htm](pathfinder-bestiary-2-items/YerPyajhgUANJwRq.htm)|Darkvision|auto-trad|
|[yf4k8ikFfqMlrB2E.htm](pathfinder-bestiary-2-items/yf4k8ikFfqMlrB2E.htm)|Scent|auto-trad|
|[YfFUNMAKaJ0wspVA.htm](pathfinder-bestiary-2-items/YfFUNMAKaJ0wspVA.htm)|Darkvision|auto-trad|
|[yFIIVeKjDVqU6qlw.htm](pathfinder-bestiary-2-items/yFIIVeKjDVqU6qlw.htm)|Greater Darkvision|auto-trad|
|[yFWK77O4rMtMcEmp.htm](pathfinder-bestiary-2-items/yFWK77O4rMtMcEmp.htm)|Swallow Whole|auto-trad|
|[yH19s0CEMs71KZgv.htm](pathfinder-bestiary-2-items/yH19s0CEMs71KZgv.htm)|+1 Status to All Saves vs. Positive|auto-trad|
|[yHiEUWvnlHVJLlCA.htm](pathfinder-bestiary-2-items/yHiEUWvnlHVJLlCA.htm)|Draining Strike|auto-trad|
|[YHLI3DUcgA33ovkw.htm](pathfinder-bestiary-2-items/YHLI3DUcgA33ovkw.htm)|Jaws|auto-trad|
|[YHmyiWDMvEgujfl1.htm](pathfinder-bestiary-2-items/YHmyiWDMvEgujfl1.htm)|Bully's Bludgeon|auto-trad|
|[YhTp7RIQLVLYYNUn.htm](pathfinder-bestiary-2-items/YhTp7RIQLVLYYNUn.htm)|Sticky Feet|auto-trad|
|[yI8fil9Hp8Ob0BcY.htm](pathfinder-bestiary-2-items/yI8fil9Hp8Ob0BcY.htm)|Occult Innate Spells|auto-trad|
|[yiiBFzSFMWspwb05.htm](pathfinder-bestiary-2-items/yiiBFzSFMWspwb05.htm)|Talon|auto-trad|
|[yinuvv1DAaHpt2dq.htm](pathfinder-bestiary-2-items/yinuvv1DAaHpt2dq.htm)|Divine Innate Spells|auto-trad|
|[yJ6tSdgHei2VkcFn.htm](pathfinder-bestiary-2-items/yJ6tSdgHei2VkcFn.htm)|Swift Claw|auto-trad|
|[yjgVEFL2BwjC3Ylc.htm](pathfinder-bestiary-2-items/yjgVEFL2BwjC3Ylc.htm)|At-Will Spells|auto-trad|
|[YJzGe5kr1qCZKV34.htm](pathfinder-bestiary-2-items/YJzGe5kr1qCZKV34.htm)|Magma Scorpion Venom|auto-trad|
|[yKM6d3CSPA4ymKUk.htm](pathfinder-bestiary-2-items/yKM6d3CSPA4ymKUk.htm)|Constant Spells|auto-trad|
|[yKOp4n7Sk96x65EX.htm](pathfinder-bestiary-2-items/yKOp4n7Sk96x65EX.htm)|Jaws|auto-trad|
|[YkuWnKDR7iQkfOh8.htm](pathfinder-bestiary-2-items/YkuWnKDR7iQkfOh8.htm)|Megalania Venom|auto-trad|
|[ykVZX3vwjXXl9WoS.htm](pathfinder-bestiary-2-items/ykVZX3vwjXXl9WoS.htm)|At-Will Spells|auto-trad|
|[yl8CvIWxZx0c6fcn.htm](pathfinder-bestiary-2-items/yl8CvIWxZx0c6fcn.htm)|Tail|auto-trad|
|[YlO5up5kkSrroKf6.htm](pathfinder-bestiary-2-items/YlO5up5kkSrroKf6.htm)|Tongue Grab|auto-trad|
|[Ym0WhQCJHDGT3v4z.htm](pathfinder-bestiary-2-items/Ym0WhQCJHDGT3v4z.htm)|Attack of Opportunity (Special)|auto-trad|
|[YMLwRoRwaGIKGsjv.htm](pathfinder-bestiary-2-items/YMLwRoRwaGIKGsjv.htm)|Foot|auto-trad|
|[ymt7FhkmnZp7ABEM.htm](pathfinder-bestiary-2-items/ymt7FhkmnZp7ABEM.htm)|Scent|auto-trad|
|[yn2c3QvZMZCfu55r.htm](pathfinder-bestiary-2-items/yn2c3QvZMZCfu55r.htm)|Darkvision|auto-trad|
|[YnfFdQs1dxCIBHYJ.htm](pathfinder-bestiary-2-items/YnfFdQs1dxCIBHYJ.htm)|Darkvision|auto-trad|
|[yNsJmPBe7WVLrPiX.htm](pathfinder-bestiary-2-items/yNsJmPBe7WVLrPiX.htm)|Magic-Warping Aura|auto-trad|
|[yoc8sNwSb16TykKF.htm](pathfinder-bestiary-2-items/yoc8sNwSb16TykKF.htm)|Claw|auto-trad|
|[yokGmVR1dAkRTCmE.htm](pathfinder-bestiary-2-items/yokGmVR1dAkRTCmE.htm)|Trample|auto-trad|
|[yooFGlf4aPfCD1vV.htm](pathfinder-bestiary-2-items/yooFGlf4aPfCD1vV.htm)|Verdant Burst|auto-trad|
|[yqx6BirfDar1gC35.htm](pathfinder-bestiary-2-items/yqx6BirfDar1gC35.htm)|Tremorsense|auto-trad|
|[YRhZxIyDuKIuiHky.htm](pathfinder-bestiary-2-items/YRhZxIyDuKIuiHky.htm)|Sneak Attack|auto-trad|
|[YrMHTjkFMDgVtW5i.htm](pathfinder-bestiary-2-items/YrMHTjkFMDgVtW5i.htm)|Grab|auto-trad|
|[yRTlpaC9IkrTE5xd.htm](pathfinder-bestiary-2-items/yRTlpaC9IkrTE5xd.htm)|Grab|auto-trad|
|[yRuXhvVShtatFwyU.htm](pathfinder-bestiary-2-items/yRuXhvVShtatFwyU.htm)|Telepathy 100 feet|auto-trad|
|[YRXnBeKMp7iUfdDz.htm](pathfinder-bestiary-2-items/YRXnBeKMp7iUfdDz.htm)|Shell Defense|auto-trad|
|[YS1pxmdE8Ebmyy23.htm](pathfinder-bestiary-2-items/YS1pxmdE8Ebmyy23.htm)|Find Target|auto-trad|
|[Ys2OTavkoyvMU7QG.htm](pathfinder-bestiary-2-items/Ys2OTavkoyvMU7QG.htm)|Primal Innate Spells|auto-trad|
|[ysLsaBnQ2SlAerXO.htm](pathfinder-bestiary-2-items/ysLsaBnQ2SlAerXO.htm)|Fatal Faker|auto-trad|
|[YsR1W3Gw985Xzr24.htm](pathfinder-bestiary-2-items/YsR1W3Gw985Xzr24.htm)|Grab|auto-trad|
|[ysswRv1XkPgse3iC.htm](pathfinder-bestiary-2-items/ysswRv1XkPgse3iC.htm)|Petrifying Gaze|auto-trad|
|[ysSzKfSqDhv0OTY7.htm](pathfinder-bestiary-2-items/ysSzKfSqDhv0OTY7.htm)|Fist|auto-trad|
|[YSVs60IJmBC8CFXN.htm](pathfinder-bestiary-2-items/YSVs60IJmBC8CFXN.htm)|Pack Attack|auto-trad|
|[ySwL52AguRVZG0uO.htm](pathfinder-bestiary-2-items/ySwL52AguRVZG0uO.htm)|Claw|auto-trad|
|[yu67ZekTRXDanQoC.htm](pathfinder-bestiary-2-items/yu67ZekTRXDanQoC.htm)|Boiled by Light|auto-trad|
|[YUHuvUIG5Z4hJRM4.htm](pathfinder-bestiary-2-items/YUHuvUIG5Z4hJRM4.htm)|Scent|auto-trad|
|[YuIDIcSKbqQHoLea.htm](pathfinder-bestiary-2-items/YuIDIcSKbqQHoLea.htm)|Low-Light Vision|auto-trad|
|[YV7bdMpIOclaEmMg.htm](pathfinder-bestiary-2-items/YV7bdMpIOclaEmMg.htm)|Attack of Opportunity (Jaws Only)|auto-trad|
|[YvhSv5FSqLRo98YJ.htm](pathfinder-bestiary-2-items/YvhSv5FSqLRo98YJ.htm)|Horn|auto-trad|
|[YvIUXJiNq5FoRcoe.htm](pathfinder-bestiary-2-items/YvIUXJiNq5FoRcoe.htm)|Rend|auto-trad|
|[yW0bi5z1ngsdOjTv.htm](pathfinder-bestiary-2-items/yW0bi5z1ngsdOjTv.htm)|Constant Spells|auto-trad|
|[YwEDGRZpJYEj2PKw.htm](pathfinder-bestiary-2-items/YwEDGRZpJYEj2PKw.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[YwNfN5E1fuqbDqG5.htm](pathfinder-bestiary-2-items/YwNfN5E1fuqbDqG5.htm)|Mindwarping Tide|auto-trad|
|[YXm2W59CMTMhb25j.htm](pathfinder-bestiary-2-items/YXm2W59CMTMhb25j.htm)|Jaws|auto-trad|
|[yxVTa3OmhUhlgNqp.htm](pathfinder-bestiary-2-items/yxVTa3OmhUhlgNqp.htm)|Darkvision|auto-trad|
|[YyBAAbT6VQ7kpDW4.htm](pathfinder-bestiary-2-items/YyBAAbT6VQ7kpDW4.htm)|Jaws|auto-trad|
|[YYnsAUH8Foo8uFnr.htm](pathfinder-bestiary-2-items/YYnsAUH8Foo8uFnr.htm)|Primal Prepared Spells|auto-trad|
|[yZCKHSiJwqO7N1sy.htm](pathfinder-bestiary-2-items/yZCKHSiJwqO7N1sy.htm)|Claws That Catch|auto-trad|
|[yZgxFF5bgFKbTkUO.htm](pathfinder-bestiary-2-items/yZgxFF5bgFKbTkUO.htm)|Swallow Whole|auto-trad|
|[yZN2Ny1Owqd7mZr4.htm](pathfinder-bestiary-2-items/yZN2Ny1Owqd7mZr4.htm)|Primal Prepared Spells|auto-trad|
|[yZqrximA9jX4lr29.htm](pathfinder-bestiary-2-items/yZqrximA9jX4lr29.htm)|Occult Innate Spells|auto-trad|
|[Z08HAZUvnJBGvImG.htm](pathfinder-bestiary-2-items/Z08HAZUvnJBGvImG.htm)|Swarm Mind|auto-trad|
|[Z1OJoOqn3eSaMnv3.htm](pathfinder-bestiary-2-items/Z1OJoOqn3eSaMnv3.htm)|At-Will Spells|auto-trad|
|[Z1uxEMSqDnZV9SCY.htm](pathfinder-bestiary-2-items/Z1uxEMSqDnZV9SCY.htm)|Glow|auto-trad|
|[z3NGGrVV1V5tnFUX.htm](pathfinder-bestiary-2-items/z3NGGrVV1V5tnFUX.htm)|Bodak Spawn|auto-trad|
|[z3wK3ehfyd0WrBic.htm](pathfinder-bestiary-2-items/z3wK3ehfyd0WrBic.htm)|Scent|auto-trad|
|[Z487oTtboxJKexiF.htm](pathfinder-bestiary-2-items/Z487oTtboxJKexiF.htm)|Occult Innate Spells|auto-trad|
|[Z4dJO56WCJUBTpyf.htm](pathfinder-bestiary-2-items/Z4dJO56WCJUBTpyf.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[Z5Ph0zkg0xMl6iK6.htm](pathfinder-bestiary-2-items/Z5Ph0zkg0xMl6iK6.htm)|Occult Innate Spells|auto-trad|
|[Z5SqMzsLOhD9YS8w.htm](pathfinder-bestiary-2-items/Z5SqMzsLOhD9YS8w.htm)|Jaws|auto-trad|
|[Z60YSknQUrMbR4mB.htm](pathfinder-bestiary-2-items/Z60YSknQUrMbR4mB.htm)|Divine Innate Spells|auto-trad|
|[z8CeXCEQdHlFMc0u.htm](pathfinder-bestiary-2-items/z8CeXCEQdHlFMc0u.htm)|Attack of Opportunity (Tentacle Only)|auto-trad|
|[z8DPFDp5ksGcM6bT.htm](pathfinder-bestiary-2-items/z8DPFDp5ksGcM6bT.htm)|Constrict|auto-trad|
|[Z8OBbNUnb8R13SCr.htm](pathfinder-bestiary-2-items/Z8OBbNUnb8R13SCr.htm)|Improved Grab|auto-trad|
|[zA4dkHLp5wB4ZlmE.htm](pathfinder-bestiary-2-items/zA4dkHLp5wB4ZlmE.htm)|Jaws|auto-trad|
|[ZAKbxZPkQhVGL09z.htm](pathfinder-bestiary-2-items/ZAKbxZPkQhVGL09z.htm)|Telepathy|auto-trad|
|[zaLf6csWyorRHycA.htm](pathfinder-bestiary-2-items/zaLf6csWyorRHycA.htm)|Petrifying Glance|auto-trad|
|[zbGgj0YXNPQnaWxc.htm](pathfinder-bestiary-2-items/zbGgj0YXNPQnaWxc.htm)|Foot|auto-trad|
|[zbxStPzCzfkdF5cZ.htm](pathfinder-bestiary-2-items/zbxStPzCzfkdF5cZ.htm)|Claw|auto-trad|
|[zCBg1ZpignZBuhdE.htm](pathfinder-bestiary-2-items/zCBg1ZpignZBuhdE.htm)|Telepathy|auto-trad|
|[zcMAmOWmYshvH5lq.htm](pathfinder-bestiary-2-items/zcMAmOWmYshvH5lq.htm)|Grab|auto-trad|
|[ZCOJIf8Nr9RxA4Pj.htm](pathfinder-bestiary-2-items/ZCOJIf8Nr9RxA4Pj.htm)|Occult Spontaneous Spells|auto-trad|
|[ZcS1mFdodf2L8MND.htm](pathfinder-bestiary-2-items/ZcS1mFdodf2L8MND.htm)|Painsight|auto-trad|
|[ZD69ngyFRQ3FHnUr.htm](pathfinder-bestiary-2-items/ZD69ngyFRQ3FHnUr.htm)|+1 Status to All Saves vs. Magic|auto-trad|
|[ZDDAw2k12iKwWmyi.htm](pathfinder-bestiary-2-items/ZDDAw2k12iKwWmyi.htm)|Darkvision|auto-trad|
|[zDdJpAQ7FmnjLelY.htm](pathfinder-bestiary-2-items/zDdJpAQ7FmnjLelY.htm)|Jaws|auto-trad|
|[ZdHLSjspChUCaebu.htm](pathfinder-bestiary-2-items/ZdHLSjspChUCaebu.htm)|Rhoka Sword|auto-trad|
|[ZDqmS7tceuWsp4M1.htm](pathfinder-bestiary-2-items/ZDqmS7tceuWsp4M1.htm)|Claw|auto-trad|
|[zdW63lmShrSpZWkm.htm](pathfinder-bestiary-2-items/zdW63lmShrSpZWkm.htm)|Otherworldly Vision|auto-trad|
|[ZDynxQcPOYUqgupw.htm](pathfinder-bestiary-2-items/ZDynxQcPOYUqgupw.htm)|Darkvision|auto-trad|
|[zE1qMdAfyFxOdrrK.htm](pathfinder-bestiary-2-items/zE1qMdAfyFxOdrrK.htm)|Swarming Bites|auto-trad|
|[zEg1F3xL2SCNwVj7.htm](pathfinder-bestiary-2-items/zEg1F3xL2SCNwVj7.htm)|Weak Acid|auto-trad|
|[zeyB4l2CYjcqiDrH.htm](pathfinder-bestiary-2-items/zeyB4l2CYjcqiDrH.htm)|Claw|auto-trad|
|[ZfwjAt3nb27vE9CP.htm](pathfinder-bestiary-2-items/ZfwjAt3nb27vE9CP.htm)|Tentacle|auto-trad|
|[zgKk3c4d3SCNJutG.htm](pathfinder-bestiary-2-items/zgKk3c4d3SCNJutG.htm)|Spiked Chain|auto-trad|
|[zglT7mSqeYfcswev.htm](pathfinder-bestiary-2-items/zglT7mSqeYfcswev.htm)|Hair Snare|auto-trad|
|[Zh1eW0zEMgFfvRik.htm](pathfinder-bestiary-2-items/Zh1eW0zEMgFfvRik.htm)|Grab|auto-trad|
|[zhHzZbCi9Wd6r7hj.htm](pathfinder-bestiary-2-items/zhHzZbCi9Wd6r7hj.htm)|Primal Innate Spells|auto-trad|
|[zhobc1PADu4qQ2LB.htm](pathfinder-bestiary-2-items/zhobc1PADu4qQ2LB.htm)|Deep Breath|auto-trad|
|[zhw2nm585TNwCDjL.htm](pathfinder-bestiary-2-items/zhw2nm585TNwCDjL.htm)|Telepathy|auto-trad|
|[zHWFkxssQtSSZH42.htm](pathfinder-bestiary-2-items/zHWFkxssQtSSZH42.htm)|Claw|auto-trad|
|[ZiB2QG6oo1w3v4H2.htm](pathfinder-bestiary-2-items/ZiB2QG6oo1w3v4H2.htm)|Aura of Protection|auto-trad|
|[zIgTjSPfr6rcIZod.htm](pathfinder-bestiary-2-items/zIgTjSPfr6rcIZod.htm)|Attack of Opportunity (Tail Only)|auto-trad|
|[ziM5W7Pshzb5HJ4L.htm](pathfinder-bestiary-2-items/ziM5W7Pshzb5HJ4L.htm)|Divine Innate Spells|auto-trad|
|[zJ48k7LkzzGzYZXF.htm](pathfinder-bestiary-2-items/zJ48k7LkzzGzYZXF.htm)|Paralysis|auto-trad|
|[ZJGndV534YfOPIyl.htm](pathfinder-bestiary-2-items/ZJGndV534YfOPIyl.htm)|Low-Light Vision|auto-trad|
|[zjVbARImBBAx6EKv.htm](pathfinder-bestiary-2-items/zjVbARImBBAx6EKv.htm)|Vile Touch|auto-trad|
|[zJyMYGQVmegoEJJR.htm](pathfinder-bestiary-2-items/zJyMYGQVmegoEJJR.htm)|Horn Sweep|auto-trad|
|[zKvBvpelIhRXPoSW.htm](pathfinder-bestiary-2-items/zKvBvpelIhRXPoSW.htm)|Thoughtsense|auto-trad|
|[zLE5uQwBNSB8wfkR.htm](pathfinder-bestiary-2-items/zLE5uQwBNSB8wfkR.htm)|Petrifying Glance|auto-trad|
|[zMgcdf4D6sTVSdON.htm](pathfinder-bestiary-2-items/zMgcdf4D6sTVSdON.htm)|Scent|auto-trad|
|[zMMs9T5D7z0TyE6H.htm](pathfinder-bestiary-2-items/zMMs9T5D7z0TyE6H.htm)|Divine Innate Spells|auto-trad|
|[ZMxrbMShYOtO3gEG.htm](pathfinder-bestiary-2-items/ZMxrbMShYOtO3gEG.htm)|Infiltration Tools|auto-trad|
|[znBZYCY2qwfdxbV3.htm](pathfinder-bestiary-2-items/znBZYCY2qwfdxbV3.htm)|Primal Innate Spells|auto-trad|
|[zNeDe02hwHzcyE1s.htm](pathfinder-bestiary-2-items/zNeDe02hwHzcyE1s.htm)|Squirming Embrace|auto-trad|
|[znnYZfV4tw03wze8.htm](pathfinder-bestiary-2-items/znnYZfV4tw03wze8.htm)|Wing|auto-trad|
|[Znvln0bJmtXz7d7o.htm](pathfinder-bestiary-2-items/Znvln0bJmtXz7d7o.htm)|Low-Light Vision|auto-trad|
|[ZnwcqtkzrHYsNa9W.htm](pathfinder-bestiary-2-items/ZnwcqtkzrHYsNa9W.htm)|Jaws|auto-trad|
|[zoC1ICbBEHFarVuk.htm](pathfinder-bestiary-2-items/zoC1ICbBEHFarVuk.htm)|Telepathy|auto-trad|
|[ZoL2vMSlQqYIUJAX.htm](pathfinder-bestiary-2-items/ZoL2vMSlQqYIUJAX.htm)|Attack of Opportunity|auto-trad|
|[zp27Hiz99lnmpDcm.htm](pathfinder-bestiary-2-items/zp27Hiz99lnmpDcm.htm)|Constant Spells|auto-trad|
|[ZPQhdXF3s5ZiNwMc.htm](pathfinder-bestiary-2-items/ZPQhdXF3s5ZiNwMc.htm)|Cinder|auto-trad|
|[ZRC02nGn0m7GLUtZ.htm](pathfinder-bestiary-2-items/ZRC02nGn0m7GLUtZ.htm)|Primal Innate Spells|auto-trad|
|[Zt3p0OLPOAG0F5xm.htm](pathfinder-bestiary-2-items/Zt3p0OLPOAG0F5xm.htm)|Grab|auto-trad|
|[zTHlNe6zIRCEQ1Gh.htm](pathfinder-bestiary-2-items/zTHlNe6zIRCEQ1Gh.htm)|Club|auto-trad|
|[Zttbe7aDN1s4zibB.htm](pathfinder-bestiary-2-items/Zttbe7aDN1s4zibB.htm)|Regeneration 15 (deactivated by chaotic)|auto-trad|
|[ZUHGleM4wU1ST5zV.htm](pathfinder-bestiary-2-items/ZUHGleM4wU1ST5zV.htm)|Trident|auto-trad|
|[zuum0VinuCKcAh3D.htm](pathfinder-bestiary-2-items/zuum0VinuCKcAh3D.htm)|Light Hammer|auto-trad|
|[zuY7RRh4sY8cLE7O.htm](pathfinder-bestiary-2-items/zuY7RRh4sY8cLE7O.htm)|Scent|auto-trad|
|[ZuY8xngBTZhm5ag1.htm](pathfinder-bestiary-2-items/ZuY8xngBTZhm5ag1.htm)|Divine Innate Spells|auto-trad|
|[zUZAQv67fQN1itGn.htm](pathfinder-bestiary-2-items/zUZAQv67fQN1itGn.htm)|Negative Healing|auto-trad|
|[zvArmvrursLxbgGq.htm](pathfinder-bestiary-2-items/zvArmvrursLxbgGq.htm)|Gnaw Flesh|auto-trad|
|[zVAwB8xvMUpj9BcX.htm](pathfinder-bestiary-2-items/zVAwB8xvMUpj9BcX.htm)|Jaws|auto-trad|
|[Zvjunjzq6eA7veaV.htm](pathfinder-bestiary-2-items/Zvjunjzq6eA7veaV.htm)|Jaws|auto-trad|
|[zvpqR9uOambi8VIJ.htm](pathfinder-bestiary-2-items/zvpqR9uOambi8VIJ.htm)|Jaws|auto-trad|
|[ZvqnxVWvm9QdJZVi.htm](pathfinder-bestiary-2-items/ZvqnxVWvm9QdJZVi.htm)|Blood Scent|auto-trad|
|[ZvUTgfQlDSMlOtEP.htm](pathfinder-bestiary-2-items/ZvUTgfQlDSMlOtEP.htm)|Entangling Tresses|auto-trad|
|[zVVCyjKNSDxZD2Oy.htm](pathfinder-bestiary-2-items/zVVCyjKNSDxZD2Oy.htm)|Grabbing Trunk|auto-trad|
|[ZvvD1xFO55olkqpZ.htm](pathfinder-bestiary-2-items/ZvvD1xFO55olkqpZ.htm)|Tremorsense (Imprecise) 30 feet|auto-trad|
|[ZVXPGw1qzr4AJYst.htm](pathfinder-bestiary-2-items/ZVXPGw1qzr4AJYst.htm)|Low-Light Vision|auto-trad|
|[ZwbFx8gGrqv10yY5.htm](pathfinder-bestiary-2-items/ZwbFx8gGrqv10yY5.htm)|Foot|auto-trad|
|[zWcMhsfvgxLXuw4D.htm](pathfinder-bestiary-2-items/zWcMhsfvgxLXuw4D.htm)|Constrict|auto-trad|
|[ZWe69IC3j34b3dqx.htm](pathfinder-bestiary-2-items/ZWe69IC3j34b3dqx.htm)|Attack of Opportunity|auto-trad|
|[ZwOinMaTzmRM28rJ.htm](pathfinder-bestiary-2-items/ZwOinMaTzmRM28rJ.htm)|Shadow Scream|auto-trad|
|[zwROdG6waBZIlf4v.htm](pathfinder-bestiary-2-items/zwROdG6waBZIlf4v.htm)|Darkvision|auto-trad|
|[Zxd709xPY5MAZhIj.htm](pathfinder-bestiary-2-items/Zxd709xPY5MAZhIj.htm)|Divine Innate Spells|auto-trad|
|[zYBZpthuiwKFQuoR.htm](pathfinder-bestiary-2-items/zYBZpthuiwKFQuoR.htm)|Primal Prepared Spells|auto-trad|
|[zYVeJXwYqCDo3GsP.htm](pathfinder-bestiary-2-items/zYVeJXwYqCDo3GsP.htm)|Telepathy 100 feet|auto-trad|
|[ZZAHwe62kuHct1sT.htm](pathfinder-bestiary-2-items/ZZAHwe62kuHct1sT.htm)|Low-Light Vision|auto-trad|
|[zZd6BogevnvvWpQx.htm](pathfinder-bestiary-2-items/zZd6BogevnvvWpQx.htm)|Attack of Opportunity|auto-trad|
|[zZs4I9L8EkJWK9UP.htm](pathfinder-bestiary-2-items/zZs4I9L8EkJWK9UP.htm)|Swift Tracker|auto-trad|
|[zzv3YiVwAfvq91jl.htm](pathfinder-bestiary-2-items/zzv3YiVwAfvq91jl.htm)|Fist|auto-trad|
|[ZzXV8so5F6pu1j3r.htm](pathfinder-bestiary-2-items/ZzXV8so5F6pu1j3r.htm)|Low-Light Vision|auto-trad|

## Lista de elementos modificados en VO y que deben comprobarse

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
|[0t8zkWyTQVqJBcMG.htm](pathfinder-bestiary-2-items/0t8zkWyTQVqJBcMG.htm)|Impaling Push|Empellón empalador|modificada|
|[1dUOZTn32vZg4Reo.htm](pathfinder-bestiary-2-items/1dUOZTn32vZg4Reo.htm)|Jaws|Fauces|modificada|
|[1V1IFVEwiJA8p72B.htm](pathfinder-bestiary-2-items/1V1IFVEwiJA8p72B.htm)|Baleful Glow|Baleful Glow|modificada|
|[2ESgkLReH2zyxUIl.htm](pathfinder-bestiary-2-items/2ESgkLReH2zyxUIl.htm)|Pestilential Aura|Aura pestilente|modificada|
|[2R71xX6zTcFcPntf.htm](pathfinder-bestiary-2-items/2R71xX6zTcFcPntf.htm)|Greater Constrict|Mayor Restricción|modificada|
|[9O71o5KF846nZWVa.htm](pathfinder-bestiary-2-items/9O71o5KF846nZWVa.htm)|Swarming Bites|Mordiscos de plaga|modificada|
|[A46dQA87Lnn71IzJ.htm](pathfinder-bestiary-2-items/A46dQA87Lnn71IzJ.htm)|Gory Rend|Gory Rasgadura|modificada|
|[AcMqqRGXMA4crSZt.htm](pathfinder-bestiary-2-items/AcMqqRGXMA4crSZt.htm)|Burning Lash|Burning Lash|modificada|
|[BfXMGYIBKEZIaKVV.htm](pathfinder-bestiary-2-items/BfXMGYIBKEZIaKVV.htm)|Evisceration|Evisceración|modificada|
|[bQyms0FzVu1O5MrR.htm](pathfinder-bestiary-2-items/bQyms0FzVu1O5MrR.htm)|Gnaw Metal|Gnaw Metal|modificada|
|[BVXX982lJiCTVido.htm](pathfinder-bestiary-2-items/BVXX982lJiCTVido.htm)|Bloodletting|Sangría|modificada|
|[BYqZd7dW5USVQMnx.htm](pathfinder-bestiary-2-items/BYqZd7dW5USVQMnx.htm)|Rip and Tear|Rip and Tear|modificada|
|[D5jAYC0XRC68X1gB.htm](pathfinder-bestiary-2-items/D5jAYC0XRC68X1gB.htm)|Greater Constrict|Mayor Restricción|modificada|
|[dkFTU2VIAk9BBNw3.htm](pathfinder-bestiary-2-items/dkFTU2VIAk9BBNw3.htm)|Greater Constrict|Mayor Restricción|modificada|
|[dp75b5be8kghUzzi.htm](pathfinder-bestiary-2-items/dp75b5be8kghUzzi.htm)|Ventral Tube|Tubo Ventral|modificada|
|[DT1P11EFBYdmCUeQ.htm](pathfinder-bestiary-2-items/DT1P11EFBYdmCUeQ.htm)|Claw|Garra|modificada|
|[DU6e2UkT2fc49Zk3.htm](pathfinder-bestiary-2-items/DU6e2UkT2fc49Zk3.htm)|Lesser Alchemist's Fire|Fuego de alquimista menor|modificada|
|[E5KXLULpkukpqygI.htm](pathfinder-bestiary-2-items/E5KXLULpkukpqygI.htm)|Claw|Garra|modificada|
|[EAsAGjtMDBLWo0ZO.htm](pathfinder-bestiary-2-items/EAsAGjtMDBLWo0ZO.htm)|+1 Status to All Saves vs. Mental|+2 situación a todas las salvaciones contra mental.|modificada|
|[eoLK3mAvvXiMTLnH.htm](pathfinder-bestiary-2-items/eoLK3mAvvXiMTLnH.htm)|Twist the Hook|Retorcer el Gancho|modificada|
|[F40AJYqQ0BCC2TF0.htm](pathfinder-bestiary-2-items/F40AJYqQ0BCC2TF0.htm)|Chain|Cadena|modificada|
|[fDD7cIf8ZsYLF8fv.htm](pathfinder-bestiary-2-items/fDD7cIf8ZsYLF8fv.htm)|Impaling Critical|Empalar Crítico|modificada|
|[fJR0vMezUrIu4nlW.htm](pathfinder-bestiary-2-items/fJR0vMezUrIu4nlW.htm)|Tail Sting|Tail Sting|modificada|
|[grFGZyZ8952tQ9yF.htm](pathfinder-bestiary-2-items/grFGZyZ8952tQ9yF.htm)|Entombing Breath|Entombing Breath|modificada|
|[gVLqh4wDw9v6njkz.htm](pathfinder-bestiary-2-items/gVLqh4wDw9v6njkz.htm)|Jaws|Fauces|modificada|
|[HGVXqBgfTKK0Gu4n.htm](pathfinder-bestiary-2-items/HGVXqBgfTKK0Gu4n.htm)|Trunk|Tronco|modificada|
|[IukwgU6DfXFrQy49.htm](pathfinder-bestiary-2-items/IukwgU6DfXFrQy49.htm)|Inflict Warpwave|Infligir Onda Warp|modificada|
|[J2UWT1TZOpg7ODbW.htm](pathfinder-bestiary-2-items/J2UWT1TZOpg7ODbW.htm)|Stoke the Fervent|Avivar a los fervientes|modificada|
|[J8KaQAnTGQqYov9e.htm](pathfinder-bestiary-2-items/J8KaQAnTGQqYov9e.htm)|Xill Eggs|Xill Eggs|modificada|
|[JCHWJ2nJ8ZmLOBs5.htm](pathfinder-bestiary-2-items/JCHWJ2nJ8ZmLOBs5.htm)|Breath Weapon|Breath Weapon|modificada|
|[k6pOje8wFsRd6e1G.htm](pathfinder-bestiary-2-items/k6pOje8wFsRd6e1G.htm)|Pincer|Pinza|modificada|
|[kO1NYbSXz3VNkr4L.htm](pathfinder-bestiary-2-items/kO1NYbSXz3VNkr4L.htm)|Claw|Garra|modificada|
|[kRAvWqrWJqwc6KU3.htm](pathfinder-bestiary-2-items/kRAvWqrWJqwc6KU3.htm)|Jaws|Fauces|modificada|
|[kzI56kmfH5aQquo7.htm](pathfinder-bestiary-2-items/kzI56kmfH5aQquo7.htm)|Ice Claw|Ice Claw|modificada|
|[l6Q79Qlb9vVTOERz.htm](pathfinder-bestiary-2-items/l6Q79Qlb9vVTOERz.htm)|Hurled Weapon|Arma Arrojadiza|modificada|
|[lD6n6NYRsC4x82eE.htm](pathfinder-bestiary-2-items/lD6n6NYRsC4x82eE.htm)|Fire Missile|Misil de Fuego|modificada|
|[LVDdXXZ03SwDUNZ8.htm](pathfinder-bestiary-2-items/LVDdXXZ03SwDUNZ8.htm)|Jaws|Fauces|modificada|
|[m8k2uRU1UVmPgF03.htm](pathfinder-bestiary-2-items/m8k2uRU1UVmPgF03.htm)|Regeneration 10 (Deactivated by Cold)|Regeneración|modificada|
|[mTE12ORozb0LZ81C.htm](pathfinder-bestiary-2-items/mTE12ORozb0LZ81C.htm)|Rock|Roca|modificada|
|[Mtkrvum6bboSrXtq.htm](pathfinder-bestiary-2-items/Mtkrvum6bboSrXtq.htm)|Sanguine Mauling|Zarpazo doble sanguíneo|modificada|
|[nKL6rZMmn2WdRk3g.htm](pathfinder-bestiary-2-items/nKL6rZMmn2WdRk3g.htm)|Cling|Cling|modificada|
|[nKM6FVNsXbBwEPNq.htm](pathfinder-bestiary-2-items/nKM6FVNsXbBwEPNq.htm)|Gnaw|Gnaw|modificada|
|[nMVv9cAH945ESnI9.htm](pathfinder-bestiary-2-items/nMVv9cAH945ESnI9.htm)|Claw|Garra|modificada|
|[ome4gBGaz20PH3OU.htm](pathfinder-bestiary-2-items/ome4gBGaz20PH3OU.htm)|Blood Draining Bites|Muerdemuerde Drenando sangre|modificada|
|[pUwo9wumwrjtvid1.htm](pathfinder-bestiary-2-items/pUwo9wumwrjtvid1.htm)|Infernal Wound|Herida infernal|modificada|
|[QmK15RaSe9yfdYIR.htm](pathfinder-bestiary-2-items/QmK15RaSe9yfdYIR.htm)|Vulnerable Tail|Cola Vulnerable|modificada|
|[RZEi7CewQZtkaHnE.htm](pathfinder-bestiary-2-items/RZEi7CewQZtkaHnE.htm)|Bleeding Critical|Sangrado Crítico|modificada|
|[sSAVmevencpx75UB.htm](pathfinder-bestiary-2-items/sSAVmevencpx75UB.htm)|Pestilential Aura|Aura pestilente|modificada|
|[TWEbrceKTc0P1GrD.htm](pathfinder-bestiary-2-items/TWEbrceKTc0P1GrD.htm)|Ember Ball|Bola de Ascuas|modificada|
|[uzaPYD2SNbrlxuM9.htm](pathfinder-bestiary-2-items/uzaPYD2SNbrlxuM9.htm)|Jaws|Fauces|modificada|
|[VLwTahhYo1OObMUv.htm](pathfinder-bestiary-2-items/VLwTahhYo1OObMUv.htm)|Greater Constrict|Mayor Restricción|modificada|
|[wdSBw6qGzwcD9eZu.htm](pathfinder-bestiary-2-items/wdSBw6qGzwcD9eZu.htm)|Gory Hydration|Hidratación Gory|modificada|
|[WqAfn6xBB95rkCnJ.htm](pathfinder-bestiary-2-items/WqAfn6xBB95rkCnJ.htm)|Barbed Chain|Cadena de púas|modificada|
|[X8BOiH4KPnC3n94o.htm](pathfinder-bestiary-2-items/X8BOiH4KPnC3n94o.htm)|Regeneration 15 (Deactivated by Acid or Fire)|Regeneración 15 (ácido o fuego)|modificada|
|[xCffJ5CYclgmYzmC.htm](pathfinder-bestiary-2-items/xCffJ5CYclgmYzmC.htm)|Sticky Filament|Sticky Filament|modificada|
|[XZ1UeBvCwRe6I510.htm](pathfinder-bestiary-2-items/XZ1UeBvCwRe6I510.htm)|Magma Spit|Magma Spit|modificada|
|[y0g8S6A5Uri1G8xe.htm](pathfinder-bestiary-2-items/y0g8S6A5Uri1G8xe.htm)|Drill|Taladro|modificada|
|[zcW44MHFdHDFSbQ5.htm](pathfinder-bestiary-2-items/zcW44MHFdHDFSbQ5.htm)|Jaws|Fauces|modificada|

## Lista de elementos vacíos que no se pueden traducir

| Fichero   | Nombre (EN)    | Estado |
|-----------|----------------|:------:|
|[0D7BuUymOeEx7VEM.htm](pathfinder-bestiary-2-items/0D7BuUymOeEx7VEM.htm)|Athletics|vacía|
|[0DWjvOoTNOT0cP4O.htm](pathfinder-bestiary-2-items/0DWjvOoTNOT0cP4O.htm)|Stealth|vacía|
|[1gu2mF6EgXl6v3eM.htm](pathfinder-bestiary-2-items/1gu2mF6EgXl6v3eM.htm)|Crafting|vacía|
|[1n37HSEuc8iJMxun.htm](pathfinder-bestiary-2-items/1n37HSEuc8iJMxun.htm)|Soul Gem|vacía|
|[1yoCf7iLNmn7SD5D.htm](pathfinder-bestiary-2-items/1yoCf7iLNmn7SD5D.htm)|Stealth|vacía|
|[27VDcYxOuStCRZ4n.htm](pathfinder-bestiary-2-items/27VDcYxOuStCRZ4n.htm)|Sailing Lore|vacía|
|[2C1Wu7Y8e0BUVKgV.htm](pathfinder-bestiary-2-items/2C1Wu7Y8e0BUVKgV.htm)|One or more Lore skills related to a specific plane|vacía|
|[2I1FvVi46bCoWd1l.htm](pathfinder-bestiary-2-items/2I1FvVi46bCoWd1l.htm)|Gold Lore|vacía|
|[2Kexngs6KXliJ7qM.htm](pathfinder-bestiary-2-items/2Kexngs6KXliJ7qM.htm)|Torture Lore|vacía|
|[2wByf3NmPbP0YRzy.htm](pathfinder-bestiary-2-items/2wByf3NmPbP0YRzy.htm)|Leng Ruby|vacía|
|[3ekp1Wmg9yNIy9iX.htm](pathfinder-bestiary-2-items/3ekp1Wmg9yNIy9iX.htm)|Crafting|vacía|
|[3IKSOIIWyaxP7Xbh.htm](pathfinder-bestiary-2-items/3IKSOIIWyaxP7Xbh.htm)|Stealth|vacía|
|[48eItql3Drlle3Rb.htm](pathfinder-bestiary-2-items/48eItql3Drlle3Rb.htm)|Athletics|vacía|
|[4OYFMbbC38lWGl4D.htm](pathfinder-bestiary-2-items/4OYFMbbC38lWGl4D.htm)|Warfare Lore|vacía|
|[4s8ELNWXmsGfFB7V.htm](pathfinder-bestiary-2-items/4s8ELNWXmsGfFB7V.htm)|Astronomy Lore|vacía|
|[5H5M7ucca4W3guPv.htm](pathfinder-bestiary-2-items/5H5M7ucca4W3guPv.htm)|Abaddon Lore|vacía|
|[5ImSwYK5kXJKpZbH.htm](pathfinder-bestiary-2-items/5ImSwYK5kXJKpZbH.htm)|Gemstone|vacía|
|[6dch35LAYgze01qE.htm](pathfinder-bestiary-2-items/6dch35LAYgze01qE.htm)|Stealth|vacía|
|[7iLw2emMidGNxyzJ.htm](pathfinder-bestiary-2-items/7iLw2emMidGNxyzJ.htm)|Stealth|vacía|
|[7ovDTTZtTBqn2eJ6.htm](pathfinder-bestiary-2-items/7ovDTTZtTBqn2eJ6.htm)|Performance|vacía|
|[8czzhMhJOn9zi2gL.htm](pathfinder-bestiary-2-items/8czzhMhJOn9zi2gL.htm)|Sack of Rocks|vacía|
|[8EaMfqK0UpEoUqcg.htm](pathfinder-bestiary-2-items/8EaMfqK0UpEoUqcg.htm)|Acrobatics|vacía|
|[9E67q4C5Qrwk0OqK.htm](pathfinder-bestiary-2-items/9E67q4C5Qrwk0OqK.htm)|Survival|vacía|
|[9fV5FkAeZkNfBxHH.htm](pathfinder-bestiary-2-items/9fV5FkAeZkNfBxHH.htm)|Legal Lore|vacía|
|[bHdiDlV2uqMjzq0I.htm](pathfinder-bestiary-2-items/bHdiDlV2uqMjzq0I.htm)|Astronomy Lore|vacía|
|[BlCEwmbLQjpsyFMQ.htm](pathfinder-bestiary-2-items/BlCEwmbLQjpsyFMQ.htm)|Torture Lore|vacía|
|[bo6bsp0fKrJ9ddNK.htm](pathfinder-bestiary-2-items/bo6bsp0fKrJ9ddNK.htm)|Stealth|vacía|
|[BxpAAUmfrga656x8.htm](pathfinder-bestiary-2-items/BxpAAUmfrga656x8.htm)|Sack of Rocks|vacía|
|[c4ByZjVeW0km1U7W.htm](pathfinder-bestiary-2-items/c4ByZjVeW0km1U7W.htm)|Stealth|vacía|
|[cbhhzA952sTW7Nlb.htm](pathfinder-bestiary-2-items/cbhhzA952sTW7Nlb.htm)|Stealth|vacía|
|[CdF08U1FHM2c2DIB.htm](pathfinder-bestiary-2-items/CdF08U1FHM2c2DIB.htm)|Athletics|vacía|
|[D5coiskeAG5F1V0f.htm](pathfinder-bestiary-2-items/D5coiskeAG5F1V0f.htm)|Lore (All)|vacía|
|[dmYyiqu2DVxTAvbq.htm](pathfinder-bestiary-2-items/dmYyiqu2DVxTAvbq.htm)|Torture Lore|vacía|
|[e3XyV4HjxKBZucFh.htm](pathfinder-bestiary-2-items/e3XyV4HjxKBZucFh.htm)|Stealth|vacía|
|[gfmpjwebcEUzSXoN.htm](pathfinder-bestiary-2-items/gfmpjwebcEUzSXoN.htm)|Stealth|vacía|
|[Hc2v6MiajU4rABkT.htm](pathfinder-bestiary-2-items/Hc2v6MiajU4rABkT.htm)|Rock|vacía|
|[HH2FpUMFgxmQCgXz.htm](pathfinder-bestiary-2-items/HH2FpUMFgxmQCgXz.htm)|Torture Lore|vacía|
|[HUJdrgu9JAWlm7bC.htm](pathfinder-bestiary-2-items/HUJdrgu9JAWlm7bC.htm)|Planar Lore|vacía|
|[hX2hdrvbUdBKffJD.htm](pathfinder-bestiary-2-items/hX2hdrvbUdBKffJD.htm)|Stealth|vacía|
|[Ic32ezDxqx8TwcBo.htm](pathfinder-bestiary-2-items/Ic32ezDxqx8TwcBo.htm)|Stealth|vacía|
|[ICtnIf0c4SkY2NnB.htm](pathfinder-bestiary-2-items/ICtnIf0c4SkY2NnB.htm)|Athletics|vacía|
|[ihi2vOTXGjRtjggj.htm](pathfinder-bestiary-2-items/ihi2vOTXGjRtjggj.htm)|Performance|vacía|
|[ii5QAwwyYnQHUwH2.htm](pathfinder-bestiary-2-items/ii5QAwwyYnQHUwH2.htm)|Athletics|vacía|
|[IP586mLtCybAAuh2.htm](pathfinder-bestiary-2-items/IP586mLtCybAAuh2.htm)|Steeped Weapon (7-10)|vacía|
|[iqXZDQJZ5iZnj2D0.htm](pathfinder-bestiary-2-items/iqXZDQJZ5iZnj2D0.htm)|Stealth|vacía|
|[j086JS96ezs39PtC.htm](pathfinder-bestiary-2-items/j086JS96ezs39PtC.htm)|Desert Lore|vacía|
|[jaF864bUYkmqmrC8.htm](pathfinder-bestiary-2-items/jaF864bUYkmqmrC8.htm)|Stealth|vacía|
|[jljKVq6TmhKOQUSh.htm](pathfinder-bestiary-2-items/jljKVq6TmhKOQUSh.htm)|Stealth|vacía|
|[jsZXrLmJJQhLrRL0.htm](pathfinder-bestiary-2-items/jsZXrLmJJQhLrRL0.htm)|Stealth|vacía|
|[JzkjkxNi7EFlzsmZ.htm](pathfinder-bestiary-2-items/JzkjkxNi7EFlzsmZ.htm)|Stealth|vacía|
|[khAWacLm5F3BV7QU.htm](pathfinder-bestiary-2-items/khAWacLm5F3BV7QU.htm)|Axis Lore|vacía|
|[kHGiew7m0qu0wSCx.htm](pathfinder-bestiary-2-items/kHGiew7m0qu0wSCx.htm)|Stealth|vacía|
|[kk0U4TyvtgBAzAAR.htm](pathfinder-bestiary-2-items/kk0U4TyvtgBAzAAR.htm)|Stealth|vacía|
|[kVbm9Ksg6rWgtnjS.htm](pathfinder-bestiary-2-items/kVbm9Ksg6rWgtnjS.htm)|Abyss Lore|vacía|
|[KwNXLXzskiVB9RLh.htm](pathfinder-bestiary-2-items/KwNXLXzskiVB9RLh.htm)|Stealth|vacía|
|[kynHGhmwcc8qJ5T8.htm](pathfinder-bestiary-2-items/kynHGhmwcc8qJ5T8.htm)|Stealth|vacía|
|[L0IZRmIosB8Icqo0.htm](pathfinder-bestiary-2-items/L0IZRmIosB8Icqo0.htm)|Athletics|vacía|
|[L3qJCQDZiZu5wjEW.htm](pathfinder-bestiary-2-items/L3qJCQDZiZu5wjEW.htm)|Stealth|vacía|
|[LCzHgVdDCfLFTsdX.htm](pathfinder-bestiary-2-items/LCzHgVdDCfLFTsdX.htm)|Axis Lore|vacía|
|[LTW5DHnhGB6HMte6.htm](pathfinder-bestiary-2-items/LTW5DHnhGB6HMte6.htm)|Stealth|vacía|
|[LUdvij5KTX4oc3yH.htm](pathfinder-bestiary-2-items/LUdvij5KTX4oc3yH.htm)|Sailing Lore|vacía|
|[MgWcgCTxwxAVsQWD.htm](pathfinder-bestiary-2-items/MgWcgCTxwxAVsQWD.htm)|Styx Lore|vacía|
|[mKdD7XKIAXJx8KnC.htm](pathfinder-bestiary-2-items/mKdD7XKIAXJx8KnC.htm)|Athletics|vacía|
|[NEQLZhd63z6qQPXJ.htm](pathfinder-bestiary-2-items/NEQLZhd63z6qQPXJ.htm)|Stealth|vacía|
|[neZCdGIJBKYWKN5Q.htm](pathfinder-bestiary-2-items/neZCdGIJBKYWKN5Q.htm)|Deception|vacía|
|[nm9xf6FWIfjkblnF.htm](pathfinder-bestiary-2-items/nm9xf6FWIfjkblnF.htm)|Forest Lore|vacía|
|[nwepl5Un9QYNK5Gk.htm](pathfinder-bestiary-2-items/nwepl5Un9QYNK5Gk.htm)|Crafting|vacía|
|[nWSidIDFacSNApv8.htm](pathfinder-bestiary-2-items/nWSidIDFacSNApv8.htm)|Stealth|vacía|
|[OOgOPxe66wl8FqEm.htm](pathfinder-bestiary-2-items/OOgOPxe66wl8FqEm.htm)|Athletics|vacía|
|[OQE9ap0Mj3pykdzk.htm](pathfinder-bestiary-2-items/OQE9ap0Mj3pykdzk.htm)|Acrobatics|vacía|
|[OT4mRkq8mC5ESE4d.htm](pathfinder-bestiary-2-items/OT4mRkq8mC5ESE4d.htm)|Stealth|vacía|
|[PRT7NwigGETAdytk.htm](pathfinder-bestiary-2-items/PRT7NwigGETAdytk.htm)|Torture Lore|vacía|
|[q0drSu1C5WWWuGEa.htm](pathfinder-bestiary-2-items/q0drSu1C5WWWuGEa.htm)|Genealogy Lore|vacía|
|[QaRhnU95T3Ie2dFP.htm](pathfinder-bestiary-2-items/QaRhnU95T3Ie2dFP.htm)|Stealth|vacía|
|[qjQkjjoCkG0wYEyP.htm](pathfinder-bestiary-2-items/qjQkjjoCkG0wYEyP.htm)|Soul Gem|vacía|
|[rET5SfCVJHTK7d5a.htm](pathfinder-bestiary-2-items/rET5SfCVJHTK7d5a.htm)|Athletics|vacía|
|[rs8cxUHAMnuhP7nv.htm](pathfinder-bestiary-2-items/rs8cxUHAMnuhP7nv.htm)|Stealth|vacía|
|[SAE0s9ZeUW0uDBDR.htm](pathfinder-bestiary-2-items/SAE0s9ZeUW0uDBDR.htm)|Conch Shell|vacía|
|[SEbaazZFQvxYSyns.htm](pathfinder-bestiary-2-items/SEbaazZFQvxYSyns.htm)|Gem Lore|vacía|
|[SlJZiCxXuBW155d5.htm](pathfinder-bestiary-2-items/SlJZiCxXuBW155d5.htm)|Athletics|vacía|
|[Sv1H3JJKwu64RfI1.htm](pathfinder-bestiary-2-items/Sv1H3JJKwu64RfI1.htm)|Athletics|vacía|
|[T2RZcW0Xl08THP7f.htm](pathfinder-bestiary-2-items/T2RZcW0Xl08THP7f.htm)|Games Lore|vacía|
|[T3B78udYCNXsT35C.htm](pathfinder-bestiary-2-items/T3B78udYCNXsT35C.htm)|Boneyard Lore|vacía|
|[tJgewoTin1AzHV0a.htm](pathfinder-bestiary-2-items/tJgewoTin1AzHV0a.htm)|Stealth|vacía|
|[Tml8KbX9Hdipnofj.htm](pathfinder-bestiary-2-items/Tml8KbX9Hdipnofj.htm)|Stealth|vacía|
|[TWwOYkc75X6kXsmZ.htm](pathfinder-bestiary-2-items/TWwOYkc75X6kXsmZ.htm)|Warfare Lore|vacía|
|[TZThNzsi8RUQFhrT.htm](pathfinder-bestiary-2-items/TZThNzsi8RUQFhrT.htm)|Stealth|vacía|
|[UaG0xt5nSwesLzma.htm](pathfinder-bestiary-2-items/UaG0xt5nSwesLzma.htm)|Stealth|vacía|
|[ujbUCBlQf3V81dRB.htm](pathfinder-bestiary-2-items/ujbUCBlQf3V81dRB.htm)|Torture Lore|vacía|
|[vcS0yHPna8VNFFeW.htm](pathfinder-bestiary-2-items/vcS0yHPna8VNFFeW.htm)|Elysium Lore|vacía|
|[ViIeviMg3Zbi0PdS.htm](pathfinder-bestiary-2-items/ViIeviMg3Zbi0PdS.htm)|Crafting|vacía|
|[vOVPKQFfOLMWV7F2.htm](pathfinder-bestiary-2-items/vOVPKQFfOLMWV7F2.htm)|Stealth|vacía|
|[w2BCKUoJdnaAE7ty.htm](pathfinder-bestiary-2-items/w2BCKUoJdnaAE7ty.htm)|Stealth|vacía|
|[We8X2oHUEJqX1ikE.htm](pathfinder-bestiary-2-items/We8X2oHUEJqX1ikE.htm)|Warfare Lore|vacía|
|[wmiM8DltOKYLxzz0.htm](pathfinder-bestiary-2-items/wmiM8DltOKYLxzz0.htm)|Stealth|vacía|
|[wtTc3hKG8EYcuNer.htm](pathfinder-bestiary-2-items/wtTc3hKG8EYcuNer.htm)|Sea Lore|vacía|
|[XhhI5NeGekRv5yOP.htm](pathfinder-bestiary-2-items/XhhI5NeGekRv5yOP.htm)|Stealth|vacía|
|[XIeSqagI3L9jCuKJ.htm](pathfinder-bestiary-2-items/XIeSqagI3L9jCuKJ.htm)|Crafting|vacía|
|[xPRIbGrSs4dfXGUK.htm](pathfinder-bestiary-2-items/xPRIbGrSs4dfXGUK.htm)|Soul Gem|vacía|
|[xrD7IZz445jQXJ8L.htm](pathfinder-bestiary-2-items/xrD7IZz445jQXJ8L.htm)|Athletics|vacía|
|[y7wtukvra9IyHCPL.htm](pathfinder-bestiary-2-items/y7wtukvra9IyHCPL.htm)|Boneyard Lore|vacía|
|[YjPhCJNyJk71S8rV.htm](pathfinder-bestiary-2-items/YjPhCJNyJk71S8rV.htm)|Warfare Lore|vacía|
|[ysZ3MQQymY3E22BQ.htm](pathfinder-bestiary-2-items/ysZ3MQQymY3E22BQ.htm)|Survival|vacía|
|[Z3rqV4GSnIVC3AJ3.htm](pathfinder-bestiary-2-items/Z3rqV4GSnIVC3AJ3.htm)|Sack of Rocks|vacía|
|[Zi1IXK4SmJsyEThu.htm](pathfinder-bestiary-2-items/Zi1IXK4SmJsyEThu.htm)|Athletics|vacía|
|[zie4H1Ei8xqTDKSd.htm](pathfinder-bestiary-2-items/zie4H1Ei8xqTDKSd.htm)|Boneyard Lore|vacía|
|[zsRr07XFkA5e8PQ3.htm](pathfinder-bestiary-2-items/zsRr07XFkA5e8PQ3.htm)|Stealth|vacía|

## Lista de traducciones realizadas

| Fichero   | Nombre (EN)    | Nombre (ES)    | Estado |
|-----------|----------------|----------------|:------:|
